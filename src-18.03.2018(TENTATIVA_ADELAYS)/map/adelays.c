// Copyright (c) Adelays - Licensed under GNU GPL
// adelays@ragnawork.com
// http://ragnawork.com


#define _GNU_SOURCE 1

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <ctype.h>

#include "../common/showmsg.h"
#include "../common/timer.h"
#include "../common/nullpo.h"
#include "../common/db.h"
#include "../common/malloc.h"
#include "../common/utils.h"
#include "../common/strlib.h"
#include "../common/cbasetypes.h"
#include "skill.h"
#include "map.h"
#include <string.h>
#include <math.h>
#include <stdarg.h>
#include "adelays.h"

struct adelays_jobDelay {
	int classId;
	int option;
	int sex;
	float per;
};

struct _adelays_skill_db {
	int adelays_delay150;
	int adelays_delay190;
	int adelays_harddelay150;
	int adelays_harddelay190;
	int adelays_force_harddelay;
	int adelays_harddelay_chain_max;
	int adelays_harddelay_is_double;
	int adelays_option;
	int adelays_allowhitlock;
};

struct _adelays_item_db {
	int nameid;
	int option;
};

char data[50000];

 struct _adelays_item_db adelays_item_db[100];
 struct adelays_jobDelay adelays_jobDelays[100];
 struct _adelays_skill_db adelays_skill_db[MAX_SKILL_DB];

struct Adelays_Config adelays_config;


 const struct _adelays_data {
	const char* str;
	int* val;
	int defval;
	int min;
	int max;
} adelays_data[] = {

	{ "adelays_tol", &adelays_config.adelays_tol, 100, 1, INT_MAX, },
	{ "adelays_adelays_hitlock_time_tolerance", &adelays_config.adelays_hitlock_time_tolerance, 200, 0, INT_MAX, },

	{ "adelays_fixeddelay_tol", &adelays_config.adelays_fixeddelay_tol, 30, 0, INT_MAX, },

	{ "adelays_incr", &adelays_config.adelays_incr, 1, 0, INT_MAX, },
	{ "adelays_hitlock_incr", &adelays_config.adelays_hitlock_incr, 1, 0, INT_MAX, },
	{ "adelays_max_hitlock_count", &adelays_config.adelays_max_hitlock_count, 3, 0, INT_MAX, },

	{ "adelays_reset_time", &adelays_config.adelays_reset_time, 1000000, 0, INT_MAX, },
	{ "adelays_reduce_hitlock_on_correct", &adelays_config.adelays_reduce_hitlock_on_correct, 1, 0, INT_MAX, },
	{ "adelays_reduce_hitlock_on_hitlock", &adelays_config.adelays_reduce_hitlock_on_hitlock, 1, 0, INT_MAX, },
	{ "adelays_enable", &adelays_config.adelays_enable, 1, 0, INT_MAX, },
	{ "adelays_reduce_hackcount_on_correct", &adelays_config.adelays_reduce_hackcount_on_correct, 0, 0, INT_MAX, },
	{ "adelays_reduce_hackcount_on_hitlock", &adelays_config.adelays_reduce_hackcount_on_hitlock, 1, 0, INT_MAX, },
	{ "adelays_buffer_time", &adelays_config.adelays_buffer_time, 50, 0, INT_MAX, },
	{ "adelays_buffer_size", &adelays_config.adelays_buffer_size, 2, 0, INT_MAX, },
	{ "adelays_max_infixed", &adelays_config.adelays_max_infixed, 20, 0, INT_MAX, },
	{ "adelays_scroll_fix", &adelays_config.adelays_scroll_fix, 1, 0, INT_MAX, },
	{ "adelays_jauge_kept_multiplicator_on_new_skill_without_delay", &adelays_config.adelays_jauge_kept_multiplicator_on_new_skill_without_delay, 4, 0, INT_MAX, },
	{ "adelays_jauge_kept_multiplicator", &adelays_config.adelays_jauge_kept_multiplicator, 3, 0, INT_MAX, },
	{ "adelays_fix_delay_multiplicator", &adelays_config.adelays_fix_delay_multiplicator, 5, 0, INT_MAX, },
	{ "adelays_jauge_augmentation_multiplicator", &adelays_config.adelays_jauge_augmentation_multiplicator, 3, 0, INT_MAX, },
	{ "adelays_jauge_to_decrease_multiplicator", &adelays_config.adelays_jauge_to_decrease_multiplicator, 2, 0, INT_MAX, },

	{ "adelays_max_hackcount_to_get_jauge", &adelays_config.adelays_max_hackcount_to_get_jauge, 1, 0, INT_MAX, },
	{ "adelays_acceptable_multiplicator_for_jauge", &adelays_config.adelays_acceptable_multiplicator_for_jauge, 2, 0, INT_MAX, },
	{ "adelays_delay_multiplicator_to_reduce_hackcount", &adelays_config.adelays_delay_multiplicator_to_reduce_hackcount, 3, 0, INT_MAX, },
	{ "adelays_max_infixed_to_get_jauge", &adelays_config.adelays_max_infixed_to_get_jauge, 1, 0, INT_MAX, },
	{ "adelays_allow_buffer_on_fixed", &adelays_config.adelays_allow_buffer_on_fixed, 0, 0, INT_MAX, },
	{ "adelays_max_aspd", &adelays_config.adelays_max_aspd, 190, 0, INT_MAX, },
	{ "adelays_jauge_added_after_cutanim", &adelays_config.adelays_jauge_added_after_cutanim, 20, 0, INT_MAX, },
	{ "adelays_min_jauge_multiplicator", &adelays_config.adelays_min_jauge_multiplicator, 4, 0, INT_MAX, },
	{ "adelays_allow_harddelays_on_doubles_skills", &adelays_config.adelays_allow_harddelays, 1, 0, INT_MAX, },
	{ "adelays_put_hard_limit_on_all_skills", &adelays_config.adelays_put_hard_limit_on_all_skills, 1, 0, INT_MAX, },
	{ "adelays_hard_chain_limit", &adelays_config.adelays_hard_chain_limit, 4, 0, INT_MAX, },
	{ "adelays_sit_delay_bodyreloc", &adelays_config.adelays_sit_delay_bodyreloc, 2000, 0, INT_MAX, },
	 

};


int adelays_set_value(const char* w1, const char* w2)
{
	int val = config_switch(w2);

	int i;
	ARR_FIND(0, ARRAYLENGTH(adelays_data), i, strcmpi(w1, adelays_data[i].str) == 0);
	if (i == ARRAYLENGTH(adelays_data))
		return 0; // not found

	if (val < adelays_data[i].min || val > adelays_data[i].max)
	{
		ShowWarning("Value for setting '%s': %s is invalid (min:%i max:%i)! Defaulting to %i...\n", w1, w2, adelays_data[i].min, adelays_data[i].max, adelays_data[i].defval);
		val = adelays_data[i].defval;
	}

	*adelays_data[i].val = val;
	return 1;
}

int fexists(const char *fname)
{
	FILE *file;
	file = fopen(fname, "r");
	if (file)
	{
		fclose(file);
		return 1;
	}
	return 0;
}

int adelays_init(){
	adelays_battle_config_read();

	adelays_set_defaults();

	adelays_read_db();

	return 0;
}

int adelays_battle_config_read()
{
	char line[1024], w1[1024], w2[1024];
	FILE* fp;

	adelays_set_defaults();

	fp = fopen("conf/battle/Adelays.conf", "r");
	if (fp == NULL)
		ShowError("File not found: %s\n", "conf/battle/Adelays.conf");
	else
	{
		while (fgets(line, sizeof(line), fp))
		{
			if (line[0] == '/' && line[1] == '/')
				continue;
			if (sscanf(line, "%1023[^:]:%1023s", w1, w2) != 2)
				continue;
			else
				if (adelays_set_value(w1, w2) == 0)
					ShowWarning("Unknown setting '%s' in file %s\n", w1, "conf/battle/Adelays.conf");
		}

		fclose(fp);
	}


	return 0;
}


void adelays_set_defaults()
{
	int i;
	for (i = 0; i < ARRAYLENGTH(adelays_data); i++)
		*adelays_data[i].val = adelays_data[i].defval;
}

float adelays_getVersion(){
	return 4.3f;
}

void adelays_set1HitLock(struct Adelays_State * adstate) {
	adstate->adelays_hitlock = max(1, adstate->adelays_hitlock);
} // Ok

void adelays_skillFailed(struct Adelays_State * adstate) {
	adstate->adelays_last_skill_failed = 1;
} // Ok

void adelays_setHitLock(struct Adelays_State * adstate) {
	adstate->adelays_hitlock += adelays_config.adelays_hitlock_incr;
	adstate->adelays_hitlock = min(adelays_config.adelays_max_hitlock_count, adstate->adelays_hitlock);
} //Ok

void adelays_setHitLockUntil(struct Adelays_State * adstate, int64 tick) {
	adstate->adelays_hitlock_until = tick + adelays_config.adelays_hitlock_time_tolerance;
} //Ok

 int adelays_canHardDelayOnThatSkill(int id)
{
	if (!adelays_config.adelays_allow_harddelays) return 0;
	if (!adelays_get_force_harddelay(id)) return 0;
	return 1;
}

 int adelays_get_harddelay150(int id)
{
	return adelays_skill_db[adelays_skill_get_index(id)].adelays_harddelay150;
}

 int adelays_get_harddelay190(int id)
{
	return adelays_skill_db[adelays_skill_get_index(id)].adelays_harddelay190;
}

 int adelays_get_force_harddelay(int id)
{
	return adelays_skill_db[adelays_skill_get_index(id)].adelays_force_harddelay;
}

 int adelays_getHardDelayChainCount(int id)
{
	return adelays_skill_db[adelays_skill_get_index(id)].adelays_harddelay_chain_max;
}

 int adelays_get_delay150(int id);

 int adelays_get_delay150(int id)
{
	return adelays_skill_db[adelays_skill_get_index(id)].adelays_delay150;
}

 int adelays_get_delay190(int id)
{
	return adelays_skill_db[adelays_skill_get_index(id)].adelays_delay190;
}

 int adelays_get_option(int id)
{
	return adelays_skill_db[adelays_skill_get_index(id)].adelays_option;
}

 int adelays_get_allowhitlock(int id)
{
	return adelays_skill_db[adelays_skill_get_index(id)].adelays_allowhitlock;
}

int adelays_read_db()
{

	ShowStatus("             _      _                   _  _   _  _   \n");
	ShowStatus("    /\\      | |    | |                 | || | | || |  \n");
	ShowStatus("   /  \\   __| | ___| | __ _ _   _ ___  | || |_| || |_ \n");
	ShowStatus("  / /\\ \\ / _` |/ _ \\ |/ _` | | | / __| |__   _|__   _|\n");
	ShowStatus(" / ____ \\ (_| |  __/ | (_| | |_| \\__ \\    | |_   | |  \n");
	ShowStatus("/_/    \\_\\__,_|\\___|_|\\__,_|\\__, |___/    |_(_)  |_|  \n");
	ShowStatus("                             __/ |                    \n");
	ShowStatus("                            |___/                     \n");
	ShowStatus("n");


	ShowStatus(" \n");
	ShowStatus("<Adelays> : http://adelays.net \n");
	ShowStatus("<Adelays> : .....Loading..... \n");
	ShowStatus("<Adelays> : State of Adelays : %d \n", adelays_config.adelays_enable);

	char line[1024], skillName[200];
	int delayAt150, delayAt190, option, doubles, count = 0, classId, per, sex, nameid, itemoption, allowhitlock, chainMax;
	memset(adelays_skill_db, 0, sizeof(struct _adelays_skill_db) * MAX_SKILL_DB);
	memset(adelays_jobDelays, 0, sizeof(struct adelays_jobDelay) * 100);
	memset(adelays_item_db, 0, sizeof(struct _adelays_item_db) * 100);


	// Read skills.

	FILE* file = fopen("db/adelays_skill_delays.txt", "r");
	count = 0;

	while (fgets(line, sizeof(line), file))
	{

		if (line[0] == '/' && line[1] == '/')
			continue;

		if (sscanf(line, "%23[^,],%d,%d,%d,%d", skillName, &delayAt150, &delayAt190, &option, &allowhitlock) < 5)
			continue;
		else
			if (adelays_setskillDelays(skillName, delayAt150, delayAt190, option, allowhitlock) == 0){
				ShowInfo("<Adelays> : Ignored line in file adelays_skill_delays.txt (%s). \n", skillName);
			}
			else{
				if (adelays_config.adelays_put_hard_limit_on_all_skills == 1){
					adelays_setSkillHardDelays(skillName, delayAt150 * adelays_config.adelays_hard_chain_limit, delayAt190 * adelays_config.adelays_hard_chain_limit, adelays_config.adelays_hard_chain_limit, 0);
				}
				count++;
			}
	}

	fclose(file);

	ShowStatus("<Adelays> : Adelays Skill Delays Configuration Ready For %d Skills.\n", count);


	// Read skills hard delays. 

	file = fopen("db/adelays_hard_delays.txt", "r");
	count = 0;
	while (fgets(line, sizeof(line), file))
	{

		if (line[0] == '/' && line[1] == '/')
			continue;

		if (sscanf(line, "%23[^,],%d,%d,%d,%d", skillName, &delayAt150, &delayAt190, &chainMax, &doubles) < 5)
			continue;
		else
			if (adelays_setSkillHardDelays(skillName, delayAt150, delayAt190, chainMax, doubles) == 0){
				ShowInfo("<Adelays> : Ignored line in file adelays_skills_hard_delays.txt (%s). \n", skillName);

			}
			else{
				count++;
			}
	}


	ShowStatus("<Adelays> : Adelays Hard Skill Delays Configuration Ready For %d Skills.\n", count);


	// Read items delays.

	file = fopen("db/adelays_item_delays.txt", "r");
	count = 0;

	while (fgets(line, sizeof(line), file))
	{

		if (line[0] == '/' && line[1] == '/')
			continue;


		if (sscanf(line, "%d,%d", &nameid, &itemoption) < 2)
			continue;
		else
			if (adelays_setItemDelays(count, nameid, itemoption) == 0) {
				ShowInfo("<Adelays> : Ignored line in file adelays_item_delays.txt (%d). \n", nameid);
			}
			else{
				count++;
			}
	}

	ShowStatus("<Adelays> : Adelays Items Delays Configuration Ready For %d Items.\n", count);


	file = fopen("db/adelays_job_delays.txt", "r");
	count = 0;

	while (fgets(line, sizeof(line), file))
	{

		if (line[0] == '/' && line[1] == '/')
			continue;

		if (sscanf(line, "%d,%d,%d,%d", &option, &classId, &sex, &per) < 4)
			continue;
		else
		{
			adelays_setJobDelay(count, option, classId, sex, per);
			count++;
		} 
	}


	ShowStatus("<Adelays> : Adelays Jobs Adjustements Configuration Read For %d Jobs. \n", count);

	ShowStatus("<Adelays> : Thank you for Using Adelays.\n");

	return 0;
}

int adelays_setskillDelays(char * skillName, int delayAt150, int delayAt190, int option, int allowhitlock){

	int id = adelays_skill_name2id(skillName);
	if (!id) return 0;

	adelays_skill_db[id].adelays_delay150 = delayAt150;
	adelays_skill_db[id].adelays_delay190 = delayAt190;
	adelays_skill_db[id].adelays_harddelay150 = delayAt150;
	adelays_skill_db[id].adelays_harddelay190 = delayAt190;
	adelays_skill_db[id].adelays_force_harddelay = 0;
	adelays_skill_db[id].adelays_harddelay_chain_max = 1;
	adelays_skill_db[id].adelays_option = option;
	adelays_skill_db[id].adelays_allowhitlock = allowhitlock;

	return 1;
}

int adelays_setSkillHardDelays(char * skillName, int delayAt150, int delayAt190, int chainMax, int doubles){

	int id = adelays_skill_name2id(skillName);

	if (!id) return 0;

	adelays_skill_db[id].adelays_harddelay150 = delayAt150;
	adelays_skill_db[id].adelays_harddelay190 = delayAt190;
	adelays_skill_db[id].adelays_harddelay_chain_max = chainMax;
	adelays_skill_db[id].adelays_force_harddelay = 1;
	adelays_skill_db[id].adelays_harddelay_is_double = doubles;

	return 1;
}

int adelays_setItemDelays(int count, int nameid, int option){
	if (count >= 100) return 0;
	adelays_item_db[count].nameid = nameid;
	adelays_item_db[count].option = option;

	//ShowInfo("Set Item Delays of %s To (%d, %d) \n", nameid,option);

	return 1;
}

int adelays_setJobDelay(int count, int option, int classId, int sex, int per){
	if (count >= 100) return 0;

	adelays_jobDelays[count].classId = classId;
	adelays_jobDelays[count].option = option;
	adelays_jobDelays[count].per = per / 100.0;
	adelays_jobDelays[count].sex = sex;

	//ShowInfo("Set Job Delays of %d-%d-%d To %d\% \n", classId, sex, option, per);

	return 1;
}

 int adelays_getFixDelay(unsigned short amotion, int skillid, int minskilldelay) {

	int acceptableAnimationDelay;
	int max_aspd, min_aspd, at150, at190;
	int option = 1;
	float a, b;
	max_aspd = 190;
	min_aspd = 150;

	// **** Calculate the acceptable Delay 
	option = adelays_get_option(skillid);
	if (option > 0) {
		at150 = adelays_get_delay150(skillid);
		at190 = adelays_get_delay190(skillid);

		// Formula calculation (a * x + b)
		a = (at190 - at150) / (max_aspd - min_aspd);
		b = -((a * max_aspd) - at190);

		// Correct delay difference
		acceptableAnimationDelay = (a * amotion) + b;
		acceptableAnimationDelay += adelays_config.adelays_tol;

	}
	else {
		// If it can not be calculated, just use the server delay value.
		acceptableAnimationDelay = minskilldelay;
	}

	return acceptableAnimationDelay + min(150, acceptableAnimationDelay / 5);

}

int adelays_canuseitem(int nameid, int64 cannact_tick) {
	if (adelays_config.adelays_scroll_fix < 1) return 1;

	int count = 0;
	while ((adelays_item_db[count].nameid != 0) && (count < 100)){
		if ((adelays_item_db[count].nameid == nameid) && (adelays_item_db[count].option == 1)){
			if (DIFF_TICK(adelays_gettick(), cannact_tick) < 0) {
				return 0;
			}
		}
		count++;
	}

	return 1;
}

void adelays_dataset(struct Adelays_State * adstate, char name[24]) {
	adstate->adelays_skill_tick = adelays_gettick();
	adstate->adelays_last_skill_id = 0;
	adstate->adelays_hackcount = 0;
	adstate->adelays_hitlock = 0;
	adstate->adelays_hitlock_until = adelays_gettick();
	adstate->adelays_infixed = 0;
	adstate->adelays_showinfo = 0;
	adstate->adelays_jauge = 0;
	adstate->adelays_showinfo = 0;
	adstate->adelays_allowedReduction = 0;
	adstate->adelays_expected_hard_delay = 0;
	adstate->adelays_missing_delay = 0;
	adstate->adelays_previous_client_delay = 0;
	adstate->adelays_last_client_delay = 0;
	adstate->adelays_hard_ignore = 0;
	strcpy(adstate->adelays_player_name, name);

}

 int adelays_calculateAcceptableDelay(int skillId, int amotion){
	int max_aspd, min_aspd, at150, at190;
	float a, b;
	int  acceptableAnimationDelay;

	max_aspd = 190;
	min_aspd = 150;

	at150 = adelays_get_delay150(skillId);
	at190 = adelays_get_delay190(skillId);

	// Formula calculation (a * x + b)
	a = (at190 - at150) / (max_aspd - min_aspd);
	b = -((a * max_aspd) - at190);

	// Correct delay difference
	acceptableAnimationDelay = (a * amotion) + b;
	acceptableAnimationDelay += adelays_config.adelays_tol;

	return  acceptableAnimationDelay;
}

 int adelays_get_harddelay(int skillId, int amotion, int skillamount, int minskilldelay){

	int maxSkillAmount = adelays_getHardDelayChainCount(skillId);
	if (skillamount == 0){
		skillamount = maxSkillAmount;
	}


	float rapport = skillamount / (float)maxSkillAmount;

	int max_aspd, min_aspd, at150, at190;
	float a, b;
	int  harddelay;

	max_aspd = 190;
	min_aspd = 150;

	at150 = adelays_get_harddelay150(skillId);
	at190 = adelays_get_harddelay190(skillId);

	a = (at190 - at150) / (max_aspd - min_aspd);
	b = -((a * max_aspd) - at190);

	// Correct delay difference
	harddelay = (a * amotion) + b;

	if (adelays_skillHasDoubles(skillId)){
		// For special skills, ignore the minskill delay.
		return  harddelay * rapport;
	}
	else{
		// For regular skills, add the minskilldelay.
		return  (harddelay * rapport) + minskilldelay * skillamount;
	}


}

 void adelays_calculateMissingDelay(int skillid, struct Adelays_State *adstate, int amotion, int minskilldelay)
{
	// Calculate the missing delay for the usage of a specified skill.
	// We know the count of usage of that skil, and the accumulated delay for that skill.

	if (adstate->adelays_chain_count == 0){
		adstate->adelays_missing_delay = 0;
		return;
	}

	if (!adelays_canHardDelayOnThatSkill(skillid)){
		adstate->adelays_missing_delay = 0;
		return;
	}

	adstate->adelays_missing_delay = adelays_get_harddelay(skillid, amotion, adstate->adelays_chain_count, minskilldelay) - adstate->adelays_accumulated_delay;

	if (adstate->adelays_missing_delay < 0){
		adstate->adelays_missing_delay = 0;
	}

}

void adelays_setExpectedFirstChainHardDelay(int skillid, struct Adelays_State *adstate, int amotion, int minskilldelay)
{
	int chainDoubles = adelays_getHardDelayChainCount(skillid) - 1;
	adstate->adelays_expected_hard_delay = adelays_get_harddelay(skillid, amotion, 0, minskilldelay) - ((minskilldelay + 50) * chainDoubles);
}

void adelays_setExpectedRegularHardDelay(int skillid, struct Adelays_State *adstate, int amotion, int minskilldelay)
{
	int chainCount = adelays_getHardDelayChainCount(skillid);
	adstate->adelays_expected_hard_delay = adelays_get_harddelay(skillid, amotion, 0, minskilldelay) / chainCount;
}

int adelays_apply_class_adjustement(int skillId, int acceptableAnimationDelay, short _class, unsigned char sex, unsigned int isRiding){

	int count = 0;
	int option = adelays_get_option(skillId);

	// **** Class Adjustements
	count = 0;
	while ((adelays_jobDelays[count].option != 0) && (count < 100)){
		if ((adelays_jobDelays[count].classId == _class) && (adelays_jobDelays[count].sex == sex) && (option == adelays_jobDelays[count].option)){
			acceptableAnimationDelay *= adelays_jobDelays[count].per;
			break;
		}
		count++;
	}

	// **** Peco adjustements (Only for paladins) ----


	switch (_class){
		// Paladin are faster on magical attack without peco.
	case JOB_PALADIN:case JOB_PALADIN2:
		if (!isRiding && (option == 2)){
			acceptableAnimationDelay *= 0.66;
		}
		break;

		// Knight and lord knights are faster on magical attacks with peco.
	case JOB_KNIGHT:case JOB_KNIGHT2:case JOB_LORD_KNIGHT: case JOB_LORD_KNIGHT2:
		if (isRiding && (option == 2)){
			acceptableAnimationDelay *= 0.70;
		}
		break;
	}

	return acceptableAnimationDelay;
}

int adelays_skillHasDoubles(int skillId){
	return 	adelays_skill_db[skillId].adelays_harddelay_is_double == 1;
}

int	adelays_is_sit_allowed(struct Adelays_State *adstate){

	if (adstate->adelays_last_skill_id == MO_BODYRELOCATION){

		if (DIFF_TICK(adelays_gettick(), adstate->adelays_skill_tick) < 2000){
			return 0;
		}
	}

	return 1;
}

int	adelays_process_record(int64 tick, int64 * cannact_tick, struct Adelays_State *adstate, int minskilldelay){
	int response = 0;
	int skillId = adstate->adelays_last_skill_id;
	int clientdelay = adstate->adelays_last_client_delay;

	// -------------------------
	// Check the fixed delays.
	// -------------------------

	if (adstate->adelays_infixed > 0) {
		// Reduce the fixed delays everyskills.
		adstate->adelays_infixed -= 1;

		// If there are still fixed delays, set the next skill delay.
		if (adstate->adelays_infixed > 0) {
			(*cannact_tick) = max((*cannact_tick), tick + adelays_getFixDelay(adstate->adelays_last_amotion, adstate->adelays_previous_skill_id, minskilldelay));
		}

	}
	else {

		// Place the player in fixed mode if he abuses.
		if ((adstate->adelays_hackcount > 0) && (adstate->adelays_jauge == 0)) {
			adstate->adelays_infixed = min(adelays_config.adelays_max_infixed, adstate->adelays_hackcount);

			// Set the next delay to fixed one.
			(*cannact_tick) = max((*cannact_tick), tick + adelays_getFixDelay(adstate->adelays_last_amotion, adstate->adelays_previous_skill_id, minskilldelay));
		}
	}


	// -------------------------
	// Check the hard delays.
	// -------------------------

	// Apply the hard delay.
	adstate->adelays_last_hard = 0;


	if (!adstate->adelays_hard_ignore) {

		if (adelays_canHardDelayOnThatSkill(skillId))
		{

			int fullHardDelay = adelays_get_harddelay(skillId, adstate->adelays_last_amotion, 0, minskilldelay);
			int missingDelay = 0;

			// Set the accumulated delay.


			if ((skillId != adstate->adelays_previous_skill_id) || (clientdelay > fullHardDelay)){
				// If the skill is different or that there was a pause bigger than the full hard delay:
				adstate->adelays_chain_count = 1;


				if (adelays_skillHasDoubles(skillId)){
					// Set the accumulated delay to a big value comprised in the hard delay. The chain still begins..
					//adstate->adelays_accumulated_delay =  (fullHardDelay / (adelays_getHardDelayChainCount(skillId)/2)) - minskilldelay;
					adelays_get_harddelay(skillId, adstate->adelays_last_amotion, 3, minskilldelay);
				}
				else{
					// Set the accumulated delay to the normal value for 1 skill..
					adstate->adelays_accumulated_delay = adelays_get_harddelay(skillId, adstate->adelays_last_amotion, 1, minskilldelay);

				}
				adstate->adelays_previous_client_delay = adstate->adelays_accumulated_delay;

			}
			else {

				// The chain of skill is continuing. Increase it and save the accumulated delay.

				// Check if the delay is correct (If we expect a specific delay).
				// Only for specifics skills like AV or SB.
				if (adstate->adelays_expected_hard_delay > 0){
					// Here, we expect a specific delay.
					if (adelays_skillHasDoubles(skillId) && clientdelay < adstate->adelays_expected_hard_delay * 0.50){
						// Here the delay is way too far from what we expected.
						// We cancel the skill and put an hard delay.
						adstate->adelays_chain_count--;
						strcpy(adstate->adelays_last_result, "CANCELLED");
						missingDelay = (adstate->adelays_expected_hard_delay - clientdelay) + (adstate->adelays_expected_hard_delay - (clientdelay * 2)) * 0.2;
						(*cannact_tick) = max((*cannact_tick) + missingDelay, tick + missingDelay);
						adstate->adelays_last_hard = missingDelay;
						response = 2;
					}
					else if (clientdelay < adstate->adelays_expected_hard_delay){
						adstate->adelays_chain_count--;
						strcpy(adstate->adelays_last_result, "CANCELLED");
						missingDelay = (adstate->adelays_expected_hard_delay - clientdelay) + (adstate->adelays_expected_hard_delay - clientdelay) * 0.2;
						(*cannact_tick) = max((*cannact_tick) + missingDelay, tick + missingDelay);
						adstate->adelays_last_hard = missingDelay;
						response = 2;
					}

				}

				adstate->adelays_chain_count++;
				adstate->adelays_accumulated_delay += clientdelay;
				adstate->adelays_previous_client_delay = clientdelay;
			}


			// Calculate the missing delay.
			adelays_calculateMissingDelay(skillId, adstate, adstate->adelays_last_amotion, minskilldelay);

			// Calculate the next expected hard delay.
			adstate->adelays_expected_hard_delay = 0;


			if (adelays_time_to_hard(adstate, skillId)){
				// If the chain count reached the maximum, we add the missing delay as an hard delay, and setup the expected next delay.
				missingDelay = adstate->adelays_missing_delay + adstate->adelays_missing_delay * 0.33;

				// This is only for display
				adstate->adelays_last_hard = missingDelay;
				// -----------

				// We increase the canact tick with the missing delay.
				(*cannact_tick) = max((*cannact_tick) + missingDelay, tick + missingDelay);

				// We reset the adelays values.
				adstate->adelays_chain_count = 0;
				adstate->adelays_accumulated_delay = 0;

				// Then we set the first chain delay.
				if (adelays_skillHasDoubles(skillId)){
					adelays_setExpectedFirstChainHardDelay(skillId, adstate, adstate->adelays_last_amotion, minskilldelay);
				}
				else{
					/*if (missingDelay > 0){
						adstate->adelays_expected_hard_delay = missingDelay + adelays_get_harddelay(skillId, adstate->adelays_last_amotion, 1, minskilldelay);
						}*/
				}
			}
		}

	}


	// -------------------------
	// File Log System.
	// -------------------------
	if (adstate->adelays_log == 1) {

		// Save infos in a log. file.
		FILE *fp;
		time_t rawtime;
		time(&rawtime);
		char *timestr = asctime(localtime(&rawtime));
		timestr[strlen(timestr) - 1] = 0;

		fp = fopen("conf/adelays_log", "a+");
		fprintf(fp, "[%s] %s : SD:%04d CD:%04d HTC:%d HKC:%02d BUF:%02d DEX:%d HD:%04dms HDC:%d [%s] %s \n",
			timestr,
			adstate->adelays_player_name,
			adstate->adelays_last_server_delay,
			adstate->adelays_last_client_delay,
			adstate->adelays_hitlock,
			adstate->adelays_hackcount,
			adstate->adelays_jauge,
			adstate->adelays_last_amotion,
			adstate->adelays_last_hard,
			adstate->adelays_chain_count,
			adelays_skill_get_name(adstate->adelays_last_skill_id),
			adstate->adelays_last_result);
		fclose(fp);

		// Also show info in console.
		ShowInfo("AD:%04d SD:%04d CD:%04d HTC:%d HKC:%02d BUF:%02d ASP:%d HD:%04dms HDC:%d [%s] %c%c \n",
			adstate->adelays_last_acceptable_delay,
			adstate->adelays_last_server_delay,
			adstate->adelays_last_client_delay,
			adstate->adelays_hitlock,
			adstate->adelays_hackcount,
			adstate->adelays_jauge,
			adstate->adelays_last_amotion,
			adstate->adelays_last_hard,
			adstate->adelays_chain_count,
			adelays_skill_get_name(adstate->adelays_last_skill_id),
			adstate->adelays_last_result[0],
			adstate->adelays_last_result[1]);
	}


	return response;

}

int	adelays_get_log_message(char message[256], struct Adelays_State *adstate){
	sprintf(message, "SD:%04d CD:%04d HT:%d HK:%02d BUF:%02d ASPD:%03d HD:%04dms HDC:%d [%s] %s",
		adstate->adelays_last_server_delay,
		adstate->adelays_last_client_delay,
		adstate->adelays_hitlock,
		adstate->adelays_hackcount,
		adstate->adelays_jauge,
		adstate->adelays_last_amotion,
		adstate->adelays_last_hard,
		adstate->adelays_chain_count,
		adelays_skill_get_name(adstate->adelays_last_skill_id),
		adstate->adelays_last_result);

	return 0;
}

int adelays_recordSkillDelay(int skillid, int delayfix, int cooldown, struct Adelays_State *adstate, int64 tick, unsigned short oriamotion, short _class, unsigned char sex, unsigned int isRiding, int minskilldelay) {
	int clientTotalDelay, serverTotaldelay, acceptableAnimationDelay, nextAcceptableAnimationDelay;
	int option = 1, oldskilloption = 1, amotion = 0, diff;
	int allowhitlock = 1;
	int t;
	int oldskillId;

	// Adelays is disabled. Return default data.
	if (adelays_config.adelays_enable != 1){
		return 1;
	}

	// Make sure the last skill ID is set to a normal value.
	if (adstate->adelays_last_skill_id == 0){
		adstate->adelays_last_skill_id = skillid;
	}

	// Keep a trace of the previous skill.
	oldskillId = adstate->adelays_last_skill_id;


	// Calculate Total Server Delay
	serverTotaldelay = max(delayfix, cooldown);

	// Calculate Total Client Delay
	clientTotalDelay = DIFF_TICK(tick, adstate->adelays_skill_tick);


	// Make sure delays can not be negative. 
	if ((clientTotalDelay < 0) || (serverTotaldelay < 0)){
		// Impossible case, but who knows.
		return 1;
	}

	// Get the normal ASPD of the player
	amotion = cap_value((2000 - oriamotion) / 10, 150, adelays_config.adelays_max_aspd);
	adstate->adelays_last_amotion = amotion;


	// Reset fixed and hack count in case of big pause
	if (clientTotalDelay >= adelays_config.adelays_reset_time){
		adstate->adelays_hackcount = 0;
		adstate->adelays_infixed = 0;
		adstate->adelays_hitlock = 0;
		adstate->adelays_jauge = 0;
	}

	// Also reset hitlock in case of big pause.
	if (clientTotalDelay >= 5000){
		if (adstate->adelays_hitlock > 1){
			adstate->adelays_hitlock = 1;
		}
	}



	// ---------- V 3.2 Detection System -------------

	//By default, assume the skill is correct.
	strcpy(adstate->adelays_last_result, "CORRECT");

	// If we are in fixed mode, it's fixed by default.
	if (adstate->adelays_infixed > 0) {
		strcpy(adstate->adelays_last_result, "FIXED");
	}

	// Calculate the acceptable Delay 
	oldskilloption = adelays_get_option(oldskillId);
	option = adelays_get_option(skillid);
	allowhitlock = adelays_get_allowhitlock(skillid);

	// If both skills have delays then calculate the delay between them.
	if ((option > 0) && (oldskilloption > 0)) {

		acceptableAnimationDelay = adelays_calculateAcceptableDelay(oldskillId, amotion);
		nextAcceptableAnimationDelay = adelays_calculateAcceptableDelay(skillid, amotion);

		// Reduct the jauge on skill change.
		// If it's a new skill but both skills have delays, keep a little jauge:
		if (oldskillId != skillid){
			// Make sure the jauge is not too big for the new skill..
			if (adstate->adelays_jauge > (nextAcceptableAnimationDelay*adelays_config.adelays_buffer_size))
				adstate->adelays_jauge = nextAcceptableAnimationDelay*adelays_config.adelays_buffer_size;

		}



	}
	else {
		// Here, the previous or current skill is a nodelay skill. (That only work if the current skill allow cutanim).

		// Reduct the jauge on skill change.
		// If it's a new skill but one doesn't have delay. Reduce it.
		if (oldskillId != skillid){

			adstate->adelays_jauge = adstate->adelays_jauge / adelays_config.adelays_jauge_kept_multiplicator_on_new_skill_without_delay;
		}
		// If the acceptable delay can not be calculated, just use the server delay value.
		acceptableAnimationDelay = 0;

		if ((oldskilloption == 0) && option > 0){
			// If only the previous skill is nodelay, then it has cut the anim of the current skill.
			strcpy(adstate->adelays_last_result, "CUTANIM");

			// In that case, we add a little jauge in case, because it can causes hacks sometimes.
			if (adstate->adelays_last_skill_id != skillid){
				adstate->adelays_jauge = adelays_config.adelays_jauge_added_after_cutanim;
			}

		}
		else {
			// This is a nodelay skill. Just write it for information.
			strcpy(adstate->adelays_last_result, "NODELAY");
		}

	}

	// Apply the class adjustements.
	acceptableAnimationDelay = adelays_apply_class_adjustement(skillid, acceptableAnimationDelay, _class, sex, isRiding);

	// Store the acceptable delay.
	adstate->adelays_last_acceptable_delay = acceptableAnimationDelay;
	adstate->adelays_last_client_delay = clientTotalDelay;
	adstate->adelays_last_server_delay = serverTotaldelay;

	// Prepare Tick for next round.
	adstate->adelays_previous_skill_id = oldskillId;
	adstate->adelays_last_skill_id = skillid;
	adstate->adelays_skill_tick = tick;

	// Do not ignore hard delays by default.
	adstate->adelays_hard_ignore = 0;
	diff = acceptableAnimationDelay - clientTotalDelay;

	// Check if the delay is correct or not
	if (
		(
		(option > 0) && (oldskilloption > 0)
		&&
		(serverTotaldelay < acceptableAnimationDelay)
		&&
		(clientTotalDelay < acceptableAnimationDelay)
		)

		)
	{

		// Here the delay is not correct.
		if (adstate->adelays_last_skill_failed > 0){
			strcpy(adstate->adelays_last_result, "FAIL");
			adstate->adelays_hard_ignore = 1;
		}

		// We try to augment it with the jauge.
		else if ((adstate->adelays_jauge > 0) && (adstate->adelays_jauge >= (diff / adelays_config.adelays_jauge_augmentation_multiplicator))) {
			// Reduction Worked.

			// Depending on the difference, we should remove more or less.
			if (diff < 10){
				diff *= 4;
			}
			else if (diff < acceptableAnimationDelay / 10){
				diff *= 6;
			}
			else if (diff < acceptableAnimationDelay / 8){
				diff *= 3;
			}
			else if (diff < acceptableAnimationDelay / 6){
				diff *= 2;
			}
			else if (diff < acceptableAnimationDelay / 4){
				// No change here.
			}
			else {
				diff *= 0.5;
			}

			adstate->adelays_jauge = max(0, adstate->adelays_jauge - (diff / adelays_config.adelays_jauge_to_decrease_multiplicator));

			//clientTotalDelay = acceptableAnimationDelay;
			strcpy(adstate->adelays_last_result, "BUFFER");

			// In case its a hitlock, just specify it. saying BUFFER is not really informative.
			if ((adstate->adelays_hitlock > 0) && allowhitlock){
				strcpy(adstate->adelays_last_result, "HITLK/DCING");
			}

		}
		else {
			// Augmentation Didn't Work.
			adstate->adelays_jauge = 0;

			// Let's check if there could be an hitlock (if the skill allow that).
			if (((adstate->adelays_hitlock > 0) || (DIFF_TICK(tick, adstate->adelays_hitlock_until) < 0)) && allowhitlock)
			{

				// Allow reduction of hackcount if hitlock is big.
				if ((adstate->adelays_hitlock >= 2) && (adstate->adelays_hackcount) >= 1){
					adstate->adelays_hackcount -= 1;
				}

				// **** Here there is an Hitlock.
				if (adstate->adelays_hitlock > 0){
					adstate->adelays_hitlock -= adelays_config.adelays_reduce_hitlock_on_hitlock;
					adstate->adelays_hitlock = max(0, adstate->adelays_hitlock);
				}

				adstate->adelays_hard_ignore = 1;
				strcpy(adstate->adelays_last_result, "HITLK/DCING");

				// That can be a skill chain.
				if ((DIFF_TICK(tick, adstate->adelays_hitlock_until) < 0)){
					strcpy(adstate->adelays_last_result, "CHAIN");
				}
			}
			else
			{
				// **** Here this is not Hitlock... It's an HACK.
				strcpy(adstate->adelays_last_result, "HACK");
				adstate->adelays_hackcount += adelays_config.adelays_incr;
				adstate->adelays_hackcount = min(adstate->adelays_hackcount, adelays_config.adelays_max_infixed);


			}

			// If there is was hitlock but that the skill didn't allow it, reduce the hitlock count by 1 too.
			if ((!allowhitlock) && (adstate->adelays_hitlock > 0)){
				adstate->adelays_hitlock -= adelays_config.adelays_reduce_hitlock_on_hitlock;
				adstate->adelays_hitlock = max(0, adstate->adelays_hitlock);
			}

		}




	}
	else

	{
		// Here the delay is correct.

		if ((option > 0) && (oldskilloption > 0))
		{
			// Reduce Hackcount if it is a verified skill.

			// Only reduce the hackcount it's some kind of a pause between two skills.
			if (adstate->adelays_infixed < 1){

				// Reduce the hackcount depending the time the skill took to be run.
				if (clientTotalDelay < (acceptableAnimationDelay * 3))
				{
					adstate->adelays_hackcount -= ceil(clientTotalDelay / max(10, max(serverTotaldelay, acceptableAnimationDelay)));

				}
				else {
					adstate->adelays_hackcount -= 1;
				}

				// **** Always reduce this value (normally = 0)
				adstate->adelays_hackcount -= adelays_config.adelays_reduce_hackcount_on_correct;
			}


			// ** If the hack count is small, put a "jauge".
			// ** We add a jauge because sometimes players cast a skill very fast multiple time.
			// ** It doesn't apply if they are in fixed delays mode.
			// ** This jauge should not b
			if ((adstate->adelays_hackcount <= adelays_config.adelays_max_hackcount_to_get_jauge) && (adstate->adelays_infixed < adelays_config.adelays_max_infixed_to_get_jauge)) {

				if (
					(clientTotalDelay < ((acceptableAnimationDelay*adelays_config.adelays_acceptable_multiplicator_for_jauge) + adelays_config.adelays_buffer_time))
					)
				{
					// Augment from 1 on every correct skill.
					t = clientTotalDelay - acceptableAnimationDelay;

					if (t > 0) {

						// Make sure the jauge given is at least a interresting value. Avoid hacks when big ping.

						if (t < (acceptableAnimationDelay / 10))
							adstate->adelays_jauge += max(acceptableAnimationDelay / adelays_config.adelays_min_jauge_multiplicator, t);
						else if (t < (acceptableAnimationDelay / 5))
							adstate->adelays_jauge += max(acceptableAnimationDelay / adelays_config.adelays_min_jauge_multiplicator, t*1.5);
						else if (t < (acceptableAnimationDelay / 2))
							adstate->adelays_jauge += max(acceptableAnimationDelay / adelays_config.adelays_min_jauge_multiplicator, t*1.8);
						else
							adstate->adelays_jauge += max(acceptableAnimationDelay / adelays_config.adelays_min_jauge_multiplicator, t * 2);

					}


				}
				else if (clientTotalDelay > ((acceptableAnimationDelay*adelays_config.adelays_acceptable_multiplicator_for_jauge) + adelays_config.adelays_buffer_time)) {
					// If there was a big pause, reset the jauge to a small value. (Avoid getting stuck in hack at second skill).
					adstate->adelays_jauge = acceptableAnimationDelay / adelays_config.adelays_jauge_kept_multiplicator;
				}

			}
			else if ((adstate->adelays_infixed >= adelays_config.adelays_max_infixed_to_get_jauge) && adelays_config.adelays_allow_buffer_on_fixed){
				// Allow a buffer to be given in fixed mode.
				t = max(0, clientTotalDelay - adelays_getFixDelay(amotion, oldskillId, minskilldelay));
				adstate->adelays_jauge += t;
			}


			// Make sure the jauge is not too big.
			if (adstate->adelays_jauge > (acceptableAnimationDelay*adelays_config.adelays_buffer_size))
				adstate->adelays_jauge = acceptableAnimationDelay*adelays_config.adelays_buffer_size;


		}
		else {

			// The skill is a NodelaySkill. Allow Hack count reduction only the first time it's used.

			// ***** Reduce HackCount Once if First Skill Without Delay
			if (adstate->adelays_allowedReduction == 1 && (adstate->adelays_infixed < 1)) {
				adstate->adelays_hackcount -= adelays_config.adelays_reduce_hackcount_on_hitlock;
				adstate->adelays_allowedReduction = 0;
			}

		}

		// Cap hackcount
		adstate->adelays_hackcount = max(0, adstate->adelays_hackcount);

		// Reduce hitlock not matter what.
		if (adstate->adelays_hitlock > 0)
		{
			adstate->adelays_hitlock -= 1;
		}
		// Cap hitlock.
		adstate->adelays_hitlock = max(0, adstate->adelays_hitlock);


	}


	// Allow a  new future reduction of the hack count on the next hitlock skill in case it's a verified skill.
	if (option > 0)
	{
		adstate->adelays_allowedReduction = 1;
	}

	adstate->adelays_last_skill_failed = 0;


	return 0;
}

int adelays_time_to_hard(struct Adelays_State *adstate, int skillId)
{
	if ((adstate->adelays_chain_count == adelays_getHardDelayChainCount(skillId))){
		return true;
	}
	return 0;
}

int adelays_set_damages(struct Adelays_State *srcadstate, struct Adelays_State * destadstate, int skillId, int64 tick, int sdelay, int damage, int div){

	if (damage > 0) {
		if (srcadstate)
		{
			// Some specific condition can make an hitlock on the caster.
			switch (skillId) {
			case AL_HEAL: // When Heal attacks, it has nodelay.
				adelays_set1HitLock(srcadstate);
			default:
				break;
			}
		}

		if (destadstate)
		{
			// If there is a damage target
			adelays_setHitLock(destadstate);
			if (div > 1) adelays_setHitLockUntil(destadstate, tick + sdelay + div * 200);
		}


	}

	return 0;

}

int adelays_is_enabled(){
	return (adelays_config.adelays_enable == 1);
}

 