// Copyright (c) Athena Dev Teams - Licensed under GNU GPL
// For more information, see LICENCE in the main folder

#include "../common/cbasetypes.h"
#include "../common/timer.h"
#include "../common/nullpo.h"
#include "../common/malloc.h"
#include "../common/showmsg.h"
#include "../common/strlib.h"
#include "../common/utils.h"
#include "../common/ers.h"
#include "../common/random.h"

#include "skill.h"
#include "atcommand.h"
#include "map.h"
#include "path.h"
#include "clif.h"
#include "pc.h"
#include "status.h"
#include "pet.h"
#include "homunculus.h"
#include "mercenary.h"
#include "mob.h"
#include "npc.h"
#include "battle.h"
#include "battleground.h"
#include "party.h"
#include "itemdb.h"
#include "script.h"
#include "intif.h"
#include "log.h"
#include "chrif.h"
#include "guild.h"
#include "date.h"
#include "unit.h"
#include "achievement.h"
#include "faction.h"
#include "adelays.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>


#define SKILLUNITTIMER_INTERVAL	100

// ranges reserved for mapping skill ids to skilldb offsets
//WARNING all this shouldn't be higher than 999!!!
#define HM_SKILLRANGEMIN 700
#define HM_SKILLRANGEMAX (HM_SKILLRANGEMIN + MAX_HOMUNSKILL)
#define MC_SKILLRANGEMIN (HM_SKILLRANGEMAX + 1)
#define MC_SKILLRANGEMAX (MC_SKILLRANGEMIN + MAX_MERCSKILL)
#define GD_SKILLRANGEMIN (MC_SKILLRANGEMAX + 1)
#define GD_SKILLRANGEMAX (GD_SKILLRANGEMIN + MAX_GUILDSKILL)

static struct eri *skill_unit_ers = NULL; //For handling skill_unit's [Skotlex]
static struct eri *skill_timer_ers = NULL; //For handling skill_timerskills [Skotlex]

DBMap* skillunit_db = NULL; // int id -> struct skill_unit*

DBMap* skilldb_name2id = NULL;
struct s_skill_db skill_db[MAX_SKILL_DB];
struct s_skill_produce_db skill_produce_db[MAX_SKILL_PRODUCE_DB];
struct s_skill_arrow_db skill_arrow_db[MAX_SKILL_ARROW_DB];
struct s_skill_abra_db skill_abra_db[MAX_SKILL_ABRA_DB];

struct s_skill_unit_layout skill_unit_layout[MAX_SKILL_UNIT_LAYOUT];
int firewall_unit_pos;
int icewall_unit_pos;

//Since only mob-casted splash skills can hit ice-walls
static inline int splash_target(struct block_list* bl)
{
	return ( bl->type == BL_MOB ) ? BL_SKILL|BL_CHAR : BL_CHAR;
}

/// Returns the id of the skill, or 0 if not found.
int skill_name2id(const char* name)
{
	if( name == NULL )
		return 0;

	return (int)(intptr_t)strdb_get(skilldb_name2id, name);
}

/// Maps skill ids to skill db offsets.
/// Returns the skill's array index, or 0 (Unknown Skill).
int skill_get_index( int id )
{
	// avoid ranges reserved for mapping guild/homun/mercenary skills
	if( (id >= GD_SKILLRANGEMIN && id <= GD_SKILLRANGEMAX)
	||  (id >= HM_SKILLRANGEMIN && id <= HM_SKILLRANGEMAX)
	||  (id >= MC_SKILLRANGEMIN && id <= MC_SKILLRANGEMAX) )
		return 0;

	// map skill id to skill db index
	if( id >= GD_SKILLBASE )
		id = GD_SKILLRANGEMIN + id - GD_SKILLBASE;
	else
	if( id >= MC_SKILLBASE )
		id = MC_SKILLRANGEMIN + id - MC_SKILLBASE;
	else
	if( id >= HM_SKILLBASE )
		id = HM_SKILLRANGEMIN + id - HM_SKILLBASE;
	else
		; // identity

	// validate result
	if( id <= 0 || id >= MAX_SKILL_DB )
		return 0;

	return id;
}

const char* skill_get_name( int id )
{
	return skill_db[skill_get_index(id)].name;
}

const char* skill_get_desc( int id )
{
	return skill_db[skill_get_index(id)].desc;
}

// out of bounds error checking [celest]
static void skill_chk(int* id, int  lv)
{
	*id = skill_get_index(*id); // checks/adjusts id
	if( lv <= 0 || lv > MAX_SKILL_LEVEL ) *id = 0;
}

#define skill_get(var,id,lv) { skill_chk(&id,lv); if(!id) return 0; return var; }

// Skill DB
int	skill_get_hit( int id )               { skill_get (skill_db[id].hit, id, 1); }
int	skill_get_inf( int id )               { skill_get (skill_db[id].inf, id, 1); }
int	skill_get_ele( int id , int lv )      { skill_get (skill_db[id].element[lv-1], id, lv); }
int	skill_get_nk( int id )                { skill_get (skill_db[id].nk, id, 1); }
int	skill_get_max( int id )               { skill_get (skill_db[id].max, id, 1); }
int	skill_get_range( int id , int lv )    { skill_get (skill_db[id].range[lv-1], id, lv); }
int	skill_get_splash( int id , int lv )   { skill_chk (&id, lv); return (skill_db[id].splash[lv-1]>=0?skill_db[id].splash[lv-1]:AREA_SIZE); }
int	skill_get_hp( int id ,int lv )        { skill_get (skill_db[id].hp[lv-1], id, lv); }
int	skill_get_sp( int id ,int lv )        { skill_get (skill_db[id].sp[lv-1], id, lv); }
int	skill_get_hp_rate(int id, int lv )    { skill_get (skill_db[id].hp_rate[lv-1], id, lv); }
int	skill_get_sp_rate(int id, int lv )    { skill_get (skill_db[id].sp_rate[lv-1], id, lv); }
int	skill_get_state(int id)               { skill_get (skill_db[id].state, id, 1); }
int	skill_get_spiritball(int id, int lv)  { skill_get (skill_db[id].spiritball[lv-1], id, lv); }
int	skill_get_itemid(int id, int idx)     { skill_get (skill_db[id].itemid[idx], id, 1); }
int	skill_get_itemqty(int id, int idx)    { skill_get (skill_db[id].amount[idx], id, 1); }
int	skill_get_zeny( int id ,int lv )      { skill_get (skill_db[id].zeny[lv-1], id, lv); }
int	skill_get_num( int id ,int lv )       { skill_get (skill_db[id].num[lv-1], id, lv); }
int	skill_get_cast( int id ,int lv )      { skill_get (skill_db[id].cast[lv-1], id, lv); }
int	skill_get_delay( int id ,int lv )     { skill_get (skill_db[id].delay[lv-1], id, lv); }
int	skill_get_cooldown( int id ,int lv )  { skill_get (skill_db[id].cooldown[lv-1], id, lv); }
int	skill_get_walkdelay( int id ,int lv ) { skill_get (skill_db[id].walkdelay[lv-1], id, lv); }
int	skill_get_time( int id ,int lv )      { skill_get (skill_db[id].upkeep_time[lv-1], id, lv); }
int	skill_get_time2( int id ,int lv )     { skill_get (skill_db[id].upkeep_time2[lv-1], id, lv); }
int	skill_get_castdef( int id )           { skill_get (skill_db[id].cast_def_rate, id, 1); }
int	skill_get_weapontype( int id )        { skill_get (skill_db[id].weapon, id, 1); }
int	skill_get_ammotype( int id )          { skill_get (skill_db[id].ammo, id, 1); }
int	skill_get_ammo_qty( int id, int lv )  { skill_get (skill_db[id].ammo_qty[lv-1], id, lv); }
int	skill_get_inf2( int id )              { skill_get (skill_db[id].inf2, id, 1); }
int	skill_get_castcancel( int id )        { skill_get (skill_db[id].castcancel, id, 1); }
int	skill_get_maxcount( int id ,int lv )  { skill_get (skill_db[id].maxcount[lv-1], id, lv); }
int	skill_get_blewcount( int id ,int lv ) { skill_get (skill_db[id].blewcount[lv-1], id, lv); }
int	skill_get_mhp( int id ,int lv )       { skill_get (skill_db[id].mhp[lv-1], id, lv); }
int	skill_get_castnodex( int id ,int lv ) { skill_get (skill_db[id].castnodex[lv-1], id, lv); }
int	skill_get_delaynodex( int id ,int lv ){ skill_get (skill_db[id].delaynodex[lv-1], id, lv); }
int	skill_get_nocast ( int id )           { skill_get (skill_db[id].nocast, id, 1); }
int	skill_get_blocked ( int id )          { skill_get (skill_db[id].blocked, id, 1); }
int	skill_get_type( int id )              { skill_get (skill_db[id].skill_type, id, 1); }
int	skill_get_unit_id ( int id, int flag ){ skill_get (skill_db[id].unit_id[flag], id, 1); }
int	skill_get_unit_interval( int id )     { skill_get (skill_db[id].unit_interval, id, 1); }
int	skill_get_unit_range( int id, int lv ){ skill_get (skill_db[id].unit_range[lv-1], id, lv); }
int	skill_get_unit_target( int id )       { skill_get (skill_db[id].unit_target&BCT_ALL, id, 1); }
int	skill_get_unit_bl_target( int id )    { skill_get (skill_db[id].unit_target&BL_ALL, id, 1); }
int	skill_get_unit_flag( int id )         { skill_get (skill_db[id].unit_flag, id, 1); }
int	skill_get_unit_layout_type( int id ,int lv ){ skill_get (skill_db[id].unit_layout_type[lv-1], id, lv); }

int skill_tree_get_max(int id, int b_class)
{
	int i;
	b_class = pc_class2idx(b_class);

	ARR_FIND( 0, MAX_SKILL_TREE, i, skill_tree[b_class][i].id == 0 || skill_tree[b_class][i].id == id );
	if( i < MAX_SKILL_TREE && skill_tree[b_class][i].id == id )
		return skill_tree[b_class][i].max;
	else
		return skill_get_max(id);
}

int skill_frostjoke_scream(struct block_list *bl,va_list ap);
int skill_attack_area(struct block_list *bl,va_list ap);
struct skill_unit_group *skill_locate_element_field(struct block_list *bl); // [Skotlex]
int skill_graffitiremover(struct block_list *bl, va_list ap); // [Valaris]
int skill_greed(struct block_list *bl, va_list ap);
static int skill_cell_overlap(struct block_list *bl, va_list ap);
static int skill_trap_splash(struct block_list *bl, va_list ap);
struct skill_unit_group_tickset *skill_unitgrouptickset_search(struct block_list *bl,struct skill_unit_group *sg,int tick);
static int skill_unit_onplace(struct skill_unit *src,struct block_list *bl,unsigned int tick);
static int skill_unit_onleft(int skill_id, struct block_list *bl,unsigned int tick);
static int skill_unit_effect(struct block_list *bl,va_list ap);
int skill_blockpc_get(struct map_session_data *sd, int skillid);

/*==========================================
 * Party/Team Checks for Skills
 *------------------------------------------*/
bool skill_check_party(struct map_session_data *sd)
{
	if( sd )
	{
		if( !map[sd->bl.m].flag.battleground && sd->status.party_id )
			return true;
		else if( map[sd->bl.m].flag.battleground && sd->bg_id )
			return true;
	}
	return false;
}

bool skill_check_sameparty(struct map_session_data *sd1, struct map_session_data *sd2)
{
	if( sd1 && sd2 )
	{
		if( !map[sd1->bl.m].flag.battleground && sd1->status.party_id && sd1->status.party_id == sd2->status.party_id )
			return true;
		else if( map[sd1->bl.m].flag.battleground && sd1->bg_id && sd1->bg_id == sd2->bg_id )
			return true;
	}
	return false;
}


int enchant_eff[5] = { 10, 14, 17, 19, 20 };
int deluge_eff[5] = { 5, 9, 12, 14, 15 };

int skill_get_casttype (int id)
{
	int inf = skill_get_inf(id);
	if (inf&(INF_GROUND_SKILL))
		return CAST_GROUND;
	if (inf&INF_SUPPORT_SKILL)
		return CAST_NODAMAGE;
	if (inf&INF_SELF_SKILL) {
		if(skill_get_inf2(id)&INF2_NO_TARGET_SELF)
			return CAST_DAMAGE; //Combo skill.
		return CAST_NODAMAGE;
	}
	if (skill_get_nk(id)&NK_NO_DAMAGE)
		return CAST_NODAMAGE;
	return CAST_DAMAGE;
};

//Returns actual skill range taking into account attack range and AC_OWL [Skotlex]
int skill_get_range2 (struct block_list *bl, int id, int lv)
{
	int range;
	if( bl->type == BL_MOB && battle_config.mob_ai&0x400 )
		return 9; //Mobs have a range of 9 regardless of skill used.

	range = skill_get_range(id, lv);

	if( range < 0 )
	{
		if( battle_config.use_weapon_skill_range&bl->type )
			return status_get_range(bl);
		range *=-1;
	}

	//TODO: Find a way better than hardcoding the list of skills affected by AC_VULTURE
	switch( id )
	{
	case AC_SHOWER:			case MA_SHOWER:
	case AC_DOUBLE:			case MA_DOUBLE:
	case HT_BLITZBEAT:
	case AC_CHARGEARROW:
	case MA_CHARGEARROW:
	case SN_FALCONASSAULT:
	case HT_POWER:
		if( bl->type == BL_PC )
			range += pc_checkskill((TBL_PC*)bl, AC_VULTURE);
		else
			range += 10; //Assume level 10?
		break;
	// added to allow GS skills to be effected by the range of Snake Eyes [Reddozen]
	case GS_RAPIDSHOWER:
	case GS_PIERCINGSHOT:
	case GS_FULLBUSTER:
	case GS_SPREADATTACK:
	case GS_GROUNDDRIFT:
		if (bl->type == BL_PC)
			range += pc_checkskill((TBL_PC*)bl, GS_SNAKEEYE);
		else
			range += 10; //Assume level 10?
		break;
	case NJ_KIRIKAGE:
		if (bl->type == BL_PC)
			range = skill_get_range(NJ_SHADOWJUMP,pc_checkskill((TBL_PC*)bl,NJ_SHADOWJUMP));
		break;
	}

	if( !range && bl->type != BL_PC )
		return 9; // Enable non players to use self skills on others. [Skotlex]
	return range;
}

int skill_calc_heal(struct block_list *src, struct block_list *target, int skill_id, int skill_lv, bool heal)
{
	int skill, hp;
	struct map_session_data *sd = BL_CAST(BL_PC, src);
	struct map_session_data *tsd = BL_CAST(BL_PC, target);
	struct status_change* sc;

	switch( skill_id )
	{
	case BA_APPLEIDUN:
		hp = 30+5*skill_lv+5*(status_get_vit(src)/10); // HP recovery
		if( sd )
			hp += 5*pc_checkskill(sd,BA_MUSICALLESSON);
		break;
	case PR_SANCTUARY:
		hp = (skill_lv>6)?777:skill_lv*100;
		break;
	case NPC_EVILLAND:
		hp = (skill_lv>6)?666:skill_lv*100;
		break;
	default:
		if (skill_lv >= battle_config.max_heal_lv)
			return battle_config.max_heal;

		hp = ( status_get_lv(src)+status_get_int(src) )/8 *(4+ skill_lv*8);
		if( sd && ((skill = pc_checkskill(sd, HP_MEDITATIO)) > 0) )
			hp += hp * skill * 2 / 100;
		else if( src->type == BL_HOM && (skill = merc_hom_checkskill(((TBL_HOM*)src), HLIF_BRAIN)) > 0 )
			hp += hp * skill * 2 / 100;
		break;
	}

	if( ( (target && target->type == BL_MER) || !heal ) && skill_id != NPC_EVILLAND )
		hp >>= 1;

	if( sd && (skill = pc_skillheal_bonus(sd, skill_id)) )
		hp += hp*skill/100;
	
	if( tsd && (skill = pc_skillheal2_bonus(tsd, skill_id)) )
		hp += hp*skill/100;

	sc = status_get_sc(target);
	if( sc && sc->count )
	{
		if( sc->data[SC_CRITICALWOUND] && heal ) // Critical Wound has no effect on offensive heal. [Inkfish]
			hp -= hp * sc->data[SC_CRITICALWOUND]->val2/100;
		if( sc->data[SC_INCHEALRATE] && skill_id != NPC_EVILLAND && skill_id != BA_APPLEIDUN )
			hp += hp * sc->data[SC_INCHEALRATE]->val1/100; // Only affects Heal, Sanctuary and PotionPitcher.(like bHealPower) [Inkfish]
	}

	return hp;
}

// Making plagiarize check its own function [Aru]
int can_copy (struct map_session_data *sd, int skillid, struct block_list* bl)
{
	// Never copy NPC/Wedding Skills
	if (skill_get_inf2(skillid)&(INF2_NPC_SKILL|INF2_WEDDING_SKILL))
		return 0;

	// High-class skills
	if((skillid >= LK_AURABLADE && skillid <= ASC_CDP) || (skillid >= ST_PRESERVE && skillid <= CR_CULTIVATION))
	{
		if(battle_config.copyskill_restrict == 2)
			return 0;
		else if(battle_config.copyskill_restrict)
			return (sd->status.class_ == JOB_STALKER);
	}

	//Added so plagarize can't copy agi/bless if you're undead since it damages you
	if ((skillid == AL_INCAGI || skillid == AL_BLESSING || 
		skillid == CASH_BLESSING || skillid == CASH_INCAGI || 
		skillid == MER_INCAGI || skillid == MER_BLESSING))
		return 0;

	return 1;
}

// [MouseJstr] - skill ok to cast? and when?
int skillnotok (int skillid, struct map_session_data *sd)
{
	int i,m;
	nullpo_retr (1, sd);
	m = sd->bl.m;
	i = skill_get_index(skillid);

	if (i == 0)
		return 1; // invalid skill id

	if (battle_config.gm_skilluncond && pc_isGM(sd) >= battle_config.gm_skilluncond)
		return 0; // GMs can do any damn thing they want

	if( skillid == AL_TELEPORT && sd->skillitem == skillid && sd->skillitemlv > 2 )
		return 0; // Teleport lv 3 bypasses this check.[Inkfish]

	if( skill_blockpc_get(sd,skillid) != -1 )
		return 1;

	// Check skill restrictions [Celest]
	if( (skill_get_blocked(skillid) && !pc_isGM(sd)) || sd->state.only_walk )
		return 1;
	if(!map_flag_vs(m) && skill_get_nocast (skillid) & 1)
		return 1;
	if(map[m].flag.pvp && skill_get_nocast (skillid) & 2)
		return 1;
	if(map_flag_gvg(m) && skill_get_nocast (skillid) & 4)
		return 1;
	if(map[m].flag.battleground && skill_get_nocast (skillid) & 8)
		return 1;
	if(map[m].flag.restricted && map[m].zone && skill_get_nocast (skillid) & (8*map[m].zone))
		return 1;

	switch (skillid) {
		// Ancient WoE - Platinum Skills
		case KN_CHARGEATK:
		case CR_SHRINK:
		case AS_VENOMKNIFE:
		case RG_CLOSECONFINE:
		case WZ_SIGHTBLASTER:
		case SA_CREATECON:
		case SA_ELEMENTWATER:
		case HT_PHANTASMIC:
		case BA_PANGVOICE:
		case DC_WINKCHARM:
		case BS_GREED:
		case PR_REDEMPTIO:
		case MO_KITRANSLATION:
		case MO_BALKYOUNG:
		case SA_ELEMENTGROUND:
		case SA_ELEMENTFIRE:
		case SA_ELEMENTWIND:
		// Ancient WoE - Marriage Skills
		case WE_MALE:
		case WE_FEMALE:
		case WE_BABY:
			if( map[m].flag.ancient )
				return 1;
			break;
		case AL_WARP:
			if(map[m].flag.nowarp) {
				clif_skill_teleportmessage(sd,0);
				return 1;
			}
			return 0;
		case AL_TELEPORT:
			if(map[m].flag.noteleport || !guild_canescape(sd)) {
				clif_skill_teleportmessage(sd,0);
				return 1;
			}
			return 0; // gonna be checked in 'skill_castend_nodamage_id'
		case WE_CALLPARTNER:
		case WE_CALLPARENT:
		case WE_CALLBABY:
			if (map[m].flag.nomemo) {
				clif_skill_teleportmessage(sd,1);
				return 1;
			}
			if( map[m].flag.ancient )
				return 1;
			break;	
		case MC_VENDING:
			if( sd->state.vending )
			{
				clif_displaymessage(sd->fd, "Please close your current Shop first.");
				return 1;
			}
		case ALL_BUYING_STORE:
			if( battle_config.super_woe_enable )
			{
				clif_displaymessage (sd->fd, "Vending not available on Super WoE / GvG Events.");
				return 1;
			}
			if( map[sd->bl.m].flag.novending )
			{
				clif_displaymessage (sd->fd, "You can't open shop on this map");
				return 1;
			}
			if( map[sd->bl.m].flag.vending_cell != map_getcell(sd->bl.m,sd->bl.x,sd->bl.y,CELL_CHKNOVENDING) )
			{
				clif_displaymessage(sd->fd, "You can't open shop on this cell");
				return 1;
			}
			if( sd->state.secure_items )
			{
				clif_displaymessage(sd->fd, "You can't open shop. Blocked with @security");
				return 1;
			}
			break;
		case MC_IDENTIFY:
			return 0; // always allowed
		case WZ_ICEWALL:
			// noicewall flag [Valaris]
			if (map[m].flag.noicewall) {
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				return 1;
			}
			break;
		case GD_EMERGENCYCALL:
			if( map[m].flag.noemergencycall )
			{
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				return 1;
			}
			if( !map[m].flag.battleground && (!(battle_config.emergency_call&((agit_flag || agit2_flag)?2:1)) || !(battle_config.emergency_call&(map[m].flag.gvg || map[m].flag.gvg_castle?8:4)) || (battle_config.emergency_call&16 && map[m].flag.nowarpto && !map[m].flag.gvg_castle)) )
			{
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				return 1;
			}
		case GD_BATTLEORDER:
		case GD_REGENERATION:
		case GD_RESTORE:
			if( map[m].flag.ancient )
				return 1;
			break;
	}
	return (map[m].flag.noskill);
}

int skillnotok_hom(int skillid, struct homun_data *hd)
{
	int i = skill_get_index(skillid);
	nullpo_retr(1,hd);

	if (i == 0)
		return 1; // invalid skill id

	if (hd->blockskill[i] > 0)
		return 1;

	//Use master's criteria.
	return skillnotok(skillid, hd->master);
}

int skillnotok_mercenary(int skillid, struct mercenary_data *md)
{
	int i = skill_get_index(skillid);
	nullpo_retr(1,md);

	if( i == 0 )
		return 1; // Invalid Skill ID
	if( md->blockskill[i] > 0 )
		return 1;

	return skillnotok(skillid, md->master);
}

struct s_skill_unit_layout* skill_get_unit_layout (int skillid, int skilllv, struct block_list* src, int x, int y)
{
	int pos = skill_get_unit_layout_type(skillid,skilllv);
	int dir;

	if (pos < -1 || pos >= MAX_SKILL_UNIT_LAYOUT) {
		ShowError("skill_get_unit_layout: unsupported layout type %d for skill %d (level %d)\n", pos, skillid, skilllv);
		pos = cap_value(pos, 0, MAX_SQUARE_LAYOUT); // cap to nearest square layout
	}

	if (pos != -1) // simple single-definition layout
		return &skill_unit_layout[pos];

	dir = (src->x == x && src->y == y) ? 6 : map_calc_dir(src,x,y); // 6 - default aegis direction

	if (skillid == MG_FIREWALL)
		return &skill_unit_layout [firewall_unit_pos + dir];
	else if (skillid == WZ_ICEWALL)
		return &skill_unit_layout [icewall_unit_pos + dir];

	ShowError("skill_get_unit_layout: unknown unit layout for skill %d (level %d)\n", skillid, skilllv);
	return &skill_unit_layout[0]; // default 1x1 layout
}

/*==========================================
 *
 *------------------------------------------*/
int skill_additional_effect (struct block_list* src, struct block_list *bl, int skillid, int skilllv, int attack_type, int dmg_lv, unsigned int tick)
{
	struct map_session_data *sd, *dstsd;
	struct mob_data *md, *dstmd;
	struct status_data *sstatus, *tstatus;
	struct status_change *sc, *tsc;

	enum sc_type status;
	int skill;
	int rate;

	nullpo_ret(src);
	nullpo_ret(bl);

	if(skillid < 0) return 0;
	if(skillid > 0 && skilllv <= 0) return 0;	// don't forget auto attacks! - celest

	if( dmg_lv < ATK_BLOCK ) // Don't apply effect if miss.
		return 0;

	sd = BL_CAST(BL_PC, src);
	md = BL_CAST(BL_MOB, src);
	dstsd = BL_CAST(BL_PC, bl);
	dstmd = BL_CAST(BL_MOB, bl);

	sc = status_get_sc(src);
	tsc = status_get_sc(bl);
	sstatus = status_get_status_data(src);
	tstatus = status_get_status_data(bl);
	if (!tsc) //skill additional effect is about adding effects to the target...
		//So if the target can't be inflicted with statuses, this is pointless.
		return 0;

	if( sd )
	{ // These statuses would be applied anyway even if the damage was blocked by some skills. [Inkfish]
		if( skillid != WS_CARTTERMINATION && skillid != AM_DEMONSTRATION && skillid != CR_REFLECTSHIELD && skillid != MS_REFLECTSHIELD && skillid != ASC_BREAKER )
		{ // Trigger status effects
			enum sc_type type;
			int i;
			for( i = 0; i < ARRAYLENGTH(sd->addeff) && sd->addeff[i].flag; i++ )
			{
				rate = sd->addeff[i].rate;
				if( attack_type&BF_LONG ) // Any ranged physical attack takes status arrows into account (Grimtooth...) [DracoRPG]
					rate += sd->addeff[i].arrow_rate;
				if( !rate ) continue;

				if( (sd->addeff[i].flag&(ATF_WEAPON|ATF_MAGIC|ATF_MISC)) != (ATF_WEAPON|ATF_MAGIC|ATF_MISC) ) 
				{ // Trigger has attack type consideration. 
					if( (sd->addeff[i].flag&ATF_WEAPON && attack_type&BF_WEAPON) ||
						(sd->addeff[i].flag&ATF_MAGIC && attack_type&BF_MAGIC) ||
						(sd->addeff[i].flag&ATF_MISC && attack_type&BF_MISC) ) ;
					else
						continue; 
				}

				if( (sd->addeff[i].flag&(ATF_LONG|ATF_SHORT)) != (ATF_LONG|ATF_SHORT) )
				{ // Trigger has range consideration.
					if((sd->addeff[i].flag&ATF_LONG && !(attack_type&BF_LONG)) ||
						(sd->addeff[i].flag&ATF_SHORT && !(attack_type&BF_SHORT)))
						continue; //Range Failed.
				}

				type =  sd->addeff[i].id;
				skill = skill_get_time2(status_sc2skill(type),7);

				if (sd->addeff[i].flag&ATF_TARGET)
					status_change_start(bl,type,rate,7,0,0,0,skill,0);

				if (sd->addeff[i].flag&ATF_SELF)
					status_change_start(src,type,rate,7,0,0,0,skill,0);
			}
		}

		if( skillid )
		{ // Trigger status effects on skills
			enum sc_type type;
			int i;
			for( i = 0; i < ARRAYLENGTH(sd->addeff3) && sd->addeff3[i].skill; i++ )
			{
				if( skillid != sd->addeff3[i].skill || !sd->addeff3[i].rate )
					continue;
				type = sd->addeff3[i].id;
				skill = skill_get_time2(status_sc2skill(type),7);

				if( sd->addeff3[i].target&ATF_TARGET )
					status_change_start(bl,type,sd->addeff3[i].rate,7,0,0,0,skill,0);
				if( sd->addeff3[i].target&ATF_SELF )
					status_change_start(src,type,sd->addeff3[i].rate,7,0,0,0,skill,0);
			}
		}
	}

	if( dmg_lv < ATK_DEF ) // no damage, return;
		return 0;

	switch(skillid)
	{
	case 0: // Normal attacks (no skill used)
	{
		if( attack_type&BF_SKILL )
			break; // If a normal attack is a skill, it's splash damage. [Inkfish]
		if(sd) {
			// Automatic trigger of Blitz Beat
			if (pc_isfalcon(sd) && sd->status.weapon == W_BOW && (skill=pc_checkskill(sd,HT_BLITZBEAT))>0 &&
				rand()%1000 <= sstatus->luk*10/3+1 ) {
				rate=(sd->status.job_level+9)/10;
				skill_castend_damage_id(src,bl,HT_BLITZBEAT,(skill<rate)?skill:rate,tick,SD_LEVEL);
			}
			// Gank
			if(dstmd && sd->status.weapon != W_BOW &&
				(skill=pc_checkskill(sd,RG_SNATCHER)) > 0 &&
				(skill*15 + 55) + pc_checkskill(sd,TF_STEAL)*10 > rand()%1000) {
				if(pc_steal_item(sd,bl,pc_checkskill(sd,TF_STEAL)))
					clif_skill_nodamage(src,bl,TF_STEAL,skill,1);
				else
					clif_skill_fail(sd,RG_SNATCHER,USESKILL_FAIL_LEVEL,0);
			}
			// Chance to trigger Taekwon kicks [Dralnu]
			if(sc && !sc->data[SC_COMBO]) {
				if(sc->data[SC_READYSTORM] &&
					sc_start(src,SC_COMBO, 15, TK_STORMKICK,
						(2000 - 4*sstatus->agi - 2*sstatus->dex)))
					; //Stance triggered
				else if(sc->data[SC_READYDOWN] &&
					sc_start(src,SC_COMBO, 15, TK_DOWNKICK,
						(2000 - 4*sstatus->agi - 2*sstatus->dex)))
					; //Stance triggered
				else if(sc->data[SC_READYTURN] &&
					sc_start(src,SC_COMBO, 15, TK_TURNKICK,
						(2000 - 4*sstatus->agi - 2*sstatus->dex)))
					; //Stance triggered
				else if(sc->data[SC_READYCOUNTER])
				{	//additional chance from SG_FRIEND [Komurka]
					rate = 20;
					if (sc->data[SC_SKILLRATE_UP] && sc->data[SC_SKILLRATE_UP]->val1 == TK_COUNTER) {
						rate += rate*sc->data[SC_SKILLRATE_UP]->val2/100;
						status_change_end(src, SC_SKILLRATE_UP, INVALID_TIMER);
					}
					sc_start4(src,SC_COMBO, rate, TK_COUNTER, bl->id,0,0,
						(2000 - 4*sstatus->agi - 2*sstatus->dex));
				}
			}
		}

		if (sc) {
			struct status_change_entry *sce;
			// Enchant Poison gives a chance to poison attacked enemies
			if((sce=sc->data[SC_ENCPOISON])) //Don't use sc_start since chance comes in 1/10000 rate.
				status_change_start(bl,SC_POISON,sce->val2, sce->val1,0,0,0,
					skill_get_time2(AS_ENCHANTPOISON,sce->val1),0);
			// Enchant Deadly Poison gives a chance to deadly poison attacked enemies
			if((sce=sc->data[SC_EDP]))
				sc_start4(bl,SC_DPOISON,sce->val2, sce->val1,0,0,0,
					skill_get_time2(ASC_EDP,sce->val1));
		}
	}
	break;

	case SM_BASH:
		if( sd && skilllv > 5 && pc_checkskill(sd,SM_FATALBLOW)>0 ){
			//TODO: How much % per base level it actually is?
			sc_start(bl,SC_STUN,(5*(skilllv-5)+(int)sd->status.base_level/10),
				skilllv,skill_get_time2(SM_FATALBLOW,skilllv));
		}
		break;

	case MER_CRASH:
		sc_start(bl,SC_STUN,(6*skilllv),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case AS_VENOMKNIFE:
		if (sd) //Poison chance must be that of Envenom. [Skotlex]
			skilllv = pc_checkskill(sd, TF_POISON);
	case TF_POISON:
	case AS_SPLASHER:
		if(!sc_start(bl,SC_POISON,(4*skilllv+10),skilllv,skill_get_time2(skillid,skilllv))
			&&	sd && skillid==TF_POISON
		)
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		break;

	case AS_SONICBLOW:
		sc_start(bl,SC_STUN,(2*skilllv+10),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case WZ_FIREPILLAR:
		unit_set_walkdelay(bl, tick, skill_get_time2(skillid, skilllv), 1);
		break;

	case MG_FROSTDIVER:
	case WZ_FROSTNOVA:
		sc_start(bl,SC_FREEZE,skilllv*3+35,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case WZ_STORMGUST:
		 //Tharis pointed out that this is normal freeze chance with a base of 300%
		if(tsc->sg_counter >= 3 &&
			sc_start(bl,SC_FREEZE,300,skilllv,skill_get_time2(skillid,skilllv)))
			tsc->sg_counter = 0;
		break;

	case WZ_METEOR:
		sc_start(bl,SC_STUN,3*skilllv,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case WZ_VERMILION:
		sc_start(bl,SC_BLIND,4*skilllv,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case HT_FREEZINGTRAP:
	case MA_FREEZINGTRAP:
		sc_start(bl,SC_FREEZE,(3*skilllv+35),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case HT_FLASHER:
		sc_start(bl,SC_BLIND,(10*skilllv+30),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case HT_LANDMINE:
	case MA_LANDMINE:
		sc_start(bl,SC_STUN,(5*skilllv+30),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case HT_SHOCKWAVE:
		status_percent_damage(src, bl, 0, 15*skilllv+5, false);
		break;

	case HT_SANDMAN:
	case MA_SANDMAN:
		sc_start(bl,SC_SLEEP,(10*skilllv+40),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case TF_SPRINKLESAND:
		sc_start(bl,SC_BLIND,20,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case TF_THROWSTONE:
		sc_start(bl,SC_STUN,3,skilllv,skill_get_time(skillid,skilllv));
		sc_start(bl,SC_BLIND,3,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case NPC_DARKCROSS:
	case CR_HOLYCROSS:
		sc_start(bl,SC_BLIND,3*skilllv,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case CR_GRANDCROSS:
	case NPC_GRANDDARKNESS:
		//Chance to cause blind status vs demon and undead element, but not against players
		if(!dstsd && (battle_check_undead(tstatus->race,tstatus->def_ele) || tstatus->race == RC_DEMON))
			sc_start(bl,SC_BLIND,100,skilllv,skill_get_time2(skillid,skilllv));
		attack_type |= BF_WEAPON;
		break;

	case AM_ACIDTERROR:
		sc_start(bl,SC_BLEEDING,(skilllv*3),skilllv,skill_get_time2(skillid,skilllv));
		if (skill_break_equip(bl, EQP_ARMOR, 100*skill_get_time(skillid,skilllv), BCT_ENEMY))
			clif_emotion(bl,E_OMG);
		break;

	case AM_DEMONSTRATION:
		skill_break_equip(bl, EQP_WEAPON, 100*skilllv, BCT_ENEMY);
		break;

	case CR_SHIELDCHARGE:
		sc_start(bl,SC_STUN,(15+skilllv*5),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case PA_PRESSURE:
		status_percent_damage(src, bl, 0, 15+5*skilllv, false);
		break;

	case RG_RAID:
		sc_start(bl,SC_STUN,(10+3*skilllv),skilllv,skill_get_time(skillid,skilllv));
		sc_start(bl,SC_BLIND,(10+3*skilllv),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case BA_FROSTJOKER:
		sc_start(bl,SC_FREEZE,(15+5*skilllv),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case DC_SCREAM:
		sc_start(bl,SC_STUN,(25+5*skilllv),skilllv,skill_get_time2(skillid,skilllv));
		break;

	case BD_LULLABY:
		sc_start(bl,SC_SLEEP,15,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case DC_UGLYDANCE:
		rate = 5+5*skilllv;
		if(sd && (skill=pc_checkskill(sd,DC_DANCINGLESSON)))
		    rate += 5+skill;
		status_zap(bl, 0, rate);
  		break;
	case SL_STUN:
		if (tstatus->size==1) //Only stuns mid-sized mobs.
			sc_start(bl,SC_STUN,(30+10*skilllv),skilllv,skill_get_time(skillid,skilllv));
		break;

	case NPC_PETRIFYATTACK:
		sc_start4(bl,status_skill2sc(skillid),50+10*skilllv,
			skilllv,0,0,skill_get_time(skillid,skilllv),
			skill_get_time2(skillid,skilllv));
		break;
	case NPC_CURSEATTACK:
	case NPC_SLEEPATTACK:
	case NPC_BLINDATTACK:
	case NPC_POISON:
	case NPC_SILENCEATTACK:
	case NPC_STUNATTACK:
	case NPC_HELLPOWER:
		sc_start(bl,status_skill2sc(skillid),50+10*skilllv,skilllv,skill_get_time2(skillid,skilllv));
		break;
	case NPC_ACIDBREATH:
	case NPC_ICEBREATH:
		sc_start(bl,status_skill2sc(skillid),70,skilllv,skill_get_time2(skillid,skilllv));
		break;
	case NPC_BLEEDING:
		sc_start(bl,SC_BLEEDING,(20*skilllv),skilllv,skill_get_time2(skillid,skilllv));
		break;
	case NPC_MENTALBREAKER:
	{	//Based on observations by Tharis, Mental Breaker should do SP damage
	  	//equal to Matk*skLevel.
		rate = sstatus->matk_min;
		if (rate < sstatus->matk_max)
			rate += rand()%(sstatus->matk_max - sstatus->matk_min);
		rate*=skilllv;
		status_zap(bl, 0, rate);
		break;
	}
	// Equipment breaking monster skills [Celest]
	case NPC_WEAPONBRAKER:
		skill_break_equip(bl, EQP_WEAPON, 150*skilllv, BCT_ENEMY);
		break;
	case NPC_ARMORBRAKE:
		skill_break_equip(bl, EQP_ARMOR, 150*skilllv, BCT_ENEMY);
		break;
	case NPC_HELMBRAKE:
		skill_break_equip(bl, EQP_HELM, 150*skilllv, BCT_ENEMY);
		break;
	case NPC_SHIELDBRAKE:
		skill_break_equip(bl, EQP_SHIELD, 150*skilllv, BCT_ENEMY);
		break;

	case CH_TIGERFIST:
		sc_start(bl,SC_STOP,(10+skilllv*10),0,skill_get_time2(skillid,skilllv));
		break;

	case LK_SPIRALPIERCE:
	case ML_SPIRALPIERCE:
		sc_start(bl,SC_STOP,(15+skilllv*5),0,skill_get_time2(skillid,skilllv));
		break;

	case ST_REJECTSWORD:
		sc_start(bl,SC_AUTOCOUNTER,(skilllv*15),skilllv,skill_get_time(skillid,skilllv));
		break;

	case PF_FOGWALL:
		if (src != bl && !tsc->data[SC_DELUGE])
			status_change_start(bl,SC_BLIND,10000,skilllv,0,0,0,skill_get_time2(skillid,skilllv),8);
		break;

	case LK_HEADCRUSH: //Headcrush has chance of causing Bleeding status, except on demon and undead element
		if (!(battle_check_undead(tstatus->race, tstatus->def_ele) || tstatus->race == RC_DEMON))
			sc_start(bl, SC_BLEEDING,50, skilllv, skill_get_time2(skillid,skilllv));
		break;

	case LK_JOINTBEAT:
		status = status_skill2sc(skillid);
		if (tsc->jb_flag) {
			sc_start2(bl,status,(5*skilllv+5),skilllv,tsc->jb_flag&BREAK_FLAGS,skill_get_time2(skillid,skilllv));
			tsc->jb_flag = 0;
		}
		break;
	case ASC_METEORASSAULT:
		//Any enemies hit by this skill will receive Stun, Darkness, or external bleeding status ailment with a 5%+5*SkillLV% chance.
		switch(rand()%3) {
			case 0:
				sc_start(bl,SC_BLIND,(5+skilllv*5),skilllv,skill_get_time2(skillid,1));
				break;
			case 1:
				sc_start(bl,SC_STUN,(5+skilllv*5),skilllv,skill_get_time2(skillid,2));
				break;
			default:
				sc_start(bl,SC_BLEEDING,(5+skilllv*5),skilllv,skill_get_time2(skillid,3));
  		}
		break;

	case HW_NAPALMVULCAN:
		sc_start(bl,SC_CURSE,5*skilllv,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case WS_CARTTERMINATION:	// Cart termination
		sc_start(bl,SC_STUN,5*skilllv,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case CR_ACIDDEMONSTRATION:
		skill_break_equip(bl, EQP_WEAPON|EQP_ARMOR, 100*skilllv, BCT_ENEMY);
		break;

	case TK_DOWNKICK:
		sc_start(bl,SC_STUN,100,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case TK_JUMPKICK:
		if( dstsd && dstsd->class_ != MAPID_SOUL_LINKER && !tsc->data[SC_PRESERVE] )
		{// debuff the following statuses
			status_change_end(bl, SC_SPIRIT, INVALID_TIMER);
			status_change_end(bl, SC_ADRENALINE2, INVALID_TIMER);
			status_change_end(bl, SC_KAITE, INVALID_TIMER);
			status_change_end(bl, SC_KAAHI, INVALID_TIMER);
			status_change_end(bl, SC_ONEHAND, INVALID_TIMER);
			status_change_end(bl, SC_ASPDPOTION2, INVALID_TIMER);
		}
		break;
	case TK_TURNKICK:
	case MO_BALKYOUNG: //Note: attack_type is passed as BF_WEAPON for the actual target, BF_MISC for the splash-affected mobs.
		if(attack_type&BF_MISC) //70% base stun chance...
			sc_start(bl,SC_STUN,70,skilllv,skill_get_time2(skillid,skilllv));
		break;
	case GS_BULLSEYE: //0.1% coma rate.
		if(tstatus->race == RC_BRUTE || tstatus->race == RC_DEMIHUMAN)
			status_change_start(bl,SC_COMA,10,skilllv,0,0,0,0,0);
		break;
	case GS_PIERCINGSHOT:
		sc_start(bl,SC_BLEEDING,(skilllv*3),skilllv,skill_get_time2(skillid,skilllv));
		break;
	case NJ_HYOUSYOURAKU:
		sc_start(bl,SC_FREEZE,(10+10*skilllv),skilllv,skill_get_time2(skillid,skilllv));
		break;
	case GS_FLING:
		sc_start(bl,SC_FLING,100, sd?sd->spiritball_old:5,skill_get_time(skillid,skilllv));
		break;
	case GS_DISARM:
		rate = 3*skilllv;
		if (sstatus->dex > tstatus->dex)
			rate += (sstatus->dex - tstatus->dex)/5; //TODO: Made up formula
		skill_strip_equip(bl, EQP_WEAPON, rate, skilllv, skill_get_time(skillid,skilllv));
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;
	case NPC_EVILLAND:
		sc_start(bl,SC_BLIND,5*skilllv,skilllv,skill_get_time2(skillid,skilllv));
		break;
	case NPC_HELLJUDGEMENT:
		sc_start(bl,SC_CURSE,100,skilllv,skill_get_time2(skillid,skilllv));
		break;
	case NPC_CRITICALWOUND:
		sc_start(bl,SC_CRITICALWOUND,100,skilllv,skill_get_time2(skillid,skilllv));
		break;
	}

	if (md && battle_config.summons_trigger_autospells && md->master_id && md->special_state.ai)
	{	//Pass heritage to Master for status causing effects. [Skotlex]
		sd = map_id2sd(md->master_id);
		src = sd?&sd->bl:src;
	}

	if( attack_type&BF_WEAPON )
	{ // Coma, Breaking Equipment
		if( sd && sd->special_state.bonus_coma )
		{
			rate  = sd->weapon_coma_ele[tstatus->def_ele];
			rate += sd->weapon_coma_race[tstatus->race];
			rate += sd->weapon_coma_race[tstatus->mode&MD_BOSS?RC_BOSS:RC_NONBOSS];
			if (rate)
				status_change_start(bl, SC_COMA, rate, 0, 0, 0, 0, 0, 0);
		}
		if( sd && battle_config.equip_self_break_rate )
		{	// Self weapon breaking
			rate = battle_config.equip_natural_break_rate;
			if( sc )
			{
				if(sc->data[SC_OVERTHRUST])
					rate += 10;
				if(sc->data[SC_MAXOVERTHRUST])
					rate += 10;
			}
			if( rate )
				skill_break_equip(src, EQP_WEAPON, rate, BCT_SELF);
		}	
		if( battle_config.equip_skill_break_rate && skillid != WS_CARTTERMINATION && skillid != ITM_TOMAHAWK )
		{	// Cart Termination/Tomahawk won't trigger breaking data. Why? No idea, go ask Gravity.
			// Target weapon breaking
			rate = 0;
			if( sd )
				rate += sd->break_weapon_rate;
			if( sc && sc->data[SC_MELTDOWN] )
				rate += sc->data[SC_MELTDOWN]->val2;
			if( rate )
				skill_break_equip(bl, EQP_WEAPON, rate, BCT_ENEMY);

			// Target armor breaking
			rate = 0;
			if( sd )
				rate += sd->break_armor_rate;
			if( sc && sc->data[SC_MELTDOWN] )
				rate += sc->data[SC_MELTDOWN]->val3;
			if( rate )
				skill_break_equip(bl, EQP_ARMOR, rate, BCT_ENEMY);
		}
	}

	// Autospell when attacking
	if( sd && !status_isdead(bl) && sd->autospell[0].id )
	{
		struct block_list *tbl;
		struct unit_data *ud;
		int i, skilllv;

		for (i = 0; i < ARRAYLENGTH(sd->autospell) && sd->autospell[i].id; i++) {

			if(!(sd->autospell[i].flag&attack_type&BF_WEAPONMASK &&
				 sd->autospell[i].flag&attack_type&BF_RANGEMASK &&
				 sd->autospell[i].flag&attack_type&BF_SKILLMASK))
				continue; // one or more trigger conditions were not fulfilled

			skill = (sd->autospell[i].id > 0) ? sd->autospell[i].id : -sd->autospell[i].id;

			if (skillnotok(skill, sd))
				continue;

			skilllv = sd->autospell[i].lv?sd->autospell[i].lv:1;
			if (skilllv < 0) skilllv = 1+rand()%(-skilllv);

			rate = (!sd->state.arrow_atk) ? sd->autospell[i].rate : sd->autospell[i].rate / 2;

			if (rand()%1000 >= rate)
				continue;

			tbl = (sd->autospell[i].id < 0) ? src : bl;

			if( battle_config.autospell_check_range &&
				!battle_check_range(src, tbl, skill_get_range2(src, skill,skilllv) + (skill == RG_CLOSECONFINE?0:1)) )
				continue;

			if (skill == AS_SONICBLOW)
				pc_stop_attack(sd); //Special case, Sonic Blow autospell should stop the player attacking.

			sd->state.autocast = 1;
			skill_consume_requirement(sd,skill,skilllv,1);
			switch (skill_get_casttype(skill)) {
				case CAST_GROUND:
					skill_castend_pos2(src, tbl->x, tbl->y, skill, skilllv, tick, 0);
					break;
				case CAST_NODAMAGE:
					skill_castend_nodamage_id(src, tbl, skill, skilllv, tick, 0);
					break;
				case CAST_DAMAGE:
					skill_castend_damage_id(src, tbl, skill, skilllv, tick, 0);
					break;
			}
			sd->state.autocast = 0;
			//Set canact delay. [Skotlex]
			ud = unit_bl2ud(src);
			if (ud) {
				rate = skill_delayfix(src, skill, skilllv);
				if (DIFF_TICK(ud->canact_tick, tick + rate) < 0){
					ud->canact_tick = tick+rate;
					if( sd && skill_get_cooldown(skill,skilllv) > 0 )
						skill_blockpc_start(sd, skill, skill_get_cooldown(skill, skilllv));
					if ( battle_config.display_status_timers && sd )
						clif_status_change(src, SI_ACTIONDELAY, 1, rate);
				}
			}
		}
	}

	//Autobonus when attacking
	if( sd && sd->autobonus[0].rate )
	{
		int i;
		for( i = 0; i < ARRAYLENGTH(sd->autobonus); i++ )
		{
			if( rand()%1000 >= sd->autobonus[i].rate )
				continue;
			if( sd->autobonus[i].active != INVALID_TIMER )
				continue;
			if(!(sd->autobonus[i].atk_type&attack_type&BF_WEAPONMASK &&
				 sd->autobonus[i].atk_type&attack_type&BF_RANGEMASK &&
				 sd->autobonus[i].atk_type&attack_type&BF_SKILLMASK))
				continue; // one or more trigger conditions were not fulfilled
			pc_exeautobonus(sd,&sd->autobonus[i]);
		}
	}

	//Polymorph
	if(sd && sd->classchange && attack_type&BF_WEAPON &&
		dstmd && !(tstatus->mode&MD_BOSS) &&
		(rand()%10000 < sd->classchange))
	{
		struct mob_db *mob;
		int class_;
		skill = 0;
		do {
			do {
				class_ = rand() % MAX_MOB_DB;
			} while (!mobdb_checkid(class_));

			rate = rand() % 1000000;
			mob = mob_db(class_);
		} while (
			(mob->status.mode&(MD_BOSS|MD_PLANT) || mob->summonper[0] <= rate) &&
		  	(skill++) < 2000);
		if (skill < 2000)
			mob_class_change(dstmd,class_);
	}

	return 0;
}

int skill_onskillusage(struct map_session_data *sd, struct block_list *bl, int skillid, unsigned int tick)
{
	int skill, skilllv, i;
	struct block_list *tbl;

	if( sd == NULL || skillid <= 0 )
		return 0;

	for( i = 0; i < ARRAYLENGTH(sd->autospell3) && sd->autospell3[i].flag; i++ )
	{
		if( sd->autospell3[i].flag != skillid )
			continue;

		if( sd->autospell3[i].lock )
			continue;  // autospell already being executed

		skill = (sd->autospell3[i].id > 0) ? sd->autospell3[i].id : -sd->autospell3[i].id;
		if( skillnotok(skill, sd) )
			continue;

		skilllv = sd->autospell3[i].lv ? sd->autospell3[i].lv : 1;
		if( skilllv < 0 ) skilllv = 1 + rand()%(-skilllv);

		if( sd->autospell3[i].id >= 0 && bl == NULL )
			continue; // No target
		if( rand()%1000 >= sd->autospell3[i].rate )
			continue;
		tbl = (sd->autospell3[i].id < 0) ? &sd->bl : bl;

		if( battle_config.autospell_check_range &&
			!battle_check_range(&sd->bl, tbl, skill_get_range2(&sd->bl, skill,skilllv) + (skill == RG_CLOSECONFINE?0:1)) )
			continue;

		sd->state.autocast = 1;
		sd->autospell3[i].lock = true;
		skill_consume_requirement(sd,skill,skilllv,1);
		switch( skill_get_casttype(skill) )
		{
			case CAST_GROUND:   skill_castend_pos2(&sd->bl, tbl->x, tbl->y, skill, skilllv, tick, 0); break;
			case CAST_NODAMAGE: skill_castend_nodamage_id(&sd->bl, tbl, skill, skilllv, tick, 0); break;
			case CAST_DAMAGE:   skill_castend_damage_id(&sd->bl, tbl, skill, skilllv, tick, 0); break;
		}
		sd->autospell3[i].lock = false;
		sd->state.autocast = 0;
	}

	if( sd && sd->autobonus3[0].rate )
	{
		for( i = 0; i < ARRAYLENGTH(sd->autobonus3); i++ )
		{
			if( rand()%1000 >= sd->autobonus3[i].rate )
				continue;
			if( sd->autobonus3[i].active != INVALID_TIMER )
				continue;
			if( sd->autobonus3[i].atk_type != skillid )
				continue;
			pc_exeautobonus(sd,&sd->autobonus3[i]);
		}
	}

	return 1;
}

/* Splitted off from skill_additional_effect, which is never called when the
 * attack skill kills the enemy. Place in this function counter status effects
 * when using skills (eg: Asura's sp regen penalty, or counter-status effects
 * from cards) that will take effect on the source, not the target. [Skotlex]
 * Note: Currently this function only applies to Extremity Fist and BF_WEAPON
 * type of skills, so not every instance of skill_additional_effect needs a call
 * to this one.
 */
int skill_counter_additional_effect (struct block_list* src, struct block_list *bl, int skillid, int skilllv, int attack_type, unsigned int tick)
{
	int rate;
	struct map_session_data *sd=NULL;
	struct map_session_data *dstsd=NULL;
	struct status_change *tsc;

	nullpo_ret(src);
	nullpo_ret(bl);

	if(skillid < 0) return 0;
	if(skillid > 0 && skilllv <= 0) return 0;	// don't forget auto attacks! - celest

	tsc = status_get_sc(bl);
	if (tsc && !tsc->count)
		tsc = NULL;

	sd = BL_CAST(BL_PC, src);
	dstsd = BL_CAST(BL_PC, bl);

	if(dstsd && attack_type&BF_WEAPON)
	{	//Counter effects.
		enum sc_type type;
		int i, time;
		for(i=0; i < ARRAYLENGTH(dstsd->addeff2) && dstsd->addeff2[i].flag; i++)
		{
			rate = dstsd->addeff2[i].rate;
			if (attack_type&BF_LONG)
				rate+=dstsd->addeff2[i].arrow_rate;
			if (!rate) continue;

			if ((dstsd->addeff2[i].flag&(ATF_LONG|ATF_SHORT)) != (ATF_LONG|ATF_SHORT))
			{	//Trigger has range consideration.
				if((dstsd->addeff2[i].flag&ATF_LONG && !(attack_type&BF_LONG)) ||
					(dstsd->addeff2[i].flag&ATF_SHORT && !(attack_type&BF_SHORT)))
					continue; //Range Failed.
			}
			type = dstsd->addeff2[i].id;
			time = skill_get_time2(status_sc2skill(type),7);

			if (dstsd->addeff2[i].flag&ATF_TARGET)
				status_change_start(src,type,rate,7,0,0,0,time,0);

			if (dstsd->addeff2[i].flag&ATF_SELF && !status_isdead(bl))
				status_change_start(bl,type,rate,7,0,0,0,time,0);
		}
	}

	switch(skillid){
	case MO_EXTREMITYFIST:
		sc_start(src,status_skill2sc(skillid),100,skilllv,skill_get_time2(skillid,skilllv));
		break;
	case GS_FULLBUSTER:
		sc_start(src,SC_BLIND,2*skilllv,skilllv,skill_get_time2(skillid,skilllv));
		break;
	case HFLI_SBR44:	//[orn]
	case HVAN_EXPLOSION:
		if(src->type == BL_HOM){
			TBL_HOM *hd = (TBL_HOM*)src;
			hd->homunculus.intimacy = 200;
			if (hd->master)
				clif_send_homdata(hd->master,SP_INTIMATE,hd->homunculus.intimacy/100);
		}
		break;
	case CR_GRANDCROSS:
	case NPC_GRANDDARKNESS:
		attack_type |= BF_WEAPON;
		break;
	}

	if(sd && (sd->class_&MAPID_UPPERMASK) == MAPID_STAR_GLADIATOR &&
		rand()%10000 < battle_config.sg_miracle_skill_ratio)	//SG_MIRACLE [Komurka]
		sc_start(src,SC_MIRACLE,100,1,battle_config.sg_miracle_skill_duration);

	if(sd && skillid && attack_type&BF_MAGIC && status_isdead(bl) &&
	 	!(skill_get_inf(skillid)&(INF_GROUND_SKILL|INF_SELF_SKILL)) &&
		(rate=pc_checkskill(sd,HW_SOULDRAIN))>0
	){	//Soul Drain should only work on targetted spells [Skotlex]
		if (pc_issit(sd)) pc_setstand(sd); //Character stuck in attacking animation while 'sitting' fix. [Skotlex]
		clif_skill_nodamage(src,bl,HW_SOULDRAIN,rate,1);
		status_heal(src, 0, status_get_lv(bl)*(95+15*rate)/100, 2);
	}

	if( sd && status_isdead(bl) )
	{
		int sp = 0, hp = 0;
		if( attack_type&BF_WEAPON )
		{
			sp += sd->sp_gain_value;
			sp += sd->sp_gain_race[status_get_race(bl)];
			sp += sd->sp_gain_race[is_boss(bl)?RC_BOSS:RC_NONBOSS];
			hp += sd->hp_gain_value;
		}
		if( attack_type&BF_MAGIC )
		{
			sp += sd->magic_sp_gain_value;
			hp += sd->magic_hp_gain_value;
		}
		if( hp || sp )
			status_heal(src, hp, sp, battle_config.show_hp_sp_gain?2:0);
	}

	// Trigger counter-spells to retaliate against damage causing skills.
	if(dstsd && !status_isdead(bl) && dstsd->autospell2[0].id &&
		!(skillid && skill_get_nk(skillid)&NK_NO_DAMAGE))
	{
		struct block_list *tbl;
		struct unit_data *ud;
		int i, skillid, skilllv, rate;

		for (i = 0; i < ARRAYLENGTH(dstsd->autospell2) && dstsd->autospell2[i].id; i++) {

			if(!(dstsd->autospell2[i].flag&attack_type&BF_WEAPONMASK &&
				 dstsd->autospell2[i].flag&attack_type&BF_RANGEMASK &&
				 dstsd->autospell2[i].flag&attack_type&BF_SKILLMASK))
				continue; // one or more trigger conditions were not fulfilled

			skillid = (dstsd->autospell2[i].id > 0) ? dstsd->autospell2[i].id : -dstsd->autospell2[i].id;
			skilllv = dstsd->autospell2[i].lv?dstsd->autospell2[i].lv:1;
			if (skilllv < 0) skilllv = 1+rand()%(-skilllv);

			rate = dstsd->autospell2[i].rate;
			if (attack_type&BF_LONG)
				 rate>>=1;

			if (skillnotok(skillid, dstsd))
				continue;
			if (rand()%1000 >= rate)
				continue;

			tbl = (dstsd->autospell2[i].id < 0) ? bl : src;

			if( !battle_check_range(src, tbl, skill_get_range2(src, skillid,skilllv) + (skillid == RG_CLOSECONFINE?0:1)) && battle_config.autospell_check_range )
				continue;

			dstsd->state.autocast = 1;
			skill_consume_requirement(dstsd,skillid,skilllv,1);
			switch (skill_get_casttype(skillid)) {
				case CAST_GROUND:
					skill_castend_pos2(bl, tbl->x, tbl->y, skillid, skilllv, tick, 0);
					break;
				case CAST_NODAMAGE:
					skill_castend_nodamage_id(bl, tbl, skillid, skilllv, tick, 0);
					break;
				case CAST_DAMAGE:
					skill_castend_damage_id(bl, tbl, skillid, skilllv, tick, 0);
					break;
			}
			dstsd->state.autocast = 0;
			//Set canact delay. [Skotlex]
			ud = unit_bl2ud(bl);
			if (ud) {
				rate = skill_delayfix(bl, skillid, skilllv);
				if (DIFF_TICK(ud->canact_tick, tick + rate) < 0){
					ud->canact_tick = tick+rate;
					if( sd && skill_get_cooldown(skillid,skilllv) > 0 )
						skill_blockpc_start(sd, skillid, skill_get_cooldown(skillid, skilllv));
					if ( battle_config.display_status_timers && dstsd )
						clif_status_change(bl, SI_ACTIONDELAY, 1, rate);
				}
			}
		}
	}

	//Autobonus when attacked
	if( dstsd && !status_isdead(bl) && dstsd->autobonus2[0].rate && !(skillid && skill_get_nk(skillid)&NK_NO_DAMAGE) )
	{
		int i;
		for( i = 0; i < ARRAYLENGTH(dstsd->autobonus2); i++ )
		{
			if( rand()%1000 >= dstsd->autobonus2[i].rate )
				continue;
			if( dstsd->autobonus2[i].active != INVALID_TIMER )
				continue;
			if(!(dstsd->autobonus2[i].atk_type&attack_type&BF_WEAPONMASK &&
				 dstsd->autobonus2[i].atk_type&attack_type&BF_RANGEMASK &&
				 dstsd->autobonus2[i].atk_type&attack_type&BF_SKILLMASK))
				continue; // one or more trigger conditions were not fulfilled
			pc_exeautobonus(dstsd,&dstsd->autobonus2[i]);
		}
	}

	return 0;
}
/*=========================================================================
 Breaks equipment. On-non players causes the corresponding strip effect.
 - rate goes from 0 to 10000 (100.00%)
 - flag is a BCT_ flag to indicate which type of adjustment should be used
   (BCT_ENEMY/BCT_PARTY/BCT_SELF) are the valid values.
--------------------------------------------------------------------------*/
int skill_break_equip (struct block_list *bl, unsigned short where, int rate, int flag)
{
	const int where_list[4]     = {EQP_WEAPON, EQP_ARMOR, EQP_SHIELD, EQP_HELM};
	const enum sc_type scatk[4] = {SC_STRIPWEAPON, SC_STRIPARMOR, SC_STRIPSHIELD, SC_STRIPHELM};
	const enum sc_type scdef[4] = {SC_CP_WEAPON, SC_CP_ARMOR, SC_CP_SHIELD, SC_CP_HELM};
	struct status_change *sc = status_get_sc(bl);
	int i,j;
	TBL_PC *sd;
	sd = BL_CAST(BL_PC, bl);
	if (sc && !sc->count)
		sc = NULL;

	if (sd) {
		if (sd->unbreakable_equip)
			where &= ~sd->unbreakable_equip;
		if (sd->unbreakable)
			rate -= rate*sd->unbreakable/100;
		if (where&EQP_WEAPON) {
			switch (sd->status.weapon) {
				case W_FIST:	//Bare fists should not break :P
				case W_1HAXE:
				case W_2HAXE:
				case W_MACE: // Axes and Maces can't be broken [DracoRPG]
				case W_2HMACE:
				case W_STAFF:
				case W_2HSTAFF:
				case W_BOOK: //Rods and Books can't be broken [Skotlex]
				case W_HUUMA:
					where &= ~EQP_WEAPON;
			}
		}
	}
	if (flag&BCT_ENEMY) {
		if (battle_config.equip_skill_break_rate != 100)
			rate = rate*battle_config.equip_skill_break_rate/100;
	} else if (flag&(BCT_PARTY|BCT_SELF)) {
		if (battle_config.equip_self_break_rate != 100)
			rate = rate*battle_config.equip_self_break_rate/100;
	}

	for (i = 0; i < 4; i++) {
		if (where&where_list[i]) {
			if (sc && sc->count && sc->data[scdef[i]])
				where&=~where_list[i];
			else if (rand()%10000 >= rate)
				where&=~where_list[i];
			else if (!sd && !(status_get_mode(bl)&MD_BOSS)) //Cause Strip effect.
				sc_start(bl,scatk[i],100,0,skill_get_time(status_sc2skill(scatk[i]),1));
		}
	}
	if (!where) //Nothing to break.
		return 0;
	if (sd) {
		for (i = 0; i < EQI_MAX; i++) {
			j = sd->equip_index[i];
			if (j < 0 || sd->status.inventory[j].attribute == 1 || !sd->inventory_data[j])
				continue;
			flag = 0;
			switch(i) {
				case EQI_HEAD_TOP: //Upper Head
					flag = (where&EQP_HELM);
					break;
				case EQI_ARMOR: //Body
					flag = (where&EQP_ARMOR);
					break;
				case EQI_HAND_R: //Left/Right hands
				case EQI_HAND_L:
					flag = (
						(where&EQP_WEAPON && sd->inventory_data[j]->type == IT_WEAPON) ||
						(where&EQP_SHIELD && sd->inventory_data[j]->type == IT_ARMOR));
					break;
				case EQI_SHOES:
					flag = (where&EQP_SHOES);
					break;
				case EQI_GARMENT:
					flag = (where&EQP_GARMENT);
					break;
				default:
					continue;
			}
			if (flag) {
				sd->status.inventory[j].attribute = 1;
				pc_unequipitem(sd, j, 3);
			}
		}
		clif_equiplist(sd);
	}

	return where; //Return list of pieces broken.
}

int skill_strip_equip(struct block_list *bl, unsigned short where, int rate, int lv, int time)
{
	struct status_change *sc;
	const int pos[4]             = {EQP_WEAPON, EQP_SHIELD, EQP_ARMOR, EQP_HELM};
	const enum sc_type sc_atk[4] = {SC_STRIPWEAPON, SC_STRIPSHIELD, SC_STRIPARMOR, SC_STRIPHELM};
	const enum sc_type sc_def[4] = {SC_CP_WEAPON, SC_CP_SHIELD, SC_CP_ARMOR, SC_CP_HELM};
	int i;

	if (rand()%100 >= rate)
		return 0;

	sc = status_get_sc(bl);
	if (!sc)
		return 0;

	for (i = 0; i < ARRAYLENGTH(pos); i++) {
		if (where&pos[i] && sc->data[sc_def[i]])
			where&=~pos[i];
	}
	if (!where) return 0;

	for (i = 0; i < ARRAYLENGTH(pos); i++) {
		if (where&pos[i] && !sc_start(bl, sc_atk[i], 100, lv, time))
			where&=~pos[i];
	}
	return where?1:0;
}


/*=========================================================================
 Used to knock back players, monsters, traps, etc
 - 'count' is the number of squares to knock back
 - 'direction' indicates the way OPPOSITE to the knockback direction (or -1 for default behavior)
 - if 'flag&0x1', position update packets must not be sent.
 -------------------------------------------------------------------------*/
int skill_blown(struct block_list* src, struct block_list* target, int count, int direction, int flag)
{
	int dx = 0, dy = 0;
	struct skill_unit* su = NULL;

	nullpo_ret(src);

	if (src != target && map_flag_gvg3(target->m))
		return 0; //No knocking back in WoE
	if (count == 0)
		return 0; //Actual knockback distance is 0.

	switch (target->type)
	{
		case BL_MOB:
		{
			struct mob_data* md = BL_CAST(BL_MOB, target);
			if( md->class_ == MOBID_EMPERIUM || md->class_ == 1674 )
				return 0;
			if(src != target && is_boss(target)) //Bosses can't be knocked-back
				return 0;
		}
			break;
		case BL_PC:
		{
			struct map_session_data *sd = BL_CAST(BL_PC, target);
			if( sd->sc.data[SC_BASILICA] && sd->sc.data[SC_BASILICA]->val4 == sd->bl.id && !is_boss(src))
				return 0; // Basilica caster can't be knocked-back by normal monsters.
			if( src != target && sd->special_state.no_knockback )
				return 0;
			if( (sd->state.vending || sd->state.buyingstore) && map[target->m].flag.vending_cell )
				return 0;
		}
			break;
		case BL_SKILL:
			su = (struct skill_unit *)target;
			if( su && su->group && su->group->unit_id == UNT_ANKLESNARE )
				return 0; // ankle snare cannot be knocked back
			break;
	}

	if (direction == -1) // <optimized>: do the computation here instead of outside
		direction = map_calc_dir(target, src->x, src->y); // direction from src to target, reversed

	if (direction >= 0 && direction < 8)
	{	// take the reversed 'direction' and reverse it
		dx = -dirx[direction];
		dy = -diry[direction];
	}

	return unit_blown(target, dx, dy, count, flag&0x1);
}


//Checks if 'bl' should reflect back a spell cast by 'src'.
//type is the type of magic attack: 0: indirect (aoe), 1: direct (targetted)
static int skill_magic_reflect(struct block_list* src, struct block_list* bl, int type)
{
	struct status_change *sc = status_get_sc(bl);
	struct map_session_data* sd = BL_CAST(BL_PC, bl);

	// item-based reflection
	if( sd && sd->magic_damage_return && type && rand()%100 < sd->magic_damage_return )
		return 1;

	if( is_boss(src) )
		return 0;

	// status-based reflection
	if( !sc || sc->count == 0 )
		return 0;

	if( sc->data[SC_MAGICMIRROR] && rand()%100 < sc->data[SC_MAGICMIRROR]->val2 )
		return 1;

	if( sc->data[SC_KAITE] && (src->type == BL_PC || status_get_lv(src) <= 80) )
	{// Kaite only works against non-players if they are low-level.
		clif_specialeffect(bl, 438, AREA);
		if( --sc->data[SC_KAITE]->val2 <= 0 )
			status_change_end(bl, SC_KAITE, INVALID_TIMER);
		return 2;
	}

	return 0;
}

/*
 * =========================================================================
 * Does a skill attack with the given properties.
 * src is the master behind the attack (player/mob/pet)
 * dsrc is the actual originator of the damage, can be the same as src, or a BL_SKILL
 * bl is the target to be attacked.
 * flag can hold a bunch of information:
 * flag&0xFFF is passed to the underlying battle_calc_attack for processing
 *      (usually holds number of targets, or just 1 for simple splash attacks)
 * flag&0x1000 is used to tag that this is a splash-attack (so the damage
 *      packet shouldn't display a skill animation)
 * flag&0x2000 is used to signal that the skilllv should be passed as -1 to the
 *      client (causes player characters to not scream skill name)
 *-------------------------------------------------------------------------*/
int skill_attack (int attack_type, struct block_list* src, struct block_list *dsrc, struct block_list *bl, int skillid, int skilllv, unsigned int tick, int flag)
{
	struct Damage dmg;
	struct status_data *sstatus, *tstatus;
	struct status_change *sc;
	struct map_session_data *sd, *tsd;
	int type,damage,rdamage=0;
	bool reflected = false;

	if(skillid > 0 && skilllv <= 0) return 0;

	nullpo_ret(src);	//Source is the master behind the attack (player/mob/pet)
	nullpo_ret(dsrc); //dsrc is the actual originator of the damage, can be the same as src, or a skill casted by src.
	nullpo_ret(bl); //Target to be attacked.

	if (src != dsrc) {
		//When caster is not the src of attack, this is a ground skill, and as such, do the relevant target checking. [Skotlex]
		if( !status_check_skilluse(battle_config.skill_caster_check?src:NULL, bl, skillid, skilllv, 2) )
			return 0;
	} else if ((flag&SD_ANIMATION) && skill_get_nk(skillid)&NK_SPLASH) {
		//Note that splash attacks often only check versus the targetted mob, those around the splash area normally don't get checked for being hidden/cloaked/etc. [Skotlex]
		if( !status_check_skilluse(src, bl, skillid, skilllv, 2) )
			return 0;
	}

	sd = BL_CAST(BL_PC, src);
	tsd = BL_CAST(BL_PC, bl);

	sstatus = status_get_status_data(src);
	tstatus = status_get_status_data(bl);
	sc= status_get_sc(bl);
	if (sc && !sc->count) sc = NULL; //Don't need it.

	// Is this check really needed? FrostNova won't hurt you if you step right where the caster is?
	if(skillid == WZ_FROSTNOVA && dsrc->x == bl->x && dsrc->y == bl->y)
		return 0;
	 //Trick Dead protects you from damage, but not from buffs and the like, hence it's placed here.
	if (sc && sc->data[SC_TRICKDEAD] && !(sstatus->mode&MD_BOSS))
		return 0;

	dmg = battle_calc_attack(attack_type,src,bl,skillid,skilllv,flag&0xFFF);

	//Skotlex: Adjusted to the new system
	if(src->type==BL_PET)
	{ // [Valaris]
		struct pet_data *pd = (TBL_PET*)src;
		if (pd->a_skill && pd->a_skill->div_ && pd->a_skill->id == skillid)
		{
			int element = skill_get_ele(skillid, skilllv);
			if (skillid == -1)
				element = sstatus->rhw.ele;
			if (element != ELE_NEUTRAL || !(battle_config.attack_attr_none&BL_PET))
				dmg.damage=battle_attr_fix(src, bl, skilllv, element, tstatus->def_ele, tstatus->ele_lv);
			else
				dmg.damage= skilllv;
			dmg.damage2=0;
			dmg.div_= pd->a_skill->div_;
		}
	}

	if( dmg.flag&BF_MAGIC && ( skillid != NPC_EARTHQUAKE || (battle_config.eq_single_target_reflectable && (flag&0xFFF) == 1) ) )
	{ // Earthquake on multiple targets is not counted as a target skill. [Inkfish]
		if( (dmg.damage || dmg.damage2) && (type = skill_magic_reflect(src, bl, src==dsrc)) )
		{	//Magic reflection, switch caster/target
			struct block_list *tbl = bl;
			bl = src;
			src = tbl;
			sd = BL_CAST(BL_PC, src);
			tsd = BL_CAST(BL_PC, bl);
			sc = status_get_sc(bl);
			if (sc && !sc->count) sc = NULL; //Don't need it.
			reflected = true;

			//Spirit of Wizard blocks Kaite's reflection
			if( type == 2 && sc && sc->data[SC_SPIRIT] && sc->data[SC_SPIRIT]->val2 == SL_WIZARD )
			{	//Consume one Fragment per hit of the casted skill? [Skotlex]
			  	type = tsd?pc_search_inventory (tsd, 7321):0;
				if (type >= 0) {
					if ( tsd ) pc_delitem(tsd, type, 1, 0, 1, LOG_TYPE_CONSUME);
					dmg.damage = dmg.damage2 = 0;
					dmg.dmg_lv = ATK_MISS;
					sc->data[SC_SPIRIT]->val3 = skillid;
					sc->data[SC_SPIRIT]->val4 = dsrc->id;
				}
			}
		}

		if(sc && sc->data[SC_MAGICROD] && src == dsrc) {
			int sp = skill_get_sp(skillid,skilllv);
			dmg.damage = dmg.damage2 = 0;
			dmg.dmg_lv = ATK_MISS; //This will prevent skill additional effect from taking effect. [Skotlex]
			sp = sp * sc->data[SC_MAGICROD]->val2 / 100;
			if(skillid == WZ_WATERBALL && skilllv > 1)
				sp = sp/((skilllv|1)*(skilllv|1)); //Estimate SP cost of a single water-ball
			status_heal(bl, 0, sp, 2);
			clif_skill_nodamage(bl,bl,SA_MAGICROD,sc->data[SC_MAGICROD]->val1,1);
		}
	}

	damage = dmg.damage + dmg.damage2;

	if( (skillid == AL_INCAGI || skillid == AL_BLESSING || 
		skillid == CASH_BLESSING || skillid == CASH_INCAGI ||
		skillid == MER_INCAGI || skillid == MER_BLESSING) && tsd->sc.data[SC_CHANGEUNDEAD] )
		damage = 1;

	if( damage > 0 && dmg.flag&BF_WEAPON && src != bl && ( src == dsrc || ( dsrc->type == BL_SKILL && ( skillid == SG_SUN_WARM || skillid == SG_MOON_WARM || skillid == SG_STAR_WARM ) ) )
		&& skillid != WS_CARTTERMINATION )
		rdamage = battle_calc_return_damage(bl, damage, dmg.flag);

	//Skill hit type
	type=(skillid==0)?5:skill_get_hit(skillid);

	if(damage < dmg.div_
		//Only skills that knockback even when they miss. [Skotlex]
		&& skillid != CH_PALMSTRIKE)
		dmg.blewcount = 0;

	if(skillid == CR_GRANDCROSS||skillid == NPC_GRANDDARKNESS) {
		if(battle_config.gx_disptype) dsrc = src;
		if(src == bl) type = 4;
		else flag|=SD_ANIMATION;
	}
	if(skillid == NJ_TATAMIGAESHI) {
		dsrc = src; //For correct knockback.
		flag|=SD_ANIMATION;
	}

	if(sd) {
		int flag = 0; //Used to signal if this skill can be combo'ed later on.
		struct status_change_entry *sce;
		if ((sce = sd->sc.data[SC_COMBO]))
		{	//End combo state after skill is invoked. [Skotlex]
			switch (skillid) {
			case TK_TURNKICK:
			case TK_STORMKICK:
			case TK_DOWNKICK:
			case TK_COUNTER:
				if (pc_famerank(sd->status.char_id,MAPID_TAEKWON))
			  	{	//Extend combo time.
					sce->val1 = skillid; //Update combo-skill
					sce->val3 = skillid;
					delete_timer(sce->timer, status_change_timer);
					sce->timer = add_timer(tick+sce->val4, status_change_timer, src->id, SC_COMBO);
					break;
				}
				unit_cancel_combo(src); // Cancel combo wait
				break;
			default:
				if( src == dsrc ) // Ground skills are exceptions. [Inkfish]
					status_change_end(src, SC_COMBO, INVALID_TIMER);
			}
		}
		switch(skillid)
		{
			case MO_TRIPLEATTACK:
				if (pc_checkskill(sd, MO_CHAINCOMBO) > 0)
					flag=1;
				break;
			case MO_CHAINCOMBO:
				if(pc_checkskill(sd, MO_COMBOFINISH) > 0 && sd->spiritball > 0)
					flag=1;
				break;
			case MO_COMBOFINISH:
				if (sd->status.party_id>0) //bonus from SG_FRIEND [Komurka]
					party_skill_check(sd, sd->status.party_id, MO_COMBOFINISH, skilllv);
				if (pc_checkskill(sd, CH_TIGERFIST) > 0 && sd->spiritball > 0)
					flag=1;
			case CH_TIGERFIST:
				if (!flag && pc_checkskill(sd, CH_CHAINCRUSH) > 0 && sd->spiritball > 1)
					flag=1;
			case CH_CHAINCRUSH:
				if (!flag && pc_checkskill(sd, MO_EXTREMITYFIST) > 0 && sd->spiritball > 0 && sd->sc.data[SC_EXPLOSIONSPIRITS])
					flag=1;
				break;
			case AC_DOUBLE:
				if( (tstatus->race == RC_BRUTE || tstatus->race == RC_INSECT) && pc_checkskill(sd, HT_POWER))
				{
					//TODO: This code was taken from Triple Blows, is this even how it should be? [Skotlex]
					sc_start2(src,SC_COMBO,100,HT_POWER,bl->id,2000);
					clif_combo_delay(src,2000);
				}
				break;
			case TK_COUNTER:
			{	//bonus from SG_FRIEND [Komurka]
				int level;
				if(sd->status.party_id>0 && (level = pc_checkskill(sd,SG_FRIEND)))
					party_skill_check(sd, sd->status.party_id, TK_COUNTER,level);
			}
				break;
			case SL_STIN:
			case SL_STUN:
				if (skilllv >= 7 && !sd->sc.data[SC_SMA])
					sc_start(src,SC_SMA,100,skilllv,skill_get_time(SL_SMA, skilllv));
				break;
			case GS_FULLBUSTER:
				//Can't attack nor use items until skill's delay expires. [Skotlex]
				sd->ud.attackabletime = sd->canuseitem_tick = sd->ud.canact_tick;
				break;
		}	//Switch End
		if (flag) { //Possible to chain
			flag = DIFF_TICK(sd->ud.canact_tick, tick);
			if (flag < 1) flag = 1;
			sc_start2(src,SC_COMBO,100,skillid,bl->id,flag);
			clif_combo_delay(src, flag);
		}
	}

	//Display damage.
	switch( skillid )
	{
	case PA_GOSPEL: //Should look like Holy Cross [Skotlex]
		dmg.dmotion = clif_skill_damage(dsrc,bl,tick,dmg.amotion,dmg.dmotion, damage, dmg.div_, CR_HOLYCROSS, -1, 5);
		break;
	//Skills that need be passed as a normal attack for the client to display correctly.
	case HVAN_EXPLOSION:
	case NPC_SELFDESTRUCTION:
		if(src->type==BL_PC)
			dmg.blewcount = 10;
		dmg.amotion = 0; //Disable delay or attack will do no damage since source is dead by the time it takes effect. [Skotlex]
		// fall through
	case KN_AUTOCOUNTER:
	case NPC_CRITICALSLASH:
	case TF_DOUBLE:
	case GS_CHAINACTION:
		dmg.dmotion = clif_damage(src,bl,tick,dmg.amotion,dmg.dmotion,damage,dmg.div_,dmg.type,dmg.damage2);
		break;

	case AS_SPLASHER:
		if( flag&SD_ANIMATION ) // the surrounding targets
			dmg.dmotion = clif_skill_damage(dsrc,bl,tick, dmg.amotion, dmg.dmotion, damage, dmg.div_, skillid, -1, 5); // needs -1 as skill level
		else // the central target doesn't display an animation
			dmg.dmotion = clif_skill_damage(dsrc,bl,tick, dmg.amotion, dmg.dmotion, damage, dmg.div_, skillid, -2, 5); // needs -2(!) as skill level
		break;

	case AS_GRIMTOOTH:
		if( battle_config.anti_mayapurple_hack && sd )
		{ // Show the user position (Anti WPE Filter)
			sd->state.evade_antiwpefilter = 1;
			map_foreachinrange(clif_insight_tbl2bl, src, AREA_SIZE, BL_PC, src);
		}
	default:
		if( flag&SD_ANIMATION && dmg.div_ < 2 ) //Disabling skill animation doesn't works on multi-hit.
			type = 5;
		dmg.dmotion = clif_skill_damage(dsrc,bl,tick, dmg.amotion, dmg.dmotion, damage, dmg.div_, skillid, flag&SD_LEVEL?-1:skilllv, type);
		if( battle_config.anti_mayapurple_hack && sd && skillid == AS_GRIMTOOTH )
		{ // Hide the user again (Anti WPE Filter)
			sd->state.evade_antiwpefilter = 0;
			if( sd->sc.option&(OPTION_HIDE|OPTION_CLOAK) )
				clif_clearunit_invisible(src);
		}
		break;
	}

	map_freeblock_lock();

	if(damage > 0 && dmg.flag&BF_SKILL && tsd
		&& pc_checkskill(tsd,RG_PLAGIARISM)
	  	&& (!sc || !sc->data[SC_PRESERVE])
		&& damage < tsd->battle_status.hp)
	{	//Updated to not be able to copy skills if the blow will kill you. [Skotlex]
		if ((tsd->status.skill[skillid].id == 0 || tsd->status.skill[skillid].flag == SKILL_FLAG_PLAGIARIZED) &&
			can_copy(tsd,skillid,bl))	// Split all the check into their own function [Aru]
		{
			int lv = skilllv;
			if (tsd->cloneskill_id && tsd->status.skill[tsd->cloneskill_id].flag == SKILL_FLAG_PLAGIARIZED){
				tsd->status.skill[tsd->cloneskill_id].id = 0;
				tsd->status.skill[tsd->cloneskill_id].lv = 0;
				tsd->status.skill[tsd->cloneskill_id].flag = 0;
				clif_deleteskill(tsd,tsd->cloneskill_id);
			}

			if ((type = pc_checkskill(tsd,RG_PLAGIARISM)) < lv)
				lv = type;

			tsd->cloneskill_id = skillid;
			pc_setglobalreg(tsd, "CLONE_SKILL", skillid);
			pc_setglobalreg(tsd, "CLONE_SKILL_LV", lv);

			tsd->status.skill[skillid].id = skillid;
			tsd->status.skill[skillid].lv = lv;
			tsd->status.skill[skillid].flag = SKILL_FLAG_PLAGIARIZED;
			clif_addskill(tsd,skillid);
		}
	}
	if( skillid != WZ_SIGHTRASHER &&
		skillid != WZ_SIGHTBLASTER &&
		skillid != AC_SHOWER && skillid != MA_SHOWER &&
		skillid != SM_MAGNUM && skillid != MS_MAGNUM &&
		bl->type == BL_SKILL && damage > 0 )
	{
		struct skill_unit* su = (struct skill_unit*)bl;
		if (su->group && skill_get_inf2(su->group->skill_id)&INF2_TRAP)
			damage = 0; //Sight rasher, blaster, and arrow shower may dmg traps. [Kevin]
	}

	if (dmg.dmg_lv >= ATK_MISS && (type = skill_get_walkdelay(skillid, skilllv)) > 0)
	{	//Skills with can't walk delay also stop normal attacking for that
		//duration when the attack connects. [Skotlex]
		struct unit_data *ud = unit_bl2ud(src);
		if (ud && DIFF_TICK(ud->attackabletime, tick + type) < 0)
			ud->attackabletime = tick + type;
	}

	if( !dmg.amotion )
	{ //Instant damage
		status_fix_damage(src,bl,damage,dmg.dmotion,skillid); //Deal damage before knockback to allow stuff like firewall+storm gust combo.
		if( !status_isdead(bl) )
			skill_additional_effect(src,bl,skillid,skilllv,dmg.flag,dmg.dmg_lv,tick);
		if( damage > 0 ) //Counter status effects [Skotlex]
			skill_counter_additional_effect(src,bl,skillid,skilllv,dmg.flag,tick);
	}

	//Only knockback if it's still alive, otherwise a "ghost" is left behind. [Skotlex]
	//Reflected spells do not bounce back (bl == dsrc since it only happens for direct skills)
	if (dmg.blewcount > 0 && bl!=dsrc && !status_isdead(bl))
	{
		int direction = -1; // default
		switch(skillid)
		{
			case MG_FIREWALL:  direction = unit_getdir(bl); break; // backwards
			case WZ_STORMGUST: direction = rand()%8;        break; // randomly
			case PR_SANCTUARY: direction = unit_getdir(bl); break; // backwards
		}
		skill_blown(dsrc,bl,dmg.blewcount,direction,0);
	}

	//Delayed damage must be dealt after the knockback (it needs to know actual position of target)
	if (dmg.amotion)
		battle_delay_damage(tick,dmg.amotion,src,bl,dmg.flag,skillid,skilllv,damage,dmg.dmg_lv,dmg.dmotion,reflected);

	if(skillid == RG_INTIMIDATE && damage > 0 && !(tstatus->mode&MD_BOSS)) {
		int rate = 50 + skilllv * 5;
		rate = rate + (status_get_lv(src) - status_get_lv(bl));
		if(rand()%100 < rate)
			skill_addtimerskill(src,tick + 800,bl->id,0,0,skillid,skilllv,0,flag);
	}

	if(skillid == CR_GRANDCROSS || skillid == NPC_GRANDDARKNESS)
		dmg.flag |= BF_WEAPON;

	if( sd && dmg.flag&BF_WEAPON && src != bl && ( src == dsrc || ( dsrc->type == BL_SKILL && ( skillid == SG_SUN_WARM || skillid == SG_MOON_WARM || skillid == SG_STAR_WARM ) ) )  && damage > 0 )
	{
		if (battle_config.left_cardfix_to_right)
			battle_drain(sd, bl, dmg.damage, dmg.damage, tstatus->race, tstatus->mode&MD_BOSS);
		else
			battle_drain(sd, bl, dmg.damage, dmg.damage2, tstatus->race, tstatus->mode&MD_BOSS);
	}

	if( rdamage > 0 )
	{
		if( dmg.amotion )
			battle_delay_damage(tick,dmg.amotion,bl,src,0,0,0,rdamage,ATK_DEF,0,true);
		else
			status_fix_damage(bl,src,rdamage,0,0);
		clif_damage(src,src,tick, dmg.amotion,0,rdamage,dmg.div_>1?dmg.div_:1,4,0);
		//Use Reflect Shield to signal this kind of skill trigger. [Skotlex]
		if( tsd && src != bl )
			battle_drain(tsd, src, rdamage, rdamage, sstatus->race, is_boss(src));
		skill_additional_effect(bl, src, CR_REFLECTSHIELD, 1, BF_WEAPON|BF_SHORT|BF_NORMAL,ATK_DEF,tick);
	}

	if (!(flag&2) &&
		(
			skillid == MG_COLDBOLT || skillid == MG_FIREBOLT || skillid == MG_LIGHTNINGBOLT
		) &&
		(sc = status_get_sc(src)) &&
		sc->data[SC_DOUBLECAST] &&
		rand() % 100 < sc->data[SC_DOUBLECAST]->val2)
	{
		skill_addtimerskill(src, tick + dmg.amotion, bl->id, 0, 0, skillid, skilllv, BF_MAGIC, flag|2);
	}

	map_freeblock_unlock();

	return damage;
}

/*==========================================
 * スキル範??U?用(map_foreachinareaから呼ばれる)
 * flagについて?F16?i?を確認
 * MSB <- 00fTffff ->LSB
 *	T	=タ?ゲット選?用(BCT_*)
 *  ffff=自由に使用可能
 *  0	=予約?B0に固定
 *------------------------------------------*/
static int skill_area_temp[8];
typedef int (*SkillFunc)(struct block_list *, struct block_list *, int, int, unsigned int, int);
int skill_area_sub (struct block_list *bl, va_list ap)
{
	struct block_list *src;
	int skill_id,skill_lv,flag;
	unsigned int tick;
	SkillFunc func;

	nullpo_ret(bl);

	src=va_arg(ap,struct block_list *);
	skill_id=va_arg(ap,int);
	skill_lv=va_arg(ap,int);
	tick=va_arg(ap,unsigned int);
	flag=va_arg(ap,int);
	func=va_arg(ap,SkillFunc);

	if(battle_check_target(src,bl,flag) > 0)
	{
		// several splash skills need this initial dummy packet to display correctly
		if (flag&SD_PREAMBLE && skill_area_temp[2] == 0)
			clif_skill_damage(src,bl,tick, status_get_amotion(src), 0, -30000, 1, skill_id, skill_lv, 6);

		if (flag&(SD_SPLASH|SD_PREAMBLE))
			skill_area_temp[2]++;

		return func(src,bl,skill_id,skill_lv,tick,flag);
	}
	return 0;
}

static int skill_check_unit_range_sub (struct block_list *bl, va_list ap)
{
	struct skill_unit *unit;
	int skillid,g_skillid;

	unit = (struct skill_unit *)bl;

	if(bl->prev == NULL || bl->type != BL_SKILL)
		return 0;

	if(!unit->alive)
		return 0;

	skillid = va_arg(ap,int);
	g_skillid = unit->group->skill_id;

	switch (skillid)
	{
		case MG_SAFETYWALL:
		case AL_PNEUMA:
			if(g_skillid != MG_SAFETYWALL && g_skillid != AL_PNEUMA)
				return 0;
			break;
		case AL_WARP:
		case HT_SKIDTRAP:
		case MA_SKIDTRAP:
		case HT_LANDMINE:
		case MA_LANDMINE:
		case HT_ANKLESNARE:
		case HT_SHOCKWAVE:
		case HT_SANDMAN:
		case MA_SANDMAN:
		case HT_FLASHER:
		case HT_FREEZINGTRAP:
		case MA_FREEZINGTRAP:
		case HT_BLASTMINE:
		case HT_CLAYMORETRAP:
		case HT_TALKIEBOX:
		case HP_BASILICA:
			//Non stackable on themselves and traps (including venom dust which does not has the trap inf2 set)
			if (skillid != g_skillid && !(skill_get_inf2(g_skillid)&INF2_TRAP) && g_skillid != AS_VENOMDUST)
				return 0;
			break;
		default: //Avoid stacking with same kind of trap. [Skotlex]
			if (g_skillid != skillid)
				return 0;
			break;
	}

	return 1;
}

static int skill_check_unit_range (struct block_list *bl, int x, int y, int skillid, int skilllv)
{
	//Non players do not check for the skill's splash-trigger area.
	int range = bl->type==BL_PC?skill_get_unit_range(skillid, skilllv):0;
	int layout_type = skill_get_unit_layout_type(skillid,skilllv);
	if (layout_type==-1 || layout_type>MAX_SQUARE_LAYOUT) {
		ShowError("skill_check_unit_range: unsupported layout type %d for skill %d\n",layout_type,skillid);
		return 0;
	}

	range += layout_type;
	return map_foreachinarea(skill_check_unit_range_sub,bl->m,x-range,y-range,x+range,y+range,BL_SKILL,skillid);
}

static int skill_check_unit_range2_sub (struct block_list *bl, va_list ap)
{
	int skillid;

	if(bl->prev == NULL)
		return 0;

	skillid = va_arg(ap,int);

	if( status_isdead(bl) && skillid != AL_WARP )
		return 0;

	if( skillid == HP_BASILICA && bl->type == BL_PC )
		return 0;

	if( skillid == AM_DEMONSTRATION && bl->type == BL_MOB && ((TBL_MOB*)bl)->class_ == MOBID_EMPERIUM )
		return 0; //Allow casting Bomb/Demonstration Right under emperium [Skotlex]
	return 1;
}

static int skill_check_unit_range2 (struct block_list *bl, int x, int y, int skillid, int skilllv)
{
	int range, type;

	switch (skillid) {	// to be expanded later
	case WZ_ICEWALL:
		range = 2;
		break;
	default:
		{
			int layout_type = skill_get_unit_layout_type(skillid,skilllv);
			if (layout_type==-1 || layout_type>MAX_SQUARE_LAYOUT) {
				ShowError("skill_check_unit_range2: unsupported layout type %d for skill %d\n",layout_type,skillid);
				return 0;
			}
			range = skill_get_unit_range(skillid,skilllv) + layout_type;
		}
		break;
	}

	// if the caster is a monster/NPC, only check for players
	// otherwise just check characters
	if (bl->type == BL_PC)
		type = BL_CHAR;
	else
		type = BL_PC;

	return map_foreachinarea(skill_check_unit_range2_sub, bl->m,
		x - range, y - range, x + range, y + range,
		type, skillid);
}

int skill_guildaura_sub (struct map_session_data* sd, int id, int strvit, int agidex)
{
	if(id == sd->bl.id && battle_config.guild_aura&16)
		return 0;  // Do not affect guild leader

	if (sd->sc.data[SC_GUILDAURA]) {
		struct status_change_entry *sce = sd->sc.data[SC_GUILDAURA];
		if (sce->val3 != strvit || sce->val4 != agidex) {
			sce->val3 = strvit;
			sce->val4 = agidex;
			status_calc_bl(&sd->bl, StatusChangeFlagTable[SC_GUILDAURA]);
		}
		return 0;
	}
	sc_start4(&sd->bl, SC_GUILDAURA,100, 1, id, strvit, agidex, 1000);
	return 1;
}

/*==========================================
 * Checks that you have the requirements for casting a skill for homunculus/mercenary.
 * Flag:
 * &1: finished casting the skill (invoke hp/sp/item consumption)
 * &2: picked menu entry (Warp Portal, Teleport and other menu based skills)
 *------------------------------------------*/
static int skill_check_condition_mercenary(struct block_list *bl, int skill, int lv, int type)
{
	struct status_data *status;
	struct map_session_data *sd = NULL;
	int i, j, hp, sp, hp_rate, sp_rate, state, mhp;
	int itemid[MAX_SKILL_ITEM_REQUIRE],amount[ARRAYLENGTH(itemid)],index[ARRAYLENGTH(itemid)];

	if( lv < 1 || lv > MAX_SKILL_LEVEL )
		return 0;
	nullpo_ret(bl);

	switch( bl->type )
	{
		case BL_HOM: sd = ((TBL_HOM*)bl)->master; break;
		case BL_MER: sd = ((TBL_MER*)bl)->master; break;
	}

	status = status_get_status_data(bl);
	if( (j = skill_get_index(skill)) == 0 )
		return 0;

	// Requeriments
	for( i = 0; i < ARRAYLENGTH(itemid); i++ )
	{
		itemid[i] = skill_db[j].itemid[i];
		amount[i] = skill_db[j].amount[i];
	}
	hp = skill_db[j].hp[lv-1];
	sp = skill_db[j].sp[lv-1];
	hp_rate = skill_db[j].hp_rate[lv-1];
	sp_rate = skill_db[j].sp_rate[lv-1];
	state = skill_db[j].state;
	if( (mhp = skill_db[j].mhp[lv-1]) > 0 )
		hp += (status->max_hp * mhp) / 100;
	if( hp_rate > 0 )
		hp += (status->hp * hp_rate) / 100;
	else
		hp += (status->max_hp * (-hp_rate)) / 100;
	if( sp_rate > 0 )
		sp += (status->sp * sp_rate) / 100;
	else
		sp += (status->max_sp * (-sp_rate)) / 100;

	if( bl->type == BL_HOM )
	{ // Intimacy Requeriments
		struct homun_data *hd = BL_CAST(BL_HOM, bl);
		switch( skill )
		{
			case HFLI_SBR44:
				if( hd->homunculus.intimacy <= 200 )
					return 0;
				break;
			case HVAN_EXPLOSION:
				if( hd->homunculus.intimacy < (unsigned int)battle_config.hvan_explosion_intimate )
					return 0;
				break;
		}
	}

	if( !(type&2) )
	{
		if( hp > 0 && status->hp <= (unsigned int)hp )
		{
			clif_skill_fail(sd, skill, USESKILL_FAIL_HP_INSUFFICIENT, 0);
			return 0;
		}
		if( sp > 0 && status->sp <= (unsigned int)sp )
		{
			clif_skill_fail(sd, skill, USESKILL_FAIL_SP_INSUFFICIENT, 0);
			return 0;
		}
	}

	if( !type )
		switch( state )
		{
			case ST_MOVE_ENABLE:
				if( !unit_can_move(bl) )
				{
					clif_skill_fail(sd, skill, USESKILL_FAIL_LEVEL, 0);
					return 0;
				}
				break;
		}
	if( !(type&1) )
		return 1;

	// Check item existences
	for( i = 0; i < ARRAYLENGTH(itemid); i++ )
	{
		index[i] = -1;
		if( itemid[i] < 1 ) continue; // No item
		index[i] = pc_search_inventory(sd, itemid[i]);
		if( index[i] < 0 || sd->status.inventory[index[i]].amount < amount[i] )
		{
			clif_skill_fail(sd, skill, USESKILL_FAIL_LEVEL, 0);
			return 0;
		}
	}

	// Consume items
	for( i = 0; i < ARRAYLENGTH(itemid); i++ )
	{
		if( index[i] >= 0 ) pc_delitem(sd, index[i], amount[i], 0, 1, LOG_TYPE_CONSUME);
	}

	if( type&2 )
		return 1;

	if( sp || hp )
		status_zap(bl, hp, sp);

	return 1;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_area_sub_count (struct block_list *src, struct block_list *target, int skillid, int skilllv, unsigned int tick, int flag)
{
	return 1;
}

/*==========================================
 *
 *------------------------------------------*/
static int skill_timerskill(int tid, unsigned int tick, int id, intptr_t data)
{
	struct block_list *src = map_id2bl(id),*target;
	struct unit_data *ud = unit_bl2ud(src);
	struct skill_timerskill *skl = NULL;
	int range;

	nullpo_ret(src);
	nullpo_ret(ud);
	skl = ud->skilltimerskill[data];
	nullpo_ret(skl);
	ud->skilltimerskill[data] = NULL;

	do {
		if(src->prev == NULL)
			break;
		if(skl->target_id) {
			target = map_id2bl(skl->target_id);
			if( skl->skill_id == RG_INTIMIDATE && (!target || target->prev == NULL || !check_distance_bl(src,target,AREA_SIZE)) )
				target = src; //Required since it has to warp.
			if(target == NULL)
				break;
			if(target->prev == NULL)
				break;
			if(src->m != target->m)
				break;
			if(status_isdead(src))
				break;
			if(status_isdead(target) && skl->skill_id != RG_INTIMIDATE && skl->skill_id != WZ_WATERBALL)
				break;

			switch(skl->skill_id) {
				case RG_INTIMIDATE:
					if (unit_warp(src,-1,-1,-1,CLR_TELEPORT) == 0) {
						short x,y;
						map_search_freecell(src, 0, &x, &y, 1, 1, 0);
						if (target != src && !status_isdead(target))
							unit_warp(target, -1, x, y, CLR_TELEPORT);
					}
					break;
				case BA_FROSTJOKER:
				case DC_SCREAM:
					range= skill_get_splash(skl->skill_id, skl->skill_lv);
					map_foreachinarea(skill_frostjoke_scream,skl->map,skl->x-range,skl->y-range,
						skl->x+range,skl->y+range,BL_CHAR,src,skl->skill_id,skl->skill_lv,tick);
					break;
				case NPC_EARTHQUAKE:
					skill_area_temp[0] = map_foreachinrange(skill_area_sub, src, skill_get_splash(skl->skill_id, skl->skill_lv), BL_CHAR, src, skl->skill_id, skl->skill_lv, tick, BCT_ENEMY, skill_area_sub_count);
					skill_area_temp[1] = src->id;
					skill_area_temp[2] = 0;
					map_foreachinrange(skill_area_sub, src, skill_get_splash(skl->skill_id, skl->skill_lv), splash_target(src), src, skl->skill_id, skl->skill_lv, tick, skl->flag, skill_castend_damage_id);
					if( skl->type > 1 )
						skill_addtimerskill(src,tick+250,src->id,0,0,skl->skill_id,skl->skill_lv,skl->type-1,skl->flag);
					break;
				case WZ_WATERBALL:
					if (!status_isdead(target))
						skill_attack(BF_MAGIC,src,src,target,skl->skill_id,skl->skill_lv,tick,skl->flag);
					if (skl->type>1 && !status_isdead(target)) {
						skill_addtimerskill(src,tick+125,target->id,0,0,skl->skill_id,skl->skill_lv,skl->type-1,skl->flag);
					} else {
						struct status_change *sc = status_get_sc(src);
						if(sc) {
							status_change_end(src, SC_MAGICPOWER, INVALID_TIMER);
							if(sc->data[SC_SPIRIT] &&
								sc->data[SC_SPIRIT]->val2 == SL_WIZARD &&
								sc->data[SC_SPIRIT]->val3 == skl->skill_id)
								sc->data[SC_SPIRIT]->val3 = 0; //Clear bounced spell check.
						}
					}
					break;
				default:
					skill_attack(skl->type,src,src,target,skl->skill_id,skl->skill_lv,tick,skl->flag);
					break;
			}
		}
		else {
			if(src->m != skl->map)
				break;
			switch( skl->skill_id )
			{
				case WZ_METEOR:
					if( skl->type >= 0 )
					{
						int x = skl->type>>16, y = skl->type&0xFFFF;
						if( path_search_long(NULL, src->m, src->x, src->y, x, y, CELL_CHKWALL) )
							skill_unitsetting(src,skl->skill_id,skl->skill_lv,x,y,skl->flag);
						if( path_search_long(NULL, src->m, src->x, src->y, skl->x, skl->y, CELL_CHKWALL) )
							clif_skill_poseffect(src,skl->skill_id,skl->skill_lv,skl->x,skl->y,tick);
					}
					else if( path_search_long(NULL, src->m, src->x, src->y, skl->x, skl->y, CELL_CHKWALL) )
						skill_unitsetting(src,skl->skill_id,skl->skill_lv,skl->x,skl->y,skl->flag);
					break;
			}
		}
	} while (0);
	//Free skl now that it is no longer needed.
	ers_free(skill_timer_ers, skl);
	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_addtimerskill (struct block_list *src, unsigned int tick, int target, int x,int y, int skill_id, int skill_lv, int type, int flag)
{
	int i;
	struct unit_data *ud;
	nullpo_retr(1, src);
	ud = unit_bl2ud(src);
	nullpo_retr(1, ud);

	ARR_FIND( 0, MAX_SKILLTIMERSKILL, i, ud->skilltimerskill[i] == 0 );
	if( i == MAX_SKILLTIMERSKILL ) return 1;

	ud->skilltimerskill[i] = ers_alloc(skill_timer_ers, struct skill_timerskill);
	ud->skilltimerskill[i]->timer = add_timer(tick, skill_timerskill, src->id, i);
	ud->skilltimerskill[i]->src_id = src->id;
	ud->skilltimerskill[i]->target_id = target;
	ud->skilltimerskill[i]->skill_id = skill_id;
	ud->skilltimerskill[i]->skill_lv = skill_lv;
	ud->skilltimerskill[i]->map = src->m;
	ud->skilltimerskill[i]->x = x;
	ud->skilltimerskill[i]->y = y;
	ud->skilltimerskill[i]->type = type;
	ud->skilltimerskill[i]->flag = flag;
	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_cleartimerskill (struct block_list *src)
{
	int i;
	struct unit_data *ud;
	nullpo_ret(src);
	ud = unit_bl2ud(src);
	nullpo_ret(ud);

	for(i=0;i<MAX_SKILLTIMERSKILL;i++) {
		if(ud->skilltimerskill[i]) {
			delete_timer(ud->skilltimerskill[i]->timer, skill_timerskill);
			ers_free(skill_timer_ers, ud->skilltimerskill[i]);
			ud->skilltimerskill[i]=NULL;
		}
	}
	return 1;
}

static int skill_reveal_trap (struct block_list *bl, va_list ap)
{
	TBL_SKILL *su = (TBL_SKILL*)bl;
	if (su->alive && su->group && skill_get_inf2(su->group->skill_id)&INF2_TRAP)
	{	//Reveal trap.
		//Change look is not good enough, the client ignores it as an actual trap still. [Skotlex]
		//clif_changetraplook(bl, su->group->unit_id);
		clif_skill_setunit(su);
		return 1;
	}
	return 0;
}

/*==========================================
 *
 *
 *------------------------------------------*/
int skill_castend_damage_id (struct block_list* src, struct block_list *bl, int skillid, int skilllv, unsigned int tick, int flag)
{
	struct map_session_data *sd = NULL;
	struct status_data *tstatus;
	struct status_change *sc;

	if (skillid > 0 && skilllv <= 0) return 0;

	nullpo_retr(1, src);
	nullpo_retr(1, bl);

	if (src->m != bl->m)
		return 1;

	if (bl->prev == NULL)
		return 1;

	sd = BL_CAST(BL_PC, src);

	if (status_isdead(bl))
		return 1;

	if (skillid && skill_get_type(skillid) == BF_MAGIC && status_isimmune(bl) == 100)
	{	//GTB makes all targetted magic display miss with a single bolt.
		sc_type sct = status_skill2sc(skillid);
		if(sct != SC_NONE)
			status_change_end(bl, sct, INVALID_TIMER);
		clif_skill_damage(src, bl, tick, status_get_amotion(src), status_get_dmotion(bl), 0, 1, skillid, skilllv, skill_get_hit(skillid));
		return 1;
	}

	sc = status_get_sc(src);
	if (sc && !sc->count)
		sc = NULL; //Unneeded

	tstatus = status_get_status_data(bl);

	map_freeblock_lock();

	switch(skillid)
	{
	case MER_CRASH:
	case SM_BASH:
	case MS_BASH:
	case MC_MAMMONITE:
	case TF_DOUBLE:
	case AC_DOUBLE:
	case MA_DOUBLE:
	case AS_SONICBLOW:
	case KN_PIERCE:
	case ML_PIERCE:
	case KN_SPEARBOOMERANG:
	case TF_POISON:
	case TF_SPRINKLESAND:
	case AC_CHARGEARROW:
	case MA_CHARGEARROW:
	case RG_INTIMIDATE:
	case AM_ACIDTERROR:
	case BA_MUSICALSTRIKE:
	case DC_THROWARROW:
	case BA_DISSONANCE:
	case CR_HOLYCROSS:
	case NPC_DARKCROSS:
	case CR_SHIELDCHARGE:
	case CR_SHIELDBOOMERANG:
	case NPC_PIERCINGATT:
	case NPC_MENTALBREAKER:
	case NPC_RANGEATTACK:
	case NPC_CRITICALSLASH:
	case NPC_COMBOATTACK:
	case NPC_GUIDEDATTACK:
	case NPC_POISON:
	case NPC_RANDOMATTACK:
	case NPC_WATERATTACK:
	case NPC_GROUNDATTACK:
	case NPC_FIREATTACK:
	case NPC_WINDATTACK:
	case NPC_POISONATTACK:
	case NPC_HOLYATTACK:
	case NPC_DARKNESSATTACK:
	case NPC_TELEKINESISATTACK:
	case NPC_UNDEADATTACK:
	case NPC_ARMORBRAKE:
	case NPC_WEAPONBRAKER:
	case NPC_HELMBRAKE:
	case NPC_SHIELDBRAKE:
	case NPC_BLINDATTACK:
	case NPC_SILENCEATTACK:
	case NPC_STUNATTACK:
	case NPC_PETRIFYATTACK:
	case NPC_CURSEATTACK:
	case NPC_SLEEPATTACK:
	case LK_AURABLADE:
	case LK_SPIRALPIERCE:
	case ML_SPIRALPIERCE:
	case LK_HEADCRUSH:
	case CG_ARROWVULCAN:
	case HW_MAGICCRASHER:
	case ITM_TOMAHAWK:
	case MO_TRIPLEATTACK:
	case CH_CHAINCRUSH:
	case CH_TIGERFIST:
	case PA_SHIELDCHAIN:	// Shield Chain
	case PA_SACRIFICE:
	case WS_CARTTERMINATION:	// Cart Termination
	case AS_VENOMKNIFE:
	case HT_PHANTASMIC:
	case HT_POWER:
	case TK_DOWNKICK:
	case TK_COUNTER:
	case GS_CHAINACTION:
	case GS_TRIPLEACTION:
	case GS_MAGICALBULLET:
	case GS_TRACKING:
	case GS_PIERCINGSHOT:
	case GS_RAPIDSHOWER:
	case GS_DUST:
	case GS_DISARM:				// Added disarm. [Reddozen]
	case GS_FULLBUSTER:
	case NJ_SYURIKEN:
	case NJ_KUNAI:
	case ASC_BREAKER:
	case HFLI_MOON:	//[orn]
	case HFLI_SBR44:	//[orn]
	case NPC_BLEEDING:
	case NPC_CRITICALWOUND:
	case NPC_HELLPOWER:
		skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
		break;

	case LK_JOINTBEAT: // decide the ailment first (affects attack damage and effect)
		switch( rand()%6 ){
		case 0: flag |= BREAK_ANKLE; break;
		case 1: flag |= BREAK_WRIST; break;
		case 2: flag |= BREAK_KNEE; break;
		case 3: flag |= BREAK_SHOULDER; break;
		case 4: flag |= BREAK_WAIST; break;
		case 5: flag |= BREAK_NECK; break;
		}
		//TODO: is there really no cleaner way to do this?
		sc = status_get_sc(bl);
		if (sc) sc->jb_flag = flag;
		skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
		break;

	case MO_COMBOFINISH:
		if (!(flag&1) && sc && sc->data[SC_SPIRIT] && sc->data[SC_SPIRIT]->val2 == SL_MONK)
		{	//Becomes a splash attack when Soul Linked.
			map_foreachinrange(skill_area_sub, bl,
				skill_get_splash(skillid, skilllv),splash_target(src),
				src,skillid,skilllv,tick, flag|BCT_ENEMY|1,
				skill_castend_damage_id);
		} else
			skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
		break;

	case TK_STORMKICK: // Taekwon kicks [Dralnu]
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		skill_area_temp[1] = 0;
		map_foreachinrange(skill_attack_area, src,
			skill_get_splash(skillid, skilllv), splash_target(src),
			BF_WEAPON, src, src, skillid, skilllv, tick, flag, BCT_ENEMY);
		break;

	case KN_CHARGEATK:
		{
		bool path = path_search_long(NULL, src->m, src->x, src->y, bl->x, bl->y,CELL_CHKWALL);
		unsigned int dist = distance_bl(src, bl);
		unsigned int dir = map_calc_dir(bl, src->x, src->y);

		// teleport to target (if not on WoE grounds)
		if( !map_flag_gvg(src->m) && !map[src->m].flag.battleground && unit_movepos(src, bl->x, bl->y, 0, 1) )
			clif_slide(src, bl->x, bl->y);

		// cause damage and knockback if the path to target was a straight one
		if( path )
		{
			skill_attack(BF_WEAPON, src, src, bl, skillid, skilllv, tick, dist);
			skill_blown(src, bl, dist, dir, 0);
			//HACK: since knockback officially defaults to the left, the client also turns to the left... therefore,
			// make the caster look in the direction of the target
			unit_setdir(src, (dir+4)%8);
		}

		}
		break;

	case TK_JUMPKICK:
		if( unit_movepos(src, bl->x, bl->y, 1, 1) )
		{
			skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
			clif_slide(src,bl->x,bl->y);
		}
		break;

	case SN_SHARPSHOOTING:
	case MA_SHARPSHOOTING:
	case NJ_KAMAITACHI:
		//It won't shoot through walls since on castend there has to be a direct
		//line of sight between caster and target.
		skill_area_temp[1] = bl->id;
		map_foreachinpath (skill_attack_area,src->m,src->x,src->y,bl->x,bl->y,
			skill_get_splash(skillid, skilllv),skill_get_maxcount(skillid,skilllv), splash_target(src),
			skill_get_type(skillid),src,src,skillid,skilllv,tick,flag,BCT_ENEMY);
		break;

	case NPC_ACIDBREATH:
	case NPC_DARKNESSBREATH:
	case NPC_FIREBREATH:
	case NPC_ICEBREATH:
	case NPC_THUNDERBREATH:
		skill_area_temp[1] = bl->id;
		map_foreachinpath(skill_attack_area,src->m,src->x,src->y,bl->x,bl->y,
			skill_get_splash(skillid, skilllv),skill_get_maxcount(skillid,skilllv), splash_target(src),
			skill_get_type(skillid),src,src,skillid,skilllv,tick,flag,BCT_ENEMY);
		break;

	case MO_INVESTIGATE:
		skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
		status_change_end(src, SC_BLADESTOP, INVALID_TIMER);
		break;

	case RG_BACKSTAP:
		{
			int dir = map_calc_dir(src, bl->x, bl->y), t_dir = unit_getdir(bl);
			if ((!check_distance_bl(src, bl, 0) && !map_check_dir(dir, t_dir)) || bl->type == BL_SKILL) {
				status_change_end(src, SC_HIDING, INVALID_TIMER);
				skill_attack(BF_WEAPON, src, src, bl, skillid, skilllv, tick, flag);
				dir = dir < 4 ? dir+4 : dir-4; // change direction [Celest]
				unit_setdir(bl,dir);
			}
			else if (sd)
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		}
		break;

	case MO_FINGEROFFENSIVE:
		skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
		if (battle_config.finger_offensive_type && sd) {
			int i;
			for (i = 1; i < sd->spiritball_old; i++)
				skill_addtimerskill(src, tick + i * 200, bl->id, 0, 0, skillid, skilllv, BF_WEAPON, flag);
		}
		status_change_end(src, SC_BLADESTOP, INVALID_TIMER);
		break;

	case MO_CHAINCOMBO:
		skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
		status_change_end(src, SC_BLADESTOP, INVALID_TIMER);
		break;

	case NJ_ISSEN:
		status_change_end(src, SC_NEN, INVALID_TIMER);
		status_change_end(src, SC_HIDING, INVALID_TIMER);
		// fall through
	case MO_EXTREMITYFIST:
		if( skillid == MO_EXTREMITYFIST )
		{
			status_change_end(src, SC_EXPLOSIONSPIRITS, INVALID_TIMER);
			status_change_end(src, SC_BLADESTOP, INVALID_TIMER);
		}
		//Client expects you to move to target regardless of distance
		{
			struct unit_data *ud = unit_bl2ud(src);
			short dx,dy;
			int i,speed;
			i = skillid == MO_EXTREMITYFIST?1:2; //Move 2 cells for Issen, 1 for Asura
			dx = bl->x - src->x;
			dy = bl->y - src->y;
			if (dx < 0) dx-=i;
			else if (dx > 0) dx+=i;
			if (dy < 0) dy-=i;
			else if (dy > 0) dy+=i;
			if (!dx && !dy) dy++;
			if (map_getcell(src->m, src->x+dx, src->y+dy, CELL_CHKNOPASS))
			{
				dx = bl->x;
				dy = bl->y;
			} else {
				dx = src->x + dx;
				dy = src->y + dy;
			}

			skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);

			if(unit_walktoxy(src, dx, dy, 2) && ud) {
				//Increase can't walk delay to not alter your walk path
				ud->canmove_tick = tick;
				speed = status_get_speed(src);
				for (i = 0; i < ud->walkpath.path_len; i ++)
				{
					if(ud->walkpath.path[i]&1)
						ud->canmove_tick+=7*speed/5;
					else
						ud->canmove_tick+=speed;
				}
			}
		}
		break;

	//Splash attack skills.
	case AS_GRIMTOOTH:
	case MC_CARTREVOLUTION:
	case NPC_SPLASHATTACK:
		flag |= SD_PREAMBLE; // a fake packet will be sent for the first target to be hit
	case AS_SPLASHER:
	case SM_MAGNUM:
	case MS_MAGNUM:
	case HT_BLITZBEAT:
	case AC_SHOWER:
	case MA_SHOWER:
	case MG_NAPALMBEAT:
	case MG_FIREBALL:
	case RG_RAID:
	case HW_NAPALMVULCAN:
	case NJ_HUUMA:
	case NJ_BAKUENRYU:
	case ASC_METEORASSAULT:
	case GS_DESPERADO:
	case GS_SPREADATTACK:
	case NPC_EARTHQUAKE:
	case NPC_PULSESTRIKE:
	case NPC_HELLJUDGEMENT:
	case NPC_VAMPIRE_GIFT:
		if( flag&1 )
		{	//Recursive invocation
			// skill_area_temp[0] holds number of targets in area
			// skill_area_temp[1] holds the id of the original target
			// skill_area_temp[2] counts how many targets have already been processed
			int sflag = skill_area_temp[0] & 0xFFF, heal;
			if( flag&SD_LEVEL )
				sflag |= SD_LEVEL; // -1 will be used in packets instead of the skill level
			if( skill_area_temp[1] != bl->id && !(skill_get_inf2(skillid)&INF2_NPC_SKILL) )
				sflag |= SD_ANIMATION; // original target gets no animation (as well as all NPC skills)

			heal = skill_attack(skill_get_type(skillid), src, src, bl, skillid, skilllv, tick, sflag);
			if( skillid == NPC_VAMPIRE_GIFT && heal > 0 )
			{
				clif_skill_nodamage(NULL, src, AL_HEAL, heal, 1);
				status_heal(src,heal,0,0);
			}
		}
		else
		{
			if ( skillid == NJ_BAKUENRYU )
				clif_skill_nodamage(src,bl,skillid,skilllv,1);

			skill_area_temp[0] = 0;
			skill_area_temp[1] = bl->id;
			skill_area_temp[2] = 0;

			// if skill damage should be split among targets, count them
			//SD_LEVEL -> Forced splash damage for Auto Blitz-Beat -> count targets
			//special case: Venom Splasher uses a different range for searching than for splashing
			if( flag&SD_LEVEL || skill_get_nk(skillid)&NK_SPLASHSPLIT )
				skill_area_temp[0] = map_foreachinrange(skill_area_sub, bl, (skillid == AS_SPLASHER)?1:skill_get_splash(skillid, skilllv), BL_CHAR, src, skillid, skilllv, tick, BCT_ENEMY, skill_area_sub_count);

			// recursive invocation of skill_castend_damage_id() with flag|1
			map_foreachinrange(skill_area_sub, bl, skill_get_splash(skillid, skilllv), splash_target(src), src, skillid, skilllv, tick, flag|BCT_ENEMY|SD_SPLASH|1, skill_castend_damage_id);

			//FIXME: Isn't EarthQuake a ground skill after all?
			if( skillid == NPC_EARTHQUAKE )
				skill_addtimerskill(src,tick+250,src->id,0,0,skillid,skilllv,2,flag|BCT_ENEMY|SD_SPLASH|1);

			//FIXME: move this to skill_additional_effect or some such? [ultramage]
			if( skillid == SM_MAGNUM || skillid == MS_MAGNUM )
			{ // Initiate 10% of your damage becomes fire element.
				sc_start4(src,SC_WATK_ELEMENT,100,3,20,0,0,skill_get_time2(skillid, skilllv));
				if( sd )
					skill_blockpc_start(sd, skillid, skill_get_time(skillid, skilllv));
				if( bl->type == BL_MER )
					skill_blockmerc_start((TBL_MER*)bl, skillid, skill_get_time(skillid, skilllv));
			}
		}
		break;

	case KN_BRANDISHSPEAR:
	case ML_BRANDISH:
		//Coded apart for it needs the flag passed to the damage calculation.
		if (skill_area_temp[1] != bl->id)
			skill_attack(skill_get_type(skillid), src, src, bl, skillid, skilllv, tick, flag|SD_ANIMATION);
		else
			skill_attack(skill_get_type(skillid), src, src, bl, skillid, skilllv, tick, flag);
		break;

	case KN_BOWLINGBASH:
	case MS_BOWLINGBASH:
		if(flag&1){
			if(bl->id==skill_area_temp[1])
				break;
			//two hits for 500%
			skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,SD_ANIMATION);
			skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,SD_ANIMATION);
		} else {
			int i,c;
			c = skill_get_blewcount(skillid,skilllv);
			// keep moving target in the direction that src is looking, square by square
			for(i=0;i<c;i++){
				if (!skill_blown(src,bl,1,(unit_getdir(src)+4)%8,0x1))
					break; //Can't knockback
				skill_area_temp[0] = map_foreachinrange(skill_area_sub, bl, skill_get_splash(skillid, skilllv), BL_CHAR, src, skillid, skilllv, tick, flag|BCT_ENEMY, skill_area_sub_count);
				if( skill_area_temp[0] > 1 ) break; // collision
			}
			clif_blown(bl); //Update target pos.
			if (i!=c) { //Splash
				skill_area_temp[1] = bl->id;
				map_foreachinrange(skill_area_sub, bl, skill_get_splash(skillid, skilllv), splash_target(src), src, skillid, skilllv, tick, flag|BCT_ENEMY|1, skill_castend_damage_id);
			}
			//Weirdo dual-hit property, two attacks for 500%
			skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,0);
			skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,0);
		}
		break;

	case KN_SPEARSTAB:
		if(flag&1) {
			if (bl->id==skill_area_temp[1])
				break;
			if (skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,SD_ANIMATION))
				skill_blown(src,bl,skill_area_temp[2],-1,0);
		} else {
			int x=bl->x,y=bl->y,i,dir;
			dir = map_calc_dir(bl,src->x,src->y);
			skill_area_temp[1] = bl->id;
			skill_area_temp[2] = skill_get_blewcount(skillid,skilllv);
			// all the enemies between the caster and the target are hit, as well as the target
			if (skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,0))
				skill_blown(src,bl,skill_area_temp[2],-1,0);
			for (i=0;i<4;i++) {
				map_foreachincell(skill_area_sub,bl->m,x,y,BL_CHAR,
					src,skillid,skilllv,tick,flag|BCT_ENEMY|1,skill_castend_damage_id);
				x += dirx[dir];
				y += diry[dir];
			}
		}
		break;

	case TK_TURNKICK:
	case MO_BALKYOUNG: //Active part of the attack. Skill-attack [Skotlex]
	{
		skill_area_temp[1] = bl->id; //NOTE: This is used in skill_castend_nodamage_id to avoid affecting the target.
		if (skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag))
			map_foreachinrange(skill_area_sub,bl,
				skill_get_splash(skillid, skilllv),BL_CHAR,
				src,skillid,skilllv,tick,flag|BCT_ENEMY|1,
				skill_castend_nodamage_id);
	}
		break;
	case CH_PALMSTRIKE: //	Palm Strike takes effect 1sec after casting. [Skotlex]
	//	clif_skill_nodamage(src,bl,skillid,skilllv,0); //Can't make this one display the correct attack animation delay :/
		clif_damage(src,bl,tick,status_get_amotion(src),0,-1,1,4,0); //Display an absorbed damage attack.
		skill_addtimerskill(src, tick + 1000, bl->id, 0, 0, skillid, skilllv, BF_WEAPON, flag);
		break;

	case PR_TURNUNDEAD:
	case ALL_RESURRECTION:
		if (!battle_check_undead(tstatus->race, tstatus->def_ele))
			break;
		skill_attack(BF_MAGIC,src,src,bl,skillid,skilllv,tick,flag);
		break;

	case MG_SOULSTRIKE:
	case NPC_DARKSTRIKE:
	case MG_COLDBOLT:
	case MG_FIREBOLT:
	case MG_LIGHTNINGBOLT:
	case WZ_EARTHSPIKE:
	case AL_HEAL:
	case AL_HOLYLIGHT:
	case WZ_JUPITEL:
	case NPC_DARKTHUNDER:
	case PR_ASPERSIO:
	case MG_FROSTDIVER:
	case WZ_SIGHTBLASTER:
	case WZ_SIGHTRASHER:
	case NJ_KOUENKA:
	case NJ_HYOUSENSOU:
	case NJ_HUUJIN:
		skill_attack(BF_MAGIC,src,src,bl,skillid,skilllv,tick,flag);
		break;

	case NPC_MAGICALATTACK:
		skill_attack(BF_MAGIC,src,src,bl,skillid,skilllv,tick,flag);
		sc_start(src,status_skill2sc(skillid),100,skilllv,skill_get_time(skillid,skilllv));
		break;

	case HVAN_CAPRICE: //[blackhole89]
		{
			int ran=rand()%4;
			int sid = 0;
			switch(ran)
			{
			case 0: sid=MG_COLDBOLT; break;
			case 1: sid=MG_FIREBOLT; break;
			case 2: sid=MG_LIGHTNINGBOLT; break;
			case 3: sid=WZ_EARTHSPIKE; break;
			}
			skill_attack(BF_MAGIC,src,src,bl,sid,skilllv,tick,flag|SD_LEVEL);
		}
		break;
	case WZ_WATERBALL:
		skill_attack(BF_MAGIC,src,src,bl,skillid,skilllv,tick,flag);
		{
			int range = skilllv / 2;
			int maxlv = skill_get_max(skillid); // learnable level
			int count = 0;
			int x, y;
			struct skill_unit* unit;

			if( skilllv > maxlv )
			{
				if( src->type == BL_MOB && skilllv == 10 )
					range = 4;
				else
					range = maxlv / 2;
			}

			for( y = src->y - range; y <= src->y + range; ++y )
				for( x = src->x - range; x <= src->x + range; ++x )
				{
					if( !map_find_skill_unit_oncell(src,x,y,SA_LANDPROTECTOR,NULL) )
					{
						if( src->type != BL_PC || map_getcell(src->m,x,y,CELL_CHKWATER) ) // non-players bypass the water requirement
							count++; // natural water cell
						else if( (unit = map_find_skill_unit_oncell(src,x,y,SA_DELUGE,NULL)) != NULL || (unit = map_find_skill_unit_oncell(src,x,y,NJ_SUITON,NULL)) != NULL )
						{
							count++; // skill-induced water cell
							skill_delunit(unit); // consume cell
						}
					}
				}

			if( count > 1 ) // queue the remaining count - 1 timerskill Waterballs
				skill_addtimerskill(src,tick+150,bl->id,0,0,skillid,skilllv,count-1,flag);
		}
		break;

	case PR_BENEDICTIO:
		//Should attack undead and demons. [Skotlex]
		if (battle_check_undead(tstatus->race, tstatus->def_ele) || tstatus->race == RC_DEMON)
			skill_attack(BF_MAGIC, src, src, bl, skillid, skilllv, tick, flag);
	break;

	case SL_SMA:
		status_change_end(src, SC_SMA, INVALID_TIMER);
	case SL_STIN:
	case SL_STUN:
		if (sd && !battle_config.allow_es_magic_pc && bl->type != BL_MOB) {
			status_change_start(src,SC_STUN,10000,skilllv,0,0,0,500,10);
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			break;
		}
		skill_attack(BF_MAGIC,src,src,bl,skillid,skilllv,tick,flag);
		break;

	case NPC_DARKBREATH:
		clif_emotion(src,E_AG);
	case SN_FALCONASSAULT:
	case PA_PRESSURE:
	case CR_ACIDDEMONSTRATION:
	case TF_THROWSTONE:
	case NPC_SMOKING:
	case GS_FLING:
	case NJ_ZENYNAGE:
		skill_attack(BF_MISC,src,src,bl,skillid,skilllv,tick,flag);
		break;

	case HVAN_EXPLOSION:
	case NPC_SELFDESTRUCTION:
		if (src != bl)
			skill_attack(BF_MISC,src,src,bl,skillid,skilllv,tick,flag);
		break;

	// Celest
	case PF_SOULBURN:
		if (rand()%100 < (skilllv < 5 ? 30 + skilllv * 10 : 70)) {
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			if (skilllv == 5)
				skill_attack(BF_MAGIC,src,src,bl,skillid,skilllv,tick,flag);
			status_percent_damage(src, bl, 0, 100, false);
		} else {
			clif_skill_nodamage(src,src,skillid,skilllv,1);
			if (skilllv == 5)
				skill_attack(BF_MAGIC,src,src,src,skillid,skilllv,tick,flag);
			status_percent_damage(src, src, 0, 100, false);
		}
		if (sd) skill_blockpc_start (sd, skillid, (skilllv < 5 ? 10000: 15000));
		break;

	case NPC_BLOODDRAIN:
	case NPC_ENERGYDRAIN:
		{
			int heal = skill_attack( (skillid == NPC_BLOODDRAIN) ? BF_WEAPON : BF_MAGIC,
					src, src, bl, skillid, skilllv, tick, flag);
			if (heal > 0){
				clif_skill_nodamage(NULL, src, AL_HEAL, heal, 1);
				status_heal(src, heal, 0, 0);
			}
		}
		break;

	case GS_BULLSEYE:
		skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
		break;

	case NJ_KASUMIKIRI:
		if (skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag) > 0)
			sc_start(src,SC_HIDING,100,skilllv,skill_get_time(skillid,skilllv));
		break;
	case NJ_KIRIKAGE:
		if( !map_flag_gvg(src->m) && !map[src->m].flag.battleground )
		{	//You don't move on GVG grounds.
			short x, y;
			map_search_freecell(bl, 0, &x, &y, 1, 1, 0);
			if (unit_movepos(src, x, y, 0, 0))
				clif_slide(src,src->x,src->y);
		}
		status_change_end(src, SC_HIDING, INVALID_TIMER);
		skill_attack(BF_WEAPON,src,src,bl,skillid,skilllv,tick,flag);
		break;
	case 0:
		if(sd) {
			if (flag & 3){
				if (bl->id != skill_area_temp[1])
					skill_attack(BF_WEAPON, src, src, bl, skillid, skilllv, tick, SD_LEVEL|flag);
			} else {
				skill_area_temp[1] = bl->id;
				map_foreachinrange(skill_area_sub, bl,
					sd->splash_range, BL_CHAR,
					src, skillid, skilllv, tick, flag | BCT_ENEMY | 1,
					skill_castend_damage_id);
				flag|=1; //Set flag to 1 so ammo is not double-consumed. [Skotlex]
			}
		}
		break;

	default:
		ShowWarning("skill_castend_damage_id: Unknown skill used:%d\n",skillid);
		clif_skill_damage(src, bl, tick, status_get_amotion(src), tstatus->dmotion,
			0, abs(skill_get_num(skillid, skilllv)),
			skillid, skilllv, skill_get_hit(skillid));
		map_freeblock_unlock();
		return 1;
	}

	map_freeblock_unlock();

	if( sd && !(flag&1) )
	{
		if( sd->state.arrow_atk ) //Consume arrow on last invocation to this skill.
			battle_consume_ammo(sd, skillid, skilllv);
		skill_onskillusage(sd, bl, skillid, tick);
		skill_consume_requirement(sd,skillid,skilllv,2);
	}

	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_castend_nodamage_id (struct block_list *src, struct block_list *bl, int skillid, int skilllv, unsigned int tick, int flag)
{
	struct map_session_data *sd, *dstsd;
	struct mob_data *md, *dstmd;
	struct homun_data *hd;
	struct mercenary_data *mer;
	struct status_data *sstatus, *tstatus;
	struct status_change *tsc;
	struct status_change_entry *tsce;

	int i;
	enum sc_type type;

	if(skillid > 0 && skilllv <= 0) return 0;	// celest

	nullpo_retr(1, src);
	nullpo_retr(1, bl);

	if (src->m != bl->m)
		return 1;

	sd = BL_CAST(BL_PC, src);
	hd = BL_CAST(BL_HOM, src);
	md = BL_CAST(BL_MOB, src);
	mer = BL_CAST(BL_MER, src);

	dstsd = BL_CAST(BL_PC, bl);
	dstmd = BL_CAST(BL_MOB, bl);

	if(bl->prev == NULL)
		return 1;
	if(status_isdead(src))
		return 1;

	if( src != bl && status_isdead(bl) && skillid != ALL_RESURRECTION && skillid != PR_REDEMPTIO && skillid != NPC_WIDESOULDRAIN )
		return 1;

	tstatus = status_get_status_data(bl);
	sstatus = status_get_status_data(src);

	//Check for undead skills that convert a no-damage skill into a damage one. [Skotlex]
	switch (skillid) {
		case HLIF_HEAL:	//[orn]
			if (bl->type != BL_HOM) {
				if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0) ;
	        break ;
			}
 		case AL_HEAL:
		case ALL_RESURRECTION:
		case PR_ASPERSIO:
			//Apparently only player casted skills can be offensive like this.
			if (sd && battle_check_undead(tstatus->race,tstatus->def_ele)) {
				if (battle_check_target(src, bl, BCT_ENEMY) < 1) {
				  	//Offensive heal does not works on non-enemies. [Skotlex]
					clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
					return 0;
				}
				return skill_castend_damage_id (src, bl, skillid, skilllv, tick, flag);
			}
			break;
		case NPC_SMOKING: //Since it is a self skill, this one ends here rather than in damage_id. [Skotlex]
			return skill_castend_damage_id (src, bl, skillid, skilllv, tick, flag);
		default:
			//Skill is actually ground placed.
			if (src == bl && skill_get_unit_id(skillid,0))
				return skill_castend_pos2(src,bl->x,bl->y,skillid,skilllv,tick,0);
	}

	type = status_skill2sc(skillid);
	tsc = status_get_sc(bl);
	tsce = (tsc && type != -1)?tsc->data[type]:NULL;

	if (src!=bl && type > -1 &&
		(i = skill_get_ele(skillid, skilllv)) > ELE_NEUTRAL &&
		skill_get_inf(skillid) != INF_SUPPORT_SKILL &&
		battle_attr_fix(NULL, NULL, 100, i, tstatus->def_ele, tstatus->ele_lv) <= 0)
		return 1; //Skills that cause an status should be blocked if the target element blocks its element.

	map_freeblock_lock();
	switch(skillid)
	{
	case HLIF_HEAL:	//[orn]
	case AL_HEAL:
		{
			int heal = skill_calc_heal(src, bl, skillid, skilllv, true);
			int heal_get_jobexp;

			if( status_isimmune(bl) || (dstmd && dstmd->class_ == MOBID_EMPERIUM) )
				heal = 0;

			if( dstmd && mob_is_battleground(dstmd) )
				heal = 1;

			if( sd && dstsd && sd->status.partner_id == dstsd->status.char_id && (sd->class_&MAPID_UPPERMASK) == MAPID_SUPER_NOVICE && sd->status.sex == 0 )
				heal = heal*2;

			if( tsc && tsc->count )
			{
				if( tsc->data[SC_KAITE] && !(sstatus->mode&MD_BOSS) )
				{ //Bounce back heal
					if (--tsc->data[SC_KAITE]->val2 <= 0)
						status_change_end(bl, SC_KAITE, INVALID_TIMER);
					if (src == bl)
						heal=0; //When you try to heal yourself under Kaite, the heal is voided.
					else {
						bl = src;
						dstsd = sd;
					}
				} else
				if (tsc->data[SC_BERSERK])
					heal = 0; //Needed so that it actually displays 0 when healing.
			}
			heal_get_jobexp = status_heal(bl,heal,0,0);
			clif_skill_nodamage (src, bl, skillid, heal, 1);

			if( sd && dstsd && heal > 0 && sd != dstsd )
			{
				if( sd->status.guild_id && map_allowed_woe(src->m) )
				{
					if( sd->status.guild_id == dstsd->status.guild_id || (!map[src->m].flag.gvg_noalliance && guild_isallied(sd->status.guild_id, dstsd->status.guild_id)) )
						add2limit(sd->status.wstats.healing_done, heal_get_jobexp, UINT_MAX);
					else
						add2limit(sd->status.wstats.wrong_healing_done, heal_get_jobexp, UINT_MAX);
				}
				else if( map[src->m].flag.battleground && sd->bg_id && dstsd->bg_id )
				{
					if( sd->bg_id == dstsd->bg_id )
						add2limit(sd->status.bgstats.healing_done, heal_get_jobexp, UINT_MAX);
					else
						add2limit(sd->status.bgstats.wrong_healing_done, heal_get_jobexp, UINT_MAX);
				}

				if( battle_config.heal_exp > 0 )
				{
					heal_get_jobexp = heal_get_jobexp * battle_config.heal_exp / 100;
					if( heal_get_jobexp <= 0 )
						heal_get_jobexp = 1;
					pc_gainexp(sd, bl, 0, heal_get_jobexp, false);
				}
			}
		}
		break;

	case PR_REDEMPTIO:
		if (sd && !(flag&1)) {
			if (sd->status.party_id == 0) {
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			skill_area_temp[0] = 0;
			party_foreachsamemap(skill_area_sub,
				sd,skill_get_splash(skillid, skilllv),
				src,skillid,skilllv,tick, flag|BCT_PARTY|1,
				skill_castend_nodamage_id);
			if (skill_area_temp[0] == 0) {
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			skill_area_temp[0] = 5 - skill_area_temp[0]; // The actual penalty...
			if (skill_area_temp[0] > 0 && !map[src->m].flag.noexppenalty) { //Apply penalty
				sd->status.base_exp -= min(sd->status.base_exp, pc_nextbaseexp(sd) * skill_area_temp[0] * 2/1000); //0.2% penalty per each.
				sd->status.job_exp -= min(sd->status.job_exp, pc_nextjobexp(sd) * skill_area_temp[0] * 2/1000);
				pc_onstatuschanged(sd,SP_BASEEXP);
				pc_onstatuschanged(sd,SP_JOBEXP);
			}
			status_set_hp(src, 1, 0);
			status_set_sp(src, 0, 0);
			break;
		} else if (status_isdead(bl) && flag&1) { //Revive
			skill_area_temp[0]++; //Count it in, then fall-through to the Resurrection code.
			skilllv = 3; //Resurrection level 3 is used
		} else //Invalid target, skip resurrection.
			break;

	case ALL_RESURRECTION:
		if( sd && map_flag_gvg3(bl->m) )
		{	//No reviving in WoE grounds!
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			break;
		}
		if (!status_isdead(bl))
			break;
		{
			int per = 0, sper = 0;
			if (tsc && tsc->data[SC_HELLPOWER])
				break;

			if (map[bl->m].flag.pvp && dstsd && dstsd->pvp_point < 0)
				break;

			switch(skilllv){
			case 1: per=10; break;
			case 2: per=30; break;
			case 3: per=50; break;
			case 4: per=80; break;
			}
			if(dstsd && dstsd->special_state.restart_full_recover)
				per = sper = 100;
			if (status_revive(bl, per, sper))
			{
				clif_skill_nodamage(src,bl,ALL_RESURRECTION,skilllv,1); //Both Redemptio and Res show this skill-animation.
				if(sd && dstsd && battle_config.resurrection_exp > 0)
				{
					int exp = 0,jexp = 0;
					int lv = dstsd->status.base_level - sd->status.base_level, jlv = dstsd->status.job_level - sd->status.job_level;
					if(lv > 0 && pc_nextbaseexp(dstsd)) {
						exp = (int)((double)dstsd->status.base_exp * (double)lv * (double)battle_config.resurrection_exp / 1000000.);
						if (exp < 1) exp = 1;
					}
					if(jlv > 0 && pc_nextjobexp(dstsd)) {
						jexp = (int)((double)dstsd->status.job_exp * (double)lv * (double)battle_config.resurrection_exp / 1000000.);
						if (jexp < 1) jexp = 1;
					}
					if(exp > 0 || jexp > 0)
						pc_gainexp (sd, bl, exp, jexp, false);
				}
			}
		}
		break;

	case AL_DECAGI:
	case MER_DECAGI:
		clif_skill_nodamage (src, bl, skillid, skilllv,
			sc_start(bl, type, (40 + skilllv * 2 + (status_get_lv(src) + sstatus->int_)/5), skilllv, skill_get_time(skillid,skilllv)));
		break;

	case AL_CRUCIS:
		if (flag&1)
			sc_start(bl,type, 23+skilllv*4 +status_get_lv(src) -status_get_lv(bl), skilllv,skill_get_time(skillid,skilllv));
		else {
			map_foreachinrange(skill_area_sub, src, skill_get_splash(skillid, skilllv), BL_CHAR,
				src, skillid, skilllv, tick, flag|BCT_ENEMY|1, skill_castend_nodamage_id);
			clif_skill_nodamage(src, bl, skillid, skilllv, 1);
		}
		break;

	case PR_LEXDIVINA:
	case MER_LEXDIVINA:
		if( tsce )
			status_change_end(bl,type, INVALID_TIMER);
		else
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv));
		clif_skill_nodamage (src, bl, skillid, skilllv, 1);
		break;

	case SA_ABRACADABRA:
		{
			int abra_skillid = 0, abra_skilllv;
			do {
				i = rand() % MAX_SKILL_ABRA_DB;
				abra_skillid = skill_abra_db[i].skillid;
			} while (abra_skillid == 0 ||
				skill_abra_db[i].req_lv > skilllv || //Required lv for it to appear
				rand()%10000 >= skill_abra_db[i].per
			);
			abra_skilllv = min(skilllv, skill_get_max(abra_skillid));
			clif_skill_nodamage (src, bl, skillid, skilllv, 1);

			if( sd )
			{// player-casted
				sd->state.abra_flag = 1;
				sd->skillitem = abra_skillid;
				sd->skillitemlv = abra_skilllv;
				clif_item_skill(sd, abra_skillid, abra_skilllv);
			}
			else
			{// mob-casted
				struct unit_data *ud = unit_bl2ud(src);
				int inf = skill_get_inf(abra_skillid);
				int target_id = 0;
				if (!ud) break;
				if (inf&INF_SELF_SKILL || inf&INF_SUPPORT_SKILL) {
					if (src->type == BL_PET)
						bl = (struct block_list*)((TBL_PET*)src)->msd;
					if (!bl) bl = src;
					unit_skilluse_id(src, bl->id, abra_skillid, abra_skilllv);
				} else {	//Assume offensive skills
					if (ud->target)
						target_id = ud->target;
					else switch (src->type) {
						case BL_MOB: target_id = ((TBL_MOB*)src)->target_id; break;
						case BL_PET: target_id = ((TBL_PET*)src)->target_id; break;
					}
					if (!target_id)
						break;
					if (skill_get_casttype(abra_skillid) == CAST_GROUND) {
						bl = map_id2bl(target_id);
						if (!bl) bl = src;
						unit_skilluse_pos(src, bl->x, bl->y, abra_skillid, abra_skilllv);
					} else
						unit_skilluse_id(src, target_id, abra_skillid, abra_skilllv);
				}
			}
		}
		break;

	case SA_COMA:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time2(skillid,skilllv)));
		break;
	case SA_FULLRECOVERY:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if (status_isimmune(bl))
			break;
		status_percent_heal(bl, 100, 100);
		break;
	case NPC_ALLHEAL:
		{
			int heal;
			if( status_isimmune(bl) )
				break;
			heal = status_percent_heal(bl, 100, 0);
			clif_skill_nodamage(NULL, bl, AL_HEAL, heal, 1);
			if( dstmd )
			{ // Reset Damage Logs
				memset(dstmd->dmglog, 0, sizeof(dstmd->dmglog));
				dstmd->tdmg = 0;
			}
		}
		break;
	case SA_SUMMONMONSTER:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if (sd) mob_once_spawn(sd,src->m,src->x,src->y,"--ja--",-1,1,"");
		break;
	case SA_LEVELUP:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if (sd && pc_nextbaseexp(sd)) pc_gainexp(sd, NULL, pc_nextbaseexp(sd) * 10 / 100, 0, false);
		break;
	case SA_INSTANTDEATH:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		status_set_hp(bl,1,0);
		break;
	case SA_QUESTION:
	case SA_GRAVITY:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;
	case SA_CLASSCHANGE:
	case SA_MONOCELL:
		if (dstmd)
		{
			int class_;
			if ( sd && dstmd->status.mode&MD_BOSS )
			{
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			class_ = skillid==SA_MONOCELL?1002:mob_get_random_id(2, 1, 0);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			mob_class_change(dstmd,class_);
			if( tsc && dstmd->status.mode&MD_BOSS )
			{
				const enum sc_type scs[] = { SC_QUAGMIRE, SC_PROVOKE, SC_ROKISWEIL, SC_GRAVITATION, SC_SUITON, SC_STRIPWEAPON, SC_STRIPSHIELD, SC_STRIPARMOR, SC_STRIPHELM, SC_BLADESTOP };
				for (i = SC_COMMON_MIN; i <= SC_COMMON_MAX; i++)
					if (tsc->data[i]) status_change_end(bl, (sc_type)i, INVALID_TIMER);
				for (i = 0; i < ARRAYLENGTH(scs); i++)
					if (tsc->data[scs[i]]) status_change_end(bl, scs[i], INVALID_TIMER);
			}
		}
		break;
	case SA_DEATH:
		if ( sd && dstmd && dstmd->status.mode&MD_BOSS )
		{
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			break;
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		status_kill(bl);
		break;
	case SA_REVERSEORCISH:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid, skilllv)));
		break;
	case SA_FORTUNE:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if(sd) pc_getzeny(sd,status_get_lv(bl)*100);
		break;
	case SA_TAMINGMONSTER:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if (sd && dstmd) {
			ARR_FIND( 0, MAX_PET_DB, i, dstmd->class_ == pet_db[i].class_ );
			if( i < MAX_PET_DB )
				pet_catch_process1(sd, dstmd->class_);
		}
		break;

	case CR_PROVIDENCE:
		if(sd && dstsd){ //Check they are not another crusader [Skotlex]
			if ((dstsd->class_&MAPID_UPPERMASK) == MAPID_CRUSADER) {
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				map_freeblock_unlock();
				return 1;
			}
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		break;

	case CG_MARIONETTE:
		{
			struct status_change* sc = status_get_sc(src);

			if( sd && dstsd && (dstsd->class_&MAPID_UPPERMASK) == MAPID_BARDDANCER && dstsd->status.sex == sd->status.sex )
			{// Cannot cast on another bard/dancer-type class of the same gender as caster
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				map_freeblock_unlock();
				return 1;
			}

			if( sc && tsc )
			{
				if( !sc->data[SC_MARIONETTE] && !tsc->data[SC_MARIONETTE2] )
				{
					sc_start(src,SC_MARIONETTE,100,bl->id,skill_get_time(skillid,skilllv));
					sc_start(bl,SC_MARIONETTE2,100,src->id,skill_get_time(skillid,skilllv));
					clif_skill_nodamage(src,bl,skillid,skilllv,1);
				}
				else
				if(  sc->data[SC_MARIONETTE ] &&  sc->data[SC_MARIONETTE ]->val1 == bl->id &&
					tsc->data[SC_MARIONETTE2] && tsc->data[SC_MARIONETTE2]->val1 == src->id )
				{
					status_change_end(src, SC_MARIONETTE, INVALID_TIMER);
					status_change_end(bl, SC_MARIONETTE2, INVALID_TIMER);
				}
				else
				{
					if( sd )
						clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);

					map_freeblock_unlock();
					return 1;
				}
			}
		}
		break;

	case RG_CLOSECONFINE:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start4(bl,type,100,skilllv,src->id,0,0,skill_get_time(skillid,skilllv)));
		break;
	case SA_FLAMELAUNCHER:	// added failure chance and chance to break weapon if turned on [Valaris]
	case SA_FROSTWEAPON:
	case SA_LIGHTNINGLOADER:
	case SA_SEISMICWEAPON:
		if (dstsd) {
			if(dstsd->status.weapon == W_FIST ||
				(dstsd->sc.count && !dstsd->sc.data[type] &&
				(	//Allow re-enchanting to lenghten time. [Skotlex]
					dstsd->sc.data[SC_FIREWEAPON] ||
					dstsd->sc.data[SC_WATERWEAPON] ||
					dstsd->sc.data[SC_WINDWEAPON] ||
					dstsd->sc.data[SC_EARTHWEAPON] ||
					dstsd->sc.data[SC_SHADOWWEAPON] ||
					dstsd->sc.data[SC_GHOSTWEAPON] ||
					dstsd->sc.data[SC_ENCPOISON]
				))
				) {
				if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				clif_skill_nodamage(src,bl,skillid,skilllv,0);
				break;
			}
		}
		// 100% success rate at lv4 & 5, but lasts longer at lv5
		if(!clif_skill_nodamage(src,bl,skillid,skilllv, sc_start(bl,type,(60+skilllv*10),skilllv, skill_get_time(skillid,skilllv)))) {
			if (sd)
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			if (skill_break_equip(bl, EQP_WEAPON, 10000, BCT_PARTY) && sd && sd != dstsd)
				clif_displaymessage(sd->fd,"You broke target's weapon");
		}
		break;

	case PR_ASPERSIO:
		if (sd && dstmd) {
			clif_skill_nodamage(src,bl,skillid,skilllv,0);
			break;
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		break;

	case ITEM_ENCHANTARMS:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start2(bl,type,100,skilllv,
				skill_get_ele(skillid,skilllv), skill_get_time(skillid,skilllv)));
		break;

	case TK_SEVENWIND:
		switch(skill_get_ele(skillid,skilllv)) {
			case ELE_EARTH : type = SC_EARTHWEAPON;  break;
			case ELE_WIND  : type = SC_WINDWEAPON;   break;
			case ELE_WATER : type = SC_WATERWEAPON;  break;
			case ELE_FIRE  : type = SC_FIREWEAPON;   break;
			case ELE_GHOST : type = SC_GHOSTWEAPON;  break;
			case ELE_DARK  : type = SC_SHADOWWEAPON; break;
			case ELE_HOLY  : type = SC_ASPERSIO;     break;
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));

		sc_start(bl,SC_SEVENWIND,100,skilllv,skill_get_time(skillid,skilllv));

		break;

	case PR_KYRIE:
	case MER_KYRIE:	
		clif_skill_nodamage(bl,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		break;
	//Passive Magnum, should had been casted on yourself.
	case SM_MAGNUM:
	case MS_MAGNUM:
		skill_area_temp[1] = 0;
		map_foreachinrange(skill_area_sub, src, skill_get_splash(skillid, skilllv), BL_SKILL|BL_CHAR,
			src,skillid,skilllv,tick, flag|BCT_ENEMY|1, skill_castend_damage_id);
		clif_skill_nodamage (src,src,skillid,skilllv,1);
		//Initiate 10% of your damage becomes fire element.
		sc_start4(src,SC_WATK_ELEMENT,100,3,20,0,0,skill_get_time2(skillid, skilllv));
		if (sd) skill_blockpc_start (sd, skillid, skill_get_time(skillid, skilllv));
		break;

	case AL_INCAGI:
	case AL_BLESSING:
	case MER_INCAGI:
	case MER_BLESSING:
		if (dstsd != NULL && tsc->data[SC_CHANGEUNDEAD]) {
			skill_attack(BF_MISC,src,src,bl,skillid,skilllv,tick,flag);
			break;
		}
	case PR_SLOWPOISON:
	case PR_IMPOSITIO:
	case PR_LEXAETERNA:
	case PR_SUFFRAGIUM:
	case PR_BENEDICTIO:
	case LK_BERSERK:
	case MS_BERSERK:
	case KN_AUTOCOUNTER:
	case KN_TWOHANDQUICKEN:
	case KN_ONEHAND:
	case MER_QUICKEN:
	case CR_SPEARQUICKEN:
	case CR_REFLECTSHIELD:
	case MS_REFLECTSHIELD:
	case AS_POISONREACT:
	case MC_LOUD:
	case MG_ENERGYCOAT:
	case MO_EXPLOSIONSPIRITS:
	case MO_STEELBODY:
	case MO_BLADESTOP:
	case LK_AURABLADE:
	case LK_PARRYING:
	case MS_PARRYING:
	case LK_CONCENTRATION:
	case WS_CARTBOOST:
	case SN_SIGHT:
	case WS_MELTDOWN:
	case WS_OVERTHRUSTMAX:
	case ST_REJECTSWORD:
	case HW_MAGICPOWER:
	case PF_MEMORIZE:
	case PA_SACRIFICE:
	case ASC_EDP:
	case PF_DOUBLECASTING:
	case SG_SUN_COMFORT:
	case SG_MOON_COMFORT:
	case SG_STAR_COMFORT:
	case NPC_HALLUCINATION:
	case GS_MADNESSCANCEL:
	case GS_ADJUSTMENT:
	case GS_INCREASING:
	case NJ_KASUMIKIRI:
	case NJ_UTSUSEMI:
	case NJ_NEN:
	case NPC_DEFENDER:
	case NPC_MAGICMIRROR:
	case ST_PRESERVE:
	case NPC_INVINCIBLE:
	case NPC_INVINCIBLEOFF:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		break;
	case NPC_STOP:
		if( clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start2(bl,type,100,skilllv,src->id,skill_get_time(skillid,skilllv)) ) )
			sc_start2(src,type,100,skilllv,bl->id,skill_get_time(skillid,skilllv));
		break;
	case HP_ASSUMPTIO:
		if( sd && dstmd )
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		else
			clif_skill_nodamage(src,bl,skillid,skilllv,
				sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		break;
	case MG_SIGHT:
	case MER_SIGHT:
	case AL_RUWACH:
	case WZ_SIGHTBLASTER:
	case NPC_WIDESIGHT:
	case NPC_STONESKIN:
	case NPC_ANTIMAGIC:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start2(bl,type,100,skilllv,skillid,skill_get_time(skillid,skilllv)));
		break;
	case HLIF_AVOID:
	case HAMI_DEFENCE:
		i = skill_get_time(skillid,skilllv);
		clif_skill_nodamage(bl,bl,skillid,skilllv,sc_start(bl,type,100,skilllv,i)); // Master
		clif_skill_nodamage(src,src,skillid,skilllv,sc_start(src,type,100,skilllv,i)); // Homunc
		break;
	case NJ_BUNSINJYUTSU:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		status_change_end(bl, SC_NEN, INVALID_TIMER);
		break;
/* Was modified to only affect targetted char.	[Skotlex]
	case HP_ASSUMPTIO:
		if (flag&1)
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv));
		else
		{
			map_foreachinrange(skill_area_sub, bl,
				skill_get_splash(skillid, skilllv), BL_PC,
				src, skillid, skilllv, tick, flag|BCT_ALL|1,
				skill_castend_nodamage_id);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
		}
		break;
*/
	case SM_ENDURE:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		if (sd)
			skill_blockpc_start (sd, skillid, skill_get_time2(skillid,skilllv));
		break;

	case AS_ENCHANTPOISON: // Prevent spamming [Valaris]
		if (sd && dstsd && dstsd->sc.count) {
			if (dstsd->sc.data[SC_FIREWEAPON] ||
				dstsd->sc.data[SC_WATERWEAPON] ||
				dstsd->sc.data[SC_WINDWEAPON] ||
				dstsd->sc.data[SC_EARTHWEAPON] ||
				dstsd->sc.data[SC_SHADOWWEAPON] ||
				dstsd->sc.data[SC_GHOSTWEAPON]
			//	dstsd->sc.data[SC_ENCPOISON] //People say you should be able to recast to lengthen the timer. [Skotlex]
			) {
					clif_skill_nodamage(src,bl,skillid,skilllv,0);
					clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
					break;
			}
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		break;

	case LK_TENSIONRELAX:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start4(bl,type,100,skilllv,0,0,skill_get_time2(skillid,skilllv),
				skill_get_time(skillid,skilllv)));
		break;

	case MC_CHANGECART:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;

	case TK_MISSION:
		if (sd) {
			int id;
			if (sd->mission_mobid && (sd->mission_count || rand()%100)) { //Cannot change target when already have one
				clif_mission_info(sd, sd->mission_mobid, sd->mission_count);
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			id = mob_get_random_id(0,0xE, sd->status.base_level);
			if (!id) {
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			sd->mission_mobid = id;
			sd->mission_count = 0;
			pc_setglobalreg(sd,"TK_MISSION_ID", id);
			clif_mission_info(sd, id, 0);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
		}
		break;

	case AC_CONCENTRATION:
		{
			clif_skill_nodamage(src,bl,skillid,skilllv,
				sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
			map_foreachinrange( status_change_timer_sub, src,
				skill_get_splash(skillid, skilllv), BL_CHAR,
				src,NULL,type,tick);
		}
		break;

	case SM_PROVOKE:
	case SM_SELFPROVOKE:
	case MER_PROVOKE:
		if( (tstatus->mode&MD_BOSS) || battle_check_undead(tstatus->race,tstatus->def_ele) )
		{
			map_freeblock_unlock();
			return 1;
		}
		//TODO: How much does base level affects? Dummy value of 1% per level difference used. [Skotlex]
		clif_skill_nodamage(src,bl,skillid == SM_SELFPROVOKE ? SM_PROVOKE : skillid,skilllv,
			(i = sc_start(bl,type, skillid == SM_SELFPROVOKE ? 100:( 50 + 3*skilllv + status_get_lv(src) - status_get_lv(bl)), skilllv, skill_get_time(skillid,skilllv))));
		if( !i )
		{
			if( sd )
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			map_freeblock_unlock();
			return 0;
		}
		unit_skillcastcancel(bl, 2);

		if( tsc && tsc->count )
		{
			status_change_end(bl, SC_FREEZE, INVALID_TIMER);
			if( tsc->data[SC_STONE] && tsc->opt1 == OPT1_STONE )
				status_change_end(bl, SC_STONE, INVALID_TIMER);
			status_change_end(bl, SC_SLEEP, INVALID_TIMER);
		}

		if( dstmd )
		{
			dstmd->state.provoke_flag = src->id;
			mob_target(dstmd, src, skill_get_range2(src,skillid,skilllv));
		}
		break;

	case ML_DEVOTION:
	case CR_DEVOTION:
		{
			int count, lv;
			if( !dstsd || (!sd && !mer) )
			{ // Only players can be devoted
				if( sd )
					clif_skill_fail(sd, skillid, USESKILL_FAIL_LEVEL, 0);
				break;
			}

			if( (lv = status_get_lv(src) - dstsd->status.base_level) < 0 )
				lv = -lv;
			if( lv > battle_config.devotion_level_difference || // Level difference requeriments
				(dstsd->sc.data[type] && dstsd->sc.data[type]->val1 != src->id) || // Cannot Devote a player devoted from another source
				(skillid == ML_DEVOTION && (!mer || mer != dstsd->md)) || // Mercenary only can devote owner
				(dstsd->class_&MAPID_UPPERMASK) == MAPID_CRUSADER || // Crusader Cannot be devoted
				(dstsd->sc.data[SC_HELLPOWER])) // Players affected by SC_HELLPOWERR cannot be devoted.
			{
				if( sd )
					clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				map_freeblock_unlock();
				return 1;
			}

			i = 0;
			count = (sd)? min(skilllv,5) : 1; // Mercenary only can Devote owner
			if( sd )
			{ // Player Devoting Player
				ARR_FIND(0, count, i, sd->devotion[i] == bl->id );
				if( i == count )
				{
					ARR_FIND(0, count, i, sd->devotion[i] == 0 );
					if( i == count )
					{ // No free slots, skill Fail
						clif_skill_fail(sd, skillid, USESKILL_FAIL_LEVEL, 0);
						map_freeblock_unlock();
						return 1;
					}
				}

				sd->devotion[i] = bl->id;
			}
			else
				mer->devotion_flag = 1; // Mercenary Devoting Owner

			clif_skill_nodamage(src, bl, skillid, skilllv,
				sc_start4(bl, type, 100, src->id, i, skill_get_range2(src,skillid,skilllv),0, skill_get_time2(skillid, skilllv)));
			clif_devotion(src, NULL);
		}
		break;

	case MO_CALLSPIRITS:
		if(sd) {
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			pc_addspiritball(sd,skill_get_time(skillid,skilllv),skilllv);
		}
		break;

	case CH_SOULCOLLECT:
		if(sd) {
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			for (i = 0; i < 5; i++)
				pc_addspiritball(sd,skill_get_time(skillid,skilllv),5);
		}
		break;

	case MO_KITRANSLATION:
		if(dstsd && (dstsd->class_&MAPID_BASEMASK)!=MAPID_GUNSLINGER) {
			pc_addspiritball(dstsd,skill_get_time(skillid,skilllv),5);
		}
		break;

	case TK_TURNKICK:
	case MO_BALKYOUNG: //Passive part of the attack. Splash knock-back+stun. [Skotlex]
		if (skill_area_temp[1] != bl->id) {
			skill_blown(src,bl,skill_get_blewcount(skillid,skilllv),-1,0);
			skill_additional_effect(src,bl,skillid,skilllv,BF_MISC,ATK_DEF,tick); //Use Misc rather than weapon to signal passive pushback
		}
		break;

	case MO_ABSORBSPIRITS:
		i = 0;
		if (dstsd && dstsd->spiritball && (sd == dstsd || map_flag_vs(src->m) || (sd && sd->state.pvpmode && dstsd->state.pvpmode)) && (dstsd->class_&MAPID_BASEMASK)!=MAPID_GUNSLINGER)
		{	// split the if for readability, and included gunslingers in the check so that their coins cannot be removed [Reddozen]
			i = dstsd->spiritball * 7;
			pc_delspiritball(dstsd,dstsd->spiritball,0);
		} else if (dstmd && !(tstatus->mode&MD_BOSS) && rand() % 100 < 20)
		{	// check if target is a monster and not a Boss, for the 20% chance to absorb 2 SP per monster's level [Reddozen]
			i = 2 * dstmd->level;
			mob_target(dstmd,src,0);
		}
		if (i) status_heal(src, 0, i, 3);
		clif_skill_nodamage(src,bl,skillid,skilllv,i?1:0);
		break;

	case AC_MAKINGARROW:
		if(sd) {
			clif_arrow_create_list(sd);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
		}
		break;

	case AM_PHARMACY:
		if(sd) {
			clif_skill_produce_mix_list(sd,22);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
		}
		break;

	case SA_CREATECON:
		if(sd) {
			clif_skill_produce_mix_list(sd,23);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
		}
		break;

	case BS_HAMMERFALL:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,SC_STUN,(20 + 10 * skilllv),skilllv,skill_get_time2(skillid,skilllv)));
		break;
	case RG_RAID:
		skill_area_temp[1] = 0;
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		map_foreachinrange(skill_area_sub, bl,
			skill_get_splash(skillid, skilllv), splash_target(src),
			src,skillid,skilllv,tick, flag|BCT_ENEMY|1,
			skill_castend_damage_id);
		status_change_end(src, SC_HIDING, INVALID_TIMER);
		break;

	case ASC_METEORASSAULT:
	case GS_SPREADATTACK:
		skill_area_temp[1] = 0;
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		map_foreachinrange(skill_area_sub, bl, skill_get_splash(skillid, skilllv), splash_target(src), 
			src, skillid, skilllv, tick, flag|BCT_ENEMY|SD_SPLASH|1, skill_castend_damage_id);
		break;

	case NPC_EARTHQUAKE:
	case NPC_VAMPIRE_GIFT:
	case NPC_HELLJUDGEMENT:
	case NPC_PULSESTRIKE:
		skill_castend_damage_id(src, src, skillid, skilllv, tick, flag);
		break;

	case KN_BRANDISHSPEAR:
	case ML_BRANDISH:
		skill_brandishspear(src, bl, skillid, skilllv, tick, flag);
		break;

	case WZ_SIGHTRASHER:
		//Passive side of the attack.
		status_change_end(src, SC_SIGHT, INVALID_TIMER);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		map_foreachinrange(skill_area_sub,src,
			skill_get_splash(skillid, skilllv),BL_CHAR|BL_SKILL,
			src,skillid,skilllv,tick, flag|BCT_ENEMY|1,
			skill_castend_damage_id);
		break;

	case NJ_HYOUSYOURAKU:
	case NJ_RAIGEKISAI:
	case WZ_FROSTNOVA:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		skill_area_temp[1] = 0;
		map_foreachinrange(skill_attack_area, src,
			skill_get_splash(skillid, skilllv), splash_target(src),
			BF_MAGIC, src, src, skillid, skilllv, tick, flag, BCT_ENEMY);
		break;

	case HVAN_EXPLOSION:	//[orn]
	case NPC_SELFDESTRUCTION:
		//Self Destruction hits everyone in range (allies+enemies)
		//Except for Summoned Marine spheres on non-versus maps, where it's just enemy.
		i = ((!md || md->special_state.ai == 2 || md->class_ == 1904) && !map_flag_vs(src->m))?
			BCT_ENEMY:BCT_ALL;
		clif_skill_nodamage(src, src, skillid, -1, 1);
		map_delblock(src); //Required to prevent chain-self-destructions hitting back.
		map_foreachinrange(skill_area_sub, bl,
			skill_get_splash(skillid, skilllv), splash_target(src),
			src, skillid, skilllv, tick, flag|i,
			skill_castend_damage_id);
		map_addblock(src);
		status_damage_(src, src, sstatus->max_hp,0,0,1,skillid);
		break;

	case AL_ANGELUS:
	case PR_MAGNIFICAT:
	case PR_GLORIA:
	case SN_WINDWALK:
	case CASH_BLESSING:
	case CASH_INCAGI:
	case CASH_ASSUMPTIO:
		if( !skill_check_party(sd) || (flag&1) )
			clif_skill_nodamage(bl, bl, skillid, skilllv, sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		else if( sd )
			party_foreachsamemap(skill_area_sub, sd, skill_get_splash(skillid, skilllv), src, skillid, skilllv, tick, flag|BCT_PARTY|1, skill_castend_nodamage_id);
		break;
	case MER_MAGNIFICAT:
		if( mer != NULL )
		{
			clif_skill_nodamage(bl, bl, skillid, skilllv, sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
			if( mer->master && skill_check_party(mer->master) && !(flag&1) )
				party_foreachsamemap(skill_area_sub, mer->master, skill_get_splash(skillid, skilllv), src, skillid, skilllv, tick, flag|BCT_PARTY|1, skill_castend_nodamage_id);
			else if( mer->master && !(flag&1) )
				clif_skill_nodamage(src, &mer->master->bl, skillid, skilllv, sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		}
		break;

	case BS_ADRENALINE:
	case BS_ADRENALINE2:
	case BS_WEAPONPERFECT:
	case BS_OVERTHRUST:
		if (sd == NULL || sd->status.party_id == 0 || (flag & 1)) {
			clif_skill_nodamage(bl,bl,skillid,skilllv,
				sc_start2(bl,type,100,skilllv,(src == bl)? 1:0,skill_get_time(skillid,skilllv)));
		} else if (sd) {
			party_foreachsamemap(skill_area_sub,
				sd,skill_get_splash(skillid, skilllv),
				src,skillid,skilllv,tick, flag|BCT_PARTY|1,
				skill_castend_nodamage_id);
		}
		break;

	case BS_MAXIMIZE:
	case NV_TRICKDEAD:
	case CR_DEFENDER:
	case ML_DEFENDER:
	case CR_AUTOGUARD:
	case ML_AUTOGUARD:
	case TK_READYSTORM:
	case TK_READYDOWN:
	case TK_READYTURN:
	case TK_READYCOUNTER:
	case TK_DODGE:
	case CR_SHRINK:
	case SG_FUSION:
	case GS_GATLINGFEVER:
		if( tsce )
		{
			clif_skill_nodamage(src,bl,skillid,skilllv,status_change_end(bl, type, INVALID_TIMER));
			map_freeblock_unlock();
			return 0;
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		break;
	case SL_KAITE:
	case SL_KAAHI:
	case SL_KAIZEL:
	case SL_KAUPE:
		if (sd) {
			if (!dstsd || !(
				(sd->sc.data[SC_SPIRIT] && sd->sc.data[SC_SPIRIT]->val2 == SL_SOULLINKER) ||
				(dstsd->class_&MAPID_UPPERMASK) == MAPID_SOUL_LINKER ||
				dstsd->status.char_id == sd->status.char_id ||
				dstsd->status.char_id == sd->status.partner_id ||
				dstsd->status.char_id == sd->status.child
			)) {
				status_change_start(src,SC_STUN,10000,skilllv,0,0,0,500,8);
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid, skilllv)));
		break;
	case SM_AUTOBERSERK:
	case MER_AUTOBERSERK:
		if( tsce )
			i = status_change_end(bl, type, INVALID_TIMER);
		else
			i = sc_start(bl,type,100,skilllv,60000);
		clif_skill_nodamage(src,bl,skillid,skilllv,i);
		break;
	case TF_HIDING:
	case ST_CHASEWALK:
	case ALL_RIDING:
		if (tsce)
		{
			clif_skill_nodamage(src,bl,skillid,-1,status_change_end(bl, type, INVALID_TIMER)); //Hide skill-scream animation.
			map_freeblock_unlock();
			return 0;
		}
		clif_skill_nodamage(src,bl,skillid,-1,sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));		
		break;
	case TK_RUN:
		if (tsce)
		{
			clif_skill_nodamage(src,bl,skillid,skilllv,status_change_end(bl, type, INVALID_TIMER));
			map_freeblock_unlock();
			return 0;
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,sc_start4(bl,type,100,skilllv,unit_getdir(bl),0,0,0));
		if (sd) // If the client receives a skill-use packet inmediately before a walkok packet, it will discard the walk packet! [Skotlex]
			clif_walkok(sd); // So aegis has to resend the walk ok.
		break;
	case AS_CLOAKING:
		if (tsce)
		{
			i = status_change_end(bl, type, INVALID_TIMER);
			if( i )
				clif_skill_nodamage(src,bl,skillid,-1,i);
			else if( sd )
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			map_freeblock_unlock();
			return 0;
		}
		i = sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv));
		if( i )
			clif_skill_nodamage(src,bl,skillid,-1,i);
		else if( sd )
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		break;

	case BD_ADAPTATION:
		if(tsc && tsc->data[SC_DANCING]){
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			status_change_end(bl, SC_DANCING, INVALID_TIMER);
		}
		break;

	case BA_FROSTJOKER:
	case DC_SCREAM:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		skill_addtimerskill(src,tick+2000,bl->id,src->x,src->y,skillid,skilllv,0,flag);

		if (md) {
			// custom hack to make the mob display the skill, because these skills don't show the skill use text themselves
			//NOTE: mobs don't have the sprite animation that is used when performing this skill (will cause glitches)
			char temp[70];
			snprintf(temp, sizeof(temp), "%s : %s !!",md->name,skill_db[skillid].desc);
			clif_message(&md->bl,temp);
		}
		break;

	case BA_PANGVOICE:
		clif_skill_nodamage(src,bl,skillid,skilllv, sc_start(bl,SC_CONFUSION,50,7,skill_get_time(skillid,skilllv)));
		break;

	case DC_WINKCHARM:
		if( dstsd )
			clif_skill_nodamage(src,bl,skillid,skilllv, sc_start(bl,SC_CONFUSION,30,7,skill_get_time2(skillid,skilllv)));
		else
		if( dstmd )
		{
			if( status_get_lv(src) > status_get_lv(bl)
			&&  (tstatus->race == RC_DEMON || tstatus->race == RC_DEMIHUMAN || tstatus->race == RC_ANGEL)
			&&  !(tstatus->mode&MD_BOSS) )
				clif_skill_nodamage(src,bl,skillid,skilllv, sc_start(bl,type,70,skilllv,skill_get_time(skillid,skilllv)));
			else
			{
				clif_skill_nodamage(src,bl,skillid,skilllv,0);
				if(sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			}
		}
		break;

	case TF_STEAL:
		if(sd) {
			if(pc_steal_item(sd,bl,skilllv))
				clif_skill_nodamage(src,bl,skillid,skilllv,1);
			else
				clif_skill_fail(sd,skillid,USESKILL_FAIL,0);
		}
		break;

	case RG_STEALCOIN:
		if(sd) {
			if(pc_steal_coin(sd,bl))
			{
				dstmd->state.provoke_flag = src->id;
				mob_target(dstmd, src, skill_get_range2(src,skillid,skilllv));
				clif_skill_nodamage(src,bl,skillid,skilllv,1);

			} 
			else
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		}
		break;

	case MG_STONECURSE:
		{
			if (tstatus->mode&MD_BOSS) {
				if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			if(status_isimmune(bl) || !tsc)
				break;

			if (tsc->data[SC_STONE]) {
				status_change_end(bl, SC_STONE, INVALID_TIMER);
				if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			if (sc_start4(bl,SC_STONE,(skilllv*4+20),
				skilllv, 0, 0, skill_get_time(skillid, skilllv),
				skill_get_time2(skillid,skilllv)))
					clif_skill_nodamage(src,bl,skillid,skilllv,1);
			else if(sd) {
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				// Level 6-10 doesn't consume a red gem if it fails [celest]
				if (skilllv > 5)
				{ // not to consume items
					map_freeblock_unlock();
					return 0;
				}
			}
		}
		break;

	case NV_FIRSTAID:
		clif_skill_nodamage(src,bl,skillid,5,1);
		status_heal(bl,5,0,0);
		break;

	case AL_CURE:
		if(status_isimmune(bl)) {
			clif_skill_nodamage(src,bl,skillid,skilllv,0);
			break;
		}
		status_change_end(bl, SC_SILENCE, INVALID_TIMER);
		status_change_end(bl, SC_BLIND, INVALID_TIMER);
		status_change_end(bl, SC_CONFUSION, INVALID_TIMER);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;

	case TF_DETOXIFY:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		status_change_end(bl, SC_POISON, INVALID_TIMER);
		status_change_end(bl, SC_DPOISON, INVALID_TIMER);
		break;

	case PR_STRECOVERY:
		if(status_isimmune(bl)) {
			clif_skill_nodamage(src,bl,skillid,skilllv,0);
			break;
		}
		if (tsc && tsc->opt1) {
			status_change_end(bl, SC_FREEZE, INVALID_TIMER);
			status_change_end(bl, SC_STONE, INVALID_TIMER);
			status_change_end(bl, SC_SLEEP, INVALID_TIMER);
			status_change_end(bl, SC_STUN, INVALID_TIMER);
		}
		//Is this equation really right? It looks so... special.
		if(battle_check_undead(tstatus->race,tstatus->def_ele))
		{
			status_change_start(bl, SC_BLIND,
				100*(100-(tstatus->int_/2+tstatus->vit/3+tstatus->luk/10)),
				1,0,0,0,
				skill_get_time2(skillid, skilllv) * (100-(tstatus->int_+tstatus->vit)/2)/100,10);
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if(dstmd)
			mob_unlocktarget(dstmd,tick);
		break;

	// Mercenary Supportive Skills
	case MER_BENEDICTION:
		status_change_end(bl, SC_CURSE, INVALID_TIMER);
		status_change_end(bl, SC_BLIND, INVALID_TIMER);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;
	case MER_COMPRESS:
		status_change_end(bl, SC_BLEEDING, INVALID_TIMER);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;
	case MER_MENTALCURE:
		status_change_end(bl, SC_CONFUSION, INVALID_TIMER);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;
	case MER_RECUPERATE:
		status_change_end(bl, SC_POISON, INVALID_TIMER);
		status_change_end(bl, SC_SILENCE, INVALID_TIMER);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;
	case MER_REGAIN:
		status_change_end(bl, SC_SLEEP, INVALID_TIMER);
		status_change_end(bl, SC_STUN, INVALID_TIMER);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;
	case MER_TENDER:
		status_change_end(bl, SC_FREEZE, INVALID_TIMER);
		status_change_end(bl, SC_STONE, INVALID_TIMER);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;

	case MER_SCAPEGOAT:
		if( mer && mer->master )
		{
			status_heal(&mer->master->bl, mer->battle_status.hp, 0, 2);
			status_damage_(src, src, mer->battle_status.max_hp, 0, 0, 1, skillid);
		}
		break;

	case MER_ESTIMATION:
		if( !mer )
			break;
		sd = mer->master;
	case WZ_ESTIMATION:
		if( sd == NULL )
			break;
		if( dstsd )
		{ // Fail on Players
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			break;
		}
		if( dstmd && dstmd->class_ == MOBID_EMPERIUM )
			break; // Cannot be Used on Emperium

		clif_skill_nodamage(src, bl, skillid, skilllv, 1);
		clif_skill_estimation(sd, bl);
		if( skillid == MER_ESTIMATION )
			sd = NULL;
		break;

	case BS_REPAIRWEAPON:
		if(sd && dstsd)
			clif_item_repair_list(sd,dstsd);
		break;

	case MC_IDENTIFY:
		if(sd)
			clif_item_identify_list(sd);
		break;

	// Weapon Refining [Celest]
	case WS_WEAPONREFINE:
		if(sd)
			clif_item_refine_list(sd);
		break;

	case MC_VENDING:
		if( sd )
		{
			if( battle_config.vending_zeny_id )
				clif_vend(sd,skilllv); // Extended Vending System
			else
			{
				if ( !pc_can_give_items(pc_isGM(sd)) )
					clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				else
					clif_openvendingreq(sd,2+skilllv);
			}
		}
		break;

	case AL_TELEPORT:
		if(sd)
		{
			if( (map[bl->m].flag.noteleport || !guild_canescape(sd)) && skilllv <= 2 )
			{
				clif_skill_teleportmessage(sd,0);
				break;
			}
			if(!battle_config.duel_allow_teleport && sd->duel_group && skilllv <= 2) { // duel restriction [LuzZza]
				clif_displaymessage(sd->fd, "Duel: Can't use teleport in duel.");
				break;
			}

			if( sd->state.autocast || ( (sd->skillitem == AL_TELEPORT || battle_config.skip_teleport_lv1_menu) && skilllv == 1 ) || skilllv == 3 )
			{
				if( skilllv == 1 )
					pc_randomwarp(sd,CLR_TELEPORT);
				else
					pc_setpos(sd,sd->status.save_point.map,sd->status.save_point.x,sd->status.save_point.y,CLR_TELEPORT);
				break;
			}

			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			if( skilllv == 1 )
				clif_skill_warppoint(sd,skillid,skilllv, (unsigned short)-1,0,0,0);
			else
				clif_skill_warppoint(sd,skillid,skilllv, (unsigned short)-1,sd->status.save_point.map,0,0);
		} else
			unit_warp(bl,-1,-1,-1,CLR_TELEPORT);
		break;

	case NPC_EXPULSION:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		unit_warp(bl,-1,-1,-1,CLR_TELEPORT);
		break;

	case AL_HOLYWATER:
		if(sd) {
			if (skill_produce_mix(sd, skillid, 523, 0, 0, 0, 1))
				clif_skill_nodamage(src,bl,skillid,skilllv,1);
			else
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		}
		break;

	case TF_PICKSTONE:
		if(sd) {
			int eflag;
			struct item item_tmp;
			struct block_list tbl;
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			memset(&item_tmp,0,sizeof(item_tmp));
			memset(&tbl,0,sizeof(tbl)); // [MouseJstr]
			item_tmp.nameid = ITEMID_STONE;
			item_tmp.identify = 1;
			tbl.id = 0;
			clif_takeitem(&sd->bl,&tbl);
			eflag = pc_additem(sd,&item_tmp,1,LOG_TYPE_PRODUCE);
			if(eflag) {
				clif_additem(sd,0,0,eflag);
				map_addflooritem(&item_tmp,1,sd->bl.m,sd->bl.x,sd->bl.y,0,0,0,0,0);
			}
		}
		break;
	case ASC_CDP:
     	if(sd) {
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			skill_produce_mix(sd, skillid, 678, 0, 0, 0, 1); //Produce a Deadly Poison Bottle.
		}
		break;

	case RG_STRIPWEAPON:
	case RG_STRIPSHIELD:
	case RG_STRIPARMOR:
	case RG_STRIPHELM:
	case ST_FULLSTRIP:
	{
		unsigned short location = 0;
		int d = 0;
		
		//Rate in percent
		if ( skillid == ST_FULLSTRIP ) {
			i = 5 + 2*skilllv + (sstatus->dex - tstatus->dex)/5;
		} else {
			i = 5 + 5*skilllv + (sstatus->dex - tstatus->dex)/5;
		}
		if (i < 5) i = 5; //Minimum rate 5%

		//Duration in ms
		d = skill_get_time(skillid,skilllv) + (sstatus->dex - tstatus->dex)*500;
		if (d < 0) d = 0; //Minimum duration 0ms

		switch (skillid) {
		case RG_STRIPWEAPON:
			location = EQP_WEAPON;
			break;
		case RG_STRIPSHIELD:
			location = EQP_SHIELD;
			break;
		case RG_STRIPARMOR:
			location = EQP_ARMOR;
			break;
		case RG_STRIPHELM:
			location = EQP_HELM;
			break;
		case ST_FULLSTRIP:
			location = EQP_WEAPON|EQP_SHIELD|EQP_ARMOR|EQP_HELM;
			break;
		}

		//Special message when trying to use strip on FCP [Jobbie]
		if( sd && skillid == ST_FULLSTRIP && tsc && tsc->data[SC_CP_WEAPON] && tsc->data[SC_CP_HELM] && tsc->data[SC_CP_ARMOR] && tsc->data[SC_CP_SHIELD] )
		{
			clif_gospel_info(sd, 0x28);
			break;
		}

		//Attempts to strip at rate i and duration d
		if( (i = skill_strip_equip(bl, location, i, skilllv, d)) || skillid != ST_FULLSTRIP )
			clif_skill_nodamage(src,bl,skillid,skilllv,i); 

		//Nothing stripped.
		if( sd && !i )
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
	}
		break;

	case AM_BERSERKPITCHER:
	case AM_POTIONPITCHER:
		{
			int i,x,hp = 0,sp = 0,bonus=100;
			if( dstmd && dstmd->class_ == MOBID_EMPERIUM )
			{
				map_freeblock_unlock();
				return 1;
			}
			if( sd )
			{
				x = skilllv%11 - 1;
				i = pc_search_inventory(sd,skill_db[skillid].itemid[x]);
				if(i < 0 || skill_db[skillid].itemid[x] <= 0)
				{
					clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
					map_freeblock_unlock();
					return 1;
				}
				if(sd->inventory_data[i] == NULL || sd->status.inventory[i].amount < skill_db[skillid].amount[x])
				{
					clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
					map_freeblock_unlock();
					return 1;
				}
				if( skillid == AM_BERSERKPITCHER )
				{
					if( dstsd && dstsd->status.base_level < (unsigned int)sd->inventory_data[i]->elv )
					{
						clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
						map_freeblock_unlock();
						return 1;
					}
				}
				potion_flag = 1;
				potion_hp = potion_sp = potion_per_hp = potion_per_sp = 0;
				potion_target = bl->id;
				run_script(sd->inventory_data[i]->script,0,sd->bl.id,0);
				potion_flag = potion_target = 0;
				if( sd->sc.data[SC_SPIRIT] && sd->sc.data[SC_SPIRIT]->val2 == SL_ALCHEMIST )
					bonus += sd->status.base_level;
				if( potion_per_hp > 0 || potion_per_sp > 0 )
				{
					hp = tstatus->max_hp * potion_per_hp / 100;
					hp = hp * (100 + pc_checkskill(sd,AM_POTIONPITCHER)*10 + pc_checkskill(sd,AM_LEARNINGPOTION)*5)*bonus/10000;
					if( dstsd )
					{
						sp = dstsd->status.max_sp * potion_per_sp / 100;
						sp = sp * (100 + pc_checkskill(sd,AM_POTIONPITCHER)*10 + pc_checkskill(sd,AM_LEARNINGPOTION)*5)*bonus/10000;
					}
				}
				else
				{
					if( potion_hp > 0 )
					{
						hp = potion_hp * (100 + pc_checkskill(sd,AM_POTIONPITCHER)*10 + pc_checkskill(sd,AM_LEARNINGPOTION)*5)*bonus/10000;
						hp = hp * (100 + (tstatus->vit<<1)) / 100;
						if( dstsd )
							hp = hp * (100 + pc_checkskill(dstsd,SM_RECOVERY)*10) / 100;
					}
					if( potion_sp > 0 )
					{
						sp = potion_sp * (100 + pc_checkskill(sd,AM_POTIONPITCHER)*10 + pc_checkskill(sd,AM_LEARNINGPOTION)*5)*bonus/10000;
						sp = sp * (100 + (tstatus->int_<<1)) / 100;
						if( dstsd )
							sp = sp * (100 + pc_checkskill(dstsd,MG_SRECOVERY)*10) / 100;
					}
				}

				if (sd->itemgrouphealrate[IG_POTION]>0)
				{
					hp += hp * sd->itemgrouphealrate[IG_POTION] / 100;
					sp += sp * sd->itemgrouphealrate[IG_POTION] / 100;
				}

				if( (i = pc_skillheal_bonus(sd, skillid)) )
				{
					hp += hp * i / 100;
					sp += sp * i / 100;
				}
			}
			else
			{
				hp = (1 + rand()%400) * (100 + skilllv*10) / 100;
				hp = hp * (100 + (tstatus->vit<<1)) / 100;
				if( dstsd )
					hp = hp * (100 + pc_checkskill(dstsd,SM_RECOVERY)*10) / 100;
			}
			if( dstsd && (i = pc_skillheal2_bonus(dstsd, skillid)) )
			{
				hp += hp * i / 100;
				sp += sp * i / 100;
			}
			if( tsc && tsc->data[SC_CRITICALWOUND] )
			{
				hp -= hp * tsc->data[SC_CRITICALWOUND]->val2 / 100;
				sp -= sp * tsc->data[SC_CRITICALWOUND]->val2 / 100;
			}
			if( dstmd && mob_is_battleground(dstmd) )
				hp = 1;

			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			if( hp > 0 || (skillid == AM_POTIONPITCHER && sp <= 0) )
				clif_skill_nodamage(NULL,bl,AL_HEAL,hp,1);
			if( sp > 0 )
				clif_skill_nodamage(NULL,bl,MG_SRECOVERY,sp,1);
			status_heal(bl,hp,sp,0);
		}
		break;
	case AM_CP_WEAPON:
	case AM_CP_SHIELD:
	case AM_CP_ARMOR:
	case AM_CP_HELM:
		{
			enum sc_type scid = (sc_type)(SC_STRIPWEAPON + (skillid - AM_CP_WEAPON));
			status_change_end(bl, scid, INVALID_TIMER);
			clif_skill_nodamage(src,bl,skillid,skilllv,
				sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		}
		break;
	case AM_TWILIGHT1:
		if (sd) {
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			//Prepare 200 White Potions.
			if (!skill_produce_mix(sd, skillid, 504, 0, 0, 0, 200))
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		}
		break;
	case AM_TWILIGHT2:
		if (sd) {
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			//Prepare 200 Slim White Potions.
			if (!skill_produce_mix(sd, skillid, 547, 0, 0, 0, 200))
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		}
		break;
	case AM_TWILIGHT3:
		if (sd) {
			//check if you can produce all three, if not, then fail:
			if (!skill_can_produce_mix(sd,970,-1, 100) //100 Alcohol
				|| !skill_can_produce_mix(sd,7136,-1, 50) //50 Acid Bottle
				|| !skill_can_produce_mix(sd,7135,-1, 50) //50 Flame Bottle
			) {
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			skill_produce_mix(sd, skillid, 970, 0, 0, 0, 100);
			skill_produce_mix(sd, skillid, 7136, 0, 0, 0, 50);
			skill_produce_mix(sd, skillid, 7135, 0, 0, 0, 50);
		}
		break;
	case SA_DISPELL:
		if (flag&1 || (i = skill_get_splash(skillid, skilllv)) < 1)
		{
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			if((dstsd && (dstsd->class_&MAPID_UPPERMASK) == MAPID_SOUL_LINKER)
				|| (tsc && tsc->data[SC_SPIRIT] && tsc->data[SC_SPIRIT]->val2 == SL_ROGUE) //Rogue's spirit defends againt dispel.
				|| rand()%100 >= 50+10*skilllv)
			{
				if (sd)
					clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			if(status_isimmune(bl) || !tsc || !tsc->count)
				break;
			for(i=0;i<SC_MAX;i++)
			{
				if (!tsc->data[i])
					continue;
				switch (i) {
				case SC_WEIGHT50:		case SC_WEIGHT90:		case SC_HALLUCINATION:
				case SC_STRIPWEAPON:	case SC_STRIPSHIELD:	case SC_STRIPARMOR:
				case SC_STRIPHELM:		case SC_CP_WEAPON:		case SC_CP_SHIELD:
				case SC_CP_ARMOR:		case SC_CP_HELM:		case SC_COMBO:
				case SC_STRFOOD:		case SC_AGIFOOD:		case SC_VITFOOD:
				case SC_INTFOOD:		case SC_DEXFOOD:		case SC_LUKFOOD:
				case SC_HITFOOD:		case SC_FLEEFOOD:		case SC_BATKFOOD:
				case SC_WATKFOOD:		case SC_MATKFOOD:		case SC_DANCING:
				case SC_GUILDAURA:		case SC_EDP:			case SC_AUTOBERSERK:
				case SC_CARTBOOST:		case SC_MELTDOWN:		case SC_SAFETYWALL:
				case SC_SMA:			case SC_SPEEDUP0:		case SC_NOCHAT:
				case SC_ANKLE:			case SC_SPIDERWEB:		case SC_JAILED:
				case SC_ITEMBOOST:		case SC_EXPBOOST:		case SC_LIFEINSURANCE:
				case SC_BOSSMAPINFO:	case SC_PNEUMA:			case SC_AUTOSPELL:
				case SC_INCHITRATE:		case SC_INCATKRATE:		case SC_NEN:
				case SC_READYSTORM:		case SC_READYDOWN:		case SC_READYTURN:
				case SC_READYCOUNTER:	case SC_DODGE:			case SC_WARM:
				case SC_SPEEDUP1:		case SC_AUTOTRADE:		case SC_CRITICALWOUND:
				case SC_JEXPBOOST:		case SC_INVINCIBLE:		case SC_INVINCIBLEOFF:
				case SC_HELLPOWER:		case SC_MANU_ATK:		case SC_MANU_DEF:
				case SC_SPL_ATK:		case SC_SPL_DEF:		case SC_MANU_MATK:
				case SC_SPL_MATK:		case SC_RICHMANKIM:		case SC_ETERNALCHAOS:
				case SC_DRUMBATTLE:		case SC_NIBELUNGEN:		case SC_ROKISWEIL:
				case SC_INTOABYSS:		case SC_SIEGFRIED:		case SC_HUMMING:
				case SC_FORTUNE:		case SC_FOOD_STR_CASH:		case SC_FOOD_AGI_CASH:
				case SC_FOOD_VIT_CASH:		case SC_FOOD_DEX_CASH:		case SC_FOOD_INT_CASH:
				case SC_FOOD_LUK_CASH:
					continue;
				case SC_ASSUMPTIO:
					if( bl->type == BL_MOB )
						continue;
					break;
				}
				if(i==SC_BERSERK) tsc->data[i]->val2=0; //Mark a dispelled berserk to avoid setting hp to 100 by setting hp penalty to 0.
				status_change_end(bl, (sc_type)i, INVALID_TIMER);
			}
			break;
		}
		//Affect all targets on splash area.
		map_foreachinrange(skill_area_sub, bl, i, BL_CHAR,
			src, skillid, skilllv, tick, flag|1,
			skill_castend_damage_id);
		break;

	case TF_BACKSLIDING: //This is the correct implementation as per packet logging information. [Skotlex]
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		skill_blown(src,bl,skill_get_blewcount(skillid,skilllv),unit_getdir(bl),0);
		break;

	case TK_HIGHJUMP:
		{
			int x,y, dir = unit_getdir(src);
				if(sd && sd->sc.data[SC_ANKLE] ){
					clif_skill_fail(sd,skillid,0,0);
				return 0;
			}
		  	//Fails on noteleport maps, except for vs maps [Skotlex]
			if(map[src->m].flag.noteleport &&
				!(map_flag_vs(src->m) || map_flag_gvg2(src->m))
			) {
				x = src->x;
				y = src->y;
			} else {
				x = src->x + dirx[dir]*skilllv*2;
				y = src->y + diry[dir]*skilllv*2;
			}

			clif_skill_nodamage(src,bl,TK_HIGHJUMP,skilllv,1);
			if(!map_count_oncell(src->m,x,y,BL_PC|BL_NPC|BL_MOB) && map_getcell(src->m,x,y,CELL_CHKREACH)) {
				clif_slide(src,x,y);
				unit_movepos(src, x, y, 1, 0);
			}
		}
		break;

	case SA_CASTCANCEL:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		unit_skillcastcancel(src,1);
		if(sd) {
			int sp = skill_get_sp(sd->skillid_old,sd->skilllv_old);
			sp = sp * (90 - (skilllv-1)*20) / 100;
			if(sp < 0) sp = 0;
			status_zap(src, 0, sp);
		}
		break;
	case SA_SPELLBREAKER:
		{
			int sp;
			if(tsc && tsc->data[SC_MAGICROD]) {
				sp = skill_get_sp(skillid,skilllv);
				sp = sp * tsc->data[SC_MAGICROD]->val2 / 100;
				if(sp < 1) sp = 1;
				status_heal(bl,0,sp,2);
				clif_skill_nodamage(bl,bl,SA_MAGICROD,tsc->data[SC_MAGICROD]->val1,1);
				status_percent_damage(bl, src, 0, -20, false); //20% max SP damage.
			} else {
				struct unit_data *ud = unit_bl2ud(bl);
				int bl_skillid=0,bl_skilllv=0,hp = 0;
				if (!ud || ud->skilltimer == INVALID_TIMER)
					break; //Nothing to cancel.
				bl_skillid = ud->skillid;
				bl_skilllv = ud->skilllv;
				if (tstatus->mode & MD_BOSS)
				{	//Only 10% success chance against bosses. [Skotlex]
					if (rand()%100 < 90)
					{
						if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
						break;
					}
				} else if (!dstsd || map_flag_vs(bl->m)) //HP damage only on pvp-maps when against players.
					hp = tstatus->max_hp/50; //Recover 2% HP [Skotlex]

				clif_skill_nodamage(src,bl,skillid,skilllv,1);
				unit_skillcastcancel(bl,0);
				sp = skill_get_sp(bl_skillid,bl_skilllv);
				status_zap(bl, hp, sp);

				if (hp && skilllv >= 5)
					hp>>=1;	//Recover half damaged HP at level 5 [Skotlex]
				else
					hp = 0;

				if (sp) //Recover some of the SP used
					sp = sp*(25*(skilllv-1))/100;

				if(hp || sp)
					status_heal(src, hp, sp, 2);
			}
		}
		break;
	case SA_MAGICROD:
		//It activates silently, no use animation.
		sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv));
		break;
	case SA_AUTOSPELL:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if(sd)
			clif_autospell(sd,skilllv);
		else {
			int maxlv=1,spellid=0;
			static const int spellarray[3] = { MG_COLDBOLT,MG_FIREBOLT,MG_LIGHTNINGBOLT };
			if(skilllv >= 10) {
				spellid = MG_FROSTDIVER;
//				if (tsc && tsc->data[SC_SPIRIT] && tsc->data[SC_SPIRIT]->val2 == SA_SAGE)
//					maxlv = 10;
//				else
					maxlv = skilllv - 9;
			}
			else if(skilllv >=8) {
				spellid = MG_FIREBALL;
				maxlv = skilllv - 7;
			}
			else if(skilllv >=5) {
				spellid = MG_SOULSTRIKE;
				maxlv = skilllv - 4;
			}
			else if(skilllv >=2) {
				int i = rand()%3;
				spellid = spellarray[i];
				maxlv = skilllv - 1;
			}
			else if(skilllv > 0) {
				spellid = MG_NAPALMBEAT;
				maxlv = 3;
			}
			if(spellid > 0)
				sc_start4(src,SC_AUTOSPELL,100,skilllv,spellid,maxlv,0,
					skill_get_time(SA_AUTOSPELL,skilllv));
		}
		break;

	case BS_GREED:
		if(sd){
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			map_foreachinrange(skill_greed,bl,
				skill_get_splash(skillid, skilllv),BL_ITEM,bl);
		}
		break;

	case SA_ELEMENTWATER:
	case SA_ELEMENTFIRE:
	case SA_ELEMENTGROUND:
	case SA_ELEMENTWIND:
		if(sd && !dstmd) //Only works on monsters.
			break;
		if(tstatus->mode&MD_BOSS)
			break;
	case NPC_ATTRICHANGE:
	case NPC_CHANGEWATER:
	case NPC_CHANGEGROUND:
	case NPC_CHANGEFIRE:
	case NPC_CHANGEWIND:
	case NPC_CHANGEPOISON:
	case NPC_CHANGEHOLY:
	case NPC_CHANGEDARKNESS:
	case NPC_CHANGETELEKINESIS:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start2(bl, type, 100, skilllv, skill_get_ele(skillid,skilllv),
				skill_get_time(skillid, skilllv)));
		break;
	case NPC_CHANGEUNDEAD:
		//This skill should fail if target is wearing bathory/evil druid card [Brainstorm]
		//TO-DO This is ugly, fix it
		if(tstatus->def_ele==ELE_UNDEAD || tstatus->def_ele==ELE_DARK) break;
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start2(bl, type, 100, skilllv, skill_get_ele(skillid,skilllv),
				skill_get_time(skillid, skilllv)));
		break;

	case NPC_PROVOCATION:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if (md) mob_unlocktarget(md, tick);
		break;

	case NPC_KEEPING:
	case NPC_BARRIER:
		{
			int skill_time = skill_get_time(skillid,skilllv);
			struct unit_data *ud = unit_bl2ud(bl);
			if (clif_skill_nodamage(src,bl,skillid,skilllv,
					sc_start(bl,type,100,skilllv,skill_time))
			&& ud) {	//Disable attacking/acting/moving for skill's duration.
				ud->attackabletime =
				ud->canact_tick =
				ud->canmove_tick = tick + skill_time;
			}
		}
		break;

	case NPC_REBIRTH:
		if( md && md->state.rebirth )
			break; // only works once
		sc_start(bl,type,100,skilllv,-1);
		break;

	case NPC_DARKBLESSING:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start2(bl,type,(50+skilllv*5),skilllv,skilllv,skill_get_time2(skillid,skilllv)));
		break;

	case NPC_LICK:
		status_zap(bl, 0, 100);
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,(skilllv*5),skilllv,skill_get_time2(skillid,skilllv)));
		break;

	case NPC_SUICIDE:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		status_kill(src); //When suiciding, neither exp nor drops is given.
		break;

	case NPC_SUMMONSLAVE:
	case NPC_SUMMONMONSTER:
		if(md && md->skillidx >= 0 && !md->option.no_slaves)
			mob_summonslave(md,md->db->skill[md->skillidx].val,skilllv,skillid);
		break;

	case NPC_CALLSLAVE:
		mob_warpslave(src,MOB_SLAVEDISTANCE);
		break;

	case NPC_RANDOMMOVE:
		if (md) {
			md->next_walktime = tick - 1;
			mob_randomwalk(md,tick);
		}
		break;

	case NPC_SPEEDUP:
		{
			// or does it increase casting rate? just a guess xD
			int i = SC_ASPDPOTION0 + skilllv - 1;
			if (i > SC_ASPDPOTION3)
				i = SC_ASPDPOTION3;
			clif_skill_nodamage(src,bl,skillid,skilllv,
				sc_start(bl,(sc_type)i,100,skilllv,skilllv * 60000));
		}
		break;

	case NPC_REVENGE:
		// not really needed... but adding here anyway ^^
		if (md && md->master_id > 0) {
			struct block_list *mbl, *tbl;
			if ((mbl = map_id2bl(md->master_id)) == NULL ||
				(tbl = battle_gettargeted(mbl)) == NULL)
				break;
			md->state.provoke_flag = tbl->id;
			mob_target(md, tbl, sstatus->rhw.range);
		}
		break;

	case NPC_RUN:
		{
			const int mask[8][2] = {{0,-1},{1,-1},{1,0},{1,1},{0,1},{-1,1},{-1,0},{-1,-1}};
			int dir = (bl == src)?unit_getdir(src):map_calc_dir(src,bl->x,bl->y); //If cast on self, run forward, else run away.
			unit_stop_attack(src);
			//Run skillv tiles overriding the can-move check.
			if (unit_walktoxy(src, src->x + skilllv * mask[dir][0], src->y + skilllv * mask[dir][1], 2) && md)
				md->state.skillstate = MSS_WALK; //Otherwise it isn't updated in the ai.
		}
		break;

	case NPC_TRANSFORMATION:
	case NPC_METAMORPHOSIS:
		if(md && md->skillidx >= 0) {
			int class_ = mob_random_class (md->db->skill[md->skillidx].val,0);
			if (skilllv > 1) //Multiply the rest of mobs. [Skotlex]
				mob_summonslave(md,md->db->skill[md->skillidx].val,skilllv-1,skillid);
			if (class_) mob_class_change(md, class_);
		}
		break;

	case NPC_EMOTION_ON:
	case NPC_EMOTION:
		//va[0] is the emotion to use.
		//NPC_EMOTION & NPC_EMOTION_ON can change a mob's mode 'permanently' [Skotlex]
		//val[1] 'sets' the mode
		//val[2] adds to the current mode
		//val[3] removes from the current mode
		//val[4] if set, asks to delete the previous mode change.
		if(md && md->skillidx >= 0 && tsc)
		{
			clif_emotion(bl, md->db->skill[md->skillidx].val[0]);
			if(md->db->skill[md->skillidx].val[4] && tsce)
				status_change_end(bl, type, INVALID_TIMER);

			if(md->db->skill[md->skillidx].val[1] || md->db->skill[md->skillidx].val[2])
				sc_start4(src, type, 100, skilllv,
					md->db->skill[md->skillidx].val[1],
					md->db->skill[md->skillidx].val[2],
					md->db->skill[md->skillidx].val[3],
					skill_get_time(skillid, skilllv));
		}
		break;

	case NPC_POWERUP:
		sc_start(bl,SC_INCATKRATE,100,200,skill_get_time(skillid, skilllv));
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,100,skill_get_time(skillid, skilllv)));
		break;

	case NPC_AGIUP:
		sc_start(bl,SC_SPEEDUP1,100,skilllv,skill_get_time(skillid, skilllv));
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,100,skill_get_time(skillid, skilllv)));
		break;

	case NPC_INVISIBLE:
		//Have val4 passed as 6 is for "infinite cloak" (do not end on attack/skill use).
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start4(bl,type,100,skilllv,0,0,6,skill_get_time(skillid,skilllv)));
		break;

	case NPC_SIEGEMODE:
		// not sure what it does
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;

	case WE_MALE:
		{
			int hp_rate=(skilllv <= 0)? 0:skill_db[skillid].hp_rate[skilllv-1];
			int gain_hp= tstatus->max_hp*abs(hp_rate)/100; // The earned is the same % of the target HP than it costed the caster. [Skotlex]
			clif_skill_nodamage(src,bl,skillid,status_heal(bl, gain_hp, 0, 0),1);
		}
		break;
	case WE_FEMALE:
		{
			int sp_rate=(skilllv <= 0)? 0:skill_db[skillid].sp_rate[skilllv-1];
			int gain_sp=tstatus->max_sp*abs(sp_rate)/100;// The earned is the same % of the target SP than it costed the caster. [Skotlex]
			clif_skill_nodamage(src,bl,skillid,status_heal(bl, 0, gain_sp, 0),1);
		}
		break;

	// parent-baby skills
	case WE_BABY:
		if(sd){
			struct map_session_data *f_sd = pc_get_father(sd);
			struct map_session_data *m_sd = pc_get_mother(sd);
			// if neither was found
			if(!f_sd && !m_sd){
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				map_freeblock_unlock();
				return 0;
			}
			status_change_start(bl,SC_STUN,10000,skilllv,0,0,0,skill_get_time2(skillid,skilllv),8);
			if (f_sd) sc_start(&f_sd->bl,type,100,skilllv,skill_get_time(skillid,skilllv));
			if (m_sd) sc_start(&m_sd->bl,type,100,skilllv,skill_get_time(skillid,skilllv));
		}
		break;

	case PF_HPCONVERSION:
		{
			int hp, sp;
			hp = sstatus->max_hp/10;
			sp = hp * 10 * skilllv / 100;
			if (!status_charge(src,hp,0)) {
				if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
			clif_skill_nodamage(src, bl, skillid, skilllv, 1);
			status_heal(bl,0,sp,2);
		}
		break;

	case MA_REMOVETRAP:
	case HT_REMOVETRAP:
		//FIXME: I think clif_skill_fail() is supposed to be sent if it fails below [ultramage]
		clif_skill_nodamage(src, bl, skillid, skilllv, 1);
		{
			struct skill_unit* su;
			struct skill_unit_group* sg;
			su = BL_CAST(BL_SKILL, bl);

			// Mercenaries can remove any trap
			// Players can only remove their own traps or traps on Vs maps.
			if( su && (sg = su->group) && (src->type == BL_MER || sg->src_id == src->id || map_flag_vs(bl->m)) && (skill_get_inf2(sg->skill_id)&INF2_TRAP) )
			{
				if( sd && !(sg->unit_id == UNT_USED_TRAPS || (sg->unit_id == UNT_ANKLESNARE && sg->val2 != 0 )) )
				{ // prevent picking up expired traps
					if( battle_config.skill_removetrap_type )
					{ // get back all items used to deploy the trap
						for( i = 0; i < 10; i++ )
						{
							if( skill_db[su->group->skill_id].itemid[i] > 0 )
							{
								int flag;
								struct item item_tmp;
								memset(&item_tmp,0,sizeof(item_tmp));
								item_tmp.nameid = skill_db[su->group->skill_id].itemid[i];
								item_tmp.identify = 1;
								if( item_tmp.nameid && (flag=pc_additem(sd,&item_tmp,skill_db[su->group->skill_id].amount[i],LOG_TYPE_OTHER)) )
								{
									clif_additem(sd,0,0,flag);
									map_addflooritem(&item_tmp,skill_db[su->group->skill_id].amount[i],sd->bl.m,sd->bl.x,sd->bl.y,0,0,0,0,0);
								}
							}
						}
					}
					else
					{ // get back 1 trap
						struct item item_tmp;
						memset(&item_tmp,0,sizeof(item_tmp));
						item_tmp.nameid = ITEMID_TRAP;
						item_tmp.identify = 1;
						if( item_tmp.nameid && (flag=pc_additem(sd,&item_tmp,1,LOG_TYPE_OTHER)) )
						{
							clif_additem(sd,0,0,flag);
							map_addflooritem(&item_tmp,1,sd->bl.m,sd->bl.x,sd->bl.y,0,0,0,0,0);
						}
					}
				}
				skill_delunit(su);
			}
		}
		break;
	case HT_SPRINGTRAP:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		{
			struct skill_unit *su=NULL;
			if((bl->type==BL_SKILL) && (su=(struct skill_unit *)bl) && (su->group) ){
				switch(su->group->unit_id){
					case UNT_ANKLESNARE:	// ankle snare
						if (su->group->val2 != 0)
							// if it is already trapping something don't spring it,
							// remove trap should be used instead
							break;
						// otherwise fallthrough to below
					case UNT_BLASTMINE:
					case UNT_SKIDTRAP:
					case UNT_LANDMINE:
					case UNT_SHOCKWAVE:
					case UNT_SANDMAN:
					case UNT_FLASHER:
					case UNT_FREEZINGTRAP:
					case UNT_CLAYMORETRAP:
					case UNT_TALKIEBOX:
						su->group->unit_id = UNT_USED_TRAPS;
						clif_changetraplook(bl, UNT_USED_TRAPS);
						su->group->limit=DIFF_TICK(tick+1500,su->group->tick);
						su->limit=DIFF_TICK(tick+1500,su->group->tick);
				}
			}
		}
		break;
	case BD_ENCORE:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		if(sd)
			unit_skilluse_id(src,src->id,sd->skillid_dance,sd->skilllv_dance);
		break;

	case AS_SPLASHER:
		if(tstatus->mode&MD_BOSS || tstatus-> hp > tstatus->max_hp*3/4) {
			if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			map_freeblock_unlock();
			return 1;
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start4(bl,type,100,skilllv,skillid,src->id,skill_get_time(skillid,skilllv),1000));
		if (sd) skill_blockpc_start (sd, skillid, skill_get_time(skillid, skilllv)+3000);
		break;

	case PF_MINDBREAKER:
		{
			if(tstatus->mode&MD_BOSS || battle_check_undead(tstatus->race,tstatus->def_ele))
			{
				map_freeblock_unlock();
				return 1;
			}

			if (tsce)
			{	//HelloKitty2 (?) explained that this silently fails when target is
				//already inflicted. [Skotlex]
				map_freeblock_unlock();
				return 1;
			}

			//Has a 55% + skilllv*5% success chance.
			if (!clif_skill_nodamage(src,bl,skillid,skilllv,
				sc_start(bl,type,55+5*skilllv,skilllv,skill_get_time(skillid,skilllv))))
			{
				if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				map_freeblock_unlock();
				return 0;
			}

			unit_skillcastcancel(bl,0);

			if(tsc && tsc->count){
				status_change_end(bl, SC_FREEZE, INVALID_TIMER);
				if(tsc->data[SC_STONE] && tsc->opt1 == OPT1_STONE)
					status_change_end(bl, SC_STONE, INVALID_TIMER);
				status_change_end(bl, SC_SLEEP, INVALID_TIMER);
			}

			if(dstmd)
				mob_target(dstmd,src,skill_get_range2(src,skillid,skilllv));
		}
		break;

	case PF_SOULCHANGE:
		{
			unsigned int sp1 = 0, sp2 = 0;
			if (dstmd) {
				if (dstmd->state.soul_change_flag) {
					if(sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
					break;
				}
				dstmd->state.soul_change_flag = 1;
				sp2 = sstatus->max_sp * 3 /100;
				status_heal(src, 0, sp2, 2);
				clif_skill_nodamage(src,bl,skillid,skilllv,1);
				break;
			}
			sp1 = sstatus->sp;
			sp2 = tstatus->sp;
			status_set_sp(src, sp2, 3);
			status_set_sp(bl, sp1, 3);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
		}
		break;

	// Slim Pitcher
	case CR_SLIMPITCHER:
		// Updated to block Slim Pitcher from working on barricades and guardian stones.
		if( dstmd && (dstmd->class_ == MOBID_EMPERIUM || (dstmd->class_ >= 1905 && dstmd->class_ <= 1915) || (dstmd->class_ >= 2105 && dstmd->class_ <= 2107)) )
			break;
		if (potion_hp || potion_sp) {
			int hp = potion_hp, sp = potion_sp;
			hp = hp * (100 + (tstatus->vit<<1))/100;
			sp = sp * (100 + (tstatus->int_<<1))/100;
			if (dstsd) {
				if (hp)
					hp = hp * (100 + pc_checkskill(dstsd,SM_RECOVERY)*10 + pc_skillheal2_bonus(dstsd, skillid))/100;
				if (sp)
					sp = sp * (100 + pc_checkskill(dstsd,MG_SRECOVERY)*10 + pc_skillheal2_bonus(dstsd, skillid))/100;
			}
			if (tsc && tsc->data[SC_CRITICALWOUND])
			{
				hp -= hp * tsc->data[SC_CRITICALWOUND]->val2 / 100;
				sp -= sp * tsc->data[SC_CRITICALWOUND]->val2 / 100;
			}
			if(hp > 0)
				clif_skill_nodamage(NULL,bl,AL_HEAL,hp,1);
			if(sp > 0)
				clif_skill_nodamage(NULL,bl,MG_SRECOVERY,sp,1);
			status_heal(bl,hp,sp,0);
		}
		break;
	// Full Chemical Protection
	case CR_FULLPROTECTION:
		{
			int i, skilltime;
			skilltime = skill_get_time(skillid,skilllv);
			if (!tsc) {
				clif_skill_nodamage(src,bl,skillid,skilllv,0);
				break;
			}
			for (i=0; i<4; i++) {
				status_change_end(bl, (sc_type)(SC_STRIPWEAPON + i), INVALID_TIMER);
				sc_start(bl,(sc_type)(SC_CP_WEAPON + i),100,skilllv,skilltime);
			}
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
		}
		break;

	case RG_CLEANER:	//AppleGirl
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;

	case CG_LONGINGFREEDOM:
		{
			if (tsc && !tsce && (tsce=tsc->data[SC_DANCING]) && tsce->val4
				&& (tsce->val1&0xFFFF) != CG_MOONLIT) //Can't use Longing for Freedom while under Moonlight Petals. [Skotlex]
			{
				clif_skill_nodamage(src,bl,skillid,skilllv,
					sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
			}
		}
		break;

	case CG_TAROTCARD:
		{
			int eff, count = -1;
			if( rand() % 100 > skilllv * 8 || (dstmd && ((dstmd->guardian_data && dstmd->class_ == MOBID_EMPERIUM) || mob_is_battleground(dstmd))) )
			{
				if( sd )
					clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);

				map_freeblock_unlock();
				return 0;
			}
			status_zap(src,0,skill_db[skill_get_index(skillid)].sp[skilllv]); // consume sp only if succeeded [Inkfish]
			do {
				eff = rand() % 14;
				clif_specialeffect(bl, 523 + eff, AREA);
				switch (eff)
				{
				case 0:	// heals SP to 0
					status_percent_damage(src, bl, 0, 100, false);
					break;
				case 1:	// matk halved
					sc_start(bl,SC_INCMATKRATE,100,-50,skill_get_time2(skillid,skilllv));
					break;
				case 2:	// all buffs removed
					status_change_clear_buffs(bl,1);
					break;
				case 3:	// 1000 damage, random armor destroyed
					{
						int where[] = { EQP_ARMOR, EQP_SHIELD, EQP_HELM, EQP_SHOES, EQP_GARMENT };
						status_fix_damage(src, bl, 1000, 0, skillid);
						clif_damage(src,bl,tick,0,0,1000,0,0,0);
						if( !status_isdead(bl) )
							skill_break_equip(bl, where[rand()%5], 10000, BCT_ENEMY);
					}
					break;
				case 4:	// atk halved
					sc_start(bl,SC_INCATKRATE,100,-50,skill_get_time2(skillid,skilllv));
					break;
				case 5:	// 2000HP heal, random teleported
					status_heal(src, 2000, 0, 0);
					if( !map_flag_gvg3(bl->m) )
						unit_warp(bl, -1,-1,-1, CLR_TELEPORT);
					break;
				case 6:	// random 2 other effects
					if (count == -1)
						count = 3;
					else
						count++; //Should not retrigger this one.
					break;
				case 7:	// stop freeze or stoned
					{
						enum sc_type sc[] = { SC_STOP, SC_FREEZE, SC_STONE };
						sc_start(bl,sc[rand()%3],100,skilllv,skill_get_time2(skillid,skilllv));
					}
					break;
				case 8:	// curse coma and poison
					sc_start(bl,SC_COMA,100,skilllv,skill_get_time2(skillid,skilllv));
					sc_start(bl,SC_CURSE,100,skilllv,skill_get_time2(skillid,skilllv));
					sc_start(bl,SC_POISON,100,skilllv,skill_get_time2(skillid,skilllv));
					break;
				case 9:	// confusion
					sc_start(bl,SC_CONFUSION,100,skilllv,skill_get_time2(skillid,skilllv));
					break;
				case 10:	// 6666 damage, atk matk halved, cursed
					status_fix_damage(src, bl, 6666, 0, skillid);
					clif_damage(src,bl,tick,0,0,6666,0,0,0);
					sc_start(bl,SC_INCATKRATE,100,-50,skill_get_time2(skillid,skilllv));
					sc_start(bl,SC_INCMATKRATE,100,-50,skill_get_time2(skillid,skilllv));
					sc_start(bl,SC_CURSE,skilllv,100,skill_get_time2(skillid,skilllv));
					break;
				case 11:	// 4444 damage
					status_fix_damage(src, bl, 4444, 0, skillid);
					clif_damage(src,bl,tick,0,0,4444,0,0,0);
					break;
				case 12:	// stun
					sc_start(bl,SC_STUN,100,skilllv,5000);
					break;
				case 13:	// atk,matk,hit,flee,def reduced
					sc_start(bl,SC_INCATKRATE,100,-20,skill_get_time2(skillid,skilllv));
					sc_start(bl,SC_INCMATKRATE,100,-20,skill_get_time2(skillid,skilllv));
					sc_start(bl,SC_INCHITRATE,100,-20,skill_get_time2(skillid,skilllv));
					sc_start(bl,SC_INCFLEERATE,100,-20,skill_get_time2(skillid,skilllv));
					sc_start(bl,SC_INCDEFRATE,100,-20,skill_get_time2(skillid,skilllv));
					break;
				default:
					break;
				}
			} while ((--count) > 0);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
		}
		break;

	case SL_ALCHEMIST:
	case SL_ASSASIN:
	case SL_BARDDANCER:
	case SL_BLACKSMITH:
	case SL_CRUSADER:
	case SL_HUNTER:
	case SL_KNIGHT:
	case SL_MONK:
	case SL_PRIEST:
	case SL_ROGUE:
	case SL_SAGE:
	case SL_SOULLINKER:
	case SL_STAR:
	case SL_SUPERNOVICE:
	case SL_WIZARD:
		//NOTE: here, 'type' has the value of the associated MAPID, not of the SC_SPIRIT constant.
		if (sd && !(dstsd && (dstsd->class_&MAPID_UPPERMASK) == type)) {
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			break;
		}
		if (skillid == SL_SUPERNOVICE && dstsd && dstsd->die_counter && !(rand()%100))
		{	//Erase death count 1% of the casts
			dstsd->die_counter = 0;
			pc_setglobalreg(dstsd,"PC_DIE_COUNTER", 0);
			clif_specialeffect(bl, 0x152, AREA);
			//SC_SPIRIT invokes status_calc_pc for us.
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start4(bl,SC_SPIRIT,100,skilllv,skillid,0,0,skill_get_time(skillid,skilllv)));
		sc_start(src,SC_SMA,100,skilllv,skill_get_time(SL_SMA,skilllv));
		break;
	case SL_HIGH:
		if (sd && !(dstsd && (dstsd->class_&JOBL_UPPER) && !(dstsd->class_&JOBL_2) && dstsd->status.base_level < 70)) {
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			break;
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start4(bl,type,100,skilllv,skillid,0,0,skill_get_time(skillid,skilllv)));
		sc_start(src,SC_SMA,100,skilllv,skill_get_time(SL_SMA,skilllv));
		break;

	case SL_SWOO:
		if (tsce) {
			sc_start(src,SC_STUN,100,skilllv,10000);
			break;
		}
	case SL_SKA: // [marquis007]
	case SL_SKE:
		if (sd && !battle_config.allow_es_magic_pc && bl->type != BL_MOB) {
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			status_change_start(src,SC_STUN,10000,skilllv,0,0,0,500,10);
			break;
		}
		clif_skill_nodamage(src,bl,skillid,skilllv,sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		if (skillid == SL_SKE)
			sc_start(src,SC_SMA,100,skilllv,skill_get_time(SL_SMA,skilllv));
		break;

	// New guild skills [Celest]
	case GD_BATTLEORDER:
	case GD_REGENERATION:
	case GD_RESTORE:
		if( flag&1 )
		{
			if( (!map[src->m].flag.battleground && status_get_guild_id(src) == status_get_guild_id(bl)) || (map[src->m].flag.battleground && bg_team_get_id(src) == bg_team_get_id(bl)) )
			{
				if( skillid != GD_RESTORE )
					sc_start(bl, type, 100, skilllv, skill_get_time(skillid, skilllv));
				else
					clif_skill_nodamage(src,bl,AL_HEAL,status_percent_heal(bl,90,90),1);
			}
		}
		else if( !map[src->m].flag.battleground && (i = status_get_guild_id(src)) )
		{
			struct guild *g = guild_search(i);
			struct guild_castle *gc;

			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			map_foreachinrange(skill_area_sub, src, skill_get_splash(skillid, skilllv), BL_PC, src, skillid, skilllv, tick, flag|BCT_GUILD|1, skill_castend_nodamage_id);

			guild_block_skill_start(g, skillid, skill_get_time2(skillid,skilllv));
			if( g && (gc = guild_mapindex2gc(map[src->m].index)) != NULL )
			{
				switch( skillid )
				{
				case GD_BATTLEORDER:  add2limit(g->castle[gc->castle_id].skill_battleorder, 1, USHRT_MAX); break;
				case GD_REGENERATION: add2limit(g->castle[gc->castle_id].skill_regeneration, 1, USHRT_MAX); break;
				case GD_RESTORE:      add2limit(g->castle[gc->castle_id].skill_restore, 1, USHRT_MAX); break;
				}
				g->castle[gc->castle_id].changed = true;
			}
		}
		else if( map[src->m].flag.battleground && (i = bg_team_get_id(src)) )
		{
			struct battleground_data *bg = bg_team_search(i);
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			map_foreachinrange(skill_area_sub, src, skill_get_splash(skillid, skilllv), BL_PC, src, skillid, skilllv, tick, flag|BCT_GUILD|1, skill_castend_nodamage_id);
			bg_block_skill_start(bg, skillid, skill_get_time2(skillid,skilllv));
		}
		break;
	case GD_EMERGENCYCALL:
		{
			int dx[9] = {-1, 1, 0, 0,-1, 1,-1, 1, 0},
				dy[9] = { 0, 0, 1,-1, 1,-1,-1, 1, 0};

			int j = 0, count = 0;
			int in_i = 0, out_i = 0, cas_i = 0;
			struct guild *g = NULL;
			struct battleground_data *bg = NULL;
			struct guild_castle *gc = NULL;
			struct map_session_data
				*sd_in[MAX_GUILD], // Guild members in the same Castle
				*sd_cas[MAX_GUILD], // Guild members in other Castles
				*sd_out[MAX_GUILD], // Guild members in others maps
				*pl_sd;

			memset(sd_in, 0, sizeof(sd_in));
			memset(sd_out, 0, sizeof(sd_out));
			memset(sd_cas, 0, sizeof(sd_cas));

			if( sd && map[src->m].flag.battleground && (bg = sd->bmaster_flag) != NULL )
			{ // Battleground Usage
				for( i = 0; i < MAX_BG_MEMBERS; i++ )
				{
					if( (dstsd = bg->members[i].sd) == NULL || sd == dstsd || pc_isdead(dstsd) )
						continue;
					sd_in[in_i++] = dstsd;
					count++;
				}

				bg_block_skill_start(bg, skillid, skill_get_time2(skillid,skilllv));
			}
			else if( !map[src->m].flag.battleground && (g = guild_search(status_get_guild_id(src))) != NULL )
			{ // Normal Field or GvG Usage
				for( i = 0; i < g->max_member; i++ )
				{
					if( (dstsd = g->member[i].sd) == NULL || sd == dstsd || pc_isdead(dstsd) )
						continue;
					if( map[dstsd->bl.m].flag.nowarp && !map_flag_gvg2(dstsd->bl.m) )
						continue;
					if( map[sd->bl.m].flag.ancient && (!pc_class2ancientwoe(dstsd->status.class_) || dstsd->md) )
						continue; // Ancient WoE

					if( dstsd->bl.m == src->m )
						sd_in[in_i++] = dstsd;
					else if( map[dstsd->bl.m].flag.gvg_castle )
						sd_cas[cas_i++] = dstsd;
					else
						sd_out[out_i++] = dstsd;

					count++;
				}

				if( map[src->m].guild_max && count >= map[src->m].guild_max )
					count = map[src->m].guild_max - 1;

				guild_block_skill_start(g, skillid, skill_get_time2(skillid,skilllv));
				if( (gc = guild_mapindex2gc(map[src->m].index)) != NULL )
				{
					add2limit(g->castle[gc->castle_id].skill_emergencycall, 1, USHRT_MAX);
					g->castle[gc->castle_id].changed = true;
				}
			}

			clif_skill_nodamage(src, bl, skillid, skilllv, 1);

			for( i = 0; i < count; i++, j++ )
			{
				if( j > 8 ) j = 0; // Respawn point index
				if( map_getcell(src->m, src->x + dx[j], src->y + dy[j], CELL_CHKNOREACH) )
					dx[j] = dy[j] = 0; // Set it to the same source x,y
				
				if( i < in_i )
					pl_sd = sd_in[i];
				else if( i < in_i + out_i )
					pl_sd = sd_out[i - in_i];
				else if( i < in_i + out_i + cas_i )
					pl_sd = sd_cas[i - (in_i + out_i)];
				else continue; // Should not

				pc_setpos(pl_sd, map_id2index(src->m), src->x + dx[j], src->y + dy[j], CLR_RESPAWN);
			}
		}
		break;

	case SG_FEEL:
		//AuronX reported you CAN memorize the same map as all three. [Skotlex]
		if (sd) {
			if(!sd->feel_map[skilllv-1].index)
				clif_feel_req(sd->fd,sd, skilllv);
			else
				clif_feel_info(sd, skilllv-1, 1);
		}
		break;

	case SG_HATE:
		if (sd) {
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			if (!pc_set_hate_mob(sd, skilllv-1, bl))
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		}
		break;

	case GS_GLITTERING:
		if(sd) {
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			if(rand()%100 < (20+10*skilllv))
				pc_addspiritball(sd,skill_get_time(skillid,skilllv),10);
			else if(sd->spiritball > 0)
				pc_delspiritball(sd,1,0);
		}
		break;

	case GS_CRACKER:
		if (!dstsd)	// according to latest patch, should not work on players [Reddozen]
		{
			i =65 -5*distance_bl(src,bl); //Base rate
			if (i < 30) i = 30;
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			sc_start(bl,SC_STUN, i,skilllv,skill_get_time2(skillid,skilllv));
		}
		else if (sd)
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		break;

	case AM_CALLHOMUN:	//[orn]
		if (sd && !merc_call_homunculus(sd))
			clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		break;

	case AM_REST:
		if (sd)
		{
			if (merc_hom_vaporize(sd,1))
				clif_skill_nodamage(src, bl, skillid, skilllv, 1);
			else
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
		}
		break;

	case HAMI_CASTLE:	//[orn]
		if(rand()%100 < 20*skilllv && src != bl)
		{
			int x,y;
			x = src->x;
			y = src->y;
			if (hd)
				skill_blockhomun_start(hd, skillid, skill_get_time2(skillid,skilllv));

			if (unit_movepos(src,bl->x,bl->y,0,0)) {
				clif_skill_nodamage(src,src,skillid,skilllv,1); // Homunc
				clif_slide(src,bl->x,bl->y) ;
				if (unit_movepos(bl,x,y,0,0))
				{
					clif_skill_nodamage(bl,bl,skillid,skilllv,1); // Master
					clif_slide(bl,x,y) ;
				}

				//TODO: Shouldn't also players and the like switch targets?
				map_foreachinrange(skill_chastle_mob_changetarget,src,
					AREA_SIZE, BL_MOB, bl, src);
			}
		}
		// Failed
		else if (hd && hd->master)
			clif_skill_fail(hd->master, skillid, USESKILL_FAIL_LEVEL, 0);
		else if (sd)
			clif_skill_fail(sd, skillid, USESKILL_FAIL_LEVEL, 0);
		break;
	case HVAN_CHAOTIC:	//[orn]
		{
			static const int per[5][2]={{20,50},{50,60},{25,75},{60,64},{34,67}};
			int rnd = rand()%100;
			i = (skilllv-1)%5;
			if(rnd<per[i][0]) //Self
				bl = src;
			else if(rnd<per[i][1]) //Master
				bl = battle_get_master(src);
			else //Enemy
				bl = map_id2bl(battle_gettarget(src));

			if (!bl) bl = src;
			i = skill_calc_heal(src, bl, skillid, 1+rand()%skilllv, true);
			//Eh? why double skill packet?
			clif_skill_nodamage(src,bl,AL_HEAL,i,1);
			clif_skill_nodamage(src,bl,skillid,i,1);
			status_heal(bl, i, 0, 0);
		}
		break;
	//Homun single-target support skills [orn]
	case HAMI_BLOODLUST:
	case HFLI_FLEET:
	case HFLI_SPEED:
	case HLIF_CHANGE:
		clif_skill_nodamage(src,bl,skillid,skilllv,
			sc_start(bl,type,100,skilllv,skill_get_time(skillid,skilllv)));
		if (hd)
			skill_blockhomun_start(hd, skillid, skill_get_time2(skillid,skilllv));
		break;

	case NPC_DRAGONFEAR:
		if (flag&1) {
			const enum sc_type sc[] = { SC_STUN, SC_SILENCE, SC_CONFUSION, SC_BLEEDING };
			int j;
			j = i = rand()%ARRAYLENGTH(sc);
			while ( !sc_start(bl,sc[i],100,skilllv,skill_get_time2(skillid,i+1)) ) {
				i++;
				if ( i == ARRAYLENGTH(sc) )
					i = 0;
				if (i == j)
					break;
			}
			break;
		}
	case NPC_WIDEBLEEDING:
	case NPC_WIDECONFUSE:
	case NPC_WIDECURSE:
	case NPC_WIDEFREEZE:
	case NPC_WIDESLEEP:
	case NPC_WIDESILENCE:
	case NPC_WIDESTONE:
	case NPC_WIDESTUN:
	case NPC_SLOWCAST:
	case NPC_WIDEHELLDIGNITY:
		if (flag&1)
			sc_start(bl,type,100,skilllv,skill_get_time2(skillid,skilllv));
		else {
			skill_area_temp[2] = 0; //For SD_PREAMBLE
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			map_foreachinrange(skill_area_sub, bl,
				skill_get_splash(skillid, skilllv),BL_CHAR,
				src,skillid,skilllv,tick, flag|BCT_ENEMY|SD_PREAMBLE|1,
				skill_castend_nodamage_id);
		}
		break;
	case NPC_WIDESOULDRAIN:
		if (flag&1)
			status_percent_damage(src,bl,0,((skilllv-1)%5+1)*20,false);
		else {
			skill_area_temp[2] = 0; //For SD_PREAMBLE
			clif_skill_nodamage(src,bl,skillid,skilllv,1);
			map_foreachinrange(skill_area_sub, bl,
				skill_get_splash(skillid, skilllv),BL_CHAR,
				src,skillid,skilllv,tick, flag|BCT_ENEMY|SD_PREAMBLE|1,
				skill_castend_nodamage_id);
		}
		break;
	case NPC_TALK:
	case ALL_WEWISH:
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		break;
	case ALL_BUYING_STORE:
		if( sd )
		{// players only, skill allows 5 buying slots
			clif_skill_nodamage(src, bl, skillid, skilllv, buyingstore_setup(sd, MAX_BUYINGSTORE_SLOTS));
		}
		break;
	default:
		ShowWarning("skill_castend_nodamage_id: Unknown skill used:%d\n",skillid);
		clif_skill_nodamage(src,bl,skillid,skilllv,1);
		map_freeblock_unlock();
		return 1;
	}

	if( skill_get_inf(skillid)&INF_SUPPORT_SKILL && sd && dstsd && sd != dstsd )
	{
		if( map_allowed_woe(src->m) && sd->status.guild_id )
		{
			if( sd->status.guild_id == dstsd->status.guild_id || (!map[src->m].flag.gvg_noalliance && guild_isallied(sd->status.guild_id, dstsd->status.guild_id)) )
				add2limit(sd->status.wstats.support_skills_used, 1, UINT_MAX);
			else
				add2limit(sd->status.wstats.wrong_support_skills_used, 1, UINT_MAX);
		}
		else if( map[src->m].flag.battleground && sd->bg_id && dstsd->bg_id )
		{
			if( sd->bg_id == dstsd->bg_id )
				add2limit(sd->status.bgstats.support_skills_used, 1, UINT_MAX);
			else
				add2limit(sd->status.bgstats.wrong_support_skills_used, 1, UINT_MAX);
		}
	}

	if (dstmd) { //Mob skill event for no damage skills (damage ones are handled in battle_calc_damage) [Skotlex]
		mob_log_damage(dstmd, src, 0); //Log interaction (counts as 'attacker' for the exp bonus)
		mobskill_event(dstmd, src, tick, MSC_SKILLUSED|(skillid<<16));
	}

	if( sd && !(flag&1) )
	{
		if( sd->state.arrow_atk ) //Consume arrow on last invocation to this skill.
			battle_consume_ammo(sd, skillid, skilllv);
		skill_onskillusage(sd, bl, skillid, tick);
		skill_consume_requirement(sd,skillid,skilllv,2);
	}

	map_freeblock_unlock();
	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_castend_id(int tid, unsigned int tick, int id, intptr_t data)
{
	struct block_list *target, *src;
	struct map_session_data *sd;
	struct mob_data *md;
	struct unit_data *ud;
	struct status_change *sc = NULL;
	int inf,inf2,flag = 0;

	src = map_id2bl(id);
	if( src == NULL )
	{
		ShowDebug("skill_castend_id: src == NULL (tid=%d, id=%d)\n", tid, id);
		return 0;// not found
	}

	ud = unit_bl2ud(src);
	if( ud == NULL )
	{
		ShowDebug("skill_castend_id: ud == NULL (tid=%d, id=%d)\n", tid, id);
		return 0;// ???
	}

	sd = BL_CAST(BL_PC,  src);
	md = BL_CAST(BL_MOB, src);

	if( src->prev == NULL ) {
		ud->skilltimer = INVALID_TIMER;
		return 0;
	}

	if(ud->skillid != SA_CASTCANCEL )
	{// otherwise handled in unit_skillcastcancel()
		if( ud->skilltimer != tid ) {
			ShowError("skill_castend_id: Timer mismatch %d!=%d!\n", ud->skilltimer, tid);
			ud->skilltimer = INVALID_TIMER;
			return 0;
		}

		if( sd && ud->skilltimer != INVALID_TIMER && pc_checkskill(sd,SA_FREECAST) > 0 )
		{// restore original walk speed
			ud->skilltimer = INVALID_TIMER;
			status_calc_bl(&sd->bl, SCB_SPEED);
		}

		ud->skilltimer = INVALID_TIMER;
	}

	if (ud->skilltarget == id)
		target = src;
	else
		target = map_id2bl(ud->skilltarget);

	// Use a do so that you can break out of it when the skill fails.
	do {
		if(!target || target->prev==NULL) break;

		if(src->m != target->m || status_isdead(src)) break;

		switch (ud->skillid) {
			//These should become skill_castend_pos
			case WE_CALLPARTNER:
				if(sd) clif_callpartner(sd);
			case WE_CALLPARENT:
			case WE_CALLBABY:
			case AM_RESURRECTHOMUN:
			case PF_SPIDERWEB:
				//Find a random spot to place the skill. [Skotlex]
				inf2 = skill_get_splash(ud->skillid, ud->skilllv);
				ud->skillx = target->x + inf2;
				ud->skilly = target->y + inf2;
				if (inf2 && !map_random_dir(target, &ud->skillx, &ud->skilly)) {
					ud->skillx = target->x;
					ud->skilly = target->y;
				}
				ud->skilltimer=tid;
				return skill_castend_pos(tid,tick,id,data);
		}

		if(ud->skillid == RG_BACKSTAP) {
			int dir = map_calc_dir(src,target->x,target->y),t_dir = unit_getdir(target);
			if(check_distance_bl(src, target, 0) || map_check_dir(dir,t_dir)) {
				break;
			}
		}

		if( ud->skillid == PR_TURNUNDEAD )
		{
			struct status_data *tstatus = status_get_status_data(target);
			if( !battle_check_undead(tstatus->race, tstatus->def_ele) )
				break;
		}

		if( ud->skillid == PR_LEXDIVINA || ud->skillid == MER_LEXDIVINA )
		{
			sc = status_get_sc(target);
			if( battle_check_target(src,target, BCT_ENEMY) <= 0 && (!sc || !sc->data[SC_SILENCE]) )
			{ //If it's not an enemy, and not silenced, you can't use the skill on them. [Skotlex]
				clif_skill_nodamage (src, target, ud->skillid, ud->skilllv, 0);
				break;
			}
		}
		else
		{ // Check target validity.
			inf = skill_get_inf(ud->skillid);
			inf2 = skill_get_inf2(ud->skillid);

			if(inf&INF_ATTACK_SKILL ||
				(inf&INF_SELF_SKILL && inf2&INF2_NO_TARGET_SELF)) //Combo skills
				inf = BCT_ENEMY; //Offensive skill.
			else if(inf2&INF2_NO_ENEMY)
				inf = BCT_NOENEMY;
			else
				inf = 0;

			if(inf2 & (INF2_PARTY_ONLY|INF2_GUILD_ONLY) && src != target)
			{
				inf |=
					(inf2&INF2_PARTY_ONLY?BCT_PARTY:0)|
					(inf2&INF2_GUILD_ONLY?BCT_GUILD:0);
				//Remove neutral targets (but allow enemy if skill is designed to be so)
				inf &= ~BCT_NEUTRAL;
			}

			if( ud->skillid >= SL_SKE && ud->skillid <= SL_SKA && target->type == BL_MOB )
			{
				if( ((TBL_MOB*)target)->class_ == MOBID_EMPERIUM )
					break;
			}
			else
			if (inf && battle_check_target(src, target, inf) <= 0)
				break;

			if(inf&BCT_ENEMY && (sc = status_get_sc(target)) &&
				sc->data[SC_FOGWALL] &&
				rand()%100 < 75)
		  	{	//Fogwall makes all offensive-type targetted skills fail at 75%
				if (sd) clif_skill_fail(sd,ud->skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
		}

		//Avoid doing double checks for instant-cast skills.
		if (tid != INVALID_TIMER && !status_check_skilluse(src, target, ud->skillid, ud->skilllv, 1))
			break;

		if(md) {
			md->last_thinktime=tick +MIN_MOBTHINKTIME;
			if(md->skillidx >= 0 && md->db->skill[md->skillidx].emotion >= 0)
				clif_emotion(src, md->db->skill[md->skillidx].emotion);
		}

		if(src != target && battle_config.skill_add_range &&
			!check_distance_bl(src, target, skill_get_range2(src,ud->skillid,ud->skilllv)+battle_config.skill_add_range))
		{
			if (sd) {
				clif_skill_fail(sd,ud->skillid,USESKILL_FAIL_LEVEL,0);
				if(battle_config.skill_out_range_consume) //Consume items anyway. [Skotlex]
					skill_consume_requirement(sd,ud->skillid,ud->skilllv,3);
			}
			break;
		}

		if( sd )
		{
			if( !skill_check_condition_castend(sd, ud->skillid, ud->skilllv) )
				break;
			else
				skill_consume_requirement(sd,ud->skillid,ud->skilllv,1);
		}

		if( (src->type == BL_MER || src->type == BL_HOM) && !skill_check_condition_mercenary(src, ud->skillid, ud->skilllv, 1) )
			break;

		if (ud->state.running && ud->skillid == TK_JUMPKICK)
			flag = 1;

		if (ud->walktimer != INVALID_TIMER && ud->skillid != TK_RUN)
			unit_stop_walking(src,1);

		if( sd && skill_get_cooldown(ud->skillid,ud->skilllv) > 0 ) // Skill cooldown. [LimitLine]
			skill_blockpc_start(sd, ud->skillid, skill_get_cooldown(ud->skillid, ud->skilllv));
		if( !sd || sd->skillitem != ud->skillid || skill_get_delay(ud->skillid,ud->skilllv) )
			ud->canact_tick = tick + skill_delayfix(src, ud->skillid, ud->skilllv); //Tests show wings don't overwrite the delay but skill scrolls do. [Inkfish]

#ifdef ADELAYS
	// Adelays in skill_castend_id (1/2)
		if (adelays_is_enabled() && sd) {

	// Record the delay.
			adelays_recordSkillDelay(ud->skillid, skill_delayfix(src, ud->skillid, ud->skilllv), skill_get_cooldown(ud->skillid,ud->skilllv), &(sd->adelays_state), tick, sd->battle_status.amotion, sd->status.class_, sd->status.sex, sd->sc.option&OPTION_RIDING,battle_config.min_skill_delay_limit);
			int64 cannact_tick64 = (int64) ud->canact_tick;
			int action = adelays_process_record(tick, &(cannact_tick64), &(sd->adelays_state), battle_config.min_skill_delay_limit);
			ud->canact_tick = (unsigned int) cannact_tick64;

			char message[256];
	// Show the log in game.
		if (sd->adelays_state.adelays_showinfo == 1) {
			adelays_get_log_message(message, &(sd->adelays_state));
			clif_disp_overhead(sd, message);
		}


		if (action == 2)
		{
		// Skill was cancelled.
		break;
		}

	}
#endif
	// Adelays End.		

		if( battle_config.display_status_timers && sd )
			clif_status_change(src, SI_ACTIONDELAY, 1, skill_delayfix(src, ud->skillid, ud->skilllv));
		if( sd && sd->skillitem != ud->skillid )
		{ // Skill Usage Counter
			int i;
			if( map_allowed_woe(sd->bl.m) )
			{
				ARR_FIND(0,MAX_SKILL_TREE,i,sd->status.skillcount[i].id == ud->skillid || !sd->status.skillcount[i].id);
				if( i < MAX_SKILL_TREE )
				{
					sd->status.skillcount[i].id = ud->skillid;
					sd->status.skillcount[i].count++;
				}
			}
			else if( map[sd->bl.m].flag.battleground && sd->bg_id )
			{
				ARR_FIND(0,MAX_SKILL_TREE,i,sd->status.bg_skillcount[i].id == ud->skillid || !sd->status.bg_skillcount[i].id);
				if( i < MAX_SKILL_TREE )
				{
					sd->status.bg_skillcount[i].id = ud->skillid;
					sd->status.bg_skillcount[i].count++;
				}
			}
		}

		if( sd )
		{
			switch( ud->skillid )
			{
			case GS_DESPERADO:
				sd->canequip_tick = tick + skill_get_time(ud->skillid, ud->skilllv);
				break;
			case CR_GRANDCROSS:
			case NPC_GRANDDARKNESS:
				if( (sc = status_get_sc(src)) && sc->data[SC_STRIPSHIELD] )
				{
					const struct TimerData *timer = get_timer(sc->data[SC_STRIPSHIELD]->timer);
					if( timer && timer->func == status_change_timer && DIFF_TICK(timer->tick,gettick()+skill_get_time(ud->skillid, ud->skilllv)) > 0 )
						break;
				}
				sc_start2(src, SC_STRIPSHIELD, 100, 0, 1, skill_get_time(ud->skillid, ud->skilllv));
				break;
			}
		}
		if (skill_get_state(ud->skillid) != ST_MOVE_ENABLE)
			unit_set_walkdelay(src, tick, battle_config.default_walk_delay+skill_get_walkdelay(ud->skillid, ud->skilllv), 1);

		if(battle_config.skill_log && battle_config.skill_log&src->type)
			ShowInfo("Type %d, ID %d skill castend id [id =%d, lv=%d, target ID %d]\n",
				src->type, src->id, ud->skillid, ud->skilllv, target->id);

		map_freeblock_lock();
		if (skill_get_casttype(ud->skillid) == CAST_NODAMAGE)
			skill_castend_nodamage_id(src,target,ud->skillid,ud->skilllv,tick,flag);
		else
			skill_castend_damage_id(src,target,ud->skillid,ud->skilllv,tick,flag);

		sc = status_get_sc(src);
		if(sc && sc->count) {
		  	if(sc->data[SC_MAGICPOWER] &&
				ud->skillid != HW_MAGICPOWER && ud->skillid != WZ_WATERBALL)
				status_change_end(src, SC_MAGICPOWER, INVALID_TIMER);
			if(sc->data[SC_SPIRIT] &&
				sc->data[SC_SPIRIT]->val2 == SL_WIZARD &&
				sc->data[SC_SPIRIT]->val3 == ud->skillid &&
			  	ud->skillid != WZ_WATERBALL)
				sc->data[SC_SPIRIT]->val3 = 0; //Clear bounced spell check.

			if( sc->data[SC_DANCING] && skill_get_inf2(ud->skillid)&INF2_SONG_DANCE && sd )
				skill_blockpc_start(sd,BD_ADAPTATION,3000);
		}

		if( sd && ud->skillid != SA_ABRACADABRA ) // Hocus-Pocus has just set the data so leave it as it is.[Inkfish]
			sd->skillitem = sd->skillitemlv = 0;

		if (ud->skilltimer == INVALID_TIMER) {
			if(md) md->skillidx = -1;
			else ud->skillid = 0; //mobs can't clear this one as it is used for skill condition 'afterskill'
			ud->skilllv = ud->skilltarget = 0;
		}
		map_freeblock_unlock();
		return 1;
	} while(0);

#ifdef ADELAYS
    // Adelays in skill_castend_id (2/2)
	if(sd && adelays_is_enabled()) adelays_skillFailed(&(sd->adelays_state));
    // Adelays End.
#endif

	//Skill failed.
	if (ud->skillid == MO_EXTREMITYFIST && sd && !(sc && sc->data[SC_FOGWALL]))
  	{	//When Asura fails... (except when it fails from Fog of Wall)
		//Consume SP/spheres
		skill_consume_requirement(sd,ud->skillid, ud->skilllv,1);
		status_set_sp(src, 0, 0);
		sc = &sd->sc;
		if (sc->count)
		{	//End states
			status_change_end(src, SC_EXPLOSIONSPIRITS, INVALID_TIMER);
			status_change_end(src, SC_BLADESTOP, INVALID_TIMER);
		}
		if (target && target->m == src->m)
		{	//Move character to target anyway.
			int dx,dy;
			dx = target->x - src->x;
			dy = target->y - src->y;
			if(dx > 0) dx++;
			else if(dx < 0) dx--;
			if (dy > 0) dy++;
			else if(dy < 0) dy--;

			if (unit_movepos(src, src->x+dx, src->y+dy, 1, 1))
			{	//Display movement + animation.
				clif_slide(src,src->x,src->y);
				clif_skill_damage(src,target,tick,sd->battle_status.amotion,0,0,1,ud->skillid, ud->skilllv, 5);
			}
			clif_skill_fail(sd,ud->skillid,USESKILL_FAIL_LEVEL,0);
		}
	}

	ud->skillid = ud->skilllv = ud->skilltarget = 0;
	if( !sd || sd->skillitem != ud->skillid || skill_get_delay(ud->skillid,ud->skilllv) )
		ud->canact_tick = tick;
	//You can't place a skill failed packet here because it would be
	//sent in ALL cases, even cases where skill_check_condition fails
	//which would lead to double 'skill failed' messages u.u [Skotlex]
	if(sd)
		sd->skillitem = sd->skillitemlv = 0;
	else if(md)
		md->skillidx = -1;
	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_castend_pos(int tid, unsigned int tick, int id, intptr_t data)
{
	struct block_list* src = map_id2bl(id);
	int maxcount;
	struct map_session_data *sd;
	struct unit_data *ud = unit_bl2ud(src);
	struct mob_data *md;

	nullpo_ret(ud);

	if( src == NULL || ud == NULL ) return 0; // Temporal Fix

	sd = BL_CAST(BL_PC , src);
	md = BL_CAST(BL_MOB, src);

	if( src->prev == NULL ) {
		ud->skilltimer = INVALID_TIMER;
		return 0;
	}

	if( ud->skilltimer != tid )
	{
		ShowError("skill_castend_pos: Timer mismatch %d!=%d\n", ud->skilltimer, tid);
		ud->skilltimer = INVALID_TIMER;
		return 0;
	}

	if( sd && ud->skilltimer != INVALID_TIMER && pc_checkskill(sd,SA_FREECAST) > 0 )
	{// restore original walk speed
		ud->skilltimer = INVALID_TIMER;
		status_calc_bl(&sd->bl, SCB_SPEED);
	}
	ud->skilltimer = INVALID_TIMER;

	do {
		if( status_isdead(src) )
			break;

		if( !(src->type&battle_config.skill_reiteration) &&
			skill_get_unit_flag(ud->skillid)&UF_NOREITERATION &&
			skill_check_unit_range(src,ud->skillx,ud->skilly,ud->skillid,ud->skilllv)
		  )
		{
			if (sd) clif_skill_fail(sd,ud->skillid,USESKILL_FAIL_LEVEL,0);
			break;
		}
		if( src->type&battle_config.skill_nofootset &&
			skill_get_unit_flag(ud->skillid)&UF_NOFOOTSET &&
			skill_check_unit_range2(src,ud->skillx,ud->skilly,ud->skillid,ud->skilllv)
		  )
		{
			if (sd) clif_skill_fail(sd,ud->skillid,USESKILL_FAIL_LEVEL,0);
			break;
		}
		if( src->type&battle_config.land_skill_limit &&
			(maxcount = skill_get_maxcount(ud->skillid, ud->skilllv)) > 0
		  ) {
			int i;
			for(i=0;i<MAX_SKILLUNITGROUP && ud->skillunit[i] && maxcount;i++) {
				if(ud->skillunit[i]->skill_id == ud->skillid)
					maxcount--;
			}
			if( maxcount == 0 )
			{
				if (sd) clif_skill_fail(sd,ud->skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
		}

		if(tid != INVALID_TIMER)
		{	//Avoid double checks on instant cast skills. [Skotlex]
			if( !status_check_skilluse(src, NULL, ud->skillid, ud->skilllv, 1) )
				break;
			if(battle_config.skill_add_range &&
				!check_distance_blxy(src, ud->skillx, ud->skilly, skill_get_range2(src,ud->skillid,ud->skilllv)+battle_config.skill_add_range)) {
				if (sd && battle_config.skill_out_range_consume) //Consume items anyway.
					skill_consume_requirement(sd,ud->skillid,ud->skilllv,3);
				break;
			}
		}

		if( sd )
		{
			if( !skill_check_condition_castend(sd, ud->skillid, ud->skilllv) )
				break;
			else
				skill_consume_requirement(sd,ud->skillid,ud->skilllv,1);
		}

		if( (src->type == BL_MER || src->type == BL_HOM) && !skill_check_condition_mercenary(src, ud->skillid, ud->skilllv, 1) )
			break;

		if(md) {
			md->last_thinktime=tick +MIN_MOBTHINKTIME;
			if(md->skillidx >= 0 && md->db->skill[md->skillidx].emotion >= 0)
				clif_emotion(src, md->db->skill[md->skillidx].emotion);
		}

		if(battle_config.skill_log && battle_config.skill_log&src->type)
			ShowInfo("Type %d, ID %d skill castend pos [id =%d, lv=%d, (%d,%d)]\n",
				src->type, src->id, ud->skillid, ud->skilllv, ud->skillx, ud->skilly);

		if (ud->walktimer != INVALID_TIMER)
			unit_stop_walking(src,1);

		if( sd && skill_get_cooldown(ud->skillid,ud->skilllv) > 0 ) // Skill cooldown. [LimitLine]
			skill_blockpc_start(sd, ud->skillid, skill_get_cooldown(ud->skillid, ud->skilllv));
		if( !sd || sd->skillitem != ud->skillid || skill_get_delay(ud->skillid,ud->skilllv) )
			ud->canact_tick = tick + skill_delayfix(src, ud->skillid, ud->skilllv);
   
	// Adelays skill_castend_pos (1/2)
#ifdef ADELAYS
		if (adelays_is_enabled() && sd) {
	// Record the delay.
			adelays_recordSkillDelay(ud->skillid, skill_delayfix(src, ud->skillid, ud->skilllv), skill_get_cooldown(ud->skillid,ud->skilllv), &(sd->adelays_state), tick, sd->battle_status.amotion, sd->status.class_, sd->status.sex, sd->sc.option&OPTION_RIDING,battle_config.min_skill_delay_limit);
			int64 cannact_tick64 = (int64) ud->canact_tick;
			int action = adelays_process_record(tick, &(cannact_tick64), &(sd->adelays_state), battle_config.min_skill_delay_limit);
			ud->canact_tick = (unsigned int) cannact_tick64;

			char message[256];
	// Show the log in game.
			if (sd->adelays_state.adelays_showinfo == 1) {
				adelays_get_log_message(message, &(sd->adelays_state));
				clif_disp_overhead(sd, message);
			}

			if (action == 2)
			{
				// Skill was cancelled.
				break;
			}

		}
#endif
	// Adelays End.

		if( battle_config.display_status_timers && sd )
			clif_status_change(src, SI_ACTIONDELAY, 1, skill_delayfix(src, ud->skillid, ud->skilllv));
		if( sd && sd->skillitem != ud->skillid )
		{ // Skill Usage Counter
			int i;
			if( map_allowed_woe(sd->bl.m) )
			{
				ARR_FIND(0,MAX_SKILL_TREE,i,sd->status.skillcount[i].id == ud->skillid || !sd->status.skillcount[i].id);
				if( i < MAX_SKILL_TREE )
				{
					sd->status.skillcount[i].id = ud->skillid;
					sd->status.skillcount[i].count++;
				}
			}
			else if( map[sd->bl.m].flag.battleground && sd->bg_id )
			{
				ARR_FIND(0,MAX_SKILL_TREE,i,sd->status.bg_skillcount[i].id == ud->skillid || !sd->status.bg_skillcount[i].id);
				if( i < MAX_SKILL_TREE )
				{
					sd->status.bg_skillcount[i].id = ud->skillid;
					sd->status.bg_skillcount[i].count++;
				}
			}
		}
		unit_set_walkdelay(src, tick, battle_config.default_walk_delay+skill_get_walkdelay(ud->skillid, ud->skilllv), 1);

		map_freeblock_lock();
		skill_castend_pos2(src,ud->skillx,ud->skilly,ud->skillid,ud->skilllv,tick,0);

		if( sd && sd->skillitem != AL_WARP ) // Warp-Portal thru items will clear data in skill_castend_map. [Inkfish]
			sd->skillitem = sd->skillitemlv = 0;

		if (ud->skilltimer == INVALID_TIMER) {
			if (md) md->skillidx = -1;
			else ud->skillid = 0; //Non mobs can't clear this one as it is used for skill condition 'afterskill'
			ud->skilllv = ud->skillx = ud->skilly = 0;
		}

		map_freeblock_unlock();
		return 1;
	} while(0);

// Adelays in skill_castend_pos (2/2)
#ifdef ADELAYS
	if(adelays_is_enabled() && sd) adelays_skillFailed(&(sd->adelays_state));
#endif
// Adelays End.

	if( !sd || sd->skillitem != ud->skillid || skill_get_delay(ud->skillid,ud->skilllv) )
		ud->canact_tick = tick;
	ud->skillid = ud->skilllv = 0;
	if(sd)
		sd->skillitem = sd->skillitemlv = 0;
	else if(md)
		md->skillidx  = -1;
	return 0;

}

/*==========================================
 *
 *------------------------------------------*/
int skill_castend_pos2(struct block_list* src, int x, int y, int skillid, int skilllv, unsigned int tick, int flag)
{
	struct map_session_data* sd;
	struct status_change* sc;
	struct status_change_entry *sce;
	struct skill_unit_group* sg;
	enum sc_type type;
	int i;

	//if(skilllv <= 0) return 0;
	if(skillid > 0 && skilllv <= 0) return 0;	// celest

	nullpo_ret(src);

	if(status_isdead(src))
		return 0;

	sd = BL_CAST(BL_PC, src);

	sc = status_get_sc(src);
	type = status_skill2sc(skillid);
	sce = (sc && type != -1)?sc->data[type]:NULL;

	switch (skillid) { //Skill effect.
		case WZ_METEOR:
		case MO_BODYRELOCATION:
		case CR_CULTIVATION:
		case HW_GANBANTEIN:
			break; //Effect is displayed on respective switch case.
		default:
			if(skill_get_inf(skillid)&INF_SELF_SKILL)
				clif_skill_nodamage(src,src,skillid,skilllv,1);
			else
				clif_skill_poseffect(src,skillid,skilllv,x,y,tick);
	}

	switch(skillid)
	{
	case PR_BENEDICTIO:
		skill_area_temp[1] = src->id;
		i = skill_get_splash(skillid, skilllv);
		map_foreachinarea(skill_area_sub,
			src->m, x-i, y-i, x+i, y+i, BL_PC,
			src, skillid, skilllv, tick, flag|BCT_ALL|1,
			skill_castend_nodamage_id);
		map_foreachinarea(skill_area_sub,
			src->m, x-i, y-i, x+i, y+i, BL_CHAR,
			src, skillid, skilllv, tick, flag|BCT_ENEMY|1,
			skill_castend_damage_id);
		break;

	case BS_HAMMERFALL:
		i = skill_get_splash(skillid, skilllv);
		map_foreachinarea (skill_area_sub,
			src->m, x-i, y-i, x+i, y+i, BL_CHAR,
			src, skillid, skilllv, tick, flag|BCT_ENEMY|2,
			skill_castend_nodamage_id);
		break;

	case HT_DETECTING:
		i = skill_get_splash(skillid, skilllv);
		map_foreachinarea( status_change_timer_sub,
			src->m, x-i, y-i, x+i,y+i,BL_CHAR,
			src,NULL,SC_SIGHT,tick);
		if(battle_config.traps_setting&1)
		map_foreachinarea( skill_reveal_trap,
			src->m, x-i, y-i, x+i,y+i,BL_SKILL);
		break;

	case SA_VOLCANO:
	case SA_DELUGE:
	case SA_VIOLENTGALE:
	{	//Does not consumes if the skill is already active. [Skotlex]
		struct skill_unit_group *sg;
		if ((sg= skill_locate_element_field(src)) != NULL && ( sg->skill_id == SA_VOLCANO || sg->skill_id == SA_DELUGE || sg->skill_id == SA_VIOLENTGALE ))
		{
			if (sg->limit - DIFF_TICK(gettick(), sg->tick) > 0)
			{
				skill_unitsetting(src,skillid,skilllv,x,y,0);
				return 0; // not to consume items
			}
			else
				sg->limit = 0; //Disable it.
		}
		skill_unitsetting(src,skillid,skilllv,x,y,0);
		break;
	}
	case MG_SAFETYWALL:
	case MG_FIREWALL:
	case MG_THUNDERSTORM:
	case AL_PNEUMA:
	case WZ_ICEWALL:
	case WZ_FIREPILLAR:
	case WZ_QUAGMIRE:
	case WZ_VERMILION:
	case WZ_STORMGUST:
	case WZ_HEAVENDRIVE:
	case PR_SANCTUARY:
	case PR_MAGNUS:
	case CR_GRANDCROSS:
	case NPC_GRANDDARKNESS:
	case HT_SKIDTRAP:
	case MA_SKIDTRAP:
	case HT_LANDMINE:
	case MA_LANDMINE:
	case HT_ANKLESNARE:
	case HT_SHOCKWAVE:
	case HT_SANDMAN:
	case MA_SANDMAN:
	case HT_FLASHER:
	case HT_FREEZINGTRAP:
	case MA_FREEZINGTRAP:
	case HT_BLASTMINE:
	case HT_CLAYMORETRAP:
	case AS_VENOMDUST:
	case AM_DEMONSTRATION:
	case PF_FOGWALL:
	case PF_SPIDERWEB:
	case HT_TALKIEBOX:
	case WE_CALLPARTNER:
	case WE_CALLPARENT:
	case WE_CALLBABY:
	case AC_SHOWER:	//Ground-placed skill implementation.
	case MA_SHOWER:
	case SA_LANDPROTECTOR:
	case BD_LULLABY:
	case BD_RICHMANKIM:
	case BD_ETERNALCHAOS:
	case BD_DRUMBATTLEFIELD:
	case BD_RINGNIBELUNGEN:
	case BD_ROKISWEIL:
	case BD_INTOABYSS:
	case BD_SIEGFRIED:
	case BA_DISSONANCE:
	case BA_POEMBRAGI:
	case BA_WHISTLE:
	case BA_ASSASSINCROSS:
	case BA_APPLEIDUN:
	case DC_UGLYDANCE:
	case DC_HUMMING:
	case DC_DONTFORGETME:
	case DC_FORTUNEKISS:
	case DC_SERVICEFORYOU:
	case CG_MOONLIT:
	case GS_DESPERADO:
	case NJ_KAENSIN:
	case NJ_BAKUENRYU:
	case NJ_SUITON:
	case NJ_HYOUSYOURAKU:
	case NJ_RAIGEKISAI:
	case NJ_KAMAITACHI:
	case NPC_EVILLAND:
		flag|=1;//Set flag to 1 to prevent deleting ammo (it will be deleted on group-delete).
	case GS_GROUNDDRIFT: //Ammo should be deleted right away.
		skill_unitsetting(src,skillid,skilllv,x,y,0);
		break;
	case RG_GRAFFITI:			/* Graffiti [Valaris] */
		skill_clear_unitgroup(src);
		skill_unitsetting(src,skillid,skilllv,x,y,0);
		flag|=1;
		break;
	case HP_BASILICA:
		if( sc->data[SC_BASILICA] )
			status_change_end(src, SC_BASILICA, INVALID_TIMER); // Cancel Basilica
		else
		{ // Create Basilica. Start SC on caster. Unit timer start SC on others.
			skill_clear_unitgroup(src);
			if( skill_unitsetting(src,skillid,skilllv,x,y,0) )
				sc_start4(src,type,100,skilllv,0,0,src->id,skill_get_time(skillid,skilllv));
			flag|=1;
		}
		break;
	case CG_HERMODE:
		skill_clear_unitgroup(src);
		if ((sg = skill_unitsetting(src,skillid,skilllv,x,y,0)))
			sc_start4(src,SC_DANCING,100,
				skillid,0,skilllv,sg->group_id,skill_get_time(skillid,skilllv));
		flag|=1;
		break;
	case RG_CLEANER: // [Valaris]
		i = skill_get_splash(skillid, skilllv);
		map_foreachinarea(skill_graffitiremover,src->m,x-i,y-i,x+i,y+i,BL_SKILL);
		break;

	case WZ_METEOR:
		{
			int flag = 0, area = skill_get_splash(skillid, skilllv);
			short tmpx = 0, tmpy = 0, x1 = 0, y1 = 0;

			if( sc && sc->data[SC_MAGICPOWER] )
				flag = flag|2; //Store the magic power flag for future use. [Skotlex]

			for( i = 0; i < 2 + (skilllv>>1); i++ )
			{
				// Creates a random Cell in the Splash Area
				tmpx = x - area + rand()%(area * 2 + 1);
				tmpy = y - area + rand()%(area * 2 + 1);

				if( i == 0 && path_search_long(NULL, src->m, src->x, src->y, tmpx, tmpy, CELL_CHKWALL) )
					clif_skill_poseffect(src,skillid,skilllv,tmpx,tmpy,tick);

				if( i > 0 )
					skill_addtimerskill(src,tick+i*1000,0,tmpx,tmpy,skillid,skilllv,(x1<<16)|y1,flag&2); //Only pass the Magic Power flag

				x1 = tmpx;
				y1 = tmpy;
			}

			skill_addtimerskill(src,tick+i*1000,0,tmpx,tmpy,skillid,skilllv,-1,flag&2);
		}
		break;

	case AL_WARP:
		if(sd)
		{
			clif_skill_warppoint(sd, skillid, skilllv, sd->status.save_point.map,
				(skilllv >= 2) ? sd->status.memo_point[0].map : 0,
				(skilllv >= 3) ? sd->status.memo_point[1].map : 0,
				(skilllv >= 4) ? sd->status.memo_point[2].map : 0
			);
		}
		return 0; // not to consume item.

	case MO_BODYRELOCATION:
		if(sd && sd->sc.data[SC_ANKLE] ){
			clif_skill_fail(sd,skillid,0,0);
			return 0;
		}
		if (unit_movepos(src, x, y, 1, 1)) {
			clif_skill_poseffect(src,skillid,skilllv,src->x,src->y,tick);
//			clif_slide(src, src->x, src->y); //Poseffect is the one that makes the char snap on the client...
			if (sd) skill_blockpc_start (sd, MO_EXTREMITYFIST, 2000);
		}
		break;
	case NJ_SHADOWJUMP:
		if( !map_flag_gvg(src->m) && !map[src->m].flag.battleground )
		{	//You don't move on GVG grounds.
			unit_movepos(src, x, y, 1, 0);
			clif_slide(src,x,y);
		}
		status_change_end(src, SC_HIDING, INVALID_TIMER);
		break;
	case AM_SPHEREMINE:
	case AM_CANNIBALIZE:
		{
			int summons[5] = { 1589, 1579, 1575, 1555, 1590 };
			//int summons[5] = { 1020, 1068, 1118, 1500, 1368 };
			int class_ = skillid==AM_SPHEREMINE?1142:summons[skilllv-1];
			struct mob_data *md;

			// Correct info, don't change any of this! [celest]
			md = mob_once_spawn_sub(src, src->m, x, y, status_get_name(src),class_,"");
			if (md) {
				md->master_id = src->id;
				md->special_state.ai = skillid==AM_SPHEREMINE?2:3;
				if( md->deletetimer != INVALID_TIMER )
					delete_timer(md->deletetimer, mob_timer_delete);
				md->deletetimer = add_timer (gettick() + skill_get_time(skillid,skilllv), mob_timer_delete, md->bl.id, 0);
				mob_spawn (md); //Now it is ready for spawning.
			}
		}
		break;

	// Slim Pitcher [Celest]
	case CR_SLIMPITCHER:
		if (sd) {
			int i = skilllv%11 - 1;
			int j = pc_search_inventory(sd,skill_db[skillid].itemid[i]);
			if( j < 0 || skill_db[skillid].itemid[i] <= 0 || sd->inventory_data[j] == NULL || sd->status.inventory[j].amount < skill_db[skillid].amount[i] )
			{
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				return 1;
			}
			potion_flag = 1;
			potion_hp = 0;
			potion_sp = 0;
			run_script(sd->inventory_data[j]->script,0,sd->bl.id,0);
			potion_flag = 0;
			//Apply skill bonuses
			i = pc_checkskill(sd,CR_SLIMPITCHER)*10
				+ pc_checkskill(sd,AM_POTIONPITCHER)*10
				+ pc_checkskill(sd,AM_LEARNINGPOTION)*5
				+ pc_skillheal_bonus(sd, skillid);

			potion_hp = potion_hp * (100+i)/100;
			potion_sp = potion_sp * (100+i)/100;

			if(potion_hp > 0 || potion_sp > 0) {
				i = skill_get_splash(skillid, skilllv);
				map_foreachinarea(skill_area_sub,
					src->m,x-i,y-i,x+i,y+i,BL_CHAR,
					src,skillid,skilllv,tick,flag|BCT_PARTY|BCT_GUILD|1,
					skill_castend_nodamage_id);
			}
		} else {
			int i = skilllv%11 - 1;
			struct item_data *item;
			i = skill_db[skillid].itemid[i];
			item = itemdb_search(i);
			potion_flag = 1;
			potion_hp = 0;
			potion_sp = 0;
			run_script(item->script,0,src->id,0);
			potion_flag = 0;
			i = skill_get_max(CR_SLIMPITCHER)*10;

			potion_hp = potion_hp * (100+i)/100;
			potion_sp = potion_sp * (100+i)/100;

			if(potion_hp > 0 || potion_sp > 0) {
				i = skill_get_splash(skillid, skilllv);
				map_foreachinarea(skill_area_sub,
					src->m,x-i,y-i,x+i,y+i,BL_CHAR,
					src,skillid,skilllv,tick,flag|BCT_PARTY|BCT_GUILD|1,
						skill_castend_nodamage_id);
			}
		}
		break;

	case HW_GANBANTEIN:
		if (rand()%100 < 80) {
			int dummy = 1;
			clif_skill_poseffect(src,skillid,skilllv,x,y,tick);
			i = skill_get_splash(skillid, skilllv);
			map_foreachinarea(skill_cell_overlap, src->m, x-i, y-i, x+i, y+i, BL_SKILL, HW_GANBANTEIN, &dummy, src);
		} else {
			if (sd) clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			return 1;
		}
		break;

	case HW_GRAVITATION:
		if ((sg = skill_unitsetting(src,skillid,skilllv,x,y,0)))
			sc_start4(src,type,100,skilllv,0,BCT_SELF,sg->group_id,skill_get_time(skillid,skilllv));
		flag|=1;
		break;

	// Plant Cultivation [Celest]
	case CR_CULTIVATION:
		if (sd) {
			int i;
			if( map_count_oncell(src->m,x,y,BL_CHAR) > 0 )
			{
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				return 1;
			}
			clif_skill_poseffect(src,skillid,skilllv,x,y,tick);
			if (rand()%100 < 50) {
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
			} else {
				TBL_MOB* md = mob_once_spawn_sub(src, src->m, x, y, "--ja--",(skilllv < 2 ? 1084+rand()%2 : 1078+rand()%6),"");
				if (!md) break;
				if ((i = skill_get_time(skillid, skilllv)) > 0)
				{
					if( md->deletetimer != INVALID_TIMER )
						delete_timer(md->deletetimer, mob_timer_delete);
					md->deletetimer = add_timer (tick + i, mob_timer_delete, md->bl.id, 0);
				}
				mob_spawn (md);
			}
		}
		break;

	case SG_SUN_WARM:
	case SG_MOON_WARM:
	case SG_STAR_WARM:
		skill_clear_unitgroup(src);
		if ((sg = skill_unitsetting(src,skillid,skilllv,src->x,src->y,0)))
			sc_start4(src,type,100,skilllv,0,0,sg->group_id,skill_get_time(skillid,skilllv));
		flag|=1;
		break;

	case PA_GOSPEL:
		if (sce && sce->val4 == BCT_SELF)
		{
			status_change_end(src, SC_GOSPEL, INVALID_TIMER);
			return 0;
		}
		else
	  	{
			sg = skill_unitsetting(src,skillid,skilllv,src->x,src->y,0);
			if (!sg) break;
			if (sce)
				status_change_end(src, type, INVALID_TIMER); //Was under someone else's Gospel. [Skotlex]
			sc_start4(src,type,100,skilllv,0,sg->group_id,BCT_SELF,skill_get_time(skillid,skilllv));
			clif_skill_poseffect(src, skillid, skilllv, 0, 0, tick); // PA_GOSPEL music packet
		}
		break;
	case NJ_TATAMIGAESHI:
		if (skill_unitsetting(src,skillid,skilllv,src->x,src->y,0))
			sc_start(src,type,100,skilllv,skill_get_time2(skillid,skilllv));
		break;

	case AM_RESURRECTHOMUN:	//[orn]
		if (sd)
		{
			if (!merc_resurrect_homunculus(sd, 20*skilllv, x, y))
			{
				clif_skill_fail(sd,skillid,USESKILL_FAIL_LEVEL,0);
				break;
			}
		}
		break;

	default:
		ShowWarning("skill_castend_pos2: Unknown skill used:%d\n",skillid);
		return 1;
	}

	status_change_end(src, SC_MAGICPOWER, INVALID_TIMER);

	if( sd )
	{
		if( sd->state.arrow_atk && !(flag&1) ) //Consume arrow if a ground skill was not invoked. [Skotlex]
			battle_consume_ammo(sd, skillid, skilllv);
		skill_onskillusage(sd, NULL, skillid, tick);
		skill_consume_requirement(sd,skillid,skilllv,2);
	}

	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_castend_map (struct map_session_data *sd, short skill_num, const char *map)
{
	nullpo_ret(sd);

//Simplify skill_failed code.
#define skill_failed(sd) { sd->menuskill_id = sd->menuskill_val = 0; }
	if(skill_num != sd->menuskill_id)
		return 0;

	if( sd->bl.prev == NULL || pc_isdead(sd) ) {
		skill_failed(sd);
		return 0;
	}

	if(sd->sc.opt1 || sd->sc.option&OPTION_HIDE ) {
		skill_failed(sd);
		return 0;
	}
	if(sd->sc.count && (
		sd->sc.data[SC_SILENCE] ||
		sd->sc.data[SC_ROKISWEIL] ||
		sd->sc.data[SC_AUTOCOUNTER] ||
		sd->sc.data[SC_STEELBODY] ||
		sd->sc.data[SC_DANCING] ||
		sd->sc.data[SC_BERSERK] ||
		sd->sc.data[SC_BASILICA] ||
		sd->sc.data[SC_MARIONETTE]
	 )) {
		skill_failed(sd);
		return 0;
	}

	pc_stop_attack(sd);
	pc_stop_walking(sd,0);

	if(battle_config.skill_log && battle_config.skill_log&BL_PC)
		ShowInfo("PC %d skill castend skill =%d map=%s\n",sd->bl.id,skill_num,map);

	if(strcmp(map,"cancel")==0) {
		skill_failed(sd);
		return 0;
	}

	switch(skill_num)
	{
	case AL_TELEPORT:
		if(strcmp(map,"Random")==0)
			pc_randomwarp(sd,CLR_TELEPORT);
		else if (sd->menuskill_val > 1) //Need lv2 to be able to warp here.
			pc_setpos(sd,sd->status.save_point.map,sd->status.save_point.x,sd->status.save_point.y,CLR_TELEPORT);
		break;

	case AL_WARP:
		{
			const struct point *p[4];
			struct skill_unit_group *group;
			int i, lv, wx, wy;
			int maxcount=0;
			int x,y;
			unsigned short mapindex;

			mapindex  = mapindex_name2id((char*)map);
			if(!mapindex) { //Given map not found?
				clif_skill_fail(sd,skill_num,USESKILL_FAIL_LEVEL,0);
				skill_failed(sd);
				return 0;
			}
			p[0] = &sd->status.save_point;
			p[1] = &sd->status.memo_point[0];
			p[2] = &sd->status.memo_point[1];
			p[3] = &sd->status.memo_point[2];

			if((maxcount = skill_get_maxcount(skill_num, sd->menuskill_val)) > 0) {
				for(i=0;i<MAX_SKILLUNITGROUP && sd->ud.skillunit[i] && maxcount;i++) {
					if(sd->ud.skillunit[i]->skill_id == skill_num)
						maxcount--;
				}
				if(!maxcount) {
					clif_skill_fail(sd,skill_num,USESKILL_FAIL_LEVEL,0);
					skill_failed(sd);
					return 0;
				}
			}

			lv = sd->skillitem==skill_num?sd->skillitemlv:pc_checkskill(sd,skill_num);
			wx = sd->menuskill_val>>16;
			wy = sd->menuskill_val&0xffff;

			if( lv <= 0 ) return 0;
			if( lv > 4 ) lv = 4; // crash prevention

			// check if the chosen map exists in the memo list
			ARR_FIND( 0, lv, i, mapindex == p[i]->map );
			if( i < lv ) {
				x=p[i]->x;
				y=p[i]->y;
			} else {
				skill_failed(sd);
				return 0;
			}

			if(!skill_check_condition_castend(sd, sd->menuskill_id, lv))
			{  // This checks versus skillid/skilllv...
				skill_failed(sd);
				return 0;
			}

			skill_consume_requirement(sd,sd->menuskill_id,lv,2);
			sd->skillitem = sd->skillitemlv = 0; // Clear data that's skipped in 'skill_castend_pos' [Inkfish]

			if((group=skill_unitsetting(&sd->bl,skill_num,lv,wx,wy,0))==NULL) {
				skill_failed(sd);
				return 0;
			}

			// record the destination coordinates
			group->val2 = (x<<16)|y;
			group->val3 = mapindex;
		}
		break;
	}

	sd->menuskill_id = sd->menuskill_val = 0;
	return 0;
#undef skill_failed
}

/// transforms 'target' skill unit into dissonance (if conditions are met)
static int skill_dance_overlap_sub(struct block_list* bl, va_list ap)
{
	struct skill_unit* target = (struct skill_unit*)bl;
	struct skill_unit* src = va_arg(ap, struct skill_unit*);
	int flag = va_arg(ap, int);

	if (src == target)
		return 0;
	if (!target->group || !(target->group->state.song_dance&0x1))
		return 0;
	if (!(target->val2 & src->val2 & ~UF_ENSEMBLE)) //They don't match (song + dance) is valid.
		return 0;

	if (flag) //Set dissonance
		target->val2 |= UF_ENSEMBLE; //Add ensemble to signal this unit is overlapping.
	else //Remove dissonance
		target->val2 &= ~UF_ENSEMBLE;

	clif_skill_setunit(target); //Update look of affected cell.

	return 1;
}

//Does the song/dance overlapping -> dissonance check. [Skotlex]
//When flag is 0, this unit is about to be removed, cancel the dissonance effect
//When 1, this unit has been positioned, so start the cancel effect.
int skill_dance_overlap(struct skill_unit* unit, int flag)
{
	if (!unit || !unit->group || !(unit->group->state.song_dance&0x1))
		return 0;
	if (!flag && !(unit->val2&UF_ENSEMBLE))
		return 0; //Nothing to remove, this unit is not overlapped.

	if (unit->val1 != unit->group->skill_id)
	{	//Reset state
		unit->val1 = unit->group->skill_id;
		unit->val2 &= ~UF_ENSEMBLE;
	}

	return map_foreachincell(skill_dance_overlap_sub, unit->bl.m,unit->bl.x,unit->bl.y,BL_SKILL, unit,flag);
}

/*==========================================
 * Converts this group information so that it is handled as a Dissonance or Ugly Dance cell.
 * Flag: 0 - Convert, 1 - Revert.
 *------------------------------------------*/
static bool skill_dance_switch(struct skill_unit* unit, int flag)
{
	static int prevflag = 1;  // by default the backup is empty
	static struct skill_unit_group backup;
	struct skill_unit_group* group = unit->group;

	// val2&UF_ENSEMBLE is a hack to indicate dissonance
	if ( !(group->state.song_dance&0x1 && unit->val2&UF_ENSEMBLE) )
		return false;

	if( flag == prevflag )
	{// protection against attempts to read an empty backup / write to a full backup
		ShowError("skill_dance_switch: Attempted to %s (skill_id=%d, skill_lv=%d, src_id=%d).\n",
			flag ? "read an empty backup" : "write to a full backup",
			group->skill_id, group->skill_lv, group->src_id);
		return false;
	}
	prevflag = flag;

	if( !flag )
	{	//Transform
		int skillid = unit->val2&UF_SONG ? BA_DISSONANCE : DC_UGLYDANCE;

		// backup
		backup.skill_id    = group->skill_id;
		backup.skill_lv    = group->skill_lv;
		backup.unit_id     = group->unit_id;
		backup.target_flag = group->target_flag;
		backup.bl_flag     = group->bl_flag;
		backup.interval    = group->interval;

		// replace
		group->skill_id    = skillid;
		group->skill_lv    = 1;
		group->unit_id     = skill_get_unit_id(skillid,0);
		group->target_flag = skill_get_unit_target(skillid);
		group->bl_flag     = skill_get_unit_bl_target(skillid);
		group->interval    = skill_get_unit_interval(skillid);
	}
	else
	{	//Restore
		group->skill_id    = backup.skill_id;
		group->skill_lv    = backup.skill_lv;
		group->unit_id     = backup.unit_id;
		group->target_flag = backup.target_flag;
		group->bl_flag     = backup.bl_flag;
		group->interval    = backup.interval;
	}

	return true;
}

/*==========================================
 * Initializes and sets a ground skill.
 * flag&1 is used to determine when the skill 'morphs' (Warp portal becomes active, or Fire Pillar becomes active)
 * flag&2 is used to determine if this skill was casted with Magic Power active.
 *------------------------------------------*/
struct skill_unit_group* skill_unitsetting (struct block_list *src, short skillid, short skilllv, short x, short y, int flag)
{
	struct skill_unit_group *group;
	int i,limit,val1=0,val2=0,val3=0;
	int target,interval,range,unit_flag;
	struct s_skill_unit_layout *layout;
	struct map_session_data *sd;
	struct status_data *status;
	struct status_change *sc;
	int active_flag=1;
	int subunt=0;

	nullpo_retr(NULL, src);

	limit = skill_get_time(skillid,skilllv);
	range = skill_get_unit_range(skillid,skilllv);
	interval = skill_get_unit_interval(skillid);
	target = skill_get_unit_target(skillid);
	unit_flag = skill_get_unit_flag(skillid);
	layout = skill_get_unit_layout(skillid,skilllv,src,x,y);

	sd = BL_CAST(BL_PC, src);
	status = status_get_status_data(src);
	sc = status_get_sc(src);	// for traps, firewall and fogwall - celest

	switch( skillid )
	{
	case MG_SAFETYWALL:
		val2=skilllv+1;
		break;
	case MG_FIREWALL:
		if(sc && sc->data[SC_VIOLENTGALE])
			limit = limit*3/2;
		val2=4+skilllv;
		break;

	case AL_WARP:
		val1=skilllv+6;
		if(!(flag&1))
			limit=2000;
		else // previous implementation (not used anymore)
		{	//Warp Portal morphing to active mode, extract relevant data from src. [Skotlex]
			if( src->type != BL_SKILL ) return NULL;
			group = ((TBL_SKILL*)src)->group;
			src = map_id2bl(group->src_id);
			if( !src ) return NULL;
			val2 = group->val2; //Copy the (x,y) position you warp to
			val3 = group->val3; //as well as the mapindex to warp to.
		}
		break;
	case HP_BASILICA:
		val1 = src->id; // Store caster id.
		break;

	case PR_SANCTUARY:
	case NPC_EVILLAND:
		val1=(skilllv+3)*2;
		break;

	case WZ_FIREPILLAR:
		if((flag&1)!=0)
			limit=1000;
		val1=skilllv+2;
		break;
	case WZ_QUAGMIRE:	//The target changes to "all" if used in a gvg map. [Skotlex]
	case AM_DEMONSTRATION:
		if (map_flag_vs(src->m) && battle_config.vs_traps_bctall
			&& (src->type&battle_config.vs_traps_bctall))
			target = BCT_ALL;
		break;
	case HT_SHOCKWAVE:
		val1=skilllv*15+10;
	case HT_SANDMAN:
	case MA_SANDMAN:
	case HT_CLAYMORETRAP:
	case HT_SKIDTRAP:
	case MA_SKIDTRAP:
	case HT_LANDMINE:
	case MA_LANDMINE:
	case HT_ANKLESNARE:
	case HT_FLASHER:
	case HT_FREEZINGTRAP:
	case MA_FREEZINGTRAP:
	case HT_BLASTMINE:
		if( map_flag_gvg(src->m) || map[src->m].flag.battleground )
			limit *= 4; // longer trap times in WOE [celest]
		if( battle_config.vs_traps_bctall && map_flag_vs(src->m) && (src->type&battle_config.vs_traps_bctall) )
			target = BCT_ALL;
		break;

	case SA_LANDPROTECTOR:
	case SA_VOLCANO:
	case SA_DELUGE:
	case SA_VIOLENTGALE:
	{
		struct skill_unit_group *old_sg;
		if ((old_sg = skill_locate_element_field(src)) != NULL)
		{	//HelloKitty confirmed that these are interchangeable,
			//so you can change element and not consume gemstones.
			if ((
				old_sg->skill_id == SA_VOLCANO ||
				old_sg->skill_id == SA_DELUGE ||
				old_sg->skill_id == SA_VIOLENTGALE
			) && old_sg->limit > 0)
			{	//Use the previous limit (minus the elapsed time) [Skotlex]
				limit = old_sg->limit - DIFF_TICK(gettick(), old_sg->tick);
				if (limit < 0)	//This can happen...
					limit = skill_get_time(skillid,skilllv);
			}
			skill_clear_group(src,1);
		}
		break;
	}

	case BA_DISSONANCE:
	case DC_UGLYDANCE:
		val1 = 10;	//FIXME: This value is not used anywhere, what is it for? [Skotlex]
		break;
	case BA_WHISTLE:
		val1 = skilllv +status->agi/10; // Flee increase
		val2 = ((skilllv+1)/2)+status->luk/10; // Perfect dodge increase
		if(sd){
			val1 += pc_checkskill(sd,BA_MUSICALLESSON);
			val2 += pc_checkskill(sd,BA_MUSICALLESSON);
		}
		break;
	case DC_HUMMING:
        val1 = 2*skilllv+status->dex/10; // Hit increase
		if(sd)
			val1 += pc_checkskill(sd,DC_DANCINGLESSON);
		break;
	case BA_POEMBRAGI:
		val1 = 3*skilllv+status->dex/10; // Casting time reduction
		//For some reason at level 10 the base delay reduction is 50%.
		val2 = (skilllv<10?3*skilllv:50)+status->int_/5; // After-cast delay reduction
		if(sd){
			val1 += 2*pc_checkskill(sd,BA_MUSICALLESSON);
			val2 += 2*pc_checkskill(sd,BA_MUSICALLESSON);
		}
		break;
	case DC_DONTFORGETME:
		val1 = status->dex/10 + 6*skilllv + 5; // ASPD decrease
		val2 = status->agi/10 + 3*skilllv + 5; // Movement speed adjustment.
		if(sd){
			val1 += pc_checkskill(sd,DC_DANCINGLESSON);
			val2 += pc_checkskill(sd,DC_DANCINGLESSON);
		}
		break;
	case BA_APPLEIDUN:
		val1 = 5+2*skilllv+status->vit/10; // MaxHP percent increase
		if(sd)
			val1 += pc_checkskill(sd,BA_MUSICALLESSON);
		break;
	case DC_SERVICEFORYOU:
		val1 = 15+skilllv+(status->int_/10); // MaxSP percent increase TO-DO: this INT bonus value is guessed
		val2 = 20+3*skilllv+(status->int_/10); // SP cost reduction
		if(sd){
			val1 += pc_checkskill(sd,DC_DANCINGLESSON); //TO-DO This bonus value is guessed
			val2 += pc_checkskill(sd,DC_DANCINGLESSON); //TO-DO Should be half this value
		}
		break;
	case BA_ASSASSINCROSS:
		val1 = 100+10*skilllv+status->agi; // ASPD increase
		if(sd)
			val1 += 5*pc_checkskill(sd,BA_MUSICALLESSON);
		break;
	case DC_FORTUNEKISS:
		val1 = 10+skilllv+(status->luk/10); // Critical increase
		if(sd)
			val1 += pc_checkskill(sd,DC_DANCINGLESSON);
		val1*=10; //Because every 10 crit is an actual cri point.
		break;
	case BD_DRUMBATTLEFIELD:
		val1 = (skilllv+1)*25;	//Watk increase
		val2 = (skilllv+1)*2;	//Def increase
		break;
	case BD_RINGNIBELUNGEN:
		val1 = (skilllv+2)*25;	//Watk increase
		break;
	case BD_RICHMANKIM:
		val1 = 25 + 11*skilllv; //Exp increase bonus.
		break;
	case BD_SIEGFRIED:
		val1 = 55 + skilllv*5;	//Elemental Resistance
		val2 = skilllv*10;	//Status ailment resistance
		break;
	case WE_CALLPARTNER:
		if (sd) val1 = sd->status.partner_id;
		break;
	case WE_CALLPARENT:
		if (sd) {
			val1 = sd->status.father;
		 	val2 = sd->status.mother;
		}
		break;
	case WE_CALLBABY:
		if (sd) val1 = sd->status.child;
		break;
	case NJ_KAENSIN:
		skill_clear_group(src, 1); //Delete previous Kaensins/Suitons
		val2 = (skilllv+1)/2 + 4;
		break;
	case NJ_SUITON:
		skill_clear_group(src, 1);
		break;

	case GS_GROUNDDRIFT:
		{
		int element[5]={ELE_WIND,ELE_DARK,ELE_POISON,ELE_WATER,ELE_FIRE};

		val1 = status->rhw.ele;
		if (!val1)
			val1=element[rand()%5];

		switch (val1)
		{
			case ELE_FIRE:
				subunt++;
			case ELE_WATER:
				subunt++;
			case ELE_POISON:
				subunt++;
			case ELE_DARK:
				subunt++;
			case ELE_WIND:
				break;
			default:
				subunt=rand()%5;
				break;
		}

		break;
		}
	}

	nullpo_retr(NULL, group=skill_initunitgroup(src,layout->count,skillid,skilllv,skill_get_unit_id(skillid,flag&1)+subunt, limit, interval));
	group->val1=val1;
	group->val2=val2;
	group->val3=val3;
	group->target_flag=target;
	group->bl_flag= skill_get_unit_bl_target(skillid);
	group->state.magic_power = (flag&2 || (sc && sc->data[SC_MAGICPOWER])); //Store the magic power flag. [Skotlex]
	group->state.ammo_consume = (sd && sd->state.arrow_atk && skillid != GS_GROUNDDRIFT); //Store if this skill needs to consume ammo.
	group->state.song_dance = (unit_flag&(UF_DANCE|UF_SONG)?1:0)|(unit_flag&UF_ENSEMBLE?2:0); //Signals if this is a song/dance/duet

  	//if tick is greater than current, do not invoke onplace function just yet. [Skotlex]
	if (DIFF_TICK(group->tick, gettick()) > SKILLUNITTIMER_INTERVAL)
		active_flag = 0;

	if(skillid==HT_TALKIEBOX || skillid==RG_GRAFFITI){
		group->valstr=(char *) aMalloc(MESSAGE_SIZE*sizeof(char));
		if (sd)
			safestrncpy(group->valstr, sd->message, MESSAGE_SIZE);
		else //Eh... we have to write something here... even though mobs shouldn't use this. [Skotlex]
			safestrncpy(group->valstr, "Boo!", MESSAGE_SIZE);
	}

	if (group->state.song_dance) {
		if(sd){
			sd->skillid_dance = skillid;
			sd->skilllv_dance = skilllv;
		}
		if (
			sc_start4(src, SC_DANCING, 100, skillid, group->group_id, skilllv,
				(group->state.song_dance&2?BCT_SELF:0), limit+1000) &&
			sd && group->state.song_dance&2 && skillid != CG_HERMODE //Hermod is a encore with a warp!
		)
			skill_check_pc_partner(sd, skillid, &skilllv, 1, 1);
	}

	limit = group->limit;
	for( i = 0; i < layout->count; i++ )
	{
		struct skill_unit *unit;
		int ux = x + layout->dx[i];
		int uy = y + layout->dy[i];
		int val1 = skilllv;
		int val2 = 0;
		int alive = 1;

		if( !group->state.song_dance && !map_getcell(src->m,ux,uy,CELL_CHKREACH) )
			continue; // don't place skill units on walls (except for songs/dances/encores)
		if( battle_config.skill_wall_check && skill_get_unit_flag(skillid)&UF_PATHCHECK && !path_search_long(NULL,src->m,ux,uy,x,y,CELL_CHKWALL) )
			continue; // no path between cell and center of casting.

		switch( skillid )
		{
		case MG_FIREWALL:
		case NJ_KAENSIN:
			val2=group->val2;
			break;
		case WZ_ICEWALL:
			val1 = (skilllv <= 1) ? 500 : 200 + 200*skilllv;
			val2 = map_getcell(src->m, ux, uy, CELL_GETTYPE);
			break;
		case HT_LANDMINE:
		case MA_LANDMINE:
		case HT_ANKLESNARE:
		case HT_SHOCKWAVE:
		case HT_SANDMAN:
		case MA_SANDMAN:
		case HT_FLASHER:
		case HT_FREEZINGTRAP:
		case MA_FREEZINGTRAP:
		case HT_TALKIEBOX:
		case HT_SKIDTRAP:
		case MA_SKIDTRAP:
			val1 = 36000;
			break;
		case GS_DESPERADO:
			val1 = abs(layout->dx[i]);
			val2 = abs(layout->dy[i]);
			if (val1 < 2 || val2 < 2) { //Nearby cross, linear decrease with no diagonals
				if (val2 > val1) val1 = val2;
				if (val1) val1--;
				val1 = 36 -12*val1;
			} else //Diagonal edges
				val1 = 28 -4*val1 -4*val2;
			if (val1 < 1) val1 = 1;
			val2 = 0;
			break;
		default:
			if (group->state.song_dance&0x1)
				val2 = unit_flag&(UF_DANCE|UF_SONG); //Store whether this is a song/dance
			break;
		}

		if( range <= 0 )
			map_foreachincell(skill_cell_overlap,src->m,ux,uy,BL_SKILL,skillid,&alive, src);
		if( !alive )
			continue;

		nullpo_retr(NULL, unit=skill_initunit(group,i,ux,uy,val1,val2));
		unit->limit=limit;
		unit->range=range;

		if (skillid == PF_FOGWALL && alive == 2)
		{	//Double duration of cells on top of Deluge/Suiton
			unit->limit *= 2;
			group->limit = unit->limit;
		}

		// execute on all targets standing on this cell
		if (range==0 && active_flag)
			map_foreachincell(skill_unit_effect,unit->bl.m,unit->bl.x,unit->bl.y,group->bl_flag,&unit->bl,gettick(),1);
	}

	if (!group->alive_count)
	{	//No cells? Something that was blocked completely by Land Protector?
		skill_delunitgroup(group);
		return NULL;
	}


	if (skillid == NJ_TATAMIGAESHI) //Store number of tiles.
		group->val1 = group->alive_count;

	return group;
}

/*==========================================
 *
 *------------------------------------------*/
static int skill_unit_onplace (struct skill_unit *src, struct block_list *bl, unsigned int tick)
{
	struct skill_unit_group *sg;
	struct block_list *ss;
	struct status_change *sc;
	struct status_change_entry *sce;
	enum sc_type type;
	int skillid;

	nullpo_ret(src);
	nullpo_ret(bl);

	if(bl->prev==NULL || !src->alive || status_isdead(bl))
		return 0;

	nullpo_ret(sg=src->group);
	nullpo_ret(ss=map_id2bl(sg->src_id));

	if( skill_get_type(sg->skill_id) == BF_MAGIC && map_getcell(bl->m, bl->x, bl->y, CELL_CHKLANDPROTECTOR) && sg->skill_id != SA_LANDPROTECTOR )
		return 0; //AoE skills are ineffective. [Skotlex]

	sc = status_get_sc(bl);

	if (sc && sc->option&OPTION_HIDE && sg->skill_id != WZ_HEAVENDRIVE)
		return 0; //Hidden characters are immune to AoE skills except Heaven's Drive. [Skotlex]

	type = status_skill2sc(sg->skill_id);
	sce = (sc && type != -1)?sc->data[type]:NULL;
	skillid = sg->skill_id; //In case the group is deleted, we need to return the correct skill id, still.
	switch (sg->unit_id)
	{
	case UNT_SPIDERWEB:
		if( sc && sc->data[SC_SPIDERWEB] && sc->data[SC_SPIDERWEB]->val1 > 0 )
		{ // If you are fiberlocked and can't move, it will only increase your fireweakness level. [Inkfish]
			sc->data[SC_SPIDERWEB]->val2++;
			break;
		}
		else if( sc )
		{
			int sec = skill_get_time2(sg->skill_id,sg->skill_lv);
			if( status_change_start(bl,type,10000,sg->skill_lv,1,sg->group_id,0,sec,8) )
			{
				const struct TimerData* td = sc->data[type]?get_timer(sc->data[type]->timer):NULL; 
				if( td )
					sec = DIFF_TICK(td->tick, tick);
				map_moveblock(bl, src->bl.x, src->bl.y, tick);
				clif_fixpos(bl);
				sg->val2 = bl->id;
			}
			else
				sec = 3000; //Couldn't trap it?
			sg->limit = DIFF_TICK(tick,sg->tick)+sec;
		}
		break;
	case UNT_SAFETYWALL:
		if (!sce)
			sc_start4(bl,type,100,sg->skill_lv,sg->group_id,sg->group_id,0,sg->limit);
		break;

	case UNT_PNEUMA:
		if (!sce)
			sc_start4(bl,type,100,sg->skill_lv,sg->group_id,0,0,sg->limit);
		break;

	case UNT_WARP_WAITING:
		if(bl->type==BL_PC){
			struct map_session_data *sd = (struct map_session_data *)bl;
			if((!sd->chatID || battle_config.chat_warpportal)
				&& sd->ud.to_x == src->bl.x && sd->ud.to_y == src->bl.y)
			{
				int x = sg->val2>>16;
				int y = sg->val2&0xffff;
				unsigned short m = sg->val3;

				if( --sg->val1 <= 0 )
					skill_delunitgroup(sg);

				pc_setpos(sd,m,x,y,CLR_TELEPORT);
				sg = src->group; // avoid dangling pointer (pc_setpos can cause deletion of 'sg')
			}
		} else
		if(bl->type == BL_MOB)
		{
			struct mob_data *md = (struct mob_data *)bl;
			if(battle_config.mob_warp&2 || md->option.allow_warp == 2) {
				int m = map_mapindex2mapid(sg->val3);
				if (m < 0) break; //Map not available on this map-server.
				unit_warp(bl,m,sg->val2>>16,sg->val2&0xffff,CLR_TELEPORT);
			}
		}
		break;

	case UNT_QUAGMIRE:
		if(!sce)
			sc_start4(bl,type,100,sg->skill_lv,sg->group_id,0,0,sg->limit);
		break;

	case UNT_VOLCANO:
	case UNT_DELUGE:
	case UNT_VIOLENTGALE:
		if(!sce)
			sc_start(bl,type,100,sg->skill_lv,sg->limit);
		break;

	case UNT_SUITON:
		if(!sce)
			sc_start4(bl,type,100,sg->skill_lv,
			map_flag_vs(bl->m) || battle_check_target(&src->bl,bl,BCT_ENEMY)>0?1:0, //Send val3 =1 to reduce agi.
			0,0,sg->limit);
		break;

	case UNT_HERMODE:
		if (sg->src_id!=bl->id && battle_check_target(&src->bl,bl,BCT_PARTY|BCT_GUILD) > 0)
			status_change_clear_buffs(bl,1); //Should dispell only allies.
	case UNT_RICHMANKIM:
	case UNT_ETERNALCHAOS:
	case UNT_DRUMBATTLEFIELD:
	case UNT_RINGNIBELUNGEN:
	case UNT_ROKISWEIL:
	case UNT_INTOABYSS:
	case UNT_SIEGFRIED:
		 //Needed to check when a dancer/bard leaves their ensemble area.
		if (sg->src_id==bl->id && !(sc && sc->data[SC_SPIRIT] && sc->data[SC_SPIRIT]->val2 == SL_BARDDANCER))
			return skillid;
		if (!sce)
			sc_start4(bl,type,100,sg->skill_lv,sg->val1,sg->val2,0,sg->limit);
		break;
	case UNT_WHISTLE:
	case UNT_ASSASSINCROSS:
	case UNT_POEMBRAGI:
	case UNT_APPLEIDUN:
	case UNT_HUMMING:
	case UNT_DONTFORGETME:
	case UNT_FORTUNEKISS:
	case UNT_SERVICEFORYOU:
		if (sg->src_id==bl->id && !(sc && sc->data[SC_SPIRIT] && sc->data[SC_SPIRIT]->val2 == SL_BARDDANCER))
			return 0;
		if (!sc) return 0;
		if (!sce)
			sc_start4(bl,type,100,sg->skill_lv,sg->val1,sg->val2,0,sg->limit);
		else if (sce->val4 == 1) {
			//Readjust timers since the effect will not last long.
			sce->val4 = 0;
			delete_timer(sce->timer, status_change_timer);
			sce->timer = add_timer(tick+sg->limit, status_change_timer, bl->id, type);
		}
		break;

	case UNT_FOGWALL:
		if (!sce)
		{
			sc_start4(bl, type, 100, sg->skill_lv, sg->val1, sg->val2, sg->group_id, sg->limit);
			if (battle_check_target(&src->bl,bl,BCT_ENEMY)>0)
				skill_additional_effect (ss, bl, sg->skill_id, sg->skill_lv, BF_MISC, ATK_DEF, tick);
		}
		break;

	case UNT_GRAVITATION:
		if (!sce)
			sc_start4(bl,type,100,sg->skill_lv,0,BCT_ENEMY,sg->group_id,sg->limit);
		break;

// officially, icewall has no problems existing on occupied cells [ultramage]
//	case UNT_ICEWALL: //Destroy the cell. [Skotlex]
//		src->val1 = 0;
//		if(src->limit + sg->tick > tick + 700)
//			src->limit = DIFF_TICK(tick+700,sg->tick);
//		break;

	case UNT_MOONLIT:
		//Knockback out of area if affected char isn't in Moonlit effect
		if (sc && sc->data[SC_DANCING] && (sc->data[SC_DANCING]->val1&0xFFFF) == CG_MOONLIT)
			break;
		if (ss == bl) //Also needed to prevent infinite loop crash.
			break;
		skill_blown(ss,bl,skill_get_blewcount(sg->skill_id,sg->skill_lv),unit_getdir(bl),0);
		break;
	}
	return skillid;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_unit_onplace_timer (struct skill_unit *src, struct block_list *bl, unsigned int tick)
{
	struct skill_unit_group *sg;
	struct block_list *ss;
	TBL_PC* tsd;
	struct status_data *tstatus, *sstatus;
	struct status_change *tsc, *sc;
	struct skill_unit_group_tickset *ts;
	enum sc_type type;
	int skillid;
	int diff=0;

	nullpo_ret(src);
	nullpo_ret(bl);

	if (bl->prev==NULL || !src->alive || status_isdead(bl))
		return 0;

	nullpo_ret(sg=src->group);
	nullpo_ret(ss=map_id2bl(sg->src_id));
	tsd = BL_CAST(BL_PC, bl);
	tsc = status_get_sc(bl);
	tstatus = status_get_status_data(bl);
	if (sg->state.magic_power)  //For magic power.
	{
		sc = status_get_sc(ss);
		sstatus = status_get_status_data(ss);
	} else {
		sc = NULL;
		sstatus = NULL;
	}
	type = status_skill2sc(sg->skill_id);
	skillid = sg->skill_id;

	if (sg->interval == -1) {
		switch (sg->unit_id) {
			case UNT_ANKLESNARE: //These happen when a trap is splash-triggered by multiple targets on the same cell.
			case UNT_FIREPILLAR_ACTIVE:
				return 0;
			default:
				ShowError("skill_unit_onplace_timer: interval error (unit id %x)\n", sg->unit_id);
				return 0;
		}
	}

	if ((ts = skill_unitgrouptickset_search(bl,sg,tick)))
	{	//Not all have it, eg: Traps don't have it even though they can be hit by Heaven's Drive [Skotlex]
		diff = DIFF_TICK(tick,ts->tick);
		if (diff < 0)
			return 0;
		ts->tick = tick+sg->interval;

		if ((skillid==CR_GRANDCROSS || skillid==NPC_GRANDDARKNESS) && !battle_config.gx_allhit)
			ts->tick += sg->interval*(map_count_oncell(bl->m,bl->x,bl->y,BL_CHAR)-1);
	}
	//Temporarily set magic power to have it take effect. [Skotlex]
	if (sg->state.magic_power && sc && !sc->data[SC_MAGICPOWER])
	{	//Store previous values.
		swap(sstatus->matk_min, sc->mp_matk_min);
		swap(sstatus->matk_max, sc->mp_matk_max);
		//Note to NOT return from the function until this is unset!
	}

	switch (sg->unit_id)
	{
		case UNT_FIREWALL:
		case UNT_KAEN:
		{
			int count=0;
			const int x = bl->x, y = bl->y;

			//Take into account these hit more times than the timer interval can handle.
			do
				skill_attack(BF_MAGIC,ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick+count*sg->interval,0);
			while(--src->val2 && x == bl->x && y == bl->y &&
				++count < SKILLUNITTIMER_INTERVAL/sg->interval && !status_isdead(bl));

			if (src->val2<=0)
				skill_delunit(src);
		}
		break;

		case UNT_SANCTUARY:
			if( battle_check_undead(tstatus->race, tstatus->def_ele) || tstatus->race==RC_DEMON )
			{ //Only damage enemies with offensive Sanctuary. [Skotlex]
				if( battle_check_target(&src->bl,bl,BCT_ENEMY) > 0 && skill_attack(BF_MAGIC, ss, &src->bl, bl, sg->skill_id, sg->skill_lv, tick, 0) )
					sg->val1 -= 2; // reduce healing count if this was meant for damaging [hekate]
			}
			else
			{
				int heal = skill_calc_heal(ss,bl,sg->skill_id,sg->skill_lv,true);
				struct mob_data *md = BL_CAST(BL_MOB, bl);
				if( tstatus->hp >= tstatus->max_hp )
					break;
				if( md && mob_is_battleground(md) )
					heal = 1;
				if( status_isimmune(bl) )
					heal = 0;
				clif_skill_nodamage(&src->bl, bl, AL_HEAL, heal, 1);
				status_heal(bl, heal, 0, 0);
				if( diff >= 500 )
					sg->val1--;
			}
			if( sg->val1 <= 0 )
				skill_delunitgroup(sg);
			break;

		case UNT_EVILLAND:
			//Will heal demon and undead element monsters, but not players.
			if ((bl->type == BL_PC) || (!battle_check_undead(tstatus->race, tstatus->def_ele) && tstatus->race!=RC_DEMON))
			{	//Damage enemies
				if(battle_check_target(&src->bl,bl,BCT_ENEMY)>0)
					skill_attack(BF_MISC, ss, &src->bl, bl, sg->skill_id, sg->skill_lv, tick, 0);
			} else {
				int heal = skill_calc_heal(ss,bl,sg->skill_id,sg->skill_lv,true);
				if (tstatus->hp >= tstatus->max_hp)
					break;
				if (status_isimmune(bl))
					heal = 0;
				clif_skill_nodamage(&src->bl, bl, AL_HEAL, heal, 1);
				status_heal(bl, heal, 0, 0);
			}
			break;

		case UNT_MAGNUS:
			if (!battle_check_undead(tstatus->race,tstatus->def_ele) && tstatus->race!=RC_DEMON)
				break;
			skill_attack(BF_MAGIC,ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick,0);
			break;

		case UNT_DUMMYSKILL:
			switch (sg->skill_id)
			{
				case SG_SUN_WARM: //SG skills [Komurka]
				case SG_MOON_WARM:
				case SG_STAR_WARM:
				{
					int count = 0;
					const int x = bl->x, y = bl->y;

					//If target isn't knocked back it should hit every "interval" ms [Playtester]
					do
					{
						if( bl->type == BL_PC )
							status_zap(bl, 0, 15); // sp damage to players
						else // mobs
						if( status_charge(ss, 0, 2) ) // costs 2 SP per hit
						{
							if( !skill_attack(BF_WEAPON,ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick+count*sg->interval,0) )
								status_charge(ss, 0, 8); //costs additional 8 SP if miss
						}
						else
						{ //should end when out of sp.
							sg->limit = DIFF_TICK(tick,sg->tick);
							break;
						}
					} while( x == bl->x && y == bl->y &&
						++count < SKILLUNITTIMER_INTERVAL/sg->interval && !status_isdead(bl) );
				}
				break;
				case WZ_STORMGUST: //SG counter does not reset per stormgust. IE: One hit from a SG and two hits from another will freeze you.
					if (tsc) 
						tsc->sg_counter++; //SG hit counter.
					if (skill_attack(skill_get_type(sg->skill_id),ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick,0) <= 0 && tsc)
						tsc->sg_counter=0; //Attack absorbed.
				break;

				case GS_DESPERADO:
					if (rand()%100 < src->val1)
						skill_attack(BF_WEAPON,ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick,0);
				break;

				default:
					skill_attack(skill_get_type(sg->skill_id),ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick,0);
			}
			break;

		case UNT_FIREPILLAR_WAITING:
			skill_unitsetting(ss,sg->skill_id,sg->skill_lv,src->bl.x,src->bl.y,1);
			skill_delunit(src);
			break;

		case UNT_SKIDTRAP:
			{
				skill_blown(&src->bl,bl,skill_get_blewcount(sg->skill_id,sg->skill_lv),unit_getdir(bl),0);
				sg->unit_id = UNT_USED_TRAPS;
				clif_changetraplook(&src->bl, UNT_USED_TRAPS);
				sg->limit=DIFF_TICK(tick,sg->tick)+1500;
			}
			break;

		case UNT_ANKLESNARE:
			if( sg->val2 == 0 && tsc )
			{
				int sec = skill_get_time2(sg->skill_id,sg->skill_lv);
				if( status_change_start(bl,type,10000,sg->skill_lv,sg->group_id,0,0,sec, 8) )
				{
					const struct TimerData* td = tsc->data[type]?get_timer(tsc->data[type]->timer):NULL; 
					if( td )
						sec = DIFF_TICK(td->tick, tick);
					unit_movepos(bl, src->bl.x, src->bl.y, 0, 0);
					clif_fixpos(bl);
					sg->val2 = bl->id;
				}
				else
					sec = 3000; //Couldn't trap it?
				clif_skillunit_update(&src->bl);
				sg->limit = DIFF_TICK(tick,sg->tick)+sec;
				sg->interval = -1;
				src->range = 0;
			}
			break;

		case UNT_VENOMDUST:
			if(tsc && !tsc->data[type])
				status_change_start(bl,type,10000,sg->skill_lv,sg->group_id,0,0,skill_get_time2(sg->skill_id,sg->skill_lv),8);
			break;

		case UNT_LANDMINE:
		case UNT_CLAYMORETRAP:
		case UNT_BLASTMINE:
		case UNT_SHOCKWAVE:
		case UNT_SANDMAN:
		case UNT_FLASHER:
		case UNT_FREEZINGTRAP:
		case UNT_FIREPILLAR_ACTIVE:
			map_foreachinrange(skill_trap_splash,&src->bl, skill_get_splash(sg->skill_id, sg->skill_lv), sg->bl_flag, &src->bl,tick);
			if (sg->unit_id != UNT_FIREPILLAR_ACTIVE)
				clif_changetraplook(&src->bl, sg->unit_id==UNT_LANDMINE?UNT_FIREPILLAR_ACTIVE:UNT_USED_TRAPS);
			src->range = -1; //Disable range so it does not invoke a for each in area again.
			sg->limit=DIFF_TICK(tick,sg->tick)+1500;
			break;

		case UNT_TALKIEBOX:
			if (sg->src_id == bl->id)
				break;
			if (sg->val2 == 0){
				clif_talkiebox(&src->bl, sg->valstr);
				sg->unit_id = UNT_USED_TRAPS;
				clif_changetraplook(&src->bl, UNT_USED_TRAPS);
				sg->limit = DIFF_TICK(tick, sg->tick) + 5000;
				sg->val2 = -1;
			}
			break;

		case UNT_LULLABY:
			if (ss->id == bl->id)
				break;
			skill_additional_effect(ss, bl, sg->skill_id, sg->skill_lv, BF_LONG|BF_SKILL|BF_MISC, ATK_DEF, tick);
			break;

		case UNT_UGLYDANCE:	//Ugly Dance [Skotlex]
			if (ss->id != bl->id)
				skill_additional_effect(ss, bl, sg->skill_id, sg->skill_lv, BF_LONG|BF_SKILL|BF_MISC, ATK_DEF, tick);
			break;

		case UNT_DISSONANCE:
			skill_attack(BF_MISC, ss, &src->bl, bl, sg->skill_id, sg->skill_lv, tick, 0);
			break;

		case UNT_APPLEIDUN: //Apple of Idun [Skotlex]
		{
			int heal;
			if( sg->src_id == bl->id && !(tsc && tsc->data[SC_SPIRIT] && tsc->data[SC_SPIRIT]->val2 == SL_BARDDANCER) )
				break; // affects self only when soullinked
			heal = skill_calc_heal(ss,bl,sg->skill_id, sg->skill_lv, true);
			clif_skill_nodamage(&src->bl, bl, AL_HEAL, heal, 1);
			status_heal(bl, heal, 0, 0);
			break;
		}

 		case UNT_TATAMIGAESHI:
		case UNT_DEMONSTRATION:
			skill_attack(BF_WEAPON,ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick,0);
			break;

		case UNT_GOSPEL:
			if (rand()%100 > sg->skill_lv*10 || ss == bl)
				break;
			if (battle_check_target(ss,bl,BCT_PARTY)>0)
			{ // Support Effect only on party, not guild
				int heal;
				int i = rand()%13; // Positive buff count
				int time = skill_get_time2(sg->skill_id, sg->skill_lv); //Duration
				switch (i)
				{
					case 0: // Heal 1~9999 HP
						heal = rand() %9999+1;
						clif_skill_nodamage(ss,bl,AL_HEAL,heal,1);
						status_heal(bl,heal,0,0);
						break;
					case 1: // End all negative status
						status_change_clear_buffs(bl,2);
						if (tsd) clif_gospel_info(tsd, 0x15);
						break;
					case 2: // Immunity to all status
						sc_start(bl,SC_SCRESIST,100,100,time);
						if (tsd) clif_gospel_info(tsd, 0x16);
						break;
					case 3: // MaxHP +100%
						sc_start(bl,SC_INCMHPRATE,100,100,time);
						if (tsd) clif_gospel_info(tsd, 0x17);
						break;
					case 4: // MaxSP +100%
						sc_start(bl,SC_INCMSPRATE,100,100,time);
						if (tsd) clif_gospel_info(tsd, 0x18);
						break;
					case 5: // All stats +20
						sc_start(bl,SC_INCALLSTATUS,100,20,time);
						if (tsd) clif_gospel_info(tsd, 0x19);
						break;
					case 6: // Level 10 Blessing
						sc_start(bl,SC_BLESSING,100,10,time);
						break;
					case 7: // Level 10 Increase AGI
						sc_start(bl,SC_INCREASEAGI,100,10,time);
						break;
					case 8: // Enchant weapon with Holy element
						sc_start(bl,SC_ASPERSIO,100,1,time);
						if (tsd) clif_gospel_info(tsd, 0x1c);
						break;
					case 9: // Enchant armor with Holy element
						sc_start(bl,SC_BENEDICTIO,100,1,time);
						if (tsd) clif_gospel_info(tsd, 0x1d);
						break;
					case 10: // DEF +25%
						sc_start(bl,SC_INCDEFRATE,100,25,time);
						if (tsd) clif_gospel_info(tsd, 0x1e);
						break;
					case 11: // ATK +100%
						sc_start(bl,SC_INCATKRATE,100,100,time);
						if (tsd) clif_gospel_info(tsd, 0x1f);
						break;
					case 12: // HIT/Flee +50
						sc_start(bl,SC_INCHIT,100,50,time);
						sc_start(bl,SC_INCFLEE,100,50,time);
						if (tsd) clif_gospel_info(tsd, 0x20);
						break;
				}
			}
			else if (battle_check_target(&src->bl,bl,BCT_ENEMY)>0)
			{ // Offensive Effect
				int i = rand()%9; // Negative buff count
				int time = skill_get_time2(sg->skill_id, sg->skill_lv);
				switch (i)
				{
					case 0: // Deal 1~9999 damage
						skill_attack(BF_MISC,ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick,0);
						break;
					case 1: // Curse
						sc_start(bl,SC_CURSE,100,1,time);
						break;
					case 2: // Blind
						sc_start(bl,SC_BLIND,100,1,time);
						break;
					case 3: // Poison
						sc_start(bl,SC_POISON,100,1,time);
						break;
					case 4: // Level 10 Provoke
						sc_start(bl,SC_PROVOKE,100,10,time);
						break;
					case 5: // DEF -100%
						sc_start(bl,SC_INCDEFRATE,100,-100,time);
						break;
					case 6: // ATK -100%
						sc_start(bl,SC_INCATKRATE,100,-100,time);
						break;
					case 7: // Flee -100%
						sc_start(bl,SC_INCFLEERATE,100,-100,time);
						break;
					case 8: // Speed/ASPD -25%
						sc_start4(bl,SC_GOSPEL,100,1,0,0,BCT_ENEMY,time);
						break;
				}
			}
			break;

		case UNT_BASILICA:
			{
				int i = battle_check_target(&src->bl, bl, BCT_ENEMY);
				if( i > 0 && !(status_get_mode(bl)&MD_BOSS) )
				{ // knock-back any enemy except Boss
					skill_blown(&src->bl, bl, 2, unit_getdir(bl), 0);
					clif_fixpos(bl);
				}

				if( sg->src_id != bl->id && i <= 0 )
					sc_start4(bl, type, 100, 0, 0, 0, src->bl.id, sg->interval + 100);
			}
			break;

		case UNT_GRAVITATION:
			skill_attack(skill_get_type(sg->skill_id),ss,&src->bl,bl,sg->skill_id,sg->skill_lv,tick,0);
			break;

		case UNT_GROUNDDRIFT_WIND:
		case UNT_GROUNDDRIFT_DARK:
		case UNT_GROUNDDRIFT_POISON:
		case UNT_GROUNDDRIFT_WATER:
		case UNT_GROUNDDRIFT_FIRE:
			map_foreachinrange(skill_trap_splash,&src->bl,
				skill_get_splash(sg->skill_id, sg->skill_lv), sg->bl_flag,
				&src->bl,tick);
			sg->unit_id = UNT_USED_TRAPS;
			//clif_changetraplook(&src->bl, UNT_FIREPILLAR_ACTIVE);
			sg->limit=DIFF_TICK(tick,sg->tick)+1500;
			break;
	}

	if (sg->state.magic_power && sc && !sc->data[SC_MAGICPOWER])
	{	//Unset magic power.
		swap(sstatus->matk_min, sc->mp_matk_min);
		swap(sstatus->matk_max, sc->mp_matk_max);
	}

	if (bl->type == BL_MOB && ss != bl)
		mobskill_event((TBL_MOB*)bl, ss, tick, MSC_SKILLUSED|(skillid<<16));

	return skillid;
}
/*==========================================
 * Triggered when a char steps out of a skill cell
 *------------------------------------------*/
int skill_unit_onout (struct skill_unit *src, struct block_list *bl, unsigned int tick)
{
	struct skill_unit_group *sg;
	struct status_change *sc;
	struct status_change_entry *sce;
	enum sc_type type;

	nullpo_ret(src);
	nullpo_ret(bl);
	nullpo_ret(sg=src->group);
	sc = status_get_sc(bl);
	type = status_skill2sc(sg->skill_id);
	sce = (sc && type != -1)?sc->data[type]:NULL;

	if( bl->prev==NULL ||
		(status_isdead(bl) && sg->unit_id != UNT_ANKLESNARE && sg->unit_id != UNT_SPIDERWEB) ) //Need to delete the trap if the source died.
		return 0;

	switch(sg->unit_id){
	case UNT_SAFETYWALL:
	case UNT_PNEUMA:
		if (sce)
			status_change_end(bl, type, INVALID_TIMER);
		break;

	case UNT_BASILICA:
		if( sce && sce->val4 == src->bl.id )
			status_change_end(bl, type, INVALID_TIMER);
		break;

	case UNT_HERMODE:	//Clear Hermode if the owner moved.
		if (sce && sce->val3 == BCT_SELF && sce->val4 == sg->group_id)
			status_change_end(bl, type, INVALID_TIMER);
		break;

	case UNT_SPIDERWEB:
		{
			struct block_list *target = map_id2bl(sg->val2);
			if (target && target==bl)
			{
				if (sce && sce->val3 == sg->group_id)
					status_change_end(bl, type, INVALID_TIMER);
				sg->limit = DIFF_TICK(tick,sg->tick)+1000;
			}
			break;
		}
	}
	return sg->skill_id;
}

/*==========================================
 * Triggered when a char steps out of a skill group (entirely) [Skotlex]
 *------------------------------------------*/
static int skill_unit_onleft (int skill_id, struct block_list *bl, unsigned int tick)
{
	struct status_change *sc;
	struct status_change_entry *sce;
	enum sc_type type;

	sc = status_get_sc(bl);
	if (sc && !sc->count)
		sc = NULL;

	type = status_skill2sc(skill_id);
	sce = (sc && type != -1)?sc->data[type]:NULL;

	switch (skill_id)
	{
		case WZ_QUAGMIRE:
			if (bl->type==BL_MOB)
				break;
			if (sce)
				status_change_end(bl, type, INVALID_TIMER);
			break;

		case BD_LULLABY:
		case BD_RICHMANKIM:
		case BD_ETERNALCHAOS:
		case BD_DRUMBATTLEFIELD:
		case BD_RINGNIBELUNGEN:
		case BD_ROKISWEIL:
		case BD_INTOABYSS:
		case BD_SIEGFRIED:
			if(sc && sc->data[SC_DANCING] && (sc->data[SC_DANCING]->val1&0xFFFF) == skill_id)
			{	//Check if you just stepped out of your ensemble skill to cancel dancing. [Skotlex]
				//We don't check for SC_LONGING because someone could always have knocked you back and out of the song/dance.
				//FIXME: This code is not perfect, it doesn't checks for the real ensemble's owner,
				//it only checks if you are doing the same ensemble. So if there's two chars doing an ensemble
				//which overlaps, by stepping outside of the other parther's ensemble will cause you to cancel
				//your own. Let's pray that scenario is pretty unlikely and noone will complain too much about it.
				status_change_end(bl, SC_DANCING, INVALID_TIMER);
			}
		case MG_SAFETYWALL:
		case AL_PNEUMA:
		case SA_VOLCANO:
		case SA_DELUGE:
		case SA_VIOLENTGALE:
		case CG_HERMODE:
		case HW_GRAVITATION:
		case NJ_SUITON:
			if (sce)
				status_change_end(bl, type, INVALID_TIMER);
			break;

		case BA_POEMBRAGI:
		case BA_WHISTLE:
		case BA_ASSASSINCROSS:
		case BA_APPLEIDUN:
		case DC_HUMMING:
		case DC_DONTFORGETME:
		case DC_FORTUNEKISS:
		case DC_SERVICEFORYOU:
			if (sce)
			{
				delete_timer(sce->timer, status_change_timer);
				//NOTE: It'd be nice if we could get the skill_lv for a more accurate extra time, but alas...
				//not possible on our current implementation.
				sce->val4 = 1; //Store the fact that this is a "reduced" duration effect.
				sce->timer = add_timer(tick+skill_get_time2(skill_id,1), status_change_timer, bl->id, type);
			}
			break;
		case PF_FOGWALL:
			if (sce)
			{
				status_change_end(bl, type, INVALID_TIMER);
				if ((sce=sc->data[SC_BLIND]))
				{
					if (bl->type == BL_PC) //Players get blind ended inmediately, others have it still for 30 secs. [Skotlex]
						status_change_end(bl, SC_BLIND, INVALID_TIMER);
					else {
						delete_timer(sce->timer, status_change_timer);
						sce->timer = add_timer(30000+tick, status_change_timer, bl->id, SC_BLIND);
					}
				}
			}
			break;
	}

	return skill_id;
}

/*==========================================
 * Invoked when a unit cell has been placed/removed/deleted.
 * flag values:
 * flag&1: Invoke onplace function (otherwise invoke onout)
 * flag&4: Invoke a onleft call (the unit might be scheduled for deletion)
 *------------------------------------------*/
static int skill_unit_effect (struct block_list* bl, va_list ap)
{
	struct skill_unit* unit = va_arg(ap,struct skill_unit*);
	struct skill_unit_group* group = unit->group;
	unsigned int tick = va_arg(ap,unsigned int);
	unsigned int flag = va_arg(ap,unsigned int);
	int skill_id;
	bool dissonance;

	if( (!unit->alive && !(flag&4)) || bl->prev == NULL )
		return 0;

	nullpo_ret(group);

	dissonance = skill_dance_switch(unit, 0);

	//Necessary in case the group is deleted after calling on_place/on_out [Skotlex]
	skill_id = group->skill_id;

	//Target-type check.
	if( !(group->bl_flag&bl->type && battle_check_target(&unit->bl,bl,group->target_flag)>0) )
	{
		if( flag&4 && group->src_id == bl->id && group->state.song_dance&0x2 )
			skill_unit_onleft(skill_id, bl, tick);//Ensemble check to terminate it.
	}
	else
	{
		if( flag&1 )
			skill_unit_onplace(unit,bl,tick);
		else
			skill_unit_onout(unit,bl,tick);

		if( flag&4 )
	  		skill_unit_onleft(skill_id, bl, tick);
	}

	if( dissonance ) skill_dance_switch(unit, 1);

	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_unit_ondamaged (struct skill_unit *src, struct block_list *bl, int damage, unsigned int tick)
{
	struct skill_unit_group *sg;

	nullpo_ret(src);
	nullpo_ret(sg=src->group);

	switch( sg->unit_id )
	{
	case UNT_SKIDTRAP:
	case UNT_LANDMINE:
	case UNT_SHOCKWAVE:
	case UNT_SANDMAN:
	case UNT_FLASHER:
	case UNT_FREEZINGTRAP:
	case UNT_TALKIEBOX:
	case UNT_ANKLESNARE:
	case UNT_ICEWALL:
		src->val1-=damage;
		break;
	case UNT_BLASTMINE:
	case UNT_CLAYMORETRAP:
		skill_blown(bl, &src->bl, 2, -1, 0);
		break;
	default:
		damage = 0;
		break;
	}
	return damage;
}

/*==========================================
 *
 *------------------------------------------*/
static int skill_check_condition_char_sub (struct block_list *bl, va_list ap)
{
	int *c, skillid;
	struct block_list *src;
	struct map_session_data *sd;
	struct map_session_data *tsd;
	int *p_sd;	//Contains the list of characters found.

	nullpo_ret(bl);
	nullpo_ret(tsd=(struct map_session_data*)bl);
	nullpo_ret(src=va_arg(ap,struct block_list *));
	nullpo_ret(sd=(struct map_session_data*)src);

	c=va_arg(ap,int *);
	p_sd = va_arg(ap, int *);
	skillid = va_arg(ap,int);

	if ((skillid != PR_BENEDICTIO && *c >=1) || *c >=2)
		return 0; //Partner found for ensembles, or the two companions for Benedictio. [Skotlex]

	if (bl == src)
		return 0;

	if(pc_isdead(tsd))
		return 0;

	if (tsd->sc.data[SC_SILENCE] || tsd->sc.opt1)
		return 0;

	switch(skillid)
	{
		case PR_BENEDICTIO:
		{
			int dir = map_calc_dir(&sd->bl,tsd->bl.x,tsd->bl.y);
			dir = (unit_getdir(&sd->bl) + dir)%8; //This adjusts dir to account for the direction the sd is facing.
			if ((tsd->class_&MAPID_BASEMASK) == MAPID_ACOLYTE && (dir == 2 || dir == 6) //Must be standing to the left/right of Priest.
				&& sd->status.sp >= 10)
				p_sd[(*c)++]=tsd->bl.id;
			return 1;
		}
		default: //Warning: Assuming Ensemble Dance/Songs for code speed. [Skotlex]
			{
				int skilllv;
				if(pc_issit(tsd) || !unit_can_move(&tsd->bl))
					return 0;
				if (sd->status.sex != tsd->status.sex &&
						(tsd->class_&MAPID_UPPERMASK) == MAPID_BARDDANCER &&
						(skilllv = pc_checkskill(tsd, skillid)) > 0 &&
						(tsd->weapontype1==W_MUSICAL || tsd->weapontype1==W_WHIP) &&
						skill_check_sameparty(sd,tsd) &&
						!tsd->sc.data[SC_DANCING])
				{
					p_sd[(*c)++]=tsd->bl.id;
					return skilllv;
				} else {
					return 0;
				}
			}
			break;
	}
	return 0;
}

/*==========================================
 * Checks and stores partners for ensemble skills [Skotlex]
 *------------------------------------------*/
int skill_check_pc_partner (struct map_session_data *sd, short skill_id, short* skill_lv, int range, int cast_flag)
{
	static int c=0;
	static int p_sd[2] = { 0, 0 };
	int i;

	if (!battle_config.player_skill_partner_check ||
		(battle_config.gm_skilluncond && pc_isGM(sd) >= battle_config.gm_skilluncond))
		return 99; //As if there were infinite partners.

	if (cast_flag)
	{	//Execute the skill on the partners.
		struct map_session_data* tsd;
		switch (skill_id)
		{
			case PR_BENEDICTIO:
				for (i = 0; i < c; i++)
				{
					if ((tsd = map_id2sd(p_sd[i])) != NULL)
						status_charge(&tsd->bl, 0, 10);
				}
				return c;
			default: //Warning: Assuming Ensemble skills here (for speed)
				if (c > 0 && sd->sc.data[SC_DANCING] && (tsd = map_id2sd(p_sd[0])) != NULL)
				{
					sd->sc.data[SC_DANCING]->val4 = tsd->bl.id;
					sc_start4(&tsd->bl,SC_DANCING,100,skill_id,sd->sc.data[SC_DANCING]->val2,*skill_lv,sd->bl.id,skill_get_time(skill_id,*skill_lv)+1000);
					clif_skill_nodamage(&tsd->bl, &sd->bl, skill_id, *skill_lv, 1);
					tsd->skillid_dance = skill_id;
					tsd->skilllv_dance = *skill_lv;
				}
				return c;
		}
	}

	//Else: new search for partners.
	c = 0;
	memset (p_sd, 0, sizeof(p_sd));
	i = map_foreachinrange(skill_check_condition_char_sub, &sd->bl, range, BL_PC, &sd->bl, &c, &p_sd, skill_id);

	if (skill_id != PR_BENEDICTIO) //Apply the average lv to encore skills.
		*skill_lv = (i+(*skill_lv))/(c+1); //I know c should be one, but this shows how it could be used for the average of n partners.
	return c;
}

/*==========================================
 *
 *------------------------------------------*/
static int skill_check_condition_mob_master_sub (struct block_list *bl, va_list ap)
{
	int *c,src_id,mob_class,skill;
	struct mob_data *md;

	md=(struct mob_data*)bl;
	src_id=va_arg(ap,int);
	mob_class=va_arg(ap,int);
	skill=va_arg(ap,int);
	c=va_arg(ap,int *);

	if( md->master_id != src_id || md->special_state.ai != (unsigned)(skill == AM_SPHEREMINE?2:3) )
		return 0; //Non alchemist summoned mobs have nothing to do here.

	if( mob_class > 0 && md->class_ == mob_class )
		(*c)++;
	else if( mob_class < 0 && md->class_ != -mob_class )
		(*c)++;
	else if( mob_class == 0 )
		(*c)++;

	return 1;
}

/*==========================================
 * Determines if a given skill should be made to consume ammo
 * when used by the player. [Skotlex]
 *------------------------------------------*/
int skill_isammotype (struct map_session_data *sd, int skill)
{
	return (
		battle_config.arrow_decrement==2 &&
		(sd->status.weapon == W_BOW || (sd->status.weapon >= W_REVOLVER && sd->status.weapon <= W_GRENADE)) &&
		skill != HT_PHANTASMIC &&
		skill_get_type(skill) == BF_WEAPON &&
	  	!(skill_get_nk(skill)&NK_NO_DAMAGE) &&
		!skill_get_spiritball(skill,1) //Assume spirit spheres are used as ammo instead.
	);
}

int skill_check_condition_castbegin(struct map_session_data* sd, short skill, short lv)
{
	struct status_data *status;
	struct status_change *sc;
	struct skill_condition require;
	int i;

	nullpo_ret(sd);

	if (lv <= 0 || sd->chatID) return 0;

	if( battle_config.gm_skilluncond && pc_isGM(sd)>= battle_config.gm_skilluncond && sd->skillitem != skill )
	{	//GMs don't override the skillItem check, otherwise they can use items without them being consumed! [Skotlex]	
		sd->state.arrow_atk = skill_get_ammotype(skill)?1:0; //Need to do arrow state check.
		sd->spiritball_old = sd->spiritball; //Need to do Spiritball check.
		return 1;
	}

	if( sd->menuskill_id == AM_PHARMACY )
	{
		switch( skill )
		{
		case AM_PHARMACY:
		case AC_MAKINGARROW:
		case BS_REPAIRWEAPON:
		case AM_TWILIGHT1:
		case AM_TWILIGHT2:
		case AM_TWILIGHT3:
			return 0;
		}
	}

	status = &sd->battle_status;
	sc = &sd->sc;
	if( !sc->count )
		sc = NULL;

	if( sd->skillitem == skill )
	{
		if( sd->state.abra_flag ) // Hocus-Pocus was used. [Inkfish]
			sd->state.abra_flag = 0;
		else
		{ // When a target was selected, consume items that were skipped in pc_use_item [Skotlex]
			if( (i = sd->itemindex) == -1 ||
				sd->status.inventory[i].nameid != sd->itemid ||
				sd->inventory_data[i] == NULL ||
				!sd->inventory_data[i]->flag.delay_consume ||
				sd->status.inventory[i].amount < 1
				)
			{	//Something went wrong, item exploit?
				sd->itemid = sd->itemindex = -1;
				return 0;
			}
			//Consume
			sd->itemid = sd->itemindex = -1;
			if( skill == WZ_EARTHSPIKE && sc && sc->data[SC_EARTHSCROLL] && rand()%100 > sc->data[SC_EARTHSCROLL]->val2 ) // [marquis007]
				; //Do not consume item.
			else if( sd->status.inventory[i].expire_time == 0 )
				pc_delitem(sd,i,1,0,0,LOG_TYPE_CONSUME); // Rental usable items are not consumed until expiration
		}
		return 1;
	}

	if( pc_is90overweight(sd) )
	{
		clif_skill_fail(sd,skill,USESKILL_FAIL_WEIGHTOVER,0);
		return 0;
	}

	switch( skill )
	{ // Turn off check.
	case BS_MAXIMIZE:		case NV_TRICKDEAD:	case TF_HIDING:			case AS_CLOAKING:		case CR_AUTOGUARD:
	case ML_AUTOGUARD:		case CR_DEFENDER:	case ML_DEFENDER:		case ST_CHASEWALK:		case PA_GOSPEL:
	case CR_SHRINK:			case TK_RUN:		case GS_GATLINGFEVER:	case TK_READYCOUNTER:	case TK_READYDOWN:
	case TK_READYSTORM:		case TK_READYTURN:	case SG_FUSION:			case ALL_RIDING:
		if( sc && sc->data[status_skill2sc(skill)] )
			return 1;
	}

	if( lv < 1 || lv > MAX_SKILL_LEVEL )
		return 0;

	require = skill_get_requirement(sd,skill,lv);

	//Can only update state when weapon/arrow info is checked.
	sd->state.arrow_atk = require.ammo?1:0;

	// perform skill-specific checks (and actions)
	switch( skill )
	{
	case SA_CASTCANCEL:
		if(sd->ud.skilltimer == INVALID_TIMER) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case AL_WARP:
		if(!battle_config.duel_allow_teleport && sd->duel_group) { // duel restriction [LuzZza]
			clif_displaymessage(sd->fd, "Duel: Can't use warp in duel.");
			return 0;
		}
		break;
	case MO_CALLSPIRITS:
		if(sd->spiritball >= lv) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case MO_FINGEROFFENSIVE:
	case GS_FLING:
		if( sd->spiritball > 0 && sd->spiritball < require.spiritball )
			sd->spiritball_old = require.spiritball = sd->spiritball;
		else
			sd->spiritball_old = require.spiritball;
		break;
	case MO_CHAINCOMBO:
		if(!sc)
			return 0;
		if(sc->data[SC_BLADESTOP])
			break;
		if(sc->data[SC_COMBO] && sc->data[SC_COMBO]->val1 == MO_TRIPLEATTACK)
			break;
		return 0;
	case MO_COMBOFINISH:
		if(!(sc && sc->data[SC_COMBO] && sc->data[SC_COMBO]->val1 == MO_CHAINCOMBO))
			return 0;
		break;
	case CH_TIGERFIST:
		if(!(sc && sc->data[SC_COMBO] && sc->data[SC_COMBO]->val1 == MO_COMBOFINISH))
			return 0;
		break;
	case CH_CHAINCRUSH:
		if(!(sc && sc->data[SC_COMBO]))
			return 0;
		if(sc->data[SC_COMBO]->val1 != MO_COMBOFINISH && sc->data[SC_COMBO]->val1 != CH_TIGERFIST)
			return 0;
		break;
	case MO_EXTREMITYFIST:
//		if(sc && sc->data[SC_EXTREMITYFIST]) //To disable Asura during the 5 min skill block uncomment this...
//			return 0;
		if( sc && sc->data[SC_BLADESTOP] )
			break;
		if( sc && sc->data[SC_COMBO] )
		{
			switch(sc->data[SC_COMBO]->val1) {
				case MO_COMBOFINISH:
				case CH_TIGERFIST:
				case CH_CHAINCRUSH:
					break;
				default:
					return 0;
			}
		}
		else if( !unit_can_move(&sd->bl) )
	  	{	//Placed here as ST_MOVE_ENABLE should not apply if rooted or on a combo. [Skotlex]
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;

	case TK_MISSION:
		if( (sd->class_&MAPID_UPPERMASK) != MAPID_TAEKWON )
		{// Cannot be used by Non-Taekwon classes
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;

	case TK_READYCOUNTER:
	case TK_READYDOWN:
	case TK_READYSTORM:
	case TK_READYTURN:
	case TK_JUMPKICK:
		if( (sd->class_&MAPID_UPPERMASK) == MAPID_SOUL_LINKER )
		{// Soul Linkers cannot use this skill
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;

	case TK_TURNKICK:
	case TK_STORMKICK:
	case TK_DOWNKICK:
	case TK_COUNTER:
		if ((sd->class_&MAPID_UPPERMASK) == MAPID_SOUL_LINKER)
			return 0; //Anti-Soul Linker check in case you job-changed with Stances active.
		if(!(sc && sc->data[SC_COMBO]))
			return 0; //Combo needs to be ready

		if (sc->data[SC_COMBO]->val3)
		{	//Kick chain
			//Do not repeat a kick.
			if (sc->data[SC_COMBO]->val3 != skill)
				break;
			status_change_end(&sd->bl, SC_COMBO, INVALID_TIMER);
			return 0;
		}
		if(sc->data[SC_COMBO]->val1 != skill)
		{	//Cancel combo wait.
			unit_cancel_combo(&sd->bl);
			return 0;
		}
		break; //Combo ready.
	case BD_ADAPTATION:
		{
			int time;
			if(!(sc && sc->data[SC_DANCING]))
			{
				clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
				return 0;
			}
			time = 1000*(sc->data[SC_DANCING]->val3>>16);
			if (skill_get_time(
				(sc->data[SC_DANCING]->val1&0xFFFF), //Dance Skill ID
				(sc->data[SC_DANCING]->val1>>16)) //Dance Skill LV
				- time < skill_get_time2(skill,lv))
			{
				clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
				return 0;
			}
		}
		break;

	case PR_BENEDICTIO:
		if (skill_check_pc_partner(sd, skill, &lv, 1, 0) < 2)
		{
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;

	case SL_SMA:
		if(!(sc && sc->data[SC_SMA]))
			return 0;
		break;

	case HT_POWER:
		if(!(sc && sc->data[SC_COMBO] && sc->data[SC_COMBO]->val1 == skill))
			return 0;
		break;

	case CG_HERMODE:
		if(!npc_check_areanpc(1,sd->bl.m,sd->bl.x,sd->bl.y,skill_get_splash(skill, lv)))
		{
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case CG_MOONLIT: //Check there's no wall in the range+1 area around the caster. [Skotlex]
		{
			int i,x,y,range = skill_get_splash(skill, lv)+1;
			int size = range*2+1;
			for (i=0;i<size*size;i++) {
				x = sd->bl.x+(i%size-range);
				y = sd->bl.y+(i/size-range);
				if (map_getcell(sd->bl.m,x,y,CELL_CHKWALL)) {
					clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
					return 0;
				}
			}
		}
		break;
	case PR_REDEMPTIO:
		{
			int exp;
			if( ((exp = pc_nextbaseexp(sd)) > 0 && get_percentage(sd->status.base_exp, exp) < 1) ||
				((exp = pc_nextjobexp(sd)) > 0 && get_percentage(sd->status.job_exp, exp) < 1)) {
				clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0); //Not enough exp.
				return 0;
			}
			break;
		}
	case AM_TWILIGHT2:
	case AM_TWILIGHT3:
		if (!party_skill_check(sd, sd->status.party_id, skill, lv))
		{
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case SG_SUN_WARM:
	case SG_MOON_WARM:
	case SG_STAR_WARM:
		if (sc && sc->data[SC_MIRACLE])
			break;
		i = skill-SG_SUN_WARM;
		if (sd->bl.m == sd->feel_map[i].m)
			break;
		clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
		return 0;
		break;
	case SG_SUN_COMFORT:
	case SG_MOON_COMFORT:
	case SG_STAR_COMFORT:
		if (sc && sc->data[SC_MIRACLE])
			break;
		i = skill-SG_SUN_COMFORT;
		if (sd->bl.m == sd->feel_map[i].m &&
			(battle_config.allow_skill_without_day || sg_info[i].day_func()))
			break;
		clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
		return 0;
	case SG_FUSION:
		if (sc && sc->data[SC_SPIRIT] && sc->data[SC_SPIRIT]->val2 == SL_STAR)
			break;
		//Auron insists we should implement SP consumption when you are not Soul Linked. [Skotlex]
		//Only invoke on skill begin cast (instant cast skill). [Kevin]
		if( require.sp > 0 )
		{
			if (status->sp < (unsigned int)require.sp)
				clif_skill_fail(sd,skill,USESKILL_FAIL_SP_INSUFFICIENT,0);
			else
				status_zap(&sd->bl, 0, require.sp);
		}
		return 0;
	case GD_BATTLEORDER:
	case GD_REGENERATION:
	case GD_RESTORE:
		if( !map_flag_gvg2(sd->bl.m) && !map[sd->bl.m].flag.battleground )
		{
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
	case GD_EMERGENCYCALL:
		// other checks were already done in skillnotok()
		if( map[sd->bl.m].flag.battleground )
		{
			if( !(sd->bg_id && sd->bmaster_flag) )
				return 0; // Not Team Leader on Battleground
		}
		else
		{
			if( !(sd->status.guild_id && sd->state.gmaster_flag) )
				return 0; // Not Guild Leader
		}
		break;

	case GS_GLITTERING:
		if(sd->spiritball >= 10) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;

	case NJ_ISSEN:
		if (status->hp < 2) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
	case NJ_BUNSINJYUTSU:
		if (!(sc && sc->data[SC_NEN])) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
	  	}
		break;

	case NJ_ZENYNAGE:
		if( sd->status.zeny < require.zeny )
		{
			if( !battle_config.skill_zeny2item || pc_search_inventory(sd,battle_config.skill_zeny2item) < 0 )
			{ // Item Replacement of Zeny for Skills
				clif_skill_fail(sd,skill,USESKILL_FAIL_MONEY,0);
				return 0;
			}
		}
		break;
	case PF_HPCONVERSION:
		if (status->sp == status->max_sp)
			return 0; //Unusable when at full SP.
		break;
	case AM_CALLHOMUN: //Can't summon if a hom is already out
		if (sd->status.hom_id && sd->hd && !sd->hd->homunculus.vaporize) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case AM_REST: //Can't vapo homun if you don't have an active homunc or it's hp is < 80%
		if (!merc_is_hom_active(sd->hd) || sd->hd->battle_status.hp < (sd->hd->battle_status.max_hp*80/100))
		{
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	}

	switch(require.state) {
	case ST_HIDING:
		if(!(sc && sc->option&OPTION_HIDE)) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_CLOAKING:
		if(!pc_iscloaking(sd)) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_HIDDEN:
		if(!pc_ishiding(sd)) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_RIDING:
		if(!pc_isriding(sd)) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_FALCON:
		if(!pc_isfalcon(sd)) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_CARTBOOST:
		if(!(sc && sc->data[SC_CARTBOOST])) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
	case ST_CART:
		if(!pc_iscarton(sd)) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_SHIELD:
		if(sd->status.shield <= 0) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_SIGHT:
		if(!(sc && sc->data[SC_SIGHT])) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_EXPLOSIONSPIRITS:
		if(!(sc && sc->data[SC_EXPLOSIONSPIRITS])) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_RECOV_WEIGHT_RATE:
		if(battle_config.natural_heal_weight_rate <= 100 && sd->weight*100/sd->max_weight >= (unsigned int)battle_config.natural_heal_weight_rate) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_MOVE_ENABLE:
		if (sc && sc->data[SC_COMBO] && sc->data[SC_COMBO]->val1 == skill)
			sd->ud.canmove_tick = gettick(); //When using a combo, cancel the can't move delay to enable the skill. [Skotlex]

		if (!unit_can_move(&sd->bl)) {
			clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
		break;
	case ST_WATER:
		if (sc && (sc->data[SC_DELUGE] || sc->data[SC_SUITON]))
			break;
		if (map_getcell(sd->bl.m,sd->bl.x,sd->bl.y,CELL_CHKWATER))
			break;
		clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
		return 0;
	}

	if(require.mhp > 0 && get_percentage(status->hp, status->max_hp) > require.mhp) {
		//mhp is the max-hp-requirement, that is,
		//you must have this % or less of HP to cast it.
		clif_skill_fail(sd,skill,USESKILL_FAIL_HP_INSUFFICIENT,0);
		return 0;
	}

	if( require.weapon && !pc_check_weapontype(sd,require.weapon) ) {
		clif_skill_fail(sd,skill,USESKILL_FAIL_THIS_WEAPON,0);
		return 0;
	}

	if( require.sp > 0 && status->sp < (unsigned int)require.sp) {
		clif_skill_fail(sd,skill,USESKILL_FAIL_SP_INSUFFICIENT,0);
		return 0;
	}

	if( require.zeny > 0 && sd->status.zeny < require.zeny )
	{
		if( !battle_config.skill_zeny2item || pc_search_inventory(sd,battle_config.skill_zeny2item) < 0 )
		{ // Item Replacement of Zeny for Skills
			clif_skill_fail(sd,skill,USESKILL_FAIL_MONEY,0);
			return 0;
		}
	}

	if( require.spiritball > 0 && sd->spiritball < require.spiritball) {
		clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
		return 0;
	}

	return 1;
}

int skill_check_condition_castend(struct map_session_data* sd, short skill, short lv)
{
	struct skill_condition require;
	struct status_data *status;
	int i;
	int index[MAX_SKILL_ITEM_REQUIRE];

	nullpo_ret(sd);

	if( lv <= 0 || sd->chatID )
		return 0;

	if( battle_config.gm_skilluncond && pc_isGM(sd) >= battle_config.gm_skilluncond && sd->skillitem != skill )
	{	//GMs don't override the skillItem check, otherwise they can use items without them being consumed! [Skotlex]	
		sd->state.arrow_atk = skill_get_ammotype(skill)?1:0; //Need to do arrow state check.
		sd->spiritball_old = sd->spiritball; //Need to do Spiritball check.
		return 1;
	}

	if( sd->menuskill_id == AM_PHARMACY )
	{ // Cast start or cast end??
		switch( skill )
		{
		case AM_PHARMACY:
		case AC_MAKINGARROW:
		case BS_REPAIRWEAPON:
		case AM_TWILIGHT1:
		case AM_TWILIGHT2:
		case AM_TWILIGHT3:
			return 0;
		}
	}
	
	if( sd->skillitem == skill ) // Casting finished (Item skill or Hocus-Pocus)
		return 1;

	if( pc_is90overweight(sd) )
	{
		clif_skill_fail(sd,skill,USESKILL_FAIL_WEIGHTOVER,0);
		return 0;
	}


	/** Keitenai Speed Hack Protection **/
	switch( skill ){
		case SM_BASH:							if(sd && sd->state.K_LOCK_SM_BASH)				return false;	break;
		case SM_MAGNUM:							if(sd && sd->state.K_LOCK_SM_MAGNUM)			return false;	break;
		case MG_NAPALMBEAT:						if(sd && sd->state.K_LOCK_MG_NAPALMBEAT)		return false;	break;
		case MG_SOULSTRIKE:						if(sd && sd->state.K_LOCK_MG_SOULSTRIKE)		return false;	break;
		case MG_COLDBOLT:						if(sd && sd->state.K_LOCK_MG_COLDBOLT)			return false;	break;
		case MG_FROSTDIVER:						if(sd && sd->state.K_LOCK_MG_FROSTDIVER)		return false;	break;
		case MG_STONECURSE:						if(sd && sd->state.K_LOCK_MG_STONECURSE)		return false;	break;
		case MG_FIREBALL:						if(sd && sd->state.K_LOCK_MG_FIREBALL)			return false;	break;
		case MG_FIREWALL:						if(sd && sd->state.K_LOCK_MG_FIREWALL)			return false;	break;
		case MG_FIREBOLT:						if(sd && sd->state.K_LOCK_MG_FIREBOLT)			return false;	break;
		case MG_LIGHTNINGBOLT:					if(sd && sd->state.K_LOCK_MG_LIGHTNINGBOLT)		return false;	break;
		case MG_THUNDERSTORM:					if(sd && sd->state.K_LOCK_MG_THUNDERSTORM)		return false;	break;
		case AL_HEAL:							if(sd && sd->state.K_LOCK_AL_HEAL)				return false;	break;
		case AL_DECAGI:							if(sd && sd->state.K_LOCK_AL_DECAGI)			return false;	break;
		case AL_CRUCIS:							if(sd && sd->state.K_LOCK_AL_CRUCIS)			return false;	break;
		case MC_MAMMONITE:						if(sd && sd->state.K_LOCK_MC_MAMMONITE)			return false;	break;
		case AC_DOUBLE:							if(sd && sd->state.K_LOCK_AC_DOUBLE)			return false;	break;
		case AC_SHOWER:							if(sd && sd->state.K_LOCK_AC_SHOWER)			return false;	break;
		case TF_POISON:							if(sd && sd->state.K_LOCK_TF_POISON)			return false;	break;
		case KN_PIERCE:							if(sd && sd->state.K_LOCK_KN_PIERCE)			return false;	break;
		case KN_BRANDISHSPEAR:					if(sd && sd->state.K_LOCK_KN_BRANDISHSPEAR)		return false;	break;
		case KN_SPEARSTAB:						if(sd && sd->state.K_LOCK_KN_SPEARSTAB)			return false;	break;
		case KN_SPEARBOOMERANG:					if(sd && sd->state.K_LOCK_KN_SPEARBOOMERANG)	return false;	break;
		case KN_BOWLINGBASH:					if(sd && sd->state.K_LOCK_KN_BOWLINGBASH)		return false;	break;
		case PR_LEXDIVINA:						if(sd && sd->state.K_LOCK_PR_LEXDIVINA)			return false;	break;
		case PR_TURNUNDEAD:						if(sd && sd->state.K_LOCK_PR_TURNUNDEAD)		return false;	break;
		case PR_LEXAETERNA:						if(sd && sd->state.K_LOCK_PR_LEXAETERNA)		return false;	break;
		case PR_MAGNUS:							if(sd && sd->state.K_LOCK_PR_MAGNUS)			return false;	break;
		case WZ_FIREPILLAR:						if(sd && sd->state.K_LOCK_WZ_FIREPILLAR)		return false;	break;
		case WZ_SIGHTRASHER:					if(sd && sd->state.K_LOCK_WZ_SIGHTRASHER)		return false;	break;
		case WZ_FIREIVY:						if(sd && sd->state.K_LOCK_WZ_FIREIVY)			return false;	break;
		case WZ_METEOR:							if(sd && sd->state.K_LOCK_WZ_METEOR)			return false;	break;
		case WZ_JUPITEL:						if(sd && sd->state.K_LOCK_WZ_JUPITEL)			return false;	break;
		case WZ_VERMILION:						if(sd && sd->state.K_LOCK_WZ_VERMILION)			return false;	break;
		case WZ_WATERBALL:						if(sd && sd->state.K_LOCK_WZ_WATERBALL)			return false;	break;
		case WZ_ICEWALL:						if(sd && sd->state.K_LOCK_WZ_ICEWALL)			return false;	break;
		case WZ_FROSTNOVA:						if(sd && sd->state.K_LOCK_WZ_FROSTNOVA)			return false;	break;
		case WZ_STORMGUST:						if(sd && sd->state.K_LOCK_WZ_STORMGUST)			return false;	break;
		case WZ_EARTHSPIKE:						if(sd && sd->state.K_LOCK_WZ_EARTHSPIKE)		return false;	break;
		case WZ_HEAVENDRIVE:					if(sd && sd->state.K_LOCK_WZ_HEAVENDRIVE)		return false;	break;
		case WZ_QUAGMIRE:						if(sd && sd->state.K_LOCK_WZ_QUAGMIRE)			return false;	break;
		case WZ_ESTIMATION:						if(sd && sd->state.K_LOCK_WZ_ESTIMATION)		return false;	break;
		case BS_HAMMERFALL:						if(sd && sd->state.K_LOCK_BS_HAMMERFALL)		return false;	break;
		case HT_BLITZBEAT:						if(sd && sd->state.K_LOCK_HT_BLITZBEAT)			return false;	break;
		case AS_SONICBLOW:						if(sd && sd->state.K_LOCK_AS_SONICBLOW)			return false;	break;
		case AS_GRIMTOOTH:						if(sd && sd->state.K_LOCK_AS_GRIMTOOTH)			return false;	break;
		case AC_CHARGEARROW:					if(sd && sd->state.K_LOCK_AC_CHARGEARROW)		return false;	break;
		case TF_BACKSLIDING:					if(sd && sd->state.K_LOCK_TF_BACKSLIDING)		return false;	break;
		case MC_CARTREVOLUTION:					if(sd && sd->state.K_LOCK_MC_CARTREVOLUTION)	return false;	break;
		case AL_HOLYLIGHT:						if(sd && sd->state.K_LOCK_AL_HOLYLIGHT)			return false;	break;
		case RG_BACKSTAP:						if(sd && sd->state.K_LOCK_RG_BACKSTAP)			return false;	break;
		case RG_RAID:							if(sd && sd->state.K_LOCK_RG_RAID)				return false;	break;
		case RG_GRAFFITI:						if(sd && sd->state.K_LOCK_RG_GRAFFITI)			return false;	break;
		case RG_FLAGGRAFFITI:					if(sd && sd->state.K_LOCK_RG_FLAGGRAFFITI)		return false;	break;
		case RG_COMPULSION:						if(sd && sd->state.K_LOCK_RG_COMPULSION)		return false;	break;
		case RG_PLAGIARISM:						if(sd && sd->state.K_LOCK_RG_PLAGIARISM)		return false;	break;
		case AM_DEMONSTRATION:					if(sd && sd->state.K_LOCK_AM_DEMONSTRATION)		return false;	break;
		case AM_ACIDTERROR:						if(sd && sd->state.K_LOCK_AM_ACIDTERROR)		return false;	break;
		case AM_POTIONPITCHER:					if(sd && sd->state.K_LOCK_AM_POTIONPITCHER)		return false;	break;
		case AM_CANNIBALIZE:					if(sd && sd->state.K_LOCK_AM_CANNIBALIZE)		return false;	break;
		case AM_SPHEREMINE:						if(sd && sd->state.K_LOCK_AM_SPHEREMINE)		return false;	break;
		case AM_FLAMECONTROL:					if(sd && sd->state.K_LOCK_AM_FLAMECONTROL)		return false;	break;
		case AM_DRILLMASTER:					if(sd && sd->state.K_LOCK_AM_DRILLMASTER)		return false;	break;
		case CR_SHIELDBOOMERANG:				if(sd && sd->state.K_LOCK_CR_SHLDBOOMRANG)		return false;	break;
		case CR_HOLYCROSS:						if(sd && sd->state.K_LOCK_CR_HOLYCROSS)			return false;	break;
		case CR_GRANDCROSS:						if(sd && sd->state.K_LOCK_CR_GRANDCROSS)		return false;	break;
		case MO_CALLSPIRITS:					if(sd && sd->state.K_LOCK_MO_CALLSPIRITS)		return false;	break;
		case MO_ABSORBSPIRITS:					if(sd && sd->state.K_LOCK_MO_ABSORBSPIRITS)		return false;	break;
		case MO_BODYRELOCATION:					if(sd && sd->state.K_LOCK_MO_BODYRELOCATION)	return false;	break;
		case MO_INVESTIGATE:					if(sd && sd->state.K_LOCK_MO_INVESTIGATE)		return false;	break;
		case MO_FINGEROFFENSIVE:				if(sd && sd->state.K_LOCK_MO_FINGEROFFENSE)		return false;	break;
		case MO_EXPLOSIONSPIRITS:				if(sd && sd->state.K_LOCK_MO_EXPLODESPIRIT)		return false;	break;
		case MO_EXTREMITYFIST:					if(sd && sd->state.K_LOCK_MO_EXTREMITYFIST)		return false;	break;
		case MO_CHAINCOMBO:						if(sd && sd->state.K_LOCK_MO_CHAINCOMBO)		return false;	break;
		case MO_COMBOFINISH:					if(sd && sd->state.K_LOCK_MO_COMBOFINISH)		return false;	break;
		case SA_CASTCANCEL:						if(sd && sd->state.K_LOCK_SA_CASTCANCEL)		return false;	break;
		case SA_SPELLBREAKER:					if(sd && sd->state.K_LOCK_SA_SPELLBREAKER)		return false;	break;
		case SA_DISPELL:						if(sd && sd->state.K_LOCK_SA_DISPELL)			return false;	break;
		case SA_ABRACADABRA:					if(sd && sd->state.K_LOCK_SA_ABRACADABRA)		return false;	break;
		case SA_MONOCELL:						if(sd && sd->state.K_LOCK_SA_MONOCELL)			return false;	break;
		case SA_CLASSCHANGE:					if(sd && sd->state.K_LOCK_SA_CLASSCHANGE)		return false;	break;
		case SA_SUMMONMONSTER:					if(sd && sd->state.K_LOCK_SA_SUMMONMONSTER)		return false;	break;
		case SA_REVERSEORCISH:					if(sd && sd->state.K_LOCK_SA_REVERSEORCISH)		return false;	break;
		case SA_DEATH:							if(sd && sd->state.K_LOCK_SA_DEATH)				return false;	break;
		case SA_FORTUNE:						if(sd && sd->state.K_LOCK_SA_FORTUNE)			return false;	break;
		case SA_TAMINGMONSTER:					if(sd && sd->state.K_LOCK_SA_TAMINGMONSTER)		return false;	break;
		case SA_QUESTION:						if(sd && sd->state.K_LOCK_SA_QUESTION)			return false;	break;
		case SA_GRAVITY:						if(sd && sd->state.K_LOCK_SA_GRAVITY)			return false;	break;
		case SA_LEVELUP:						if(sd && sd->state.K_LOCK_SA_LEVELUP)			return false;	break;
		case SA_INSTANTDEATH:					if(sd && sd->state.K_LOCK_SA_INSTANTDEATH)		return false;	break;
		case SA_FULLRECOVERY:					if(sd && sd->state.K_LOCK_SA_FULLRECOVERY)		return false;	break;
		case SA_COMA:							if(sd && sd->state.K_LOCK_SA_COMA)				return false;	break;
		case BD_ADAPTATION:						if(sd && sd->state.K_LOCK_BD_ADAPTATION)		return false;	break;
		case BD_ENCORE:							if(sd && sd->state.K_LOCK_BD_ENCORE)			return false;	break;
		case BD_LULLABY:						if(sd && sd->state.K_LOCK_BD_LULLABY)			return false;	break;
		case BD_RICHMANKIM:						if(sd && sd->state.K_LOCK_BD_RICHMANKIM)		return false;	break;
		case BA_MUSICALSTRIKE:					if(sd && sd->state.K_LOCK_BA_MUSICALSTRIKE)		return false;	break;
		case BA_DISSONANCE:						if(sd && sd->state.K_LOCK_BA_DISSONANCE)		return false;	break;
		case BA_FROSTJOKER:						if(sd && sd->state.K_LOCK_BA_FROSTJOKER)		return false;	break;
		case BA_WHISTLE:						if(sd && sd->state.K_LOCK_BA_WHISTLE)			return false;	break;
		case BA_ASSASSINCROSS:					if(sd && sd->state.K_LOCK_BA_ASSASSINCROSS)		return false;	break;
		case BA_POEMBRAGI:						if(sd && sd->state.K_LOCK_BA_POEMBRAGI)			return false;	break;
		case BA_APPLEIDUN:						if(sd && sd->state.K_LOCK_BA_APPLEIDUN)			return false;	break;
		case DC_THROWARROW:						if(sd && sd->state.K_LOCK_DC_THROWARROW)		return false;	break;
		case DC_UGLYDANCE:						if(sd && sd->state.K_LOCK_DC_UGLYDANCE)			return false;	break;
		case DC_SCREAM:							if(sd && sd->state.K_LOCK_DC_SCREAM)			return false;	break;
		case DC_HUMMING:						if(sd && sd->state.K_LOCK_DC_HUMMING)			return false;	break;
		case DC_DONTFORGETME:					if(sd && sd->state.K_LOCK_DC_DONTFORGETME)		return false;	break;
		case DC_FORTUNEKISS:					if(sd && sd->state.K_LOCK_DC_FORTUNEKISS)		return false;	break;
		case DC_SERVICEFORYOU:					if(sd && sd->state.K_LOCK_DC_SERVICEFORYOU)		return false;	break;
		case LK_FURY:							if(sd && sd->state.K_LOCK_LK_FURY)				return false;	break;
		case HW_MAGICCRASHER:					if(sd && sd->state.K_LOCK_HW_MAGICCRASHER)		return false;	break;
		case PA_PRESSURE:						if(sd && sd->state.K_LOCK_PA_PRESSURE)			return false;	break;
		case CH_PALMSTRIKE:						if(sd && sd->state.K_LOCK_CH_PALMSTRIKE)		return false;	break;
		case CH_TIGERFIST:						if(sd && sd->state.K_LOCK_CH_TIGERFIST)			return false;	break;
		case CH_CHAINCRUSH:						if(sd && sd->state.K_LOCK_CH_CHAINCRUSH)		return false;	break;
		case PF_SOULCHANGE:						if(sd && sd->state.K_LOCK_PF_SOULCHANGE)		return false;	break;
		case PF_SOULBURN:						if(sd && sd->state.K_LOCK_PF_SOULBURN)			return false;	break;
		case ASC_BREAKER:						if(sd && sd->state.K_LOCK_ASC_BREAKER)			return false;	break;
		case SN_FALCONASSAULT:					if(sd && sd->state.K_LOCK_SN_FALCONASSAULT)		return false;	break;
		case SN_SHARPSHOOTING:					if(sd && sd->state.K_LOCK_SN_SHARPSHOOTING)		return false;	break;
		case CR_ALCHEMY:						if(sd && sd->state.K_LOCK_CR_ALCHEMY)			return false;	break;
		case CR_SYNTHESISPOTION:				if(sd && sd->state.K_LOCK_CR_SYNTHESIS)			return false;	break;
		case CG_ARROWVULCAN:					if(sd && sd->state.K_LOCK_CG_ARROWVULCAN)		return false;	break;
		case CG_MOONLIT:						if(sd && sd->state.K_LOCK_CG_MOONLIT)			return false;	break;
		case CG_MARIONETTE:						if(sd && sd->state.K_LOCK_CG_MARIONETTE)		return false;	break;
		case LK_SPIRALPIERCE:					if(sd && sd->state.K_LOCK_LK_SPIRALPIERCE)		return false;	break;
		case LK_HEADCRUSH:						if(sd && sd->state.K_LOCK_LK_HEADCRUSH)			return false;	break;
		case LK_JOINTBEAT:						if(sd && sd->state.K_LOCK_LK_JOINTBEAT)			return false;	break;
		case HW_NAPALMVULCAN:					if(sd && sd->state.K_LOCK_HW_NAPALMVULCAN)		return false;	break;
		case CH_SOULCOLLECT:					if(sd && sd->state.K_LOCK_CH_SOULCOLLECT)		return false;	break;
		case PF_MINDBREAKER:					if(sd && sd->state.K_LOCK_PF_MINDBREAKER)		return false;	break;
		case PF_SPIDERWEB:						if(sd && sd->state.K_LOCK_PF_SPIDERWEB)			return false;	break;
		case ASC_METEORASSAULT:					if(sd && sd->state.K_LOCK_ASC_METEORASSAULT)	return false;	break;
		case TK_STORMKICK:						if(sd && sd->state.K_LOCK_TK_STORMKICK)			return false;	break;
		case TK_DOWNKICK:						if(sd && sd->state.K_LOCK_TK_DOWNKICK)			return false;	break;
		case TK_TURNKICK:						if(sd && sd->state.K_LOCK_TK_TURNKICK)			return false;	break;
		case TK_JUMPKICK:						if(sd && sd->state.K_LOCK_TK_JUMPKICK)			return false;	break;
		case TK_POWER:							if(sd && sd->state.K_LOCK_TK_POWER)				return false;	break;
		case TK_HIGHJUMP:						if(sd && sd->state.K_LOCK_TK_HIGHJUMP)			return false;	break;
		case SL_KAIZEL:							if(sd && sd->state.K_LOCK_SL_KAIZEL)			return false;	break;
		case SL_KAAHI:							if(sd && sd->state.K_LOCK_SL_KAAHI)				return false;	break;
		case SL_KAUPE:							if(sd && sd->state.K_LOCK_SL_KAUPE)				return false;	break;
		case SL_KAITE:							if(sd && sd->state.K_LOCK_SL_KAITE)				return false;	break;
		case SL_KAINA:							if(sd && sd->state.K_LOCK_SL_KAINA)				return false;	break;
		case SL_STIN:							if(sd && sd->state.K_LOCK_SL_STIN)				return false;	break;
		case SL_STUN:							if(sd && sd->state.K_LOCK_SL_STUN)				return false;	break;
		case SL_SMA:							if(sd && sd->state.K_LOCK_SL_SMA)				return false;	break;
		case SL_SWOO:							if(sd && sd->state.K_LOCK_SL_SWOO)				return false;	break;
		case SL_SKE:							if(sd && sd->state.K_LOCK_SL_SKE)				return false;	break;
		case SL_SKA:							if(sd && sd->state.K_LOCK_SL_SKA)				return false;	break;
		case ST_FULLSTRIP:						if(sd && sd->state.K_LOCK_ST_FULLSTRIP)			return false;	break;
		case CR_SLIMPITCHER:					if(sd && sd->state.K_LOCK_CR_SLIMPITCHER)		return false;	break;
		case CR_FULLPROTECTION:					if(sd && sd->state.K_LOCK_CR_FULLPROTECTION)	return false;	break;
		case PA_SHIELDCHAIN:					if(sd && sd->state.K_LOCK_PA_SHIELDCHAIN)		return false;	break;
		case HP_MANARECHARGE:					if(sd && sd->state.K_LOCK_HP_MANARECHARGE)		return false;	break;
		case PF_DOUBLECASTING:					if(sd && sd->state.K_LOCK_PF_DOUBLECASTING)		return false;	break;
		case HW_GANBANTEIN:						if(sd && sd->state.K_LOCK_HW_GANBANTEIN)		return false;	break;
		case HW_GRAVITATION:					if(sd && sd->state.K_LOCK_HW_GRAVITATION)		return false;	break;
		case WS_CARTTERMINATION:				if(sd && sd->state.K_LOCK_WS_CARTTERMINATION)	return false;	break;
		case CG_HERMODE:						if(sd && sd->state.K_LOCK_CG_HERMODE)			return false;	break;
		case CG_TAROTCARD:						if(sd && sd->state.K_LOCK_CG_TAROTCARD)			return false;	break;
		case CR_ACIDDEMONSTRATION:				if(sd && sd->state.K_LOCK_CR_ACIDDEMO)			return false;	break;
		case SL_HIGH:							if(sd && sd->state.K_LOCK_SL_HIGH)				return false;	break;
		case HT_POWER:							if(sd && sd->state.K_LOCK_HT_POWER)				return false;	break;
		case GS_TRIPLEACTION:					if(sd && sd->state.K_LOCK_GS_TRIPLEACTION)		return false;	break;
		case GS_BULLSEYE:						if(sd && sd->state.K_LOCK_GS_BULLSEYE)			return false;	break;
		case GS_MADNESSCANCEL:					if(sd && sd->state.K_LOCK_GS_MADNESSCANCEL)		return false;	break;
		case GS_INCREASING:						if(sd && sd->state.K_LOCK_GS_INCREASING)		return false;	break;
		case GS_MAGICALBULLET:					if(sd && sd->state.K_LOCK_GS_MAGICALBULLET)		return false;	break;
		case GS_CRACKER:						if(sd && sd->state.K_LOCK_GS_CRACKER)			return false;	break;
		case GS_SINGLEACTION:					if(sd && sd->state.K_LOCK_GS_SINGLEACTION)		return false;	break;
		case GS_CHAINACTION:					if(sd && sd->state.K_LOCK_GS_CHAINACTION)		return false;	break;
		case GS_TRACKING:						if(sd && sd->state.K_LOCK_GS_TRACKING)			return false;	break;
		case GS_DISARM:							if(sd && sd->state.K_LOCK_GS_DISARM)			return false;	break;
		case GS_PIERCINGSHOT:					if(sd && sd->state.K_LOCK_GS_PIERCINGSHOT)		return false;	break;
		case GS_RAPIDSHOWER:					if(sd && sd->state.K_LOCK_GS_RAPIDSHOWER)		return false;	break;
		case GS_DESPERADO:						if(sd && sd->state.K_LOCK_GS_DESPERADO)			return false;	break;
		case GS_GATLINGFEVER:					if(sd && sd->state.K_LOCK_GS_GATLINGFEVER)		return false;	break;
		case GS_DUST:							if(sd && sd->state.K_LOCK_GS_DUST)				return false;	break;
		case GS_FULLBUSTER:						if(sd && sd->state.K_LOCK_GS_FULLBUSTER)		return false;	break;
		case GS_SPREADATTACK:					if(sd && sd->state.K_LOCK_GS_SPREADATTACK)		return false;	break;
		case GS_GROUNDDRIFT:					if(sd && sd->state.K_LOCK_GS_GROUNDDRIFT)		return false;	break;
		case NJ_TOBIDOUGU:						if(sd && sd->state.K_LOCK_NJ_TOBIDOUGU)			return false;	break;
		case NJ_SYURIKEN:						if(sd && sd->state.K_LOCK_NJ_SYURIKEN)			return false;	break;
		case NJ_KUNAI:							if(sd && sd->state.K_LOCK_NJ_KUNAI)				return false;	break;
		case NJ_HUUMA:							if(sd && sd->state.K_LOCK_NJ_HUUMA)				return false;	break;
		case NJ_ZENYNAGE:						if(sd && sd->state.K_LOCK_NJ_ZENYNAGE)			return false;	break;
		case NJ_TATAMIGAESHI:					if(sd && sd->state.K_LOCK_NJ_TATAMIGAESHI)		return false;	break;
		case NJ_KASUMIKIRI:						if(sd && sd->state.K_LOCK_NJ_KASUMIKIRI)		return false;	break;
		case NJ_SHADOWJUMP:						if(sd && sd->state.K_LOCK_NJ_SHADOWJUMP)		return false;	break;
		case NJ_KIRIKAGE:						if(sd && sd->state.K_LOCK_NJ_KIRIKAGE)			return false;	break;
		case NJ_UTSUSEMI:						if(sd && sd->state.K_LOCK_NJ_UTSUSEMI)			return false;	break;
		case NJ_BUNSINJYUTSU:					if(sd && sd->state.K_LOCK_NJ_BUNSINJYUTSU)		return false;	break;
		case NJ_NINPOU:							if(sd && sd->state.K_LOCK_NJ_NINPOU)			return false;	break;
		case NJ_KOUENKA:						if(sd && sd->state.K_LOCK_NJ_KOUENKA)			return false;	break;
		case NJ_KAENSIN:						if(sd && sd->state.K_LOCK_NJ_KAENSIN)			return false;	break;
		case NJ_BAKUENRYU:						if(sd && sd->state.K_LOCK_NJ_BAKUENRYU)			return false;	break;
		case NJ_HYOUSENSOU:						if(sd && sd->state.K_LOCK_NJ_HYOUSENSOU)		return false;	break;
		case NJ_SUITON:							if(sd && sd->state.K_LOCK_NJ_SUITON)			return false;	break;
		case NJ_HYOUSYOURAKU:					if(sd && sd->state.K_LOCK_NJ_HYOUSYOURAKU)		return false;	break;
		case NJ_HUUJIN:							if(sd && sd->state.K_LOCK_NJ_HUUJIN)			return false;	break;
		case NJ_RAIGEKISAI:						if(sd && sd->state.K_LOCK_NJ_RAIGEKISAI)		return false;	break;
		case NJ_KAMAITACHI:						if(sd && sd->state.K_LOCK_NJ_KAMAITACHI)		return false;	break;
		case NJ_NEN:							if(sd && sd->state.K_LOCK_NJ_NEN)				return false;	break;
		case NJ_ISSEN:							if(sd && sd->state.K_LOCK_NJ_ISSEN)				return false;	break;
		case KN_CHARGEATK:						if(sd && sd->state.K_LOCK_KN_CHARGEATK)			return false;	break;
		case AS_VENOMKNIFE:						if(sd && sd->state.K_LOCK_AS_VENOMKNIFE)		return false;	break;
		case RG_CLOSECONFINE:					if(sd && sd->state.K_LOCK_RG_CLOSECONFINE)		return false;	break;
		case WZ_SIGHTBLASTER:					if(sd && sd->state.K_LOCK_WZ_SIGHTBLASTER)		return false;	break;
		case HT_PHANTASMIC:						if(sd && sd->state.K_LOCK_HT_PHANTASMIC)		return false;	break;
		case BA_PANGVOICE:						if(sd && sd->state.K_LOCK_BA_PANGVOICE)			return false;	break;
		case DC_WINKCHARM:						if(sd && sd->state.K_LOCK_DC_WINKCHARM)			return false;	break;
		case PR_REDEMPTIO:						if(sd && sd->state.K_LOCK_PR_REDEMPTIO)			return false;	break;
		case MO_KITRANSLATION:					if(sd && sd->state.K_LOCK_MO_KITRANSLATION)		return false;	break;
		case MO_BALKYOUNG:						if(sd && sd->state.K_LOCK_MO_BALKYOUNG)			return false;	break;
		case RK_SONICWAVE:						if(sd && sd->state.K_LOCK_RK_SONICWAVE)			return false;	break;
		case RK_DEATHBOUND:						if(sd && sd->state.K_LOCK_RK_DEATHBOUND)		return false;	break;
		case RK_HUNDREDSPEAR:					if(sd && sd->state.K_LOCK_RK_HUNDREDSPEAR)		return false;	break;
		case RK_WINDCUTTER:						if(sd && sd->state.K_LOCK_RK_WINDCUTTER)		return false;	break;
		case RK_IGNITIONBREAK:					if(sd && sd->state.K_LOCK_RK_IGNITIONBREAK)		return false;	break;
		case RK_DRAGONBREATH:					if(sd && sd->state.K_LOCK_RK_DRAGONBREATH)		return false;	break;
		case RK_CRUSHSTRIKE:					if(sd && sd->state.K_LOCK_RK_CRUSHSTRIKE)		return false;	break;
		case RK_STORMBLAST:						if(sd && sd->state.K_LOCK_RK_STORMBLAST)		return false;	break;
		case RK_PHANTOMTHRUST:					if(sd && sd->state.K_LOCK_RK_PHANTOMTHRUST)		return false;	break;
		case GC_CROSSIMPACT:					if(sd && sd->state.K_LOCK_GC_CROSSIMPACT)		return false;	break;
		case GC_WEAPONCRUSH:					if(sd && sd->state.K_LOCK_GC_WEAPONCRUSH)		return false;	break;
		case GC_ROLLINGCUTTER:					if(sd && sd->state.K_LOCK_GC_ROLLINGCUTTER)		return false;	break;
		case GC_CROSSRIPPERSLASHER:				if(sd && sd->state.K_LOCK_GC_CROSSSLASHER)		return false;	break;
		case AB_JUDEX:							if(sd && sd->state.K_LOCK_AB_JUDEX)				return false;	break;
		case AB_ADORAMUS:						if(sd && sd->state.K_LOCK_AB_ADORAMUS)			return false;	break;
		case AB_CHEAL:							if(sd && sd->state.K_LOCK_AB_CHEAL)				return false;	break;
		case AB_EPICLESIS:						if(sd && sd->state.K_LOCK_AB_EPICLESIS)			return false;	break;
		case AB_PRAEFATIO:						if(sd && sd->state.K_LOCK_AB_PRAEFATIO)			return false;	break;
		case AB_EUCHARISTICA:					if(sd && sd->state.K_LOCK_AB_EUCHARISTICA)		return false;	break;
		case AB_RENOVATIO:						if(sd && sd->state.K_LOCK_AB_RENOVATIO)			return false;	break;
		case AB_HIGHNESSHEAL:					if(sd && sd->state.K_LOCK_AB_HIGHNESSHEAL)		return false;	break;
		case AB_CLEARANCE:						if(sd && sd->state.K_LOCK_AB_CLEARANCE)			return false;	break;
		case AB_EXPIATIO:						if(sd && sd->state.K_LOCK_AB_EXPIATIO)			return false;	break;
		case AB_DUPLELIGHT:						if(sd && sd->state.K_LOCK_AB_DUPLELIGHT)		return false;	break;
		case AB_DUPLELIGHT_MELEE:				if(sd && sd->state.K_LOCK_AB_DUPLIGHT_MELEE)	return false;	break;
		case AB_DUPLELIGHT_MAGIC:				if(sd && sd->state.K_LOCK_AB_DUPLIGHT_MAGIC)	return false;	break;
		case AB_SILENTIUM:						if(sd && sd->state.K_LOCK_AB_SILENTIUM)			return false;	break;
		case WL_WHITEIMPRISON:					if(sd && sd->state.K_LOCK_WL_WHITEIMPRISON)		return false;	break;
		case WL_SOULEXPANSION:					if(sd && sd->state.K_LOCK_WL_SOULEXPANSION)		return false;	break;
		case WL_FROSTMISTY:						if(sd && sd->state.K_LOCK_WL_FROSTMISTY)		return false;	break;
		case WL_JACKFROST:						if(sd && sd->state.K_LOCK_WL_JACKFROST)			return false;	break;
		case WL_MARSHOFABYSS:					if(sd && sd->state.K_LOCK_WL_MARSHOFABYSS)		return false;	break;
		case WL_RADIUS:							if(sd && sd->state.K_LOCK_WL_RADIUS)			return false;	break;
		case WL_STASIS:							if(sd && sd->state.K_LOCK_WL_STASIS)			return false;	break;
		case WL_DRAINLIFE:						if(sd && sd->state.K_LOCK_WL_DRAINLIFE)			return false;	break;
		case WL_CRIMSONROCK:					if(sd && sd->state.K_LOCK_WL_CRIMSONROCK)		return false;	break;
		case WL_HELLINFERNO:					if(sd && sd->state.K_LOCK_WL_HELLINFERNO)		return false;	break;
		case WL_COMET:							if(sd && sd->state.K_LOCK_WL_COMET)				return false;	break;
		case WL_CHAINLIGHTNING:					if(sd && sd->state.K_LOCK_WL_CHAINLIGHTNING)	return false;	break;
		case WL_CHAINLIGHTNING_ATK:				if(sd && sd->state.K_LOCK_WL_CHAINLIGHTNING_)	return false;	break;
		case WL_EARTHSTRAIN:					if(sd && sd->state.K_LOCK_WL_EARTHSTRAIN)		return false;	break;
		case WL_TETRAVORTEX:					if(sd && sd->state.K_LOCK_WL_TETRAVORTEX)		return false;	break;
		case WL_TETRAVORTEX_FIRE:				if(sd && sd->state.K_LOCK_WL_TETRA_FIRE)		return false;	break;
		case WL_TETRAVORTEX_WATER:				if(sd && sd->state.K_LOCK_WL_TETRA_WATER)		return false;	break;
		case WL_TETRAVORTEX_WIND:				if(sd && sd->state.K_LOCK_WL_TETRA_WIND)		return false;	break;
		case WL_TETRAVORTEX_GROUND:				if(sd && sd->state.K_LOCK_WL_TETRA_GROUND)		return false;	break;
		case WL_RELEASE:						if(sd && sd->state.K_LOCK_WL_RELEASE)			return false;	break;
		case WL_READING_SB:						if(sd && sd->state.K_LOCK_WL_READING_SB)		return false;	break;
		case WL_FREEZE_SP:						if(sd && sd->state.K_LOCK_WL_FREEZE_SP)			return false;	break;
		case RA_ARROWSTORM:						if(sd && sd->state.K_LOCK_RA_ARROWSTORM)		return false;	break;
		case RA_AIMEDBOLT:						if(sd && sd->state.K_LOCK_RA_AIMEDBOLT)			return false;	break;
		case RA_WUGSTRIKE:						if(sd && sd->state.K_LOCK_RA_WUGSTRIKE)			return false;	break;
		case RA_WUGBITE:						if(sd && sd->state.K_LOCK_RA_WUGBITE)			return false;	break;
		case NC_BOOSTKNUCKLE:					if(sd && sd->state.K_LOCK_NC_BOOSTKNUCKLE)		return false;	break;
		case NC_PILEBUNKER:						if(sd && sd->state.K_LOCK_NC_PILEBUNKER)		return false;	break;
		case NC_VULCANARM:						if(sd && sd->state.K_LOCK_NC_VULCANARM)			return false;	break;
		case NC_FLAMELAUNCHER:					if(sd && sd->state.K_LOCK_NC_FLAMELAUNCHER)		return false;	break;
		case NC_COLDSLOWER:						if(sd && sd->state.K_LOCK_NC_COLDSLOWER)		return false;	break;
		case NC_ARMSCANNON:						if(sd && sd->state.K_LOCK_NC_ARMSCANNON)		return false;	break;
		case NC_ACCELERATION:					if(sd && sd->state.K_LOCK_NC_ACCELERATION)		return false;	break;
		case NC_F_SIDESLIDE:					if(sd && sd->state.K_LOCK_NC_F_SIDESLIDE)		return false;	break;
		case NC_B_SIDESLIDE:					if(sd && sd->state.K_LOCK_NC_B_SIDESLIDE)		return false;	break;
		case NC_MAINFRAME:						if(sd && sd->state.K_LOCK_NC_MAINFRAME)			return false;	break;
		case NC_SHAPESHIFT:						if(sd && sd->state.K_LOCK_NC_SHAPESHIFT)		return false;	break;
		case NC_INFRAREDSCAN:					if(sd && sd->state.K_LOCK_NC_INFRAREDSCAN)		return false;	break;
		case NC_ANALYZE:						if(sd && sd->state.K_LOCK_NC_ANALYZE)			return false;	break;
		case NC_MAGNETICFIELD:					if(sd && sd->state.K_LOCK_NC_MAGNETICFIELD)		return false;	break;
		case NC_NEUTRALBARRIER:					if(sd && sd->state.K_LOCK_NC_NEUTRALBARRIER)	return false;	break;
		case NC_STEALTHFIELD:					if(sd && sd->state.K_LOCK_NC_STEALTHFIELD)		return false;	break;
		case NC_AXEBOOMERANG:					if(sd && sd->state.K_LOCK_NC_AXEBOOMERANG)		return false;	break;
		case NC_POWERSWING:						if(sd && sd->state.K_LOCK_NC_POWERSWING)		return false;	break;
		case NC_AXETORNADO:						if(sd && sd->state.K_LOCK_NC_AXETORNADO)		return false;	break;
		case NC_SILVERSNIPER:					if(sd && sd->state.K_LOCK_NC_SILVERSNIPER)		return false;	break;
		case NC_MAGICDECOY:						if(sd && sd->state.K_LOCK_NC_MAGICDECOY)		return false;	break;
		case NC_DISJOINT:						if(sd && sd->state.K_LOCK_NC_DISJOINT)			return false;	break;
		case SC_FATALMENACE:					if(sd && sd->state.K_LOCK_SC_FATALMENACE)		return false;	break;
		case SC_TRIANGLESHOT:					if(sd && sd->state.K_LOCK_SC_TRIANGLESHOT)		return false;	break;
		case SC_INVISIBILITY:					if(sd && sd->state.K_LOCK_SC_INVISIBILITY)		return false;	break;
		case SC_ENERVATION:						if(sd && sd->state.K_LOCK_SC_ENERVATION)		return false;	break;
		case SC_GROOMY:							if(sd && sd->state.K_LOCK_SC_GROOMY)			return false;	break;
		case SC_IGNORANCE:						if(sd && sd->state.K_LOCK_SC_IGNORANCE)			return false;	break;
		case SC_LAZINESS:						if(sd && sd->state.K_LOCK_SC_LAZINESS)			return false;	break;
		case SC_UNLUCKY:						if(sd && sd->state.K_LOCK_SC_UNLUCKY)			return false;	break;
		case SC_WEAKNESS:						if(sd && sd->state.K_LOCK_SC_WEAKNESS)			return false;	break;
		case SC_STRIPACCESSARY:					if(sd && sd->state.K_LOCK_SC_STRIPACCESSARY)	return false;	break;
		case SC_MANHOLE:						if(sd && sd->state.K_LOCK_SC_MANHOLE)			return false;	break;
		case SC_DIMENSIONDOOR:					if(sd && sd->state.K_LOCK_SC_DIMENSIONDOOR)		return false;	break;
		case SC_CHAOSPANIC:						if(sd && sd->state.K_LOCK_SC_CHAOSPANIC)		return false;	break;
		case SC_MAELSTROM:						if(sd && sd->state.K_LOCK_SC_MAELSTROM)			return false;	break;
		case SC_BLOODYLUST:						if(sd && sd->state.K_LOCK_SC_BLOODYLUST)		return false;	break;
		case SC_FEINTBOMB:						if(sd && sd->state.K_LOCK_SC_FEINTBOMB)			return false;	break;
		case LG_CANNONSPEAR:					if(sd && sd->state.K_LOCK_LG_CANNONSPEAR)		return false;	break;
		case LG_BANISHINGPOINT:					if(sd && sd->state.K_LOCK_LG_BANISHINGPOINT)	return false;	break;
		case LG_TRAMPLE:						if(sd && sd->state.K_LOCK_LG_TRAMPLE)			return false;	break;
		case LG_PINPOINTATTACK:					if(sd && sd->state.K_LOCK_LG_PINPOINTATTACK)	return false;	break;
		case LG_RAGEBURST:						if(sd && sd->state.K_LOCK_LG_RAGEBURST)			return false;	break;
		case LG_EXEEDBREAK:						if(sd && sd->state.K_LOCK_LG_EXEEDBREAK)		return false;	break;
		case LG_OVERBRAND:						if(sd && sd->state.K_LOCK_LG_OVERBRAND)			return false;	break;
		case LG_BANDING:						if(sd && sd->state.K_LOCK_LG_BANDING)			return false;	break;
		case LG_MOONSLASHER:					if(sd && sd->state.K_LOCK_LG_MOONSLASHER)		return false;	break;
		case LG_RAYOFGENESIS:					if(sd && sd->state.K_LOCK_LG_RAYOFGENESIS)		return false;	break;
		case LG_PIETY:							if(sd && sd->state.K_LOCK_LG_PIETY)				return false;	break;
		case LG_EARTHDRIVE:						if(sd && sd->state.K_LOCK_LG_EARTHDRIVE)		return false;	break;
		case LG_HESPERUSLIT:					if(sd && sd->state.K_LOCK_LG_HESPERUSLIT)		return false;	break;
		case SR_DRAGONCOMBO:					if(sd && sd->state.K_LOCK_SR_DRAGONCOMBO)		return false;	break;
		case SR_SKYNETBLOW:						if(sd && sd->state.K_LOCK_SR_SKYNETBLOW)		return false;	break;
		case SR_EARTHSHAKER:					if(sd && sd->state.K_LOCK_SR_EARTHSHAKER)		return false;	break;
		case SR_FALLENEMPIRE:					if(sd && sd->state.K_LOCK_SR_FALLENEMPIRE)		return false;	break;
		case SR_TIGERCANNON:					if(sd && sd->state.K_LOCK_SR_TIGERCANNON)		return false;	break;
		case SR_HELLGATE:						if(sd && sd->state.K_LOCK_SR_HELLGATE)			return false;	break;
		case SR_RAMPAGEBLASTER:					if(sd && sd->state.K_LOCK_SR_RAMPAGEBLASTER)	return false;	break;
		case SR_CRESCENTELBOW:					if(sd && sd->state.K_LOCK_SR_CRESCENTELBOW)		return false;	break;
		case SR_CURSEDCIRCLE:					if(sd && sd->state.K_LOCK_SR_CURSEDCIRCLE)		return false;	break;
		case SR_LIGHTNINGWALK:					if(sd && sd->state.K_LOCK_SR_LIGHTNINGWALK)		return false;	break;
		case SR_KNUCKLEARROW:					if(sd && sd->state.K_LOCK_SR_KNUCKLEARROW)		return false;	break;
		case SR_WINDMILL:						if(sd && sd->state.K_LOCK_SR_WINDMILL)			return false;	break;
		case SR_RAISINGDRAGON:					if(sd && sd->state.K_LOCK_SR_RAISINGDRAGON)		return false;	break;
		case SR_GENTLETOUCH:					if(sd && sd->state.K_LOCK_SR_GENTLETOUCH)		return false;	break;
		case SR_ASSIMILATEPOWER:				if(sd && sd->state.K_LOCK_SR_ASSIMILATE)		return false;	break;
		case SR_POWERVELOCITY:					if(sd && sd->state.K_LOCK_SR_POWERVELOCITY)		return false;	break;
		case SR_CRESCENTELBOW_AUTOSPELL:		if(sd && sd->state.K_LOCK_SR_CRESBOW_AUTO)		return false;	break;
		case SR_GATEOFHELL:						if(sd && sd->state.K_LOCK_SR_GATEOFHELL)		return false;	break;
		case SR_GENTLETOUCH_QUIET:				if(sd && sd->state.K_LOCK_SR_GENTLE_QUIET)		return false;	break;
		case SR_GENTLETOUCH_CURE:				if(sd && sd->state.K_LOCK_SR_GENTLE_CURE)		return false;	break;
		case SR_GENTLETOUCH_ENERGYGAIN:			if(sd && sd->state.K_LOCK_SR_GENTLE_EGAIN)		return false;	break;
		case SR_GENTLETOUCH_CHANGE:				if(sd && sd->state.K_LOCK_SR_GENTLE_CHANGE)		return false;	break;
		case SR_GENTLETOUCH_REVITALIZE:			if(sd && sd->state.K_LOCK_SR_GENTLE_REVIT)		return false;	break;
		case WA_SWING_DANCE:					if(sd && sd->state.K_LOCK_WA_SWING_DANCE)		return false;	break;
		case WA_SYMPHONY_OF_LOVER:				if(sd && sd->state.K_LOCK_WA_SYMPHONY)			return false;	break;
		case WA_MOONLIT_SERENADE:				if(sd && sd->state.K_LOCK_WA_MOONLIT)			return false;	break;
		case MI_RUSH_WINDMILL:					if(sd && sd->state.K_LOCK_MI_RUSH_WINDMILL)		return false;	break;
		case MI_ECHOSONG:						if(sd && sd->state.K_LOCK_MI_ECHOSONG)			return false;	break;
		case MI_HARMONIZE:						if(sd && sd->state.K_LOCK_MI_HARMONIZE)			return false;	break;
		case WM_LESSON:							if(sd && sd->state.K_LOCK_WM_LESSON)			return false;	break;
		case WM_METALICSOUND:					if(sd && sd->state.K_LOCK_WM_METALICSOUND)		return false;	break;
		case WM_REVERBERATION:					if(sd && sd->state.K_LOCK_WM_REVERB)			return false;	break;
		case WM_REVERBERATION_MELEE:			if(sd && sd->state.K_LOCK_WM_REVERB_MELEE)		return false;	break;
		case WM_REVERBERATION_MAGIC:			if(sd && sd->state.K_LOCK_WM_REVERB_MAGIC)		return false;	break;
		case WM_DOMINION_IMPULSE:				if(sd && sd->state.K_LOCK_WM_DOMINION)			return false;	break;
		case WM_SEVERE_RAINSTORM:				if(sd && sd->state.K_LOCK_WM_RAINSTORM)			return false;	break;
		case WM_SEVERE_RAINSTORM_MELEE:			if(sd && sd->state.K_LOCK_WM_RAINSTORM_)		return false;	break;
		case WM_POEMOFNETHERWORLD:				if(sd && sd->state.K_LOCK_WM_POEM)				return false;	break;
		case WM_VOICEOFSIREN:					if(sd && sd->state.K_LOCK_WM_VOICEOFSIREN)		return false;	break;
		case WM_DEADHILLHERE:					if(sd && sd->state.K_LOCK_WM_DEADHILLHERE)		return false;	break;
		case WM_LULLABY_DEEPSLEEP:				if(sd && sd->state.K_LOCK_WM_DEEPSLEEP)			return false;	break;
		case WM_SIRCLEOFNATURE:					if(sd && sd->state.K_LOCK_WM_SIRCLEOFNATURE)	return false;	break;
		case WM_RANDOMIZESPELL:					if(sd && sd->state.K_LOCK_WM_RANDOMIZESPELL)	return false;	break;
		case WM_GLOOMYDAY:						if(sd && sd->state.K_LOCK_WM_GLOOMYDAY)			return false;	break;
		case WM_GREAT_ECHO:						if(sd && sd->state.K_LOCK_WM_GREAT_ECHO)		return false;	break;
		case WM_SONG_OF_MANA:					if(sd && sd->state.K_LOCK_WM_SONG_OF_MANA)		return false;	break;
		case WM_DANCE_WITH_WUG:					if(sd && sd->state.K_LOCK_WM_DANCE_WITH_WUG)	return false;	break;
		case WM_SOUND_OF_DESTRUCTION:			if(sd && sd->state.K_LOCK_WM_DESTRUCTION)		return false;	break;
		case WM_SATURDAY_NIGHT_FEVER:			if(sd && sd->state.K_LOCK_WM_SATNIGHT_FEVER)	return false;	break;
		case WM_LERADS_DEW:						if(sd && sd->state.K_LOCK_WM_LERADS_DEW)		return false;	break;
		case WM_MELODYOFSINK:					if(sd && sd->state.K_LOCK_WM_MELODYOFSINK)		return false;	break;
		case WM_BEYOND_OF_WARCRY:				if(sd && sd->state.K_LOCK_WM_WARCRY)			return false;	break;
		case WM_UNLIMITED_HUMMING_VOICE:		if(sd && sd->state.K_LOCK_WM_HUMMING_VOICE)		return false;	break;
		case SO_FIREWALK:						if(sd && sd->state.K_LOCK_SO_FIREWALK)			return false;	break;
		case SO_ELECTRICWALK:					if(sd && sd->state.K_LOCK_SO_ELECTRICWALK)		return false;	break;
		case SO_SPELLFIST:						if(sd && sd->state.K_LOCK_SO_SPELLFIST)			return false;	break;
		case SO_EARTHGRAVE:						if(sd && sd->state.K_LOCK_SO_EARTHGRAVE)		return false;	break;
		case SO_DIAMONDDUST:					if(sd && sd->state.K_LOCK_SO_DIAMONDDUST)		return false;	break;
		case SO_POISON_BUSTER:					if(sd && sd->state.K_LOCK_SO_POISON_BUSTER)		return false;	break;
		case SO_PSYCHIC_WAVE:					if(sd && sd->state.K_LOCK_SO_PSYCHIC_WAVE)		return false;	break;
		case SO_CLOUD_KILL:						if(sd && sd->state.K_LOCK_SO_CLOUD_KILL)		return false;	break;
		case SO_STRIKING:						if(sd && sd->state.K_LOCK_SO_STRIKING)			return false;	break;
		case SO_WARMER:							if(sd && sd->state.K_LOCK_SO_WARMER)			return false;	break;
		case SO_VACUUM_EXTREME:					if(sd && sd->state.K_LOCK_SO_VACUUM_EXTREME)	return false;	break;
		case SO_VARETYR_SPEAR:					if(sd && sd->state.K_LOCK_SO_VARETYR_SPEAR)		return false;	break;
		case SO_ARRULLO:						if(sd && sd->state.K_LOCK_SO_ARRULLO)			return false;	break;
		case SO_EL_CONTROL:						if(sd && sd->state.K_LOCK_SO_EL_CONTROL)		return false;	break;
		case SO_EL_ACTION:						if(sd && sd->state.K_LOCK_SO_EL_ACTION)			return false;	break;
		case SO_EL_ANALYSIS:					if(sd && sd->state.K_LOCK_SO_EL_ANALYSIS)		return false;	break;
		case SO_EL_SYMPATHY:					if(sd && sd->state.K_LOCK_SO_EL_SYMPATHY)		return false;	break;
		case SO_EL_CURE:						if(sd && sd->state.K_LOCK_SO_EL_CURE)			return false;	break;
		case GN_CART_TORNADO:					if(sd && sd->state.K_LOCK_GN_CART_TORNADO)		return false;	break;
		case GN_CARTCANNON:						if(sd && sd->state.K_LOCK_GN_CARTCANNON)		return false;	break;
		case GN_THORNS_TRAP:					if(sd && sd->state.K_LOCK_GN_THORNS_TRAP)		return false;	break;
		case GN_BLOOD_SUCKER:					if(sd && sd->state.K_LOCK_GN_BLOOD_SUCKER)		return false;	break;
		case GN_SPORE_EXPLOSION:				if(sd && sd->state.K_LOCK_GN_SPORE_EXPLO)		return false;	break;
		case GN_WALLOFTHORN:					if(sd && sd->state.K_LOCK_GN_WALLOFTHORN)		return false;	break;
		case GN_CRAZYWEED:						if(sd && sd->state.K_LOCK_GN_CRAZYWEED)			return false;	break;
		case GN_CRAZYWEED_ATK:					if(sd && sd->state.K_LOCK_GN_CRAZYWEED_)		return false;	break;
		case GN_DEMONIC_FIRE:					if(sd && sd->state.K_LOCK_GN_DEMONIC_FIRE)		return false;	break;
		case GN_FIRE_EXPANSION:					if(sd && sd->state.K_LOCK_GN_FIRE_EXPAN)		return false;	break;
		case GN_FIRE_EXPANSION_SMOKE_POWDER:	if(sd && sd->state.K_LOCK_GN_FIRE_EXSMOKE)		return false;	break;
		case GN_FIRE_EXPANSION_TEAR_GAS:		if(sd && sd->state.K_LOCK_GN_FIRE_EXTEAR)		return false;	break;
		case GN_FIRE_EXPANSION_ACID:			if(sd && sd->state.K_LOCK_GN_FIRE_EXACID)		return false;	break;
		case GN_HELLS_PLANT:					if(sd && sd->state.K_LOCK_GN_HELLS_PLANT)		return false;	break;
		case GN_HELLS_PLANT_ATK:				if(sd && sd->state.K_LOCK_GN_HELLS_PLANT_)		return false;	break;
		case GN_MANDRAGORA:						if(sd && sd->state.K_LOCK_GN_MANDRAGORA)		return false;	break;
		case GN_SLINGITEM:						if(sd && sd->state.K_LOCK_GN_SLINGITEM)			return false;	break;
		case GN_SLINGITEM_RANGEMELEEATK:		if(sd && sd->state.K_LOCK_GN_SLINGITEM_)		return false;	break;
		case GN_CHANGEMATERIAL:					if(sd && sd->state.K_LOCK_GN_CHANGEMATERIAL)	return false;	break;
		case AB_SECRAMENT:						if(sd && sd->state.K_LOCK_AB_SECRAMENT)			return false;	break;
		case SR_HOWLINGOFLION:					if(sd && sd->state.K_LOCK_SR_HOWLINGOFLION)		return false;	break;
		case SR_RIDEINLIGHTNING:				if(sd && sd->state.K_LOCK_SR_RIDELIGHTNING)		return false;	break;
		case LG_OVERBRAND_BRANDISH:				if(sd && sd->state.K_LOCK_LG_OVER_BRANDISH)		return false;	break;
		case LG_OVERBRAND_PLUSATK:				if(sd && sd->state.K_LOCK_LG_OVER_PLUSATK)		return false;	break;	
}

	// perform skill-specific checks (and actions)
	switch( skill )
	{
	case PR_BENEDICTIO:
		skill_check_pc_partner(sd, skill, &lv, 1, 1);
		break;
	case AM_CANNIBALIZE:
	case AM_SPHEREMINE:
	{
		int c=0;
		int summons[5] = { 1589, 1579, 1575, 1555, 1590 };
		//int summons[5] = { 1020, 1068, 1118, 1500, 1368 };
		int maxcount = (skill==AM_CANNIBALIZE)? 6-lv : skill_get_maxcount(skill,lv);
		int mob_class = (skill==AM_CANNIBALIZE)? summons[lv-1] :1142;
		if(battle_config.land_skill_limit && maxcount>0 && (battle_config.land_skill_limit&BL_PC)) {
			i = map_foreachinmap(skill_check_condition_mob_master_sub ,sd->bl.m, BL_MOB, sd->bl.id, mob_class, skill, &c);
			if(c >= maxcount ||
				(skill==AM_CANNIBALIZE && c != i && battle_config.summon_flora&2))
			{	//Fails when: exceed max limit. There are other plant types already out.
				clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
				return 0;
			}
		}
		break;
	}
	}

	status = &sd->battle_status;

	require = skill_get_requirement(sd,skill,lv);

	if( require.hp > 0 && status->hp <= (unsigned int)require.hp) {
		clif_skill_fail(sd,skill,USESKILL_FAIL_HP_INSUFFICIENT,0);
		return 0;
	}

	if( require.ammo ) { //Skill requires stuff equipped in the arrow slot.
		if((i=sd->equip_index[EQI_AMMO]) < 0 ||
			!sd->inventory_data[i] ||
			sd->status.inventory[i].amount < require.ammo_qty
		) {
			clif_arrow_fail(sd,0);
			return 0;
		}
		if (!(require.ammo&1<<sd->inventory_data[i]->look))
		{	//Ammo type check. Send the "wrong weapon type" message
			//which is the closest we have to wrong ammo type. [Skotlex]
			clif_arrow_fail(sd,0); //Haplo suggested we just send the equip-arrows message instead. [Skotlex]
			//clif_skill_fail(sd,skill,USESKILL_FAIL_THIS_WEAPON,0);
			return 0;
		}
	}

	for( i = 0; i < MAX_SKILL_ITEM_REQUIRE; ++i )
	{
		if( !require.itemid[i] )
			continue;
		index[i] = pc_search_inventory(sd,require.itemid[i]);
		if( index[i] < 0 || sd->status.inventory[index[i]].amount < require.amount[i] ) {
			if( require.itemid[i] == ITEMID_RED_GEMSTONE )
				clif_skill_fail(sd,skill,USESKILL_FAIL_REDJAMSTONE,0);// red gemstone required
			else if( require.itemid[i] == ITEMID_BLUE_GEMSTONE )
				clif_skill_fail(sd,skill,USESKILL_FAIL_BLUEJAMSTONE,0);// blue gemstone required
			else
				clif_skill_fail(sd,skill,USESKILL_FAIL_LEVEL,0);
			return 0;
		}
	}

	return 1;
}

// type&2: consume items (after skill was used)
// type&1: consume the others (before skill was used)
int skill_consume_requirement( struct map_session_data *sd, short skill, short lv, short type)
{
	int n,i;
	struct skill_condition req;
	int rankFlag = 0;

	nullpo_ret(sd);
	if( map_allowed_woe(sd->bl.m) && sd->status.guild_id )
		rankFlag = 1;
	else if( map[sd->bl.m].flag.battleground && sd->bg_id )
		rankFlag = 2;

	req = skill_get_requirement(sd,skill,lv);

	if( type&1 )
	{
		if( skill == CG_TAROTCARD || sd->state.autocast )
			req.sp = 0; // TarotCard will consume sp in skill_cast_nodamage_id [Inkfish]
		if(req.hp || req.sp)
			status_zap(&sd->bl, req.hp, req.sp);
		if( req.sp )
		{
			if( rankFlag == 1 )
				add2limit(sd->status.wstats.sp_used, req.sp, UINT_MAX);
			else if( rankFlag == 2 )
				add2limit(sd->status.bgstats.sp_used, req.sp, UINT_MAX);
		}

		if(req.spiritball > 0)
		{
			pc_delspiritball(sd,req.spiritball,0);

			if( rankFlag == 1 )
				add2limit(sd->status.wstats.spiritb_used, req.spiritball, UINT_MAX);
			else if( rankFlag == 2 )
				add2limit(sd->status.bgstats.spiritb_used, req.spiritball, UINT_MAX);
		}

		if(req.zeny > 0 && skill != NJ_ZENYNAGE)
		{
			int j;
			if( battle_config.skill_zeny2item && (j = pc_search_inventory(sd,battle_config.skill_zeny2item)) >= 0 )
				pc_delitem(sd,j,1,0,0,LOG_TYPE_CONSUME);
			else
			{
				if( sd->status.zeny < req.zeny )
					req.zeny = sd->status.zeny;
				pc_payzeny(sd,req.zeny);
			}

			if( rankFlag == 1 )
				add2limit(sd->status.wstats.zeny_used, req.zeny, UINT_MAX);
			else if( rankFlag == 2 )
				add2limit(sd->status.bgstats.zeny_used, req.zeny, UINT_MAX);
			achievement_validate_zeny(sd,ATZ_USE_SKILL,req.zeny);
		}
	}

	if( type&2 )
	{
		struct status_change *sc = &sd->sc;

		if( !sc->count )
			sc = NULL;

		for( i = 0; i < MAX_SKILL_ITEM_REQUIRE; ++i )
		{
			if( !req.itemid[i] )
				continue;

			if( itemid_isgemstone(req.itemid[i]) && sc && sc->data[SC_SPIRIT] && sc->data[SC_SPIRIT]->val2 == SL_WIZARD )
				continue; //Gemstones are checked, but not substracted from inventory.

			if( (n = pc_search_inventory(sd,req.itemid[i])) >= 0 )
				pc_delitem(sd,n,req.amount[i],0,1,LOG_TYPE_CONSUME);

			if( rankFlag )
				switch( req.itemid[i] )
				{
				case ITEMID_POISONBOTTLE:
					if( rankFlag == 1 )
						add2limit(sd->status.wstats.poison_bottles, req.amount[i], UINT_MAX);
					else
						add2limit(sd->status.bgstats.poison_bottles, req.amount[i], UINT_MAX);
					break;
				case ITEMID_YELLOW_GEMSTONE:
					if( rankFlag == 1 )
						add2limit(sd->status.wstats.yellow_gemstones, req.amount[i], UINT_MAX);
					else
						add2limit(sd->status.bgstats.yellow_gemstones, req.amount[i], UINT_MAX);
					break;
				case ITEMID_RED_GEMSTONE:
					if( rankFlag == 1 )
						add2limit(sd->status.wstats.red_gemstones, req.amount[i], UINT_MAX);
					else
						add2limit(sd->status.bgstats.red_gemstones, req.amount[i], UINT_MAX);
					break;
				case ITEMID_BLUE_GEMSTONE:
					if( rankFlag == 1 )
						add2limit(sd->status.wstats.blue_gemstones, req.amount[i], UINT_MAX);
					else
						add2limit(sd->status.bgstats.blue_gemstones, req.amount[i], UINT_MAX);
					break;
				}

			achievement_validate_item(sd,AT_ITEM_CONSUME,req.itemid[i],req.amount[i]);
		}
	}

	return 1;
}

struct skill_condition skill_get_requirement(struct map_session_data* sd, short skill, short lv)
{
	struct skill_condition req;
	struct status_data *status;
	struct status_change *sc;
	int i,j,hp_rate,sp_rate;

	memset(&req,0,sizeof(req));

	if( !sd )
		return req;

	if( sd->skillitem == skill )
		return req; // Item skills and Hocus-Pocus don't have requirements.[Inkfish]

	sc = &sd->sc;
	if( !sc->count )
		sc = NULL;

	switch( skill )
	{ // Turn off check.
	case BS_MAXIMIZE:		case NV_TRICKDEAD:	case TF_HIDING:			case AS_CLOAKING:		case CR_AUTOGUARD:
	case ML_AUTOGUARD:		case CR_DEFENDER:	case ML_DEFENDER:		case ST_CHASEWALK:		case PA_GOSPEL:
	case CR_SHRINK:			case TK_RUN:		case GS_GATLINGFEVER:	case TK_READYCOUNTER:	case TK_READYDOWN:
	case TK_READYSTORM:		case TK_READYTURN:	case SG_FUSION:			case ALL_RIDING:
		if( sc && sc->data[status_skill2sc(skill)] )
			return req;
	}

	j = skill_get_index(skill);
	if( j == 0 ) // invalid skill id
  		return req;
	if( lv < 1 || lv > MAX_SKILL_LEVEL )
		return req;

	status = &sd->battle_status;

	req.hp = skill_db[j].hp[lv-1];
	hp_rate = skill_db[j].hp_rate[lv-1];
	if(hp_rate > 0)
		req.hp += (status->hp * hp_rate)/100;
	else
		req.hp += (status->max_hp * (-hp_rate))/100;

	req.sp = skill_db[j].sp[lv-1];
	if((sd->skillid_old == BD_ENCORE) && skill == sd->skillid_dance)
		req.sp /= 2;
	sp_rate = skill_db[j].sp_rate[lv-1];
	if(sp_rate > 0)
		req.sp += (status->sp * sp_rate)/100;
	else
		req.sp += (status->max_sp * (-sp_rate))/100;
	if( sd->dsprate!=100 )
		req.sp = req.sp * sd->dsprate / 100;

	req.zeny = skill_db[j].zeny[lv-1];

	req.spiritball = skill_db[j].spiritball[lv-1];

	req.state = skill_db[j].state;

	req.mhp = skill_db[j].mhp[lv-1];

	req.weapon = skill_db[j].weapon;

	req.ammo_qty = skill_db[j].ammo_qty[lv-1];
	if (req.ammo_qty)
		req.ammo = skill_db[j].ammo;

	if (!req.ammo && skill && skill_isammotype(sd, skill))
	{	//Assume this skill is using the weapon, therefore it requires arrows.
		req.ammo = 0xFFFFFFFF; //Enable use on all ammo types.
		req.ammo_qty = 1;
	}

	for( i = 0; i < MAX_SKILL_ITEM_REQUIRE; i++ )
	{
		if( (skill == AM_POTIONPITCHER || skill == CR_SLIMPITCHER || skill == CR_CULTIVATION) && i != lv%11 - 1 )
			continue;

		switch( skill )
		{
		case AM_CALLHOMUN:
			if (sd->status.hom_id) //Don't delete items when hom is already out.
				continue;
			break;
		case WZ_FIREPILLAR: // celest
			if (lv <= 5)	// no gems required at level 1-5
				continue;
			break;
		}

		req.itemid[i] = skill_db[j].itemid[i];
		req.amount[i] = skill_db[j].amount[i];

		if( itemid_isgemstone(req.itemid[i]) )
		{
			if( sd->special_state.no_gemstone )
			{	//Make it substract 1 gem rather than skipping the cost.
				//if( --req.amount[i] < 1 )
				//	req.itemid[i] = 0; //OFICIAL
				req.itemid[i] = 0;
				req.amount[i] = 0;
			}
			if(sc && sc->data[SC_INTOABYSS])
			{
				if( skill != SA_ABRACADABRA )
					req.itemid[i] = req.amount[i] = 0;
				else if( --req.amount[i] < 1 )
					req.amount[i] = 1; // Hocus Pocus allways use at least 1 gem
			}
		}
	}

	// Check for cost reductions due to skills & SCs
	switch(skill) {
		case MC_MAMMONITE:
			if(pc_checkskill(sd,BS_UNFAIRLYTRICK)>0)
				req.zeny -= req.zeny*10/100;
			break;
		case AL_HOLYLIGHT:
			if(sc && sc->data[SC_SPIRIT] && sc->data[SC_SPIRIT]->val2 == SL_PRIEST)
				req.sp *= 5;
			break;
		case SL_SMA:
		case SL_STUN:
		case SL_STIN:
		{
			int kaina_lv = pc_checkskill(sd,SL_KAINA);

			if(kaina_lv==0 || sd->status.base_level<70)
				break;
			if(sd->status.base_level>=90)
				req.sp -= req.sp*7*kaina_lv/100;
			else if(sd->status.base_level>=80)
				req.sp -= req.sp*5*kaina_lv/100;
			else if(sd->status.base_level>=70)
				req.sp -= req.sp*3*kaina_lv/100;
		}
			break;
		case MO_TRIPLEATTACK:
		case MO_CHAINCOMBO:
		case MO_COMBOFINISH:
		case CH_TIGERFIST:
		case CH_CHAINCRUSH:
			if(sc && sc->data[SC_SPIRIT] && sc->data[SC_SPIRIT]->val2 == SL_MONK)
				req.sp -= req.sp*25/100; //FIXME: Need real data. this is a custom value.
			break;
		case MO_BODYRELOCATION:
			if( sc && sc->data[SC_EXPLOSIONSPIRITS] )
				req.spiritball = 0;
			break;
		case MO_EXTREMITYFIST:
			if( sc )
			{
				if( sc->data[SC_BLADESTOP] )
					req.spiritball--;
				else if( sc->data[SC_COMBO] )
				{
					switch( sc->data[SC_COMBO]->val1 )
					{
						case MO_COMBOFINISH:
							req.spiritball = 4;
							break;
						case CH_TIGERFIST:
							req.spiritball = 3;
							break;
						case CH_CHAINCRUSH:	//It should consume whatever is left as long as it's at least 1.
							req.spiritball = sd->spiritball?sd->spiritball:1;
							break;
					}
				}
			}
			break;
	}
	
	return req;
}

/*==========================================
 * Does cast-time reductions based on dex, item bonuses and config setting
 *------------------------------------------*/
int skill_castfix (struct block_list *bl, int skill_id, int skill_lv)
{
	int time = skill_get_cast(skill_id, skill_lv);
	struct map_session_data *sd;

	nullpo_ret(bl);

	/** Keitenai SpeedHack Protection **/
	if(BL_CAST(BL_PC, bl) && battle_config.KEITENAI_DELAY_SYSTEM)
		keitenai_sk_delay(BL_CAST(BL_PC, bl),skill_id,skill_lv,0);

	sd = BL_CAST(BL_PC, bl);

	// calculate base cast time (reduced by dex)
	if( !(skill_get_castnodex(skill_id, skill_lv)&1) )
	{
		int scale = battle_config.castrate_dex_scale - status_get_dex(bl);
		if( scale > 0 )	// not instant cast
			time = time * scale / battle_config.castrate_dex_scale;
		else return 0;	// instant cast
	}

	// calculate cast time reduced by item/card bonuses
	if( !(skill_get_castnodex(skill_id, skill_lv)&4) && sd )
	{
		int i;
		if( sd->castrate != 100 )
			time = time * sd->castrate / 100;
		for( i = 0; i < ARRAYLENGTH(sd->skillcast) && sd->skillcast[i].id; i++ )
		{
			if( sd->skillcast[i].id == skill_id )
			{
				time+= time * sd->skillcast[i].val / 100;
				break;
			}
		}
	}

	// config cast time multiplier
	if (battle_config.cast_rate != 100)
		time = time * battle_config.cast_rate / 100;

	if( sd && sd->state.showcast )
	{
		char output[128];
		sprintf(output, "- Casted in [%d] ms. -", time);
		clif_disp_onlyself(sd, output, strlen(output));
		sd->state.showcast = 0;
	}

	/* keitenai */
	if (battle_config.KEITENAI_DELAY_SYSTEM){
		struct homun_data *keitenai = BL_CAST(BL_HOM,bl);
		if(keitenai) time = 200;
	}

	// return final cast time
	return (time > 0) ? time : 0;
}

/*==========================================
 * Does cast-time reductions based on sc data.
 *------------------------------------------*/
int skill_castfix_sc (struct block_list *bl, int time)
{
	struct status_change *sc = status_get_sc(bl);

	if (sc && sc->count) {
		if (sc->data[SC_SLOWCAST])
			time += time * sc->data[SC_SLOWCAST]->val2 / 100;
		if (sc->data[SC_SUFFRAGIUM]) {
			time -= time * sc->data[SC_SUFFRAGIUM]->val2 / 100;
			status_change_end(bl, SC_SUFFRAGIUM, INVALID_TIMER);
		}
		if (sc->data[SC_MEMORIZE]) {
			time>>=1;
			if ((--sc->data[SC_MEMORIZE]->val2) <= 0)
				status_change_end(bl, SC_MEMORIZE, INVALID_TIMER);
		}
		if (sc->data[SC_POEMBRAGI])
			time -= time * sc->data[SC_POEMBRAGI]->val2 / 100;
	}
	return (time > 0) ? time : 0;
}

/*==========================================
 * Does delay reductions based on dex/agi, sc data, item bonuses, ...
 *------------------------------------------*/
int skill_delayfix (struct block_list *bl, int skill_id, int skill_lv)
{
	int delaynodex = skill_get_delaynodex(skill_id, skill_lv);
	int time = skill_get_delay(skill_id, skill_lv);
	struct map_session_data *sd;
	struct status_change *sc = status_get_sc(bl);

	nullpo_ret(bl);
	sd = BL_CAST(BL_PC, bl);

	if (skill_id == SA_ABRACADABRA)
		return 0; //Will use picked skill's delay.

	if (bl->type&battle_config.no_skill_delay)
		return battle_config.min_skill_delay_limit;

	if (time < 0)
		time = -time + status_get_amotion(bl);	// If set to <0, add to attack motion.

	// Delay reductions
	switch (skill_id)
  	{	//Monk combo skills have their delay reduced by agi/dex.
	case MO_TRIPLEATTACK:
	case MO_CHAINCOMBO:
	case MO_COMBOFINISH:
	case CH_TIGERFIST:
	case CH_CHAINCRUSH:
		time -= 4*status_get_agi(bl) - 2*status_get_dex(bl);
		break;
	case HP_BASILICA:
		if( sc && !sc->data[SC_BASILICA] )
			time = 0; // There is no Delay on Basilica creation, only on cancel
		break;
	default:
		if (battle_config.delay_dependon_dex && !(delaynodex&1))
		{	// if skill delay is allowed to be reduced by dex
			int scale = battle_config.castrate_dex_scale - status_get_dex(bl);
			if (scale > 0)
				time = time * scale / battle_config.castrate_dex_scale;
			else //To be capped later to minimum.
				time = 0;
		}
		if (battle_config.delay_dependon_agi && !(delaynodex&1))
		{	// if skill delay is allowed to be reduced by agi
			int scale = battle_config.castrate_dex_scale - status_get_agi(bl);
			if (scale > 0)
				time = time * scale / battle_config.castrate_dex_scale;
			else //To be capped later to minimum.
				time = 0;
		}
	}

	if ( sc && sc->data[SC_SPIRIT] )
	{
		switch (skill_id) {
			case CR_SHIELDBOOMERANG:
				if (sc->data[SC_SPIRIT]->val2 == SL_CRUSADER)
					time /= 2;
				break;
			case AS_SONICBLOW:
				if (!map_flag_gvg3(bl->m) && sc->data[SC_SPIRIT]->val2 == SL_ASSASIN)
					time /= 2;
				break;
		}
	}

	if (!(delaynodex&2))
	{
		if (sc && sc->count) {
			if (sc->data[SC_POEMBRAGI])
				time -= time * sc->data[SC_POEMBRAGI]->val3 / 100;
		}
	}

	if( !(delaynodex&4) && sd && sd->delayrate != 100 )
		time = time * sd->delayrate / 100;

	if (battle_config.delay_rate != 100)
		time = time * battle_config.delay_rate / 100;

	if (time < status_get_amotion(bl))
		time = status_get_amotion(bl); // Delay can never be below amotion [Playtester]

	if( sd && sd->state.showcastdelay )
	{
		char output[128];
		sprintf(output, "Calculated delay is [ %d ] milliseconds for level %d %s...", max(time, battle_config.min_skill_delay_limit), skill_lv, skill_get_desc(skill_id));
		clif_disp_onlyself(sd, output, strlen(output));
		sd->state.showcastdelay = 0;
	}

	return max(time, battle_config.min_skill_delay_limit);
}

/*=========================================
 *
 *-----------------------------------------*/
struct square {
	int val1[5];
	int val2[5];
};

static void skill_brandishspear_first (struct square *tc, int dir, int x, int y)
{
	nullpo_retv(tc);

	if(dir == 0){
		tc->val1[0]=x-2;
		tc->val1[1]=x-1;
		tc->val1[2]=x;
		tc->val1[3]=x+1;
		tc->val1[4]=x+2;
		tc->val2[0]=
		tc->val2[1]=
		tc->val2[2]=
		tc->val2[3]=
		tc->val2[4]=y-1;
	}
	else if(dir==2){
		tc->val1[0]=
		tc->val1[1]=
		tc->val1[2]=
		tc->val1[3]=
		tc->val1[4]=x+1;
		tc->val2[0]=y+2;
		tc->val2[1]=y+1;
		tc->val2[2]=y;
		tc->val2[3]=y-1;
		tc->val2[4]=y-2;
	}
	else if(dir==4){
		tc->val1[0]=x-2;
		tc->val1[1]=x-1;
		tc->val1[2]=x;
		tc->val1[3]=x+1;
		tc->val1[4]=x+2;
		tc->val2[0]=
		tc->val2[1]=
		tc->val2[2]=
		tc->val2[3]=
		tc->val2[4]=y+1;
	}
	else if(dir==6){
		tc->val1[0]=
		tc->val1[1]=
		tc->val1[2]=
		tc->val1[3]=
		tc->val1[4]=x-1;
		tc->val2[0]=y+2;
		tc->val2[1]=y+1;
		tc->val2[2]=y;
		tc->val2[3]=y-1;
		tc->val2[4]=y-2;
	}
	else if(dir==1){
		tc->val1[0]=x-1;
		tc->val1[1]=x;
		tc->val1[2]=x+1;
		tc->val1[3]=x+2;
		tc->val1[4]=x+3;
		tc->val2[0]=y-4;
		tc->val2[1]=y-3;
		tc->val2[2]=y-1;
		tc->val2[3]=y;
		tc->val2[4]=y+1;
	}
	else if(dir==3){
		tc->val1[0]=x+3;
		tc->val1[1]=x+2;
		tc->val1[2]=x+1;
		tc->val1[3]=x;
		tc->val1[4]=x-1;
		tc->val2[0]=y-1;
		tc->val2[1]=y;
		tc->val2[2]=y+1;
		tc->val2[3]=y+2;
		tc->val2[4]=y+3;
	}
	else if(dir==5){
		tc->val1[0]=x+1;
		tc->val1[1]=x;
		tc->val1[2]=x-1;
		tc->val1[3]=x-2;
		tc->val1[4]=x-3;
		tc->val2[0]=y+3;
		tc->val2[1]=y+2;
		tc->val2[2]=y+1;
		tc->val2[3]=y;
		tc->val2[4]=y-1;
	}
	else if(dir==7){
		tc->val1[0]=x-3;
		tc->val1[1]=x-2;
		tc->val1[2]=x-1;
		tc->val1[3]=x;
		tc->val1[4]=x+1;
		tc->val2[1]=y;
		tc->val2[0]=y+1;
		tc->val2[2]=y-1;
		tc->val2[3]=y-2;
		tc->val2[4]=y-3;
	}

}

static void skill_brandishspear_dir (struct square* tc, int dir, int are)
{
	int c;
	nullpo_retv(tc);

	for( c = 0; c < 5; c++ )
	{
		switch( dir )
		{
			case 0:                   tc->val2[c]+=are; break;
			case 1: tc->val1[c]-=are; tc->val2[c]+=are; break;
			case 2: tc->val1[c]-=are;                   break;
			case 3: tc->val1[c]-=are; tc->val2[c]-=are; break;
			case 4:                   tc->val2[c]-=are; break;
			case 5: tc->val1[c]+=are; tc->val2[c]-=are; break;
			case 6: tc->val1[c]+=are;                   break;
			case 7: tc->val1[c]+=are; tc->val2[c]+=are; break;
		}
	}
}

void skill_brandishspear(struct block_list* src, struct block_list* bl, int skillid, int skilllv, unsigned int tick, int flag)
{
	int c,n=4;
	int dir = map_calc_dir(src,bl->x,bl->y);
	struct square tc;
	int x=bl->x,y=bl->y;
	skill_brandishspear_first(&tc,dir,x,y);
	skill_brandishspear_dir(&tc,dir,4);
	skill_area_temp[1] = bl->id;

	if(skilllv > 9){
		for(c=1;c<4;c++){
			map_foreachincell(skill_area_sub,
				bl->m,tc.val1[c],tc.val2[c],BL_CHAR,
				src,skillid,skilllv,tick, flag|BCT_ENEMY|n,
				skill_castend_damage_id);
		}
	}
	if(skilllv > 6){
		skill_brandishspear_dir(&tc,dir,-1);
		n--;
	}else{
		skill_brandishspear_dir(&tc,dir,-2);
		n-=2;
	}

	if(skilllv > 3){
		for(c=0;c<5;c++){
			map_foreachincell(skill_area_sub,
				bl->m,tc.val1[c],tc.val2[c],BL_CHAR,
				src,skillid,skilllv,tick, flag|BCT_ENEMY|n,
				skill_castend_damage_id);
			if(skilllv > 6 && n==3 && c==4){
				skill_brandishspear_dir(&tc,dir,-1);
				n--;c=-1;
			}
		}
	}
	for(c=0;c<10;c++){
		if(c==0||c==5) skill_brandishspear_dir(&tc,dir,-1);
		map_foreachincell(skill_area_sub,
			bl->m,tc.val1[c%5],tc.val2[c%5],BL_CHAR,
			src,skillid,skilllv,tick, flag|BCT_ENEMY|1,
			skill_castend_damage_id);
	}
}

/*==========================================
 * Weapon Repair [Celest/DracoRPG]
 *------------------------------------------*/
void skill_repairweapon (struct map_session_data *sd, int idx)
{
	int material;
	int materials[4] = { 1002, 998, 999, 756 };
	struct item *item;
	struct map_session_data *target_sd;

	nullpo_retv(sd);
	target_sd = map_id2sd(sd->menuskill_val);
	if (!target_sd) //Failed....
		return;
	if(idx==0xFFFF) // No item selected ('Cancel' clicked)
		return;
	if(idx < 0 || idx >= MAX_INVENTORY)
		return; //Invalid index??

   item = &target_sd->status.inventory[idx];
	if(item->nameid <= 0 || item->attribute == 0)
		return; //Again invalid item....

	if(sd!=target_sd && !battle_check_range(&sd->bl,&target_sd->bl,skill_get_range2(&sd->bl, sd->menuskill_id,pc_checkskill(sd, sd->menuskill_id)))){
		clif_item_repaireffect(sd,idx,1);
		return;
	}

	if (itemdb_type(item->nameid)==IT_WEAPON)
		material = materials [itemdb_wlv(item->nameid)-1]; // Lv1/2/3/4 weapons consume 1 Iron Ore/Iron/Steel/Rough Oridecon
	else
		material = materials [2]; // Armors consume 1 Steel
	if (pc_search_inventory(sd,material) < 0 ) {
		clif_skill_fail(sd,sd->menuskill_id,USESKILL_FAIL_LEVEL,0);
		return;
	}
	clif_skill_nodamage(&sd->bl,&target_sd->bl,sd->menuskill_id,1,1);
	item->attribute=0;
	clif_equiplist(target_sd);
	pc_delitem(sd,pc_search_inventory(sd,material),1,0,0,LOG_TYPE_CONSUME);
	clif_item_repaireffect(sd,idx,0);
	if(sd!=target_sd)
		clif_item_repaireffect(target_sd,idx,0);
}

/*==========================================
 * Item Appraisal
 *------------------------------------------*/
void skill_identify (struct map_session_data *sd, int idx)
{
	int flag=1;

	nullpo_retv(sd);

	if(idx >= 0 && idx < MAX_INVENTORY) {
		if(sd->status.inventory[idx].nameid > 0 && sd->status.inventory[idx].identify == 0 ){
			flag=0;
			sd->status.inventory[idx].identify=1;
		}
	}
	clif_item_identified(sd,idx,flag);
}

/*==========================================
 * Weapon Refine [Celest]
 *------------------------------------------*/
void skill_weaponrefine (struct map_session_data *sd, int idx)
{
	int i = 0, ep = 0, per;
	int material[5] = { 0, 1010, 1011, 984, 984 };
	struct item *item;

	nullpo_retv(sd);

	if (idx >= 0 && idx < MAX_INVENTORY)
	{
		struct item_data *ditem = sd->inventory_data[idx];
		item = &sd->status.inventory[idx];

		if(item->nameid > 0 && ditem->type == IT_WEAPON)
		{
			if( item->refine >= sd->menuskill_val
			||  item->refine >= MAX_REFINE		// if it's no longer refineable
			||  ditem->flag.no_refine 	// if the item isn't refinable
			||  (i = pc_search_inventory(sd, material [ditem->wlv])) < 0 )
			{
				clif_skill_fail(sd,sd->menuskill_id,USESKILL_FAIL_LEVEL,0);
				return;
			}

			per = percentrefinery [ditem->wlv][(int)item->refine];
			per += (((signed int)sd->status.job_level)-50)/2; //Updated per the new kro descriptions. [Skotlex]

			pc_delitem(sd, i, 1, 0, 0, LOG_TYPE_OTHER);
			if (per > rand() % 100) {
				item->refine++;
				if(item->equip) {
					ep = item->equip;
					pc_unequipitem(sd,idx,3);
				}
				clif_refine(sd->fd,0,idx,item->refine);
				clif_delitem(sd,idx,1,3);
				clif_additem(sd,idx,1,0);
				if (ep)
					pc_equipitem(sd,idx,ep);
				clif_misceffect(&sd->bl,3);
				if(item->refine == MAX_REFINE &&
					item->card[0] == CARD0_FORGE &&
					(int)MakeDWord(item->card[2],item->card[3]) == sd->status.char_id)
				{ // Fame point system [DracoRPG]
					switch(ditem->wlv){
						case 1:
							pc_addfame(sd,1,0); // Success to refine to +10 a lv1 weapon you forged = +1 fame point
							break;
						case 2:
							pc_addfame(sd,25,0); // Success to refine to +10 a lv2 weapon you forged = +25 fame point
							break;
						case 3:
							pc_addfame(sd,1000,0); // Success to refine to +10 a lv3 weapon you forged = +1000 fame point
							break;
					}
				}
			} else {
				item->refine = 0;
				if(item->equip)
					pc_unequipitem(sd,idx,3);
				clif_refine(sd->fd,1,idx,item->refine);
				pc_delitem(sd,idx,1,0,2,LOG_TYPE_OTHER);
				clif_misceffect(&sd->bl,2);
				clif_emotion(&sd->bl, E_OMG);
			}
		}
	}
}

/*==========================================
 *
 *------------------------------------------*/
int skill_autospell (struct map_session_data *sd, int skillid)
{
	int skilllv;
	int maxlv=1,lv;

	nullpo_ret(sd);

	skilllv = sd->menuskill_val;
	lv=pc_checkskill(sd,skillid);

	if(skilllv <= 0 || !lv) return 0; // Player must learn the skill before doing auto-spell [Lance]

	if(skillid==MG_NAPALMBEAT)	maxlv=3;
	else if(skillid==MG_COLDBOLT || skillid==MG_FIREBOLT || skillid==MG_LIGHTNINGBOLT){
		if (sd->sc.data[SC_SPIRIT] && sd->sc.data[SC_SPIRIT]->val2 == SL_SAGE)
			maxlv =10; //Soul Linker bonus. [Skotlex]
		else if(skilllv==2) maxlv=1;
		else if(skilllv==3) maxlv=2;
		else if(skilllv>=4) maxlv=3;
	}
	else if(skillid==MG_SOULSTRIKE){
		if(skilllv==5) maxlv=1;
		else if(skilllv==6) maxlv=2;
		else if(skilllv>=7) maxlv=3;
	}
	else if(skillid==MG_FIREBALL){
		if(skilllv==8) maxlv=1;
		else if(skilllv>=9) maxlv=2;
	}
	else if(skillid==MG_FROSTDIVER) maxlv=1;
	else return 0;

	if(maxlv > lv)
		maxlv = lv;

	sc_start4(&sd->bl,SC_AUTOSPELL,100,skilllv,skillid,maxlv,0,
		skill_get_time(SA_AUTOSPELL,skilllv));
	return 0;
}

/*==========================================
 * Sitting skills functions.
 *------------------------------------------*/
static int skill_sit_count (struct block_list *bl, va_list ap)
{
	struct map_session_data *sd;
	int type =va_arg(ap,int);
	sd=(struct map_session_data*)bl;

	if(!pc_issit(sd))
		return 0;

	if(type&1 && pc_checkskill(sd,RG_GANGSTER) > 0)
		return 1;

	if(type&2 && (pc_checkskill(sd,TK_HPTIME) > 0 || pc_checkskill(sd,TK_SPTIME) > 0))
		return 1;

	return 0;
}

static int skill_sit_in (struct block_list *bl, va_list ap)
{
	struct map_session_data *sd;
	int type =va_arg(ap,int);

	sd=(struct map_session_data*)bl;

	if(!pc_issit(sd))
		return 0;

	if(type&1 && pc_checkskill(sd,RG_GANGSTER) > 0)
		sd->state.gangsterparadise=1;

	if(type&2 && (pc_checkskill(sd,TK_HPTIME) > 0 || pc_checkskill(sd,TK_SPTIME) > 0 ))
	{
		sd->state.rest=1;
		status_calc_regen(bl, &sd->battle_status, &sd->regen);
		status_calc_regen_rate(bl, &sd->regen, &sd->sc);
	}

	return 0;
}

static int skill_sit_out (struct block_list *bl, va_list ap)
{
	struct map_session_data *sd;
	int type =va_arg(ap,int);
	sd=(struct map_session_data*)bl;
	if(sd->state.gangsterparadise && type&1)
		sd->state.gangsterparadise=0;
	if(sd->state.rest && type&2) {
		sd->state.rest=0;
		status_calc_regen(bl, &sd->battle_status, &sd->regen);
		status_calc_regen_rate(bl, &sd->regen, &sd->sc);
	}
	return 0;
}

int skill_sit (struct map_session_data *sd, int type)
{
	int flag = 0;
	int range = 0, lv;
	nullpo_ret(sd);


	if((lv = pc_checkskill(sd,RG_GANGSTER)) > 0) {
		flag|=1;
		range = skill_get_splash(RG_GANGSTER, lv);
	}
	if((lv = pc_checkskill(sd,TK_HPTIME)) > 0) {
		flag|=2;
		range = skill_get_splash(TK_HPTIME, lv);
	}
	else if ((lv = pc_checkskill(sd,TK_SPTIME)) > 0) {
		flag|=2;
		range = skill_get_splash(TK_SPTIME, lv);
	}

	if (!flag) return 0;

	if(type) {
		if (map_foreachinrange(skill_sit_count,&sd->bl, range, BL_PC, flag) > 1)
			map_foreachinrange(skill_sit_in,&sd->bl, range, BL_PC, flag);
	} else {
		if (map_foreachinrange(skill_sit_count,&sd->bl, range, BL_PC, flag) < 2)
			map_foreachinrange(skill_sit_out,&sd->bl, range, BL_PC, flag);
	}
	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_frostjoke_scream (struct block_list *bl, va_list ap)
{
	struct block_list *src;
	int skillnum,skilllv;
	unsigned int tick;

	nullpo_ret(bl);
	nullpo_ret(src=va_arg(ap,struct block_list*));

	skillnum=va_arg(ap,int);
	skilllv=va_arg(ap,int);
	if(skilllv <= 0) return 0;
	tick=va_arg(ap,unsigned int);

	if (src == bl || status_isdead(bl))
		return 0;
	if (bl->type == BL_PC) {
		struct map_session_data *sd = (struct map_session_data *)bl;
		if (sd && sd->sc.option&OPTION_INVISIBLE)
			return 0;
	}
	//It has been reported that Scream/Joke works the same regardless of woe-setting. [Skotlex]
	if(battle_check_target(src,bl,BCT_ENEMY) > 0)
		skill_additional_effect(src,bl,skillnum,skilllv,BF_MISC,ATK_DEF,tick);
	else if(battle_check_target(src,bl,BCT_PARTY) > 0 && rand()%100 < 10)
		skill_additional_effect(src,bl,skillnum,skilllv,BF_MISC,ATK_DEF,tick);

	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
static void skill_unitsetmapcell (struct skill_unit *src, int skill_num, int skill_lv, cell_t cell, bool flag)
{
	int range = skill_get_unit_range(skill_num,skill_lv);
	int x,y;

	for( y = src->bl.y - range; y <= src->bl.y + range; ++y )
		for( x = src->bl.x - range; x <= src->bl.x + range; ++x )
			map_setcell(src->bl.m, x, y, cell, flag);
}

/*==========================================
 *
 *------------------------------------------*/
int skill_attack_area (struct block_list *bl, va_list ap)
{
	struct block_list *src,*dsrc;
	int atk_type,skillid,skilllv,flag,type;
	unsigned int tick;

	if(status_isdead(bl))
		return 0;

	atk_type = va_arg(ap,int);
	src=va_arg(ap,struct block_list*);
	dsrc=va_arg(ap,struct block_list*);
	skillid=va_arg(ap,int);
	skilllv=va_arg(ap,int);
	tick=va_arg(ap,unsigned int);
	flag=va_arg(ap,int);
	type=va_arg(ap,int);


	if (skill_area_temp[1] == bl->id) //This is the target of the skill, do a full attack and skip target checks.
		return skill_attack(atk_type,src,dsrc,bl,skillid,skilllv,tick,flag);

	if(battle_check_target(dsrc,bl,type) <= 0 ||
		!status_check_skilluse(NULL, bl, skillid, skilllv, 2))
		return 0;


	switch (skillid) {
	case WZ_FROSTNOVA: //Skills that don't require the animation to be removed
	case NPC_ACIDBREATH:
	case NPC_DARKNESSBREATH:
	case NPC_FIREBREATH:
	case NPC_ICEBREATH:
	case NPC_THUNDERBREATH:
		return skill_attack(atk_type,src,dsrc,bl,skillid,skilllv,tick,flag);
	default:
		//Area-splash, disable skill animation.
		return skill_attack(atk_type,src,dsrc,bl,skillid,skilllv,tick,flag|SD_ANIMATION);
	}
}
/*==========================================
 *
 *------------------------------------------*/
int skill_clear_group (struct block_list *bl, int flag)
{
	struct unit_data *ud = unit_bl2ud(bl);
	struct skill_unit_group *group[MAX_SKILLUNITGROUP];
	int i, count=0;

	nullpo_ret(bl);
	if (!ud) return 0;

	//All groups to be deleted are first stored on an array since the array elements shift around when you delete them. [Skotlex]
	for (i=0;i<MAX_SKILLUNITGROUP && ud->skillunit[i];i++)
	{
		switch (ud->skillunit[i]->skill_id) {
			case SA_DELUGE:
			case SA_VOLCANO:
			case SA_VIOLENTGALE:
			case SA_LANDPROTECTOR:
			case NJ_SUITON:
			case NJ_KAENSIN:
				if (flag&1)
					group[count++]= ud->skillunit[i];
				break;
			default:
				if (flag&2 && skill_get_inf2(ud->skillunit[i]->skill_id)&INF2_TRAP)
					group[count++]= ud->skillunit[i];
				break;
		}

	}
	for (i=0;i<count;i++)
		skill_delunitgroup(group[i]);
	return count;
}

/*==========================================
 * Returns the first element field found [Skotlex]
 *------------------------------------------*/
struct skill_unit_group *skill_locate_element_field(struct block_list *bl)
{
	struct unit_data *ud = unit_bl2ud(bl);
	int i;
	nullpo_ret(bl);
	if (!ud) return NULL;

	for (i=0;i<MAX_SKILLUNITGROUP && ud->skillunit[i];i++) {
		switch (ud->skillunit[i]->skill_id) {
			case SA_DELUGE:
			case SA_VOLCANO:
			case SA_VIOLENTGALE:
			case SA_LANDPROTECTOR:
			case NJ_SUITON:
				return ud->skillunit[i];
		}
	}
	return NULL;
}

// for graffiti cleaner [Valaris]
int skill_graffitiremover (struct block_list *bl, va_list ap)
{
	struct skill_unit *unit=NULL;

	nullpo_ret(bl);
	nullpo_ret(ap);

	if(bl->type!=BL_SKILL || (unit=(struct skill_unit *)bl) == NULL)
		return 0;

	if((unit->group) && (unit->group->unit_id == UNT_GRAFFITI))
		skill_delunit(unit);

	return 0;
}

int skill_greed (struct block_list *bl, va_list ap)
{
	struct block_list *src;
	struct map_session_data *sd=NULL;
	struct flooritem_data *fitem=NULL;

	nullpo_ret(bl);
	nullpo_ret(src = va_arg(ap, struct block_list *));

	if(src->type == BL_PC && (sd = (struct map_session_data *)src) && bl->type == BL_ITEM && (fitem = (struct flooritem_data *)bl) && !fitem->no_bsgreed)
		pc_takeitem(sd, fitem);

	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
static int skill_cell_overlap(struct block_list *bl, va_list ap)
{
	int skillid;
	int *alive;
	struct skill_unit *unit;
	struct block_list *src;

	skillid = va_arg(ap,int);
	alive = va_arg(ap,int *);
	src = va_arg(ap,struct block_list *);
	unit = (struct skill_unit *)bl;

	if (unit == NULL || unit->group == NULL || (*alive) == 0)
		return 0;

	switch (skillid)
	{
		case SA_LANDPROTECTOR:
			if( unit->group->skill_id == SA_LANDPROTECTOR )
			{	//Check for offensive Land Protector to delete both. [Skotlex]
				(*alive) = 0;
				skill_delunit(unit);
				return 1;
			}
			if( !(skill_get_inf2(unit->group->skill_id)&(INF2_SONG_DANCE|INF2_TRAP)) )
			{	//It deletes everything except songs/dances and traps
				skill_delunit(unit);
				return 1;
			}
			break;
		case HW_GANBANTEIN:
			if( !(unit->group->state.song_dance&0x1) )
			{// Don't touch song/dance.
				skill_delunit(unit);
				return 1;
			}
			break;
		case SA_VOLCANO:
		case SA_DELUGE:
		case SA_VIOLENTGALE:
// The official implementation makes them fail to appear when casted on top of ANYTHING
// but I wonder if they didn't actually meant to fail when casted on top of each other?
// hence, I leave the alternate implementation here, commented. [Skotlex]
			if (unit->range <= 0)
			{
				(*alive) = 0;
				return 1;
			}
/*
			switch (unit->group->skill_id)
			{	//These cannot override each other.
				case SA_VOLCANO:
				case SA_DELUGE:
				case SA_VIOLENTGALE:
					(*alive) = 0;
					return 1;
			}
*/
			break;
		case PF_FOGWALL:
			switch(unit->group->skill_id)
			{
				case SA_VOLCANO: //Can't be placed on top of these
				case SA_VIOLENTGALE:
					(*alive) = 0;
					return 1;
				case SA_DELUGE:
				case NJ_SUITON:
				//Cheap 'hack' to notify the calling function that duration should be doubled [Skotlex]
					(*alive) = 2;
					break;
			}
			break;
		case HP_BASILICA:
			if (unit->group->skill_id == HP_BASILICA)
			{	//Basilica can't be placed on top of itself to avoid map-cell stacking problems. [Skotlex]
				(*alive) = 0;
				return 1;
			}
			break;
	}

	if (unit->group->skill_id == SA_LANDPROTECTOR &&
		!(skill_get_inf2(skillid)&(INF2_SONG_DANCE|INF2_TRAP)))
	{	//It deletes everything except songs/dances/traps
		(*alive) = 0;
		return 1;
	}

	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_chastle_mob_changetarget(struct block_list *bl,va_list ap)
{
	struct mob_data* md;
	struct unit_data*ud = unit_bl2ud(bl);
	struct block_list *from_bl;
	struct block_list *to_bl;
	md = (struct mob_data*)bl;
	from_bl = va_arg(ap,struct block_list *);
	to_bl = va_arg(ap,struct block_list *);

	if(ud && ud->target == from_bl->id)
		ud->target = to_bl->id;

	if(md->bl.type == BL_MOB && md->target_id == from_bl->id)
		md->target_id = to_bl->id;
	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
static int skill_trap_splash (struct block_list *bl, va_list ap)
{
	struct block_list *src;
	int tick;
	struct skill_unit *unit;
	struct skill_unit_group *sg;
	struct block_list *ss;
	src = va_arg(ap,struct block_list *);
	unit = (struct skill_unit *)src;
	tick = va_arg(ap,int);

	if( !unit->alive || bl->prev == NULL )
		return 0;

	nullpo_ret(sg = unit->group);
	nullpo_ret(ss = map_id2bl(sg->src_id));

	if(battle_check_target(src,bl,BCT_ENEMY) <= 0)
		return 0;

	switch(sg->unit_id){
		case UNT_SHOCKWAVE:
		case UNT_SANDMAN:
		case UNT_FLASHER:
			skill_additional_effect(ss,bl,sg->skill_id,sg->skill_lv,BF_MISC,ATK_DEF,tick);
			break;
		case UNT_GROUNDDRIFT_WIND:
			if(skill_attack(BF_WEAPON,ss,src,bl,sg->skill_id,sg->skill_lv,tick,sg->val1))
				sc_start(bl,SC_STUN,5,sg->skill_lv,skill_get_time2(sg->skill_id, sg->skill_lv));
			break;
		case UNT_GROUNDDRIFT_DARK:
			if(skill_attack(BF_WEAPON,ss,src,bl,sg->skill_id,sg->skill_lv,tick,sg->val1))
				sc_start(bl,SC_BLIND,5,sg->skill_lv,skill_get_time2(sg->skill_id, sg->skill_lv));
			break;
		case UNT_GROUNDDRIFT_POISON:
			if(skill_attack(BF_WEAPON,ss,src,bl,sg->skill_id,sg->skill_lv,tick,sg->val1))
				sc_start(bl,SC_POISON,5,sg->skill_lv,skill_get_time2(sg->skill_id, sg->skill_lv));
			break;
		case UNT_GROUNDDRIFT_WATER:
			if(skill_attack(BF_WEAPON,ss,src,bl,sg->skill_id,sg->skill_lv,tick,sg->val1))
				sc_start(bl,SC_FREEZE,5,sg->skill_lv,skill_get_time2(sg->skill_id, sg->skill_lv));
			break;
		case UNT_GROUNDDRIFT_FIRE:
			if(skill_attack(BF_WEAPON,ss,src,bl,sg->skill_id,sg->skill_lv,tick,sg->val1))
				skill_blown(src,bl,skill_get_blewcount(sg->skill_id,sg->skill_lv),-1,0);
			break;
		default:
			skill_attack(skill_get_type(sg->skill_id),ss,src,bl,sg->skill_id,sg->skill_lv,tick,0);
			break;
	}
	return 1;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_enchant_elemental_end (struct block_list *bl, int type)
{
	struct status_change *sc;
	const enum sc_type scs[] = { SC_ENCPOISON, SC_ASPERSIO, SC_FIREWEAPON, SC_WATERWEAPON, SC_WINDWEAPON, SC_EARTHWEAPON, SC_SHADOWWEAPON, SC_GHOSTWEAPON, SC_ENCHANTARMS };
	int i;
	nullpo_ret(bl);
	nullpo_ret(sc= status_get_sc(bl));

	if (!sc->count) return 0;

	for (i = 0; i < ARRAYLENGTH(scs); i++)
		if (type != scs[i] && sc->data[scs[i]])
			status_change_end(bl, scs[i], INVALID_TIMER);

	return 0;
}

bool skill_check_cloaking(struct block_list *bl, struct status_change_entry *sce)
{
	static int dx[] = { 0, 1, 0, -1, -1,  1, 1, -1};
	static int dy[] = {-1, 0, 1,  0, -1, -1, 1,  1};
	bool wall = true;
	int i;

	if( (bl->type == BL_PC && battle_config.pc_cloak_check_type&1)
	||	(bl->type != BL_PC && battle_config.monster_cloak_check_type&1) )
	{	//Check for walls.
		ARR_FIND( 0, 8, i, map_getcell(bl->m, bl->x+dx[i], bl->y+dy[i], CELL_CHKNOPASS) != 0 );
		if( i == 8 )
			wall = false;
	}

	if( sce )
	{
		if( !wall )
		{
			if( sce->val1 < 3 ) //End cloaking.
				status_change_end(bl, SC_CLOAKING, INVALID_TIMER);
			else
			if( sce->val4&1 )
			{	//Remove wall bonus
				sce->val4&=~1;
				status_calc_bl(bl,SCB_SPEED);
			}
		}
		else
		{
			if( !(sce->val4&1) )
			{	//Add wall speed bonus
				sce->val4|=1;
				status_calc_bl(bl,SCB_SPEED);
			}
		}
	}

	return wall;
}

/*==========================================
 *
 *------------------------------------------*/
struct skill_unit *skill_initunit (struct skill_unit_group *group, int idx, int x, int y, int val1, int val2)
{
	struct skill_unit *unit;

	nullpo_retr(NULL, group);
	nullpo_retr(NULL, group->unit); // crash-protection against poor coding
	nullpo_retr(NULL, unit=&group->unit[idx]);

	if(!unit->alive)
		group->alive_count++;

	unit->bl.id=map_get_new_object_id();
	unit->bl.type=BL_SKILL;
	unit->bl.m=group->map;
	unit->bl.x=x;
	unit->bl.y=y;
	unit->group=group;
	unit->alive=1;
	unit->val1=val1;
	unit->val2=val2;

	idb_put(skillunit_db, unit->bl.id, unit);
	map_addiddb(&unit->bl);
	map_addblock(&unit->bl);

	// perform oninit actions
	switch (group->skill_id) {
	case WZ_ICEWALL:
		map_setgatcell(unit->bl.m,unit->bl.x,unit->bl.y,5);
		clif_changemapcell(0,unit->bl.m,unit->bl.x,unit->bl.y,5,AREA);
		break;
	case SA_LANDPROTECTOR:
		skill_unitsetmapcell(unit,SA_LANDPROTECTOR,group->skill_lv,CELL_LANDPROTECTOR,true);
		break;
	default:
		if (group->state.song_dance&0x1) //Check for dissonance.
			skill_dance_overlap(unit, 1);
		break;
	}

	clif_skill_setunit(unit);

	return unit;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_delunit (struct skill_unit* unit)
{
	struct skill_unit_group *group;

	nullpo_ret(unit);
	if( !unit->alive )
		return 0;
	unit->alive=0;

	nullpo_ret(group=unit->group);

	if( group->state.song_dance&0x1 ) //Cancel dissonance effect.
		skill_dance_overlap(unit, 0);

	// invoke onout event
	if( !unit->range )
		map_foreachincell(skill_unit_effect,unit->bl.m,unit->bl.x,unit->bl.y,group->bl_flag,&unit->bl,gettick(),4);

	// perform ondelete actions
	switch (group->skill_id) {
	case HT_ANKLESNARE:
		{
		struct block_list* target = map_id2bl(group->val2);
		if( target )
			status_change_end(target, SC_ANKLE, INVALID_TIMER);
		}
		break;
	case WZ_ICEWALL:
		map_setgatcell(unit->bl.m,unit->bl.x,unit->bl.y,unit->val2);
		clif_changemapcell(0,unit->bl.m,unit->bl.x,unit->bl.y,unit->val2,ALL_SAMEMAP); // hack to avoid clientside cell bug
		break;
	case SA_LANDPROTECTOR:
		skill_unitsetmapcell(unit,SA_LANDPROTECTOR,group->skill_lv,CELL_LANDPROTECTOR,false);
		break;
	}

	clif_skill_delunit(unit);

	unit->group=NULL;
	map_delblock(&unit->bl); // don't free yet
	map_deliddb(&unit->bl);
	idb_remove(skillunit_db, unit->bl.id);
	if(--group->alive_count==0)
		skill_delunitgroup(group);

	return 0;
}
/*==========================================
 *
 *------------------------------------------*/
static DBMap* group_db = NULL;// int group_id -> struct skill_unit_group*

/// Returns the target skill_unit_group or NULL if not found.
struct skill_unit_group* skill_id2group(int group_id)
{
	return (struct skill_unit_group*)idb_get(group_db, group_id);
}


static int skill_unit_group_newid = MAX_SKILL_DB;

/// Returns a new group_id that isn't being used in group_db.
/// Fatal error if nothing is available.
static int skill_get_new_group_id(void)
{
	if( skill_unit_group_newid >= MAX_SKILL_DB && skill_id2group(skill_unit_group_newid) == NULL )
		return skill_unit_group_newid++;// available
	{// find next id
		int base_id = skill_unit_group_newid;
		while( base_id != ++skill_unit_group_newid )
		{
			if( skill_unit_group_newid < MAX_SKILL_DB )
				skill_unit_group_newid = MAX_SKILL_DB;
			if( skill_id2group(skill_unit_group_newid) == NULL )
				return skill_unit_group_newid++;// available
		}
		// full loop, nothing available
		ShowFatalError("skill_get_new_group_id: All ids are taken. Exiting...");
		exit(1);
	}
}

struct skill_unit_group* skill_initunitgroup (struct block_list* src, int count, short skillid, short skilllv, int unit_id, int limit, int interval)
{
	struct unit_data* ud = unit_bl2ud( src );
	struct skill_unit_group* group;
	int i;

	if(skillid <= 0 || skilllv <= 0) return 0;

	nullpo_retr(NULL, src);
	nullpo_retr(NULL, ud);

	// find a free spot to store the new unit group
	ARR_FIND( 0, MAX_SKILLUNITGROUP, i, ud->skillunit[i] == NULL );
	if(i == MAX_SKILLUNITGROUP)
	{
		// array is full, make room by discarding oldest group
		int j=0;
		unsigned maxdiff=0,x,tick=gettick();
		for(i=0;i<MAX_SKILLUNITGROUP && ud->skillunit[i];i++)
			if((x=DIFF_TICK(tick,ud->skillunit[i]->tick))>maxdiff){
				maxdiff=x;
				j=i;
			}
		skill_delunitgroup(ud->skillunit[j]);
		//Since elements must have shifted, we use the last slot.
		i = MAX_SKILLUNITGROUP-1;
	}

	group             = ers_alloc(skill_unit_ers, struct skill_unit_group);
	group->src_id     = src->id;
	group->party_id   = status_get_party_id(src);
	group->guild_id   = status_get_guild_id(src);
	group->bg_id      = bg_team_get_id(src);
	group->faction_id = faction_get_id(src);
	group->group_id   = skill_get_new_group_id();
	group->unit       = (struct skill_unit *)aCalloc(count,sizeof(struct skill_unit));
	group->unit_count = count;
	group->alive_count = 0;
	group->val1       = 0;
	group->val2       = 0;
	group->val3       = 0;
	group->skill_id   = skillid;
	group->skill_lv   = skilllv;
	group->unit_id    = unit_id;
	group->map        = src->m;
	group->limit      = limit;
	group->interval   = interval;
	group->tick       = gettick();
	group->valstr     = NULL;

	ud->skillunit[i] = group;

	if (skillid == PR_SANCTUARY) //Sanctuary starts healing +1500ms after casted. [Skotlex]
		group->tick += 1500;

	idb_put(group_db, group->group_id, group);
	return group;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_delunitgroup_(struct skill_unit_group *group, const char* file, int line, const char* func)
{
	struct block_list* src;
	struct unit_data *ud;
	struct status_change *sc;
	int i,j;

	if( group == NULL )
	{
		ShowDebug("skill_delunitgroup: group is NULL (source=%s:%d, %s)! Please report this! (#3504)\n", file, line, func);
		return 0;
	}

	src=map_id2bl(group->src_id);
	ud = unit_bl2ud(src);
	if(!src || !ud) {
		ShowError("skill_delunitgroup: Group's source not found! (src_id: %d skill_id: %d)\n", group->src_id, group->skill_id);
		return 0;
	}

	sc = status_get_sc(src);
	if (skill_get_unit_flag(group->skill_id)&(UF_DANCE|UF_SONG|UF_ENSEMBLE))
	{
		if (sc && sc->data[SC_DANCING])
		{
			sc->data[SC_DANCING]->val2 = 0 ; //This prevents status_change_end attempting to redelete the group. [Skotlex]
			status_change_end(src, SC_DANCING, INVALID_TIMER);
		}
	}

	// end Gospel's status change on 'src'
	// (needs to be done when the group is deleted by other means than skill deactivation)
	if( group->unit_id == UNT_GOSPEL && sc && sc->data[SC_GOSPEL] )
	{
		sc->data[SC_GOSPEL]->val3 = 0; //Remove reference to this group. [Skotlex]
		status_change_end(src, SC_GOSPEL, INVALID_TIMER);
	}

	if (group->skill_id == SG_SUN_WARM ||
		group->skill_id == SG_MOON_WARM ||
		group->skill_id == SG_STAR_WARM) {
		if(sc && sc->data[SC_WARM]) {
			sc->data[SC_WARM]->val4 = 0;
			status_change_end(src, SC_WARM, INVALID_TIMER);
		}
	}

	if (src->type==BL_PC && group->state.ammo_consume)
		battle_consume_ammo((TBL_PC*)src, group->skill_id, group->skill_lv);

	group->alive_count=0;

	// remove all unit cells
	if(group->unit != NULL)
		for( i = 0; i < group->unit_count; i++ )
			skill_delunit(&group->unit[i]);

	// clear Talkie-box string
	if( group->valstr != NULL )
	{
		aFree(group->valstr);
		group->valstr = NULL;
	}

	idb_remove(group_db, group->group_id);
	map_freeblock(&group->unit->bl); // schedules deallocation of whole array (HACK)
	group->unit=NULL;
	group->group_id=0;
	group->unit_count=0;

	// locate this group, swap with the last entry and delete it
	ARR_FIND( 0, MAX_SKILLUNITGROUP, i, ud->skillunit[i] == group );
	ARR_FIND( i, MAX_SKILLUNITGROUP, j, ud->skillunit[j] == NULL ); j--;
	if( i < MAX_SKILLUNITGROUP )
	{
		ud->skillunit[i] = ud->skillunit[j];
		ud->skillunit[j] = NULL;
		ers_free(skill_unit_ers, group);
	}
	else
		ShowError("skill_delunitgroup: Group not found! (src_id: %d skill_id: %d)\n", group->src_id, group->skill_id);

	return 1;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_clear_unitgroup (struct block_list *src)
{
	struct unit_data *ud = unit_bl2ud(src);

	nullpo_ret(ud);

	while (ud->skillunit[0])
		skill_delunitgroup(ud->skillunit[0]);

	return 1;
}

/*==========================================
 *
 *------------------------------------------*/
struct skill_unit_group_tickset *skill_unitgrouptickset_search (struct block_list *bl, struct skill_unit_group *group, int tick)
{
	int i,j=-1,k,s,id;
	struct unit_data *ud;
	struct skill_unit_group_tickset *set;

	nullpo_ret(bl);
	if (group->interval==-1)
		return NULL;

	ud = unit_bl2ud(bl);
	if (!ud) return NULL;

	set = ud->skillunittick;

	if (skill_get_unit_flag(group->skill_id)&UF_NOOVERLAP)
		id = s = group->skill_id;
	else
		id = s = group->group_id;

	for (i=0; i<MAX_SKILLUNITGROUPTICKSET; i++) {
		k = (i+s) % MAX_SKILLUNITGROUPTICKSET;
		if (set[k].id == id)
			return &set[k];
		else if (j==-1 && (DIFF_TICK(tick,set[k].tick)>0 || set[k].id==0))
			j=k;
	}

	if (j == -1) {
		ShowWarning ("skill_unitgrouptickset_search: tickset is full\n");
		j = id % MAX_SKILLUNITGROUPTICKSET;
	}

	set[j].id = id;
	set[j].tick = tick;
	return &set[j];
}

/*==========================================
 *
 *------------------------------------------*/
int skill_unit_timer_sub_onplace (struct block_list* bl, va_list ap)
{
	struct skill_unit* unit = va_arg(ap,struct skill_unit *);
	struct skill_unit_group* group = unit->group;
	unsigned int tick = va_arg(ap,unsigned int);

	if( !unit->alive || bl->prev == NULL )
		return 0;

	nullpo_ret(group);

	if( !(skill_get_inf2(group->skill_id)&(INF2_SONG_DANCE|INF2_TRAP)) && map_getcell(bl->m, bl->x, bl->y, CELL_CHKLANDPROTECTOR) )
		return 0; //AoE skills are ineffective. [Skotlex]

	if( battle_check_target(&unit->bl,bl,group->target_flag) <= 0 )
		return 0;

	skill_unit_onplace_timer(unit,bl,tick);

	return 1;
}

/*==========================================
 *
 *------------------------------------------*/
static int skill_unit_timer_sub (DBKey key, void* data, va_list ap)
{
	struct skill_unit* unit = (struct skill_unit*)data;
	struct skill_unit_group* group = unit->group;
	unsigned int tick = va_arg(ap,unsigned int);
  	bool dissonance;
	struct block_list* bl = &unit->bl;

	if( !unit->alive )
		return 0;

	nullpo_ret(group);

	// check for expiration
	if( (DIFF_TICK(tick,group->tick) >= group->limit || DIFF_TICK(tick,group->tick) >= unit->limit) )
	{// skill unit expired (inlined from skill_unit_onlimit())
		switch( group->unit_id )
		{
			case UNT_BLASTMINE:
			case UNT_GROUNDDRIFT_WIND:
			case UNT_GROUNDDRIFT_DARK:
			case UNT_GROUNDDRIFT_POISON:
			case UNT_GROUNDDRIFT_WATER:
			case UNT_GROUNDDRIFT_FIRE:
				group->unit_id = UNT_USED_TRAPS;
				//clif_changetraplook(bl, UNT_FIREPILLAR_ACTIVE);
				group->limit=DIFF_TICK(tick+1500,group->tick);
				unit->limit=DIFF_TICK(tick+1500,group->tick);
			break;

			case UNT_ANKLESNARE:
				if( group->val2 > 0 ) {
					// Used Trap don't returns back to item
					skill_delunit(unit);
					break;
				}
			case UNT_SKIDTRAP:
			case UNT_LANDMINE:
			case UNT_SHOCKWAVE:
			case UNT_SANDMAN:
			case UNT_FLASHER:
			case UNT_FREEZINGTRAP:
			case UNT_CLAYMORETRAP:
			case UNT_TALKIEBOX:
			{
				struct block_list* src;
				if( unit->val1 > 0 && (src = map_id2bl(group->src_id)) != NULL && src->type == BL_PC )
				{ // revert unit back into a trap
					struct item item_tmp;
					memset(&item_tmp,0,sizeof(item_tmp));
					item_tmp.nameid = ITEMID_TRAP;
					item_tmp.identify = 1;
					map_addflooritem(&item_tmp,1,bl->m,bl->x,bl->y,0,0,0,0,0);
				}
				skill_delunit(unit);
			}
			break;

			case UNT_WARP_ACTIVE:
				// warp portal opens (morph to a UNT_WARP_WAITING cell)
				group->unit_id = skill_get_unit_id(group->skill_id, 1); // UNT_WARP_WAITING
				clif_changelook(&unit->bl, LOOK_BASE, group->unit_id);
				// restart timers
				group->limit = skill_get_time(group->skill_id,group->skill_lv);
				unit->limit = skill_get_time(group->skill_id,group->skill_lv);
				// apply effect to all units standing on it
				map_foreachincell(skill_unit_effect,unit->bl.m,unit->bl.x,unit->bl.y,group->bl_flag,&unit->bl,gettick(),1);
			break;

			case UNT_CALLFAMILY:
			{
				struct map_session_data *sd = NULL;
				if(group->val1) {
		  			sd = map_charid2sd(group->val1);
					group->val1 = 0;
					if (sd && !map[sd->bl.m].flag.nowarp)
						pc_setpos(sd,map_id2index(unit->bl.m),unit->bl.x,unit->bl.y,CLR_TELEPORT);
				}
				if(group->val2) {
					sd = map_charid2sd(group->val2);
					group->val2 = 0;
					if (sd && !map[sd->bl.m].flag.nowarp)
						pc_setpos(sd,map_id2index(unit->bl.m),unit->bl.x,unit->bl.y,CLR_TELEPORT);
				}
				skill_delunit(unit);
			}
			break;

			default:
				skill_delunit(unit);
		}
	}
	else
	{// skill unit is still active
		switch( group->unit_id )
		{
			case UNT_ICEWALL:
				// icewall loses 50 hp every second
				unit->val1 -= SKILLUNITTIMER_INTERVAL/20; // trap's hp
				if( unit->val1 <= 0 && unit->limit + group->tick > tick + 700 )
					unit->limit = DIFF_TICK(tick+700,group->tick);
				break;
			case UNT_SKIDTRAP:
			case UNT_LANDMINE:
			case UNT_SHOCKWAVE:
			case UNT_SANDMAN:
			case UNT_FLASHER:
			case UNT_FREEZINGTRAP:
			case UNT_TALKIEBOX:
			case UNT_ANKLESNARE:
				if( unit->val1 <= 0 ) {
					if( group->unit_id == UNT_ANKLESNARE && group->val2 > 0 )
						skill_delunit(unit);
					else {
						group->unit_id = UNT_USED_TRAPS;
						group->limit = DIFF_TICK(tick, group->tick) + 1500;
					}
				}
				break;
		}
	}

	//Don't continue if unit or even group is expired and has been deleted.
	if( !group || !unit->alive )
		return 0;

	dissonance = skill_dance_switch(unit, 0);

	if( unit->range >= 0 && group->interval != -1 )
	{
		if( battle_config.skill_wall_check )
			map_foreachinshootrange(skill_unit_timer_sub_onplace, bl, unit->range, group->bl_flag, bl,tick);
		else
			map_foreachinrange(skill_unit_timer_sub_onplace, bl, unit->range, group->bl_flag, bl,tick);

		if(unit->range == -1) //Unit disabled, but it should not be deleted yet.
			group->unit_id = UNT_USED_TRAPS;

		if( group->unit_id == UNT_TATAMIGAESHI )
		{
			unit->range = -1; //Disable processed cell.
			if (--group->val1 <= 0) // number of live cells
	  		{	//All tiles were processed, disable skill.
				group->target_flag=BCT_NOONE;
				group->bl_flag= BL_NUL;
			}
		}
	}

  	if( dissonance ) skill_dance_switch(unit, 1);

	return 0;
}
/*==========================================
 * Executes on all skill units every SKILLUNITTIMER_INTERVAL miliseconds.
 *------------------------------------------*/
int skill_unit_timer(int tid, unsigned int tick, int id, intptr_t data)
{
	map_freeblock_lock();

	skillunit_db->foreach(skillunit_db, skill_unit_timer_sub, tick);

	map_freeblock_unlock();

	return 0;
}

static int skill_unit_temp[20];  // temporary storage for tracking skill unit skill ids as players move in/out of them
/*==========================================
 *
 *------------------------------------------*/
int skill_unit_move_sub (struct block_list* bl, va_list ap)
{
	struct skill_unit* unit = (struct skill_unit *)bl;
	struct skill_unit_group* group = unit->group;

	struct block_list* target = va_arg(ap,struct block_list*);
	unsigned int tick = va_arg(ap,unsigned int);
	int flag = va_arg(ap,int);

	bool dissonance;
	int skill_id;
	int i;

	nullpo_ret(group);

	if( !unit->alive || target->prev == NULL )
		return 0;

	if( unit->group->skill_id == PF_SPIDERWEB && flag&1 )
		return 0; // Fiberlock is never supposed to trigger on skill_unit_move. [Inkfish]

	dissonance = skill_dance_switch(unit, 0);

	//Necessary in case the group is deleted after calling on_place/on_out [Skotlex]
	skill_id = unit->group->skill_id;

	if( unit->group->interval != -1 && !(skill_get_unit_flag(skill_id)&UF_DUALMODE) )
	{	//Non-dualmode unit skills with a timer don't trigger when walking, so just return
		if( dissonance ) skill_dance_switch(unit, 1);
		return 0;
	}

	//Target-type check.
	if( !(group->bl_flag&target->type && battle_check_target(&unit->bl,target,group->target_flag) > 0) )
	{
		if( group->src_id == target->id && group->state.song_dance&0x2 )
		{	//Ensemble check to see if they went out/in of the area [Skotlex]
			if( flag&1 )
			{
				if( flag&2 )
				{	//Clear this skill id.
					ARR_FIND( 0, ARRAYLENGTH(skill_unit_temp), i, skill_unit_temp[i] == skill_id );
					if( i < ARRAYLENGTH(skill_unit_temp) )
						skill_unit_temp[i] = 0;
				}
			}
			else
			{
				if( flag&2 )
				{	//Store this skill id.
					ARR_FIND( 0, ARRAYLENGTH(skill_unit_temp), i, skill_unit_temp[i] == 0 );
					if( i < ARRAYLENGTH(skill_unit_temp) )
						skill_unit_temp[i] = skill_id;
					else
						ShowError("skill_unit_move_sub: Reached limit of unit objects per cell!\n");
				}

			}

			if( flag&4 )
				skill_unit_onleft(skill_id,target,tick);
		}

		if( dissonance ) skill_dance_switch(unit, 1);

		return 0;
	}
	else
	{
		if( flag&1 )
		{
			int result = skill_unit_onplace(unit,target,tick);
			if( flag&2 && result )
			{	//Clear skill ids we have stored in onout.
				ARR_FIND( 0, ARRAYLENGTH(skill_unit_temp), i, skill_unit_temp[i] == result );
				if( i < ARRAYLENGTH(skill_unit_temp) )
					skill_unit_temp[i] = 0;
			}
		}
		else
		{
			int result = skill_unit_onout(unit,target,tick);
			if( flag&2 && result )
			{	//Store this unit id.
				ARR_FIND( 0, ARRAYLENGTH(skill_unit_temp), i, skill_unit_temp[i] == 0 );
				if( i < ARRAYLENGTH(skill_unit_temp) )
					skill_unit_temp[i] = skill_id;
				else
					ShowError("skill_unit_move_sub: Reached limit of unit objects per cell!\n");
			}
		}

		//TODO: Normally, this is dangerous since the unit and group could be freed
		//inside the onout/onplace functions. Currently it is safe because we know song/dance
		//cells do not get deleted within them. [Skotlex]
		if( dissonance ) skill_dance_switch(unit, 1);

		if( flag&4 )
			skill_unit_onleft(skill_id,target,tick);

		return 1;
	}
}

/*==========================================
 * Invoked when a char has moved and unit cells must be invoked (onplace, onout, onleft)
 * Flag values:
 * flag&1: invoke skill_unit_onplace (otherwise invoke skill_unit_onout)
 * flag&2: this function is being invoked twice as a bl moves, store in memory the affected
 * units to figure out when they have left a group.
 * flag&4: Force a onleft event (triggered when the bl is killed, for example)
 *------------------------------------------*/
int skill_unit_move (struct block_list *bl, unsigned int tick, int flag)
{
	nullpo_ret(bl);

	if( bl->prev == NULL )
		return 0;

	if( flag&2 && !(flag&1) )
	{	//Onout, clear data
		memset(skill_unit_temp, 0, sizeof(skill_unit_temp));
	}

	map_foreachincell(skill_unit_move_sub,bl->m,bl->x,bl->y,BL_SKILL,bl,tick,flag);

	if( flag&2 && flag&1 )
	{	//Onplace, check any skill units you have left.
		int i;
		for( i = 0; i < ARRAYLENGTH(skill_unit_temp); i++ )
			if( skill_unit_temp[i] )
				skill_unit_onleft(skill_unit_temp[i], bl, tick);
	}

	return 0;
}

/*==========================================
* Keitenai Delay System [ keitenai ]
*------------------------------------------*/
int keitenai_sk_delay (struct map_session_data *sd, uint16 skill_id, uint16 skill_lv,uint16 flag){

	char message_skdelay[200];
	int m, k = 1000;
	int show = battle_config.WOE_K_SHOW_DELAY;
	m = sd->bl.m;

	switch (skill_id) {
		case SM_BASH:
			if(DIFF_TICK(sd->K_CHK_SM_BASH,gettick())> 0){
				if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SM_BASH,gettick())> show && (show != 0)){
					sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SM_BASH - gettick())/k);
					clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
				}
				sd->state.K_LOCK_SM_BASH = 1;
			} else if(sd && (map[m].flag.gvg_castle)){
					sd->K_CHK_SM_BASH = gettick() + battle_config.WOE_K_DELAY_SM_BASH;
				sd->state.K_LOCK_SM_BASH = 0;
		} else {
			sd->K_CHK_SM_BASH = gettick() + battle_config.K_DELAY_SM_BASH;
			sd->state.K_LOCK_SM_BASH = 0;
		}
		break;
	case SM_MAGNUM:
		if(DIFF_TICK(sd->K_CHK_SM_MAGNUM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SM_MAGNUM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SM_MAGNUM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SM_MAGNUM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SM_MAGNUM = gettick() + battle_config.WOE_K_DELAY_SM_MAGNUM;
				sd->state.K_LOCK_SM_MAGNUM = 0;
		} else {
			sd->K_CHK_SM_MAGNUM = gettick() + battle_config.K_DELAY_SM_MAGNUM;
			sd->state.K_LOCK_SM_MAGNUM = 0;
		}
		break;
	case MG_NAPALMBEAT:
		if(DIFF_TICK(sd->K_CHK_MG_NAPALMBEAT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_NAPALMBEAT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_NAPALMBEAT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_NAPALMBEAT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_NAPALMBEAT = gettick() + battle_config.WOE_K_DELAY_MG_NAPALMBEAT;
				sd->state.K_LOCK_MG_NAPALMBEAT = 0;
		} else {
			sd->K_CHK_MG_NAPALMBEAT = gettick() + battle_config.K_DELAY_MG_NAPALMBEAT;
			sd->state.K_LOCK_MG_NAPALMBEAT = 0;
		}
		break;
	case MG_SOULSTRIKE:
		if(DIFF_TICK(sd->K_CHK_MG_SOULSTRIKE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_SOULSTRIKE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_SOULSTRIKE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_SOULSTRIKE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_SOULSTRIKE = gettick() + battle_config.WOE_K_DELAY_MG_SOULSTRIKE;
				sd->state.K_LOCK_MG_SOULSTRIKE = 0;
		} else {
			sd->K_CHK_MG_SOULSTRIKE = gettick() + battle_config.K_DELAY_MG_SOULSTRIKE;
			sd->state.K_LOCK_MG_SOULSTRIKE = 0;
		}
		break;
	case MG_COLDBOLT:
		if(DIFF_TICK(sd->K_CHK_MG_COLDBOLT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_COLDBOLT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_COLDBOLT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_COLDBOLT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_COLDBOLT = gettick() + battle_config.WOE_K_DELAY_MG_COLDBOLT;
				sd->state.K_LOCK_MG_COLDBOLT = 0;
		} else {
			sd->K_CHK_MG_COLDBOLT = gettick() + battle_config.K_DELAY_MG_COLDBOLT;
			sd->state.K_LOCK_MG_COLDBOLT = 0;
		}
		break;
	case MG_FROSTDIVER:
		if(DIFF_TICK(sd->K_CHK_MG_FROSTDIVER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_FROSTDIVER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_FROSTDIVER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_FROSTDIVER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_FROSTDIVER = gettick() + battle_config.WOE_K_DELAY_MG_FROSTDIVER;
				sd->state.K_LOCK_MG_FROSTDIVER = 0;
		} else {
			sd->K_CHK_MG_FROSTDIVER = gettick() + battle_config.K_DELAY_MG_FROSTDIVER;
			sd->state.K_LOCK_MG_FROSTDIVER = 0;
		}
		break;
	case MG_STONECURSE:
		if(DIFF_TICK(sd->K_CHK_MG_STONECURSE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_STONECURSE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_STONECURSE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_STONECURSE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_STONECURSE = gettick() + battle_config.WOE_K_DELAY_MG_STONECURSE;
				sd->state.K_LOCK_MG_STONECURSE = 0;
		} else {
			sd->K_CHK_MG_STONECURSE = gettick() + battle_config.K_DELAY_MG_STONECURSE;
			sd->state.K_LOCK_MG_STONECURSE = 0;
		}
		break;
	case MG_FIREBALL:
		if(DIFF_TICK(sd->K_CHK_MG_FIREBALL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_FIREBALL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_FIREBALL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_FIREBALL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_FIREBALL = gettick() + battle_config.WOE_K_DELAY_MG_FIREBALL;
				sd->state.K_LOCK_MG_FIREBALL = 0;
		} else {
			sd->K_CHK_MG_FIREBALL = gettick() + battle_config.K_DELAY_MG_FIREBALL;
			sd->state.K_LOCK_MG_FIREBALL = 0;
		}
		break;
	case MG_FIREWALL:
		if(DIFF_TICK(sd->K_CHK_MG_FIREWALL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_FIREWALL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_FIREWALL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_FIREWALL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_FIREWALL = gettick() + battle_config.WOE_K_DELAY_MG_FIREWALL;
				sd->state.K_LOCK_MG_FIREWALL = 0;
		} else {
			sd->K_CHK_MG_FIREWALL = gettick() + battle_config.K_DELAY_MG_FIREWALL;
			sd->state.K_LOCK_MG_FIREWALL = 0;
		}
		break;
	case MG_FIREBOLT:
		if(DIFF_TICK(sd->K_CHK_MG_FIREBOLT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_FIREBOLT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_FIREBOLT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_FIREBOLT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_FIREBOLT = gettick() + battle_config.WOE_K_DELAY_MG_FIREBOLT;
				sd->state.K_LOCK_MG_FIREBOLT = 0;
		} else {
			sd->K_CHK_MG_FIREBOLT = gettick() + battle_config.K_DELAY_MG_FIREBOLT;
			sd->state.K_LOCK_MG_FIREBOLT = 0;
		}
		break;
	case MG_LIGHTNINGBOLT:
		if(DIFF_TICK(sd->K_CHK_MG_LIGHTNINGBOLT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_LIGHTNINGBOLT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_LIGHTNINGBOLT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_LIGHTNINGBOLT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_LIGHTNINGBOLT = gettick() + battle_config.WOE_K_DELAY_MG_LIGHTBOLT;
				sd->state.K_LOCK_MG_LIGHTNINGBOLT = 0;
		} else {
			sd->K_CHK_MG_LIGHTNINGBOLT = gettick() + battle_config.K_DELAY_MG_LIGHTNINGBOLT;
			sd->state.K_LOCK_MG_LIGHTNINGBOLT = 0;
		}
		break;
	case MG_THUNDERSTORM:
		if(DIFF_TICK(sd->K_CHK_MG_THUNDERSTORM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MG_THUNDERSTORM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MG_THUNDERSTORM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MG_THUNDERSTORM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MG_THUNDERSTORM = gettick() + battle_config.WOE_K_DELAY_MG_THUNDERSTORM;
				sd->state.K_LOCK_MG_THUNDERSTORM = 0;
		} else {
			sd->K_CHK_MG_THUNDERSTORM = gettick() + battle_config.K_DELAY_MG_THUNDERSTORM;
			sd->state.K_LOCK_MG_THUNDERSTORM = 0;
		}
		break;
	case AL_HEAL:
		if(DIFF_TICK(sd->K_CHK_AL_HEAL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AL_HEAL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AL_HEAL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AL_HEAL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AL_HEAL = gettick() + battle_config.WOE_K_DELAY_AL_HEAL;
				sd->state.K_LOCK_AL_HEAL = 0;
		} else {
			sd->K_CHK_AL_HEAL = gettick() + battle_config.K_DELAY_AL_HEAL;
			sd->state.K_LOCK_AL_HEAL = 0;
		}
		break;
	case AL_DECAGI:
		if(DIFF_TICK(sd->K_CHK_AL_DECAGI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AL_DECAGI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AL_DECAGI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AL_DECAGI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AL_DECAGI = gettick() + battle_config.WOE_K_DELAY_AL_DECAGI;
				sd->state.K_LOCK_AL_DECAGI = 0;
		} else {
			sd->K_CHK_AL_DECAGI = gettick() + battle_config.K_DELAY_AL_DECAGI;
			sd->state.K_LOCK_AL_DECAGI = 0;
		}
		break;
	case AL_CRUCIS:
		if(DIFF_TICK(sd->K_CHK_AL_CRUCIS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AL_CRUCIS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AL_CRUCIS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AL_CRUCIS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AL_CRUCIS = gettick() + battle_config.WOE_K_DELAY_AL_CRUCIS;
				sd->state.K_LOCK_AL_CRUCIS = 0;
		} else {
			sd->K_CHK_AL_CRUCIS = gettick() + battle_config.K_DELAY_AL_CRUCIS;
			sd->state.K_LOCK_AL_CRUCIS = 0;
		}
		break;
	case MC_MAMMONITE:
		if(DIFF_TICK(sd->K_CHK_MC_MAMMONITE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MC_MAMMONITE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MC_MAMMONITE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MC_MAMMONITE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MC_MAMMONITE = gettick() + battle_config.WOE_K_DELAY_MC_MAMMONITE;
				sd->state.K_LOCK_MC_MAMMONITE = 0;
		} else {
			sd->K_CHK_MC_MAMMONITE = gettick() + battle_config.K_DELAY_MC_MAMMONITE;
			sd->state.K_LOCK_MC_MAMMONITE = 0;
		}
		break;
	case AC_DOUBLE:
		if(DIFF_TICK(sd->K_CHK_AC_DOUBLE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AC_DOUBLE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AC_DOUBLE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AC_DOUBLE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AC_DOUBLE = gettick() + battle_config.WOE_K_DELAY_AC_DOUBLE;
				sd->state.K_LOCK_AC_DOUBLE = 0;
		} else {
			sd->K_CHK_AC_DOUBLE = gettick() + battle_config.K_DELAY_AC_DOUBLE;
			sd->state.K_LOCK_AC_DOUBLE = 0;
		}
		break;
	case AC_SHOWER:
		if(DIFF_TICK(sd->K_CHK_AC_SHOWER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AC_SHOWER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AC_SHOWER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AC_SHOWER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AC_SHOWER = gettick() + battle_config.WOE_K_DELAY_AC_SHOWER;
				sd->state.K_LOCK_AC_SHOWER = 0;
		} else {
			sd->K_CHK_AC_SHOWER = gettick() + battle_config.K_DELAY_AC_SHOWER;
			sd->state.K_LOCK_AC_SHOWER = 0;
		}
		break;
	case TF_POISON:
		if(DIFF_TICK(sd->K_CHK_TF_POISON,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_TF_POISON,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_TF_POISON - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_TF_POISON = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_TF_POISON = gettick() + battle_config.WOE_K_DELAY_TF_POISON;
				sd->state.K_LOCK_TF_POISON = 0;
		} else {
			sd->K_CHK_TF_POISON = gettick() + battle_config.K_DELAY_TF_POISON;
			sd->state.K_LOCK_TF_POISON = 0;
		}
		break;
	case KN_PIERCE:
		if(DIFF_TICK(sd->K_CHK_KN_PIERCE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_KN_PIERCE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_KN_PIERCE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_KN_PIERCE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_KN_PIERCE = gettick() + battle_config.WOE_K_DELAY_KN_PIERCE;
				sd->state.K_LOCK_KN_PIERCE = 0;
		} else {
			sd->K_CHK_KN_PIERCE = gettick() + battle_config.K_DELAY_KN_PIERCE;
			sd->state.K_LOCK_KN_PIERCE = 0;
		}
		break;
	case KN_BRANDISHSPEAR:
		if(DIFF_TICK(sd->K_CHK_KN_BRANDISHSPEAR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_KN_BRANDISHSPEAR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_KN_BRANDISHSPEAR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_KN_BRANDISHSPEAR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_KN_BRANDISHSPEAR = gettick() + battle_config.WOE_K_DELAY_KN_BRANDISHSPEAR;
				sd->state.K_LOCK_KN_BRANDISHSPEAR = 0;
		} else {
			sd->K_CHK_KN_BRANDISHSPEAR = gettick() + battle_config.K_DELAY_KN_BRANDISHSPEAR;
			sd->state.K_LOCK_KN_BRANDISHSPEAR = 0;
		}
		break;
	case KN_SPEARSTAB:
		if(DIFF_TICK(sd->K_CHK_KN_SPEARSTAB,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_KN_SPEARSTAB,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_KN_SPEARSTAB - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_KN_SPEARSTAB = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_KN_SPEARSTAB = gettick() + battle_config.WOE_K_DELAY_KN_SPEARSTAB;
				sd->state.K_LOCK_KN_SPEARSTAB = 0;
		} else {
			sd->K_CHK_KN_SPEARSTAB = gettick() + battle_config.K_DELAY_KN_SPEARSTAB;
			sd->state.K_LOCK_KN_SPEARSTAB = 0;
		}
		break;
	case KN_SPEARBOOMERANG:
		if(DIFF_TICK(sd->K_CHK_KN_SPEARBOOMERANG,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_KN_SPEARBOOMERANG,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_KN_SPEARBOOMERANG - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_KN_SPEARBOOMERANG = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_KN_SPEARBOOMERANG = gettick() + battle_config.WOE_K_DELAY_KN_SPEARBOOMER;
				sd->state.K_LOCK_KN_SPEARBOOMERANG = 0;
		} else {
			sd->K_CHK_KN_SPEARBOOMERANG = gettick() + battle_config.K_DELAY_KN_SPEARBOOMERANG;
			sd->state.K_LOCK_KN_SPEARBOOMERANG = 0;
		}
		break;
	case KN_BOWLINGBASH:
		if(DIFF_TICK(sd->K_CHK_KN_BOWLINGBASH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_KN_BOWLINGBASH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_KN_BOWLINGBASH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_KN_BOWLINGBASH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_KN_BOWLINGBASH = gettick() + battle_config.WOE_K_DELAY_KN_BOWLINGBASH;
				sd->state.K_LOCK_KN_BOWLINGBASH = 0;
		} else {
			sd->K_CHK_KN_BOWLINGBASH = gettick() + battle_config.K_DELAY_KN_BOWLINGBASH;
			sd->state.K_LOCK_KN_BOWLINGBASH = 0;
		}
		break;
	case PR_LEXDIVINA:
		if(DIFF_TICK(sd->K_CHK_PR_LEXDIVINA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PR_LEXDIVINA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PR_LEXDIVINA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PR_LEXDIVINA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PR_LEXDIVINA = gettick() + battle_config.WOE_K_DELAY_PR_LEXDIVINA;
				sd->state.K_LOCK_PR_LEXDIVINA = 0;
		} else {
			sd->K_CHK_PR_LEXDIVINA = gettick() + battle_config.K_DELAY_PR_LEXDIVINA;
			sd->state.K_LOCK_PR_LEXDIVINA = 0;
		}
		break;
	case PR_TURNUNDEAD:
		if(DIFF_TICK(sd->K_CHK_PR_TURNUNDEAD,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PR_TURNUNDEAD,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PR_TURNUNDEAD - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PR_TURNUNDEAD = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PR_TURNUNDEAD = gettick() + battle_config.WOE_K_DELAY_PR_TURNUNDEAD;
				sd->state.K_LOCK_PR_TURNUNDEAD = 0;
		} else {
			sd->K_CHK_PR_TURNUNDEAD = gettick() + battle_config.K_DELAY_PR_TURNUNDEAD;
			sd->state.K_LOCK_PR_TURNUNDEAD = 0;
		}
		break;
	case PR_LEXAETERNA:
		if(DIFF_TICK(sd->K_CHK_PR_LEXAETERNA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PR_LEXAETERNA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PR_LEXAETERNA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PR_LEXAETERNA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PR_LEXAETERNA = gettick() + battle_config.WOE_K_DELAY_PR_LEXAETERNA;
				sd->state.K_LOCK_PR_LEXAETERNA = 0;
		} else {
			sd->K_CHK_PR_LEXAETERNA = gettick() + battle_config.K_DELAY_PR_LEXAETERNA;
			sd->state.K_LOCK_PR_LEXAETERNA = 0;
		}
		break;
	case PR_MAGNUS:
		if(DIFF_TICK(sd->K_CHK_PR_MAGNUS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PR_MAGNUS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PR_MAGNUS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PR_MAGNUS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PR_MAGNUS = gettick() + battle_config.WOE_K_DELAY_PR_MAGNUS;
				sd->state.K_LOCK_PR_MAGNUS = 0;
		} else {
			sd->K_CHK_PR_MAGNUS = gettick() + battle_config.K_DELAY_PR_MAGNUS;
			sd->state.K_LOCK_PR_MAGNUS = 0;
		}
		break;
	case WZ_FIREPILLAR:
		if(DIFF_TICK(sd->K_CHK_WZ_FIREPILLAR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_FIREPILLAR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_FIREPILLAR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_FIREPILLAR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_FIREPILLAR = gettick() + battle_config.WOE_K_DELAY_WZ_FIREPILLAR;
				sd->state.K_LOCK_WZ_FIREPILLAR = 0;
		} else {
			sd->K_CHK_WZ_FIREPILLAR = gettick() + battle_config.K_DELAY_WZ_FIREPILLAR;
			sd->state.K_LOCK_WZ_FIREPILLAR = 0;
		}
		break;
	case WZ_SIGHTRASHER:
		if(DIFF_TICK(sd->K_CHK_WZ_SIGHTRASHER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_SIGHTRASHER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_SIGHTRASHER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_SIGHTRASHER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_SIGHTRASHER = gettick() + battle_config.WOE_K_DELAY_WZ_SIGHTRASHER;
				sd->state.K_LOCK_WZ_SIGHTRASHER = 0;
		} else {
			sd->K_CHK_WZ_SIGHTRASHER = gettick() + battle_config.K_DELAY_WZ_SIGHTRASHER;
			sd->state.K_LOCK_WZ_SIGHTRASHER = 0;
		}
		break;
	case WZ_FIREIVY:
		if(DIFF_TICK(sd->K_CHK_WZ_FIREIVY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_FIREIVY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_FIREIVY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_FIREIVY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_FIREIVY = gettick() + battle_config.WOE_K_DELAY_WZ_FIREIVY;
				sd->state.K_LOCK_WZ_FIREIVY = 0;
		} else {
			sd->K_CHK_WZ_FIREIVY = gettick() + battle_config.K_DELAY_WZ_FIREIVY;
			sd->state.K_LOCK_WZ_FIREIVY = 0;
		}
		break;
	case WZ_METEOR:
		if(DIFF_TICK(sd->K_CHK_WZ_METEOR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_METEOR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_METEOR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_METEOR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_METEOR = gettick() + battle_config.WOE_K_DELAY_WZ_METEOR;
				sd->state.K_LOCK_WZ_METEOR = 0;
		} else {
			sd->K_CHK_WZ_METEOR = gettick() + battle_config.K_DELAY_WZ_METEOR;
			sd->state.K_LOCK_WZ_METEOR = 0;
		}
		break;
	case WZ_JUPITEL:
		if(DIFF_TICK(sd->K_CHK_WZ_JUPITEL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_JUPITEL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_JUPITEL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_JUPITEL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_JUPITEL = gettick() + battle_config.WOE_K_DELAY_WZ_JUPITEL;
				sd->state.K_LOCK_WZ_JUPITEL = 0;
		} else {
			sd->K_CHK_WZ_JUPITEL = gettick() + battle_config.K_DELAY_WZ_JUPITEL;
			sd->state.K_LOCK_WZ_JUPITEL = 0;
		}
		break;
	case WZ_VERMILION:
		if(DIFF_TICK(sd->K_CHK_WZ_VERMILION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_VERMILION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_VERMILION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_VERMILION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_VERMILION = gettick() + battle_config.WOE_K_DELAY_WZ_VERMILION;
				sd->state.K_LOCK_WZ_VERMILION = 0;
		} else {
			sd->K_CHK_WZ_VERMILION = gettick() + battle_config.K_DELAY_WZ_VERMILION;
			sd->state.K_LOCK_WZ_VERMILION = 0;
		}
		break;
	case WZ_WATERBALL:
		if(DIFF_TICK(sd->K_CHK_WZ_WATERBALL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_WATERBALL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_WATERBALL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_WATERBALL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_WATERBALL = gettick() + battle_config.WOE_K_DELAY_WZ_WATERBALL;
				sd->state.K_LOCK_WZ_WATERBALL = 0;
		} else {
			sd->K_CHK_WZ_WATERBALL = gettick() + battle_config.K_DELAY_WZ_WATERBALL;
			sd->state.K_LOCK_WZ_WATERBALL = 0;
		}
		break;
	case WZ_ICEWALL:
		if(DIFF_TICK(sd->K_CHK_WZ_ICEWALL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_ICEWALL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_ICEWALL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_ICEWALL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_ICEWALL = gettick() + battle_config.WOE_K_DELAY_WZ_ICEWALL;
				sd->state.K_LOCK_WZ_ICEWALL = 0;
		} else {
			sd->K_CHK_WZ_ICEWALL = gettick() + battle_config.K_DELAY_WZ_ICEWALL;
			sd->state.K_LOCK_WZ_ICEWALL = 0;
		}
		break;
	case WZ_FROSTNOVA:
		if(DIFF_TICK(sd->K_CHK_WZ_FROSTNOVA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_FROSTNOVA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_FROSTNOVA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_FROSTNOVA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_FROSTNOVA = gettick() + battle_config.WOE_K_DELAY_WZ_FROSTNOVA;
				sd->state.K_LOCK_WZ_FROSTNOVA = 0;
		} else {
			sd->K_CHK_WZ_FROSTNOVA = gettick() + battle_config.K_DELAY_WZ_FROSTNOVA;
			sd->state.K_LOCK_WZ_FROSTNOVA = 0;
		}
		break;
	case WZ_STORMGUST:
		if(DIFF_TICK(sd->K_CHK_WZ_STORMGUST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_STORMGUST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_STORMGUST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_STORMGUST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_STORMGUST = gettick() + battle_config.WOE_K_DELAY_WZ_STORMGUST;
				sd->state.K_LOCK_WZ_STORMGUST = 0;
		} else {
			sd->K_CHK_WZ_STORMGUST = gettick() + battle_config.K_DELAY_WZ_STORMGUST;
			sd->state.K_LOCK_WZ_STORMGUST = 0;
		}
		break;
	case WZ_EARTHSPIKE:
		if(DIFF_TICK(sd->K_CHK_WZ_EARTHSPIKE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_EARTHSPIKE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_EARTHSPIKE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_EARTHSPIKE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_EARTHSPIKE = gettick() + battle_config.WOE_K_DELAY_WZ_EARTHSPIKE;
				sd->state.K_LOCK_WZ_EARTHSPIKE = 0;
		} else {
			sd->K_CHK_WZ_EARTHSPIKE = gettick() + battle_config.K_DELAY_WZ_EARTHSPIKE;
			sd->state.K_LOCK_WZ_EARTHSPIKE = 0;
		}
		break;
	case WZ_HEAVENDRIVE:
		if(DIFF_TICK(sd->K_CHK_WZ_HEAVENDRIVE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_HEAVENDRIVE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_HEAVENDRIVE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_HEAVENDRIVE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_HEAVENDRIVE = gettick() + battle_config.WOE_K_DELAY_WZ_HEAVENDRIVE;
				sd->state.K_LOCK_WZ_HEAVENDRIVE = 0;
		} else {
			sd->K_CHK_WZ_HEAVENDRIVE = gettick() + battle_config.K_DELAY_WZ_HEAVENDRIVE;
			sd->state.K_LOCK_WZ_HEAVENDRIVE = 0;
		}
		break;
	case WZ_QUAGMIRE:
		if(DIFF_TICK(sd->K_CHK_WZ_QUAGMIRE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_QUAGMIRE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_QUAGMIRE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_QUAGMIRE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_QUAGMIRE = gettick() + battle_config.WOE_K_DELAY_WZ_QUAGMIRE;
				sd->state.K_LOCK_WZ_QUAGMIRE = 0;
		} else {
			sd->K_CHK_WZ_QUAGMIRE = gettick() + battle_config.K_DELAY_WZ_QUAGMIRE;
			sd->state.K_LOCK_WZ_QUAGMIRE = 0;
		}
		break;
	case WZ_ESTIMATION:
		if(DIFF_TICK(sd->K_CHK_WZ_ESTIMATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_ESTIMATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_ESTIMATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_ESTIMATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_ESTIMATION = gettick() + battle_config.WOE_K_DELAY_WZ_ESTIMATION;
				sd->state.K_LOCK_WZ_ESTIMATION = 0;
		} else {
			sd->K_CHK_WZ_ESTIMATION = gettick() + battle_config.K_DELAY_WZ_ESTIMATION;
			sd->state.K_LOCK_WZ_ESTIMATION = 0;
		}
		break;
	case BS_HAMMERFALL:
		if(DIFF_TICK(sd->K_CHK_BS_HAMMERFALL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BS_HAMMERFALL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BS_HAMMERFALL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BS_HAMMERFALL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BS_HAMMERFALL = gettick() + battle_config.WOE_K_DELAY_BS_HAMMERFALL;
				sd->state.K_LOCK_BS_HAMMERFALL = 0;
		} else {
			sd->K_CHK_BS_HAMMERFALL = gettick() + battle_config.K_DELAY_BS_HAMMERFALL;
			sd->state.K_LOCK_BS_HAMMERFALL = 0;
		}
		break;
	case HT_BLITZBEAT:
		if(DIFF_TICK(sd->K_CHK_HT_BLITZBEAT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_HT_BLITZBEAT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_HT_BLITZBEAT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_HT_BLITZBEAT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_HT_BLITZBEAT = gettick() + battle_config.WOE_K_DELAY_HT_BLITZBEAT;
				sd->state.K_LOCK_HT_BLITZBEAT = 0;
		} else {
			sd->K_CHK_HT_BLITZBEAT = gettick() + battle_config.K_DELAY_HT_BLITZBEAT;
			sd->state.K_LOCK_HT_BLITZBEAT = 0;
		}
		break;
	case AS_SONICBLOW:
		if(DIFF_TICK(sd->K_CHK_AS_SONICBLOW,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AS_SONICBLOW,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AS_SONICBLOW - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AS_SONICBLOW = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AS_SONICBLOW = gettick() + battle_config.WOE_K_DELAY_AS_SONICBLOW;
				sd->state.K_LOCK_AS_SONICBLOW = 0;
		} else {
			sd->K_CHK_AS_SONICBLOW = gettick() + battle_config.K_DELAY_AS_SONICBLOW;
			sd->state.K_LOCK_AS_SONICBLOW = 0;
		}
		break;
	case AS_GRIMTOOTH:
		if(DIFF_TICK(sd->K_CHK_AS_GRIMTOOTH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AS_GRIMTOOTH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AS_GRIMTOOTH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AS_GRIMTOOTH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AS_GRIMTOOTH = gettick() + battle_config.WOE_K_DELAY_AS_GRIMTOOTH;
				sd->state.K_LOCK_AS_GRIMTOOTH = 0;
		} else {
			sd->K_CHK_AS_GRIMTOOTH = gettick() + battle_config.K_DELAY_AS_GRIMTOOTH;
			sd->state.K_LOCK_AS_GRIMTOOTH = 0;
		}
		break;
	case AC_CHARGEARROW:
		if(DIFF_TICK(sd->K_CHK_AC_CHARGEARROW,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AC_CHARGEARROW,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AC_CHARGEARROW - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AC_CHARGEARROW = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AC_CHARGEARROW = gettick() + battle_config.WOE_K_DELAY_AC_CHARGEARROW;
				sd->state.K_LOCK_AC_CHARGEARROW = 0;
		} else {
			sd->K_CHK_AC_CHARGEARROW = gettick() + battle_config.K_DELAY_AC_CHARGEARROW;
			sd->state.K_LOCK_AC_CHARGEARROW = 0;
		}
		break;
	case TF_BACKSLIDING:
		if(DIFF_TICK(sd->K_CHK_TF_BACKSLIDING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_TF_BACKSLIDING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_TF_BACKSLIDING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_TF_BACKSLIDING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_TF_BACKSLIDING = gettick() + battle_config.WOE_K_DELAY_TF_BACKSLIDING;
				sd->state.K_LOCK_TF_BACKSLIDING = 0;
		} else {
			sd->K_CHK_TF_BACKSLIDING = gettick() + battle_config.K_DELAY_TF_BACKSLIDING;
			sd->state.K_LOCK_TF_BACKSLIDING = 0;
		}
		break;
	case MC_CARTREVOLUTION:
		if(DIFF_TICK(sd->K_CHK_MC_CARTREVOLUTION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MC_CARTREVOLUTION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MC_CARTREVOLUTION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MC_CARTREVOLUTION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MC_CARTREVOLUTION = gettick() + battle_config.WOE_K_DELAY_MC_CARTREVO;
				sd->state.K_LOCK_MC_CARTREVOLUTION = 0;
		} else {
			sd->K_CHK_MC_CARTREVOLUTION = gettick() + battle_config.K_DELAY_MC_CARTREVOLUTION;
			sd->state.K_LOCK_MC_CARTREVOLUTION = 0;
		}
		break;
	case AL_HOLYLIGHT:
		if(DIFF_TICK(sd->K_CHK_AL_HOLYLIGHT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AL_HOLYLIGHT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AL_HOLYLIGHT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AL_HOLYLIGHT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AL_HOLYLIGHT = gettick() + battle_config.WOE_K_DELAY_AL_HOLYLIGHT;
				sd->state.K_LOCK_AL_HOLYLIGHT = 0;
		} else {
			sd->K_CHK_AL_HOLYLIGHT = gettick() + battle_config.K_DELAY_AL_HOLYLIGHT;
			sd->state.K_LOCK_AL_HOLYLIGHT = 0;
		}
		break;
	case RG_BACKSTAP:
		if(DIFF_TICK(sd->K_CHK_RG_BACKSTAP,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RG_BACKSTAP,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RG_BACKSTAP - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RG_BACKSTAP = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RG_BACKSTAP = gettick() + battle_config.WOE_K_DELAY_RG_BACKSTAP;
				sd->state.K_LOCK_RG_BACKSTAP = 0;
		} else {
			sd->K_CHK_RG_BACKSTAP = gettick() + battle_config.K_DELAY_RG_BACKSTAP;
			sd->state.K_LOCK_RG_BACKSTAP = 0;
		}
		break;
	case RG_RAID:
		if(DIFF_TICK(sd->K_CHK_RG_RAID,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RG_RAID,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RG_RAID - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RG_RAID = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RG_RAID = gettick() + battle_config.WOE_K_DELAY_RG_RAID;
				sd->state.K_LOCK_RG_RAID = 0;
		} else {
			sd->K_CHK_RG_RAID = gettick() + battle_config.K_DELAY_RG_RAID;
			sd->state.K_LOCK_RG_RAID = 0;
		}
		break;
	case RG_GRAFFITI:
		if(DIFF_TICK(sd->K_CHK_RG_GRAFFITI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RG_GRAFFITI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RG_GRAFFITI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RG_GRAFFITI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RG_GRAFFITI = gettick() + battle_config.WOE_K_DELAY_RG_GRAFFITI;
				sd->state.K_LOCK_RG_GRAFFITI = 0;
		} else {
			sd->K_CHK_RG_GRAFFITI = gettick() + battle_config.K_DELAY_RG_GRAFFITI;
			sd->state.K_LOCK_RG_GRAFFITI = 0;
		}
		break;
	case RG_FLAGGRAFFITI:
		if(DIFF_TICK(sd->K_CHK_RG_FLAGGRAFFITI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RG_FLAGGRAFFITI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RG_FLAGGRAFFITI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RG_FLAGGRAFFITI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RG_FLAGGRAFFITI = gettick() + battle_config.WOE_K_DELAY_RG_FLAGGRAFFITI;
				sd->state.K_LOCK_RG_FLAGGRAFFITI = 0;
		} else {
			sd->K_CHK_RG_FLAGGRAFFITI = gettick() + battle_config.K_DELAY_RG_FLAGGRAFFITI;
			sd->state.K_LOCK_RG_FLAGGRAFFITI = 0;
		}
		break;
	case RG_COMPULSION:
		if(DIFF_TICK(sd->K_CHK_RG_COMPULSION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RG_COMPULSION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RG_COMPULSION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RG_COMPULSION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RG_COMPULSION = gettick() + battle_config.WOE_K_DELAY_RG_COMPULSION;
				sd->state.K_LOCK_RG_COMPULSION = 0;
		} else {
			sd->K_CHK_RG_COMPULSION = gettick() + battle_config.K_DELAY_RG_COMPULSION;
			sd->state.K_LOCK_RG_COMPULSION = 0;
		}
		break;
	case RG_PLAGIARISM:
		if(DIFF_TICK(sd->K_CHK_RG_PLAGIARISM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RG_PLAGIARISM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RG_PLAGIARISM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RG_PLAGIARISM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RG_PLAGIARISM = gettick() + battle_config.WOE_K_DELAY_RG_PLAGIARISM;
				sd->state.K_LOCK_RG_PLAGIARISM = 0;
		} else {
			sd->K_CHK_RG_PLAGIARISM = gettick() + battle_config.K_DELAY_RG_PLAGIARISM;
			sd->state.K_LOCK_RG_PLAGIARISM = 0;
		}
		break;
	case AM_DEMONSTRATION:
		if(DIFF_TICK(sd->K_CHK_AM_DEMONSTRATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AM_DEMONSTRATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AM_DEMONSTRATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AM_DEMONSTRATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AM_DEMONSTRATION = gettick() + battle_config.WOE_K_DELAY_AM_DEMONSTRATION;
				sd->state.K_LOCK_AM_DEMONSTRATION = 0;
		} else {
			sd->K_CHK_AM_DEMONSTRATION = gettick() + battle_config.K_DELAY_AM_DEMONSTRATION;
			sd->state.K_LOCK_AM_DEMONSTRATION = 0;
		}
		break;
	case AM_ACIDTERROR:
		if(DIFF_TICK(sd->K_CHK_AM_ACIDTERROR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AM_ACIDTERROR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AM_ACIDTERROR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AM_ACIDTERROR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AM_ACIDTERROR = gettick() + battle_config.WOE_K_DELAY_AM_ACIDTERROR;
				sd->state.K_LOCK_AM_ACIDTERROR = 0;
		} else {
			sd->K_CHK_AM_ACIDTERROR = gettick() + battle_config.K_DELAY_AM_ACIDTERROR;
			sd->state.K_LOCK_AM_ACIDTERROR = 0;
		}
		break;
	case AM_POTIONPITCHER:
		if(DIFF_TICK(sd->K_CHK_AM_POTIONPITCHER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AM_POTIONPITCHER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AM_POTIONPITCHER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AM_POTIONPITCHER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AM_POTIONPITCHER = gettick() + battle_config.WOE_K_DELAY_AM_POTIONPITCH;
				sd->state.K_LOCK_AM_POTIONPITCHER = 0;
		} else {
			sd->K_CHK_AM_POTIONPITCHER = gettick() + battle_config.K_DELAY_AM_POTIONPITCHER;
			sd->state.K_LOCK_AM_POTIONPITCHER = 0;
		}
		break;
	case AM_CANNIBALIZE:
		if(DIFF_TICK(sd->K_CHK_AM_CANNIBALIZE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AM_CANNIBALIZE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AM_CANNIBALIZE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AM_CANNIBALIZE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AM_CANNIBALIZE = gettick() + battle_config.WOE_K_DELAY_AM_CANNIBALIZE;
				sd->state.K_LOCK_AM_CANNIBALIZE = 0;
		} else {
			sd->K_CHK_AM_CANNIBALIZE = gettick() + battle_config.K_DELAY_AM_CANNIBALIZE;
			sd->state.K_LOCK_AM_CANNIBALIZE = 0;
		}
		break;
	case AM_SPHEREMINE:
		if(DIFF_TICK(sd->K_CHK_AM_SPHEREMINE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AM_SPHEREMINE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AM_SPHEREMINE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AM_SPHEREMINE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AM_SPHEREMINE = gettick() + battle_config.WOE_K_DELAY_AM_SPHEREMINE;
				sd->state.K_LOCK_AM_SPHEREMINE = 0;
		} else {
			sd->K_CHK_AM_SPHEREMINE = gettick() + battle_config.K_DELAY_AM_SPHEREMINE;
			sd->state.K_LOCK_AM_SPHEREMINE = 0;
		}
		break;
	case AM_FLAMECONTROL:
		if(DIFF_TICK(sd->K_CHK_AM_FLAMECONTROL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AM_FLAMECONTROL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AM_FLAMECONTROL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AM_FLAMECONTROL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AM_FLAMECONTROL = gettick() + battle_config.WOE_K_DELAY_AM_FLAMECONTROL;
				sd->state.K_LOCK_AM_FLAMECONTROL = 0;
		} else {
			sd->K_CHK_AM_FLAMECONTROL = gettick() + battle_config.K_DELAY_AM_FLAMECONTROL;
			sd->state.K_LOCK_AM_FLAMECONTROL = 0;
		}
		break;
	case AM_DRILLMASTER:
		if(DIFF_TICK(sd->K_CHK_AM_DRILLMASTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AM_DRILLMASTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AM_DRILLMASTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AM_DRILLMASTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AM_DRILLMASTER = gettick() + battle_config.WOE_K_DELAY_AM_DRILLMASTER;
				sd->state.K_LOCK_AM_DRILLMASTER = 0;
		} else {
			sd->K_CHK_AM_DRILLMASTER = gettick() + battle_config.K_DELAY_AM_DRILLMASTER;
			sd->state.K_LOCK_AM_DRILLMASTER = 0;
		}
		break;
	case CR_SHIELDBOOMERANG:
		if(DIFF_TICK(sd->K_CHK_CR_SHLDBOOMRANG,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CR_SHLDBOOMRANG,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CR_SHLDBOOMRANG - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CR_SHLDBOOMRANG = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CR_SHLDBOOMRANG = gettick() + battle_config.WOE_K_DELAY_CR_SHLDBOOMRANG;
				sd->state.K_LOCK_CR_SHLDBOOMRANG = 0;
		} else {
			sd->K_CHK_CR_SHLDBOOMRANG = gettick() + battle_config.K_DELAY_CR_SHIELDBOOMERANG;
			sd->state.K_LOCK_CR_SHLDBOOMRANG = 0;
		}
		break;
	case CR_HOLYCROSS:
		if(DIFF_TICK(sd->K_CHK_CR_HOLYCROSS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CR_HOLYCROSS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CR_HOLYCROSS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CR_HOLYCROSS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CR_HOLYCROSS = gettick() + battle_config.WOE_K_DELAY_CR_HOLYCROSS;
				sd->state.K_LOCK_CR_HOLYCROSS = 0;
		} else {
			sd->K_CHK_CR_HOLYCROSS = gettick() + battle_config.K_DELAY_CR_HOLYCROSS;
			sd->state.K_LOCK_CR_HOLYCROSS = 0;
		}
		break;
	case CR_GRANDCROSS:
		if(DIFF_TICK(sd->K_CHK_CR_GRANDCROSS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CR_GRANDCROSS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CR_GRANDCROSS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CR_GRANDCROSS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CR_GRANDCROSS = gettick() + battle_config.WOE_K_DELAY_CR_GRANDCROSS;
				sd->state.K_LOCK_CR_GRANDCROSS = 0;
		} else {
			sd->K_CHK_CR_GRANDCROSS = gettick() + battle_config.K_DELAY_CR_GRANDCROSS;
			sd->state.K_LOCK_CR_GRANDCROSS = 0;
		}
		break;
	case MO_CALLSPIRITS:
		if(DIFF_TICK(sd->K_CHK_MO_CALLSPIRITS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_CALLSPIRITS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_CALLSPIRITS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_CALLSPIRITS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_CALLSPIRITS = gettick() + battle_config.WOE_K_DELAY_MO_CALLSPIRITS;
				sd->state.K_LOCK_MO_CALLSPIRITS = 0;
		} else {
			sd->K_CHK_MO_CALLSPIRITS = gettick() + battle_config.K_DELAY_MO_CALLSPIRITS;
			sd->state.K_LOCK_MO_CALLSPIRITS = 0;
		}
		break;
	case MO_ABSORBSPIRITS:
		if(DIFF_TICK(sd->K_CHK_MO_ABSORBSPIRITS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_ABSORBSPIRITS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_ABSORBSPIRITS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_ABSORBSPIRITS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_ABSORBSPIRITS = gettick() + battle_config.WOE_K_DELAY_MO_ABSORBSPIRITS;
				sd->state.K_LOCK_MO_ABSORBSPIRITS = 0;
		} else {
			sd->K_CHK_MO_ABSORBSPIRITS = gettick() + battle_config.K_DELAY_MO_ABSORBSPIRITS;
			sd->state.K_LOCK_MO_ABSORBSPIRITS = 0;
		}
		break;
	case MO_BODYRELOCATION:
		if(DIFF_TICK(sd->K_CHK_MO_BODYRELOCATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_BODYRELOCATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_BODYRELOCATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_BODYRELOCATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_BODYRELOCATION = gettick() + battle_config.WOE_K_DELAY_MO_BODYRELOC;
				sd->state.K_LOCK_MO_BODYRELOCATION = 0;
		} else {
			sd->K_CHK_MO_BODYRELOCATION = gettick() + battle_config.K_DELAY_MO_BODYRELOCATION;
			sd->state.K_LOCK_MO_BODYRELOCATION = 0;
		}
		break;
	case MO_INVESTIGATE:
		if(DIFF_TICK(sd->K_CHK_MO_INVESTIGATE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_INVESTIGATE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_INVESTIGATE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_INVESTIGATE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_INVESTIGATE = gettick() + battle_config.WOE_K_DELAY_MO_INVESTIGATE;
				sd->state.K_LOCK_MO_INVESTIGATE = 0;
		} else {
			sd->K_CHK_MO_INVESTIGATE = gettick() + battle_config.K_DELAY_MO_INVESTIGATE;
			sd->state.K_LOCK_MO_INVESTIGATE = 0;
		}
		break;
	case MO_FINGEROFFENSIVE:
		if(DIFF_TICK(sd->K_CHK_MO_FINGEROFFENSIVE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_FINGEROFFENSIVE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_FINGEROFFENSIVE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_FINGEROFFENSE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_FINGEROFFENSIVE = gettick() + battle_config.WOE_K_DELAY_MO_FINGEROFFENSE;
				sd->state.K_LOCK_MO_FINGEROFFENSE = 0;
		} else {
			sd->K_CHK_MO_FINGEROFFENSIVE = gettick() + battle_config.K_DELAY_MO_FINGEROFFENSIVE;
			sd->state.K_LOCK_MO_FINGEROFFENSE = 0;
		}
		break;
	case MO_EXPLOSIONSPIRITS:
		if(DIFF_TICK(sd->K_CHK_MO_EXPLOSIONSPIRITS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_EXPLOSIONSPIRITS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_EXPLOSIONSPIRITS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_EXPLODESPIRIT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_EXPLOSIONSPIRITS = gettick() + battle_config.WOE_K_DELAY_MO_EXPLOSIONSPIRIT;
				sd->state.K_LOCK_MO_EXPLODESPIRIT = 0;
		} else {
			sd->K_CHK_MO_EXPLOSIONSPIRITS = gettick() + battle_config.K_DELAY_MO_EXPLOSIONSPIRITS;
			sd->state.K_LOCK_MO_EXPLODESPIRIT = 0;
		}
		break;
	case MO_EXTREMITYFIST:
		if(DIFF_TICK(sd->K_CHK_MO_EXTREMITYFIST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_EXTREMITYFIST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_EXTREMITYFIST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_EXTREMITYFIST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_EXTREMITYFIST = gettick() + battle_config.WOE_K_DELAY_MO_EXTREMITYFIST;
				sd->state.K_LOCK_MO_EXTREMITYFIST = 0;
		} else {
			sd->K_CHK_MO_EXTREMITYFIST = gettick() + battle_config.K_DELAY_MO_EXTREMITYFIST;
			sd->state.K_LOCK_MO_EXTREMITYFIST = 0;
		}
		break;
	case MO_CHAINCOMBO:
		if(DIFF_TICK(sd->K_CHK_MO_CHAINCOMBO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_CHAINCOMBO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_CHAINCOMBO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_CHAINCOMBO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_CHAINCOMBO = gettick() + battle_config.WOE_K_DELAY_MO_CHAINCOMBO;
				sd->state.K_LOCK_MO_CHAINCOMBO = 0;
		} else {
			sd->K_CHK_MO_CHAINCOMBO = gettick() + battle_config.K_DELAY_MO_CHAINCOMBO;
			sd->state.K_LOCK_MO_CHAINCOMBO = 0;
		}
		break;
	case MO_COMBOFINISH:
		if(DIFF_TICK(sd->K_CHK_MO_COMBOFINISH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_COMBOFINISH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_COMBOFINISH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_COMBOFINISH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_COMBOFINISH = gettick() + battle_config.WOE_K_DELAY_MO_COMBOFINISH;
				sd->state.K_LOCK_MO_COMBOFINISH = 0;
		} else {
			sd->K_CHK_MO_COMBOFINISH = gettick() + battle_config.K_DELAY_MO_COMBOFINISH;
			sd->state.K_LOCK_MO_COMBOFINISH = 0;
		}
		break;
	case SA_CASTCANCEL:
		if(DIFF_TICK(sd->K_CHK_SA_CASTCANCEL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_CASTCANCEL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_CASTCANCEL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_CASTCANCEL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_CASTCANCEL = gettick() + battle_config.WOE_K_DELAY_SA_CASTCANCEL;
				sd->state.K_LOCK_SA_CASTCANCEL = 0;
		} else {
			sd->K_CHK_SA_CASTCANCEL = gettick() + battle_config.K_DELAY_SA_CASTCANCEL;
			sd->state.K_LOCK_SA_CASTCANCEL = 0;
		}
		break;
	case SA_SPELLBREAKER:
		if(DIFF_TICK(sd->K_CHK_SA_SPELLBREAKER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_SPELLBREAKER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_SPELLBREAKER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_SPELLBREAKER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_SPELLBREAKER = gettick() + battle_config.WOE_K_DELAY_SA_SPELLBREAKER;
				sd->state.K_LOCK_SA_SPELLBREAKER = 0;
		} else {
			sd->K_CHK_SA_SPELLBREAKER = gettick() + battle_config.K_DELAY_SA_SPELLBREAKER;
			sd->state.K_LOCK_SA_SPELLBREAKER = 0;
		}
		break;
	case SA_DISPELL:
		if(DIFF_TICK(sd->K_CHK_SA_DISPELL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_DISPELL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_DISPELL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_DISPELL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_DISPELL = gettick() + battle_config.WOE_K_DELAY_SA_DISPELL;
				sd->state.K_LOCK_SA_DISPELL = 0;
		} else {
			sd->K_CHK_SA_DISPELL = gettick() + battle_config.K_DELAY_SA_DISPELL;
			sd->state.K_LOCK_SA_DISPELL = 0;
		}
		break;
	case SA_ABRACADABRA:
		if(DIFF_TICK(sd->K_CHK_SA_ABRACADABRA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_ABRACADABRA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_ABRACADABRA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_ABRACADABRA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_ABRACADABRA = gettick() + battle_config.WOE_K_DELAY_SA_ABRACADABRA;
				sd->state.K_LOCK_SA_ABRACADABRA = 0;
		} else {
			sd->K_CHK_SA_ABRACADABRA = gettick() + battle_config.K_DELAY_SA_ABRACADABRA;
			sd->state.K_LOCK_SA_ABRACADABRA = 0;
		}
		break;
	case SA_MONOCELL:
		if(DIFF_TICK(sd->K_CHK_SA_MONOCELL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_MONOCELL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_MONOCELL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_MONOCELL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_MONOCELL = gettick() + battle_config.WOE_K_DELAY_SA_MONOCELL;
				sd->state.K_LOCK_SA_MONOCELL = 0;
		} else {
			sd->K_CHK_SA_MONOCELL = gettick() + battle_config.K_DELAY_SA_MONOCELL;
			sd->state.K_LOCK_SA_MONOCELL = 0;
		}
		break;
	case SA_CLASSCHANGE:
		if(DIFF_TICK(sd->K_CHK_SA_CLASSCHANGE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_CLASSCHANGE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_CLASSCHANGE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_CLASSCHANGE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_CLASSCHANGE = gettick() + battle_config.WOE_K_DELAY_SA_CLASSCHANGE;
				sd->state.K_LOCK_SA_CLASSCHANGE = 0;
		} else {
			sd->K_CHK_SA_CLASSCHANGE = gettick() + battle_config.K_DELAY_SA_CLASSCHANGE;
			sd->state.K_LOCK_SA_CLASSCHANGE = 0;
		}
		break;
	case SA_SUMMONMONSTER:
		if(DIFF_TICK(sd->K_CHK_SA_SUMMONMONSTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_SUMMONMONSTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_SUMMONMONSTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_SUMMONMONSTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_SUMMONMONSTER = gettick() + battle_config.WOE_K_DELAY_SA_SUMMONMONSTER;
				sd->state.K_LOCK_SA_SUMMONMONSTER = 0;
		} else {
			sd->K_CHK_SA_SUMMONMONSTER = gettick() + battle_config.K_DELAY_SA_SUMMONMONSTER;
			sd->state.K_LOCK_SA_SUMMONMONSTER = 0;
		}
		break;
	case SA_REVERSEORCISH:
		if(DIFF_TICK(sd->K_CHK_SA_REVERSEORCISH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_REVERSEORCISH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_REVERSEORCISH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_REVERSEORCISH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_REVERSEORCISH = gettick() + battle_config.WOE_K_DELAY_SA_REVERSEORCISH;
				sd->state.K_LOCK_SA_REVERSEORCISH = 0;
		} else {
			sd->K_CHK_SA_REVERSEORCISH = gettick() + battle_config.K_DELAY_SA_REVERSEORCISH;
			sd->state.K_LOCK_SA_REVERSEORCISH = 0;
		}
		break;
	case SA_DEATH:
		if(DIFF_TICK(sd->K_CHK_SA_DEATH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_DEATH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_DEATH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_DEATH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_DEATH = gettick() + battle_config.WOE_K_DELAY_SA_DEATH;
				sd->state.K_LOCK_SA_DEATH = 0;
		} else {
			sd->K_CHK_SA_DEATH = gettick() + battle_config.K_DELAY_SA_DEATH;
			sd->state.K_LOCK_SA_DEATH = 0;
		}
		break;
	case SA_FORTUNE:
		if(DIFF_TICK(sd->K_CHK_SA_FORTUNE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_FORTUNE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_FORTUNE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_FORTUNE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_FORTUNE = gettick() + battle_config.WOE_K_DELAY_SA_FORTUNE;
				sd->state.K_LOCK_SA_FORTUNE = 0;
		} else {
			sd->K_CHK_SA_FORTUNE = gettick() + battle_config.K_DELAY_SA_FORTUNE;
			sd->state.K_LOCK_SA_FORTUNE = 0;
		}
		break;
	case SA_TAMINGMONSTER:
		if(DIFF_TICK(sd->K_CHK_SA_TAMINGMONSTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_TAMINGMONSTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_TAMINGMONSTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_TAMINGMONSTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_TAMINGMONSTER = gettick() + battle_config.WOE_K_DELAY_SA_TAMINGMONSTER;
				sd->state.K_LOCK_SA_TAMINGMONSTER = 0;
		} else {
			sd->K_CHK_SA_TAMINGMONSTER = gettick() + battle_config.K_DELAY_SA_TAMINGMONSTER;
			sd->state.K_LOCK_SA_TAMINGMONSTER = 0;
		}
		break;
	case SA_QUESTION:
		if(DIFF_TICK(sd->K_CHK_SA_QUESTION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_QUESTION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_QUESTION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_QUESTION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_QUESTION = gettick() + battle_config.WOE_K_DELAY_SA_QUESTION;
				sd->state.K_LOCK_SA_QUESTION = 0;
		} else {
			sd->K_CHK_SA_QUESTION = gettick() + battle_config.K_DELAY_SA_QUESTION;
			sd->state.K_LOCK_SA_QUESTION = 0;
		}
		break;
	case SA_GRAVITY:
		if(DIFF_TICK(sd->K_CHK_SA_GRAVITY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_GRAVITY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_GRAVITY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_GRAVITY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_GRAVITY = gettick() + battle_config.WOE_K_DELAY_SA_GRAVITY;
				sd->state.K_LOCK_SA_GRAVITY = 0;
		} else {
			sd->K_CHK_SA_GRAVITY = gettick() + battle_config.K_DELAY_SA_GRAVITY;
			sd->state.K_LOCK_SA_GRAVITY = 0;
		}
		break;
	case SA_LEVELUP:
		if(DIFF_TICK(sd->K_CHK_SA_LEVELUP,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_LEVELUP,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_LEVELUP - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_LEVELUP = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_LEVELUP = gettick() + battle_config.WOE_K_DELAY_SA_LEVELUP;
				sd->state.K_LOCK_SA_LEVELUP = 0;
		} else {
			sd->K_CHK_SA_LEVELUP = gettick() + battle_config.K_DELAY_SA_LEVELUP;
			sd->state.K_LOCK_SA_LEVELUP = 0;
		}
		break;
	case SA_INSTANTDEATH:
		if(DIFF_TICK(sd->K_CHK_SA_INSTANTDEATH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_INSTANTDEATH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_INSTANTDEATH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_INSTANTDEATH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_INSTANTDEATH = gettick() + battle_config.WOE_K_DELAY_SA_INSTANTDEATH;
				sd->state.K_LOCK_SA_INSTANTDEATH = 0;
		} else {
			sd->K_CHK_SA_INSTANTDEATH = gettick() + battle_config.K_DELAY_SA_INSTANTDEATH;
			sd->state.K_LOCK_SA_INSTANTDEATH = 0;
		}
		break;
	case SA_FULLRECOVERY:
		if(DIFF_TICK(sd->K_CHK_SA_FULLRECOVERY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_FULLRECOVERY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_FULLRECOVERY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_FULLRECOVERY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_FULLRECOVERY = gettick() + battle_config.WOE_K_DELAY_SA_FULLRECOVERY;
				sd->state.K_LOCK_SA_FULLRECOVERY = 0;
		} else {
			sd->K_CHK_SA_FULLRECOVERY = gettick() + battle_config.K_DELAY_SA_FULLRECOVERY;
			sd->state.K_LOCK_SA_FULLRECOVERY = 0;
		}
		break;
	case SA_COMA:
		if(DIFF_TICK(sd->K_CHK_SA_COMA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SA_COMA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SA_COMA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SA_COMA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SA_COMA = gettick() + battle_config.WOE_K_DELAY_SA_COMA;
				sd->state.K_LOCK_SA_COMA = 0;
		} else {
			sd->K_CHK_SA_COMA = gettick() + battle_config.K_DELAY_SA_COMA;
			sd->state.K_LOCK_SA_COMA = 0;
		}
		break;
	case BD_ADAPTATION:
		if(DIFF_TICK(sd->K_CHK_BD_ADAPTATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BD_ADAPTATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BD_ADAPTATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BD_ADAPTATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BD_ADAPTATION = gettick() + battle_config.WOE_K_DELAY_BD_ADAPTATION;
				sd->state.K_LOCK_BD_ADAPTATION = 0;
		} else {
			sd->K_CHK_BD_ADAPTATION = gettick() + battle_config.K_DELAY_BD_ADAPTATION;
			sd->state.K_LOCK_BD_ADAPTATION = 0;
		}
		break;
	case BD_ENCORE:
		if(DIFF_TICK(sd->K_CHK_BD_ENCORE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BD_ENCORE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BD_ENCORE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BD_ENCORE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BD_ENCORE = gettick() + battle_config.WOE_K_DELAY_BD_ENCORE;
				sd->state.K_LOCK_BD_ENCORE = 0;
		} else {
			sd->K_CHK_BD_ENCORE = gettick() + battle_config.K_DELAY_BD_ENCORE;
			sd->state.K_LOCK_BD_ENCORE = 0;
		}
		break;
	case BD_LULLABY:
		if(DIFF_TICK(sd->K_CHK_BD_LULLABY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BD_LULLABY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BD_LULLABY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BD_LULLABY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BD_LULLABY = gettick() + battle_config.WOE_K_DELAY_BD_LULLABY;
				sd->state.K_LOCK_BD_LULLABY = 0;
		} else {
			sd->K_CHK_BD_LULLABY = gettick() + battle_config.K_DELAY_BD_LULLABY;
			sd->state.K_LOCK_BD_LULLABY = 0;
		}
		break;
	case BD_RICHMANKIM:
		if(DIFF_TICK(sd->K_CHK_BD_RICHMANKIM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BD_RICHMANKIM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BD_RICHMANKIM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BD_RICHMANKIM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BD_RICHMANKIM = gettick() + battle_config.WOE_K_DELAY_BD_RICHMANKIM;
				sd->state.K_LOCK_BD_RICHMANKIM = 0;
		} else {
			sd->K_CHK_BD_RICHMANKIM = gettick() + battle_config.K_DELAY_BD_RICHMANKIM;
			sd->state.K_LOCK_BD_RICHMANKIM = 0;
		}
		break;
	case BA_MUSICALSTRIKE:
		if(DIFF_TICK(sd->K_CHK_BA_MUSICALSTRIKE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BA_MUSICALSTRIKE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BA_MUSICALSTRIKE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BA_MUSICALSTRIKE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BA_MUSICALSTRIKE = gettick() + battle_config.WOE_K_DELAY_BA_MUSICALSTRIKE;
				sd->state.K_LOCK_BA_MUSICALSTRIKE = 0;
		} else {
			sd->K_CHK_BA_MUSICALSTRIKE = gettick() + battle_config.K_DELAY_BA_MUSICALSTRIKE;
			sd->state.K_LOCK_BA_MUSICALSTRIKE = 0;
		}
		break;
	case BA_DISSONANCE:
		if(DIFF_TICK(sd->K_CHK_BA_DISSONANCE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BA_DISSONANCE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BA_DISSONANCE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BA_DISSONANCE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BA_DISSONANCE = gettick() + battle_config.WOE_K_DELAY_BA_DISSONANCE;
				sd->state.K_LOCK_BA_DISSONANCE = 0;
		} else {
			sd->K_CHK_BA_DISSONANCE = gettick() + battle_config.K_DELAY_BA_DISSONANCE;
			sd->state.K_LOCK_BA_DISSONANCE = 0;
		}
		break;
	case BA_FROSTJOKER:
		if(DIFF_TICK(sd->K_CHK_BA_FROSTJOKER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BA_FROSTJOKER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BA_FROSTJOKER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BA_FROSTJOKER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BA_FROSTJOKER = gettick() + battle_config.WOE_K_DELAY_BA_FROSTJOKER;
				sd->state.K_LOCK_BA_FROSTJOKER = 0;
		} else {
			sd->K_CHK_BA_FROSTJOKER = gettick() + battle_config.K_DELAY_BA_FROSTJOKER;
			sd->state.K_LOCK_BA_FROSTJOKER = 0;
		}
		break;
	case BA_WHISTLE:
		if(DIFF_TICK(sd->K_CHK_BA_WHISTLE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BA_WHISTLE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BA_WHISTLE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BA_WHISTLE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BA_WHISTLE = gettick() + battle_config.WOE_K_DELAY_BA_WHISTLE;
				sd->state.K_LOCK_BA_WHISTLE = 0;
		} else {
			sd->K_CHK_BA_WHISTLE = gettick() + battle_config.K_DELAY_BA_WHISTLE;
			sd->state.K_LOCK_BA_WHISTLE = 0;
		}
		break;
	case BA_ASSASSINCROSS:
		if(DIFF_TICK(sd->K_CHK_BA_ASSASSINCROSS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BA_ASSASSINCROSS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BA_ASSASSINCROSS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BA_ASSASSINCROSS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BA_ASSASSINCROSS = gettick() + battle_config.WOE_K_DELAY_BA_ASSASSINCROSS;
				sd->state.K_LOCK_BA_ASSASSINCROSS = 0;
		} else {
			sd->K_CHK_BA_ASSASSINCROSS = gettick() + battle_config.K_DELAY_BA_ASSASSINCROSS;
			sd->state.K_LOCK_BA_ASSASSINCROSS = 0;
		}
		break;
	case BA_POEMBRAGI:
		if(DIFF_TICK(sd->K_CHK_BA_POEMBRAGI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BA_POEMBRAGI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BA_POEMBRAGI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BA_POEMBRAGI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BA_POEMBRAGI = gettick() + battle_config.WOE_K_DELAY_BA_POEMBRAGI;
				sd->state.K_LOCK_BA_POEMBRAGI = 0;
		} else {
			sd->K_CHK_BA_POEMBRAGI = gettick() + battle_config.K_DELAY_BA_POEMBRAGI;
			sd->state.K_LOCK_BA_POEMBRAGI = 0;
		}
		break;
	case BA_APPLEIDUN:
		if(DIFF_TICK(sd->K_CHK_BA_APPLEIDUN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BA_APPLEIDUN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BA_APPLEIDUN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BA_APPLEIDUN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BA_APPLEIDUN = gettick() + battle_config.WOE_K_DELAY_BA_APPLEIDUN;
				sd->state.K_LOCK_BA_APPLEIDUN = 0;
		} else {
			sd->K_CHK_BA_APPLEIDUN = gettick() + battle_config.K_DELAY_BA_APPLEIDUN;
			sd->state.K_LOCK_BA_APPLEIDUN = 0;
		}
		break;
	case DC_THROWARROW:
		if(DIFF_TICK(sd->K_CHK_DC_THROWARROW,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_DC_THROWARROW,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_DC_THROWARROW - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_DC_THROWARROW = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_DC_THROWARROW = gettick() + battle_config.WOE_K_DELAY_DC_THROWARROW;
				sd->state.K_LOCK_DC_THROWARROW = 0;
		} else {
			sd->K_CHK_DC_THROWARROW = gettick() + battle_config.K_DELAY_DC_THROWARROW;
			sd->state.K_LOCK_DC_THROWARROW = 0;
		}
		break;
	case DC_UGLYDANCE:
		if(DIFF_TICK(sd->K_CHK_DC_UGLYDANCE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_DC_UGLYDANCE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_DC_UGLYDANCE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_DC_UGLYDANCE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_DC_UGLYDANCE = gettick() + battle_config.WOE_K_DELAY_DC_UGLYDANCE;
				sd->state.K_LOCK_DC_UGLYDANCE = 0;
		} else {
			sd->K_CHK_DC_UGLYDANCE = gettick() + battle_config.K_DELAY_DC_UGLYDANCE;
			sd->state.K_LOCK_DC_UGLYDANCE = 0;
		}
		break;
	case DC_SCREAM:
		if(DIFF_TICK(sd->K_CHK_DC_SCREAM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_DC_SCREAM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_DC_SCREAM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_DC_SCREAM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_DC_SCREAM = gettick() + battle_config.WOE_K_DELAY_DC_SCREAM;
				sd->state.K_LOCK_DC_SCREAM = 0;
		} else {
			sd->K_CHK_DC_SCREAM = gettick() + battle_config.K_DELAY_DC_SCREAM;
			sd->state.K_LOCK_DC_SCREAM = 0;
		}
		break;
	case DC_HUMMING:
		if(DIFF_TICK(sd->K_CHK_DC_HUMMING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_DC_HUMMING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_DC_HUMMING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_DC_HUMMING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_DC_HUMMING = gettick() + battle_config.WOE_K_DELAY_DC_HUMMING;
				sd->state.K_LOCK_DC_HUMMING = 0;
		} else {
			sd->K_CHK_DC_HUMMING = gettick() + battle_config.K_DELAY_DC_HUMMING;
			sd->state.K_LOCK_DC_HUMMING = 0;
		}
		break;
	case DC_DONTFORGETME:
		if(DIFF_TICK(sd->K_CHK_DC_DONTFORGETME,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_DC_DONTFORGETME,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_DC_DONTFORGETME - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_DC_DONTFORGETME = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_DC_DONTFORGETME = gettick() + battle_config.WOE_K_DELAY_DC_DONTFORGETME;
				sd->state.K_LOCK_DC_DONTFORGETME = 0;
		} else {
			sd->K_CHK_DC_DONTFORGETME = gettick() + battle_config.K_DELAY_DC_DONTFORGETME;
			sd->state.K_LOCK_DC_DONTFORGETME = 0;
		}
		break;
	case DC_FORTUNEKISS:
		if(DIFF_TICK(sd->K_CHK_DC_FORTUNEKISS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_DC_FORTUNEKISS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_DC_FORTUNEKISS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_DC_FORTUNEKISS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_DC_FORTUNEKISS = gettick() + battle_config.WOE_K_DELAY_DC_FORTUNEKISS;
				sd->state.K_LOCK_DC_FORTUNEKISS = 0;
		} else {
			sd->K_CHK_DC_FORTUNEKISS = gettick() + battle_config.K_DELAY_DC_FORTUNEKISS;
			sd->state.K_LOCK_DC_FORTUNEKISS = 0;
		}
		break;
	case DC_SERVICEFORYOU:
		if(DIFF_TICK(sd->K_CHK_DC_SERVICEFORYOU,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_DC_SERVICEFORYOU,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_DC_SERVICEFORYOU - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_DC_SERVICEFORYOU = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_DC_SERVICEFORYOU = gettick() + battle_config.WOE_K_DELAY_DC_SERVICEFORYOU;
				sd->state.K_LOCK_DC_SERVICEFORYOU = 0;
		} else {
			sd->K_CHK_DC_SERVICEFORYOU = gettick() + battle_config.K_DELAY_DC_SERVICEFORYOU;
			sd->state.K_LOCK_DC_SERVICEFORYOU = 0;
		}
		break;
	case LK_FURY:
		if(DIFF_TICK(sd->K_CHK_LK_FURY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LK_FURY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LK_FURY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LK_FURY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LK_FURY = gettick() + battle_config.WOE_K_DELAY_LK_FURY;
				sd->state.K_LOCK_LK_FURY = 0;
		} else {
			sd->K_CHK_LK_FURY = gettick() + battle_config.K_DELAY_LK_FURY;
			sd->state.K_LOCK_LK_FURY = 0;
		}
		break;
	case HW_MAGICCRASHER:
		if(DIFF_TICK(sd->K_CHK_HW_MAGICCRASHER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_HW_MAGICCRASHER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_HW_MAGICCRASHER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_HW_MAGICCRASHER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_HW_MAGICCRASHER = gettick() + battle_config.WOE_K_DELAY_HW_MAGICCRASHER;
				sd->state.K_LOCK_HW_MAGICCRASHER = 0;
		} else {
			sd->K_CHK_HW_MAGICCRASHER = gettick() + battle_config.K_DELAY_HW_MAGICCRASHER;
			sd->state.K_LOCK_HW_MAGICCRASHER = 0;
		}
		break;
	case PA_PRESSURE:
		if(DIFF_TICK(sd->K_CHK_PA_PRESSURE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PA_PRESSURE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PA_PRESSURE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PA_PRESSURE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PA_PRESSURE = gettick() + battle_config.WOE_K_DELAY_PA_PRESSURE;
				sd->state.K_LOCK_PA_PRESSURE = 0;
		} else {
			sd->K_CHK_PA_PRESSURE = gettick() + battle_config.K_DELAY_PA_PRESSURE;
			sd->state.K_LOCK_PA_PRESSURE = 0;
		}
		break;
	case CH_PALMSTRIKE:
		if(DIFF_TICK(sd->K_CHK_CH_PALMSTRIKE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CH_PALMSTRIKE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CH_PALMSTRIKE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CH_PALMSTRIKE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CH_PALMSTRIKE = gettick() + battle_config.WOE_K_DELAY_CH_PALMSTRIKE;
				sd->state.K_LOCK_CH_PALMSTRIKE = 0;
		} else {
			sd->K_CHK_CH_PALMSTRIKE = gettick() + battle_config.K_DELAY_CH_PALMSTRIKE;
			sd->state.K_LOCK_CH_PALMSTRIKE = 0;
		}
		break;
	case CH_TIGERFIST:
		if(DIFF_TICK(sd->K_CHK_CH_TIGERFIST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CH_TIGERFIST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CH_TIGERFIST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CH_TIGERFIST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CH_TIGERFIST = gettick() + battle_config.WOE_K_DELAY_CH_TIGERFIST;
				sd->state.K_LOCK_CH_TIGERFIST = 0;
		} else {
			sd->K_CHK_CH_TIGERFIST = gettick() + battle_config.K_DELAY_CH_TIGERFIST;
			sd->state.K_LOCK_CH_TIGERFIST = 0;
		}
		break;
	case CH_CHAINCRUSH:
		if(DIFF_TICK(sd->K_CHK_CH_CHAINCRUSH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CH_CHAINCRUSH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CH_CHAINCRUSH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CH_CHAINCRUSH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CH_CHAINCRUSH = gettick() + battle_config.WOE_K_DELAY_CH_CHAINCRUSH;
				sd->state.K_LOCK_CH_CHAINCRUSH = 0;
		} else {
			sd->K_CHK_CH_CHAINCRUSH = gettick() + battle_config.K_DELAY_CH_CHAINCRUSH;
			sd->state.K_LOCK_CH_CHAINCRUSH = 0;
		}
		break;
	case PF_SOULCHANGE:
		if(DIFF_TICK(sd->K_CHK_PF_SOULCHANGE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PF_SOULCHANGE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PF_SOULCHANGE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PF_SOULCHANGE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PF_SOULCHANGE = gettick() + battle_config.WOE_K_DELAY_PF_SOULCHANGE;
				sd->state.K_LOCK_PF_SOULCHANGE = 0;
		} else {
			sd->K_CHK_PF_SOULCHANGE = gettick() + battle_config.K_DELAY_PF_SOULCHANGE;
			sd->state.K_LOCK_PF_SOULCHANGE = 0;
		}
		break;
	case PF_SOULBURN:
		if(DIFF_TICK(sd->K_CHK_PF_SOULBURN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PF_SOULBURN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PF_SOULBURN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PF_SOULBURN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PF_SOULBURN = gettick() + battle_config.WOE_K_DELAY_PF_SOULBURN;
				sd->state.K_LOCK_PF_SOULBURN = 0;
		} else {
			sd->K_CHK_PF_SOULBURN = gettick() + battle_config.K_DELAY_PF_SOULBURN;
			sd->state.K_LOCK_PF_SOULBURN = 0;
		}
		break;
	case ASC_BREAKER:
		if(DIFF_TICK(sd->K_CHK_ASC_BREAKER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_ASC_BREAKER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_ASC_BREAKER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_ASC_BREAKER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_ASC_BREAKER = gettick() + battle_config.WOE_K_DELAY_ASC_BREAKER;
				sd->state.K_LOCK_ASC_BREAKER = 0;
		} else {
			sd->K_CHK_ASC_BREAKER = gettick() + battle_config.K_DELAY_ASC_BREAKER;
			sd->state.K_LOCK_ASC_BREAKER = 0;
		}
		break;
	case SN_FALCONASSAULT:
		if(DIFF_TICK(sd->K_CHK_SN_FALCONASSAULT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SN_FALCONASSAULT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SN_FALCONASSAULT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SN_FALCONASSAULT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SN_FALCONASSAULT = gettick() + battle_config.WOE_K_DELAY_SN_FALCONASSAULT;
				sd->state.K_LOCK_SN_FALCONASSAULT = 0;
		} else {
			sd->K_CHK_SN_FALCONASSAULT = gettick() + battle_config.K_DELAY_SN_FALCONASSAULT;
			sd->state.K_LOCK_SN_FALCONASSAULT = 0;
		}
		break;
	case SN_SHARPSHOOTING:
		if(DIFF_TICK(sd->K_CHK_SN_SHARPSHOOTING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SN_SHARPSHOOTING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SN_SHARPSHOOTING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SN_SHARPSHOOTING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SN_SHARPSHOOTING = gettick() + battle_config.WOE_K_DELAY_SN_SHARPSHOOTING;
				sd->state.K_LOCK_SN_SHARPSHOOTING = 0;
		} else {
			sd->K_CHK_SN_SHARPSHOOTING = gettick() + battle_config.K_DELAY_SN_SHARPSHOOTING;
			sd->state.K_LOCK_SN_SHARPSHOOTING = 0;
		}
		break;
	case CR_ALCHEMY:
		if(DIFF_TICK(sd->K_CHK_CR_ALCHEMY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CR_ALCHEMY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CR_ALCHEMY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CR_ALCHEMY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CR_ALCHEMY = gettick() + battle_config.WOE_K_DELAY_CR_ALCHEMY;
				sd->state.K_LOCK_CR_ALCHEMY = 0;
		} else {
			sd->K_CHK_CR_ALCHEMY = gettick() + battle_config.K_DELAY_CR_ALCHEMY;
			sd->state.K_LOCK_CR_ALCHEMY = 0;
		}
		break;
	case CR_SYNTHESISPOTION:
		if(DIFF_TICK(sd->K_CHK_CR_SYNTHESIS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CR_SYNTHESIS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CR_SYNTHESIS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CR_SYNTHESIS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CR_SYNTHESIS = gettick() + battle_config.WOE_K_DELAY_CR_SYNTHESIS;
				sd->state.K_LOCK_CR_SYNTHESIS = 0;
		} else {
			sd->K_CHK_CR_SYNTHESIS = gettick() + battle_config.K_DELAY_CR_SYNTHESISPOTION;
			sd->state.K_LOCK_CR_SYNTHESIS = 0;
		}
		break;
	case CG_ARROWVULCAN:
		if(DIFF_TICK(sd->K_CHK_CG_ARROWVULCAN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CG_ARROWVULCAN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CG_ARROWVULCAN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CG_ARROWVULCAN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CG_ARROWVULCAN = gettick() + battle_config.WOE_K_DELAY_CG_ARROWVULCAN;
				sd->state.K_LOCK_CG_ARROWVULCAN = 0;
		} else {
			sd->K_CHK_CG_ARROWVULCAN = gettick() + battle_config.K_DELAY_CG_ARROWVULCAN;
			sd->state.K_LOCK_CG_ARROWVULCAN = 0;
		}
		break;
	case CG_MOONLIT:
		if(DIFF_TICK(sd->K_CHK_CG_MOONLIT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CG_MOONLIT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CG_MOONLIT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CG_MOONLIT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CG_MOONLIT = gettick() + battle_config.WOE_K_DELAY_CG_MOONLIT;
				sd->state.K_LOCK_CG_MOONLIT = 0;
		} else {
			sd->K_CHK_CG_MOONLIT = gettick() + battle_config.K_DELAY_CG_MOONLIT;
			sd->state.K_LOCK_CG_MOONLIT = 0;
		}
		break;
	case CG_MARIONETTE:
		if(DIFF_TICK(sd->K_CHK_CG_MARIONETTE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CG_MARIONETTE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CG_MARIONETTE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CG_MARIONETTE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CG_MARIONETTE = gettick() + battle_config.WOE_K_DELAY_CG_MARIONETTE;
				sd->state.K_LOCK_CG_MARIONETTE = 0;
		} else {
			sd->K_CHK_CG_MARIONETTE = gettick() + battle_config.K_DELAY_CG_MARIONETTE;
			sd->state.K_LOCK_CG_MARIONETTE = 0;
		}
		break;
	case LK_SPIRALPIERCE:
		if(DIFF_TICK(sd->K_CHK_LK_SPIRALPIERCE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LK_SPIRALPIERCE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LK_SPIRALPIERCE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LK_SPIRALPIERCE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LK_SPIRALPIERCE = gettick() + battle_config.WOE_K_DELAY_LK_SPIRALPIERCE;
				sd->state.K_LOCK_LK_SPIRALPIERCE = 0;
		} else {
			sd->K_CHK_LK_SPIRALPIERCE = gettick() + battle_config.K_DELAY_LK_SPIRALPIERCE;
			sd->state.K_LOCK_LK_SPIRALPIERCE = 0;
		}
		break;
	case LK_HEADCRUSH:
		if(DIFF_TICK(sd->K_CHK_LK_HEADCRUSH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LK_HEADCRUSH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LK_HEADCRUSH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LK_HEADCRUSH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LK_HEADCRUSH = gettick() + battle_config.WOE_K_DELAY_LK_HEADCRUSH;
				sd->state.K_LOCK_LK_HEADCRUSH = 0;
		} else {
			sd->K_CHK_LK_HEADCRUSH = gettick() + battle_config.K_DELAY_LK_HEADCRUSH;
			sd->state.K_LOCK_LK_HEADCRUSH = 0;
		}
		break;
	case LK_JOINTBEAT:
		if(DIFF_TICK(sd->K_CHK_LK_JOINTBEAT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LK_JOINTBEAT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LK_JOINTBEAT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LK_JOINTBEAT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LK_JOINTBEAT = gettick() + battle_config.WOE_K_DELAY_LK_JOINTBEAT;
				sd->state.K_LOCK_LK_JOINTBEAT = 0;
		} else {
			sd->K_CHK_LK_JOINTBEAT = gettick() + battle_config.K_DELAY_LK_JOINTBEAT;
			sd->state.K_LOCK_LK_JOINTBEAT = 0;
		}
		break;
	case HW_NAPALMVULCAN:
		if(DIFF_TICK(sd->K_CHK_HW_NAPALMVULCAN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_HW_NAPALMVULCAN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_HW_NAPALMVULCAN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_HW_NAPALMVULCAN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_HW_NAPALMVULCAN = gettick() + battle_config.WOE_K_DELAY_HW_NAPALMVULCAN;
				sd->state.K_LOCK_HW_NAPALMVULCAN = 0;
		} else {
			sd->K_CHK_HW_NAPALMVULCAN = gettick() + battle_config.K_DELAY_HW_NAPALMVULCAN;
			sd->state.K_LOCK_HW_NAPALMVULCAN = 0;
		}
		break;
	case CH_SOULCOLLECT:
		if(DIFF_TICK(sd->K_CHK_CH_SOULCOLLECT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CH_SOULCOLLECT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CH_SOULCOLLECT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CH_SOULCOLLECT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CH_SOULCOLLECT = gettick() + battle_config.WOE_K_DELAY_CH_SOULCOLLECT;
				sd->state.K_LOCK_CH_SOULCOLLECT = 0;
		} else {
			sd->K_CHK_CH_SOULCOLLECT = gettick() + battle_config.K_DELAY_CH_SOULCOLLECT;
			sd->state.K_LOCK_CH_SOULCOLLECT = 0;
		}
		break;
	case PF_MINDBREAKER:
		if(DIFF_TICK(sd->K_CHK_PF_MINDBREAKER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PF_MINDBREAKER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PF_MINDBREAKER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PF_MINDBREAKER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PF_MINDBREAKER = gettick() + battle_config.WOE_K_DELAY_PF_MINDBREAKER;
				sd->state.K_LOCK_PF_MINDBREAKER = 0;
		} else {
			sd->K_CHK_PF_MINDBREAKER = gettick() + battle_config.K_DELAY_PF_MINDBREAKER;
			sd->state.K_LOCK_PF_MINDBREAKER = 0;
		}
		break;
	case PF_SPIDERWEB:
		if(DIFF_TICK(sd->K_CHK_PF_SPIDERWEB,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PF_SPIDERWEB,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PF_SPIDERWEB - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PF_SPIDERWEB = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PF_SPIDERWEB = gettick() + battle_config.WOE_K_DELAY_PF_SPIDERWEB;
				sd->state.K_LOCK_PF_SPIDERWEB = 0;
		} else {
			sd->K_CHK_PF_SPIDERWEB = gettick() + battle_config.K_DELAY_PF_SPIDERWEB;
			sd->state.K_LOCK_PF_SPIDERWEB = 0;
		}
		break;
	case ASC_METEORASSAULT:
		if(DIFF_TICK(sd->K_CHK_ASC_METEORASSAULT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_ASC_METEORASSAULT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_ASC_METEORASSAULT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_ASC_METEORASSAULT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_ASC_METEORASSAULT = gettick() + battle_config.WOE_K_DELAY_ASC_METEORASSAULT;
				sd->state.K_LOCK_ASC_METEORASSAULT = 0;
		} else {
			sd->K_CHK_ASC_METEORASSAULT = gettick() + battle_config.K_DELAY_ASC_METEORASSAULT;
			sd->state.K_LOCK_ASC_METEORASSAULT = 0;
		}
		break;
	case TK_STORMKICK:
		if(DIFF_TICK(sd->K_CHK_TK_STORMKICK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_TK_STORMKICK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_TK_STORMKICK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_TK_STORMKICK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_TK_STORMKICK = gettick() + battle_config.WOE_K_DELAY_TK_STORMKICK;
				sd->state.K_LOCK_TK_STORMKICK = 0;
		} else {
			sd->K_CHK_TK_STORMKICK = gettick() + battle_config.K_DELAY_TK_STORMKICK;
			sd->state.K_LOCK_TK_STORMKICK = 0;
		}
		break;
	case TK_DOWNKICK:
		if(DIFF_TICK(sd->K_CHK_TK_DOWNKICK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_TK_DOWNKICK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_TK_DOWNKICK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_TK_DOWNKICK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_TK_DOWNKICK = gettick() + battle_config.WOE_K_DELAY_TK_DOWNKICK;
				sd->state.K_LOCK_TK_DOWNKICK = 0;
		} else {
			sd->K_CHK_TK_DOWNKICK = gettick() + battle_config.K_DELAY_TK_DOWNKICK;
			sd->state.K_LOCK_TK_DOWNKICK = 0;
		}
		break;
	case TK_TURNKICK:
		if(DIFF_TICK(sd->K_CHK_TK_TURNKICK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_TK_TURNKICK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_TK_TURNKICK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_TK_TURNKICK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_TK_TURNKICK = gettick() + battle_config.WOE_K_DELAY_TK_TURNKICK;
				sd->state.K_LOCK_TK_TURNKICK = 0;
		} else {
			sd->K_CHK_TK_TURNKICK = gettick() + battle_config.K_DELAY_TK_TURNKICK;
			sd->state.K_LOCK_TK_TURNKICK = 0;
		}
		break;
	case TK_JUMPKICK:
		if(DIFF_TICK(sd->K_CHK_TK_JUMPKICK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_TK_JUMPKICK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_TK_JUMPKICK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_TK_JUMPKICK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_TK_JUMPKICK = gettick() + battle_config.WOE_K_DELAY_TK_JUMPKICK;
				sd->state.K_LOCK_TK_JUMPKICK = 0;
		} else {
			sd->K_CHK_TK_JUMPKICK = gettick() + battle_config.K_DELAY_TK_JUMPKICK;
			sd->state.K_LOCK_TK_JUMPKICK = 0;
		}
		break;
	case TK_POWER:
		if(DIFF_TICK(sd->K_CHK_TK_POWER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_TK_POWER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_TK_POWER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_TK_POWER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_TK_POWER = gettick() + battle_config.WOE_K_DELAY_TK_POWER;
				sd->state.K_LOCK_TK_POWER = 0;
		} else {
			sd->K_CHK_TK_POWER = gettick() + battle_config.K_DELAY_TK_POWER;
			sd->state.K_LOCK_TK_POWER = 0;
		}
		break;
	case TK_HIGHJUMP:
		if(DIFF_TICK(sd->K_CHK_TK_HIGHJUMP,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_TK_HIGHJUMP,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_TK_HIGHJUMP - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_TK_HIGHJUMP = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_TK_HIGHJUMP = gettick() + battle_config.WOE_K_DELAY_TK_HIGHJUMP;
				sd->state.K_LOCK_TK_HIGHJUMP = 0;
		} else {
			sd->K_CHK_TK_HIGHJUMP = gettick() + battle_config.K_DELAY_TK_HIGHJUMP;
			sd->state.K_LOCK_TK_HIGHJUMP = 0;
		}
		break;
	case SL_KAIZEL:
		if(DIFF_TICK(sd->K_CHK_SL_KAIZEL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_KAIZEL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_KAIZEL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_KAIZEL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_KAIZEL = gettick() + battle_config.WOE_K_DELAY_SL_KAIZEL;
				sd->state.K_LOCK_SL_KAIZEL = 0;
		} else {
			sd->K_CHK_SL_KAIZEL = gettick() + battle_config.K_DELAY_SL_KAIZEL;
			sd->state.K_LOCK_SL_KAIZEL = 0;
		}
		break;
	case SL_KAAHI:
		if(DIFF_TICK(sd->K_CHK_SL_KAAHI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_KAAHI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_KAAHI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_KAAHI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_KAAHI = gettick() + battle_config.WOE_K_DELAY_SL_KAAHI;
				sd->state.K_LOCK_SL_KAAHI = 0;
		} else {
			sd->K_CHK_SL_KAAHI = gettick() + battle_config.K_DELAY_SL_KAAHI;
			sd->state.K_LOCK_SL_KAAHI = 0;
		}
		break;
	case SL_KAUPE:
		if(DIFF_TICK(sd->K_CHK_SL_KAUPE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_KAUPE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_KAUPE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_KAUPE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_KAUPE = gettick() + battle_config.WOE_K_DELAY_SL_KAUPE;
				sd->state.K_LOCK_SL_KAUPE = 0;
		} else {
			sd->K_CHK_SL_KAUPE = gettick() + battle_config.K_DELAY_SL_KAUPE;
			sd->state.K_LOCK_SL_KAUPE = 0;
		}
		break;
	case SL_KAITE:
		if(DIFF_TICK(sd->K_CHK_SL_KAITE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_KAITE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_KAITE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_KAITE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_KAITE = gettick() + battle_config.WOE_K_DELAY_SL_KAITE;
				sd->state.K_LOCK_SL_KAITE = 0;
		} else {
			sd->K_CHK_SL_KAITE = gettick() + battle_config.K_DELAY_SL_KAITE;
			sd->state.K_LOCK_SL_KAITE = 0;
		}
		break;
	case SL_KAINA:
		if(DIFF_TICK(sd->K_CHK_SL_KAINA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_KAINA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_KAINA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_KAINA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_KAINA = gettick() + battle_config.WOE_K_DELAY_SL_KAINA;
				sd->state.K_LOCK_SL_KAINA = 0;
		} else {
			sd->K_CHK_SL_KAINA = gettick() + battle_config.K_DELAY_SL_KAINA;
			sd->state.K_LOCK_SL_KAINA = 0;
		}
		break;
	case SL_STIN:
		if(DIFF_TICK(sd->K_CHK_SL_STIN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_STIN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_STIN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_STIN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_STIN = gettick() + battle_config.WOE_K_DELAY_SL_STIN;
				sd->state.K_LOCK_SL_STIN = 0;
		} else {
			sd->K_CHK_SL_STIN = gettick() + battle_config.K_DELAY_SL_STIN;
			sd->state.K_LOCK_SL_STIN = 0;
		}
		break;
	case SL_STUN:
		if(DIFF_TICK(sd->K_CHK_SL_STUN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_STUN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_STUN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_STUN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_STUN = gettick() + battle_config.WOE_K_DELAY_SL_STUN;
				sd->state.K_LOCK_SL_STUN = 0;
		} else {
			sd->K_CHK_SL_STUN = gettick() + battle_config.K_DELAY_SL_STUN;
			sd->state.K_LOCK_SL_STUN = 0;
		}
		break;
	case SL_SMA:
		if(DIFF_TICK(sd->K_CHK_SL_SMA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_SMA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_SMA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_SMA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_SMA = gettick() + battle_config.WOE_K_DELAY_SL_SMA;
				sd->state.K_LOCK_SL_SMA = 0;
		} else {
			sd->K_CHK_SL_SMA = gettick() + battle_config.K_DELAY_SL_SMA;
			sd->state.K_LOCK_SL_SMA = 0;
		}
		break;
	case SL_SWOO:
		if(DIFF_TICK(sd->K_CHK_SL_SWOO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_SWOO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_SWOO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_SWOO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_SWOO = gettick() + battle_config.WOE_K_DELAY_SL_SWOO;
				sd->state.K_LOCK_SL_SWOO = 0;
		} else {
			sd->K_CHK_SL_SWOO = gettick() + battle_config.K_DELAY_SL_SWOO;
			sd->state.K_LOCK_SL_SWOO = 0;
		}
		break;
	case SL_SKE:
		if(DIFF_TICK(sd->K_CHK_SL_SKE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_SKE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_SKE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_SKE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_SKE = gettick() + battle_config.WOE_K_DELAY_SL_SKE;
				sd->state.K_LOCK_SL_SKE = 0;
		} else {
			sd->K_CHK_SL_SKE = gettick() + battle_config.K_DELAY_SL_SKE;
			sd->state.K_LOCK_SL_SKE = 0;
		}
		break;
	case SL_SKA:
		if(DIFF_TICK(sd->K_CHK_SL_SKA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_SKA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_SKA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_SKA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_SKA = gettick() + battle_config.WOE_K_DELAY_SL_SKA;
				sd->state.K_LOCK_SL_SKA = 0;
		} else {
			sd->K_CHK_SL_SKA = gettick() + battle_config.K_DELAY_SL_SKA;
			sd->state.K_LOCK_SL_SKA = 0;
		}
		break;
	case ST_FULLSTRIP:
		if(DIFF_TICK(sd->K_CHK_ST_FULLSTRIP,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_ST_FULLSTRIP,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_ST_FULLSTRIP - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_ST_FULLSTRIP = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_ST_FULLSTRIP = gettick() + battle_config.WOE_K_DELAY_ST_FULLSTRIP;
				sd->state.K_LOCK_ST_FULLSTRIP = 0;
		} else {
			sd->K_CHK_ST_FULLSTRIP = gettick() + battle_config.K_DELAY_ST_FULLSTRIP;
			sd->state.K_LOCK_ST_FULLSTRIP = 0;
		}
		break;
	case CR_SLIMPITCHER:
		if(DIFF_TICK(sd->K_CHK_CR_SLIMPITCHER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CR_SLIMPITCHER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CR_SLIMPITCHER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CR_SLIMPITCHER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CR_SLIMPITCHER = gettick() + battle_config.WOE_K_DELAY_CR_SLIMPITCHER;
				sd->state.K_LOCK_CR_SLIMPITCHER = 0;
		} else {
			sd->K_CHK_CR_SLIMPITCHER = gettick() + battle_config.K_DELAY_CR_SLIMPITCHER;
			sd->state.K_LOCK_CR_SLIMPITCHER = 0;
		}
		break;
	case CR_FULLPROTECTION:
		if(DIFF_TICK(sd->K_CHK_CR_FULLPROTECTION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CR_FULLPROTECTION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CR_FULLPROTECTION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CR_FULLPROTECTION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CR_FULLPROTECTION = gettick() + battle_config.WOE_K_DELAY_CR_FULLPROTECTION;
				sd->state.K_LOCK_CR_FULLPROTECTION = 0;
		} else {
			sd->K_CHK_CR_FULLPROTECTION = gettick() + battle_config.K_DELAY_CR_FULLPROTECTION;
			sd->state.K_LOCK_CR_FULLPROTECTION = 0;
		}
		break;
	case PA_SHIELDCHAIN:
		if(DIFF_TICK(sd->K_CHK_PA_SHIELDCHAIN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PA_SHIELDCHAIN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PA_SHIELDCHAIN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PA_SHIELDCHAIN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PA_SHIELDCHAIN = gettick() + battle_config.WOE_K_DELAY_PA_SHIELDCHAIN;
				sd->state.K_LOCK_PA_SHIELDCHAIN = 0;
		} else {
			sd->K_CHK_PA_SHIELDCHAIN = gettick() + battle_config.K_DELAY_PA_SHIELDCHAIN;
			sd->state.K_LOCK_PA_SHIELDCHAIN = 0;
		}
		break;
	case HP_MANARECHARGE:
		if(DIFF_TICK(sd->K_CHK_HP_MANARECHARGE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_HP_MANARECHARGE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_HP_MANARECHARGE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_HP_MANARECHARGE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_HP_MANARECHARGE = gettick() + battle_config.WOE_K_DELAY_HP_MANARECHARGE;
				sd->state.K_LOCK_HP_MANARECHARGE = 0;
		} else {
			sd->K_CHK_HP_MANARECHARGE = gettick() + battle_config.K_DELAY_HP_MANARECHARGE;
			sd->state.K_LOCK_HP_MANARECHARGE = 0;
		}
		break;
	case PF_DOUBLECASTING:
		if(DIFF_TICK(sd->K_CHK_PF_DOUBLECASTING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PF_DOUBLECASTING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PF_DOUBLECASTING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PF_DOUBLECASTING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PF_DOUBLECASTING = gettick() + battle_config.WOE_K_DELAY_PF_DOUBLECASTING;
				sd->state.K_LOCK_PF_DOUBLECASTING = 0;
		} else {
			sd->K_CHK_PF_DOUBLECASTING = gettick() + battle_config.K_DELAY_PF_DOUBLECASTING;
			sd->state.K_LOCK_PF_DOUBLECASTING = 0;
		}
		break;
	case HW_GANBANTEIN:
		if(DIFF_TICK(sd->K_CHK_HW_GANBANTEIN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_HW_GANBANTEIN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_HW_GANBANTEIN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_HW_GANBANTEIN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_HW_GANBANTEIN = gettick() + battle_config.WOE_K_DELAY_HW_GANBANTEIN;
				sd->state.K_LOCK_HW_GANBANTEIN = 0;
		} else {
			sd->K_CHK_HW_GANBANTEIN = gettick() + battle_config.K_DELAY_HW_GANBANTEIN;
			sd->state.K_LOCK_HW_GANBANTEIN = 0;
		}
		break;
	case HW_GRAVITATION:
		if(DIFF_TICK(sd->K_CHK_HW_GRAVITATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_HW_GRAVITATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_HW_GRAVITATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_HW_GRAVITATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_HW_GRAVITATION = gettick() + battle_config.WOE_K_DELAY_HW_GRAVITATION;
				sd->state.K_LOCK_HW_GRAVITATION = 0;
		} else {
			sd->K_CHK_HW_GRAVITATION = gettick() + battle_config.K_DELAY_HW_GRAVITATION;
			sd->state.K_LOCK_HW_GRAVITATION = 0;
		}
		break;
	case WS_CARTTERMINATION:
		if(DIFF_TICK(sd->K_CHK_WS_CARTTERMINATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WS_CARTTERMINATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WS_CARTTERMINATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WS_CARTTERMINATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WS_CARTTERMINATION = gettick() + battle_config.WOE_K_DELAY_WS_CARTTERMINATION;
				sd->state.K_LOCK_WS_CARTTERMINATION = 0;
		} else {
			sd->K_CHK_WS_CARTTERMINATION = gettick() + battle_config.K_DELAY_WS_CARTTERMINATION;
			sd->state.K_LOCK_WS_CARTTERMINATION = 0;
		}
		break;
	case CG_HERMODE:
		if(DIFF_TICK(sd->K_CHK_CG_HERMODE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CG_HERMODE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CG_HERMODE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CG_HERMODE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CG_HERMODE = gettick() + battle_config.WOE_K_DELAY_CG_HERMODE;
				sd->state.K_LOCK_CG_HERMODE = 0;
		} else {
			sd->K_CHK_CG_HERMODE = gettick() + battle_config.K_DELAY_CG_HERMODE;
			sd->state.K_LOCK_CG_HERMODE = 0;
		}
		break;
	case CG_TAROTCARD:
		if(DIFF_TICK(sd->K_CHK_CG_TAROTCARD,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CG_TAROTCARD,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CG_TAROTCARD - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CG_TAROTCARD = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CG_TAROTCARD = gettick() + battle_config.WOE_K_DELAY_CG_TAROTCARD;
				sd->state.K_LOCK_CG_TAROTCARD = 0;
		} else {
			sd->K_CHK_CG_TAROTCARD = gettick() + battle_config.K_DELAY_CG_TAROTCARD;
			sd->state.K_LOCK_CG_TAROTCARD = 0;
		}
		break;
	case CR_ACIDDEMONSTRATION:
		if(DIFF_TICK(sd->K_CHK_CR_ACIDDEMONSTRATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_CR_ACIDDEMONSTRATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_CR_ACIDDEMONSTRATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_CR_ACIDDEMO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_CR_ACIDDEMONSTRATION = gettick() + battle_config.WOE_K_DELAY_CR_ACIDDEMO;
				sd->state.K_LOCK_CR_ACIDDEMO = 0;
		} else {
			sd->K_CHK_CR_ACIDDEMONSTRATION = gettick() + battle_config.K_DELAY_CR_ACIDDEMONSTRATION;
			sd->state.K_LOCK_CR_ACIDDEMO = 0;
		}
		break;
	case SL_HIGH:
		if(DIFF_TICK(sd->K_CHK_SL_HIGH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SL_HIGH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SL_HIGH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SL_HIGH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SL_HIGH = gettick() + battle_config.WOE_K_DELAY_SL_HIGH;
				sd->state.K_LOCK_SL_HIGH = 0;
		} else {
			sd->K_CHK_SL_HIGH = gettick() + battle_config.K_DELAY_SL_HIGH;
			sd->state.K_LOCK_SL_HIGH = 0;
		}
		break;
	case HT_POWER:
		if(DIFF_TICK(sd->K_CHK_HT_POWER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_HT_POWER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_HT_POWER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_HT_POWER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_HT_POWER = gettick() + battle_config.WOE_K_DELAY_HT_POWER;
				sd->state.K_LOCK_HT_POWER = 0;
		} else {
			sd->K_CHK_HT_POWER = gettick() + battle_config.K_DELAY_HT_POWER;
			sd->state.K_LOCK_HT_POWER = 0;
		}
		break;
	case GS_TRIPLEACTION:
		if(DIFF_TICK(sd->K_CHK_GS_TRIPLEACTION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_TRIPLEACTION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_TRIPLEACTION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_TRIPLEACTION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_TRIPLEACTION = gettick() + battle_config.WOE_K_DELAY_GS_TRIPLEACTION;
				sd->state.K_LOCK_GS_TRIPLEACTION = 0;
		} else {
			sd->K_CHK_GS_TRIPLEACTION = gettick() + battle_config.K_DELAY_GS_TRIPLEACTION;
			sd->state.K_LOCK_GS_TRIPLEACTION = 0;
		}
		break;
	case GS_BULLSEYE:
		if(DIFF_TICK(sd->K_CHK_GS_BULLSEYE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_BULLSEYE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_BULLSEYE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_BULLSEYE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_BULLSEYE = gettick() + battle_config.WOE_K_DELAY_GS_BULLSEYE;
				sd->state.K_LOCK_GS_BULLSEYE = 0;
		} else {
			sd->K_CHK_GS_BULLSEYE = gettick() + battle_config.K_DELAY_GS_BULLSEYE;
			sd->state.K_LOCK_GS_BULLSEYE = 0;
		}
		break;
	case GS_MADNESSCANCEL:
		if(DIFF_TICK(sd->K_CHK_GS_MADNESSCANCEL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_MADNESSCANCEL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_MADNESSCANCEL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_MADNESSCANCEL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_MADNESSCANCEL = gettick() + battle_config.WOE_K_DELAY_GS_MADNESSCANCEL;
				sd->state.K_LOCK_GS_MADNESSCANCEL = 0;
		} else {
			sd->K_CHK_GS_MADNESSCANCEL = gettick() + battle_config.K_DELAY_GS_MADNESSCANCEL;
			sd->state.K_LOCK_GS_MADNESSCANCEL = 0;
		}
		break;
	case GS_INCREASING:
		if(DIFF_TICK(sd->K_CHK_GS_INCREASING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_INCREASING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_INCREASING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_INCREASING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_INCREASING = gettick() + battle_config.WOE_K_DELAY_GS_INCREASING;
				sd->state.K_LOCK_GS_INCREASING = 0;
		} else {
			sd->K_CHK_GS_INCREASING = gettick() + battle_config.K_DELAY_GS_INCREASING;
			sd->state.K_LOCK_GS_INCREASING = 0;
		}
		break;
	case GS_MAGICALBULLET:
		if(DIFF_TICK(sd->K_CHK_GS_MAGICALBULLET,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_MAGICALBULLET,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_MAGICALBULLET - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_MAGICALBULLET = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_MAGICALBULLET = gettick() + battle_config.WOE_K_DELAY_GS_MAGICALBULLET;
				sd->state.K_LOCK_GS_MAGICALBULLET = 0;
		} else {
			sd->K_CHK_GS_MAGICALBULLET = gettick() + battle_config.K_DELAY_GS_MAGICALBULLET;
			sd->state.K_LOCK_GS_MAGICALBULLET = 0;
		}
		break;
	case GS_CRACKER:
		if(DIFF_TICK(sd->K_CHK_GS_CRACKER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_CRACKER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_CRACKER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_CRACKER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_CRACKER = gettick() + battle_config.WOE_K_DELAY_GS_CRACKER;
				sd->state.K_LOCK_GS_CRACKER = 0;
		} else {
			sd->K_CHK_GS_CRACKER = gettick() + battle_config.K_DELAY_GS_CRACKER;
			sd->state.K_LOCK_GS_CRACKER = 0;
		}
		break;
	case GS_SINGLEACTION:
		if(DIFF_TICK(sd->K_CHK_GS_SINGLEACTION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_SINGLEACTION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_SINGLEACTION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_SINGLEACTION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_SINGLEACTION = gettick() + battle_config.WOE_K_DELAY_GS_SINGLEACTION;
				sd->state.K_LOCK_GS_SINGLEACTION = 0;
		} else {
			sd->K_CHK_GS_SINGLEACTION = gettick() + battle_config.K_DELAY_GS_SINGLEACTION;
			sd->state.K_LOCK_GS_SINGLEACTION = 0;
		}
		break;
	case GS_CHAINACTION:
		if(DIFF_TICK(sd->K_CHK_GS_CHAINACTION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_CHAINACTION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_CHAINACTION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_CHAINACTION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_CHAINACTION = gettick() + battle_config.WOE_K_DELAY_GS_CHAINACTION;
				sd->state.K_LOCK_GS_CHAINACTION = 0;
		} else {
			sd->K_CHK_GS_CHAINACTION = gettick() + battle_config.K_DELAY_GS_CHAINACTION;
			sd->state.K_LOCK_GS_CHAINACTION = 0;
		}
		break;
	case GS_TRACKING:
		if(DIFF_TICK(sd->K_CHK_GS_TRACKING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_TRACKING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_TRACKING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_TRACKING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_TRACKING = gettick() + battle_config.WOE_K_DELAY_GS_TRACKING;
				sd->state.K_LOCK_GS_TRACKING = 0;
		} else {
			sd->K_CHK_GS_TRACKING = gettick() + battle_config.K_DELAY_GS_TRACKING;
			sd->state.K_LOCK_GS_TRACKING = 0;
		}
		break;
	case GS_DISARM:
		if(DIFF_TICK(sd->K_CHK_GS_DISARM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_DISARM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_DISARM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_DISARM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_DISARM = gettick() + battle_config.WOE_K_DELAY_GS_DISARM;
				sd->state.K_LOCK_GS_DISARM = 0;
		} else {
			sd->K_CHK_GS_DISARM = gettick() + battle_config.K_DELAY_GS_DISARM;
			sd->state.K_LOCK_GS_DISARM = 0;
		}
		break;
	case GS_PIERCINGSHOT:
		if(DIFF_TICK(sd->K_CHK_GS_PIERCINGSHOT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_PIERCINGSHOT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_PIERCINGSHOT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_PIERCINGSHOT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_PIERCINGSHOT = gettick() + battle_config.WOE_K_DELAY_GS_PIERCINGSHOT;
				sd->state.K_LOCK_GS_PIERCINGSHOT = 0;
		} else {
			sd->K_CHK_GS_PIERCINGSHOT = gettick() + battle_config.K_DELAY_GS_PIERCINGSHOT;
			sd->state.K_LOCK_GS_PIERCINGSHOT = 0;
		}
		break;
	case GS_RAPIDSHOWER:
		if(DIFF_TICK(sd->K_CHK_GS_RAPIDSHOWER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_RAPIDSHOWER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_RAPIDSHOWER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_RAPIDSHOWER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_RAPIDSHOWER = gettick() + battle_config.WOE_K_DELAY_GS_RAPIDSHOWER;
				sd->state.K_LOCK_GS_RAPIDSHOWER = 0;
		} else {
			sd->K_CHK_GS_RAPIDSHOWER = gettick() + battle_config.K_DELAY_GS_RAPIDSHOWER;
			sd->state.K_LOCK_GS_RAPIDSHOWER = 0;
		}
		break;
	case GS_DESPERADO:
		if(DIFF_TICK(sd->K_CHK_GS_DESPERADO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_DESPERADO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_DESPERADO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_DESPERADO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_DESPERADO = gettick() + battle_config.WOE_K_DELAY_GS_DESPERADO;
				sd->state.K_LOCK_GS_DESPERADO = 0;
		} else {
			sd->K_CHK_GS_DESPERADO = gettick() + battle_config.K_DELAY_GS_DESPERADO;
			sd->state.K_LOCK_GS_DESPERADO = 0;
		}
		break;
	case GS_GATLINGFEVER:
		if(DIFF_TICK(sd->K_CHK_GS_GATLINGFEVER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_GATLINGFEVER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_GATLINGFEVER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_GATLINGFEVER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_GATLINGFEVER = gettick() + battle_config.WOE_K_DELAY_GS_GATLINGFEVER;
				sd->state.K_LOCK_GS_GATLINGFEVER = 0;
		} else {
			sd->K_CHK_GS_GATLINGFEVER = gettick() + battle_config.K_DELAY_GS_GATLINGFEVER;
			sd->state.K_LOCK_GS_GATLINGFEVER = 0;
		}
		break;
	case GS_DUST:
		if(DIFF_TICK(sd->K_CHK_GS_DUST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_DUST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_DUST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_DUST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_DUST = gettick() + battle_config.WOE_K_DELAY_GS_DUST;
				sd->state.K_LOCK_GS_DUST = 0;
		} else {
			sd->K_CHK_GS_DUST = gettick() + battle_config.K_DELAY_GS_DUST;
			sd->state.K_LOCK_GS_DUST = 0;
		}
		break;
	case GS_FULLBUSTER:
		if(DIFF_TICK(sd->K_CHK_GS_FULLBUSTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_FULLBUSTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_FULLBUSTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_FULLBUSTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_FULLBUSTER = gettick() + battle_config.WOE_K_DELAY_GS_FULLBUSTER;
				sd->state.K_LOCK_GS_FULLBUSTER = 0;
		} else {
			sd->K_CHK_GS_FULLBUSTER = gettick() + battle_config.K_DELAY_GS_FULLBUSTER;
			sd->state.K_LOCK_GS_FULLBUSTER = 0;
		}
		break;
	case GS_SPREADATTACK:
		if(DIFF_TICK(sd->K_CHK_GS_SPREADATTACK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_SPREADATTACK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_SPREADATTACK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_SPREADATTACK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_SPREADATTACK = gettick() + battle_config.WOE_K_DELAY_GS_SPREADATTACK;
				sd->state.K_LOCK_GS_SPREADATTACK = 0;
		} else {
			sd->K_CHK_GS_SPREADATTACK = gettick() + battle_config.K_DELAY_GS_SPREADATTACK;
			sd->state.K_LOCK_GS_SPREADATTACK = 0;
		}
		break;
	case GS_GROUNDDRIFT:
		if(DIFF_TICK(sd->K_CHK_GS_GROUNDDRIFT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GS_GROUNDDRIFT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GS_GROUNDDRIFT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GS_GROUNDDRIFT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GS_GROUNDDRIFT = gettick() + battle_config.WOE_K_DELAY_GS_GROUNDDRIFT;
				sd->state.K_LOCK_GS_GROUNDDRIFT = 0;
		} else {
			sd->K_CHK_GS_GROUNDDRIFT = gettick() + battle_config.K_DELAY_GS_GROUNDDRIFT;
			sd->state.K_LOCK_GS_GROUNDDRIFT = 0;
		}
		break;
	case NJ_TOBIDOUGU:
		if(DIFF_TICK(sd->K_CHK_NJ_TOBIDOUGU,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_TOBIDOUGU,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_TOBIDOUGU - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_TOBIDOUGU = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_TOBIDOUGU = gettick() + battle_config.WOE_K_DELAY_NJ_TOBIDOUGU;
				sd->state.K_LOCK_NJ_TOBIDOUGU = 0;
		} else {
			sd->K_CHK_NJ_TOBIDOUGU = gettick() + battle_config.K_DELAY_NJ_TOBIDOUGU;
			sd->state.K_LOCK_NJ_TOBIDOUGU = 0;
		}
		break;
	case NJ_SYURIKEN:
		if(DIFF_TICK(sd->K_CHK_NJ_SYURIKEN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_SYURIKEN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_SYURIKEN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_SYURIKEN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_SYURIKEN = gettick() + battle_config.WOE_K_DELAY_NJ_SYURIKEN;
				sd->state.K_LOCK_NJ_SYURIKEN = 0;
		} else {
			sd->K_CHK_NJ_SYURIKEN = gettick() + battle_config.K_DELAY_NJ_SYURIKEN;
			sd->state.K_LOCK_NJ_SYURIKEN = 0;
		}
		break;
	case NJ_KUNAI:
		if(DIFF_TICK(sd->K_CHK_NJ_KUNAI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_KUNAI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_KUNAI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_KUNAI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_KUNAI = gettick() + battle_config.WOE_K_DELAY_NJ_KUNAI;
				sd->state.K_LOCK_NJ_KUNAI = 0;
		} else {
			sd->K_CHK_NJ_KUNAI = gettick() + battle_config.K_DELAY_NJ_KUNAI;
			sd->state.K_LOCK_NJ_KUNAI = 0;
		}
		break;
	case NJ_HUUMA:
		if(DIFF_TICK(sd->K_CHK_NJ_HUUMA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_HUUMA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_HUUMA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_HUUMA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_HUUMA = gettick() + battle_config.WOE_K_DELAY_NJ_HUUMA;
				sd->state.K_LOCK_NJ_HUUMA = 0;
		} else {
			sd->K_CHK_NJ_HUUMA = gettick() + battle_config.K_DELAY_NJ_HUUMA;
			sd->state.K_LOCK_NJ_HUUMA = 0;
		}
		break;
	case NJ_ZENYNAGE:
		if(DIFF_TICK(sd->K_CHK_NJ_ZENYNAGE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_ZENYNAGE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_ZENYNAGE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_ZENYNAGE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_ZENYNAGE = gettick() + battle_config.WOE_K_DELAY_NJ_ZENYNAGE;
				sd->state.K_LOCK_NJ_ZENYNAGE = 0;
		} else {
			sd->K_CHK_NJ_ZENYNAGE = gettick() + battle_config.K_DELAY_NJ_ZENYNAGE;
			sd->state.K_LOCK_NJ_ZENYNAGE = 0;
		}
		break;
	case NJ_TATAMIGAESHI:
		if(DIFF_TICK(sd->K_CHK_NJ_TATAMIGAESHI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_TATAMIGAESHI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_TATAMIGAESHI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_TATAMIGAESHI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_TATAMIGAESHI = gettick() + battle_config.WOE_K_DELAY_NJ_TATAMIGAESHI;
				sd->state.K_LOCK_NJ_TATAMIGAESHI = 0;
		} else {
			sd->K_CHK_NJ_TATAMIGAESHI = gettick() + battle_config.K_DELAY_NJ_TATAMIGAESHI;
			sd->state.K_LOCK_NJ_TATAMIGAESHI = 0;
		}
		break;
	case NJ_KASUMIKIRI:
		if(DIFF_TICK(sd->K_CHK_NJ_KASUMIKIRI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_KASUMIKIRI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_KASUMIKIRI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_KASUMIKIRI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_KASUMIKIRI = gettick() + battle_config.WOE_K_DELAY_NJ_KASUMIKIRI;
				sd->state.K_LOCK_NJ_KASUMIKIRI = 0;
		} else {
			sd->K_CHK_NJ_KASUMIKIRI = gettick() + battle_config.K_DELAY_NJ_KASUMIKIRI;
			sd->state.K_LOCK_NJ_KASUMIKIRI = 0;
		}
		break;
	case NJ_SHADOWJUMP:
		if(DIFF_TICK(sd->K_CHK_NJ_SHADOWJUMP,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_SHADOWJUMP,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_SHADOWJUMP - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_SHADOWJUMP = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_SHADOWJUMP = gettick() + battle_config.WOE_K_DELAY_NJ_SHADOWJUMP;
				sd->state.K_LOCK_NJ_SHADOWJUMP = 0;
		} else {
			sd->K_CHK_NJ_SHADOWJUMP = gettick() + battle_config.K_DELAY_NJ_SHADOWJUMP;
			sd->state.K_LOCK_NJ_SHADOWJUMP = 0;
		}
		break;
	case NJ_KIRIKAGE:
		if(DIFF_TICK(sd->K_CHK_NJ_KIRIKAGE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_KIRIKAGE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_KIRIKAGE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_KIRIKAGE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_KIRIKAGE = gettick() + battle_config.WOE_K_DELAY_NJ_KIRIKAGE;
				sd->state.K_LOCK_NJ_KIRIKAGE = 0;
		} else {
			sd->K_CHK_NJ_KIRIKAGE = gettick() + battle_config.K_DELAY_NJ_KIRIKAGE;
			sd->state.K_LOCK_NJ_KIRIKAGE = 0;
		}
		break;
	case NJ_UTSUSEMI:
		if(DIFF_TICK(sd->K_CHK_NJ_UTSUSEMI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_UTSUSEMI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_UTSUSEMI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_UTSUSEMI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_UTSUSEMI = gettick() + battle_config.WOE_K_DELAY_NJ_UTSUSEMI;
				sd->state.K_LOCK_NJ_UTSUSEMI = 0;
		} else {
			sd->K_CHK_NJ_UTSUSEMI = gettick() + battle_config.K_DELAY_NJ_UTSUSEMI;
			sd->state.K_LOCK_NJ_UTSUSEMI = 0;
		}
		break;
	case NJ_BUNSINJYUTSU:
		if(DIFF_TICK(sd->K_CHK_NJ_BUNSINJYUTSU,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_BUNSINJYUTSU,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_BUNSINJYUTSU - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_BUNSINJYUTSU = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_BUNSINJYUTSU = gettick() + battle_config.WOE_K_DELAY_NJ_BUNSINJYUTSU;
				sd->state.K_LOCK_NJ_BUNSINJYUTSU = 0;
		} else {
			sd->K_CHK_NJ_BUNSINJYUTSU = gettick() + battle_config.K_DELAY_NJ_BUNSINJYUTSU;
			sd->state.K_LOCK_NJ_BUNSINJYUTSU = 0;
		}
		break;
	case NJ_NINPOU:
		if(DIFF_TICK(sd->K_CHK_NJ_NINPOU,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_NINPOU,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_NINPOU - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_NINPOU = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_NINPOU = gettick() + battle_config.WOE_K_DELAY_NJ_NINPOU;
				sd->state.K_LOCK_NJ_NINPOU = 0;
		} else {
			sd->K_CHK_NJ_NINPOU = gettick() + battle_config.K_DELAY_NJ_NINPOU;
			sd->state.K_LOCK_NJ_NINPOU = 0;
		}
		break;
	case NJ_KOUENKA:
		if(DIFF_TICK(sd->K_CHK_NJ_KOUENKA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_KOUENKA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_KOUENKA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_KOUENKA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_KOUENKA = gettick() + battle_config.WOE_K_DELAY_NJ_KOUENKA;
				sd->state.K_LOCK_NJ_KOUENKA = 0;
		} else {
			sd->K_CHK_NJ_KOUENKA = gettick() + battle_config.K_DELAY_NJ_KOUENKA;
			sd->state.K_LOCK_NJ_KOUENKA = 0;
		}
		break;
	case NJ_KAENSIN:
		if(DIFF_TICK(sd->K_CHK_NJ_KAENSIN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_KAENSIN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_KAENSIN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_KAENSIN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_KAENSIN = gettick() + battle_config.WOE_K_DELAY_NJ_KAENSIN;
				sd->state.K_LOCK_NJ_KAENSIN = 0;
		} else {
			sd->K_CHK_NJ_KAENSIN = gettick() + battle_config.K_DELAY_NJ_KAENSIN;
			sd->state.K_LOCK_NJ_KAENSIN = 0;
		}
		break;
	case NJ_BAKUENRYU:
		if(DIFF_TICK(sd->K_CHK_NJ_BAKUENRYU,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_BAKUENRYU,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_BAKUENRYU - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_BAKUENRYU = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_BAKUENRYU = gettick() + battle_config.WOE_K_DELAY_NJ_BAKUENRYU;
				sd->state.K_LOCK_NJ_BAKUENRYU = 0;
		} else {
			sd->K_CHK_NJ_BAKUENRYU = gettick() + battle_config.K_DELAY_NJ_BAKUENRYU;
			sd->state.K_LOCK_NJ_BAKUENRYU = 0;
		}
		break;
	case NJ_HYOUSENSOU:
		if(DIFF_TICK(sd->K_CHK_NJ_HYOUSENSOU,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_HYOUSENSOU,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_HYOUSENSOU - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_HYOUSENSOU = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_HYOUSENSOU = gettick() + battle_config.WOE_K_DELAY_NJ_HYOUSENSOU;
				sd->state.K_LOCK_NJ_HYOUSENSOU = 0;
		} else {
			sd->K_CHK_NJ_HYOUSENSOU = gettick() + battle_config.K_DELAY_NJ_HYOUSENSOU;
			sd->state.K_LOCK_NJ_HYOUSENSOU = 0;
		}
		break;
	case NJ_SUITON:
		if(DIFF_TICK(sd->K_CHK_NJ_SUITON,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_SUITON,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_SUITON - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_SUITON = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_SUITON = gettick() + battle_config.WOE_K_DELAY_NJ_SUITON;
				sd->state.K_LOCK_NJ_SUITON = 0;
		} else {
			sd->K_CHK_NJ_SUITON = gettick() + battle_config.K_DELAY_NJ_SUITON;
			sd->state.K_LOCK_NJ_SUITON = 0;
		}
		break;
	case NJ_HYOUSYOURAKU:
		if(DIFF_TICK(sd->K_CHK_NJ_HYOUSYOURAKU,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_HYOUSYOURAKU,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_HYOUSYOURAKU - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_HYOUSYOURAKU = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_HYOUSYOURAKU = gettick() + battle_config.WOE_K_DELAY_NJ_HYOUSYOURAKU;
				sd->state.K_LOCK_NJ_HYOUSYOURAKU = 0;
		} else {
			sd->K_CHK_NJ_HYOUSYOURAKU = gettick() + battle_config.K_DELAY_NJ_HYOUSYOURAKU;
			sd->state.K_LOCK_NJ_HYOUSYOURAKU = 0;
		}
		break;
	case NJ_HUUJIN:
		if(DIFF_TICK(sd->K_CHK_NJ_HUUJIN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_HUUJIN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_HUUJIN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_HUUJIN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_HUUJIN = gettick() + battle_config.WOE_K_DELAY_NJ_HUUJIN;
				sd->state.K_LOCK_NJ_HUUJIN = 0;
		} else {
			sd->K_CHK_NJ_HUUJIN = gettick() + battle_config.K_DELAY_NJ_HUUJIN;
			sd->state.K_LOCK_NJ_HUUJIN = 0;
		}
		break;
	case NJ_RAIGEKISAI:
		if(DIFF_TICK(sd->K_CHK_NJ_RAIGEKISAI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_RAIGEKISAI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_RAIGEKISAI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_RAIGEKISAI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_RAIGEKISAI = gettick() + battle_config.WOE_K_DELAY_NJ_RAIGEKISAI;
				sd->state.K_LOCK_NJ_RAIGEKISAI = 0;
		} else {
			sd->K_CHK_NJ_RAIGEKISAI = gettick() + battle_config.K_DELAY_NJ_RAIGEKISAI;
			sd->state.K_LOCK_NJ_RAIGEKISAI = 0;
		}
		break;
	case NJ_KAMAITACHI:
		if(DIFF_TICK(sd->K_CHK_NJ_KAMAITACHI,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_KAMAITACHI,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_KAMAITACHI - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_KAMAITACHI = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_KAMAITACHI = gettick() + battle_config.WOE_K_DELAY_NJ_KAMAITACHI;
				sd->state.K_LOCK_NJ_KAMAITACHI = 0;
		} else {
			sd->K_CHK_NJ_KAMAITACHI = gettick() + battle_config.K_DELAY_NJ_KAMAITACHI;
			sd->state.K_LOCK_NJ_KAMAITACHI = 0;
		}
		break;
	case NJ_NEN:
		if(DIFF_TICK(sd->K_CHK_NJ_NEN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_NEN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_NEN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_NEN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_NEN = gettick() + battle_config.WOE_K_DELAY_NJ_NEN;
				sd->state.K_LOCK_NJ_NEN = 0;
		} else {
			sd->K_CHK_NJ_NEN = gettick() + battle_config.K_DELAY_NJ_NEN;
			sd->state.K_LOCK_NJ_NEN = 0;
		}
		break;
	case NJ_ISSEN:
		if(DIFF_TICK(sd->K_CHK_NJ_ISSEN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NJ_ISSEN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NJ_ISSEN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NJ_ISSEN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NJ_ISSEN = gettick() + battle_config.WOE_K_DELAY_NJ_ISSEN;
				sd->state.K_LOCK_NJ_ISSEN = 0;
		} else {
			sd->K_CHK_NJ_ISSEN = gettick() + battle_config.K_DELAY_NJ_ISSEN;
			sd->state.K_LOCK_NJ_ISSEN = 0;
		}
		break;
	case KN_CHARGEATK:
		if(DIFF_TICK(sd->K_CHK_KN_CHARGEATK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_KN_CHARGEATK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_KN_CHARGEATK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_KN_CHARGEATK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_KN_CHARGEATK = gettick() + battle_config.WOE_K_DELAY_KN_CHARGEATK;
				sd->state.K_LOCK_KN_CHARGEATK = 0;
		} else {
			sd->K_CHK_KN_CHARGEATK = gettick() + battle_config.K_DELAY_KN_CHARGEATK;
			sd->state.K_LOCK_KN_CHARGEATK = 0;
		}
		break;
	case AS_VENOMKNIFE:
		if(DIFF_TICK(sd->K_CHK_AS_VENOMKNIFE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AS_VENOMKNIFE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AS_VENOMKNIFE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AS_VENOMKNIFE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AS_VENOMKNIFE = gettick() + battle_config.WOE_K_DELAY_AS_VENOMKNIFE;
				sd->state.K_LOCK_AS_VENOMKNIFE = 0;
		} else {
			sd->K_CHK_AS_VENOMKNIFE = gettick() + battle_config.K_DELAY_AS_VENOMKNIFE;
			sd->state.K_LOCK_AS_VENOMKNIFE = 0;
		}
		break;
	case RG_CLOSECONFINE:
		if(DIFF_TICK(sd->K_CHK_RG_CLOSECONFINE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RG_CLOSECONFINE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RG_CLOSECONFINE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RG_CLOSECONFINE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RG_CLOSECONFINE = gettick() + battle_config.WOE_K_DELAY_RG_CLOSECONFINE;
				sd->state.K_LOCK_RG_CLOSECONFINE = 0;
		} else {
			sd->K_CHK_RG_CLOSECONFINE = gettick() + battle_config.K_DELAY_RG_CLOSECONFINE;
			sd->state.K_LOCK_RG_CLOSECONFINE = 0;
		}
		break;
	case WZ_SIGHTBLASTER:
		if(DIFF_TICK(sd->K_CHK_WZ_SIGHTBLASTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WZ_SIGHTBLASTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WZ_SIGHTBLASTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WZ_SIGHTBLASTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WZ_SIGHTBLASTER = gettick() + battle_config.WOE_K_DELAY_WZ_SIGHTBLASTER;
				sd->state.K_LOCK_WZ_SIGHTBLASTER = 0;
		} else {
			sd->K_CHK_WZ_SIGHTBLASTER = gettick() + battle_config.K_DELAY_WZ_SIGHTBLASTER;
			sd->state.K_LOCK_WZ_SIGHTBLASTER = 0;
		}
		break;
	case HT_PHANTASMIC:
		if(DIFF_TICK(sd->K_CHK_HT_PHANTASMIC,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_HT_PHANTASMIC,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_HT_PHANTASMIC - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_HT_PHANTASMIC = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_HT_PHANTASMIC = gettick() + battle_config.WOE_K_DELAY_HT_PHANTASMIC;
				sd->state.K_LOCK_HT_PHANTASMIC = 0;
		} else {
			sd->K_CHK_HT_PHANTASMIC = gettick() + battle_config.K_DELAY_HT_PHANTASMIC;
			sd->state.K_LOCK_HT_PHANTASMIC = 0;
		}
		break;
	case BA_PANGVOICE:
		if(DIFF_TICK(sd->K_CHK_BA_PANGVOICE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_BA_PANGVOICE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_BA_PANGVOICE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_BA_PANGVOICE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_BA_PANGVOICE = gettick() + battle_config.WOE_K_DELAY_BA_PANGVOICE;
				sd->state.K_LOCK_BA_PANGVOICE = 0;
		} else {
			sd->K_CHK_BA_PANGVOICE = gettick() + battle_config.K_DELAY_BA_PANGVOICE;
			sd->state.K_LOCK_BA_PANGVOICE = 0;
		}
		break;
	case DC_WINKCHARM:
		if(DIFF_TICK(sd->K_CHK_DC_WINKCHARM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_DC_WINKCHARM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_DC_WINKCHARM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_DC_WINKCHARM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_DC_WINKCHARM = gettick() + battle_config.WOE_K_DELAY_DC_WINKCHARM;
				sd->state.K_LOCK_DC_WINKCHARM = 0;
		} else {
			sd->K_CHK_DC_WINKCHARM = gettick() + battle_config.K_DELAY_DC_WINKCHARM;
			sd->state.K_LOCK_DC_WINKCHARM = 0;
		}
		break;
	case PR_REDEMPTIO:
		if(DIFF_TICK(sd->K_CHK_PR_REDEMPTIO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_PR_REDEMPTIO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_PR_REDEMPTIO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_PR_REDEMPTIO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_PR_REDEMPTIO = gettick() + battle_config.WOE_K_DELAY_PR_REDEMPTIO;
				sd->state.K_LOCK_PR_REDEMPTIO = 0;
		} else {
			sd->K_CHK_PR_REDEMPTIO = gettick() + battle_config.K_DELAY_PR_REDEMPTIO;
			sd->state.K_LOCK_PR_REDEMPTIO = 0;
		}
		break;
	case MO_KITRANSLATION:
		if(DIFF_TICK(sd->K_CHK_MO_KITRANSLATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_KITRANSLATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_KITRANSLATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_KITRANSLATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_KITRANSLATION = gettick() + battle_config.WOE_K_DELAY_MO_KITRANSLATION;
				sd->state.K_LOCK_MO_KITRANSLATION = 0;
		} else {
			sd->K_CHK_MO_KITRANSLATION = gettick() + battle_config.K_DELAY_MO_KITRANSLATION;
			sd->state.K_LOCK_MO_KITRANSLATION = 0;
		}
		break;
	case MO_BALKYOUNG:
		if(DIFF_TICK(sd->K_CHK_MO_BALKYOUNG,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MO_BALKYOUNG,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MO_BALKYOUNG - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MO_BALKYOUNG = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MO_BALKYOUNG = gettick() + battle_config.WOE_K_DELAY_MO_BALKYOUNG;
				sd->state.K_LOCK_MO_BALKYOUNG = 0;
		} else {
			sd->K_CHK_MO_BALKYOUNG = gettick() + battle_config.K_DELAY_MO_BALKYOUNG;
			sd->state.K_LOCK_MO_BALKYOUNG = 0;
		}
		break;
	case RK_SONICWAVE:
		if(DIFF_TICK(sd->K_CHK_RK_SONICWAVE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_SONICWAVE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_SONICWAVE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_SONICWAVE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_SONICWAVE = gettick() + battle_config.WOE_K_DELAY_RK_SONICWAVE;
				sd->state.K_LOCK_RK_SONICWAVE = 0;
		} else {
			sd->K_CHK_RK_SONICWAVE = gettick() + battle_config.K_DELAY_RK_SONICWAVE;
			sd->state.K_LOCK_RK_SONICWAVE = 0;
		}
		break;
	case RK_DEATHBOUND:
		if(DIFF_TICK(sd->K_CHK_RK_DEATHBOUND,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_DEATHBOUND,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_DEATHBOUND - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_DEATHBOUND = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_DEATHBOUND = gettick() + battle_config.WOE_K_DELAY_RK_DEATHBOUND;
				sd->state.K_LOCK_RK_DEATHBOUND = 0;
		} else {
			sd->K_CHK_RK_DEATHBOUND = gettick() + battle_config.K_DELAY_RK_DEATHBOUND;
			sd->state.K_LOCK_RK_DEATHBOUND = 0;
		}
		break;
	case RK_HUNDREDSPEAR:
		if(DIFF_TICK(sd->K_CHK_RK_HUNDREDSPEAR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_HUNDREDSPEAR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_HUNDREDSPEAR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_HUNDREDSPEAR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_HUNDREDSPEAR = gettick() + battle_config.WOE_K_DELAY_RK_HUNDREDSPEAR;
				sd->state.K_LOCK_RK_HUNDREDSPEAR = 0;
		} else {
			sd->K_CHK_RK_HUNDREDSPEAR = gettick() + battle_config.K_DELAY_RK_HUNDREDSPEAR;
			sd->state.K_LOCK_RK_HUNDREDSPEAR = 0;
		}
		break;
	case RK_WINDCUTTER:
		if(DIFF_TICK(sd->K_CHK_RK_WINDCUTTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_WINDCUTTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_WINDCUTTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_WINDCUTTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_WINDCUTTER = gettick() + battle_config.WOE_K_DELAY_RK_WINDCUTTER;
				sd->state.K_LOCK_RK_WINDCUTTER = 0;
		} else {
			sd->K_CHK_RK_WINDCUTTER = gettick() + battle_config.K_DELAY_RK_WINDCUTTER;
			sd->state.K_LOCK_RK_WINDCUTTER = 0;
		}
		break;
	case RK_IGNITIONBREAK:
		if(DIFF_TICK(sd->K_CHK_RK_IGNITIONBREAK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_IGNITIONBREAK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_IGNITIONBREAK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_IGNITIONBREAK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_IGNITIONBREAK = gettick() + battle_config.WOE_K_DELAY_RK_IGNITIONBREAK;
				sd->state.K_LOCK_RK_IGNITIONBREAK = 0;
		} else {
			sd->K_CHK_RK_IGNITIONBREAK = gettick() + battle_config.K_DELAY_RK_IGNITIONBREAK;
			sd->state.K_LOCK_RK_IGNITIONBREAK = 0;
		}
		break;
	case RK_DRAGONBREATH:
		if(DIFF_TICK(sd->K_CHK_RK_DRAGONBREATH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_DRAGONBREATH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_DRAGONBREATH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_DRAGONBREATH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_DRAGONBREATH = gettick() + battle_config.WOE_K_DELAY_RK_DRAGONBREATH;
				sd->state.K_LOCK_RK_DRAGONBREATH = 0;
		} else {
			sd->K_CHK_RK_DRAGONBREATH = gettick() + battle_config.K_DELAY_RK_DRAGONBREATH;
			sd->state.K_LOCK_RK_DRAGONBREATH = 0;
		}
		break;
	case RK_CRUSHSTRIKE:
		if(DIFF_TICK(sd->K_CHK_RK_CRUSHSTRIKE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_CRUSHSTRIKE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_CRUSHSTRIKE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_CRUSHSTRIKE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_CRUSHSTRIKE = gettick() + battle_config.WOE_K_DELAY_RK_CRUSHSTRIKE;
				sd->state.K_LOCK_RK_CRUSHSTRIKE = 0;
		} else {
			sd->K_CHK_RK_CRUSHSTRIKE = gettick() + battle_config.K_DELAY_RK_CRUSHSTRIKE;
			sd->state.K_LOCK_RK_CRUSHSTRIKE = 0;
		}
		break;
	case RK_STORMBLAST:
		if(DIFF_TICK(sd->K_CHK_RK_STORMBLAST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_STORMBLAST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_STORMBLAST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_STORMBLAST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_STORMBLAST = gettick() + battle_config.WOE_K_DELAY_RK_STORMBLAST;
				sd->state.K_LOCK_RK_STORMBLAST = 0;
		} else {
			sd->K_CHK_RK_STORMBLAST = gettick() + battle_config.K_DELAY_RK_STORMBLAST;
			sd->state.K_LOCK_RK_STORMBLAST = 0;
		}
		break;
	case RK_PHANTOMTHRUST:
		if(DIFF_TICK(sd->K_CHK_RK_PHANTOMTHRUST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RK_PHANTOMTHRUST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RK_PHANTOMTHRUST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RK_PHANTOMTHRUST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RK_PHANTOMTHRUST = gettick() + battle_config.WOE_K_DELAY_RK_PHANTOMTHRUST;
				sd->state.K_LOCK_RK_PHANTOMTHRUST = 0;
		} else {
			sd->K_CHK_RK_PHANTOMTHRUST = gettick() + battle_config.K_DELAY_RK_PHANTOMTHRUST;
			sd->state.K_LOCK_RK_PHANTOMTHRUST = 0;
		}
		break;
	case GC_CROSSIMPACT:
		if(DIFF_TICK(sd->K_CHK_GC_CROSSIMPACT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GC_CROSSIMPACT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GC_CROSSIMPACT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GC_CROSSIMPACT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GC_CROSSIMPACT = gettick() + battle_config.WOE_K_DELAY_GC_CROSSIMPACT;
				sd->state.K_LOCK_GC_CROSSIMPACT = 0;
		} else {
			sd->K_CHK_GC_CROSSIMPACT = gettick() + battle_config.K_DELAY_GC_CROSSIMPACT;
			sd->state.K_LOCK_GC_CROSSIMPACT = 0;
		}
		break;
	case GC_WEAPONCRUSH:
		if(DIFF_TICK(sd->K_CHK_GC_WEAPONCRUSH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GC_WEAPONCRUSH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GC_WEAPONCRUSH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GC_WEAPONCRUSH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GC_WEAPONCRUSH = gettick() + battle_config.WOE_K_DELAY_GC_WEAPONCRUSH;
				sd->state.K_LOCK_GC_WEAPONCRUSH = 0;
		} else {
			sd->K_CHK_GC_WEAPONCRUSH = gettick() + battle_config.K_DELAY_GC_WEAPONCRUSH;
			sd->state.K_LOCK_GC_WEAPONCRUSH = 0;
		}
		break;
	case GC_ROLLINGCUTTER:
		if(DIFF_TICK(sd->K_CHK_GC_ROLLINGCUTTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GC_ROLLINGCUTTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GC_ROLLINGCUTTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GC_ROLLINGCUTTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GC_ROLLINGCUTTER = gettick() + battle_config.WOE_K_DELAY_GC_ROLLINGCUTTER;
				sd->state.K_LOCK_GC_ROLLINGCUTTER = 0;
		} else {
			sd->K_CHK_GC_ROLLINGCUTTER = gettick() + battle_config.K_DELAY_GC_ROLLINGCUTTER;
			sd->state.K_LOCK_GC_ROLLINGCUTTER = 0;
		}
		break;
	case GC_CROSSRIPPERSLASHER:
		if(DIFF_TICK(sd->K_CHK_GC_CROSSRIPPERSLASHER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GC_CROSSRIPPERSLASHER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GC_CROSSRIPPERSLASHER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GC_CROSSSLASHER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GC_CROSSRIPPERSLASHER = gettick() + battle_config.WOE_K_DELAY_GC_CROSSSLASHER;
				sd->state.K_LOCK_GC_CROSSSLASHER = 0;
		} else {
			sd->K_CHK_GC_CROSSRIPPERSLASHER = gettick() + battle_config.K_DELAY_GC_CROSSRIPPERSLASHER;
			sd->state.K_LOCK_GC_CROSSSLASHER = 0;
		}
		break;
	case AB_JUDEX:
		if(DIFF_TICK(sd->K_CHK_AB_JUDEX,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_JUDEX,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_JUDEX - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_JUDEX = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_JUDEX = gettick() + battle_config.WOE_K_DELAY_AB_JUDEX;
				sd->state.K_LOCK_AB_JUDEX = 0;
		} else {
			sd->K_CHK_AB_JUDEX = gettick() + battle_config.K_DELAY_AB_JUDEX;
			sd->state.K_LOCK_AB_JUDEX = 0;
		}
		break;
	case AB_ADORAMUS:
		if(DIFF_TICK(sd->K_CHK_AB_ADORAMUS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_ADORAMUS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_ADORAMUS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_ADORAMUS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_ADORAMUS = gettick() + battle_config.WOE_K_DELAY_AB_ADORAMUS;
				sd->state.K_LOCK_AB_ADORAMUS = 0;
		} else {
			sd->K_CHK_AB_ADORAMUS = gettick() + battle_config.K_DELAY_AB_ADORAMUS;
			sd->state.K_LOCK_AB_ADORAMUS = 0;
		}
		break;
	case AB_CHEAL:
		if(DIFF_TICK(sd->K_CHK_AB_CHEAL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_CHEAL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_CHEAL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_CHEAL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_CHEAL = gettick() + battle_config.WOE_K_DELAY_AB_CHEAL;
				sd->state.K_LOCK_AB_CHEAL = 0;
		} else {
			sd->K_CHK_AB_CHEAL = gettick() + battle_config.K_DELAY_AB_CHEAL;
			sd->state.K_LOCK_AB_CHEAL = 0;
		}
		break;
	case AB_EPICLESIS:
		if(DIFF_TICK(sd->K_CHK_AB_EPICLESIS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_EPICLESIS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_EPICLESIS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_EPICLESIS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_EPICLESIS = gettick() + battle_config.WOE_K_DELAY_AB_EPICLESIS;
				sd->state.K_LOCK_AB_EPICLESIS = 0;
		} else {
			sd->K_CHK_AB_EPICLESIS = gettick() + battle_config.K_DELAY_AB_EPICLESIS;
			sd->state.K_LOCK_AB_EPICLESIS = 0;
		}
		break;
	case AB_PRAEFATIO:
		if(DIFF_TICK(sd->K_CHK_AB_PRAEFATIO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_PRAEFATIO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_PRAEFATIO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_PRAEFATIO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_PRAEFATIO = gettick() + battle_config.WOE_K_DELAY_AB_PRAEFATIO;
				sd->state.K_LOCK_AB_PRAEFATIO = 0;
		} else {
			sd->K_CHK_AB_PRAEFATIO = gettick() + battle_config.K_DELAY_AB_PRAEFATIO;
			sd->state.K_LOCK_AB_PRAEFATIO = 0;
		}
		break;
	case AB_EUCHARISTICA:
		if(DIFF_TICK(sd->K_CHK_AB_EUCHARISTICA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_EUCHARISTICA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_EUCHARISTICA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_EUCHARISTICA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_EUCHARISTICA = gettick() + battle_config.WOE_K_DELAY_AB_EUCHARISTICA;
				sd->state.K_LOCK_AB_EUCHARISTICA = 0;
		} else {
			sd->K_CHK_AB_EUCHARISTICA = gettick() + battle_config.K_DELAY_AB_EUCHARISTICA;
			sd->state.K_LOCK_AB_EUCHARISTICA = 0;
		}
		break;
	case AB_RENOVATIO:
		if(DIFF_TICK(sd->K_CHK_AB_RENOVATIO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_RENOVATIO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_RENOVATIO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_RENOVATIO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_RENOVATIO = gettick() + battle_config.WOE_K_DELAY_AB_RENOVATIO;
				sd->state.K_LOCK_AB_RENOVATIO = 0;
		} else {
			sd->K_CHK_AB_RENOVATIO = gettick() + battle_config.K_DELAY_AB_RENOVATIO;
			sd->state.K_LOCK_AB_RENOVATIO = 0;
		}
		break;
	case AB_HIGHNESSHEAL:
		if(DIFF_TICK(sd->K_CHK_AB_HIGHNESSHEAL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_HIGHNESSHEAL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_HIGHNESSHEAL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_HIGHNESSHEAL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_HIGHNESSHEAL = gettick() + battle_config.WOE_K_DELAY_AB_HIGHNESSHEAL;
				sd->state.K_LOCK_AB_HIGHNESSHEAL = 0;
		} else {
			sd->K_CHK_AB_HIGHNESSHEAL = gettick() + battle_config.K_DELAY_AB_HIGHNESSHEAL;
			sd->state.K_LOCK_AB_HIGHNESSHEAL = 0;
		}
		break;
	case AB_CLEARANCE:
		if(DIFF_TICK(sd->K_CHK_AB_CLEARANCE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_CLEARANCE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_CLEARANCE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_CLEARANCE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_CLEARANCE = gettick() + battle_config.WOE_K_DELAY_AB_CLEARANCE;
				sd->state.K_LOCK_AB_CLEARANCE = 0;
		} else {
			sd->K_CHK_AB_CLEARANCE = gettick() + battle_config.K_DELAY_AB_CLEARANCE;
			sd->state.K_LOCK_AB_CLEARANCE = 0;
		}
		break;
	case AB_EXPIATIO:
		if(DIFF_TICK(sd->K_CHK_AB_EXPIATIO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_EXPIATIO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_EXPIATIO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_EXPIATIO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_EXPIATIO = gettick() + battle_config.WOE_K_DELAY_AB_EXPIATIO;
				sd->state.K_LOCK_AB_EXPIATIO = 0;
		} else {
			sd->K_CHK_AB_EXPIATIO = gettick() + battle_config.K_DELAY_AB_EXPIATIO;
			sd->state.K_LOCK_AB_EXPIATIO = 0;
		}
		break;
	case AB_DUPLELIGHT:
		if(DIFF_TICK(sd->K_CHK_AB_DUPLELIGHT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_DUPLELIGHT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_DUPLELIGHT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_DUPLELIGHT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_DUPLELIGHT = gettick() + battle_config.WOE_K_DELAY_AB_DUPLITE;
				sd->state.K_LOCK_AB_DUPLELIGHT = 0;
		} else {
			sd->K_CHK_AB_DUPLELIGHT = gettick() + battle_config.K_DELAY_AB_DUPLELIGHT;
			sd->state.K_LOCK_AB_DUPLELIGHT = 0;
		}
		break;
	case AB_DUPLELIGHT_MELEE:
		if(DIFF_TICK(sd->K_CHK_AB_DUPLELIGHT_MELEE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_DUPLELIGHT_MELEE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_DUPLELIGHT_MELEE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_DUPLIGHT_MELEE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_DUPLELIGHT_MELEE = gettick() + battle_config.WOE_K_DELAY_AB_DUPLITE_MELEE;
				sd->state.K_LOCK_AB_DUPLIGHT_MELEE = 0;
		} else {
			sd->K_CHK_AB_DUPLELIGHT_MELEE = gettick() + battle_config.K_DELAY_AB_DUPLELIGHT_MELEE;
			sd->state.K_LOCK_AB_DUPLIGHT_MELEE = 0;
		}
		break;
	case AB_DUPLELIGHT_MAGIC:
		if(DIFF_TICK(sd->K_CHK_AB_DUPLELIGHT_MAGIC,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_DUPLELIGHT_MAGIC,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_DUPLELIGHT_MAGIC - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_DUPLIGHT_MAGIC = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_DUPLELIGHT_MAGIC = gettick() + battle_config.WOE_K_DELAY_AB_DUPLITE_MAGIC;
				sd->state.K_LOCK_AB_DUPLIGHT_MAGIC = 0;
		} else {
			sd->K_CHK_AB_DUPLELIGHT_MAGIC = gettick() + battle_config.K_DELAY_AB_DUPLELIGHT_MAGIC;
			sd->state.K_LOCK_AB_DUPLIGHT_MAGIC = 0;
		}
		break;
	case AB_SILENTIUM:
		if(DIFF_TICK(sd->K_CHK_AB_SILENTIUM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_SILENTIUM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_SILENTIUM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_SILENTIUM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_SILENTIUM = gettick() + battle_config.WOE_K_DELAY_AB_SILENTIUM;
				sd->state.K_LOCK_AB_SILENTIUM = 0;
		} else {
			sd->K_CHK_AB_SILENTIUM = gettick() + battle_config.K_DELAY_AB_SILENTIUM;
			sd->state.K_LOCK_AB_SILENTIUM = 0;
		}
		break;
	case WL_WHITEIMPRISON:
		if(DIFF_TICK(sd->K_CHK_WL_WHITEIMPRISON,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_WHITEIMPRISON,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_WHITEIMPRISON - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_WHITEIMPRISON = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_WHITEIMPRISON = gettick() + battle_config.WOE_K_DELAY_WL_WHITEIMPRISON;
				sd->state.K_LOCK_WL_WHITEIMPRISON = 0;
		} else {
			sd->K_CHK_WL_WHITEIMPRISON = gettick() + battle_config.K_DELAY_WL_WHITEIMPRISON;
			sd->state.K_LOCK_WL_WHITEIMPRISON = 0;
		}
		break;
	case WL_SOULEXPANSION:
		if(DIFF_TICK(sd->K_CHK_WL_SOULEXPANSION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_SOULEXPANSION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_SOULEXPANSION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_SOULEXPANSION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_SOULEXPANSION = gettick() + battle_config.WOE_K_DELAY_WL_SOULEXPANSION;
				sd->state.K_LOCK_WL_SOULEXPANSION = 0;
		} else {
			sd->K_CHK_WL_SOULEXPANSION = gettick() + battle_config.K_DELAY_WL_SOULEXPANSION;
			sd->state.K_LOCK_WL_SOULEXPANSION = 0;
		}
		break;
	case WL_FROSTMISTY:
		if(DIFF_TICK(sd->K_CHK_WL_FROSTMISTY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_FROSTMISTY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_FROSTMISTY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_FROSTMISTY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_FROSTMISTY = gettick() + battle_config.WOE_K_DELAY_WL_FROSTMISTY;
				sd->state.K_LOCK_WL_FROSTMISTY = 0;
		} else {
			sd->K_CHK_WL_FROSTMISTY = gettick() + battle_config.K_DELAY_WL_FROSTMISTY;
			sd->state.K_LOCK_WL_FROSTMISTY = 0;
		}
		break;
	case WL_JACKFROST:
		if(DIFF_TICK(sd->K_CHK_WL_JACKFROST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_JACKFROST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_JACKFROST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_JACKFROST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_JACKFROST = gettick() + battle_config.WOE_K_DELAY_WL_JACKFROST;
				sd->state.K_LOCK_WL_JACKFROST = 0;
		} else {
			sd->K_CHK_WL_JACKFROST = gettick() + battle_config.K_DELAY_WL_JACKFROST;
			sd->state.K_LOCK_WL_JACKFROST = 0;
		}
		break;
	case WL_MARSHOFABYSS:
		if(DIFF_TICK(sd->K_CHK_WL_MARSHOFABYSS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_MARSHOFABYSS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_MARSHOFABYSS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_MARSHOFABYSS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_MARSHOFABYSS = gettick() + battle_config.WOE_K_DELAY_WL_MARSHOFABYSS;
				sd->state.K_LOCK_WL_MARSHOFABYSS = 0;
		} else {
			sd->K_CHK_WL_MARSHOFABYSS = gettick() + battle_config.K_DELAY_WL_MARSHOFABYSS;
			sd->state.K_LOCK_WL_MARSHOFABYSS = 0;
		}
		break;
	case WL_RADIUS:
		if(DIFF_TICK(sd->K_CHK_WL_RADIUS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_RADIUS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_RADIUS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_RADIUS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_RADIUS = gettick() + battle_config.WOE_K_DELAY_WL_RADIUS;
				sd->state.K_LOCK_WL_RADIUS = 0;
		} else {
			sd->K_CHK_WL_RADIUS = gettick() + battle_config.K_DELAY_WL_RADIUS;
			sd->state.K_LOCK_WL_RADIUS = 0;
		}
		break;
	case WL_STASIS:
		if(DIFF_TICK(sd->K_CHK_WL_STASIS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_STASIS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_STASIS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_STASIS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_STASIS = gettick() + battle_config.WOE_K_DELAY_WL_STASIS;
				sd->state.K_LOCK_WL_STASIS = 0;
		} else {
			sd->K_CHK_WL_STASIS = gettick() + battle_config.K_DELAY_WL_STASIS;
			sd->state.K_LOCK_WL_STASIS = 0;
		}
		break;
	case WL_DRAINLIFE:
		if(DIFF_TICK(sd->K_CHK_WL_DRAINLIFE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_DRAINLIFE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_DRAINLIFE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_DRAINLIFE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_DRAINLIFE = gettick() + battle_config.WOE_K_DELAY_WL_DRAINLIFE;
				sd->state.K_LOCK_WL_DRAINLIFE = 0;
		} else {
			sd->K_CHK_WL_DRAINLIFE = gettick() + battle_config.K_DELAY_WL_DRAINLIFE;
			sd->state.K_LOCK_WL_DRAINLIFE = 0;
		}
		break;
	case WL_CRIMSONROCK:
		if(DIFF_TICK(sd->K_CHK_WL_CRIMSONROCK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_CRIMSONROCK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_CRIMSONROCK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_CRIMSONROCK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_CRIMSONROCK = gettick() + battle_config.WOE_K_DELAY_WL_CRIMSONROCK;
				sd->state.K_LOCK_WL_CRIMSONROCK = 0;
		} else {
			sd->K_CHK_WL_CRIMSONROCK = gettick() + battle_config.K_DELAY_WL_CRIMSONROCK;
			sd->state.K_LOCK_WL_CRIMSONROCK = 0;
		}
		break;
	case WL_HELLINFERNO:
		if(DIFF_TICK(sd->K_CHK_WL_HELLINFERNO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_HELLINFERNO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_HELLINFERNO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_HELLINFERNO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_HELLINFERNO = gettick() + battle_config.WOE_K_DELAY_WL_HELLINFERNO;
				sd->state.K_LOCK_WL_HELLINFERNO = 0;
		} else {
			sd->K_CHK_WL_HELLINFERNO = gettick() + battle_config.K_DELAY_WL_HELLINFERNO;
			sd->state.K_LOCK_WL_HELLINFERNO = 0;
		}
		break;
	case WL_COMET:
		if(DIFF_TICK(sd->K_CHK_WL_COMET,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_COMET,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_COMET - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_COMET = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_COMET = gettick() + battle_config.WOE_K_DELAY_WL_COMET;
				sd->state.K_LOCK_WL_COMET = 0;
		} else {
			sd->K_CHK_WL_COMET = gettick() + battle_config.K_DELAY_WL_COMET;
			sd->state.K_LOCK_WL_COMET = 0;
		}
		break;
	case WL_CHAINLIGHTNING:
		if(DIFF_TICK(sd->K_CHK_WL_CHAINLIGHTNING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_CHAINLIGHTNING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_CHAINLIGHTNING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_CHAINLIGHTNING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_CHAINLIGHTNING = gettick() + battle_config.WOE_K_DELAY_WL_CHAIN;
				sd->state.K_LOCK_WL_CHAINLIGHTNING = 0;
		} else {
			sd->K_CHK_WL_CHAINLIGHTNING = gettick() + battle_config.K_DELAY_WL_CHAINLIGHTNING;
			sd->state.K_LOCK_WL_CHAINLIGHTNING = 0;
		}
		break;
	case WL_CHAINLIGHTNING_ATK:
		if(DIFF_TICK(sd->K_CHK_WL_CHAINLIGHTNING_ATK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_CHAINLIGHTNING_ATK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_CHAINLIGHTNING_ATK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_CHAINLIGHTNING_ = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_CHAINLIGHTNING_ATK = gettick() + battle_config.WOE_K_DELAY_WL_CHAIN_;
				sd->state.K_LOCK_WL_CHAINLIGHTNING_ = 0;
		} else {
			sd->K_CHK_WL_CHAINLIGHTNING_ATK = gettick() + battle_config.K_DELAY_WL_CHAINLIGHTNING_ATK;
			sd->state.K_LOCK_WL_CHAINLIGHTNING_ = 0;
		}
		break;
	case WL_EARTHSTRAIN:
		if(DIFF_TICK(sd->K_CHK_WL_EARTHSTRAIN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_EARTHSTRAIN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_EARTHSTRAIN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_EARTHSTRAIN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_EARTHSTRAIN = gettick() + battle_config.WOE_K_DELAY_WL_EARTHSTRAIN;
				sd->state.K_LOCK_WL_EARTHSTRAIN = 0;
		} else {
			sd->K_CHK_WL_EARTHSTRAIN = gettick() + battle_config.K_DELAY_WL_EARTHSTRAIN;
			sd->state.K_LOCK_WL_EARTHSTRAIN = 0;
		}
		break;
	case WL_TETRAVORTEX:
		if(DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_TETRAVORTEX - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_TETRAVORTEX = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_TETRAVORTEX = gettick() + battle_config.WOE_K_DELAY_WL_TETRA;
				sd->state.K_LOCK_WL_TETRAVORTEX = 0;
		} else {
			sd->K_CHK_WL_TETRAVORTEX = gettick() + battle_config.K_DELAY_WL_TETRAVORTEX;
			sd->state.K_LOCK_WL_TETRAVORTEX = 0;
		}
		break;
	case WL_TETRAVORTEX_FIRE:
		if(DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX_FIRE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX_FIRE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_TETRAVORTEX_FIRE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_TETRA_FIRE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_TETRAVORTEX_FIRE = gettick() + battle_config.WOE_K_DELAY_WL_TETRA_FIRE;
				sd->state.K_LOCK_WL_TETRA_FIRE = 0;
		} else {
			sd->K_CHK_WL_TETRAVORTEX_FIRE = gettick() + battle_config.K_DELAY_WL_TETRAVORTEX_FIRE;
			sd->state.K_LOCK_WL_TETRA_FIRE = 0;
		}
		break;
	case WL_TETRAVORTEX_WATER:
		if(DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX_WATER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX_WATER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_TETRAVORTEX_WATER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_TETRA_WATER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_TETRAVORTEX_WATER = gettick() + battle_config.WOE_K_DELAY_WL_TETRA_WATER;
				sd->state.K_LOCK_WL_TETRA_WATER = 0;
		} else {
			sd->K_CHK_WL_TETRAVORTEX_WATER = gettick() + battle_config.K_DELAY_WL_TETRAVORTEX_WATER;
			sd->state.K_LOCK_WL_TETRA_WATER = 0;
		}
		break;
	case WL_TETRAVORTEX_WIND:
		if(DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX_WIND,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX_WIND,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_TETRAVORTEX_WIND - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_TETRA_WIND = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_TETRAVORTEX_WIND = gettick() + battle_config.WOE_K_DELAY_WL_TETRA_WIND;
				sd->state.K_LOCK_WL_TETRA_WIND = 0;
		} else {
			sd->K_CHK_WL_TETRAVORTEX_WIND = gettick() + battle_config.K_DELAY_WL_TETRAVORTEX_WIND;
			sd->state.K_LOCK_WL_TETRA_WIND = 0;
		}
		break;
	case WL_TETRAVORTEX_GROUND:
		if(DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX_GROUND,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_TETRAVORTEX_GROUND,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_TETRAVORTEX_GROUND - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_TETRA_GROUND = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_TETRAVORTEX_GROUND = gettick() + battle_config.WOE_K_DELAY_WL_TETRA_GROUND;
				sd->state.K_LOCK_WL_TETRA_GROUND = 0;
		} else {
			sd->K_CHK_WL_TETRAVORTEX_GROUND = gettick() + battle_config.K_DELAY_WL_TETRAVORTEX_GROUND;
			sd->state.K_LOCK_WL_TETRA_GROUND = 0;
		}
		break;
	case WL_RELEASE:
		if(DIFF_TICK(sd->K_CHK_WL_RELEASE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_RELEASE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_RELEASE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_RELEASE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_RELEASE = gettick() + battle_config.WOE_K_DELAY_WL_RELEASE;
				sd->state.K_LOCK_WL_RELEASE = 0;
		} else {
			sd->K_CHK_WL_RELEASE = gettick() + battle_config.K_DELAY_WL_RELEASE;
			sd->state.K_LOCK_WL_RELEASE = 0;
		}
		break;
	case WL_READING_SB:
		if(DIFF_TICK(sd->K_CHK_WL_READING_SB,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_READING_SB,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_READING_SB - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_READING_SB = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_READING_SB = gettick() + battle_config.WOE_K_DELAY_WL_READING_SB;
				sd->state.K_LOCK_WL_READING_SB = 0;
		} else {
			sd->K_CHK_WL_READING_SB = gettick() + battle_config.K_DELAY_WL_READING_SB;
			sd->state.K_LOCK_WL_READING_SB = 0;
		}
		break;
	case WL_FREEZE_SP:
		if(DIFF_TICK(sd->K_CHK_WL_FREEZE_SP,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WL_FREEZE_SP,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WL_FREEZE_SP - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WL_FREEZE_SP = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WL_FREEZE_SP = gettick() + battle_config.WOE_K_DELAY_WL_FREEZE_SP;
				sd->state.K_LOCK_WL_FREEZE_SP = 0;
		} else {
			sd->K_CHK_WL_FREEZE_SP = gettick() + battle_config.K_DELAY_WL_FREEZE_SP;
			sd->state.K_LOCK_WL_FREEZE_SP = 0;
		}
		break;
	case RA_ARROWSTORM:
		if(DIFF_TICK(sd->K_CHK_RA_ARROWSTORM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RA_ARROWSTORM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RA_ARROWSTORM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RA_ARROWSTORM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RA_ARROWSTORM = gettick() + battle_config.WOE_K_DELAY_RA_ARROWSTORM;
				sd->state.K_LOCK_RA_ARROWSTORM = 0;
		} else {
			sd->K_CHK_RA_ARROWSTORM = gettick() + battle_config.K_DELAY_RA_ARROWSTORM;
			sd->state.K_LOCK_RA_ARROWSTORM = 0;
		}
		break;
	case RA_AIMEDBOLT:
		if(DIFF_TICK(sd->K_CHK_RA_AIMEDBOLT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RA_AIMEDBOLT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RA_AIMEDBOLT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RA_AIMEDBOLT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RA_AIMEDBOLT = gettick() + battle_config.WOE_K_DELAY_RA_AIMEDBOLT;
				sd->state.K_LOCK_RA_AIMEDBOLT = 0;
		} else {
			sd->K_CHK_RA_AIMEDBOLT = gettick() + battle_config.K_DELAY_RA_AIMEDBOLT;
			sd->state.K_LOCK_RA_AIMEDBOLT = 0;
		}
		break;
	case RA_WUGSTRIKE:
		if(DIFF_TICK(sd->K_CHK_RA_WUGSTRIKE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RA_WUGSTRIKE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RA_WUGSTRIKE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RA_WUGSTRIKE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RA_WUGSTRIKE = gettick() + battle_config.WOE_K_DELAY_RA_WUGSTRIKE;
				sd->state.K_LOCK_RA_WUGSTRIKE = 0;
		} else {
			sd->K_CHK_RA_WUGSTRIKE = gettick() + battle_config.K_DELAY_RA_WUGSTRIKE;
			sd->state.K_LOCK_RA_WUGSTRIKE = 0;
		}
		break;
	case RA_WUGBITE:
		if(DIFF_TICK(sd->K_CHK_RA_WUGBITE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_RA_WUGBITE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_RA_WUGBITE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_RA_WUGBITE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_RA_WUGBITE = gettick() + battle_config.WOE_K_DELAY_RA_WUGBITE;
				sd->state.K_LOCK_RA_WUGBITE = 0;
		} else {
			sd->K_CHK_RA_WUGBITE = gettick() + battle_config.K_DELAY_RA_WUGBITE;
			sd->state.K_LOCK_RA_WUGBITE = 0;
		}
		break;
	case NC_BOOSTKNUCKLE:
		if(DIFF_TICK(sd->K_CHK_NC_BOOSTKNUCKLE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_BOOSTKNUCKLE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_BOOSTKNUCKLE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_BOOSTKNUCKLE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_BOOSTKNUCKLE = gettick() + battle_config.WOE_K_DELAY_NC_BOOSTKNUCKLE;
				sd->state.K_LOCK_NC_BOOSTKNUCKLE = 0;
		} else {
			sd->K_CHK_NC_BOOSTKNUCKLE = gettick() + battle_config.K_DELAY_NC_BOOSTKNUCKLE;
			sd->state.K_LOCK_NC_BOOSTKNUCKLE = 0;
		}
		break;
	case NC_PILEBUNKER:
		if(DIFF_TICK(sd->K_CHK_NC_PILEBUNKER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_PILEBUNKER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_PILEBUNKER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_PILEBUNKER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_PILEBUNKER = gettick() + battle_config.WOE_K_DELAY_NC_PILEBUNKER;
				sd->state.K_LOCK_NC_PILEBUNKER = 0;
		} else {
			sd->K_CHK_NC_PILEBUNKER = gettick() + battle_config.K_DELAY_NC_PILEBUNKER;
			sd->state.K_LOCK_NC_PILEBUNKER = 0;
		}
		break;
	case NC_VULCANARM:
		if(DIFF_TICK(sd->K_CHK_NC_VULCANARM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_VULCANARM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_VULCANARM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_VULCANARM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_VULCANARM = gettick() + battle_config.WOE_K_DELAY_NC_VULCANARM;
				sd->state.K_LOCK_NC_VULCANARM = 0;
		} else {
			sd->K_CHK_NC_VULCANARM = gettick() + battle_config.K_DELAY_NC_VULCANARM;
			sd->state.K_LOCK_NC_VULCANARM = 0;
		}
		break;
	case NC_FLAMELAUNCHER:
		if(DIFF_TICK(sd->K_CHK_NC_FLAMELAUNCHER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_FLAMELAUNCHER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_FLAMELAUNCHER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_FLAMELAUNCHER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_FLAMELAUNCHER = gettick() + battle_config.WOE_K_DELAY_NC_FLAMELAUNCHER;
				sd->state.K_LOCK_NC_FLAMELAUNCHER = 0;
		} else {
			sd->K_CHK_NC_FLAMELAUNCHER = gettick() + battle_config.K_DELAY_NC_FLAMELAUNCHER;
			sd->state.K_LOCK_NC_FLAMELAUNCHER = 0;
		}
		break;
	case NC_COLDSLOWER:
		if(DIFF_TICK(sd->K_CHK_NC_COLDSLOWER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_COLDSLOWER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_COLDSLOWER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_COLDSLOWER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_COLDSLOWER = gettick() + battle_config.WOE_K_DELAY_NC_COLDSLOWER;
				sd->state.K_LOCK_NC_COLDSLOWER = 0;
		} else {
			sd->K_CHK_NC_COLDSLOWER = gettick() + battle_config.K_DELAY_NC_COLDSLOWER;
			sd->state.K_LOCK_NC_COLDSLOWER = 0;
		}
		break;
	case NC_ARMSCANNON:
		if(DIFF_TICK(sd->K_CHK_NC_ARMSCANNON,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_ARMSCANNON,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_ARMSCANNON - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_ARMSCANNON = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_ARMSCANNON = gettick() + battle_config.WOE_K_DELAY_NC_ARMSCANNON;
				sd->state.K_LOCK_NC_ARMSCANNON = 0;
		} else {
			sd->K_CHK_NC_ARMSCANNON = gettick() + battle_config.K_DELAY_NC_ARMSCANNON;
			sd->state.K_LOCK_NC_ARMSCANNON = 0;
		}
		break;
	case NC_ACCELERATION:
		if(DIFF_TICK(sd->K_CHK_NC_ACCELERATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_ACCELERATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_ACCELERATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_ACCELERATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_ACCELERATION = gettick() + battle_config.WOE_K_DELAY_NC_ACCELERATION;
				sd->state.K_LOCK_NC_ACCELERATION = 0;
		} else {
			sd->K_CHK_NC_ACCELERATION = gettick() + battle_config.K_DELAY_NC_ACCELERATION;
			sd->state.K_LOCK_NC_ACCELERATION = 0;
		}
		break;
	case NC_F_SIDESLIDE:
		if(DIFF_TICK(sd->K_CHK_NC_F_SIDESLIDE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_F_SIDESLIDE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_F_SIDESLIDE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_F_SIDESLIDE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_F_SIDESLIDE = gettick() + battle_config.WOE_K_DELAY_NC_F_SIDESLIDE;
				sd->state.K_LOCK_NC_F_SIDESLIDE = 0;
		} else {
			sd->K_CHK_NC_F_SIDESLIDE = gettick() + battle_config.K_DELAY_NC_F_SIDESLIDE;
			sd->state.K_LOCK_NC_F_SIDESLIDE = 0;
		}
		break;
	case NC_B_SIDESLIDE:
		if(DIFF_TICK(sd->K_CHK_NC_B_SIDESLIDE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_B_SIDESLIDE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_B_SIDESLIDE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_B_SIDESLIDE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_B_SIDESLIDE = gettick() + battle_config.WOE_K_DELAY_NC_B_SIDESLIDE;
				sd->state.K_LOCK_NC_B_SIDESLIDE = 0;
		} else {
			sd->K_CHK_NC_B_SIDESLIDE = gettick() + battle_config.K_DELAY_NC_B_SIDESLIDE;
			sd->state.K_LOCK_NC_B_SIDESLIDE = 0;
		}
		break;
	case NC_MAINFRAME:
		if(DIFF_TICK(sd->K_CHK_NC_MAINFRAME,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_MAINFRAME,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_MAINFRAME - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_MAINFRAME = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_MAINFRAME = gettick() + battle_config.WOE_K_DELAY_NC_MAINFRAME;
				sd->state.K_LOCK_NC_MAINFRAME = 0;
		} else {
			sd->K_CHK_NC_MAINFRAME = gettick() + battle_config.K_DELAY_NC_MAINFRAME;
			sd->state.K_LOCK_NC_MAINFRAME = 0;
		}
		break;
	case NC_SHAPESHIFT:
		if(DIFF_TICK(sd->K_CHK_NC_SHAPESHIFT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_SHAPESHIFT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_SHAPESHIFT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_SHAPESHIFT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_SHAPESHIFT = gettick() + battle_config.WOE_K_DELAY_NC_SHAPESHIFT;
				sd->state.K_LOCK_NC_SHAPESHIFT = 0;
		} else {
			sd->K_CHK_NC_SHAPESHIFT = gettick() + battle_config.K_DELAY_NC_SHAPESHIFT;
			sd->state.K_LOCK_NC_SHAPESHIFT = 0;
		}
		break;
	case NC_INFRAREDSCAN:
		if(DIFF_TICK(sd->K_CHK_NC_INFRAREDSCAN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_INFRAREDSCAN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_INFRAREDSCAN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_INFRAREDSCAN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_INFRAREDSCAN = gettick() + battle_config.WOE_K_DELAY_NC_INFRAREDSCAN;
				sd->state.K_LOCK_NC_INFRAREDSCAN = 0;
		} else {
			sd->K_CHK_NC_INFRAREDSCAN = gettick() + battle_config.K_DELAY_NC_INFRAREDSCAN;
			sd->state.K_LOCK_NC_INFRAREDSCAN = 0;
		}
		break;
	case NC_ANALYZE:
		if(DIFF_TICK(sd->K_CHK_NC_ANALYZE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_ANALYZE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_ANALYZE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_ANALYZE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_ANALYZE = gettick() + battle_config.WOE_K_DELAY_NC_ANALYZE;
				sd->state.K_LOCK_NC_ANALYZE = 0;
		} else {
			sd->K_CHK_NC_ANALYZE = gettick() + battle_config.K_DELAY_NC_ANALYZE;
			sd->state.K_LOCK_NC_ANALYZE = 0;
		}
		break;
	case NC_MAGNETICFIELD:
		if(DIFF_TICK(sd->K_CHK_NC_MAGNETICFIELD,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_MAGNETICFIELD,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_MAGNETICFIELD - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_MAGNETICFIELD = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_MAGNETICFIELD = gettick() + battle_config.WOE_K_DELAY_NC_MAGNETICFIELD;
				sd->state.K_LOCK_NC_MAGNETICFIELD = 0;
		} else {
			sd->K_CHK_NC_MAGNETICFIELD = gettick() + battle_config.K_DELAY_NC_MAGNETICFIELD;
			sd->state.K_LOCK_NC_MAGNETICFIELD = 0;
		}
		break;
	case NC_NEUTRALBARRIER:
		if(DIFF_TICK(sd->K_CHK_NC_NEUTRALBARRIER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_NEUTRALBARRIER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_NEUTRALBARRIER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_NEUTRALBARRIER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_NEUTRALBARRIER = gettick() + battle_config.WOE_K_DELAY_NC_NEUTRALBARRIER;
				sd->state.K_LOCK_NC_NEUTRALBARRIER = 0;
		} else {
			sd->K_CHK_NC_NEUTRALBARRIER = gettick() + battle_config.K_DELAY_NC_NEUTRALBARRIER;
			sd->state.K_LOCK_NC_NEUTRALBARRIER = 0;
		}
		break;
	case NC_STEALTHFIELD:
		if(DIFF_TICK(sd->K_CHK_NC_STEALTHFIELD,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_STEALTHFIELD,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_STEALTHFIELD - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_STEALTHFIELD = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_STEALTHFIELD = gettick() + battle_config.WOE_K_DELAY_NC_STEALTHFIELD;
				sd->state.K_LOCK_NC_STEALTHFIELD = 0;
		} else {
			sd->K_CHK_NC_STEALTHFIELD = gettick() + battle_config.K_DELAY_NC_STEALTHFIELD;
			sd->state.K_LOCK_NC_STEALTHFIELD = 0;
		}
		break;
	case NC_AXEBOOMERANG:
		if(DIFF_TICK(sd->K_CHK_NC_AXEBOOMERANG,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_AXEBOOMERANG,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_AXEBOOMERANG - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_AXEBOOMERANG = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_AXEBOOMERANG = gettick() + battle_config.WOE_K_DELAY_NC_AXEBOOMERANG;
				sd->state.K_LOCK_NC_AXEBOOMERANG = 0;
		} else {
			sd->K_CHK_NC_AXEBOOMERANG = gettick() + battle_config.K_DELAY_NC_AXEBOOMERANG;
			sd->state.K_LOCK_NC_AXEBOOMERANG = 0;
		}
		break;
	case NC_POWERSWING:
		if(DIFF_TICK(sd->K_CHK_NC_POWERSWING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_POWERSWING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_POWERSWING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_POWERSWING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_POWERSWING = gettick() + battle_config.WOE_K_DELAY_NC_POWERSWING;
				sd->state.K_LOCK_NC_POWERSWING = 0;
		} else {
			sd->K_CHK_NC_POWERSWING = gettick() + battle_config.K_DELAY_NC_POWERSWING;
			sd->state.K_LOCK_NC_POWERSWING = 0;
		}
		break;
	case NC_AXETORNADO:
		if(DIFF_TICK(sd->K_CHK_NC_AXETORNADO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_AXETORNADO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_AXETORNADO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_AXETORNADO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_AXETORNADO = gettick() + battle_config.WOE_K_DELAY_NC_AXETORNADO;
				sd->state.K_LOCK_NC_AXETORNADO = 0;
		} else {
			sd->K_CHK_NC_AXETORNADO = gettick() + battle_config.K_DELAY_NC_AXETORNADO;
			sd->state.K_LOCK_NC_AXETORNADO = 0;
		}
		break;
	case NC_SILVERSNIPER:
		if(DIFF_TICK(sd->K_CHK_NC_SILVERSNIPER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_SILVERSNIPER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_SILVERSNIPER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_SILVERSNIPER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_SILVERSNIPER = gettick() + battle_config.WOE_K_DELAY_NC_SILVERSNIPER;
				sd->state.K_LOCK_NC_SILVERSNIPER = 0;
		} else {
			sd->K_CHK_NC_SILVERSNIPER = gettick() + battle_config.K_DELAY_NC_SILVERSNIPER;
			sd->state.K_LOCK_NC_SILVERSNIPER = 0;
		}
		break;
	case NC_MAGICDECOY:
		if(DIFF_TICK(sd->K_CHK_NC_MAGICDECOY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_MAGICDECOY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_MAGICDECOY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_MAGICDECOY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_MAGICDECOY = gettick() + battle_config.WOE_K_DELAY_NC_MAGICDECOY;
				sd->state.K_LOCK_NC_MAGICDECOY = 0;
		} else {
			sd->K_CHK_NC_MAGICDECOY = gettick() + battle_config.K_DELAY_NC_MAGICDECOY;
			sd->state.K_LOCK_NC_MAGICDECOY = 0;
		}
		break;
	case NC_DISJOINT:
		if(DIFF_TICK(sd->K_CHK_NC_DISJOINT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_NC_DISJOINT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_NC_DISJOINT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_NC_DISJOINT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_NC_DISJOINT = gettick() + battle_config.WOE_K_DELAY_NC_DISJOINT;
				sd->state.K_LOCK_NC_DISJOINT = 0;
		} else {
			sd->K_CHK_NC_DISJOINT = gettick() + battle_config.K_DELAY_NC_DISJOINT;
			sd->state.K_LOCK_NC_DISJOINT = 0;
		}
		break;
	case SC_FATALMENACE:
		if(DIFF_TICK(sd->K_CHK_SC_FATALMENACE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_FATALMENACE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_FATALMENACE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_FATALMENACE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_FATALMENACE = gettick() + battle_config.WOE_K_DELAY_SC_FATALMENACE;
				sd->state.K_LOCK_SC_FATALMENACE = 0;
		} else {
			sd->K_CHK_SC_FATALMENACE = gettick() + battle_config.K_DELAY_SC_FATALMENACE;
			sd->state.K_LOCK_SC_FATALMENACE = 0;
		}
		break;
	case SC_TRIANGLESHOT:
		if(DIFF_TICK(sd->K_CHK_SC_TRIANGLESHOT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_TRIANGLESHOT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_TRIANGLESHOT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_TRIANGLESHOT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_TRIANGLESHOT = gettick() + battle_config.WOE_K_DELAY_SC_TRIANGLESHOT;
				sd->state.K_LOCK_SC_TRIANGLESHOT = 0;
		} else {
			sd->K_CHK_SC_TRIANGLESHOT = gettick() + battle_config.K_DELAY_SC_TRIANGLESHOT;
			sd->state.K_LOCK_SC_TRIANGLESHOT = 0;
		}
		break;
	case SC_INVISIBILITY:
		if(DIFF_TICK(sd->K_CHK_SC_INVISIBILITY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_INVISIBILITY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_INVISIBILITY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_INVISIBILITY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_INVISIBILITY = gettick() + battle_config.WOE_K_DELAY_SC_INVISIBILITY;
				sd->state.K_LOCK_SC_INVISIBILITY = 0;
		} else {
			sd->K_CHK_SC_INVISIBILITY = gettick() + battle_config.K_DELAY_SC_INVISIBILITY;
			sd->state.K_LOCK_SC_INVISIBILITY = 0;
		}
		break;
	case SC_ENERVATION:
		if(DIFF_TICK(sd->K_CHK_SC_ENERVATION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_ENERVATION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_ENERVATION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_ENERVATION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_ENERVATION = gettick() + battle_config.WOE_K_DELAY_SC_ENERVATION;
				sd->state.K_LOCK_SC_ENERVATION = 0;
		} else {
			sd->K_CHK_SC_ENERVATION = gettick() + battle_config.K_DELAY_SC_ENERVATION;
			sd->state.K_LOCK_SC_ENERVATION = 0;
		}
		break;
	case SC_GROOMY:
		if(DIFF_TICK(sd->K_CHK_SC_GROOMY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_GROOMY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_GROOMY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_GROOMY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_GROOMY = gettick() + battle_config.WOE_K_DELAY_SC_GROOMY;
				sd->state.K_LOCK_SC_GROOMY = 0;
		} else {
			sd->K_CHK_SC_GROOMY = gettick() + battle_config.K_DELAY_SC_GROOMY;
			sd->state.K_LOCK_SC_GROOMY = 0;
		}
		break;
	case SC_IGNORANCE:
		if(DIFF_TICK(sd->K_CHK_SC_IGNORANCE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_IGNORANCE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_IGNORANCE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_IGNORANCE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_IGNORANCE = gettick() + battle_config.WOE_K_DELAY_SC_IGNORANCE;
				sd->state.K_LOCK_SC_IGNORANCE = 0;
		} else {
			sd->K_CHK_SC_IGNORANCE = gettick() + battle_config.K_DELAY_SC_IGNORANCE;
			sd->state.K_LOCK_SC_IGNORANCE = 0;
		}
		break;
	case SC_LAZINESS:
		if(DIFF_TICK(sd->K_CHK_SC_LAZINESS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_LAZINESS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_LAZINESS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_LAZINESS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_LAZINESS = gettick() + battle_config.WOE_K_DELAY_SC_LAZINESS;
				sd->state.K_LOCK_SC_LAZINESS = 0;
		} else {
			sd->K_CHK_SC_LAZINESS = gettick() + battle_config.K_DELAY_SC_LAZINESS;
			sd->state.K_LOCK_SC_LAZINESS = 0;
		}
		break;
	case SC_UNLUCKY:
		if(DIFF_TICK(sd->K_CHK_SC_UNLUCKY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_UNLUCKY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_UNLUCKY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_UNLUCKY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_UNLUCKY = gettick() + battle_config.WOE_K_DELAY_SC_UNLUCKY;
				sd->state.K_LOCK_SC_UNLUCKY = 0;
		} else {
			sd->K_CHK_SC_UNLUCKY = gettick() + battle_config.K_DELAY_SC_UNLUCKY;
			sd->state.K_LOCK_SC_UNLUCKY = 0;
		}
		break;
	case SC_WEAKNESS:
		if(DIFF_TICK(sd->K_CHK_SC_WEAKNESS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_WEAKNESS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_WEAKNESS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_WEAKNESS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_WEAKNESS = gettick() + battle_config.WOE_K_DELAY_SC_WEAKNESS;
				sd->state.K_LOCK_SC_WEAKNESS = 0;
		} else {
			sd->K_CHK_SC_WEAKNESS = gettick() + battle_config.K_DELAY_SC_WEAKNESS;
			sd->state.K_LOCK_SC_WEAKNESS = 0;
		}
		break;
	case SC_STRIPACCESSARY:
		if(DIFF_TICK(sd->K_CHK_SC_STRIPACCESSARY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_STRIPACCESSARY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_STRIPACCESSARY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_STRIPACCESSARY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_STRIPACCESSARY = gettick() + battle_config.WOE_K_DELAY_SC_STRIPACCESSARY;
				sd->state.K_LOCK_SC_STRIPACCESSARY = 0;
		} else {
			sd->K_CHK_SC_STRIPACCESSARY = gettick() + battle_config.K_DELAY_SC_STRIPACCESSARY;
			sd->state.K_LOCK_SC_STRIPACCESSARY = 0;
		}
		break;
	case SC_MANHOLE:
		if(DIFF_TICK(sd->K_CHK_SC_MANHOLE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_MANHOLE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_MANHOLE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_MANHOLE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_MANHOLE = gettick() + battle_config.WOE_K_DELAY_SC_MANHOLE;
				sd->state.K_LOCK_SC_MANHOLE = 0;
		} else {
			sd->K_CHK_SC_MANHOLE = gettick() + battle_config.K_DELAY_SC_MANHOLE;
			sd->state.K_LOCK_SC_MANHOLE = 0;
		}
		break;
	case SC_DIMENSIONDOOR:
		if(DIFF_TICK(sd->K_CHK_SC_DIMENSIONDOOR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_DIMENSIONDOOR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_DIMENSIONDOOR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_DIMENSIONDOOR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_DIMENSIONDOOR = gettick() + battle_config.WOE_K_DELAY_SC_DIMENSIONDOOR;
				sd->state.K_LOCK_SC_DIMENSIONDOOR = 0;
		} else {
			sd->K_CHK_SC_DIMENSIONDOOR = gettick() + battle_config.K_DELAY_SC_DIMENSIONDOOR;
			sd->state.K_LOCK_SC_DIMENSIONDOOR = 0;
		}
		break;
	case SC_CHAOSPANIC:
		if(DIFF_TICK(sd->K_CHK_SC_CHAOSPANIC,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_CHAOSPANIC,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_CHAOSPANIC - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_CHAOSPANIC = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_CHAOSPANIC = gettick() + battle_config.WOE_K_DELAY_SC_CHAOSPANIC;
				sd->state.K_LOCK_SC_CHAOSPANIC = 0;
		} else {
			sd->K_CHK_SC_CHAOSPANIC = gettick() + battle_config.K_DELAY_SC_CHAOSPANIC;
			sd->state.K_LOCK_SC_CHAOSPANIC = 0;
		}
		break;
	case SC_MAELSTROM:
		if(DIFF_TICK(sd->K_CHK_SC_MAELSTROM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_MAELSTROM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_MAELSTROM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_MAELSTROM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_MAELSTROM = gettick() + battle_config.WOE_K_DELAY_SC_MAELSTROM;
				sd->state.K_LOCK_SC_MAELSTROM = 0;
		} else {
			sd->K_CHK_SC_MAELSTROM = gettick() + battle_config.K_DELAY_SC_MAELSTROM;
			sd->state.K_LOCK_SC_MAELSTROM = 0;
		}
		break;
	case SC_BLOODYLUST:
		if(DIFF_TICK(sd->K_CHK_SC_BLOODYLUST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_BLOODYLUST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_BLOODYLUST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_BLOODYLUST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_BLOODYLUST = gettick() + battle_config.WOE_K_DELAY_SC_BLOODYLUST;
				sd->state.K_LOCK_SC_BLOODYLUST = 0;
		} else {
			sd->K_CHK_SC_BLOODYLUST = gettick() + battle_config.K_DELAY_SC_BLOODYLUST;
			sd->state.K_LOCK_SC_BLOODYLUST = 0;
		}
		break;
	case SC_FEINTBOMB:
		if(DIFF_TICK(sd->K_CHK_SC_FEINTBOMB,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SC_FEINTBOMB,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SC_FEINTBOMB - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SC_FEINTBOMB = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SC_FEINTBOMB = gettick() + battle_config.WOE_K_DELAY_SC_FEINTBOMB;
				sd->state.K_LOCK_SC_FEINTBOMB = 0;
		} else {
			sd->K_CHK_SC_FEINTBOMB = gettick() + battle_config.K_DELAY_SC_FEINTBOMB;
			sd->state.K_LOCK_SC_FEINTBOMB = 0;
		}
		break;
	case LG_CANNONSPEAR:
		if(DIFF_TICK(sd->K_CHK_LG_CANNONSPEAR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_CANNONSPEAR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_CANNONSPEAR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_CANNONSPEAR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_CANNONSPEAR = gettick() + battle_config.WOE_K_DELAY_LG_CANNONSPEAR;
				sd->state.K_LOCK_LG_CANNONSPEAR = 0;
		} else {
			sd->K_CHK_LG_CANNONSPEAR = gettick() + battle_config.K_DELAY_LG_CANNONSPEAR;
			sd->state.K_LOCK_LG_CANNONSPEAR = 0;
		}
		break;
	case LG_BANISHINGPOINT:
		if(DIFF_TICK(sd->K_CHK_LG_BANISHINGPOINT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_BANISHINGPOINT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_BANISHINGPOINT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_BANISHINGPOINT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_BANISHINGPOINT = gettick() + battle_config.WOE_K_DELAY_LG_BANISHINGPOINT;
				sd->state.K_LOCK_LG_BANISHINGPOINT = 0;
		} else {
			sd->K_CHK_LG_BANISHINGPOINT = gettick() + battle_config.K_DELAY_LG_BANISHINGPOINT;
			sd->state.K_LOCK_LG_BANISHINGPOINT = 0;
		}
		break;
	case LG_TRAMPLE:
		if(DIFF_TICK(sd->K_CHK_LG_TRAMPLE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_TRAMPLE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_TRAMPLE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_TRAMPLE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_TRAMPLE = gettick() + battle_config.WOE_K_DELAY_LG_TRAMPLE;
				sd->state.K_LOCK_LG_TRAMPLE = 0;
		} else {
			sd->K_CHK_LG_TRAMPLE = gettick() + battle_config.K_DELAY_LG_TRAMPLE;
			sd->state.K_LOCK_LG_TRAMPLE = 0;
		}
		break;
	case LG_PINPOINTATTACK:
		if(DIFF_TICK(sd->K_CHK_LG_PINPOINTATTACK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_PINPOINTATTACK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_PINPOINTATTACK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_PINPOINTATTACK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_PINPOINTATTACK = gettick() + battle_config.WOE_K_DELAY_LG_PINPOINTATTACK;
				sd->state.K_LOCK_LG_PINPOINTATTACK = 0;
		} else {
			sd->K_CHK_LG_PINPOINTATTACK = gettick() + battle_config.K_DELAY_LG_PINPOINTATTACK;
			sd->state.K_LOCK_LG_PINPOINTATTACK = 0;
		}
		break;
	case LG_RAGEBURST:
		if(DIFF_TICK(sd->K_CHK_LG_RAGEBURST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_RAGEBURST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_RAGEBURST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_RAGEBURST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_RAGEBURST = gettick() + battle_config.WOE_K_DELAY_LG_RAGEBURST;
				sd->state.K_LOCK_LG_RAGEBURST = 0;
		} else {
			sd->K_CHK_LG_RAGEBURST = gettick() + battle_config.K_DELAY_LG_RAGEBURST;
			sd->state.K_LOCK_LG_RAGEBURST = 0;
		}
		break;
	case LG_EXEEDBREAK:
		if(DIFF_TICK(sd->K_CHK_LG_EXEEDBREAK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_EXEEDBREAK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_EXEEDBREAK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_EXEEDBREAK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_EXEEDBREAK = gettick() + battle_config.WOE_K_DELAY_LG_EXEEDBREAK;
				sd->state.K_LOCK_LG_EXEEDBREAK = 0;
		} else {
			sd->K_CHK_LG_EXEEDBREAK = gettick() + battle_config.K_DELAY_LG_EXEEDBREAK;
			sd->state.K_LOCK_LG_EXEEDBREAK = 0;
		}
		break;
	case LG_OVERBRAND:
		if(DIFF_TICK(sd->K_CHK_LG_OVERBRAND,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_OVERBRAND,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_OVERBRAND - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_OVERBRAND = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_OVERBRAND = gettick() + battle_config.WOE_K_DELAY_LG_OVERBRAND;
				sd->state.K_LOCK_LG_OVERBRAND = 0;
		} else {
			sd->K_CHK_LG_OVERBRAND = gettick() + battle_config.K_DELAY_LG_OVERBRAND;
			sd->state.K_LOCK_LG_OVERBRAND = 0;
		}
		break;
	case LG_BANDING:
		if(DIFF_TICK(sd->K_CHK_LG_BANDING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_BANDING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_BANDING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_BANDING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_BANDING = gettick() + battle_config.WOE_K_DELAY_LG_BANDING;
				sd->state.K_LOCK_LG_BANDING = 0;
		} else {
			sd->K_CHK_LG_BANDING = gettick() + battle_config.K_DELAY_LG_BANDING;
			sd->state.K_LOCK_LG_BANDING = 0;
		}
		break;
	case LG_MOONSLASHER:
		if(DIFF_TICK(sd->K_CHK_LG_MOONSLASHER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_MOONSLASHER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_MOONSLASHER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_MOONSLASHER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_MOONSLASHER = gettick() + battle_config.WOE_K_DELAY_LG_MOONSLASHER;
				sd->state.K_LOCK_LG_MOONSLASHER = 0;
		} else {
			sd->K_CHK_LG_MOONSLASHER = gettick() + battle_config.K_DELAY_LG_MOONSLASHER;
			sd->state.K_LOCK_LG_MOONSLASHER = 0;
		}
		break;
	case LG_RAYOFGENESIS:
		if(DIFF_TICK(sd->K_CHK_LG_RAYOFGENESIS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_RAYOFGENESIS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_RAYOFGENESIS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_RAYOFGENESIS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_RAYOFGENESIS = gettick() + battle_config.WOE_K_DELAY_LG_RAYOFGENESIS;
				sd->state.K_LOCK_LG_RAYOFGENESIS = 0;
		} else {
			sd->K_CHK_LG_RAYOFGENESIS = gettick() + battle_config.K_DELAY_LG_RAYOFGENESIS;
			sd->state.K_LOCK_LG_RAYOFGENESIS = 0;
		}
		break;
	case LG_PIETY:
		if(DIFF_TICK(sd->K_CHK_LG_PIETY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_PIETY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_PIETY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_PIETY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_PIETY = gettick() + battle_config.WOE_K_DELAY_LG_PIETY;
				sd->state.K_LOCK_LG_PIETY = 0;
		} else {
			sd->K_CHK_LG_PIETY = gettick() + battle_config.K_DELAY_LG_PIETY;
			sd->state.K_LOCK_LG_PIETY = 0;
		}
		break;
	case LG_EARTHDRIVE:
		if(DIFF_TICK(sd->K_CHK_LG_EARTHDRIVE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_EARTHDRIVE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_EARTHDRIVE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_EARTHDRIVE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_EARTHDRIVE = gettick() + battle_config.WOE_K_DELAY_LG_EARTHDRIVE;
				sd->state.K_LOCK_LG_EARTHDRIVE = 0;
		} else {
			sd->K_CHK_LG_EARTHDRIVE = gettick() + battle_config.K_DELAY_LG_EARTHDRIVE;
			sd->state.K_LOCK_LG_EARTHDRIVE = 0;
		}
		break;
	case LG_HESPERUSLIT:
		if(DIFF_TICK(sd->K_CHK_LG_HESPERUSLIT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_HESPERUSLIT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_HESPERUSLIT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_HESPERUSLIT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_HESPERUSLIT = gettick() + battle_config.WOE_K_DELAY_LG_HESPERUSLIT;
				sd->state.K_LOCK_LG_HESPERUSLIT = 0;
		} else {
			sd->K_CHK_LG_HESPERUSLIT = gettick() + battle_config.K_DELAY_LG_HESPERUSLIT;
			sd->state.K_LOCK_LG_HESPERUSLIT = 0;
		}
		break;
	case SR_DRAGONCOMBO:
		if(DIFF_TICK(sd->K_CHK_SR_DRAGONCOMBO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_DRAGONCOMBO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_DRAGONCOMBO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_DRAGONCOMBO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_DRAGONCOMBO = gettick() + battle_config.WOE_K_DELAY_SR_DRAGONCOMBO;
				sd->state.K_LOCK_SR_DRAGONCOMBO = 0;
		} else {
			sd->K_CHK_SR_DRAGONCOMBO = gettick() + battle_config.K_DELAY_SR_DRAGONCOMBO;
			sd->state.K_LOCK_SR_DRAGONCOMBO = 0;
		}
		break;
	case SR_SKYNETBLOW:
		if(DIFF_TICK(sd->K_CHK_SR_SKYNETBLOW,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_SKYNETBLOW,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_SKYNETBLOW - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_SKYNETBLOW = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_SKYNETBLOW = gettick() + battle_config.WOE_K_DELAY_SR_SKYNETBLOW;
				sd->state.K_LOCK_SR_SKYNETBLOW = 0;
		} else {
			sd->K_CHK_SR_SKYNETBLOW = gettick() + battle_config.K_DELAY_SR_SKYNETBLOW;
			sd->state.K_LOCK_SR_SKYNETBLOW = 0;
		}
		break;
	case SR_EARTHSHAKER:
		if(DIFF_TICK(sd->K_CHK_SR_EARTHSHAKER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_EARTHSHAKER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_EARTHSHAKER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_EARTHSHAKER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_EARTHSHAKER = gettick() + battle_config.WOE_K_DELAY_SR_EARTHSHAKER;
				sd->state.K_LOCK_SR_EARTHSHAKER = 0;
		} else {
			sd->K_CHK_SR_EARTHSHAKER = gettick() + battle_config.K_DELAY_SR_EARTHSHAKER;
			sd->state.K_LOCK_SR_EARTHSHAKER = 0;
		}
		break;
	case SR_FALLENEMPIRE:
		if(DIFF_TICK(sd->K_CHK_SR_FALLENEMPIRE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_FALLENEMPIRE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_FALLENEMPIRE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_FALLENEMPIRE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_FALLENEMPIRE = gettick() + battle_config.WOE_K_DELAY_SR_FALLENEMPIRE;
				sd->state.K_LOCK_SR_FALLENEMPIRE = 0;
		} else {
			sd->K_CHK_SR_FALLENEMPIRE = gettick() + battle_config.K_DELAY_SR_FALLENEMPIRE;
			sd->state.K_LOCK_SR_FALLENEMPIRE = 0;
		}
		break;
	case SR_TIGERCANNON:
		if(DIFF_TICK(sd->K_CHK_SR_TIGERCANNON,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_TIGERCANNON,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_TIGERCANNON - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_TIGERCANNON = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_TIGERCANNON = gettick() + battle_config.WOE_K_DELAY_SR_TIGERCANNON;
				sd->state.K_LOCK_SR_TIGERCANNON = 0;
		} else {
			sd->K_CHK_SR_TIGERCANNON = gettick() + battle_config.K_DELAY_SR_TIGERCANNON;
			sd->state.K_LOCK_SR_TIGERCANNON = 0;
		}
		break;
	case SR_HELLGATE:
		if(DIFF_TICK(sd->K_CHK_SR_HELLGATE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_HELLGATE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_HELLGATE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_HELLGATE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_HELLGATE = gettick() + battle_config.WOE_K_DELAY_SR_HELLGATE;
				sd->state.K_LOCK_SR_HELLGATE = 0;
		} else {
			sd->K_CHK_SR_HELLGATE = gettick() + battle_config.K_DELAY_SR_HELLGATE;
			sd->state.K_LOCK_SR_HELLGATE = 0;
		}
		break;
	case SR_RAMPAGEBLASTER:
		if(DIFF_TICK(sd->K_CHK_SR_RAMPAGEBLASTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_RAMPAGEBLASTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_RAMPAGEBLASTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_RAMPAGEBLASTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_RAMPAGEBLASTER = gettick() + battle_config.WOE_K_DELAY_SR_RAMPAGEBLASTER;
				sd->state.K_LOCK_SR_RAMPAGEBLASTER = 0;
		} else {
			sd->K_CHK_SR_RAMPAGEBLASTER = gettick() + battle_config.K_DELAY_SR_RAMPAGEBLASTER;
			sd->state.K_LOCK_SR_RAMPAGEBLASTER = 0;
		}
		break;
	case SR_CRESCENTELBOW:
		if(DIFF_TICK(sd->K_CHK_SR_CRESCENTELBOW,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_CRESCENTELBOW,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_CRESCENTELBOW - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_CRESCENTELBOW = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_CRESCENTELBOW = gettick() + battle_config.WOE_K_DELAY_SR_CRESCENTELBOW;
				sd->state.K_LOCK_SR_CRESCENTELBOW = 0;
		} else {
			sd->K_CHK_SR_CRESCENTELBOW = gettick() + battle_config.K_DELAY_SR_CRESCENTELBOW;
			sd->state.K_LOCK_SR_CRESCENTELBOW = 0;
		}
		break;
	case SR_CURSEDCIRCLE:
		if(DIFF_TICK(sd->K_CHK_SR_CURSEDCIRCLE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_CURSEDCIRCLE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_CURSEDCIRCLE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_CURSEDCIRCLE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_CURSEDCIRCLE = gettick() + battle_config.WOE_K_DELAY_SR_CURSEDCIRCLE;
				sd->state.K_LOCK_SR_CURSEDCIRCLE = 0;
		} else {
			sd->K_CHK_SR_CURSEDCIRCLE = gettick() + battle_config.K_DELAY_SR_CURSEDCIRCLE;
			sd->state.K_LOCK_SR_CURSEDCIRCLE = 0;
		}
		break;
	case SR_LIGHTNINGWALK:
		if(DIFF_TICK(sd->K_CHK_SR_LIGHTNINGWALK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_LIGHTNINGWALK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_LIGHTNINGWALK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_LIGHTNINGWALK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_LIGHTNINGWALK = gettick() + battle_config.WOE_K_DELAY_SR_LIGHTNINGWALK;
				sd->state.K_LOCK_SR_LIGHTNINGWALK = 0;
		} else {
			sd->K_CHK_SR_LIGHTNINGWALK = gettick() + battle_config.K_DELAY_SR_LIGHTNINGWALK;
			sd->state.K_LOCK_SR_LIGHTNINGWALK = 0;
		}
		break;
	case SR_KNUCKLEARROW:
		if(DIFF_TICK(sd->K_CHK_SR_KNUCKLEARROW,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_KNUCKLEARROW,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_KNUCKLEARROW - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_KNUCKLEARROW = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_KNUCKLEARROW = gettick() + battle_config.WOE_K_DELAY_SR_KNUCKLEARROW;
				sd->state.K_LOCK_SR_KNUCKLEARROW = 0;
		} else {
			sd->K_CHK_SR_KNUCKLEARROW = gettick() + battle_config.K_DELAY_SR_KNUCKLEARROW;
			sd->state.K_LOCK_SR_KNUCKLEARROW = 0;
		}
		break;
	case SR_WINDMILL:
		if(DIFF_TICK(sd->K_CHK_SR_WINDMILL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_WINDMILL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_WINDMILL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_WINDMILL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_WINDMILL = gettick() + battle_config.WOE_K_DELAY_SR_WINDMILL;
				sd->state.K_LOCK_SR_WINDMILL = 0;
		} else {
			sd->K_CHK_SR_WINDMILL = gettick() + battle_config.K_DELAY_SR_WINDMILL;
			sd->state.K_LOCK_SR_WINDMILL = 0;
		}
		break;
	case SR_RAISINGDRAGON:
		if(DIFF_TICK(sd->K_CHK_SR_RAISINGDRAGON,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_RAISINGDRAGON,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_RAISINGDRAGON - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_RAISINGDRAGON = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_RAISINGDRAGON = gettick() + battle_config.WOE_K_DELAY_SR_RAISINGDRAGON;
				sd->state.K_LOCK_SR_RAISINGDRAGON = 0;
		} else {
			sd->K_CHK_SR_RAISINGDRAGON = gettick() + battle_config.K_DELAY_SR_RAISINGDRAGON;
			sd->state.K_LOCK_SR_RAISINGDRAGON = 0;
		}
		break;
	case SR_GENTLETOUCH:
		if(DIFF_TICK(sd->K_CHK_SR_GENTLETOUCH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_GENTLETOUCH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_GENTLETOUCH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_GENTLETOUCH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_GENTLETOUCH = gettick() + battle_config.WOE_K_DELAY_SR_GENTLETOUCH;
				sd->state.K_LOCK_SR_GENTLETOUCH = 0;
		} else {
			sd->K_CHK_SR_GENTLETOUCH = gettick() + battle_config.K_DELAY_SR_GENTLETOUCH;
			sd->state.K_LOCK_SR_GENTLETOUCH = 0;
		}
		break;
	case SR_ASSIMILATEPOWER:
		if(DIFF_TICK(sd->K_CHK_SR_ASSIMILATE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_ASSIMILATE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_ASSIMILATE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_ASSIMILATE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_ASSIMILATE = gettick() + battle_config.WOE_K_DELAY_SR_ASSIMILATEPOWER;
				sd->state.K_LOCK_SR_ASSIMILATE = 0;
		} else {
			sd->K_CHK_SR_ASSIMILATE = gettick() + battle_config.K_DELAY_SR_ASSIMILATEPOWER;
			sd->state.K_LOCK_SR_ASSIMILATE = 0;
		}
		break;
	case SR_POWERVELOCITY:
		if(DIFF_TICK(sd->K_CHK_SR_POWERVELOCITY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_POWERVELOCITY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_POWERVELOCITY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_POWERVELOCITY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_POWERVELOCITY = gettick() + battle_config.WOE_K_DELAY_SR_POWERVELOCITY;
				sd->state.K_LOCK_SR_POWERVELOCITY = 0;
		} else {
			sd->K_CHK_SR_POWERVELOCITY = gettick() + battle_config.K_DELAY_SR_POWERVELOCITY;
			sd->state.K_LOCK_SR_POWERVELOCITY = 0;
		}
		break;
	case SR_CRESCENTELBOW_AUTOSPELL:
		if(DIFF_TICK(sd->K_CHK_SR_CRESBOW_AUTO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_CRESBOW_AUTO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_CRESBOW_AUTO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_CRESBOW_AUTO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_CRESBOW_AUTO = gettick() + battle_config.WOE_K_DELAY_SR_CRESBOW_AUTO;
				sd->state.K_LOCK_SR_CRESBOW_AUTO = 0;
		} else {
			sd->K_CHK_SR_CRESBOW_AUTO = gettick() + battle_config.K_DELAY_SR_CRESBOW_AUTO;
			sd->state.K_LOCK_SR_CRESBOW_AUTO = 0;
		}
		break;
	case SR_GATEOFHELL:
		if(DIFF_TICK(sd->K_CHK_SR_GATEOFHELL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_GATEOFHELL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_GATEOFHELL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_GATEOFHELL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_GATEOFHELL = gettick() + battle_config.WOE_K_DELAY_SR_GATEOFHELL;
				sd->state.K_LOCK_SR_GATEOFHELL = 0;
		} else {
			sd->K_CHK_SR_GATEOFHELL = gettick() + battle_config.K_DELAY_SR_GATEOFHELL;
			sd->state.K_LOCK_SR_GATEOFHELL = 0;
		}
		break;
	case SR_GENTLETOUCH_QUIET:
		if(DIFF_TICK(sd->K_CHK_SR_GENTLE_QUIET,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_GENTLE_QUIET,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_GENTLE_QUIET - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_GENTLE_QUIET = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_GENTLE_QUIET = gettick() + battle_config.WOE_K_DELAY_SR_GENTLE_QUIET;
				sd->state.K_LOCK_SR_GENTLE_QUIET = 0;
		} else {
			sd->K_CHK_SR_GENTLE_QUIET = gettick() + battle_config.K_DELAY_SR_GENTLE_QUIET;
			sd->state.K_LOCK_SR_GENTLE_QUIET = 0;
		}
		break;
	case SR_GENTLETOUCH_CURE:
		if(DIFF_TICK(sd->K_CHK_SR_GENTLE_CURE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_GENTLE_CURE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_GENTLE_CURE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_GENTLE_CURE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_GENTLE_CURE = gettick() + battle_config.WOE_K_DELAY_SR_GENTLE_CURE;
				sd->state.K_LOCK_SR_GENTLE_CURE = 0;
		} else {
			sd->K_CHK_SR_GENTLE_CURE = gettick() + battle_config.K_DELAY_SR_GENTLE_CURE;
			sd->state.K_LOCK_SR_GENTLE_CURE = 0;
		}
		break;
	case SR_GENTLETOUCH_ENERGYGAIN:
		if(DIFF_TICK(sd->K_CHK_SR_GENTLE_EGAIN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_GENTLE_EGAIN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_GENTLE_EGAIN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_GENTLE_EGAIN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_GENTLE_EGAIN = gettick() + battle_config.WOE_K_DELAY_SR_GENTLE_EGAIN;
				sd->state.K_LOCK_SR_GENTLE_EGAIN = 0;
		} else {
			sd->K_CHK_SR_GENTLE_EGAIN = gettick() + battle_config.K_DELAY_SR_GENTLE_EGAIN;
			sd->state.K_LOCK_SR_GENTLE_EGAIN = 0;
		}
		break;
	case SR_GENTLETOUCH_CHANGE:
		if(DIFF_TICK(sd->K_CHK_SR_GENTLE_CHANGE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_GENTLE_CHANGE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_GENTLE_CHANGE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_GENTLE_CHANGE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_GENTLE_CHANGE = gettick() + battle_config.WOE_K_DELAY_SR_GENTLE_CHANGE;
				sd->state.K_LOCK_SR_GENTLE_CHANGE = 0;
		} else {
			sd->K_CHK_SR_GENTLE_CHANGE = gettick() + battle_config.K_DELAY_SR_GENTLE_CHANGE;
			sd->state.K_LOCK_SR_GENTLE_CHANGE = 0;
		}
		break;
	case SR_GENTLETOUCH_REVITALIZE:
		if(DIFF_TICK(sd->K_CHK_SR_GENTLE_REVIT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_GENTLE_REVIT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_GENTLE_REVIT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_GENTLE_REVIT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_GENTLE_REVIT = gettick() + battle_config.WOE_K_DELAY_SR_GENTLE_REVIT;
				sd->state.K_LOCK_SR_GENTLE_REVIT = 0;
		} else {
			sd->K_CHK_SR_GENTLE_REVIT = gettick() + battle_config.K_DELAY_SR_GENTLE_REVIT;
			sd->state.K_LOCK_SR_GENTLE_REVIT = 0;
		}
		break;
	case WA_SWING_DANCE:
		if(DIFF_TICK(sd->K_CHK_WA_SWING_DANCE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WA_SWING_DANCE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WA_SWING_DANCE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WA_SWING_DANCE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WA_SWING_DANCE = gettick() + battle_config.WOE_K_DELAY_WA_SWING_DANCE;
				sd->state.K_LOCK_WA_SWING_DANCE = 0;
		} else {
			sd->K_CHK_WA_SWING_DANCE = gettick() + battle_config.K_DELAY_WA_SWING_DANCE;
			sd->state.K_LOCK_WA_SWING_DANCE = 0;
		}
		break;
	case WA_SYMPHONY_OF_LOVER:
		if(DIFF_TICK(sd->K_CHK_WA_SYMPHONY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WA_SYMPHONY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WA_SYMPHONY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WA_SYMPHONY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WA_SYMPHONY = gettick() + battle_config.WOE_K_DELAY_WA_SYMPHONY;
				sd->state.K_LOCK_WA_SYMPHONY = 0;
		} else {
			sd->K_CHK_WA_SYMPHONY = gettick() + battle_config.K_DELAY_WA_SYMPHONY;
			sd->state.K_LOCK_WA_SYMPHONY = 0;
		}
		break;
	case WA_MOONLIT_SERENADE:
		if(DIFF_TICK(sd->K_CHK_WA_MOONLIT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WA_MOONLIT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WA_MOONLIT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WA_MOONLIT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WA_MOONLIT = gettick() + battle_config.WOE_K_DELAY_WA_MOONLIT;
				sd->state.K_LOCK_WA_MOONLIT = 0;
		} else {
			sd->K_CHK_WA_MOONLIT = gettick() + battle_config.K_DELAY_WA_MOONLIT;
			sd->state.K_LOCK_WA_MOONLIT = 0;
		}
		break;
	case MI_RUSH_WINDMILL:
		if(DIFF_TICK(sd->K_CHK_MI_RUSH_WINDMILL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MI_RUSH_WINDMILL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MI_RUSH_WINDMILL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MI_RUSH_WINDMILL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MI_RUSH_WINDMILL = gettick() + battle_config.WOE_K_DELAY_MI_WINDMILL;
				sd->state.K_LOCK_MI_RUSH_WINDMILL = 0;
		} else {
			sd->K_CHK_MI_RUSH_WINDMILL = gettick() + battle_config.K_DELAY_MI_RUSH_WINDMILL;
			sd->state.K_LOCK_MI_RUSH_WINDMILL = 0;
		}
		break;
	case MI_ECHOSONG:
		if(DIFF_TICK(sd->K_CHK_MI_ECHOSONG,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MI_ECHOSONG,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MI_ECHOSONG - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MI_ECHOSONG = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MI_ECHOSONG = gettick() + battle_config.WOE_K_DELAY_MI_ECHOSONG;
				sd->state.K_LOCK_MI_ECHOSONG = 0;
		} else {
			sd->K_CHK_MI_ECHOSONG = gettick() + battle_config.K_DELAY_MI_ECHOSONG;
			sd->state.K_LOCK_MI_ECHOSONG = 0;
		}
		break;
	case MI_HARMONIZE:
		if(DIFF_TICK(sd->K_CHK_MI_HARMONIZE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_MI_HARMONIZE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_MI_HARMONIZE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_MI_HARMONIZE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_MI_HARMONIZE = gettick() + battle_config.WOE_K_DELAY_MI_HARMONIZE;
				sd->state.K_LOCK_MI_HARMONIZE = 0;
		} else {
			sd->K_CHK_MI_HARMONIZE = gettick() + battle_config.K_DELAY_MI_HARMONIZE;
			sd->state.K_LOCK_MI_HARMONIZE = 0;
		}
		break;
	case WM_LESSON:
		if(DIFF_TICK(sd->K_CHK_WM_LESSON,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_LESSON,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_LESSON - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_LESSON = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_LESSON = gettick() + battle_config.WOE_K_DELAY_WM_LESSON;
				sd->state.K_LOCK_WM_LESSON = 0;
		} else {
			sd->K_CHK_WM_LESSON = gettick() + battle_config.K_DELAY_WM_LESSON;
			sd->state.K_LOCK_WM_LESSON = 0;
		}
		break;
	case WM_METALICSOUND:
		if(DIFF_TICK(sd->K_CHK_WM_METALICSOUND,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_METALICSOUND,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_METALICSOUND - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_METALICSOUND = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_METALICSOUND = gettick() + battle_config.WOE_K_DELAY_WM_METALICSOUND;
				sd->state.K_LOCK_WM_METALICSOUND = 0;
		} else {
			sd->K_CHK_WM_METALICSOUND = gettick() + battle_config.K_DELAY_WM_METALICSOUND;
			sd->state.K_LOCK_WM_METALICSOUND = 0;
		}
		break;
	case WM_REVERBERATION:
		if(DIFF_TICK(sd->K_CHK_WM_REVERB,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_REVERB,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_REVERB - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_REVERB = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_REVERB = gettick() + battle_config.WOE_K_DELAY_WM_REVERB;
				sd->state.K_LOCK_WM_REVERB = 0;
		} else {
			sd->K_CHK_WM_REVERB = gettick() + battle_config.K_DELAY_WM_REVERBERATION;
			sd->state.K_LOCK_WM_REVERB = 0;
		}
		break;
	case WM_REVERBERATION_MELEE:
		if(DIFF_TICK(sd->K_CHK_WM_REVERB_MELEE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_REVERB_MELEE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_REVERB_MELEE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_REVERB_MELEE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_REVERB_MELEE = gettick() + battle_config.WOE_K_DELAY_WM_REVERB_MELEE;
				sd->state.K_LOCK_WM_REVERB_MELEE = 0;
		} else {
			sd->K_CHK_WM_REVERB_MELEE = gettick() + battle_config.K_DELAY_WM_REVERB_MELEE;
			sd->state.K_LOCK_WM_REVERB_MELEE = 0;
		}
		break;
	case WM_REVERBERATION_MAGIC:
		if(DIFF_TICK(sd->K_CHK_WM_REVERB_MAGIC,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_REVERB_MAGIC,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_REVERB_MAGIC - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_REVERB_MAGIC = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_REVERB_MAGIC = gettick() + battle_config.WOE_K_DELAY_WM_REVERB_MAGIC;
				sd->state.K_LOCK_WM_REVERB_MAGIC = 0;
		} else {
			sd->K_CHK_WM_REVERB_MAGIC = gettick() + battle_config.K_DELAY_WM_REVERB_MAGIC;
			sd->state.K_LOCK_WM_REVERB_MAGIC = 0;
		}
		break;
	case WM_DOMINION_IMPULSE:
		if(DIFF_TICK(sd->K_CHK_WM_DOMINION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_DOMINION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_DOMINION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_DOMINION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_DOMINION = gettick() + battle_config.WOE_K_DELAY_WM_DOMINION;
				sd->state.K_LOCK_WM_DOMINION = 0;
		} else {
			sd->K_CHK_WM_DOMINION = gettick() + battle_config.K_DELAY_WM_DOMINION;
			sd->state.K_LOCK_WM_DOMINION = 0;
		}
		break;
	case WM_SEVERE_RAINSTORM:
		if(DIFF_TICK(sd->K_CHK_WM_RAINSTORM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_RAINSTORM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_RAINSTORM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_RAINSTORM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_RAINSTORM = gettick() + battle_config.WOE_K_DELAY_WM_RAINSTORM;
				sd->state.K_LOCK_WM_RAINSTORM = 0;
		} else {
			sd->K_CHK_WM_RAINSTORM = gettick() + battle_config.K_DELAY_WM_RAINSTORM;
			sd->state.K_LOCK_WM_RAINSTORM = 0;
		}
		break;
	case WM_SEVERE_RAINSTORM_MELEE:
		if(DIFF_TICK(sd->K_CHK_WM_RAINSTORM_,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_RAINSTORM_,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_RAINSTORM_ - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_RAINSTORM_ = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_RAINSTORM_ = gettick() + battle_config.WOE_K_DELAY_WM_RAINSTORM_;
				sd->state.K_LOCK_WM_RAINSTORM_ = 0;
		} else {
			sd->K_CHK_WM_RAINSTORM_ = gettick() + battle_config.K_DELAY_WM_RAINSTORM_;
			sd->state.K_LOCK_WM_RAINSTORM_ = 0;
		}
		break;
	case WM_POEMOFNETHERWORLD:
		if(DIFF_TICK(sd->K_CHK_WM_POEM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_POEM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_POEM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_POEM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_POEM = gettick() + battle_config.WOE_K_DELAY_WM_POEM;
				sd->state.K_LOCK_WM_POEM = 0;
		} else {
			sd->K_CHK_WM_POEM = gettick() + battle_config.K_DELAY_WM_POEM;
			sd->state.K_LOCK_WM_POEM = 0;
		}
		break;
	case WM_VOICEOFSIREN:
		if(DIFF_TICK(sd->K_CHK_WM_VOICEOFSIREN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_VOICEOFSIREN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_VOICEOFSIREN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_VOICEOFSIREN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_VOICEOFSIREN = gettick() + battle_config.WOE_K_DELAY_WM_VOICE;
				sd->state.K_LOCK_WM_VOICEOFSIREN = 0;
		} else {
			sd->K_CHK_WM_VOICEOFSIREN = gettick() + battle_config.K_DELAY_WM_VOICEOFSIREN;
			sd->state.K_LOCK_WM_VOICEOFSIREN = 0;
		}
		break;
	case WM_DEADHILLHERE:
		if(DIFF_TICK(sd->K_CHK_WM_DEADHILLHERE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_DEADHILLHERE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_DEADHILLHERE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_DEADHILLHERE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_DEADHILLHERE = gettick() + battle_config.WOE_K_DELAY_WM_HILLHERE;
				sd->state.K_LOCK_WM_DEADHILLHERE = 0;
		} else {
			sd->K_CHK_WM_DEADHILLHERE = gettick() + battle_config.K_DELAY_WM_DEADHILLHERE;
			sd->state.K_LOCK_WM_DEADHILLHERE = 0;
		}
		break;
	case WM_LULLABY_DEEPSLEEP:
		if(DIFF_TICK(sd->K_CHK_WM_DEEPSLEEP,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_DEEPSLEEP,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_DEEPSLEEP - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_DEEPSLEEP = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_DEEPSLEEP = gettick() + battle_config.WOE_K_DELAY_WM_DEEPSLEEP;
				sd->state.K_LOCK_WM_DEEPSLEEP = 0;
		} else {
			sd->K_CHK_WM_DEEPSLEEP = gettick() + battle_config.K_DELAY_WM_LULLABY_DEEPSLEEP;
			sd->state.K_LOCK_WM_DEEPSLEEP = 0;
		}
		break;
	case WM_SIRCLEOFNATURE:
		if(DIFF_TICK(sd->K_CHK_WM_SIRCLEOFNATURE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_SIRCLEOFNATURE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_SIRCLEOFNATURE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_SIRCLEOFNATURE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_SIRCLEOFNATURE = gettick() + battle_config.WOE_K_DELAY_WM_SIRCLE;
				sd->state.K_LOCK_WM_SIRCLEOFNATURE = 0;
		} else {
			sd->K_CHK_WM_SIRCLEOFNATURE = gettick() + battle_config.K_DELAY_WM_SIRCLEOFNATURE;
			sd->state.K_LOCK_WM_SIRCLEOFNATURE = 0;
		}
		break;
	case WM_RANDOMIZESPELL:
		if(DIFF_TICK(sd->K_CHK_WM_RANDOMIZESPELL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_RANDOMIZESPELL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_RANDOMIZESPELL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_RANDOMIZESPELL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_RANDOMIZESPELL = gettick() + battle_config.WOE_K_DELAY_WM_RANDOMIZE;
				sd->state.K_LOCK_WM_RANDOMIZESPELL = 0;
		} else {
			sd->K_CHK_WM_RANDOMIZESPELL = gettick() + battle_config.K_DELAY_WM_RANDOMIZESPELL;
			sd->state.K_LOCK_WM_RANDOMIZESPELL = 0;
		}
		break;
	case WM_GLOOMYDAY:
		if(DIFF_TICK(sd->K_CHK_WM_GLOOMYDAY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_GLOOMYDAY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_GLOOMYDAY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_GLOOMYDAY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_GLOOMYDAY = gettick() + battle_config.WOE_K_DELAY_WM_GLOOMYDAY;
				sd->state.K_LOCK_WM_GLOOMYDAY = 0;
		} else {
			sd->K_CHK_WM_GLOOMYDAY = gettick() + battle_config.K_DELAY_WM_GLOOMYDAY;
			sd->state.K_LOCK_WM_GLOOMYDAY = 0;
		}
		break;
	case WM_GREAT_ECHO:
		if(DIFF_TICK(sd->K_CHK_WM_GREAT_ECHO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_GREAT_ECHO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_GREAT_ECHO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_GREAT_ECHO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_GREAT_ECHO = gettick() + battle_config.WOE_K_DELAY_WM_GREAT_ECHO;
				sd->state.K_LOCK_WM_GREAT_ECHO = 0;
		} else {
			sd->K_CHK_WM_GREAT_ECHO = gettick() + battle_config.K_DELAY_WM_GREAT_ECHO;
			sd->state.K_LOCK_WM_GREAT_ECHO = 0;
		}
		break;
	case WM_SONG_OF_MANA:
		if(DIFF_TICK(sd->K_CHK_WM_SONG_OF_MANA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_SONG_OF_MANA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_SONG_OF_MANA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_SONG_OF_MANA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_SONG_OF_MANA = gettick() + battle_config.WOE_K_DELAY_WM_SONG_OF_MANA;
				sd->state.K_LOCK_WM_SONG_OF_MANA = 0;
		} else {
			sd->K_CHK_WM_SONG_OF_MANA = gettick() + battle_config.K_DELAY_WM_SONG_OF_MANA;
			sd->state.K_LOCK_WM_SONG_OF_MANA = 0;
		}
		break;
	case WM_DANCE_WITH_WUG:
		if(DIFF_TICK(sd->K_CHK_WM_DANCE_WITH_WUG,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_DANCE_WITH_WUG,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_DANCE_WITH_WUG - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_DANCE_WITH_WUG = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_DANCE_WITH_WUG = gettick() + battle_config.WOE_K_DELAY_WM_DANCEWUG;
				sd->state.K_LOCK_WM_DANCE_WITH_WUG = 0;
		} else {
			sd->K_CHK_WM_DANCE_WITH_WUG = gettick() + battle_config.K_DELAY_WM_DANCE_WITH_WUG;
			sd->state.K_LOCK_WM_DANCE_WITH_WUG = 0;
		}
		break;
	case WM_SOUND_OF_DESTRUCTION:
		if(DIFF_TICK(sd->K_CHK_WM_SOUND_OF_DESTRUCTION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_SOUND_OF_DESTRUCTION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_SOUND_OF_DESTRUCTION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_DESTRUCTION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_SOUND_OF_DESTRUCTION = gettick() + battle_config.WOE_K_DELAY_WM_DESTRUCTION;
				sd->state.K_LOCK_WM_DESTRUCTION = 0;
		} else {
			sd->K_CHK_WM_SOUND_OF_DESTRUCTION = gettick() + battle_config.K_DELAY_WM_DESTRUCTION;
			sd->state.K_LOCK_WM_DESTRUCTION = 0;
		}
		break;
	case WM_SATURDAY_NIGHT_FEVER:
		if(DIFF_TICK(sd->K_CHK_WM_SATNIGHT_FEVER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_SATNIGHT_FEVER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_SATNIGHT_FEVER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_SATNIGHT_FEVER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_SATNIGHT_FEVER = gettick() + battle_config.WOE_K_DELAY_WM_SATFEVER;
				sd->state.K_LOCK_WM_SATNIGHT_FEVER = 0;
		} else {
			sd->K_CHK_WM_SATNIGHT_FEVER = gettick() + battle_config.K_DELAY_WM_SATNIGHT_FEVER;
			sd->state.K_LOCK_WM_SATNIGHT_FEVER = 0;
		}
		break;
	case WM_LERADS_DEW:
		if(DIFF_TICK(sd->K_CHK_WM_LERADS_DEW,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_LERADS_DEW,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_LERADS_DEW - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_LERADS_DEW = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_LERADS_DEW = gettick() + battle_config.WOE_K_DELAY_WM_LERADS_DEW;
				sd->state.K_LOCK_WM_LERADS_DEW = 0;
		} else {
			sd->K_CHK_WM_LERADS_DEW = gettick() + battle_config.K_DELAY_WM_LERADS_DEW;
			sd->state.K_LOCK_WM_LERADS_DEW = 0;
		}
		break;
	case WM_MELODYOFSINK:
		if(DIFF_TICK(sd->K_CHK_WM_MELODYOFSINK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_MELODYOFSINK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_MELODYOFSINK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_MELODYOFSINK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_MELODYOFSINK = gettick() + battle_config.WOE_K_DELAY_WM_MELODYOFSINK;
				sd->state.K_LOCK_WM_MELODYOFSINK = 0;
		} else {
			sd->K_CHK_WM_MELODYOFSINK = gettick() + battle_config.K_DELAY_WM_MELODYOFSINK;
			sd->state.K_LOCK_WM_MELODYOFSINK = 0;
		}
		break;
	case WM_BEYOND_OF_WARCRY:
		if(DIFF_TICK(sd->K_CHK_WM_WARCRY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_WARCRY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_WARCRY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_WARCRY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_WARCRY = gettick() + battle_config.WOE_K_DELAY_WM_WARCRY;
				sd->state.K_LOCK_WM_WARCRY = 0;
		} else {
			sd->K_CHK_WM_WARCRY = gettick() + battle_config.K_DELAY_WM_BEYOND_OF_WARCRY;
			sd->state.K_LOCK_WM_WARCRY = 0;
		}
		break;
	case WM_UNLIMITED_HUMMING_VOICE:
		if(DIFF_TICK(sd->K_CHK_WM_HUMMING_VOICE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_WM_HUMMING_VOICE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_WM_HUMMING_VOICE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_WM_HUMMING_VOICE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_WM_HUMMING_VOICE = gettick() + battle_config.WOE_K_DELAY_WM_HUMMING;
				sd->state.K_LOCK_WM_HUMMING_VOICE = 0;
		} else {
			sd->K_CHK_WM_HUMMING_VOICE = gettick() + battle_config.K_DELAY_WM_HUMMING_VOICE;
			sd->state.K_LOCK_WM_HUMMING_VOICE = 0;
		}
		break;
	case SO_FIREWALK:
		if(DIFF_TICK(sd->K_CHK_SO_FIREWALK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_FIREWALK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_FIREWALK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_FIREWALK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_FIREWALK = gettick() + battle_config.WOE_K_DELAY_SO_FIREWALK;
				sd->state.K_LOCK_SO_FIREWALK = 0;
		} else {
			sd->K_CHK_SO_FIREWALK = gettick() + battle_config.K_DELAY_SO_FIREWALK;
			sd->state.K_LOCK_SO_FIREWALK = 0;
		}
		break;
	case SO_ELECTRICWALK:
		if(DIFF_TICK(sd->K_CHK_SO_ELECTRICWALK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_ELECTRICWALK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_ELECTRICWALK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_ELECTRICWALK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_ELECTRICWALK = gettick() + battle_config.WOE_K_DELAY_SO_ELECTRICWALK;
				sd->state.K_LOCK_SO_ELECTRICWALK = 0;
		} else {
			sd->K_CHK_SO_ELECTRICWALK = gettick() + battle_config.K_DELAY_SO_ELECTRICWALK;
			sd->state.K_LOCK_SO_ELECTRICWALK = 0;
		}
		break;
	case SO_SPELLFIST:
		if(DIFF_TICK(sd->K_CHK_SO_SPELLFIST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_SPELLFIST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_SPELLFIST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_SPELLFIST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_SPELLFIST = gettick() + battle_config.WOE_K_DELAY_SO_SPELLFIST;
				sd->state.K_LOCK_SO_SPELLFIST = 0;
		} else {
			sd->K_CHK_SO_SPELLFIST = gettick() + battle_config.K_DELAY_SO_SPELLFIST;
			sd->state.K_LOCK_SO_SPELLFIST = 0;
		}
		break;
	case SO_EARTHGRAVE:
		if(DIFF_TICK(sd->K_CHK_SO_EARTHGRAVE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_EARTHGRAVE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_EARTHGRAVE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_EARTHGRAVE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_EARTHGRAVE = gettick() + battle_config.WOE_K_DELAY_SO_EARTHGRAVE;
				sd->state.K_LOCK_SO_EARTHGRAVE = 0;
		} else {
			sd->K_CHK_SO_EARTHGRAVE = gettick() + battle_config.K_DELAY_SO_EARTHGRAVE;
			sd->state.K_LOCK_SO_EARTHGRAVE = 0;
		}
		break;
	case SO_DIAMONDDUST:
		if(DIFF_TICK(sd->K_CHK_SO_DIAMONDDUST,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_DIAMONDDUST,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_DIAMONDDUST - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_DIAMONDDUST = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_DIAMONDDUST = gettick() + battle_config.WOE_K_DELAY_SO_DIAMONDDUST;
				sd->state.K_LOCK_SO_DIAMONDDUST = 0;
		} else {
			sd->K_CHK_SO_DIAMONDDUST = gettick() + battle_config.K_DELAY_SO_DIAMONDDUST;
			sd->state.K_LOCK_SO_DIAMONDDUST = 0;
		}
		break;
	case SO_POISON_BUSTER:
		if(DIFF_TICK(sd->K_CHK_SO_POISON_BUSTER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_POISON_BUSTER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_POISON_BUSTER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_POISON_BUSTER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_POISON_BUSTER = gettick() + battle_config.WOE_K_DELAY_SO_POISON_BUSTER;
				sd->state.K_LOCK_SO_POISON_BUSTER = 0;
		} else {
			sd->K_CHK_SO_POISON_BUSTER = gettick() + battle_config.K_DELAY_SO_POISON_BUSTER;
			sd->state.K_LOCK_SO_POISON_BUSTER = 0;
		}
		break;
	case SO_PSYCHIC_WAVE:
		if(DIFF_TICK(sd->K_CHK_SO_PSYCHIC_WAVE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_PSYCHIC_WAVE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_PSYCHIC_WAVE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_PSYCHIC_WAVE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_PSYCHIC_WAVE = gettick() + battle_config.WOE_K_DELAY_SO_PSYCHIC_WAVE;
				sd->state.K_LOCK_SO_PSYCHIC_WAVE = 0;
		} else {
			sd->K_CHK_SO_PSYCHIC_WAVE = gettick() + battle_config.K_DELAY_SO_PSYCHIC_WAVE;
			sd->state.K_LOCK_SO_PSYCHIC_WAVE = 0;
		}
		break;
	case SO_CLOUD_KILL:
		if(DIFF_TICK(sd->K_CHK_SO_CLOUD_KILL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_CLOUD_KILL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_CLOUD_KILL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_CLOUD_KILL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_CLOUD_KILL = gettick() + battle_config.WOE_K_DELAY_SO_CLOUD_KILL;
				sd->state.K_LOCK_SO_CLOUD_KILL = 0;
		} else {
			sd->K_CHK_SO_CLOUD_KILL = gettick() + battle_config.K_DELAY_SO_CLOUD_KILL;
			sd->state.K_LOCK_SO_CLOUD_KILL = 0;
		}
		break;
	case SO_STRIKING:
		if(DIFF_TICK(sd->K_CHK_SO_STRIKING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_STRIKING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_STRIKING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_STRIKING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_STRIKING = gettick() + battle_config.WOE_K_DELAY_SO_STRIKING;
				sd->state.K_LOCK_SO_STRIKING = 0;
		} else {
			sd->K_CHK_SO_STRIKING = gettick() + battle_config.K_DELAY_SO_STRIKING;
			sd->state.K_LOCK_SO_STRIKING = 0;
		}
		break;
	case SO_WARMER:
		if(DIFF_TICK(sd->K_CHK_SO_WARMER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_WARMER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_WARMER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_WARMER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_WARMER = gettick() + battle_config.WOE_K_DELAY_SO_WARMER;
				sd->state.K_LOCK_SO_WARMER = 0;
		} else {
			sd->K_CHK_SO_WARMER = gettick() + battle_config.K_DELAY_SO_WARMER;
			sd->state.K_LOCK_SO_WARMER = 0;
		}
		break;
	case SO_VACUUM_EXTREME:
		if(DIFF_TICK(sd->K_CHK_SO_VACUUM_EXTREME,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_VACUUM_EXTREME,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_VACUUM_EXTREME - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_VACUUM_EXTREME = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_VACUUM_EXTREME = gettick() + battle_config.WOE_K_DELAY_SO_VACUUM_EXTREME;
				sd->state.K_LOCK_SO_VACUUM_EXTREME = 0;
		} else {
			sd->K_CHK_SO_VACUUM_EXTREME = gettick() + battle_config.K_DELAY_SO_VACUUM_EXTREME;
			sd->state.K_LOCK_SO_VACUUM_EXTREME = 0;
		}
		break;
	case SO_VARETYR_SPEAR:
		if(DIFF_TICK(sd->K_CHK_SO_VARETYR_SPEAR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_VARETYR_SPEAR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_VARETYR_SPEAR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_VARETYR_SPEAR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_VARETYR_SPEAR = gettick() + battle_config.WOE_K_DELAY_SO_VARETYR_SPEAR;
				sd->state.K_LOCK_SO_VARETYR_SPEAR = 0;
		} else {
			sd->K_CHK_SO_VARETYR_SPEAR = gettick() + battle_config.K_DELAY_SO_VARETYR_SPEAR;
			sd->state.K_LOCK_SO_VARETYR_SPEAR = 0;
		}
		break;
	case SO_ARRULLO:
		if(DIFF_TICK(sd->K_CHK_SO_ARRULLO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_ARRULLO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_ARRULLO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_ARRULLO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_ARRULLO = gettick() + battle_config.WOE_K_DELAY_SO_ARRULLO;
				sd->state.K_LOCK_SO_ARRULLO = 0;
		} else {
			sd->K_CHK_SO_ARRULLO = gettick() + battle_config.K_DELAY_SO_ARRULLO;
			sd->state.K_LOCK_SO_ARRULLO = 0;
		}
		break;
	case SO_EL_CONTROL:
		if(DIFF_TICK(sd->K_CHK_SO_EL_CONTROL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_EL_CONTROL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_EL_CONTROL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_EL_CONTROL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_EL_CONTROL = gettick() + battle_config.WOE_K_DELAY_SO_EL_CONTROL;
				sd->state.K_LOCK_SO_EL_CONTROL = 0;
		} else {
			sd->K_CHK_SO_EL_CONTROL = gettick() + battle_config.K_DELAY_SO_EL_CONTROL;
			sd->state.K_LOCK_SO_EL_CONTROL = 0;
		}
		break;
	case SO_EL_ACTION:
		if(DIFF_TICK(sd->K_CHK_SO_EL_ACTION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_EL_ACTION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_EL_ACTION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_EL_ACTION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_EL_ACTION = gettick() + battle_config.WOE_K_DELAY_SO_EL_ACTION;
				sd->state.K_LOCK_SO_EL_ACTION = 0;
		} else {
			sd->K_CHK_SO_EL_ACTION = gettick() + battle_config.K_DELAY_SO_EL_ACTION;
			sd->state.K_LOCK_SO_EL_ACTION = 0;
		}
		break;
	case SO_EL_ANALYSIS:
		if(DIFF_TICK(sd->K_CHK_SO_EL_ANALYSIS,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_EL_ANALYSIS,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_EL_ANALYSIS - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_EL_ANALYSIS = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_EL_ANALYSIS = gettick() + battle_config.WOE_K_DELAY_SO_EL_ANALYSIS;
				sd->state.K_LOCK_SO_EL_ANALYSIS = 0;
		} else {
			sd->K_CHK_SO_EL_ANALYSIS = gettick() + battle_config.K_DELAY_SO_EL_ANALYSIS;
			sd->state.K_LOCK_SO_EL_ANALYSIS = 0;
		}
		break;
	case SO_EL_SYMPATHY:
		if(DIFF_TICK(sd->K_CHK_SO_EL_SYMPATHY,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_EL_SYMPATHY,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_EL_SYMPATHY - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_EL_SYMPATHY = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_EL_SYMPATHY = gettick() + battle_config.WOE_K_DELAY_SO_EL_SYMPATHY;
				sd->state.K_LOCK_SO_EL_SYMPATHY = 0;
		} else {
			sd->K_CHK_SO_EL_SYMPATHY = gettick() + battle_config.K_DELAY_SO_EL_SYMPATHY;
			sd->state.K_LOCK_SO_EL_SYMPATHY = 0;
		}
		break;
	case SO_EL_CURE:
		if(DIFF_TICK(sd->K_CHK_SO_EL_CURE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SO_EL_CURE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SO_EL_CURE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SO_EL_CURE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SO_EL_CURE = gettick() + battle_config.WOE_K_DELAY_SO_EL_CURE;
				sd->state.K_LOCK_SO_EL_CURE = 0;
		} else {
			sd->K_CHK_SO_EL_CURE = gettick() + battle_config.K_DELAY_SO_EL_CURE;
			sd->state.K_LOCK_SO_EL_CURE = 0;
		}
		break;
	case GN_CART_TORNADO:
		if(DIFF_TICK(sd->K_CHK_GN_CART_TORNADO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_CART_TORNADO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_CART_TORNADO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_CART_TORNADO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_CART_TORNADO = gettick() + battle_config.WOE_K_DELAY_GN_CART_TORNADO;
				sd->state.K_LOCK_GN_CART_TORNADO = 0;
		} else {
			sd->K_CHK_GN_CART_TORNADO = gettick() + battle_config.K_DELAY_GN_CART_TORNADO;
			sd->state.K_LOCK_GN_CART_TORNADO = 0;
		}
		break;
	case GN_CARTCANNON:
		if(DIFF_TICK(sd->K_CHK_GN_CARTCANNON,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_CARTCANNON,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_CARTCANNON - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_CARTCANNON = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_CARTCANNON = gettick() + battle_config.WOE_K_DELAY_GN_CARTCANNON;
				sd->state.K_LOCK_GN_CARTCANNON = 0;
		} else {
			sd->K_CHK_GN_CARTCANNON = gettick() + battle_config.K_DELAY_GN_CARTCANNON;
			sd->state.K_LOCK_GN_CARTCANNON = 0;
		}
		break;
	case GN_THORNS_TRAP:
		if(DIFF_TICK(sd->K_CHK_GN_THORNS_TRAP,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_THORNS_TRAP,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_THORNS_TRAP - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_THORNS_TRAP = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_THORNS_TRAP = gettick() + battle_config.WOE_K_DELAY_GN_THORNS_TRAP;
				sd->state.K_LOCK_GN_THORNS_TRAP = 0;
		} else {
			sd->K_CHK_GN_THORNS_TRAP = gettick() + battle_config.K_DELAY_GN_THORNS_TRAP;
			sd->state.K_LOCK_GN_THORNS_TRAP = 0;
		}
		break;
	case GN_BLOOD_SUCKER:
		if(DIFF_TICK(sd->K_CHK_GN_BLOOD_SUCKER,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_BLOOD_SUCKER,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_BLOOD_SUCKER - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_BLOOD_SUCKER = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_BLOOD_SUCKER = gettick() + battle_config.WOE_K_DELAY_GN_BLOOD_SUCKER;
				sd->state.K_LOCK_GN_BLOOD_SUCKER = 0;
		} else {
			sd->K_CHK_GN_BLOOD_SUCKER = gettick() + battle_config.K_DELAY_GN_BLOOD_SUCKER;
			sd->state.K_LOCK_GN_BLOOD_SUCKER = 0;
		}
		break;
	case GN_SPORE_EXPLOSION:
		if(DIFF_TICK(sd->K_CHK_GN_SPORE_EXPLO,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_SPORE_EXPLO,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_SPORE_EXPLO - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_SPORE_EXPLO = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_SPORE_EXPLO = gettick() + battle_config.WOE_K_DELAY_GN_SPORE_EXPLO;
				sd->state.K_LOCK_GN_SPORE_EXPLO = 0;
		} else {
			sd->K_CHK_GN_SPORE_EXPLO = gettick() + battle_config.K_DELAY_GN_SPORE_EXPLOSION;
			sd->state.K_LOCK_GN_SPORE_EXPLO = 0;
		}
		break;
	case GN_WALLOFTHORN:
		if(DIFF_TICK(sd->K_CHK_GN_WALLOFTHORN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_WALLOFTHORN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_WALLOFTHORN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_WALLOFTHORN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_WALLOFTHORN = gettick() + battle_config.WOE_K_DELAY_GN_WALLOFTHORN;
				sd->state.K_LOCK_GN_WALLOFTHORN = 0;
		} else {
			sd->K_CHK_GN_WALLOFTHORN = gettick() + battle_config.K_DELAY_GN_WALLOFTHORN;
			sd->state.K_LOCK_GN_WALLOFTHORN = 0;
		}
		break;
	case GN_CRAZYWEED:
		if(DIFF_TICK(sd->K_CHK_GN_CRAZYWEED,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_CRAZYWEED,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_CRAZYWEED - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_CRAZYWEED = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_CRAZYWEED = gettick() + battle_config.WOE_K_DELAY_GN_CRAZYWEED;
				sd->state.K_LOCK_GN_CRAZYWEED = 0;
		} else {
			sd->K_CHK_GN_CRAZYWEED = gettick() + battle_config.K_DELAY_GN_CRAZYWEED;
			sd->state.K_LOCK_GN_CRAZYWEED = 0;
		}
		break;
	case GN_CRAZYWEED_ATK:
		if(DIFF_TICK(sd->K_CHK_GN_CRAZYWEED_,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_CRAZYWEED_,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_CRAZYWEED_ - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_CRAZYWEED_ = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_CRAZYWEED_ = gettick() + battle_config.WOE_K_DELAY_GN_CRAZYWEED_;
				sd->state.K_LOCK_GN_CRAZYWEED_ = 0;
		} else {
			sd->K_CHK_GN_CRAZYWEED_ = gettick() + battle_config.K_DELAY_GN_CRAZYWEED_;
			sd->state.K_LOCK_GN_CRAZYWEED_ = 0;
		}
		break;
	case GN_DEMONIC_FIRE:
		if(DIFF_TICK(sd->K_CHK_GN_DEMONIC_FIRE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_DEMONIC_FIRE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_DEMONIC_FIRE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_DEMONIC_FIRE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_DEMONIC_FIRE = gettick() + battle_config.WOE_K_DELAY_GN_DEMONIC_FIRE;
				sd->state.K_LOCK_GN_DEMONIC_FIRE = 0;
		} else {
			sd->K_CHK_GN_DEMONIC_FIRE = gettick() + battle_config.K_DELAY_GN_DEMONIC_FIRE;
			sd->state.K_LOCK_GN_DEMONIC_FIRE = 0;
		}
		break;
	case GN_FIRE_EXPANSION:
		if(DIFF_TICK(sd->K_CHK_GN_FIRE_EXPAN,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_FIRE_EXPAN,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_FIRE_EXPAN - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_FIRE_EXPAN = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_FIRE_EXPAN = gettick() + battle_config.WOE_K_DELAY_GN_FIRE_EXP;
				sd->state.K_LOCK_GN_FIRE_EXPAN = 0;
		} else {
			sd->K_CHK_GN_FIRE_EXPAN = gettick() + battle_config.K_DELAY_GN_FIRE_EXPAN;
			sd->state.K_LOCK_GN_FIRE_EXPAN = 0;
		}
		break;
	case GN_FIRE_EXPANSION_SMOKE_POWDER:
		if(DIFF_TICK(sd->K_CHK_GN_FIRE_EXPAN_SMOKE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_FIRE_EXPAN_SMOKE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_FIRE_EXPAN_SMOKE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_FIRE_EXSMOKE = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_FIRE_EXPAN_SMOKE = gettick() + battle_config.WOE_K_DELAY_GN_FIRE_EXPSMOKE;
				sd->state.K_LOCK_GN_FIRE_EXSMOKE = 0;
		} else {
			sd->K_CHK_GN_FIRE_EXPAN_SMOKE = gettick() + battle_config.K_DELAY_GN_FIRE_EXPAN_SMOKE;
			sd->state.K_LOCK_GN_FIRE_EXSMOKE = 0;
		}
		break;
	case GN_FIRE_EXPANSION_TEAR_GAS:
		if(DIFF_TICK(sd->K_CHK_GN_FIRE_EXPAN_TEAR,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_FIRE_EXPAN_TEAR,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_FIRE_EXPAN_TEAR - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_FIRE_EXTEAR = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_FIRE_EXPAN_TEAR = gettick() + battle_config.WOE_K_DELAY_GN_FIRE_EXPTEAR;
				sd->state.K_LOCK_GN_FIRE_EXTEAR = 0;
		} else {
			sd->K_CHK_GN_FIRE_EXPAN_TEAR = gettick() + battle_config.K_DELAY_GN_FIRE_EXPAN_TEAR;
			sd->state.K_LOCK_GN_FIRE_EXTEAR = 0;
		}
		break;
	case GN_FIRE_EXPANSION_ACID:
		if(DIFF_TICK(sd->K_CHK_GN_FIRE_EXPAN_ACID,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_FIRE_EXPAN_ACID,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_FIRE_EXPAN_ACID - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_FIRE_EXACID = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_FIRE_EXPAN_ACID = gettick() + battle_config.WOE_K_DELAY_GN_FIRE_EXPACID;
				sd->state.K_LOCK_GN_FIRE_EXACID = 0;
		} else {
			sd->K_CHK_GN_FIRE_EXPAN_ACID = gettick() + battle_config.K_DELAY_GN_FIRE_EXPAN_ACID;
			sd->state.K_LOCK_GN_FIRE_EXACID = 0;
		}
		break;
	case GN_HELLS_PLANT:
		if(DIFF_TICK(sd->K_CHK_GN_HELLS_PLANT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_HELLS_PLANT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_HELLS_PLANT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_HELLS_PLANT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_HELLS_PLANT = gettick() + battle_config.WOE_K_DELAY_GN_HELLSPLANT;
				sd->state.K_LOCK_GN_HELLS_PLANT = 0;
		} else {
			sd->K_CHK_GN_HELLS_PLANT = gettick() + battle_config.K_DELAY_GN_HELLS_PLANT;
			sd->state.K_LOCK_GN_HELLS_PLANT = 0;
		}
		break;
	case GN_HELLS_PLANT_ATK:
		if(DIFF_TICK(sd->K_CHK_GN_HELLS_PLANT_,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_HELLS_PLANT_,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_HELLS_PLANT_ - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_HELLS_PLANT_ = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_HELLS_PLANT_ = gettick() + battle_config.WOE_K_DELAY_GN_HELLSPLANT_;
				sd->state.K_LOCK_GN_HELLS_PLANT_ = 0;
		} else {
			sd->K_CHK_GN_HELLS_PLANT_ = gettick() + battle_config.K_DELAY_GN_HELLS_PLANT_;
			sd->state.K_LOCK_GN_HELLS_PLANT_ = 0;
		}
		break;
	case GN_MANDRAGORA:
		if(DIFF_TICK(sd->K_CHK_GN_MANDRAGORA,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_MANDRAGORA,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_MANDRAGORA - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_MANDRAGORA = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_MANDRAGORA = gettick() + battle_config.WOE_K_DELAY_GN_MANDRAGORA;
				sd->state.K_LOCK_GN_MANDRAGORA = 0;
		} else {
			sd->K_CHK_GN_MANDRAGORA = gettick() + battle_config.K_DELAY_GN_MANDRAGORA;
			sd->state.K_LOCK_GN_MANDRAGORA = 0;
		}
		break;
	case GN_SLINGITEM:
		if(DIFF_TICK(sd->K_CHK_GN_SLINGITEM,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_SLINGITEM,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_SLINGITEM - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_SLINGITEM = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_SLINGITEM = gettick() + battle_config.WOE_K_DELAY_GN_SLINGITEM;
				sd->state.K_LOCK_GN_SLINGITEM = 0;
		} else {
			sd->K_CHK_GN_SLINGITEM = gettick() + battle_config.K_DELAY_GN_SLINGITEM;
			sd->state.K_LOCK_GN_SLINGITEM = 0;
		}
		break;
	case GN_SLINGITEM_RANGEMELEEATK:
		if(DIFF_TICK(sd->K_CHK_GN_SLING_RANGEMELEE,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_SLING_RANGEMELEE,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_SLING_RANGEMELEE - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_SLINGITEM_ = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_SLING_RANGEMELEE = gettick() + battle_config.WOE_K_DELAY_GN_SLINGITEM_;
				sd->state.K_LOCK_GN_SLINGITEM_ = 0;
		} else {
			sd->K_CHK_GN_SLING_RANGEMELEE = gettick() + battle_config.K_DELAY_GN_SLING_RANGEMELEE;
			sd->state.K_LOCK_GN_SLINGITEM_ = 0;
		}
		break;
	case GN_CHANGEMATERIAL:
		if(DIFF_TICK(sd->K_CHK_GN_CHANGEMATERIAL,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_GN_CHANGEMATERIAL,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_GN_CHANGEMATERIAL - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_GN_CHANGEMATERIAL = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_GN_CHANGEMATERIAL = gettick() + battle_config.WOE_K_DELAY_GN_CHANGEMATERIAL;
				sd->state.K_LOCK_GN_CHANGEMATERIAL = 0;
		} else {
			sd->K_CHK_GN_CHANGEMATERIAL = gettick() + battle_config.K_DELAY_GN_CHANGEMATERIAL;
			sd->state.K_LOCK_GN_CHANGEMATERIAL = 0;
		}
		break;
	case AB_SECRAMENT:
		if(DIFF_TICK(sd->K_CHK_AB_SECRAMENT,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_AB_SECRAMENT,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_AB_SECRAMENT - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_AB_SECRAMENT = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_AB_SECRAMENT = gettick() + battle_config.WOE_K_DELAY_AB_SECRAMENT;
				sd->state.K_LOCK_AB_SECRAMENT = 0;
		} else {
			sd->K_CHK_AB_SECRAMENT = gettick() + battle_config.K_DELAY_AB_SECRAMENT;
			sd->state.K_LOCK_AB_SECRAMENT = 0;
		}
		break;
	case SR_HOWLINGOFLION:
		if(DIFF_TICK(sd->K_CHK_SR_HOWLINGOFLION,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_HOWLINGOFLION,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_HOWLINGOFLION - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_HOWLINGOFLION = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_HOWLINGOFLION = gettick() + battle_config.WOE_K_DELAY_SR_HOWLINGOFLION;
				sd->state.K_LOCK_SR_HOWLINGOFLION = 0;
		} else {
			sd->K_CHK_SR_HOWLINGOFLION = gettick() + battle_config.K_DELAY_SR_HOWLINGOFLION;
			sd->state.K_LOCK_SR_HOWLINGOFLION = 0;
		}
		break;
	case SR_RIDEINLIGHTNING:
		if(DIFF_TICK(sd->K_CHK_SR_RIDEINLIGHTNING,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_SR_RIDEINLIGHTNING,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_SR_RIDEINLIGHTNING - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_SR_RIDELIGHTNING = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_SR_RIDEINLIGHTNING = gettick() + battle_config.WOE_K_DELAY_SR_RIDEINLIGHTNING;
				sd->state.K_LOCK_SR_RIDELIGHTNING = 0;
		} else {
			sd->K_CHK_SR_RIDEINLIGHTNING = gettick() + battle_config.K_DELAY_SR_RIDEINLIGHTNING;
			sd->state.K_LOCK_SR_RIDELIGHTNING = 0;
		}
		break;
	case LG_OVERBRAND_BRANDISH:
		if(DIFF_TICK(sd->K_CHK_LG_OVERBRAND_BRANDISH,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_OVERBRAND_BRANDISH,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_OVERBRAND_BRANDISH - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_OVER_BRANDISH = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_OVERBRAND_BRANDISH = gettick() + battle_config.WOE_K_DELAY_LG_OVERBRAND;
				sd->state.K_LOCK_LG_OVER_BRANDISH = 0;
		} else {
			sd->K_CHK_LG_OVERBRAND_BRANDISH = gettick() + battle_config.K_DELAY_LG_OVERBRAND_BRANDISH;
			sd->state.K_LOCK_LG_OVER_BRANDISH = 0;
		}
		break;
	case LG_OVERBRAND_PLUSATK:
		if(DIFF_TICK(sd->K_CHK_LG_OVERBRAND_PLUSATK,gettick())> 0){
			if((map[m].flag.gvg_castle) && DIFF_TICK(sd->K_CHK_LG_OVERBRAND_PLUSATK,gettick())> show && (show != 0)){
				sprintf(message_skdelay,"[ %i ] second(s) skill use delay",(sd->K_CHK_LG_OVERBRAND_PLUSATK - gettick())/k);
				clif_disp_onlyself(sd, message_skdelay, (int)strlen(message_skdelay));
			}
			sd->state.K_LOCK_LG_OVER_PLUSATK = 1;
		} else if(sd && (map[m].flag.gvg_castle)){
				sd->K_CHK_LG_OVERBRAND_PLUSATK = gettick() + battle_config.WOE_K_DELAY_LG_OVERPLUSATK;
				sd->state.K_LOCK_LG_OVER_PLUSATK = 0;
		} else {
			sd->K_CHK_LG_OVERBRAND_PLUSATK = gettick() + battle_config.K_DELAY_LG_OVERBRAND_PLUSATK;
			sd->state.K_LOCK_LG_OVER_PLUSATK = 0;
		}
		break;
}
	return 1;
}


/*==========================================
 *
 *------------------------------------------*/
int skill_unit_move_unit_group (struct skill_unit_group *group, int m, int dx, int dy)
{
	int i,j;
	unsigned int tick = gettick();
	int *m_flag;
	struct skill_unit *unit1;
	struct skill_unit *unit2;

	if (group == NULL)
		return 0;
	if (group->unit_count<=0)
		return 0;
	if (group->unit==NULL)
		return 0;

	if (skill_get_unit_flag(group->skill_id)&UF_ENSEMBLE)
		return 0; //Ensembles may not be moved around.

	if( group->unit_id == UNT_ICEWALL )
		return 0; //Icewalls don't get knocked back

	m_flag = (int *) aCalloc(group->unit_count, sizeof(int));
	//    m_flag
	//		0: Neither of the following (skill_unit_onplace & skill_unit_onout are needed)
	//		1: Unit will move to a slot that had another unit of the same group (skill_unit_onplace not needed)
	//		2: Another unit from same group will end up positioned on this unit (skill_unit_onout not needed)
	//		3: Both 1+2.
	for(i=0;i<group->unit_count;i++){
		unit1=&group->unit[i];
		if (!unit1->alive || unit1->bl.m!=m)
			continue;
		for(j=0;j<group->unit_count;j++){
			unit2=&group->unit[j];
			if (!unit2->alive)
				continue;
			if (unit1->bl.x+dx==unit2->bl.x && unit1->bl.y+dy==unit2->bl.y){
				m_flag[i] |= 0x1;
			}
			if (unit1->bl.x-dx==unit2->bl.x && unit1->bl.y-dy==unit2->bl.y){
				m_flag[i] |= 0x2;
			}
		}
	}
	j = 0;
	for (i=0;i<group->unit_count;i++) {
		unit1=&group->unit[i];
		if (!unit1->alive)
			continue;
		if (!(m_flag[i]&0x2)) {
			if (group->state.song_dance&0x1) //Cancel dissonance effect.
				skill_dance_overlap(unit1, 0);
			map_foreachincell(skill_unit_effect,unit1->bl.m,unit1->bl.x,unit1->bl.y,group->bl_flag,&unit1->bl,tick,4);
		}
		//Move Cell using "smart" criteria (avoid useless moving around)
		switch(m_flag[i])
		{
			case 0:
			//Cell moves independently, safely move it.
				map_moveblock(&unit1->bl, unit1->bl.x+dx, unit1->bl.y+dy, tick);
				break;
			case 1:
			//Cell moves unto another cell, look for a replacement cell that won't collide
			//and has no cell moving into it (flag == 2)
				for(;j<group->unit_count;j++)
				{
					if(m_flag[j]!=2 || !group->unit[j].alive)
						continue;
					//Move to where this cell would had moved.
					unit2 = &group->unit[j];
					map_moveblock(&unit1->bl, unit2->bl.x+dx, unit2->bl.y+dy, tick);
					j++; //Skip this cell as we have used it.
					break;
				}
				break;
			case 2:
			case 3:
				break; //Don't move the cell as a cell will end on this tile anyway.
		}
		if (!(m_flag[i]&0x2)) { //We only moved the cell in 0-1
			if (group->state.song_dance&0x1) //Check for dissonance effect.
				skill_dance_overlap(unit1, 1);
			clif_skill_setunit(unit1);
			map_foreachincell(skill_unit_effect,unit1->bl.m,unit1->bl.x,unit1->bl.y,group->bl_flag,&unit1->bl,tick,1);
		}
	}
	aFree(m_flag);
	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_can_produce_mix (struct map_session_data *sd, int nameid, int trigger, int qty)
{
	int i,j;

	nullpo_ret(sd);

	if(nameid<=0)
		return 0;

	for(i=0;i<MAX_SKILL_PRODUCE_DB;i++){
		if(skill_produce_db[i].nameid == nameid )
			break;
	}
	if( i >= MAX_SKILL_PRODUCE_DB )
		return 0;

	if( pc_checkadditem(sd, nameid, qty) == ADDITEM_OVERAMOUNT )
	{// cannot carry the produced stuff
		return 0;
	}

	if(trigger>=0){
		if(trigger>20) { // Non-weapon, non-food item (itemlv must match)
			if(skill_produce_db[i].itemlv!=trigger)
				return 0;
		} else if(trigger>10) { // Food (any item level between 10 and 20 will do)
			if(skill_produce_db[i].itemlv<=10 || skill_produce_db[i].itemlv>20)
				return 0;
		} else { // Weapon (itemlv must be higher or equal)
			if(skill_produce_db[i].itemlv>trigger)
				return 0;
		}
	}
	if((j=skill_produce_db[i].req_skill)>0 &&
		pc_checkskill(sd,j) < skill_produce_db[i].req_skill_lv)
		return 0;

	for(j=0;j<MAX_PRODUCE_RESOURCE;j++){
		int id,x,y;
		if( (id=skill_produce_db[i].mat_id[j]) <= 0 )
			continue;
		if(skill_produce_db[i].mat_amount[j] <= 0) {
			if(pc_search_inventory(sd,id) < 0)
				return 0;
		}
		else {
			for(y=0,x=0;y<MAX_INVENTORY;y++)
				if( sd->status.inventory[y].nameid == id )
					x+=sd->status.inventory[y].amount;
			if(x<qty*skill_produce_db[i].mat_amount[j])
				return 0;
		}
	}
	return i+1;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_produce_mix (struct map_session_data *sd, int skill_id, int nameid, int slot1, int slot2, int slot3, int qty)
{
	int slot[3];
	int i,sc,ele,idx,equip,wlv,make_per,flag;
	int num = -1; // exclude the recipe
	struct status_data *status;

	nullpo_ret(sd);
	status = status_get_status_data(&sd->bl);

	if( !(idx=skill_can_produce_mix(sd,nameid,-1, qty)) )
		return 0;
	idx--;

	if (qty < 1)
		qty = 1;

	if (!skill_id) //A skill can be specified for some override cases.
		skill_id = skill_produce_db[idx].req_skill;

	slot[0]=slot1;
	slot[1]=slot2;
	slot[2]=slot3;

	for(i=0,sc=0,ele=0;i<3;i++){ //Note that qty should always be one if you are using these!
		int j;
		if( slot[i]<=0 )
			continue;
		j = pc_search_inventory(sd,slot[i]);
		if(j < 0)
			continue;
		if(slot[i]==1000){	/* Star Crumb */
			pc_delitem(sd,j,1,1,0,LOG_TYPE_PRODUCE);
			sc++;
		}
		if(slot[i]>=994 && slot[i]<=997 && ele==0){	/* Flame Heart . . . Great Nature */
			static const int ele_table[4]={3,1,4,2};
			pc_delitem(sd,j,1,1,0,LOG_TYPE_PRODUCE);
			ele=ele_table[slot[i]-994];
		}
	}

	for(i=0;i<MAX_PRODUCE_RESOURCE;i++){
		int j,id,x;
		if( (id=skill_produce_db[idx].mat_id[i]) <= 0 )
			continue;
		num++;
		x=qty*skill_produce_db[idx].mat_amount[i];
		do{
			int y=0;
			j = pc_search_inventory(sd,id);

			if(j >= 0){
				y = sd->status.inventory[j].amount;
				if(y>x)y=x;
				pc_delitem(sd,j,y,0,0,LOG_TYPE_PRODUCE);
			} else
				ShowError("skill_produce_mix: material item error\n");

			x-=y;
		}while( j>=0 && x>0 );
	}

	if((equip=itemdb_isequip(nameid)))
		wlv = itemdb_wlv(nameid);
	if(!equip) {
		switch(skill_id){
			case BS_IRON:
			case BS_STEEL:
			case BS_ENCHANTEDSTONE:
				// Ores & Metals Refining - skill bonuses are straight from kRO website [DracoRPG]
				i = pc_checkskill(sd,skill_id);
				make_per = sd->status.job_level*20 + status->dex*10 + status->luk*10; //Base chance
				switch(nameid){
					case 998: // Iron
						make_per += 4000+i*500; // Temper Iron bonus: +26/+32/+38/+44/+50
						break;
					case 999: // Steel
						make_per += 3000+i*500; // Temper Steel bonus: +35/+40/+45/+50/+55
						break;
					case 1000: //Star Crumb
						make_per = 100000; // Star Crumbs are 100% success crafting rate? (made 1000% so it succeeds even after penalties) [Skotlex]
						break;
					default: // Enchanted Stones
						make_per += 1000+i*500; // Enchantedstone Craft bonus: +15/+20/+25/+30/+35
					break;
				}
				break;
			case ASC_CDP:
				make_per = (2000 + 40*status->dex + 20*status->luk);
				break;
			case AL_HOLYWATER:
				make_per = 100000; //100% success
				break;
			case AM_PHARMACY: // Potion Preparation - reviewed with the help of various Ragnainfo sources [DracoRPG]
			case AM_TWILIGHT1:
			case AM_TWILIGHT2:
			case AM_TWILIGHT3:
				make_per = pc_checkskill(sd,AM_LEARNINGPOTION)*50
					+ pc_checkskill(sd,AM_PHARMACY)*300 + sd->status.job_level*20
					+ (status->int_/2)*10 + status->dex*10+status->luk*10;
				if(merc_is_hom_active(sd->hd)) {//Player got a homun
					int skill;
					if((skill=merc_hom_checkskill(sd->hd,HVAN_INSTRUCT)) > 0) //His homun is a vanil with instruction change
						make_per += skill*100; //+1% bonus per level
				}
				switch(nameid){
					case 501: // Red Potion
					case 503: // Yellow Potion
					case 504: // White Potion
						make_per += (1+rand()%100)*10 + 2000;
						break;
					case 970: // Alcohol
						make_per += (1+rand()%100)*10 + 1000;
						break;
					case 7135: // Bottle Grenade
					case 7136: // Acid Bottle
					case 7137: // Plant Bottle
					case 7138: // Marine Sphere Bottle
						make_per += (1+rand()%100)*10;
						break;
					case 546: // Condensed Yellow Potion
						make_per -= (1+rand()%50)*10;
						break;
					case 547: // Condensed White Potion
					case 7139: // Glistening Coat
						make_per -= (1+rand()%100)*10;
					    break;
					//Common items, recieve no bonus or penalty, listed just because they are commonly produced
					case 505: // Blue Potion
					case 545: // Condensed Red Potion
					case 605: // Anodyne
					case 606: // Aloevera
					default:
						break;
				}
				if(battle_config.pp_rate != 100)
					make_per = make_per * battle_config.pp_rate / 100;
				break;
			case SA_CREATECON: // Elemental Converter Creation
				make_per = 100000; // should be 100% success rate
				break;
			default:
				if (sd->menuskill_id ==	AM_PHARMACY &&
					sd->menuskill_val > 10 && sd->menuskill_val <= 20)
				{	//Assume Cooking Dish
					if (sd->menuskill_val >= 15) //Legendary Cooking Set.
						make_per = 10000; //100% Success
					else
						make_per = 1200 * (sd->menuskill_val - 10)
							+ 20  * (sd->status.base_level + 1)
							+ 20  * (status->dex + 1)
							+ 100 * (rand()%(30+5*(sd->cook_mastery/400) - (6+sd->cook_mastery/80)) + (6+sd->cook_mastery/80))
							- 400 * (skill_produce_db[idx].itemlv - 11 + 1)
							- 10  * (100 - status->luk + 1)
							- 500 * (num - 1)
							- 100 * (rand()%4 + 1);
					break;
				}
				make_per = 5000;
				break;
		}
	} else { // Weapon Forging - skill bonuses are straight from kRO website, other things from a jRO calculator [DracoRPG]
		make_per = 5000 + sd->status.job_level*20 + status->dex*10 + status->luk*10; // Base
		make_per += pc_checkskill(sd,skill_id)*500; // Smithing skills bonus: +5/+10/+15
		make_per += pc_checkskill(sd,BS_WEAPONRESEARCH)*100 +((wlv >= 3)? pc_checkskill(sd,BS_ORIDEOCON)*100:0); // Weaponry Research bonus: +1/+2/+3/+4/+5/+6/+7/+8/+9/+10, Oridecon Research bonus (custom): +1/+2/+3/+4/+5
		make_per -= (ele?2000:0) + sc*1500 + (wlv>1?wlv*1000:0); // Element Stone: -20%, Star Crumb: -15% each, Weapon level malus: -0/-20/-30
		if(pc_search_inventory(sd,989) > 0) make_per+= 1000; // Emperium Anvil: +10
		else if(pc_search_inventory(sd,988) > 0) make_per+= 500; // Golden Anvil: +5
		else if(pc_search_inventory(sd,987) > 0) make_per+= 300; // Oridecon Anvil: +3
		else if(pc_search_inventory(sd,986) > 0) make_per+= 0; // Anvil: +0?
		if(battle_config.wp_rate != 100)
			make_per = make_per * battle_config.wp_rate / 100;
	}

	if (sd->class_&JOBL_BABY) //if it's a Baby Class
		make_per = (make_per * 70) / 100; //Baby penalty is 30%

	if(make_per < 1) make_per = 1;


	if(rand()%10000 < make_per || qty > 1){ //Success, or crafting multiple items.
		struct item tmp_item;
		memset(&tmp_item,0,sizeof(tmp_item));
		tmp_item.nameid=nameid;
		tmp_item.amount=1;
		tmp_item.identify=1;
		if(equip){
			tmp_item.card[0]=CARD0_FORGE;
			tmp_item.card[1]=((sc*5)<<8)+ele;
			tmp_item.card[2]=GetWord(sd->status.char_id,0); // CharId
			tmp_item.card[3]=GetWord(sd->status.char_id,1);
		} else {
			//Flag is only used on the end, so it can be used here. [Skotlex]
			switch (skill_id) {
				case BS_DAGGER:
				case BS_SWORD:
				case BS_TWOHANDSWORD:
				case BS_AXE:
				case BS_MACE:
				case BS_KNUCKLE:
				case BS_SPEAR:
					flag = battle_config.produce_item_name_input&0x1;
					break;
				case AM_PHARMACY:
				case AM_TWILIGHT1:
				case AM_TWILIGHT2:
				case AM_TWILIGHT3:
					flag = battle_config.produce_item_name_input&0x2;
					break;
				case AL_HOLYWATER:
					flag = battle_config.produce_item_name_input&0x8;
					break;
				case ASC_CDP:
					flag = battle_config.produce_item_name_input&0x10;
					break;
				default:
					flag = battle_config.produce_item_name_input&0x80;
					break;
			}
			if (flag) {
				tmp_item.card[0]=CARD0_CREATE;
				tmp_item.card[1]=0;
				tmp_item.card[2]=GetWord(sd->status.char_id,0); // CharId
				tmp_item.card[3]=GetWord(sd->status.char_id,1);
			}
		}

//		if(log_config.produce > 0)
//			log_produce(sd,nameid,slot1,slot2,slot3,1);
//TODO update PICKLOG

		if(equip){
			clif_produceeffect(sd,0,nameid);
			clif_misceffect(&sd->bl,3);
			if(itemdb_wlv(nameid) >= 3 && ((ele? 1 : 0) + sc) >= 3) // Fame point system [DracoRPG]
				pc_addfame(sd,10,0); // Success to forge a lv3 weapon with 3 additional ingredients = +10 fame point
		} else {
			int fame = 0;
			tmp_item.amount = 0;
			for (i=0; i< qty; i++)
			{	//Apply quantity modifiers.
				if (rand()%10000 < make_per || qty == 1)
				{ //Success
					tmp_item.amount++;
					if(nameid < 545 || nameid > 547)
						continue;
					if(skill_id != AM_PHARMACY &&
						skill_id != AM_TWILIGHT1 &&
						skill_id != AM_TWILIGHT2 &&
						skill_id != AM_TWILIGHT3)
						continue;
					//Add fame as needed.
					switch(++sd->potion_success_counter) {
						case 3:
							fame+=1; // Success to prepare 3 Condensed Potions in a row
							break;
						case 5:
							fame+=3; // Success to prepare 5 Condensed Potions in a row
							break;
						case 7:
							fame+=10; // Success to prepare 7 Condensed Potions in a row
							break;
						case 10:
							fame+=50; // Success to prepare 10 Condensed Potions in a row
							sd->potion_success_counter = 0;
							break;
					}
				} else //Failure
					sd->potion_success_counter = 0;
			}
			if (fame)
				pc_addfame(sd,fame,0);
			//Visual effects and the like.
			switch (skill_id) {
				case AM_PHARMACY:
				case AM_TWILIGHT1:
				case AM_TWILIGHT2:
				case AM_TWILIGHT3:
				case ASC_CDP:
					clif_produceeffect(sd,2,nameid);
					clif_misceffect(&sd->bl,5);
					break;
				case BS_IRON:
				case BS_STEEL:
				case BS_ENCHANTEDSTONE:
					clif_produceeffect(sd,0,nameid);
					clif_misceffect(&sd->bl,3);
					break;
				default: //Those that don't require a skill?
					if( skill_produce_db[idx].itemlv > 10 && skill_produce_db[idx].itemlv <= 20)
					{ //Cooking items.
						clif_specialeffect(&sd->bl, 608, AREA);
						if( sd->cook_mastery < 1999 )
							pc_setglobalreg(sd, "COOK_MASTERY",sd->cook_mastery + ( 1 << ( (skill_produce_db[idx].itemlv - 11) / 2 ) ) * 5);
					}
					break;
			}
		}
		if (tmp_item.amount) { //Success
			if((flag = pc_additem(sd,&tmp_item,tmp_item.amount,LOG_TYPE_PRODUCE))) {
				clif_additem(sd,0,0,flag);
				map_addflooritem(&tmp_item,tmp_item.amount,sd->bl.m,sd->bl.x,sd->bl.y,0,0,0,0,0);
			}
			return 1;
		}
	}
	//Failure
//	if(log_config.produce)
//		log_produce(sd,nameid,slot1,slot2,slot3,0);
//TODO update PICKLOG

	if(equip){
		clif_produceeffect(sd,1,nameid);
		clif_misceffect(&sd->bl,2);
	} else {
		switch (skill_id) {
			case ASC_CDP: //25% Damage yourself, and display same effect as failed potion.
				status_percent_damage(NULL, &sd->bl, -25, 0, true);
			case AM_PHARMACY:
			case AM_TWILIGHT1:
			case AM_TWILIGHT2:
			case AM_TWILIGHT3:
				clif_produceeffect(sd,3,nameid);
				clif_misceffect(&sd->bl,6);
				sd->potion_success_counter = 0; // Fame point system [DracoRPG]
				break;
			case BS_IRON:
			case BS_STEEL:
			case BS_ENCHANTEDSTONE:
				clif_produceeffect(sd,1,nameid);
				clif_misceffect(&sd->bl,2);
				break;
			default:
				if( skill_produce_db[idx].itemlv > 10 && skill_produce_db[idx].itemlv <= 20 )
				{ //Cooking items.
					clif_specialeffect(&sd->bl, 609, AREA);
					if( sd->cook_mastery > 0 )
						pc_setglobalreg(sd, "COOK_MASTERY", sd->cook_mastery - ( 1 << ((skill_produce_db[idx].itemlv - 11) / 2) ) - ( ( ( 1 << ((skill_produce_db[idx].itemlv - 11) / 2) ) >> 1 ) * 3 ));
				}
		}
	}
	return 0;
}

int skill_arrow_create (struct map_session_data *sd, int nameid)
{
	int i,j,flag,index=-1;
	struct item tmp_item;

	nullpo_ret(sd);

	if(nameid <= 0)
		return 1;

	for(i=0;i<MAX_SKILL_ARROW_DB;i++)
		if(nameid == skill_arrow_db[i].nameid) {
			index = i;
			break;
		}

	if(index < 0 || (j = pc_search_inventory(sd,nameid)) < 0)
		return 1;

	pc_delitem(sd,j,1,0,0,LOG_TYPE_PRODUCE);
	for(i=0;i<MAX_ARROW_RESOURCE;i++) {
		memset(&tmp_item,0,sizeof(tmp_item));
		tmp_item.identify = 1;
		tmp_item.nameid = skill_arrow_db[index].cre_id[i];
		tmp_item.amount = skill_arrow_db[index].cre_amount[i];
		if(battle_config.produce_item_name_input&0x4) {
			tmp_item.card[0]=CARD0_CREATE;
			tmp_item.card[1]=0;
			tmp_item.card[2]=GetWord(sd->status.char_id,0); // CharId
			tmp_item.card[3]=GetWord(sd->status.char_id,1);
		}
		if(tmp_item.nameid <= 0 || tmp_item.amount <= 0)
			continue;
		if((flag = pc_additem(sd,&tmp_item,tmp_item.amount,LOG_TYPE_PRODUCE))) {
			clif_additem(sd,0,0,flag);
			map_addflooritem(&tmp_item,tmp_item.amount,sd->bl.m,sd->bl.x,sd->bl.y,0,0,0,0,0);
		}
	}

	return 0;
}

/*==========================================
 *
 *------------------------------------------*/
int skill_blockpc_get(struct map_session_data *sd, int skillid)
{
	int i;
	nullpo_retr(-1,sd);

	ARR_FIND(0, MAX_SKILLCOOLDOWN, i, sd->scd[i] && sd->scd[i]->skill_id == skillid);
	return (i >= MAX_SKILLCOOLDOWN) ? -1 : i;
}

int skill_blockpc_end(int tid, unsigned int tick, int id, intptr_t data)
{
	struct map_session_data *sd = map_id2sd(id);
	int i = (int)data;

	if( !sd || data < 0 || data >= MAX_SKILLCOOLDOWN )
		return 0;

	if( !sd->scd[i] || sd->scd[i]->timer != tid )
	{
		ShowWarning("skill_blockpc_end: Invalid Timer or not Skill Cooldown.\n");
		return 0;
	}

	aFree(sd->scd[i]);
	sd->scd[i] = NULL;
	return 1;
}

int skill_blockpc_start(struct map_session_data *sd, int skillid, int tick)
{
	int i;
	nullpo_retr(-1,sd);
	if( skillid == 0 || tick < 1 )
		return -1;

	ARR_FIND(0,MAX_SKILLCOOLDOWN,i,sd->scd[i] && sd->scd[i]->skill_id == skillid);
	if( i < MAX_SKILLCOOLDOWN )
	{ // Skill already with cooldown
		delete_timer(sd->scd[i]->timer,skill_blockpc_end);
		aFree(sd->scd[i]);
		sd->scd[i] = NULL;
	}

	ARR_FIND(0,MAX_SKILLCOOLDOWN,i,!sd->scd[i]);
	if( i < MAX_SKILLCOOLDOWN )
	{ // Free Slot found
		CREATE(sd->scd[i],struct skill_cooldown_entry, 1);
		sd->scd[i]->skill_id = skillid;
		sd->scd[i]->timer = add_timer(gettick() + tick, skill_blockpc_end, sd->bl.id, i);

		if( battle_config.display_status_timers && tick > 0 )
			clif_skill_cooldown(sd,skillid,tick);

		return 1;
	}
	else
	{
		ShowWarning("skill_blockpc_start: Too many skillcooldowns, increase MAX_SKILLCOOLDOWN.\n");
		return 0;
	}
}

int skill_blockpc_clear(struct map_session_data *sd)
{
	int i;
	nullpo_ret(sd);
	for( i = 0; i < MAX_SKILLCOOLDOWN; i++ )
	{
		if( !sd->scd[i] )
			continue;
		delete_timer(sd->scd[i]->timer,skill_blockpc_end);
		aFree(sd->scd[i]);
		sd->scd[i] = NULL;
	}
	return 1;
}

int skill_blockhomun_end(int tid, unsigned int tick, int id, intptr_t data)	//[orn]
{
	struct homun_data *hd = (TBL_HOM*) map_id2bl(id);
	if (data <= 0 || data >= MAX_SKILL)
		return 0;
	if (hd) hd->blockskill[data] = 0;

	return 1;
}

int skill_blockhomun_start(struct homun_data *hd, int skillid, int tick)	//[orn]
{
	nullpo_retr (-1, hd);

	skillid = skill_get_index(skillid);
	if (skillid == 0)
		return -1;

	if (tick < 1) {
		hd->blockskill[skillid] = 0;
		return -1;
	}
	hd->blockskill[skillid] = 1;
	return add_timer(gettick() + tick, skill_blockhomun_end, hd->bl.id, skillid);
}

int skill_blockmerc_end(int tid, unsigned int tick, int id, intptr_t data)	//[orn]
{
	struct mercenary_data *md = (TBL_MER*)map_id2bl(id);
	if( data <= 0 || data >= MAX_SKILL )
		return 0;
	if( md ) md->blockskill[data] = 0;

	return 1;
}

int skill_blockmerc_start(struct mercenary_data *md, int skillid, int tick)
{
	nullpo_retr (-1, md);

	if( (skillid = skill_get_index(skillid)) == 0 )
		return -1;
	if( tick < 1 )
	{
		md->blockskill[skillid] = 0;
		return -1;
	}
	md->blockskill[skillid] = 1;
	return add_timer(gettick() + tick, skill_blockmerc_end, md->bl.id, skillid);
}

/*
 *
 */
int skill_split_str (char *str, char **val, int num)
{
	int i;

	for( i = 0; i < num && str; i++ )
	{
		val[i] = str;
		str = strchr(str,',');
		if( str )
			*str++=0;
	}

	return i;
}
/*
 *
 */
int skill_split_atoi (char *str, int *val)
{
	int i, j, diff, step = 1;

	for (i=0; i<MAX_SKILL_LEVEL; i++) {
		if (!str) break;
		val[i] = atoi(str);
		str = strchr(str,':');
		if (str)
			*str++=0;
	}
	if(i==0) //No data found.
		return 0;
	if(i==1)
	{	//Single value, have the whole range have the same value.
		for (; i < MAX_SKILL_LEVEL; i++)
			val[i] = val[i-1];
		return i;
	}
	//Check for linear change with increasing steps until we reach half of the data acquired.
	for (step = 1; step <= i/2; step++)
	{
		diff = val[i-1] - val[i-step-1];
		for(j = i-1; j >= step; j--)
			if ((val[j]-val[j-step]) != diff)
				break;

		if (j>=step) //No match, try next step.
			continue;

		for(; i < MAX_SKILL_LEVEL; i++)
		{	//Apply linear increase
			val[i] = val[i-step]+diff;
			if (val[i] < 1 && val[i-1] >=0) //Check if we have switched from + to -, cap the decrease to 0 in said cases.
			{ val[i] = 1; diff = 0; step = 1; }
		}
		return i;
	}
	//Okay.. we can't figure this one out, just fill out the stuff with the previous value.
	for (;i<MAX_SKILL_LEVEL; i++)
		val[i] = val[i-1];
	return i;
}

/*
 *
 */
void skill_init_unit_layout (void)
{
	int i,j,size,pos = 0;

	memset(skill_unit_layout,0,sizeof(skill_unit_layout));

	// standard square layouts go first
	for (i=0; i<=MAX_SQUARE_LAYOUT; i++) {
		size = i*2+1;
		skill_unit_layout[i].count = size*size;
		for (j=0; j<size*size; j++) {
			skill_unit_layout[i].dx[j] = (j%size-i);
			skill_unit_layout[i].dy[j] = (j/size-i);
		}
	}

	// afterwards add special ones
	pos = i;
	for (i=0;i<MAX_SKILL_DB;i++) {
		if (!skill_db[i].unit_id[0] || skill_db[i].unit_layout_type[0] != -1)
			continue;
		switch (i) {
			case MG_FIREWALL:
			case WZ_ICEWALL:
				// these will be handled later
				break;
			case PR_SANCTUARY:
			case NPC_EVILLAND:
			{
				static const int dx[] = {
					-1, 0, 1,-2,-1, 0, 1, 2,-2,-1,
					 0, 1, 2,-2,-1, 0, 1, 2,-1, 0, 1};
				static const int dy[]={
					-2,-2,-2,-1,-1,-1,-1,-1, 0, 0,
					 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2};
				skill_unit_layout[pos].count = 21;
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
				break;
			}
			case PR_MAGNUS:
			{
				static const int dx[] = {
					-1, 0, 1,-1, 0, 1,-3,-2,-1, 0,
					 1, 2, 3,-3,-2,-1, 0, 1, 2, 3,
					-3,-2,-1, 0, 1, 2, 3,-1, 0, 1,-1, 0, 1};
				static const int dy[] = {
					-3,-3,-3,-2,-2,-2,-1,-1,-1,-1,
					-1,-1,-1, 0, 0, 0, 0, 0, 0, 0,
					 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3};
				skill_unit_layout[pos].count = 33;
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
				break;
			}
			case AS_VENOMDUST:
			{
				static const int dx[] = {-1, 0, 0, 0, 1};
				static const int dy[] = { 0,-1, 0, 1, 0};
				skill_unit_layout[pos].count = 5;
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
				break;
			}
			case CR_GRANDCROSS:
			case NPC_GRANDDARKNESS:
			{
				static const int dx[] = {
					 0, 0,-1, 0, 1,-2,-1, 0, 1, 2,
					-4,-3,-2,-1, 0, 1, 2, 3, 4,-2,
					-1, 0, 1, 2,-1, 0, 1, 0, 0};
				static const int dy[] = {
					-4,-3,-2,-2,-2,-1,-1,-1,-1,-1,
					 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
					 1, 1, 1, 1, 2, 2, 2, 3, 4};
				skill_unit_layout[pos].count = 29;
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
				break;
			}
			case PF_FOGWALL:
			{
				static const int dx[] = {
					-2,-1, 0, 1, 2,-2,-1, 0, 1, 2,-2,-1, 0, 1, 2};
				static const int dy[] = {
					-1,-1,-1,-1,-1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1};
				skill_unit_layout[pos].count = 15;
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
				break;
			}
			case PA_GOSPEL:
			{
				static const int dx[] = {
					-1, 0, 1,-1, 0, 1,-3,-2,-1, 0,
					 1, 2, 3,-3,-2,-1, 0, 1, 2, 3,
					-3,-2,-1, 0, 1, 2, 3,-1, 0, 1,
					-1, 0, 1};
				static const int dy[] = {
					-3,-3,-3,-2,-2,-2,-1,-1,-1,-1,
					-1,-1,-1, 0, 0, 0, 0, 0, 0, 0,
					 1, 1, 1, 1, 1, 1, 1, 2, 2, 2,
					 3, 3, 3};
				skill_unit_layout[pos].count = 33;
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
				break;
			}
			case NJ_KAENSIN:
			{
				static const int dx[] = {-2,-1, 0, 1, 2,-2,-1, 0, 1, 2,-2,-1, 1, 2,-2,-1, 0, 1, 2,-2,-1, 0, 1, 2};
				static const int dy[] = { 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0,-1,-1,-1,-1,-1,-2,-2,-2,-2,-2};
				skill_unit_layout[pos].count = 24;
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
				break;
			}
			case NJ_TATAMIGAESHI:
			{
				//Level 1 (count 4, cross of 3x3)
				static const int dx1[] = {-1, 1, 0, 0};
				static const int dy1[] = { 0, 0,-1, 1};
				//Level 2-3 (count 8, cross of 5x5)
				static const int dx2[] = {-2,-1, 1, 2, 0, 0, 0, 0};
				static const int dy2[] = { 0, 0, 0, 0,-2,-1, 1, 2};
				//Level 4-5 (count 12, cross of 7x7
				static const int dx3[] = {-3,-2,-1, 1, 2, 3, 0, 0, 0, 0, 0, 0};
				static const int dy3[] = { 0, 0, 0, 0, 0, 0,-3,-2,-1, 1, 2, 3};
				//lv1
				j = 0;
				skill_unit_layout[pos].count = 4;
				memcpy(skill_unit_layout[pos].dx,dx1,sizeof(dx1));
				memcpy(skill_unit_layout[pos].dy,dy1,sizeof(dy1));
				skill_db[i].unit_layout_type[j] = pos;
				//lv2/3
				j++;
				pos++;
				skill_unit_layout[pos].count = 8;
				memcpy(skill_unit_layout[pos].dx,dx2,sizeof(dx2));
				memcpy(skill_unit_layout[pos].dy,dy2,sizeof(dy2));
				skill_db[i].unit_layout_type[j] = pos;
				skill_db[i].unit_layout_type[++j] = pos;
				//lv4/5
				j++;
				pos++;
				skill_unit_layout[pos].count = 12;
				memcpy(skill_unit_layout[pos].dx,dx3,sizeof(dx3));
				memcpy(skill_unit_layout[pos].dy,dy3,sizeof(dy3));
				skill_db[i].unit_layout_type[j] = pos;
				skill_db[i].unit_layout_type[++j] = pos;
				//Fill in the rest using lv 5.
				for (;j<MAX_SKILL_LEVEL;j++)
					skill_db[i].unit_layout_type[j] = pos;
				//Skip, this way the check below will fail and continue to the next skill.
				pos++;
				break;
			}
			default:
				ShowError("unknown unit layout at skill %d\n",i);
				break;
		}
		if (!skill_unit_layout[pos].count)
			continue;
		for (j=0;j<MAX_SKILL_LEVEL;j++)
			skill_db[i].unit_layout_type[j] = pos;
		pos++;
	}

	// firewall and icewall have 8 layouts (direction-dependent)
	firewall_unit_pos = pos;
	for (i=0;i<8;i++) {
		if (i&1) {
			skill_unit_layout[pos].count = 5;
			if (i&0x2) {
				int dx[] = {-1,-1, 0, 0, 1};
				int dy[] = { 1, 0, 0,-1,-1};
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
			} else {
				int dx[] = { 1, 1 ,0, 0,-1};
				int dy[] = { 1, 0, 0,-1,-1};
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
			}
		} else {
			skill_unit_layout[pos].count = 3;
			if (i%4==0) {
				int dx[] = {-1, 0, 1};
				int dy[] = { 0, 0, 0};
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
			} else {
				int dx[] = { 0, 0, 0};
				int dy[] = {-1, 0, 1};
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
			}
		}
		pos++;
	}
	icewall_unit_pos = pos;
	for (i=0;i<8;i++) {
		skill_unit_layout[pos].count = 5;
		if (i&1) {
			if (i&0x2) {
				int dx[] = {-2,-1, 0, 1, 2};
				int dy[] = { 2, 1, 0,-1,-2};
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
			} else {
				int dx[] = { 2, 1 ,0,-1,-2};
				int dy[] = { 2, 1, 0,-1,-2};
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
			}
		} else {
			if (i%4==0) {
				int dx[] = {-2,-1, 0, 1, 2};
				int dy[] = { 0, 0, 0, 0, 0};
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
			} else {
				int dx[] = { 0, 0, 0, 0, 0};
				int dy[] = {-2,-1, 0, 1, 2};
				memcpy(skill_unit_layout[pos].dx,dx,sizeof(dx));
				memcpy(skill_unit_layout[pos].dy,dy,sizeof(dy));
			}
		}
		pos++;
	}
}

//
// Extended Vending System
//
int skill_vending(struct map_session_data *sd, int nameid)
{
	struct item_data *item;
	char output[256];
	nullpo_ret(sd);

	if ( !pc_can_give_items(pc_isGM(sd)) )
	{
		clif_skill_fail(sd,MC_VENDING,USESKILL_FAIL_LEVEL,0);
		return 0;
	}

	if( (item = itemdb_exists(nameid)) == NULL )
	{
		clif_skill_fail(sd,MC_VENDING,USESKILL_FAIL_LEVEL,0);
		return 0;
	}

	sd->vend_coin = nameid;
	clif_openvendingreq(sd,sd->menuskill_val+2);
	sprintf(output,msg_txt(913),item->jname);
	clif_displaymessage(sd->fd,output);

	return 0;
}

/*==========================================
 * DB reading.
 * skill_db.txt
 * skill_require_db.txt
 * skill_cast_db.txt
 * skill_castnodex_db.txt
 * skill_nocast_db.txt
 * skill_unit_db.txt
 * produce_db.txt
 * create_arrow_db.txt
 * abra_db.txt
 *------------------------------------------*/

static bool skill_parse_row_skilldb(char* split[], int columns, int current)
{// id,range,hit,inf,element,nk,splash,max,list_num,castcancel,cast_defence_rate,inf2,maxcount,skill_type,blow_count,name,description
	int id = atoi(split[0]);
	int i;
	if( (id >= GD_SKILLRANGEMIN && id <= GD_SKILLRANGEMAX)
	||  (id >= HM_SKILLRANGEMIN && id <= HM_SKILLRANGEMAX)
	||  (id >= MC_SKILLRANGEMIN && id <= MC_SKILLRANGEMAX) )
	{
		ShowWarning("skill_parse_row_skilldb: Skill id %d is forbidden (interferes with guild/homun/mercenary skill mapping)!\n", id);
		return false;
	}

	i = skill_get_index(id);
	if( !i ) // invalid skill id
		return false;

	skill_split_atoi(split[1],skill_db[i].range);
	skill_db[i].hit = atoi(split[2]);
	skill_db[i].inf = atoi(split[3]);
	skill_split_atoi(split[4],skill_db[i].element);
	skill_db[i].nk = (int)strtol(split[5], NULL, 0);
	skill_split_atoi(split[6],skill_db[i].splash);
	skill_db[i].max = atoi(split[7]);
	skill_split_atoi(split[8],skill_db[i].num);

	if( strcmpi(split[9],"yes") == 0 )
		skill_db[i].castcancel = 1;
	else
		skill_db[i].castcancel = 0;
	skill_db[i].cast_def_rate = atoi(split[10]);
	skill_db[i].inf2 = (int)strtol(split[11], NULL, 0);
	skill_split_atoi(split[12],skill_db[i].maxcount);
	if( strcmpi(split[13],"weapon") == 0 )
		skill_db[i].skill_type = BF_WEAPON;
	else if( strcmpi(split[13],"magic") == 0 )
		skill_db[i].skill_type = BF_MAGIC;
	else if( strcmpi(split[13],"misc") == 0 )
		skill_db[i].skill_type = BF_MISC;
	else
		skill_db[i].skill_type = 0;
	skill_split_atoi(split[14],skill_db[i].blewcount);
	safestrncpy(skill_db[i].name, trim(split[15]), sizeof(skill_db[i].name));
	safestrncpy(skill_db[i].desc, trim(split[16]), sizeof(skill_db[i].desc));
	strdb_put(skilldb_name2id, skill_db[i].name, (void*)(intptr_t)id);

	return true;
}

static bool skill_parse_row_requiredb(char* split[], int columns, int current)
{// SkillID,HPCost,MaxHPTrigger,SPCost,HPRateCost,SPRateCost,ZenyCost,RequiredWeapons,RequiredAmmoTypes,RequiredAmmoAmount,RequiredState,SpiritSphereCost,RequiredItemID1,RequiredItemAmount1,RequiredItemID2,RequiredItemAmount2,RequiredItemID3,RequiredItemAmount3,RequiredItemID4,RequiredItemAmount4,RequiredItemID5,RequiredItemAmount5,RequiredItemID6,RequiredItemAmount6,RequiredItemID7,RequiredItemAmount7,RequiredItemID8,RequiredItemAmount8,RequiredItemID9,RequiredItemAmount9,RequiredItemID10,RequiredItemAmount10
	char* p;
	int j;

	int i = atoi(split[0]);
	i = skill_get_index(i);
	if( !i ) // invalid skill id
		return false;

	skill_split_atoi(split[1],skill_db[i].hp);
	skill_split_atoi(split[2],skill_db[i].mhp);
	skill_split_atoi(split[3],skill_db[i].sp);
	skill_split_atoi(split[4],skill_db[i].hp_rate);
	skill_split_atoi(split[5],skill_db[i].sp_rate);
	skill_split_atoi(split[6],skill_db[i].zeny);

	//FIXME: document this
	p = split[7];
	for( j = 0; j < 32; j++ )
	{
		int l = atoi(p);
		if( l == 99 ) // Any weapon
		{
			skill_db[i].weapon = 0;
			break;
		}
		else
			skill_db[i].weapon |= 1<<l;
		p = strchr(p,':');
		if(!p)
			break;
		p++;
	}

	//FIXME: document this
	p = split[8];
	for( j = 0; j < 32; j++ )
	{
		int l = atoi(p);
		if( l == 99 ) // Any ammo type
		{
			skill_db[i].ammo = 0xFFFFFFFF;
			break;
		}
		else if( l ) // 0 stands for no requirement
			skill_db[i].ammo |= 1<<l;
		p = strchr(p,':');
		if( !p )
			break;
		p++;
	}
	skill_split_atoi(split[9],skill_db[i].ammo_qty);

	if(      strcmpi(split[10],"hiding")==0 ) skill_db[i].state = ST_HIDING;
	else if( strcmpi(split[10],"cloaking")==0 ) skill_db[i].state = ST_CLOAKING;
	else if( strcmpi(split[10],"hidden")==0 ) skill_db[i].state = ST_HIDDEN;
	else if( strcmpi(split[10],"riding")==0 ) skill_db[i].state = ST_RIDING;
	else if( strcmpi(split[10],"falcon")==0 ) skill_db[i].state = ST_FALCON;
	else if( strcmpi(split[10],"cart")==0 ) skill_db[i].state = ST_CART;
	else if( strcmpi(split[10],"shield")==0 ) skill_db[i].state = ST_SHIELD;
	else if( strcmpi(split[10],"sight")==0 ) skill_db[i].state = ST_SIGHT;
	else if( strcmpi(split[10],"explosionspirits")==0 ) skill_db[i].state = ST_EXPLOSIONSPIRITS;
	else if( strcmpi(split[10],"cartboost")==0 ) skill_db[i].state = ST_CARTBOOST;
	else if( strcmpi(split[10],"recover_weight_rate")==0 ) skill_db[i].state = ST_RECOV_WEIGHT_RATE;
	else if( strcmpi(split[10],"move_enable")==0 ) skill_db[i].state = ST_MOVE_ENABLE;
	else if( strcmpi(split[10],"water")==0 ) skill_db[i].state = ST_WATER;
	else skill_db[i].state = ST_NONE;

	skill_split_atoi(split[11],skill_db[i].spiritball);
	for( j = 0; j < MAX_SKILL_ITEM_REQUIRE; j++ ) {
		skill_db[i].itemid[j] = atoi(split[12+ 2*j]);
		skill_db[i].amount[j] = atoi(split[13+ 2*j]);
	}

	return true;
}

static bool skill_parse_row_castdb(char* split[], int columns, int current)
{// SkillID,CastingTime,AfterCastActDelay,AfterCastWalkDelay,Duration1,Duration2,Cooldown
	int i = atoi(split[0]);
	i = skill_get_index(i);
	if( !i ) // invalid skill id
		return false;

	skill_split_atoi(split[1],skill_db[i].cast);
	skill_split_atoi(split[2],skill_db[i].delay);
	skill_split_atoi(split[3],skill_db[i].walkdelay);
	skill_split_atoi(split[4],skill_db[i].upkeep_time);
	skill_split_atoi(split[5],skill_db[i].upkeep_time2);
	if( split[6] ) skill_split_atoi(split[6],skill_db[i].cooldown);

	return true;
}

static bool skill_parse_row_castnodexdb(char* split[], int columns, int current)
{// Skill id,Cast,Delay (optional)
	int i = atoi(split[0]);
	i = skill_get_index(i);
	if( !i ) // invalid skill id
		return false;

	skill_split_atoi(split[1],skill_db[i].castnodex);
	if( split[2] ) // optional column
		skill_split_atoi(split[2],skill_db[i].delaynodex);

	return true;
}

static bool skill_parse_row_nocastdb(char* split[], int columns, int current)
{// SkillID,Flag
	int i = atoi(split[0]);
	i = skill_get_index(i);
	if( !i ) // invalid skill id
		return false;

	skill_db[i].nocast |= atoi(split[1]);

	return true;
}

static bool skill_parse_row_blockdb(char* split[], int columns, int current)
{// SkillID,Flag
	int i = atoi(split[0]);
	i = skill_get_index(i);
	if( !i ) // invalid skill id
		return false;

	skill_db[i].blocked = (bool)atoi(split[1]);

	return true;
}

static bool skill_parse_row_unitdb(char* split[], int columns, int current)
{// ID,unit ID,unit ID 2,layout,range,interval,target,flag
	int i = atoi(split[0]);
	i = skill_get_index(i);
	if( !i ) // invalid skill id
		return false;

	skill_db[i].unit_id[0] = strtol(split[1],NULL,16);
	skill_db[i].unit_id[1] = strtol(split[2],NULL,16);
	skill_split_atoi(split[3],skill_db[i].unit_layout_type);
	skill_split_atoi(split[4],skill_db[i].unit_range);
	skill_db[i].unit_interval = atoi(split[5]);

	if( strcmpi(split[6],"noenemy")==0 ) skill_db[i].unit_target = BCT_NOENEMY;
	else if( strcmpi(split[6],"friend")==0 ) skill_db[i].unit_target = BCT_NOENEMY;
	else if( strcmpi(split[6],"party")==0 ) skill_db[i].unit_target = BCT_PARTY;
	else if( strcmpi(split[6],"ally")==0 ) skill_db[i].unit_target = BCT_PARTY|BCT_GUILD;
	else if( strcmpi(split[6],"all")==0 ) skill_db[i].unit_target = BCT_ALL;
	else if( strcmpi(split[6],"enemy")==0 ) skill_db[i].unit_target = BCT_ENEMY;
	else if( strcmpi(split[6],"self")==0 ) skill_db[i].unit_target = BCT_SELF;
	else if( strcmpi(split[6],"noone")==0 ) skill_db[i].unit_target = BCT_NOONE;
	else skill_db[i].unit_target = strtol(split[6],NULL,16);

	skill_db[i].unit_flag = strtol(split[7],NULL,16);

	if (skill_db[i].unit_flag&UF_DEFNOTENEMY && battle_config.defnotenemy)
		skill_db[i].unit_target = BCT_NOENEMY;

	//By default, target just characters.
	skill_db[i].unit_target |= BL_CHAR;
	if (skill_db[i].unit_flag&UF_NOPC)
		skill_db[i].unit_target &= ~BL_PC;
	if (skill_db[i].unit_flag&UF_NOMOB)
		skill_db[i].unit_target &= ~BL_MOB;
	if (skill_db[i].unit_flag&UF_SKILL)
		skill_db[i].unit_target |= BL_SKILL;

	return true;
}

static bool skill_parse_row_producedb(char* split[], int columns, int current)
{// ProduceItemID,ItemLV,RequireSkill,RequireSkillLv,MaterialID1,MaterialAmount1,......
	int x,y;

	int i = atoi(split[0]);
	if( !i )
		return false;

	skill_produce_db[current].nameid = i;
	skill_produce_db[current].itemlv = atoi(split[1]);
	skill_produce_db[current].req_skill = atoi(split[2]);
	skill_produce_db[current].req_skill_lv = atoi(split[3]);

	for( x = 4, y = 0; x+1 < columns && split[x] && split[x+1] && y < MAX_PRODUCE_RESOURCE; x += 2, y++ )
	{
		skill_produce_db[current].mat_id[y] = atoi(split[x]);
		skill_produce_db[current].mat_amount[y] = atoi(split[x+1]);
	}

	return true;
}

static bool skill_parse_row_createarrowdb(char* split[], int columns, int current)
{// SourceID,MakeID1,MakeAmount1,...,MakeID5,MakeAmount5
	int x,y;

	int i = atoi(split[0]);
	if( !i )
		return false;

	skill_arrow_db[current].nameid = i;

	for( x = 1, y = 0; x+1 < columns && split[x] && split[x+1] && y < MAX_ARROW_RESOURCE; x += 2, y++ )
	{
		skill_arrow_db[current].cre_id[y] = atoi(split[x]);
		skill_arrow_db[current].cre_amount[y] = atoi(split[x+1]);
	}

	return true;
}

static bool skill_parse_row_abradb(char* split[], int columns, int current)
{// SkillID,DummyName,RequiredHocusPocusLevel,Rate
	int i = atoi(split[0]);
	if( !skill_get_index(i) || !skill_get_max(i) )
	{
		ShowError("abra_db: Invalid skill ID %d\n", i);
		return false;
	}
	if ( !skill_get_inf(i) )
	{
		ShowError("abra_db: Passive skills cannot be casted (%d/%s)\n", i, skill_get_name(i));
		return false;
	}

	skill_abra_db[current].skillid = i;
	skill_abra_db[current].req_lv = atoi(split[2]);
	skill_abra_db[current].per = atoi(split[3]);

	return true;
}

static void skill_readdb(void)
{
	// init skill db structures
	db_clear(skilldb_name2id);
	memset(skill_db,0,sizeof(skill_db));
	memset(skill_produce_db,0,sizeof(skill_produce_db));
	memset(skill_arrow_db,0,sizeof(skill_arrow_db));
	memset(skill_abra_db,0,sizeof(skill_abra_db));

	// load skill databases
	safestrncpy(skill_db[0].name, "UNKNOWN_SKILL", sizeof(skill_db[0].name));
	safestrncpy(skill_db[0].desc, "Unknown Skill", sizeof(skill_db[0].desc));
	sv_readdb(db_path, "skill_db.txt"          , ',',  17, 17, MAX_SKILL_DB, skill_parse_row_skilldb);
	sv_readdb(db_path, "skill_require_db.txt"  , ',',  32, 32, MAX_SKILL_DB, skill_parse_row_requiredb);
	sv_readdb(db_path, "skill_cast_db.txt"     , ',',   6,  7, MAX_SKILL_DB, skill_parse_row_castdb);
	sv_readdb(db_path, "skill_castnodex_db.txt", ',',   2,  3, MAX_SKILL_DB, skill_parse_row_castnodexdb);
	sv_readdb(db_path, "skill_nocast_db.txt"   , ',',   2,  2, MAX_SKILL_DB, skill_parse_row_nocastdb);
	sv_readdb(db_path, "skill_unit_db.txt"     , ',',   8,  8, MAX_SKILL_DB, skill_parse_row_unitdb);
	skill_init_unit_layout();
	sv_readdb(db_path, "produce_db.txt"        , ',',   4,  4+2*MAX_PRODUCE_RESOURCE, MAX_SKILL_PRODUCE_DB, skill_parse_row_producedb);
	sv_readdb(db_path, "create_arrow_db.txt"   , ',', 1+2,  1+2*MAX_ARROW_RESOURCE, MAX_SKILL_ARROW_DB, skill_parse_row_createarrowdb);
	sv_readdb(db_path, "abra_db.txt"           , ',',   4,  4, MAX_SKILL_ABRA_DB, skill_parse_row_abradb);
	sv_readdb(db_path, "skill_block_db.txt"    , ',',   2,  2, MAX_SKILL_DB, skill_parse_row_blockdb);
}

void skill_reload (void)
{
	skill_readdb();
}

/*==========================================
 *
 *------------------------------------------*/
int do_init_skill (void)
{
	skilldb_name2id = strdb_alloc(DB_OPT_DUP_KEY, 0);
	skill_readdb();

	group_db = idb_alloc(DB_OPT_BASE);
	skillunit_db = idb_alloc(DB_OPT_BASE);
	skill_unit_ers = ers_new(sizeof(struct skill_unit_group));
	skill_timer_ers  = ers_new(sizeof(struct skill_timerskill));

	add_timer_func_list(skill_unit_timer,"skill_unit_timer");
	add_timer_func_list(skill_castend_id,"skill_castend_id");
	add_timer_func_list(skill_castend_pos,"skill_castend_pos");
	add_timer_func_list(skill_timerskill,"skill_timerskill");
	add_timer_func_list(skill_blockpc_end, "skill_blockpc_end");

	add_timer_interval(gettick()+SKILLUNITTIMER_INTERVAL,skill_unit_timer,0,0,SKILLUNITTIMER_INTERVAL);

	return 0;
}

int do_final_skill(void)
{
	db_destroy(skilldb_name2id);
	db_destroy(group_db);
	db_destroy(skillunit_db);
	ers_destroy(skill_unit_ers);
	ers_destroy(skill_timer_ers);
	return 0;
}

#ifdef ADELAYS
// ----------------------------------
// Add at the end of skill.c
// ----------------------------------
int64 adelays_gettick(){
	return gettick();
}
int adelays_skill_name2id(const char * skillName){
	return skill_name2id(skillName);
}
int adelays_skill_get_index(int id){
	return skill_get_index(id);
}
const char * adelays_skill_get_name(int id){
	return skill_get_name(id);
}
#endif