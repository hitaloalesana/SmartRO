// Copyright (c) Athena Dev Teams - Licensed under GNU GPL
// For more information, see LICENCE in the main folder

#ifndef _PC_H_
#define _PC_H_

#include "../common/mmo.h" // JOB_*, MAX_FAME_LIST, struct fame_list, struct mmo_charstatus
#include "../common/timer.h" // INVALID_TIMER
#include "battle.h" // battle_config
#include "buyingstore.h"  // struct s_buyingstore
#include "channel.h" // channel_data
#include "battleground.h" // battleground_queue
#include "itemdb.h" // MAX_ITEMGROUP
#include "map.h" // RC_MAX
#include "script.h" // struct script_reg, struct script_regstr
#include "searchstore.h"  // struct s_search_store_info
#include "status.h" // OPTION_*, struct weapon_atk
#include "unit.h" // unit_stop_attack(), unit_stop_walking()
#include "vending.h" // struct s_vending
#include "mob.h"
#include "log.h"
#include "adelays.h"

#define MAX_PC_BONUS 10
#define MAX_PC_SKILL_REQUIRE 5
#define MAX_PC_FEELHATE 3
#define MAX_AUTOLOOTID 20

struct weapon_data {
	int atkmods[3];
	// all the variables except atkmods get zero'ed in each call of status_calc_pc
	// NOTE: if you want to add a non-zeroed variable, you need to update the memset call
	//  in status_calc_pc as well! All the following are automatically zero'ed. [Skotlex]
	int overrefine;
	int star;
	int ignore_def_ele;
	int ignore_def_race;
	int def_ratio_atk_ele;
	int def_ratio_atk_race;
	int addele[ELE_MAX];
	int addrace[RC_MAX];
	int addrace2[RC2_MAX];
	int addsize[3];

	struct drain_data {
		short rate;
		short per;
		short value;
		unsigned type:1;
	} hp_drain[RC_MAX], sp_drain[RC_MAX];

	struct {
		short class_, rate;
	}	add_dmg[MAX_PC_BONUS];

	struct {
		short flag, rate;
		unsigned char ele;
	} addele2[MAX_PC_BONUS];
};

struct s_autospell {
	short id, lv, rate, card_id, flag;
	bool lock;  // bAutoSpellOnSkill: blocks autospell from triggering again, while being executed
};

struct s_addeffect {
	enum sc_type id;
	short rate, arrow_rate;
	unsigned char flag;
};

struct s_addeffectonskill {
	enum sc_type id;
	short rate, skill;
	unsigned char target;
};

struct s_add_drop { 
	short id, group;
	int race, rate;
};

struct s_autobonus {
	short rate,atk_type;
	unsigned int duration;
	char *bonus_script, *other_script;
	int active;
	unsigned short pos;
};

struct skill_cooldown_entry {
	unsigned short skill_id;
	int timer;
};

struct map_session_data {
	struct block_list bl;
	struct unit_data ud;
	struct view_data vd;
	struct status_data base_status, battle_status;
	struct status_change sc;
	struct regen_data regen;
	struct regen_data_sub sregen, ssregen;
	//NOTE: When deciding to add a flag to state or special_state, take into consideration that state is preserved in
	//status_calc_pc, while special_state is recalculated in each call. [Skotlex]
	struct {
		unsigned int active : 1; //Marks active player (not active is logging in/out, or changing map servers)
		unsigned int menu_or_input : 1;// if a script is waiting for feedback from the player
		unsigned int dead_sit : 2;
		unsigned int lr_flag : 2;
		unsigned int connect_new : 1;
		unsigned int arrow_atk : 1;
		unsigned int gangsterparadise : 1;
		unsigned int rest : 1;
		unsigned int storage_flag : 3; //0: closed, 1: Normal Storage open, 2: guild storage open [Skotlex] 3: Rent Storage [ZephStorage]
		unsigned int snovice_dead_flag : 1; //Explosion spirits on death: 0 off, 1 used.
		unsigned int abra_flag : 1; // Abracadabra bugfix by Aru
		unsigned int gmaster_flag : 1; // is guildmaster? (caches sd->status.name == g->master)
		unsigned int autocast : 1; // Autospell flag [Inkfish]
		unsigned int autotrade : 1;	//By Fantik
		unsigned int reg_dirty : 3; //By Skotlex (marks whether registry variables have been saved or not yet)
		unsigned int showdelay :1;
		unsigned int showcast :1; // [Zephyrus] Display casting time to user
		unsigned int showcastdelay :1; // [Zephyrus] Display casting delay time to user
		unsigned int showexp :1;
		unsigned int showgain :1;
		unsigned int showzeny :1;
		unsigned int noask :1; // [LuzZza]
		unsigned int trading :1; //[Skotlex] is 1 only after a trade has started.
		unsigned int can_tradeack : 1; // client can send a tradeack
		unsigned int deal_locked :2; //1: Clicked on OK. 2: Clicked on TRADE
		unsigned int monster_ignore :1; // for monsters to ignore a character [Valaris] [zzo]
		unsigned int size :2; // for tiny/large types
		unsigned int night :1; //Holds whether or not the player currently has the SI_NIGHT effect on. [Skotlex]
		unsigned int blockedmove :1;
		unsigned int using_fake_npc :1;
		unsigned int rewarp :1; //Signals that a player should warp as soon as he is done loading a map. [Skotlex]
		unsigned int killer : 1;
		unsigned int killable : 1;
		unsigned int doridori : 1;
		unsigned int ignoreAll : 1;
		unsigned int buyingstore : 1;
		unsigned int lesseffect : 1;
		unsigned int vending : 1;
		short packet_filter; // [Zephyrus] Packet Filter
		unsigned int battleinfo : 1;
		unsigned short displaydrop;
		unsigned int pvpmode : 1; // PK Mode [Zephyrus]
		unsigned int noks : 3; // [Zeph Kill Steal Protection]
		unsigned int secure_items : 1; // [Zephyrus] Item Security
		bool changemap, changeregion;
		short pmap; // Previous map on Map Change
		unsigned short autobonus; //flag to indicate if an autobonus is activated. [Inkfish]
		unsigned int warping : 1;//states whether you're in the middle of a warp processing
		unsigned int spb : 1; // @spb / @partybuff
		unsigned int only_walk : 1; // [Zephyrus] Block Skills and Item usage to a player
		unsigned int view_mob_info : 1;
		unsigned int evade_antiwpefilter : 1; // Required sometimes to show the user previous to use the skill
		unsigned int bg_afk : 1; // Moved here to reduce searchs
		unsigned int bg_listen : 1;
/** Keitenai Spam Hack Protection **/
		unsigned K_LOCK_SM_BASH : 1;
		unsigned K_LOCK_SM_MAGNUM : 1;
		unsigned K_LOCK_MG_NAPALMBEAT : 1;
		unsigned K_LOCK_MG_SOULSTRIKE : 1;
		unsigned K_LOCK_MG_COLDBOLT : 1;
		unsigned K_LOCK_MG_FROSTDIVER : 1;
		unsigned K_LOCK_MG_STONECURSE : 1;
		unsigned K_LOCK_MG_FIREBALL : 1;
		unsigned K_LOCK_MG_FIREWALL : 1;
		unsigned K_LOCK_MG_FIREBOLT : 1;
		unsigned K_LOCK_MG_LIGHTNINGBOLT : 1;
		unsigned K_LOCK_MG_THUNDERSTORM : 1;
		unsigned K_LOCK_AL_HEAL : 1;
		unsigned K_LOCK_AL_DECAGI : 1;
		unsigned K_LOCK_AL_CRUCIS : 1;
		unsigned K_LOCK_MC_MAMMONITE : 1;
		unsigned K_LOCK_AC_DOUBLE : 1;
		unsigned K_LOCK_AC_SHOWER : 1;
		unsigned K_LOCK_TF_POISON : 1;
		unsigned K_LOCK_KN_PIERCE : 1;
		unsigned K_LOCK_KN_BRANDISHSPEAR : 1;
		unsigned K_LOCK_KN_SPEARSTAB : 1;
		unsigned K_LOCK_KN_SPEARBOOMERANG : 1;
		unsigned K_LOCK_KN_BOWLINGBASH : 1;
		unsigned K_LOCK_PR_LEXDIVINA : 1;
		unsigned K_LOCK_PR_TURNUNDEAD : 1;
		unsigned K_LOCK_PR_LEXAETERNA : 1;
		unsigned K_LOCK_PR_MAGNUS : 1;
		unsigned K_LOCK_WZ_FIREPILLAR : 1;
		unsigned K_LOCK_WZ_SIGHTRASHER : 1;
		unsigned K_LOCK_WZ_FIREIVY : 1;
		unsigned K_LOCK_WZ_METEOR : 1;
		unsigned K_LOCK_WZ_JUPITEL : 1;
		unsigned K_LOCK_WZ_VERMILION : 1;
		unsigned K_LOCK_WZ_WATERBALL : 1;
		unsigned K_LOCK_WZ_ICEWALL : 1;
		unsigned K_LOCK_WZ_FROSTNOVA : 1;
		unsigned K_LOCK_WZ_STORMGUST : 1;
		unsigned K_LOCK_WZ_EARTHSPIKE : 1;
		unsigned K_LOCK_WZ_HEAVENDRIVE : 1;
		unsigned K_LOCK_WZ_QUAGMIRE : 1;
		unsigned K_LOCK_WZ_ESTIMATION : 1;
		unsigned K_LOCK_BS_HAMMERFALL : 1;
		unsigned K_LOCK_HT_BLITZBEAT : 1;
		unsigned K_LOCK_AS_SONICBLOW : 1;
		unsigned K_LOCK_AS_GRIMTOOTH : 1;
		unsigned K_LOCK_AC_CHARGEARROW : 1;
		unsigned K_LOCK_TF_BACKSLIDING : 1;
		unsigned K_LOCK_MC_CARTREVOLUTION : 1;
		unsigned K_LOCK_AL_HOLYLIGHT : 1;
		unsigned K_LOCK_RG_BACKSTAP : 1;
		unsigned K_LOCK_RG_RAID : 1;
		unsigned K_LOCK_RG_GRAFFITI : 1;
		unsigned K_LOCK_RG_FLAGGRAFFITI : 1;
		unsigned K_LOCK_RG_COMPULSION : 1;
		unsigned K_LOCK_RG_PLAGIARISM : 1;
		unsigned K_LOCK_AM_DEMONSTRATION : 1;
		unsigned K_LOCK_AM_ACIDTERROR : 1;
		unsigned K_LOCK_AM_POTIONPITCHER : 1;
		unsigned K_LOCK_AM_CANNIBALIZE : 1;
		unsigned K_LOCK_AM_SPHEREMINE : 1;
		unsigned K_LOCK_AM_FLAMECONTROL : 1;
		unsigned K_LOCK_AM_DRILLMASTER : 1;
		unsigned K_LOCK_CR_SHLDBOOMRANG : 1;
		unsigned K_LOCK_CR_HOLYCROSS : 1;
		unsigned K_LOCK_CR_GRANDCROSS : 1;
		unsigned K_LOCK_MO_CALLSPIRITS : 1;
		unsigned K_LOCK_MO_ABSORBSPIRITS : 1;
		unsigned K_LOCK_MO_BODYRELOCATION : 1;
		unsigned K_LOCK_MO_INVESTIGATE : 1;
		unsigned K_LOCK_MO_FINGEROFFENSE : 1;
		unsigned K_LOCK_MO_EXPLODESPIRIT : 1;
		unsigned K_LOCK_MO_EXTREMITYFIST : 1;
		unsigned K_LOCK_MO_CHAINCOMBO : 1;
		unsigned K_LOCK_MO_COMBOFINISH : 1;
		unsigned K_LOCK_SA_CASTCANCEL : 1;
		unsigned K_LOCK_SA_SPELLBREAKER : 1;
		unsigned K_LOCK_SA_DISPELL : 1;
		unsigned K_LOCK_SA_ABRACADABRA : 1;
		unsigned K_LOCK_SA_MONOCELL : 1;
		unsigned K_LOCK_SA_CLASSCHANGE : 1;
		unsigned K_LOCK_SA_SUMMONMONSTER : 1;
		unsigned K_LOCK_SA_REVERSEORCISH : 1;
		unsigned K_LOCK_SA_DEATH : 1;
		unsigned K_LOCK_SA_FORTUNE : 1;
		unsigned K_LOCK_SA_TAMINGMONSTER : 1;
		unsigned K_LOCK_SA_QUESTION : 1;
		unsigned K_LOCK_SA_GRAVITY : 1;
		unsigned K_LOCK_SA_LEVELUP : 1;
		unsigned K_LOCK_SA_INSTANTDEATH : 1;
		unsigned K_LOCK_SA_FULLRECOVERY : 1;
		unsigned K_LOCK_SA_COMA : 1;
		unsigned K_LOCK_BD_ADAPTATION : 1;
		unsigned K_LOCK_BD_ENCORE : 1;
		unsigned K_LOCK_BD_LULLABY : 1;
		unsigned K_LOCK_BD_RICHMANKIM : 1;
		unsigned K_LOCK_BA_MUSICALSTRIKE : 1;
		unsigned K_LOCK_BA_DISSONANCE : 1;
		unsigned K_LOCK_BA_FROSTJOKER : 1;
		unsigned K_LOCK_BA_WHISTLE : 1;
		unsigned K_LOCK_BA_ASSASSINCROSS : 1;
		unsigned K_LOCK_BA_POEMBRAGI : 1;
		unsigned K_LOCK_BA_APPLEIDUN : 1;
		unsigned K_LOCK_DC_THROWARROW : 1;
		unsigned K_LOCK_DC_UGLYDANCE : 1;
		unsigned K_LOCK_DC_SCREAM : 1;
		unsigned K_LOCK_DC_HUMMING : 1;
		unsigned K_LOCK_DC_DONTFORGETME : 1;
		unsigned K_LOCK_DC_FORTUNEKISS : 1;
		unsigned K_LOCK_DC_SERVICEFORYOU : 1;
		unsigned K_LOCK_LK_FURY : 1;
		unsigned K_LOCK_HW_MAGICCRASHER : 1;
		unsigned K_LOCK_PA_PRESSURE : 1;
		unsigned K_LOCK_CH_PALMSTRIKE : 1;
		unsigned K_LOCK_CH_TIGERFIST : 1;
		unsigned K_LOCK_CH_CHAINCRUSH : 1;
		unsigned K_LOCK_PF_SOULCHANGE : 1;
		unsigned K_LOCK_PF_SOULBURN : 1;
		unsigned K_LOCK_ASC_BREAKER : 1;
		unsigned K_LOCK_SN_FALCONASSAULT : 1;
		unsigned K_LOCK_SN_SHARPSHOOTING : 1;
		unsigned K_LOCK_CR_ALCHEMY : 1;
		unsigned K_LOCK_CR_SYNTHESIS : 1;
		unsigned K_LOCK_CG_ARROWVULCAN : 1;
		unsigned K_LOCK_CG_MOONLIT : 1;
		unsigned K_LOCK_CG_MARIONETTE : 1;
		unsigned K_LOCK_LK_SPIRALPIERCE : 1;
		unsigned K_LOCK_LK_HEADCRUSH : 1;
		unsigned K_LOCK_LK_JOINTBEAT : 1;
		unsigned K_LOCK_HW_NAPALMVULCAN : 1;
		unsigned K_LOCK_CH_SOULCOLLECT : 1;
		unsigned K_LOCK_PF_MINDBREAKER : 1;
		unsigned K_LOCK_PF_SPIDERWEB : 1;
		unsigned K_LOCK_ASC_METEORASSAULT : 1;
		unsigned K_LOCK_TK_STORMKICK : 1;
		unsigned K_LOCK_TK_DOWNKICK : 1;
		unsigned K_LOCK_TK_TURNKICK : 1;
		unsigned K_LOCK_TK_JUMPKICK : 1;
		unsigned K_LOCK_TK_POWER : 1;
		unsigned K_LOCK_TK_HIGHJUMP : 1;
		unsigned K_LOCK_SL_KAIZEL : 1;
		unsigned K_LOCK_SL_KAAHI : 1;
		unsigned K_LOCK_SL_KAUPE : 1;
		unsigned K_LOCK_SL_KAITE : 1;
		unsigned K_LOCK_SL_KAINA : 1;
		unsigned K_LOCK_SL_STIN : 1;
		unsigned K_LOCK_SL_STUN : 1;
		unsigned K_LOCK_SL_SMA : 1;
		unsigned K_LOCK_SL_SWOO : 1;
		unsigned K_LOCK_SL_SKE : 1;
		unsigned K_LOCK_SL_SKA : 1;
		unsigned K_LOCK_ST_FULLSTRIP : 1;
		unsigned K_LOCK_CR_SLIMPITCHER : 1;
		unsigned K_LOCK_CR_FULLPROTECTION : 1;
		unsigned K_LOCK_PA_SHIELDCHAIN : 1;
		unsigned K_LOCK_HP_MANARECHARGE : 1;
		unsigned K_LOCK_PF_DOUBLECASTING : 1;
		unsigned K_LOCK_HW_GANBANTEIN : 1;
		unsigned K_LOCK_HW_GRAVITATION : 1;
		unsigned K_LOCK_WS_CARTTERMINATION : 1;
		unsigned K_LOCK_CG_HERMODE : 1;
		unsigned K_LOCK_CG_TAROTCARD : 1;
		unsigned K_LOCK_CR_ACIDDEMO : 1;
		unsigned K_LOCK_SL_HIGH : 1;
		unsigned K_LOCK_HT_POWER : 1;
		unsigned K_LOCK_GS_TRIPLEACTION : 1;
		unsigned K_LOCK_GS_BULLSEYE : 1;
		unsigned K_LOCK_GS_MADNESSCANCEL : 1;
		unsigned K_LOCK_GS_INCREASING : 1;
		unsigned K_LOCK_GS_MAGICALBULLET : 1;
		unsigned K_LOCK_GS_CRACKER : 1;
		unsigned K_LOCK_GS_SINGLEACTION : 1;
		unsigned K_LOCK_GS_CHAINACTION : 1;
		unsigned K_LOCK_GS_TRACKING : 1;
		unsigned K_LOCK_GS_DISARM : 1;
		unsigned K_LOCK_GS_PIERCINGSHOT : 1;
		unsigned K_LOCK_GS_RAPIDSHOWER : 1;
		unsigned K_LOCK_GS_DESPERADO : 1;
		unsigned K_LOCK_GS_GATLINGFEVER : 1;
		unsigned K_LOCK_GS_DUST : 1;
		unsigned K_LOCK_GS_FULLBUSTER : 1;
		unsigned K_LOCK_GS_SPREADATTACK : 1;
		unsigned K_LOCK_GS_GROUNDDRIFT : 1;
		unsigned K_LOCK_NJ_TOBIDOUGU : 1;
		unsigned K_LOCK_NJ_SYURIKEN : 1;
		unsigned K_LOCK_NJ_KUNAI : 1;
		unsigned K_LOCK_NJ_HUUMA : 1;
		unsigned K_LOCK_NJ_ZENYNAGE : 1;
		unsigned K_LOCK_NJ_TATAMIGAESHI : 1;
		unsigned K_LOCK_NJ_KASUMIKIRI : 1;
		unsigned K_LOCK_NJ_SHADOWJUMP : 1;
		unsigned K_LOCK_NJ_KIRIKAGE : 1;
		unsigned K_LOCK_NJ_UTSUSEMI : 1;
		unsigned K_LOCK_NJ_BUNSINJYUTSU : 1;
		unsigned K_LOCK_NJ_NINPOU : 1;
		unsigned K_LOCK_NJ_KOUENKA : 1;
		unsigned K_LOCK_NJ_KAENSIN : 1;
		unsigned K_LOCK_NJ_BAKUENRYU : 1;
		unsigned K_LOCK_NJ_HYOUSENSOU : 1;
		unsigned K_LOCK_NJ_SUITON : 1;
		unsigned K_LOCK_NJ_HYOUSYOURAKU : 1;
		unsigned K_LOCK_NJ_HUUJIN : 1;
		unsigned K_LOCK_NJ_RAIGEKISAI : 1;
		unsigned K_LOCK_NJ_KAMAITACHI : 1;
		unsigned K_LOCK_NJ_NEN : 1;
		unsigned K_LOCK_NJ_ISSEN : 1;
		unsigned K_LOCK_KN_CHARGEATK : 1;
		unsigned K_LOCK_AS_VENOMKNIFE : 1;
		unsigned K_LOCK_RG_CLOSECONFINE : 1;
		unsigned K_LOCK_WZ_SIGHTBLASTER : 1;
		unsigned K_LOCK_HT_PHANTASMIC : 1;
		unsigned K_LOCK_BA_PANGVOICE : 1;
		unsigned K_LOCK_DC_WINKCHARM : 1;
		unsigned K_LOCK_PR_REDEMPTIO : 1;
		unsigned K_LOCK_MO_KITRANSLATION : 1;
		unsigned K_LOCK_MO_BALKYOUNG : 1;
		unsigned K_LOCK_RK_SONICWAVE : 1;
		unsigned K_LOCK_RK_DEATHBOUND : 1;
		unsigned K_LOCK_RK_HUNDREDSPEAR : 1;
		unsigned K_LOCK_RK_WINDCUTTER : 1;
		unsigned K_LOCK_RK_IGNITIONBREAK : 1;
		unsigned K_LOCK_RK_DRAGONBREATH : 1;
		unsigned K_LOCK_RK_CRUSHSTRIKE : 1;
		unsigned K_LOCK_RK_STORMBLAST : 1;
		unsigned K_LOCK_RK_PHANTOMTHRUST : 1;
		unsigned K_LOCK_GC_CROSSIMPACT : 1;
		unsigned K_LOCK_GC_WEAPONCRUSH : 1;
		unsigned K_LOCK_GC_ROLLINGCUTTER : 1;
		unsigned K_LOCK_GC_CROSSSLASHER : 1;
		unsigned K_LOCK_AB_JUDEX : 1;
		unsigned K_LOCK_AB_ADORAMUS : 1;
		unsigned K_LOCK_AB_CHEAL : 1;
		unsigned K_LOCK_AB_EPICLESIS : 1;
		unsigned K_LOCK_AB_PRAEFATIO : 1;
		unsigned K_LOCK_AB_EUCHARISTICA : 1;
		unsigned K_LOCK_AB_RENOVATIO : 1;
		unsigned K_LOCK_AB_HIGHNESSHEAL : 1;
		unsigned K_LOCK_AB_CLEARANCE : 1;
		unsigned K_LOCK_AB_EXPIATIO : 1;
		unsigned K_LOCK_AB_DUPLELIGHT : 1;
		unsigned K_LOCK_AB_DUPLIGHT_MELEE : 1;
		unsigned K_LOCK_AB_DUPLIGHT_MAGIC : 1;
		unsigned K_LOCK_AB_SILENTIUM : 1;
		unsigned K_LOCK_WL_WHITEIMPRISON : 1;
		unsigned K_LOCK_WL_SOULEXPANSION : 1;
		unsigned K_LOCK_WL_FROSTMISTY : 1;
		unsigned K_LOCK_WL_JACKFROST : 1;
		unsigned K_LOCK_WL_MARSHOFABYSS : 1;
		unsigned K_LOCK_WL_RADIUS : 1;
		unsigned K_LOCK_WL_STASIS : 1;
		unsigned K_LOCK_WL_DRAINLIFE : 1;
		unsigned K_LOCK_WL_CRIMSONROCK : 1;
		unsigned K_LOCK_WL_HELLINFERNO : 1;
		unsigned K_LOCK_WL_COMET : 1;
		unsigned K_LOCK_WL_CHAINLIGHTNING : 1;
		unsigned K_LOCK_WL_CHAINLIGHTNING_ : 1;
		unsigned K_LOCK_WL_EARTHSTRAIN : 1;
		unsigned K_LOCK_WL_TETRAVORTEX : 1;
		unsigned K_LOCK_WL_TETRA_FIRE : 1;
		unsigned K_LOCK_WL_TETRA_WATER : 1;
		unsigned K_LOCK_WL_TETRA_WIND : 1;
		unsigned K_LOCK_WL_TETRA_GROUND : 1;
		unsigned K_LOCK_WL_RELEASE : 1;
		unsigned K_LOCK_WL_READING_SB : 1;
		unsigned K_LOCK_WL_FREEZE_SP : 1;
		unsigned K_LOCK_RA_ARROWSTORM : 1;
		unsigned K_LOCK_RA_AIMEDBOLT : 1;
		unsigned K_LOCK_RA_WUGSTRIKE : 1;
		unsigned K_LOCK_RA_WUGBITE : 1;
		unsigned K_LOCK_NC_BOOSTKNUCKLE : 1;
		unsigned K_LOCK_NC_PILEBUNKER : 1;
		unsigned K_LOCK_NC_VULCANARM : 1;
		unsigned K_LOCK_NC_FLAMELAUNCHER : 1;
		unsigned K_LOCK_NC_COLDSLOWER : 1;
		unsigned K_LOCK_NC_ARMSCANNON : 1;
		unsigned K_LOCK_NC_ACCELERATION : 1;
		unsigned K_LOCK_NC_F_SIDESLIDE : 1;
		unsigned K_LOCK_NC_B_SIDESLIDE : 1;
		unsigned K_LOCK_NC_MAINFRAME : 1;
		unsigned K_LOCK_NC_SHAPESHIFT : 1;
		unsigned K_LOCK_NC_INFRAREDSCAN : 1;
		unsigned K_LOCK_NC_ANALYZE : 1;
		unsigned K_LOCK_NC_MAGNETICFIELD : 1;
		unsigned K_LOCK_NC_NEUTRALBARRIER : 1;
		unsigned K_LOCK_NC_STEALTHFIELD : 1;
		unsigned K_LOCK_NC_AXEBOOMERANG : 1;
		unsigned K_LOCK_NC_POWERSWING : 1;
		unsigned K_LOCK_NC_AXETORNADO : 1;
		unsigned K_LOCK_NC_SILVERSNIPER : 1;
		unsigned K_LOCK_NC_MAGICDECOY : 1;
		unsigned K_LOCK_NC_DISJOINT : 1;
		unsigned K_LOCK_SC_FATALMENACE : 1;
		unsigned K_LOCK_SC_AUTOSHADOWSPELL : 1;
		unsigned K_LOCK_SC_TRIANGLESHOT : 1;
		unsigned K_LOCK_SC_INVISIBILITY : 1;
		unsigned K_LOCK_SC_ENERVATION : 1;
		unsigned K_LOCK_SC_GROOMY : 1;
		unsigned K_LOCK_SC_IGNORANCE : 1;
		unsigned K_LOCK_SC_LAZINESS : 1;
		unsigned K_LOCK_SC_UNLUCKY : 1;
		unsigned K_LOCK_SC_WEAKNESS : 1;
		unsigned K_LOCK_SC_STRIPACCESSARY : 1;
		unsigned K_LOCK_SC_MANHOLE : 1;
		unsigned K_LOCK_SC_DIMENSIONDOOR : 1;
		unsigned K_LOCK_SC_CHAOSPANIC : 1;
		unsigned K_LOCK_SC_MAELSTROM : 1;
		unsigned K_LOCK_SC_BLOODYLUST : 1;
		unsigned K_LOCK_SC_FEINTBOMB : 1;
		unsigned K_LOCK_LG_CANNONSPEAR : 1;
		unsigned K_LOCK_LG_BANISHINGPOINT : 1;
		unsigned K_LOCK_LG_TRAMPLE : 1;
		unsigned K_LOCK_LG_PINPOINTATTACK : 1;
		unsigned K_LOCK_LG_RAGEBURST : 1;
		unsigned K_LOCK_LG_EXEEDBREAK : 1;
		unsigned K_LOCK_LG_OVERBRAND : 1;
		unsigned K_LOCK_LG_BANDING : 1;
		unsigned K_LOCK_LG_MOONSLASHER : 1;
		unsigned K_LOCK_LG_RAYOFGENESIS : 1;
		unsigned K_LOCK_LG_PIETY : 1;
		unsigned K_LOCK_LG_EARTHDRIVE : 1;
		unsigned K_LOCK_LG_HESPERUSLIT : 1;
		unsigned K_LOCK_SR_DRAGONCOMBO : 1;
		unsigned K_LOCK_SR_SKYNETBLOW : 1;
		unsigned K_LOCK_SR_EARTHSHAKER : 1;
		unsigned K_LOCK_SR_FALLENEMPIRE : 1;
		unsigned K_LOCK_SR_TIGERCANNON : 1;
		unsigned K_LOCK_SR_HELLGATE : 1;
		unsigned K_LOCK_SR_RAMPAGEBLASTER : 1;
		unsigned K_LOCK_SR_CRESCENTELBOW : 1;
		unsigned K_LOCK_SR_CURSEDCIRCLE : 1;
		unsigned K_LOCK_SR_LIGHTNINGWALK : 1;
		unsigned K_LOCK_SR_KNUCKLEARROW : 1;
		unsigned K_LOCK_SR_WINDMILL : 1;
		unsigned K_LOCK_SR_RAISINGDRAGON : 1;
		unsigned K_LOCK_SR_GENTLETOUCH : 1;
		unsigned K_LOCK_SR_ASSIMILATE : 1;
		unsigned K_LOCK_SR_POWERVELOCITY : 1;
		unsigned K_LOCK_SR_CRESBOW_AUTO : 1;
		unsigned K_LOCK_SR_GATEOFHELL : 1;
		unsigned K_LOCK_SR_GENTLE_QUIET : 1;
		unsigned K_LOCK_SR_GENTLE_CURE : 1;
		unsigned K_LOCK_SR_GENTLE_EGAIN : 1;
		unsigned K_LOCK_SR_GENTLE_CHANGE : 1;
		unsigned K_LOCK_SR_GENTLE_REVIT : 1;
		unsigned K_LOCK_WA_SWING_DANCE : 1;
		unsigned K_LOCK_WA_SYMPHONY : 1;
		unsigned K_LOCK_WA_MOONLIT : 1;
		unsigned K_LOCK_MI_RUSH_WINDMILL : 1;
		unsigned K_LOCK_MI_ECHOSONG : 1;
		unsigned K_LOCK_MI_HARMONIZE : 1;
		unsigned K_LOCK_WM_LESSON : 1;
		unsigned K_LOCK_WM_METALICSOUND : 1;
		unsigned K_LOCK_WM_REVERB : 1;
		unsigned K_LOCK_WM_REVERB_MELEE : 1;
		unsigned K_LOCK_WM_REVERB_MAGIC : 1;
		unsigned K_LOCK_WM_DOMINION : 1;
		unsigned K_LOCK_WM_RAINSTORM : 1;
		unsigned K_LOCK_WM_RAINSTORM_ : 1;
		unsigned K_LOCK_WM_POEM : 1;
		unsigned K_LOCK_WM_VOICEOFSIREN : 1;
		unsigned K_LOCK_WM_DEADHILLHERE : 1;
		unsigned K_LOCK_WM_DEEPSLEEP : 1;
		unsigned K_LOCK_WM_SIRCLEOFNATURE : 1;
		unsigned K_LOCK_WM_RANDOMIZESPELL : 1;
		unsigned K_LOCK_WM_GLOOMYDAY : 1;
		unsigned K_LOCK_WM_GREAT_ECHO : 1;
		unsigned K_LOCK_WM_SONG_OF_MANA : 1;
		unsigned K_LOCK_WM_DANCE_WITH_WUG : 1;
		unsigned K_LOCK_WM_DESTRUCTION : 1;
		unsigned K_LOCK_WM_SATNIGHT_FEVER : 1;
		unsigned K_LOCK_WM_LERADS_DEW : 1;
		unsigned K_LOCK_WM_MELODYOFSINK : 1;
		unsigned K_LOCK_WM_WARCRY : 1;
		unsigned K_LOCK_WM_HUMMING_VOICE : 1;
		unsigned K_LOCK_SO_FIREWALK : 1;
		unsigned K_LOCK_SO_ELECTRICWALK : 1;
		unsigned K_LOCK_SO_SPELLFIST : 1;
		unsigned K_LOCK_SO_EARTHGRAVE : 1;
		unsigned K_LOCK_SO_DIAMONDDUST : 1;
		unsigned K_LOCK_SO_POISON_BUSTER : 1;
		unsigned K_LOCK_SO_PSYCHIC_WAVE : 1;
		unsigned K_LOCK_SO_CLOUD_KILL : 1;
		unsigned K_LOCK_SO_STRIKING : 1;
		unsigned K_LOCK_SO_WARMER : 1;
		unsigned K_LOCK_SO_VACUUM_EXTREME : 1;
		unsigned K_LOCK_SO_VARETYR_SPEAR : 1;
		unsigned K_LOCK_SO_ARRULLO : 1;
		unsigned K_LOCK_SO_EL_CONTROL : 1;
		unsigned K_LOCK_SO_EL_ACTION : 1;
		unsigned K_LOCK_SO_EL_ANALYSIS : 1;
		unsigned K_LOCK_SO_EL_SYMPATHY : 1;
		unsigned K_LOCK_SO_EL_CURE : 1;
		unsigned K_LOCK_GN_CART_TORNADO : 1;
		unsigned K_LOCK_GN_CARTCANNON : 1;
		unsigned K_LOCK_GN_THORNS_TRAP : 1;
		unsigned K_LOCK_GN_BLOOD_SUCKER : 1;
		unsigned K_LOCK_GN_SPORE_EXPLO : 1;
		unsigned K_LOCK_GN_WALLOFTHORN : 1;
		unsigned K_LOCK_GN_CRAZYWEED : 1;
		unsigned K_LOCK_GN_CRAZYWEED_ : 1;
		unsigned K_LOCK_GN_DEMONIC_FIRE : 1;
		unsigned K_LOCK_GN_FIRE_EXPAN : 1;
		unsigned K_LOCK_GN_FIRE_EXSMOKE : 1;
		unsigned K_LOCK_GN_FIRE_EXTEAR : 1;
		unsigned K_LOCK_GN_FIRE_EXACID : 1;
		unsigned K_LOCK_GN_HELLS_PLANT : 1;
		unsigned K_LOCK_GN_HELLS_PLANT_ : 1;
		unsigned K_LOCK_GN_MANDRAGORA : 1;
		unsigned K_LOCK_GN_SLINGITEM : 1;
		unsigned K_LOCK_GN_SLINGITEM_ : 1;
		unsigned K_LOCK_GN_CHANGEMATERIAL : 1;
		unsigned K_LOCK_AB_SECRAMENT : 1;
		unsigned K_LOCK_SR_HOWLINGOFLION : 1;
		unsigned K_LOCK_SR_RIDELIGHTNING : 1;
		unsigned K_LOCK_LG_OVER_BRANDISH : 1;
		unsigned K_LOCK_LG_OVER_PLUSATK : 1;
		unsigned K_LOCK_RL_GLITTERGREED : 1;
		unsigned K_LOCK_RL_GLITTERGREED_ : 1;
		unsigned K_LOCK_RL_RICHS_COIN : 1;
		unsigned K_LOCK_RL_MASS_SPIRAL : 1;
		unsigned K_LOCK_RL_BANISHING : 1;
		unsigned K_LOCK_RL_B_TRAP : 1;
		unsigned K_LOCK_RL_S_STORM : 1;
		unsigned K_LOCK_RL_E_CHAIN : 1;
		unsigned K_LOCK_RL_QD_SHOT : 1;
		unsigned K_LOCK_RL_C_MARKER : 1;
		unsigned K_LOCK_RL_FIREDANCE : 1;
		unsigned K_LOCK_RL_H_MINE : 1;
		unsigned K_LOCK_RL_P_ALTER : 1;
		unsigned K_LOCK_RL_FALLEN_ANGEL : 1;
		unsigned K_LOCK_RL_R_TRIP : 1;
		unsigned K_LOCK_RL_R_TRIP_ : 1;
		unsigned K_LOCK_RL_D_TAIL : 1;
		unsigned K_LOCK_RL_FIRE_RAIN : 1;
		unsigned K_LOCK_RL_HEAT_BARREL : 1;
		unsigned K_LOCK_RL_AM_BLAST : 1;
		unsigned K_LOCK_RL_SLUGSHOT : 1;
		unsigned K_LOCK_RL_HAMMER_OF_GOD : 1;
		unsigned K_LOCK_RL_B_FLICKER_ATK : 1;
		unsigned K_LOCK_KO_YAMIKUMO : 1;
		unsigned K_LOCK_KO_JYUMONJIKIRI : 1;
		unsigned K_LOCK_KO_SETSUDAN : 1;
		unsigned K_LOCK_KO_BAKURETSU : 1;
		unsigned K_LOCK_KO_HAPPOKUNAI : 1;
		unsigned K_LOCK_KO_MUCHANAGE : 1;
		unsigned K_LOCK_KO_HUUMARANKA : 1;
		unsigned K_LOCK_KO_MAKIBISHI : 1;
		unsigned K_LOCK_KO_MEIKYOUSISUI : 1;
		unsigned K_LOCK_KO_ZANZOU : 1;
		unsigned K_LOCK_KO_KYOUGAKU : 1;
		unsigned K_LOCK_KO_JYUSATSU : 1;
		unsigned K_LOCK_KO_KAHU_ENTEN : 1;
		unsigned K_LOCK_KO_HYOUHU_HUBUKI : 1;
		unsigned K_LOCK_KO_KAZEHU_SEIRAN : 1;
		unsigned K_LOCK_KO_DOHU_KOUKAI : 1;
		unsigned K_LOCK_KO_KAIHOU : 1;
		unsigned K_LOCK_KO_ZENKAI : 1;
		unsigned K_LOCK_KO_GENWAKU : 1;
		unsigned K_LOCK_KO_IZAYOI : 1;
		unsigned K_LOCK_KG_KAGEHUMI : 1;
		unsigned K_LOCK_KG_KYOMU : 1;
		unsigned K_LOCK_KG_KAGEMUSYA : 1;
		unsigned K_LOCK_OB_ZANGETSU : 1;
		unsigned K_LOCK_OB_OBOROGENSOU : 1;
		unsigned K_LOCK_OB_OBOROGENSOU_ : 1;
		unsigned K_LOCK_OB_AKAITSUKI : 1;
		unsigned K_LOCK_GC_DARKCROW : 1;
		unsigned K_LOCK_RA_UNLIMIT : 1;
		unsigned K_LOCK_GN_ILLUSIONDOP : 1;
		unsigned K_LOCK_RK_DRAGONBREATH_W : 1;
		unsigned K_LOCK_RK_LUXANIMA : 1;
		unsigned K_LOCK_NC_MAGMA_ERUPTION : 1;
		unsigned K_LOCK_WM_FRIGG_SONG : 1;
		unsigned K_LOCK_SO_ELESHIELD : 1;
		unsigned K_LOCK_SR_FLASHCOMBO : 1;
		unsigned K_LOCK_SC_ESCAPE : 1;
		unsigned K_LOCK_AB_OFFERTORIUM : 1;
		unsigned K_LOCK_WL_TELEK_INTENSE : 1;
		unsigned K_LOCK_ALL_FULL_THROTTLE : 1;
		unsigned K_LOCK_SU_BITE : 1;
		unsigned K_LOCK_SU_SCRATCH : 1;
		unsigned K_LOCK_SU_STOOP : 1;
		unsigned K_LOCK_SU_LOPE : 1;
		unsigned K_LOCK_SU_SPRITEMABLE : 1;
		unsigned K_LOCK_SU_POWEROFLAND : 1;
		unsigned K_LOCK_SU_SV_STEMSPEAR : 1;
		unsigned K_LOCK_SU_CN_POWDERING : 1;
		unsigned K_LOCK_SU_CN_METEOR : 1;
		unsigned K_LOCK_SU_SV_ROOTTWIST : 1;
		unsigned K_LOCK_SU_SV_ROOTTWIST_ : 1;
		unsigned K_LOCK_SU_POWEROFLIFE : 1;
		unsigned K_LOCK_SU_SCAROFTAROU : 1;
		unsigned K_LOCK_SU_PICKYPECK : 1;
		unsigned K_LOCK_SU_PICKYPECK_ : 1;
		unsigned K_LOCK_SU_ARCLOUSEDASH : 1;
		unsigned K_LOCK_SU_CARROTBEAT : 1;
		unsigned K_LOCK_SU_POWEROFSEA : 1;
		unsigned K_LOCK_SU_TUNABELLY : 1;
		unsigned K_LOCK_SU_TUNAPARTY : 1;
		unsigned K_LOCK_SU_BUNCHOFSHRIMP : 1;
		unsigned K_LOCK_SU_FRESHSHRIMP : 1;

	} state;

// Adelays in map_session_data
#ifdef ADELAYS
	struct Adelays_State adelays_state;
#endif
// Adelays End.

	struct {
		unsigned char no_weapon_damage, no_magic_damage, no_misc_damage;
		unsigned int restart_full_recover : 1;
		unsigned int no_castcancel : 1;
		unsigned int no_castcancel2 : 1;
		unsigned int no_sizefix : 1;
		unsigned int no_gemstone : 1;
		unsigned int intravision : 1; // Maya Purple Card effect [DracoRPG]
		unsigned int perfect_hiding : 1; // [Valaris]
		unsigned int no_knockback : 1;
		unsigned int bonus_coma : 1;
	} special_state;
	struct {
		unsigned short rate;
		int nameid[MAX_AUTOLOOTID];
	} aloot;

	int login_id1, login_id2;
	unsigned short class_;	//This is the internal job ID used by the map server to simplify comparisons/queries/etc. [Skotlex]
	int gmlevel;

	int packet_ver;  // 5: old, 6: 7july04, 7: 13july04, 8: 26july04, 9: 9aug04/16aug04/17aug04, 10: 6sept04, 11: 21sept04, 12: 18oct04, 13: 25oct04 ... 18
	struct mmo_charstatus status;
	struct registry save_reg;
	
	struct item_data* inventory_data[MAX_INVENTORY]; // direct pointers to itemdb entries (faster than doing item_id lookups)
	short equip_index[14];
	unsigned int weight,max_weight;
	int cart_weight,cart_num;
	int fd;
	unsigned short mapindex;
	unsigned char head_dir; //0: Look forward. 1: Look right, 2: Look left.
	unsigned int client_tick;
	int npc_id,areanpc_id,npc_shopid,touching_id;
	int npc_item_flag; //Marks the npc_id with which you can use items during interactions with said npc (see script command enable_itemuse)
	int npc_menu; // internal variable, used in npc menu handling
	int npc_amount;
	struct script_state *st;
	char npc_str[CHATBOX_SIZE]; // for passing npc input box text to script engine
	int npc_timer_id; //For player attached npc timers. [Skotlex]
	unsigned int chatID;
	time_t idletime;
	unsigned int keyboard_action_tick;
	unsigned int mouse_action_tick;
	time_t idlepvp; // [Zephyrus] Ultimo Tick de da�o PVP

	struct{
		int npc_id;
		unsigned int timeout;
	} progressbar; //Progress Bar [Inkfish]

	struct{
		char name[NAME_LENGTH];
	} ignore[MAX_IGNORE_LIST];

	int followtimer; // [MouseJstr]
	int followtarget;

	time_t emotionlasttime; // to limit flood with emotion packets

	short skillitem,skillitemlv;
	short skillid_old,skilllv_old;
	short skillid_dance,skilllv_dance;
	short cook_mastery; // range: [0,1999] [Inkfish]
	struct skill_cooldown_entry *scd[MAX_SKILLCOOLDOWN]; // Skill Cooldown
	int cloneskill_id;
	int menuskill_id, menuskill_val;

	int invincible_timer;
	unsigned int canlog_tick;
	unsigned int canescape_tick;
	unsigned int canuseitem_tick;	// [Skotlex]
	unsigned int canusecashfood_tick;
	unsigned int canequip_tick;	// [Inkfish]
	unsigned int cantalk_tick;
	unsigned int channel_cantalk_tick; // [Channel Flood Protection]
	unsigned int canjoinchn_tick;
	unsigned int cansendmail_tick; // [Mail System Flood Protection]
	unsigned int ks_floodprotect_tick; // [Kill Steal Protection]
/** Keitenai Spam Hack Protection **/
		unsigned int K_CHK_SM_BASH;
		unsigned int K_CHK_SM_MAGNUM;
		unsigned int K_CHK_MG_NAPALMBEAT;
		unsigned int K_CHK_MG_SOULSTRIKE;
		unsigned int K_CHK_MG_COLDBOLT;
		unsigned int K_CHK_MG_FROSTDIVER;
		unsigned int K_CHK_MG_STONECURSE;
		unsigned int K_CHK_MG_FIREBALL;
		unsigned int K_CHK_MG_FIREWALL;
		unsigned int K_CHK_MG_FIREBOLT;
		unsigned int K_CHK_MG_LIGHTNINGBOLT;
		unsigned int K_CHK_MG_THUNDERSTORM;
		unsigned int K_CHK_AL_HEAL;
		unsigned int K_CHK_AL_DECAGI;
		unsigned int K_CHK_AL_CRUCIS;
		unsigned int K_CHK_MC_MAMMONITE;
		unsigned int K_CHK_AC_DOUBLE;
		unsigned int K_CHK_AC_SHOWER;
		unsigned int K_CHK_TF_POISON;
		unsigned int K_CHK_KN_PIERCE;
		unsigned int K_CHK_KN_BRANDISHSPEAR;
		unsigned int K_CHK_KN_SPEARSTAB;
		unsigned int K_CHK_KN_SPEARBOOMERANG;
		unsigned int K_CHK_KN_BOWLINGBASH;
		unsigned int K_CHK_PR_LEXDIVINA;
		unsigned int K_CHK_PR_TURNUNDEAD;
		unsigned int K_CHK_PR_LEXAETERNA;
		unsigned int K_CHK_PR_MAGNUS;
		unsigned int K_CHK_WZ_FIREPILLAR;
		unsigned int K_CHK_WZ_SIGHTRASHER;
		unsigned int K_CHK_WZ_FIREIVY;
		unsigned int K_CHK_WZ_METEOR;
		unsigned int K_CHK_WZ_JUPITEL;
		unsigned int K_CHK_WZ_VERMILION;
		unsigned int K_CHK_WZ_WATERBALL;
		unsigned int K_CHK_WZ_ICEWALL;
		unsigned int K_CHK_WZ_FROSTNOVA;
		unsigned int K_CHK_WZ_STORMGUST;
		unsigned int K_CHK_WZ_EARTHSPIKE;
		unsigned int K_CHK_WZ_HEAVENDRIVE;
		unsigned int K_CHK_WZ_QUAGMIRE;
		unsigned int K_CHK_WZ_ESTIMATION;
		unsigned int K_CHK_BS_HAMMERFALL;
		unsigned int K_CHK_HT_BLITZBEAT;
		unsigned int K_CHK_AS_SONICBLOW;
		unsigned int K_CHK_AS_GRIMTOOTH;
		unsigned int K_CHK_AC_CHARGEARROW;
		unsigned int K_CHK_TF_BACKSLIDING;
		unsigned int K_CHK_MC_CARTREVOLUTION;
		unsigned int K_CHK_AL_HOLYLIGHT;
		unsigned int K_CHK_RG_BACKSTAP;
		unsigned int K_CHK_RG_RAID;
		unsigned int K_CHK_RG_GRAFFITI;
		unsigned int K_CHK_RG_FLAGGRAFFITI;
		unsigned int K_CHK_RG_COMPULSION;
		unsigned int K_CHK_RG_PLAGIARISM;
		unsigned int K_CHK_AM_DEMONSTRATION;
		unsigned int K_CHK_AM_ACIDTERROR;
		unsigned int K_CHK_AM_POTIONPITCHER;
		unsigned int K_CHK_AM_CANNIBALIZE;
		unsigned int K_CHK_AM_SPHEREMINE;
		unsigned int K_CHK_AM_FLAMECONTROL;
		unsigned int K_CHK_AM_DRILLMASTER;
		unsigned int K_CHK_CR_SHLDBOOMRANG;
		unsigned int K_CHK_CR_HOLYCROSS;
		unsigned int K_CHK_CR_GRANDCROSS;
		unsigned int K_CHK_MO_CALLSPIRITS;
		unsigned int K_CHK_MO_ABSORBSPIRITS;
		unsigned int K_CHK_MO_BODYRELOCATION;
		unsigned int K_CHK_MO_INVESTIGATE;
		unsigned int K_CHK_MO_FINGEROFFENSIVE;
		unsigned int K_CHK_MO_EXPLOSIONSPIRITS;
		unsigned int K_CHK_MO_EXTREMITYFIST;
		unsigned int K_CHK_MO_CHAINCOMBO;
		unsigned int K_CHK_MO_COMBOFINISH;
		unsigned int K_CHK_SA_CASTCANCEL;
		unsigned int K_CHK_SA_SPELLBREAKER;
		unsigned int K_CHK_SA_DISPELL;
		unsigned int K_CHK_SA_ABRACADABRA;
		unsigned int K_CHK_SA_MONOCELL;
		unsigned int K_CHK_SA_CLASSCHANGE;
		unsigned int K_CHK_SA_SUMMONMONSTER;
		unsigned int K_CHK_SA_REVERSEORCISH;
		unsigned int K_CHK_SA_DEATH;
		unsigned int K_CHK_SA_FORTUNE;
		unsigned int K_CHK_SA_TAMINGMONSTER;
		unsigned int K_CHK_SA_QUESTION;
		unsigned int K_CHK_SA_GRAVITY;
		unsigned int K_CHK_SA_LEVELUP;
		unsigned int K_CHK_SA_INSTANTDEATH;
		unsigned int K_CHK_SA_FULLRECOVERY;
		unsigned int K_CHK_SA_COMA;
		unsigned int K_CHK_BD_ADAPTATION;
		unsigned int K_CHK_BD_ENCORE;
		unsigned int K_CHK_BD_LULLABY;
		unsigned int K_CHK_BD_RICHMANKIM;
		unsigned int K_CHK_BA_MUSICALSTRIKE;
		unsigned int K_CHK_BA_DISSONANCE;
		unsigned int K_CHK_BA_FROSTJOKER;
		unsigned int K_CHK_BA_WHISTLE;
		unsigned int K_CHK_BA_ASSASSINCROSS;
		unsigned int K_CHK_BA_POEMBRAGI;
		unsigned int K_CHK_BA_APPLEIDUN;
		unsigned int K_CHK_DC_THROWARROW;
		unsigned int K_CHK_DC_UGLYDANCE;
		unsigned int K_CHK_DC_SCREAM;
		unsigned int K_CHK_DC_HUMMING;
		unsigned int K_CHK_DC_DONTFORGETME;
		unsigned int K_CHK_DC_FORTUNEKISS;
		unsigned int K_CHK_DC_SERVICEFORYOU;
		unsigned int K_CHK_LK_FURY;
		unsigned int K_CHK_HW_MAGICCRASHER;
		unsigned int K_CHK_PA_PRESSURE;
		unsigned int K_CHK_CH_PALMSTRIKE;
		unsigned int K_CHK_CH_TIGERFIST;
		unsigned int K_CHK_CH_CHAINCRUSH;
		unsigned int K_CHK_PF_SOULCHANGE;
		unsigned int K_CHK_PF_SOULBURN;
		unsigned int K_CHK_ASC_BREAKER;
		unsigned int K_CHK_SN_FALCONASSAULT;
		unsigned int K_CHK_SN_SHARPSHOOTING;
		unsigned int K_CHK_CR_ALCHEMY;
		unsigned int K_CHK_CR_SYNTHESIS;
		unsigned int K_CHK_CG_ARROWVULCAN;
		unsigned int K_CHK_CG_MOONLIT;
		unsigned int K_CHK_CG_MARIONETTE;
		unsigned int K_CHK_LK_SPIRALPIERCE;
		unsigned int K_CHK_LK_HEADCRUSH;
		unsigned int K_CHK_LK_JOINTBEAT;
		unsigned int K_CHK_HW_NAPALMVULCAN;
		unsigned int K_CHK_CH_SOULCOLLECT;
		unsigned int K_CHK_PF_MINDBREAKER;
		unsigned int K_CHK_PF_SPIDERWEB;
		unsigned int K_CHK_ASC_METEORASSAULT;
		unsigned int K_CHK_TK_STORMKICK;
		unsigned int K_CHK_TK_DOWNKICK;
		unsigned int K_CHK_TK_TURNKICK;
		unsigned int K_CHK_TK_JUMPKICK;
		unsigned int K_CHK_TK_POWER;
		unsigned int K_CHK_TK_HIGHJUMP;
		unsigned int K_CHK_SL_KAIZEL;
		unsigned int K_CHK_SL_KAAHI;
		unsigned int K_CHK_SL_KAUPE;
		unsigned int K_CHK_SL_KAITE;
		unsigned int K_CHK_SL_KAINA;
		unsigned int K_CHK_SL_STIN;
		unsigned int K_CHK_SL_STUN;
		unsigned int K_CHK_SL_SMA;
		unsigned int K_CHK_SL_SWOO;
		unsigned int K_CHK_SL_SKE;
		unsigned int K_CHK_SL_SKA;
		unsigned int K_CHK_ST_FULLSTRIP;
		unsigned int K_CHK_WS_WEAPONREFINE;
		unsigned int K_CHK_CR_SLIMPITCHER;
		unsigned int K_CHK_CR_FULLPROTECTION;
		unsigned int K_CHK_PA_SHIELDCHAIN;
		unsigned int K_CHK_HP_MANARECHARGE;
		unsigned int K_CHK_PF_DOUBLECASTING;
		unsigned int K_CHK_HW_GANBANTEIN;
		unsigned int K_CHK_HW_GRAVITATION;
		unsigned int K_CHK_WS_CARTTERMINATION;
		unsigned int K_CHK_CG_HERMODE;
		unsigned int K_CHK_CG_TAROTCARD;
		unsigned int K_CHK_CR_ACIDDEMONSTRATION;
		unsigned int K_CHK_SL_HIGH;
		unsigned int K_CHK_HT_POWER;
		unsigned int K_CHK_GS_TRIPLEACTION;
		unsigned int K_CHK_GS_BULLSEYE;
		unsigned int K_CHK_GS_MADNESSCANCEL;
		unsigned int K_CHK_GS_INCREASING;
		unsigned int K_CHK_GS_MAGICALBULLET;
		unsigned int K_CHK_GS_CRACKER;
		unsigned int K_CHK_GS_SINGLEACTION;
		unsigned int K_CHK_GS_CHAINACTION;
		unsigned int K_CHK_GS_TRACKING;
		unsigned int K_CHK_GS_DISARM;
		unsigned int K_CHK_GS_PIERCINGSHOT;
		unsigned int K_CHK_GS_RAPIDSHOWER;
		unsigned int K_CHK_GS_DESPERADO;
		unsigned int K_CHK_GS_GATLINGFEVER;
		unsigned int K_CHK_GS_DUST;
		unsigned int K_CHK_GS_FULLBUSTER;
		unsigned int K_CHK_GS_SPREADATTACK;
		unsigned int K_CHK_GS_GROUNDDRIFT;
		unsigned int K_CHK_NJ_TOBIDOUGU;
		unsigned int K_CHK_NJ_SYURIKEN;
		unsigned int K_CHK_NJ_KUNAI;
		unsigned int K_CHK_NJ_HUUMA;
		unsigned int K_CHK_NJ_ZENYNAGE;
		unsigned int K_CHK_NJ_TATAMIGAESHI;
		unsigned int K_CHK_NJ_KASUMIKIRI;
		unsigned int K_CHK_NJ_SHADOWJUMP;
		unsigned int K_CHK_NJ_KIRIKAGE;
		unsigned int K_CHK_NJ_UTSUSEMI;
		unsigned int K_CHK_NJ_BUNSINJYUTSU;
		unsigned int K_CHK_NJ_NINPOU;
		unsigned int K_CHK_NJ_KOUENKA;
		unsigned int K_CHK_NJ_KAENSIN;
		unsigned int K_CHK_NJ_BAKUENRYU;
		unsigned int K_CHK_NJ_HYOUSENSOU;
		unsigned int K_CHK_NJ_SUITON;
		unsigned int K_CHK_NJ_HYOUSYOURAKU;
		unsigned int K_CHK_NJ_HUUJIN;
		unsigned int K_CHK_NJ_RAIGEKISAI;
		unsigned int K_CHK_NJ_KAMAITACHI;
		unsigned int K_CHK_NJ_NEN;
		unsigned int K_CHK_NJ_ISSEN;
		unsigned int K_CHK_KN_CHARGEATK;
		unsigned int K_CHK_AS_VENOMKNIFE;
		unsigned int K_CHK_RG_CLOSECONFINE;
		unsigned int K_CHK_WZ_SIGHTBLASTER;
		unsigned int K_CHK_HT_PHANTASMIC;
		unsigned int K_CHK_BA_PANGVOICE;
		unsigned int K_CHK_DC_WINKCHARM;
		unsigned int K_CHK_PR_REDEMPTIO;
		unsigned int K_CHK_MO_KITRANSLATION;
		unsigned int K_CHK_MO_BALKYOUNG;
		unsigned int K_CHK_RK_SONICWAVE;
		unsigned int K_CHK_RK_DEATHBOUND;
		unsigned int K_CHK_RK_HUNDREDSPEAR;
		unsigned int K_CHK_RK_WINDCUTTER;
		unsigned int K_CHK_RK_IGNITIONBREAK;
		unsigned int K_CHK_RK_DRAGONBREATH;
		unsigned int K_CHK_RK_CRUSHSTRIKE;
		unsigned int K_CHK_RK_STORMBLAST;
		unsigned int K_CHK_RK_PHANTOMTHRUST;
		unsigned int K_CHK_GC_CROSSIMPACT;
		unsigned int K_CHK_GC_WEAPONCRUSH;
		unsigned int K_CHK_GC_ROLLINGCUTTER;
		unsigned int K_CHK_GC_CROSSRIPPERSLASHER;
		unsigned int K_CHK_AB_JUDEX;
		unsigned int K_CHK_AB_ADORAMUS;
		unsigned int K_CHK_AB_CHEAL;
		unsigned int K_CHK_AB_EPICLESIS;
		unsigned int K_CHK_AB_PRAEFATIO;
		unsigned int K_CHK_AB_EUCHARISTICA;
		unsigned int K_CHK_AB_RENOVATIO;
		unsigned int K_CHK_AB_HIGHNESSHEAL;
		unsigned int K_CHK_AB_CLEARANCE;
		unsigned int K_CHK_AB_EXPIATIO;
		unsigned int K_CHK_AB_DUPLELIGHT;
		unsigned int K_CHK_AB_DUPLELIGHT_MELEE;
		unsigned int K_CHK_AB_DUPLELIGHT_MAGIC;
		unsigned int K_CHK_AB_SILENTIUM;
		unsigned int K_CHK_WL_WHITEIMPRISON;
		unsigned int K_CHK_WL_SOULEXPANSION;
		unsigned int K_CHK_WL_FROSTMISTY;
		unsigned int K_CHK_WL_JACKFROST;
		unsigned int K_CHK_WL_MARSHOFABYSS;
		unsigned int K_CHK_WL_RADIUS;
		unsigned int K_CHK_WL_STASIS;
		unsigned int K_CHK_WL_DRAINLIFE;
		unsigned int K_CHK_WL_CRIMSONROCK;
		unsigned int K_CHK_WL_HELLINFERNO;
		unsigned int K_CHK_WL_COMET;
		unsigned int K_CHK_WL_CHAINLIGHTNING;
		unsigned int K_CHK_WL_CHAINLIGHTNING_ATK;
		unsigned int K_CHK_WL_EARTHSTRAIN;
		unsigned int K_CHK_WL_TETRAVORTEX;
		unsigned int K_CHK_WL_TETRAVORTEX_FIRE;
		unsigned int K_CHK_WL_TETRAVORTEX_WATER;
		unsigned int K_CHK_WL_TETRAVORTEX_WIND;
		unsigned int K_CHK_WL_TETRAVORTEX_GROUND;
		unsigned int K_CHK_WL_RELEASE;
		unsigned int K_CHK_WL_READING_SB;
		unsigned int K_CHK_WL_FREEZE_SP;
		unsigned int K_CHK_RA_ARROWSTORM;
		unsigned int K_CHK_RA_AIMEDBOLT;
		unsigned int K_CHK_RA_WUGSTRIKE;
		unsigned int K_CHK_RA_WUGBITE;
		unsigned int K_CHK_NC_BOOSTKNUCKLE;
		unsigned int K_CHK_NC_PILEBUNKER;
		unsigned int K_CHK_NC_VULCANARM;
		unsigned int K_CHK_NC_FLAMELAUNCHER;
		unsigned int K_CHK_NC_COLDSLOWER;
		unsigned int K_CHK_NC_ARMSCANNON;
		unsigned int K_CHK_NC_ACCELERATION;
		unsigned int K_CHK_NC_F_SIDESLIDE;
		unsigned int K_CHK_NC_B_SIDESLIDE;
		unsigned int K_CHK_NC_MAINFRAME;
		unsigned int K_CHK_NC_SHAPESHIFT;
		unsigned int K_CHK_NC_INFRAREDSCAN;
		unsigned int K_CHK_NC_ANALYZE;
		unsigned int K_CHK_NC_MAGNETICFIELD;
		unsigned int K_CHK_NC_NEUTRALBARRIER;
		unsigned int K_CHK_NC_STEALTHFIELD;
		unsigned int K_CHK_NC_AXEBOOMERANG;
		unsigned int K_CHK_NC_POWERSWING;
		unsigned int K_CHK_NC_AXETORNADO;
		unsigned int K_CHK_NC_SILVERSNIPER;
		unsigned int K_CHK_NC_MAGICDECOY;
		unsigned int K_CHK_NC_DISJOINT;
		unsigned int K_CHK_SC_FATALMENACE;
		unsigned int K_CHK_SC_TRIANGLESHOT;
		unsigned int K_CHK_SC_INVISIBILITY;
		unsigned int K_CHK_SC_ENERVATION;
		unsigned int K_CHK_SC_GROOMY;
		unsigned int K_CHK_SC_IGNORANCE;
		unsigned int K_CHK_SC_LAZINESS;
		unsigned int K_CHK_SC_UNLUCKY;
		unsigned int K_CHK_SC_WEAKNESS;
		unsigned int K_CHK_SC_STRIPACCESSARY;
		unsigned int K_CHK_SC_MANHOLE;
		unsigned int K_CHK_SC_DIMENSIONDOOR;
		unsigned int K_CHK_SC_CHAOSPANIC;
		unsigned int K_CHK_SC_MAELSTROM;
		unsigned int K_CHK_SC_BLOODYLUST;
		unsigned int K_CHK_SC_FEINTBOMB;
		unsigned int K_CHK_LG_CANNONSPEAR;
		unsigned int K_CHK_LG_BANISHINGPOINT;
		unsigned int K_CHK_LG_TRAMPLE;
		unsigned int K_CHK_LG_PINPOINTATTACK;
		unsigned int K_CHK_LG_RAGEBURST;
		unsigned int K_CHK_LG_EXEEDBREAK;
		unsigned int K_CHK_LG_OVERBRAND;
		unsigned int K_CHK_LG_BANDING;
		unsigned int K_CHK_LG_MOONSLASHER;
		unsigned int K_CHK_LG_RAYOFGENESIS;
		unsigned int K_CHK_LG_PIETY;
		unsigned int K_CHK_LG_EARTHDRIVE;
		unsigned int K_CHK_LG_HESPERUSLIT;
		unsigned int K_CHK_SR_DRAGONCOMBO;
		unsigned int K_CHK_SR_SKYNETBLOW;
		unsigned int K_CHK_SR_EARTHSHAKER;
		unsigned int K_CHK_SR_FALLENEMPIRE;
		unsigned int K_CHK_SR_TIGERCANNON;
		unsigned int K_CHK_SR_HELLGATE;
		unsigned int K_CHK_SR_RAMPAGEBLASTER;
		unsigned int K_CHK_SR_CRESCENTELBOW;
		unsigned int K_CHK_SR_CURSEDCIRCLE;
		unsigned int K_CHK_SR_LIGHTNINGWALK;
		unsigned int K_CHK_SR_KNUCKLEARROW;
		unsigned int K_CHK_SR_WINDMILL;
		unsigned int K_CHK_SR_RAISINGDRAGON;
		unsigned int K_CHK_SR_GENTLETOUCH;
		unsigned int K_CHK_SR_ASSIMILATE;
		unsigned int K_CHK_SR_POWERVELOCITY;
		unsigned int K_CHK_SR_CRESBOW_AUTO;
		unsigned int K_CHK_SR_GATEOFHELL;
		unsigned int K_CHK_SR_GENTLE_QUIET;
		unsigned int K_CHK_SR_GENTLE_CURE;
		unsigned int K_CHK_SR_GENTLE_EGAIN;
		unsigned int K_CHK_SR_GENTLE_CHANGE;
		unsigned int K_CHK_SR_GENTLE_REVIT;
		unsigned int K_CHK_WA_SWING_DANCE;
		unsigned int K_CHK_WA_SYMPHONY;
		unsigned int K_CHK_WA_MOONLIT;
		unsigned int K_CHK_MI_RUSH_WINDMILL;
		unsigned int K_CHK_MI_ECHOSONG;
		unsigned int K_CHK_MI_HARMONIZE;
		unsigned int K_CHK_WM_LESSON;
		unsigned int K_CHK_WM_METALICSOUND;
		unsigned int K_CHK_WM_REVERB;
		unsigned int K_CHK_WM_REVERB_MELEE;
		unsigned int K_CHK_WM_REVERB_MAGIC;
		unsigned int K_CHK_WM_DOMINION;
		unsigned int K_CHK_WM_RAINSTORM;
		unsigned int K_CHK_WM_RAINSTORM_;
		unsigned int K_CHK_WM_POEM;
		unsigned int K_CHK_WM_VOICEOFSIREN;
		unsigned int K_CHK_WM_DEADHILLHERE;
		unsigned int K_CHK_WM_DEEPSLEEP;
		unsigned int K_CHK_WM_SIRCLEOFNATURE;
		unsigned int K_CHK_WM_RANDOMIZESPELL;
		unsigned int K_CHK_WM_GLOOMYDAY;
		unsigned int K_CHK_WM_GREAT_ECHO;
		unsigned int K_CHK_WM_SONG_OF_MANA;
		unsigned int K_CHK_WM_DANCE_WITH_WUG;
		unsigned int K_CHK_WM_SOUND_OF_DESTRUCTION;
		unsigned int K_CHK_WM_SATNIGHT_FEVER;
		unsigned int K_CHK_WM_LERADS_DEW;
		unsigned int K_CHK_WM_MELODYOFSINK;
		unsigned int K_CHK_WM_WARCRY;
		unsigned int K_CHK_WM_HUMMING_VOICE;
		unsigned int K_CHK_SO_FIREWALK;
		unsigned int K_CHK_SO_ELECTRICWALK;
		unsigned int K_CHK_SO_SPELLFIST;
		unsigned int K_CHK_SO_EARTHGRAVE;
		unsigned int K_CHK_SO_DIAMONDDUST;
		unsigned int K_CHK_SO_POISON_BUSTER;
		unsigned int K_CHK_SO_PSYCHIC_WAVE;
		unsigned int K_CHK_SO_CLOUD_KILL;
		unsigned int K_CHK_SO_STRIKING;
		unsigned int K_CHK_SO_WARMER;
		unsigned int K_CHK_SO_VACUUM_EXTREME;
		unsigned int K_CHK_SO_VARETYR_SPEAR;
		unsigned int K_CHK_SO_ARRULLO;
		unsigned int K_CHK_SO_EL_CONTROL;
		unsigned int K_CHK_SO_EL_ACTION;
		unsigned int K_CHK_SO_EL_ANALYSIS;
		unsigned int K_CHK_SO_EL_SYMPATHY;
		unsigned int K_CHK_SO_EL_CURE;
		unsigned int K_CHK_GN_CART_TORNADO;
		unsigned int K_CHK_GN_CARTCANNON;
		unsigned int K_CHK_GN_THORNS_TRAP;
		unsigned int K_CHK_GN_BLOOD_SUCKER;
		unsigned int K_CHK_GN_SPORE_EXPLO;
		unsigned int K_CHK_GN_WALLOFTHORN;
		unsigned int K_CHK_GN_CRAZYWEED;
		unsigned int K_CHK_GN_CRAZYWEED_;
		unsigned int K_CHK_GN_DEMONIC_FIRE;
		unsigned int K_CHK_GN_FIRE_EXPAN;
		unsigned int K_CHK_GN_FIRE_EXPAN_SMOKE;
		unsigned int K_CHK_GN_FIRE_EXPAN_TEAR;
		unsigned int K_CHK_GN_FIRE_EXPAN_ACID;
		unsigned int K_CHK_GN_HELLS_PLANT;
		unsigned int K_CHK_GN_HELLS_PLANT_;
		unsigned int K_CHK_GN_MANDRAGORA;
		unsigned int K_CHK_GN_SLINGITEM;
		unsigned int K_CHK_GN_CHANGEMATERIAL;
		unsigned int K_CHK_GN_SLING_RANGEMELEE;
		unsigned int K_CHK_AB_SECRAMENT;
		unsigned int K_CHK_SR_HOWLINGOFLION;
		unsigned int K_CHK_SR_RIDEINLIGHTNING;
		unsigned int K_CHK_LG_OVERBRAND_BRANDISH;
		unsigned int K_CHK_LG_OVERBRAND_PLUSATK;
		unsigned int K_CHK_RL_GLITTERING_GREED;
		unsigned int K_CHK_RL_RICHS_COIN;
		unsigned int K_CHK_RL_MASS_SPIRAL;
		unsigned int K_CHK_RL_BANISHING_BUSTER;
		unsigned int K_CHK_RL_B_TRAP;
		unsigned int K_CHK_RL_S_STORM;
		unsigned int K_CHK_RL_E_CHAIN;
		unsigned int K_CHK_RL_QD_SHOT;
		unsigned int K_CHK_RL_C_MARKER;
		unsigned int K_CHK_RL_FIREDANCE;
		unsigned int K_CHK_RL_H_MINE;
		unsigned int K_CHK_RL_P_ALTER;
		unsigned int K_CHK_RL_FALLEN_ANGEL;
		unsigned int K_CHK_RL_R_TRIP;
		unsigned int K_CHK_RL_D_TAIL;
		unsigned int K_CHK_RL_FIRE_RAIN;
		unsigned int K_CHK_RL_HEAT_BARREL;
		unsigned int K_CHK_RL_AM_BLAST;
		unsigned int K_CHK_RL_SLUGSHOT;
		unsigned int K_CHK_RL_HAMMER_OF_GOD;
		unsigned int K_CHK_RL_R_TRIP_PLUSATK;
		unsigned int K_CHK_RL_B_FLICKER_ATK;
		unsigned int K_CHK_RL_GLITTERING_ATK;
		unsigned int K_CHK_KO_YAMIKUMO;
		unsigned int K_CHK_KO_JYUMONJIKIRI;
		unsigned int K_CHK_KO_SETSUDAN;
		unsigned int K_CHK_KO_BAKURETSU;
		unsigned int K_CHK_KO_HAPPOKUNAI;
		unsigned int K_CHK_KO_MUCHANAGE;
		unsigned int K_CHK_KO_HUUMARANKA;
		unsigned int K_CHK_KO_MAKIBISHI;
		unsigned int K_CHK_KO_MEIKYOUSISUI;
		unsigned int K_CHK_KO_ZANZOU;
		unsigned int K_CHK_KO_KYOUGAKU;
		unsigned int K_CHK_KO_JYUSATSU;
		unsigned int K_CHK_KO_KAHU_ENTEN;
		unsigned int K_CHK_KO_HYOUHU_HUBUKI;
		unsigned int K_CHK_KO_KAZEHU_SEIRAN;
		unsigned int K_CHK_KO_DOHU_KOUKAI;
		unsigned int K_CHK_KO_KAIHOU;
		unsigned int K_CHK_KO_ZENKAI;
		unsigned int K_CHK_KO_GENWAKU;
		unsigned int K_CHK_KO_IZAYOI;
		unsigned int K_CHK_KG_KAGEHUMI;
		unsigned int K_CHK_KG_KYOMU;
		unsigned int K_CHK_KG_KAGEMUSYA;
		unsigned int K_CHK_OB_ZANGETSU;
		unsigned int K_CHK_OB_OBOROGENSOU;
		unsigned int K_CHK_OB_OBOROGENSOU_TRANS;
		unsigned int K_CHK_OB_AKAITSUKI;
		unsigned int K_CHK_GC_DARKCROW;
		unsigned int K_CHK_RA_UNLIMIT;
		unsigned int K_CHK_GN_ILLUSIONDOPING;
		unsigned int K_CHK_RK_DRAGONBREATH_W;
		unsigned int K_CHK_RK_LUXANIMA;
		unsigned int K_CHK_NC_MAGMA_ERUPTION;
		unsigned int K_CHK_WM_FRIGG_SONG;
		unsigned int K_CHK_SO_ELESHIELD;
		unsigned int K_CHK_SR_FLASHCOMBO;
		unsigned int K_CHK_SC_ESCAPE;
		unsigned int K_CHK_AB_OFFERTORIUM;
		unsigned int K_CHK_WL_TELEK_INTENSE;
		unsigned int K_CHK_ALL_FULL_THROTTLE;
		unsigned int K_CHK_SU_BITE;
		unsigned int K_CHK_SU_SCRATCH;
		unsigned int K_CHK_SU_STOOP;
		unsigned int K_CHK_SU_LOPE;
		unsigned int K_CHK_SU_SPRITEMABLE;
		unsigned int K_CHK_SU_POWEROFLAND;
		unsigned int K_CHK_SU_SV_STEMSPEAR;
		unsigned int K_CHK_SU_CN_POWDERING;
		unsigned int K_CHK_SU_CN_METEOR;
		unsigned int K_CHK_SU_SV_ROOTTWIST;
		unsigned int K_CHK_SU_SV_ROOTTWIST_ATK;
		unsigned int K_CHK_SU_POWEROFLIFE;
		unsigned int K_CHK_SU_SCAROFTAROU;
		unsigned int K_CHK_SU_PICKYPECK;
		unsigned int K_CHK_SU_PICKYPECK_;
		unsigned int K_CHK_SU_ARCLOUSEDASH;
		unsigned int K_CHK_SU_CARROTBEAT;
		unsigned int K_CHK_SU_POWEROFSEA;
		unsigned int K_CHK_SU_TUNABELLY;
		unsigned int K_CHK_SU_TUNAPARTY;
		unsigned int K_CHK_SU_BUNCHOFSHRIMP;
		unsigned int K_CHK_SU_FRESHSHRIMP;

	struct {
		int nameid;
		unsigned int tick;
	} item_delay[MAX_ITEMDELAYS]; // [Paradox924X]

	// [Flood Protection - Automute]
	unsigned int last_talk_message;
	short message_count;

	short weapontype1,weapontype2;
	short disguise; // [Valaris]

	struct weapon_data right_weapon, left_weapon;
	
	// here start arrays to be globally zeroed at the beginning of status_calc_pc()
	int param_bonus[6],param_equip[6]; //Stores card/equipment bonuses.
	int subele[ELE_MAX];
	int subrace[RC_MAX];
	int subrace2[RC2_MAX];
	int subsize[3];
	int reseff[SC_COMMON_MAX-SC_COMMON_MIN+1];
	int weapon_coma_ele[ELE_MAX];
	int weapon_coma_race[RC_MAX];
	int weapon_atk[16];
	int weapon_atk_rate[16];
	int arrow_addele[ELE_MAX];
	int arrow_addrace[RC_MAX];
	int arrow_addsize[3];
	int magic_addele[ELE_MAX];
	int magic_addrace[RC_MAX];
	int magic_addsize[3];
	int critaddrace[RC_MAX];
	int expaddrace[RC_MAX];
	int ignore_mdef[RC_MAX];
	int ignore_def[RC_MAX];
	int itemgrouphealrate[MAX_ITEMGROUP];
	short sp_gain_race[RC_MAX];
	// zeroed arrays end here.
	// zeroed structures start here
	struct s_autospell autospell[15], autospell2[15], autospell3[15];
	struct s_addeffect addeff[MAX_PC_BONUS], addeff2[MAX_PC_BONUS];
	struct s_addeffectonskill addeff3[MAX_PC_BONUS];

	struct { //skillatk raises bonus dmg% of skills, skillheal increases heal%, skillblown increases bonus blewcount for some skills.
		unsigned short id;
		short val;
	} skillatk[MAX_PC_BONUS], skillheal[5], skillheal2[5], skillblown[MAX_PC_BONUS], skillcast[MAX_PC_BONUS];
	struct {
		short value;
		int rate;
		int tick;
	} hp_loss, sp_loss, hp_regen, sp_regen;
	struct {
		short class_, rate;
	}	add_def[MAX_PC_BONUS], add_mdef[MAX_PC_BONUS], add_mdmg[MAX_PC_BONUS];
	struct s_add_drop add_drop[MAX_PC_BONUS];
	struct {
		int nameid;
		int rate;
	} itemhealrate[MAX_PC_BONUS];
	struct {
		short flag, rate;
		unsigned char ele;
	} subele2[MAX_PC_BONUS];
	// zeroed structures end here
	// manually zeroed structures start here.
	struct s_autobonus autobonus[MAX_PC_BONUS], autobonus2[MAX_PC_BONUS], autobonus3[MAX_PC_BONUS]; //Auto script on attack, when attacked, on skill usage
	// manually zeroed structures end here.
	// zeroed vars start here.
	int atk_rate;
	int arrow_atk,arrow_ele,arrow_cri,arrow_hit;
	int nsshealhp,nsshealsp;
	int critical_def,double_rate;
	int long_attack_atk_rate; //Long range atk rate, not weapon based. [Skotlex]
	int near_attack_def_rate,long_attack_def_rate,magic_def_rate,misc_def_rate;
	int ignore_mdef_ele;
	int ignore_mdef_race;
	int perfect_hit;
	int perfect_hit_add;
	int get_zeny_rate;
	int get_zeny_num; //Added Get Zeny Rate [Skotlex]
	int double_add_rate;
	int short_weapon_damage_return,long_weapon_damage_return;
	int magic_damage_return; // AppleGirl Was Here
	int random_attack_increase_add,random_attack_increase_per; // [Valaris]
	int break_weapon_rate,break_armor_rate;
	int crit_atk_rate;
	int classchange; // [Valaris]
	int speed_rate, speed_add_rate, aspd_add;
	int itemhealrate2; // [Epoque] Increase heal rate of all healing items.
	unsigned int setitem_hash, setitem_hash2; //Split in 2 because shift operations only work on int ranges. [Skotlex]
	
	short splash_range, splash_add_range;
	short add_steal_rate;
	short add_heal_rate, add_heal2_rate;
	short sp_gain_value, hp_gain_value, magic_sp_gain_value, magic_hp_gain_value;
	short sp_vanish_rate;
	short sp_vanish_per;	
	unsigned short unbreakable;	// chance to prevent ANY equipment breaking [celest]
	unsigned short unbreakable_equip; //100% break resistance on certain equipment
	unsigned short unstripable_equip;

	// zeroed vars end here.

	int castrate,delayrate,hprate,sprate,dsprate;
	int hprecov_rate,sprecov_rate;
	int matk_rate;
	int critical_rate,hit_rate,flee_rate,flee2_rate,def_rate,def2_rate,mdef_rate,mdef2_rate;

	int itemid;
	short itemindex;	//Used item's index in sd->inventory [Skotlex]

	short catch_target_class; // pet catching, stores a pet class to catch (short now) [zzo]

	short spiritball, spiritball_old;
	int spirit_timer[MAX_SKILL_LEVEL];

	unsigned char potion_success_counter; //Potion successes in row counter
	unsigned char mission_count; //Stores the bounty kill count for TK_MISSION
	short mission_mobid; //Stores the target mob_id for TK_MISSION
	int die_counter; //Total number of times you've died
	int devotion[5]; //Stores the account IDs of chars devoted to.
	int reg_num; //Number of registries (type numeric)
	int regstr_num; //Number of registries (type string)

	struct script_reg *reg;
	struct script_regstr *regstr;

	int trade_partner;
	struct { 
		struct {
			short index, amount;
		} item[10];
		int zeny, weight;
	} deal;

	bool party_creating; // whether the char is requesting party creation
	bool party_joining; // whether the char is accepting party invitation
	int party_invite, party_invite_account; // for handling party invitation (holds party id and account id)
	int adopt_invite; // Adoption

	int guild_invite,guild_invite_account;
	int guild_emblem_id,guild_alliance,guild_alliance_account;
	short guild_x,guild_y; // For guildmate position display. [Skotlex] should be short [zzo]
	int guildspy; // [Syrus22]
	int partyspy; // [Syrus22]

	int vended_id;
	int vender_id;
	int vend_num;
	char message[MESSAGE_SIZE];
	struct s_vending vending[MAX_VENDING];
	int vend_coin;

	unsigned int buyer_id;  // uid of open buying store
	struct s_buyingstore buyingstore;

	struct s_search_store_info searchstore;

	struct pet_data *pd;
	struct homun_data *hd;	// [blackhole89]
	struct mercenary_data *md;

	int channels;
	struct channel_data *cd[MAX_USER_CHANNELS];
	int channel_invite_timer;

	struct{
		int  m; //-1 - none, other: map index corresponding to map name.
		unsigned short index; //map index
	}feel_map[3];// 0 - Sun; 1 - Moon; 2 - Stars
	short hate_mob[3];

	int pvp_timer;
	short pvp_point;
	unsigned short pvp_rank, pvp_lastusers;
	unsigned short pvp_won, pvp_lost;

	char eventqueue[MAX_EVENTQUEUE][EVENT_NAME_LENGTH];
	int eventtimer[MAX_EVENTTIMER];
	unsigned short eventcount; // [celest]

	unsigned char change_level; // [celest]
	unsigned gm_power : 1; // Flag para comando maspower [Tab]
	int gm_stats[6];

	struct {
		int MVPKiller;
		time_t session_start;
		unsigned int session_base_exp, session_job_exp;
	} custom_data;

	unsigned int pvpevent_fame;

	char fakename[NAME_LENGTH]; // fake names [Valaris]

	int duel_group; // duel vars [LuzZza]
	int duel_invite;

	int killerrid, killedrid;

	char away_message[128]; // [LuzZza]

	int cashPoints, kafraPoints;
	int Premium_Tick;

	int rental_timer;

	// Hunting Missions [Zephyrus]
	int hunting_time;
	struct {
		int mob_id;
		short count;
	} hunting[5];

	// Auction System [Zephyrus]
	struct {
		int index, amount;
	} auction;

	// Mail System [Zephyrus]
	struct {
		short nameid;
		int index, amount, zeny;
		struct mail_data inbox;
		bool changed; // if true, should sync with charserver on next mailbox request
	} mail;

	//Quest log system [Kevin] [Inkfish]
	int num_quests;
	int avail_quests;
	int quest_index[MAX_QUEST_DB];
	struct quest quest_log[MAX_QUEST_DB];
	bool save_quest;

	// Achievement System
	struct s_achievement achievement[ACHIEVEMENT_MAX];
	int achievement_count;
	int achievement_cutin_timer;
	bool save_achievement;

	// Graveyard System
	int graveyard_npc_id;

	// Battleground and Queue System
	unsigned int bg_id;
	struct battleground_data *bmaster_flag;
	unsigned short bg_kills; 
	struct queue_data *qd;
	unsigned short bg_team;

	unsigned short user_font;
	short view_aura, user_aura;

	// Language System
	int lang_id;
	unsigned int lang_mastery;
};


//Total number of classes (for data storage)
#define CLASS_COUNT (JOB_MAX - JOB_NOVICE_HIGH + JOB_MAX_BASIC)

enum weapon_type {
	W_FIST,	//Bare hands
	W_DAGGER,	//1
	W_1HSWORD,	//2
	W_2HSWORD,	//3
	W_1HSPEAR,	//4
	W_2HSPEAR,	//5
	W_1HAXE,	//6
	W_2HAXE,	//7
	W_MACE,	//8
	W_2HMACE,	//9 (unused)
	W_STAFF,	//10
	W_BOW,	//11
	W_KNUCKLE,	//12	
	W_MUSICAL,	//13
	W_WHIP,	//14
	W_BOOK,	//15
	W_KATAR,	//16
	W_REVOLVER,	//17
	W_RIFLE,	//18
	W_GATLING,	//19
	W_SHOTGUN,	//20
	W_GRENADE,	//21
	W_HUUMA,	//22
	W_2HSTAFF,	//23
	MAX_WEAPON_TYPE,
	// dual-wield constants
	W_DOUBLE_DD, // 2 daggers
	W_DOUBLE_SS, // 2 swords
	W_DOUBLE_AA, // 2 axes
	W_DOUBLE_DS, // dagger + sword
	W_DOUBLE_DA, // dagger + axe
	W_DOUBLE_SA, // sword + axe
};

enum ammo_type {
	A_ARROW = 1,
	A_DAGGER,   //2
	A_BULLET,   //3
	A_SHELL,    //4
	A_GRENADE,  //5
	A_SHURIKEN, //6
	A_KUNAI     //7
};

//Equip position constants
enum equip_pos {
	EQP_HEAD_LOW = 0x0001, 
	EQP_HEAD_MID = 0x0200, //512
	EQP_HEAD_TOP = 0x0100, //256
	EQP_HAND_R   = 0x0002,
	EQP_HAND_L   = 0x0020, //32
	EQP_ARMOR    = 0x0010, //16
	EQP_SHOES    = 0x0040, //64
	EQP_GARMENT  = 0x0004,
	EQP_ACC_L    = 0x0008,
	EQP_ACC_R    = 0x0080, //128
	EQP_AMMO     = 0x8000, //32768
	EQP_COS_HEAD_TOP = 0x0400, // 1024
	EQP_COS_HEAD_MID = 0x0800, // 2048
	EQP_COS_HEAD_LOW = 0x1000, // 4096
};

#define EQP_WEAPON EQP_HAND_R
#define EQP_SHIELD EQP_HAND_L
#define EQP_ARMS (EQP_HAND_R|EQP_HAND_L)
#define EQP_HELM (EQP_HEAD_LOW|EQP_HEAD_MID|EQP_HEAD_TOP|EQP_COS_HEAD_TOP|EQP_COS_HEAD_MID|EQP_COS_HEAD_LOW)
#define EQP_ACC (EQP_ACC_L|EQP_ACC_R)

/// Equip positions that use a visible sprite
#if PACKETVER < 20110111
	#define EQP_VISIBLE EQP_HELM
#else
	#define EQP_VISIBLE (EQP_HELM|EQP_GARMENT)
#endif

//Equip indexes constants. (eg: sd->equip_index[EQI_AMMO] returns the index
//where the arrows are equipped)
enum equip_index {
	EQI_ACC_L = 0,
	EQI_ACC_R,
	EQI_SHOES,
	EQI_GARMENT,
	EQI_HEAD_LOW,
	EQI_HEAD_MID,
	EQI_HEAD_TOP,
	EQI_ARMOR,
	EQI_HAND_L,
	EQI_HAND_R,
	EQI_AMMO,
	EQI_MAX_BONUS = 10,
	EQI_COS_HEAD_TOP,
	EQI_COS_MID_TOP,
	EQI_COS_LOW_TOP,
	EQI_MAX
};

extern int global_size;

#define pc_setdead(sd)        ( (sd)->state.dead_sit = (sd)->vd.dead_sit = 1 )
#define pc_setsit(sd)         ( (sd)->state.dead_sit = (sd)->vd.dead_sit = 2 )
#define pc_isdead(sd)         ( (sd)->state.dead_sit == 1 )
#define pc_issit(sd)          ( (sd)->vd.dead_sit == 2 )
#define pc_isidle(sd)         ( (sd)->chatID || (sd)->state.vending || (sd)->state.buyingstore || DIFF_TICK(last_tick, (sd)->idletime) >= battle_config.idle_no_share )
#define pc_istrading(sd)      ( (sd)->npc_id || (sd)->state.vending || (sd)->state.buyingstore || (sd)->state.trading )
#define pc_cant_act(sd)       ( (sd)->npc_id || (sd)->state.vending || (sd)->state.buyingstore || (sd)->chatID || (sd)->sc.opt1 || (sd)->state.trading || (sd)->state.storage_flag )
#define pc_setdir(sd,b,h)     ( (sd)->ud.dir = (b) ,(sd)->head_dir = (h) )
#define pc_setchatid(sd,n)    ( (sd)->chatID = n )
#define pc_ishiding(sd)       ( (sd)->sc.option&(OPTION_HIDE|OPTION_CLOAK|OPTION_CHASEWALK) )
#define pc_iscloaking(sd)     ( !((sd)->sc.option&OPTION_CHASEWALK) && ((sd)->sc.option&OPTION_CLOAK) )
#define pc_ischasewalk(sd)    ( (sd)->sc.option&OPTION_CHASEWALK )
#define pc_iscarton(sd)       ( (sd)->sc.option&OPTION_CART )
#define pc_isfalcon(sd)       ( (sd)->sc.option&OPTION_FALCON )
#define pc_isriding(sd)       ( (sd)->sc.option&OPTION_RIDING )
#define pc_isinvisible(sd)    ( (sd)->sc.option&OPTION_INVISIBLE )
#define pc_is50overweight(sd) ( (sd)->weight*100 >= (sd)->max_weight*battle_config.natural_heal_weight_rate )
#define pc_is90overweight(sd) ( (sd)->weight*10 >= (sd)->max_weight*9 )
#define pc_maxparameter(sd)   ( (sd)->class_&JOBL_BABY ? battle_config.max_baby_parameter : battle_config.max_parameter )

#define pc_stop_walking(sd, type) unit_stop_walking(&(sd)->bl, type)
#define pc_stop_attack(sd) unit_stop_attack(&(sd)->bl)

//Weapon check considering dual wielding.
#define pc_check_weapontype(sd, type) ((type)&((sd)->status.weapon < MAX_WEAPON_TYPE? \
	1<<(sd)->status.weapon:(1<<(sd)->weapontype1)|(1<<(sd)->weapontype2)))
//Checks if the given class value corresponds to a player class. [Skotlex]
#define pcdb_checkid(class_) \
( \
	( (class_) >= JOB_NOVICE      && (class_) <  JOB_MAX_BASIC   ) \
||	( (class_) >= JOB_NOVICE_HIGH && (class_) <  JOB_MAX         ) \
)

//Ancient WoE Check
#define pc_class2ancientwoe(class_) ( (class_ >= JOB_NOVICE && class_ <= JOB_SUPER_NOVICE && class_ != JOB_WEDDING) )
//#define pc_class2ancientwoe(class_) ( (class_ >= JOB_NOVICE && class_ <= JOB_SUPER_NOVICE && class_ != JOB_WEDDING) || (class_ >= JOB_BABY && class_ <= JOB_SUPER_BABY) )
// Note : Switch to allow Baby Class to enter Ancient WoE.

int pc_class2idx(int class_);
int pc_isGM(struct map_session_data *sd);
bool pc_isPremium(struct map_session_data *sd);

int pc_getrefinebonus(int lv,int type);
bool pc_can_give_items(int level);

void pc_onstatuschanged(struct map_session_data* sd, int type);

int pc_setrestartvalue(struct map_session_data *sd,int type);
int pc_makesavestatus(struct map_session_data *);
void pc_respawn(struct map_session_data* sd, clr_type clrtype);
int pc_setnewpc(struct map_session_data*,int,int,int,unsigned int,int,int);
bool pc_authok(struct map_session_data* sd, int, time_t, int gmlevel, struct mmo_charstatus* status);
void pc_authfail(struct map_session_data *);
int pc_reg_received(struct map_session_data *sd);

int pc_isequip(struct map_session_data *sd,int n);
int pc_isequip2(struct map_session_data *sd, int nameid);
int pc_equippoint(struct map_session_data *sd,int n);
int pc_setinventorydata(struct map_session_data *sd);

int pc_checkskill(struct map_session_data *sd,int skill_id);
int pc_checkallowskill(struct map_session_data *sd);
int pc_checkequip(struct map_session_data *sd,int pos);

int pc_calc_skilltree(struct map_session_data *sd);
int pc_calc_skilltree_normalize_job(struct map_session_data *sd);
int pc_clean_skilltree(struct map_session_data *sd);

#define pc_checkoverhp(sd) ((sd)->battle_status.hp == (sd)->battle_status.max_hp)
#define pc_checkoversp(sd) ((sd)->battle_status.sp == (sd)->battle_status.max_sp)

int pc_setpos(struct map_session_data* sd, unsigned short mapindex, int x, int y, clr_type clrtype);
int pc_setsavepoint(struct map_session_data*,short,int,int);
int pc_randomwarp(struct map_session_data *sd,clr_type type);
int pc_warpto(struct map_session_data* sd, struct map_session_data* pl_sd);
int pc_recall(struct map_session_data* sd, struct map_session_data* pl_sd);
int pc_memo(struct map_session_data* sd, int pos);

int pc_checkadditem(struct map_session_data*,int,int);
int pc_inventoryblank(struct map_session_data*);
int pc_search_inventory(struct map_session_data *sd,int item_id);
int pc_payzeny(struct map_session_data*,int);
int pc_additem(struct map_session_data*,struct item*,int,e_log_pick_type);
int pc_getzeny(struct map_session_data*,int);
int pc_delitem(struct map_session_data*,int,int,int,short,e_log_pick_type);

// Special Shop System
void pc_paycash(struct map_session_data *sd, int price, int points);
void pc_getcash(struct map_session_data *sd, int cash, int points);

int pc_cart_additem(struct map_session_data *sd,struct item *item_data,int amount,e_log_pick_type log_type);
int pc_cart_delitem(struct map_session_data *sd,int n,int amount,int type,e_log_pick_type log_type);
int pc_putitemtocart(struct map_session_data *sd,int idx,int amount);
int pc_getitemfromcart(struct map_session_data *sd,int idx,int amount);
int pc_cartitem_amount(struct map_session_data *sd,int idx,int amount);

int pc_takeitem(struct map_session_data*,struct flooritem_data*);
int pc_dropitem(struct map_session_data*,int,int);

bool pc_isequipped(struct map_session_data *sd, int nameid);
bool pc_can_Adopt(struct map_session_data *p1_sd, struct map_session_data *p2_sd, struct map_session_data *b_sd );
bool pc_adoption(struct map_session_data *p1_sd, struct map_session_data *p2_sd, struct map_session_data *b_sd);

int pc_updateweightstatus(struct map_session_data *sd);

int pc_addautobonus(struct s_autobonus *bonus,char max,const char *script,short rate,unsigned int dur,short atk_type,const char *o_script,unsigned short pos,bool onskill);
int pc_exeautobonus(struct map_session_data* sd,struct s_autobonus *bonus);
int pc_endautobonus(int tid, unsigned int tick, int id, intptr_t data);
int pc_delautobonus(struct map_session_data* sd,struct s_autobonus *bonus,char max,bool restore);

int pc_bonus(struct map_session_data*,int,int);
int pc_bonus2(struct map_session_data *sd,int,int,int);
int pc_bonus3(struct map_session_data *sd,int,int,int,int);
int pc_bonus4(struct map_session_data *sd,int,int,int,int,int);
int pc_bonus5(struct map_session_data *sd,int,int,int,int,int,int);
int pc_skill(struct map_session_data* sd, int id, int level, int flag);

int pc_insert_card(struct map_session_data *sd,int idx_card,int idx_equip);

int pc_steal_item(struct map_session_data *sd,struct block_list *bl, int skilllv);
int pc_steal_coin(struct map_session_data *sd,struct block_list *bl);

int pc_modifybuyvalue(struct map_session_data*,int);
int pc_modifysellvalue(struct map_session_data*,int);

int pc_follow(struct map_session_data*, int); // [MouseJstr]
int pc_stop_following(struct map_session_data*);

unsigned int pc_maxbaselv(struct map_session_data *sd);
unsigned int pc_maxjoblv(struct map_session_data *sd);
int pc_checkbaselevelup(struct map_session_data *sd);
int pc_checkjoblevelup(struct map_session_data *sd);
int pc_gainexp(struct map_session_data*,struct block_list*,unsigned int,unsigned int, bool);
unsigned int pc_nextbaseexp(struct map_session_data *);
unsigned int pc_thisbaseexp(struct map_session_data *);
unsigned int pc_nextjobexp(struct map_session_data *);
unsigned int pc_thisjobexp(struct map_session_data *);
int pc_gets_status_point(int);
int pc_need_status_point(struct map_session_data *,int,int);
int pc_statusup(struct map_session_data*,int);
int pc_statusup2(struct map_session_data*,int,int);
int pc_skillup(struct map_session_data*,int);
int pc_allskillup(struct map_session_data*);
int pc_resetlvl(struct map_session_data*,int type);
int pc_resetstate(struct map_session_data*);
int pc_resetskill(struct map_session_data*, int);
int pc_resetfeel(struct map_session_data*);
int pc_resethate(struct map_session_data*);
int pc_equipitem(struct map_session_data*,int,int);
int pc_unequipitem(struct map_session_data*,int,int);
int pc_checkitem(struct map_session_data*);
int pc_useitem(struct map_session_data*,int);

int pc_skillatk_bonus(struct map_session_data *sd, int skill_num);
int pc_skillheal_bonus(struct map_session_data *sd, int skill_num);
int pc_skillheal2_bonus(struct map_session_data *sd, int skill_num);

void pc_damage(struct map_session_data *sd,struct block_list *src,unsigned int hp, unsigned int sp);
int pc_dead(struct map_session_data *sd,struct block_list *src,int skill);
void pc_revive(struct map_session_data *sd,unsigned int hp, unsigned int sp);
void pc_heal(struct map_session_data *sd,unsigned int hp,unsigned int sp, int type);
int pc_itemheal(struct map_session_data *sd,int itemid, int hp,int sp);
int pc_percentheal(struct map_session_data *sd,int,int);
int pc_jobchange(struct map_session_data *,int, int);
int pc_setoption(struct map_session_data *,int);
int pc_setcart(struct map_session_data* sd, int type);
int pc_setfalcon(struct map_session_data* sd, int flag);
int pc_setriding(struct map_session_data* sd, int flag);
int pc_changelook(struct map_session_data *,int,int);
int pc_equiplookall(struct map_session_data *sd);

int pc_readparam(struct map_session_data*,int);
int pc_setparam(struct map_session_data*,int,int);
int pc_readreg(struct map_session_data*,int);
int pc_setreg(struct map_session_data*,int,int);
char *pc_readregstr(struct map_session_data *sd,int reg);
int pc_setregstr(struct map_session_data *sd,int reg,const char *str);

#define pc_readglobalreg(sd,reg) pc_readregistry(sd,reg,3)
#define pc_setglobalreg(sd,reg,val) pc_setregistry(sd,reg,val,3)
#define pc_readglobalreg_str(sd,reg) pc_readregistry_str(sd,reg,3)
#define pc_setglobalreg_str(sd,reg,val) pc_setregistry_str(sd,reg,val,3)
#define pc_readaccountreg(sd,reg) pc_readregistry(sd,reg,2)
#define pc_setaccountreg(sd,reg,val) pc_setregistry(sd,reg,val,2)
#define pc_readaccountregstr(sd,reg) pc_readregistry_str(sd,reg,2)
#define pc_setaccountregstr(sd,reg,val) pc_setregistry_str(sd,reg,val,2)
#define pc_readaccountreg2(sd,reg) pc_readregistry(sd,reg,1)
#define pc_setaccountreg2(sd,reg,val) pc_setregistry(sd,reg,val,1)
#define pc_readaccountreg2str(sd,reg) pc_readregistry_str(sd,reg,1)
#define pc_setaccountreg2str(sd,reg,val) pc_setregistry_str(sd,reg,val,1)
int pc_readregistry(struct map_session_data*,const char*,int);
int pc_setregistry(struct map_session_data*,const char*,int,int);
char *pc_readregistry_str(struct map_session_data*,const char*,int);
int pc_setregistry_str(struct map_session_data*,const char*,const char*,int);

int pc_addeventtimer(struct map_session_data *sd,int tick,const char *name);
int pc_deleventtimer(struct map_session_data *sd,const char *name);
int pc_cleareventtimer(struct map_session_data *sd);
int pc_addeventtimercount(struct map_session_data *sd,const char *name,int tick);

int pc_calc_pvprank(struct map_session_data *sd);
int pc_calc_pvprank_timer(int tid, unsigned int tick, int id, intptr_t data);

int pc_calc_skillpoint(struct map_session_data* sd);
int pc_ismarried(struct map_session_data *sd);
int pc_marriage(struct map_session_data *sd,struct map_session_data *dstsd);
int pc_divorce(struct map_session_data *sd);
struct map_session_data *pc_get_partner(struct map_session_data *sd);
struct map_session_data *pc_get_father(struct map_session_data *sd);
struct map_session_data *pc_get_mother(struct map_session_data *sd);
struct map_session_data *pc_get_child(struct map_session_data *sd);

void pc_bleeding (struct map_session_data *sd, unsigned int diff_tick);
void pc_regen (struct map_session_data *sd, unsigned int diff_tick);

void pc_setstand(struct map_session_data *sd);
int pc_candrop(struct map_session_data *sd,struct item *item);

int pc_jobid2mapid(unsigned short b_class);	// Skotlex
int pc_mapid2jobid(unsigned short class_, int sex);	// Skotlex

const char * job_name(int class_);

struct skill_tree_entry {
	short id;
	unsigned char max;
	unsigned char joblv;
	struct {
		short id;
		unsigned char lv;
	} need[MAX_PC_SKILL_REQUIRE];
}; // Celest
extern struct skill_tree_entry skill_tree[CLASS_COUNT][MAX_SKILL_TREE];

struct sg_data {
	short anger_id;
	short bless_id;
	short comfort_id;
	char feel_var[NAME_LENGTH];
	char hate_var[NAME_LENGTH];
	int (*day_func)(void);
};
extern const struct sg_data sg_info[MAX_PC_FEELHATE];

void pc_setinvincibletimer(struct map_session_data* sd, int val);
void pc_delinvincibletimer(struct map_session_data* sd);

int pc_addspiritball(struct map_session_data *sd,int,int);
int pc_delspiritball(struct map_session_data *sd,int,int);
void pc_addfame(struct map_session_data *sd,int count,short flag);
unsigned char pc_famerank(int char_id, int job);
int pc_set_hate_mob(struct map_session_data *sd, int pos, struct block_list *bl);

extern unsigned int stats_point_table[MAX_LEVEL+1];

extern struct fame_list smith_fame_list[MAX_FAME_LIST];
extern struct fame_list chemist_fame_list[MAX_FAME_LIST];
extern struct fame_list taekwon_fame_list[MAX_FAME_LIST];
extern struct fame_list pvprank_fame_list[MAX_FAME_LIST];
extern struct fame_list pvpevent_fame_list[MAX_FAME_LIST];
extern struct fame_list bgrank_fame_list[MAX_FAME_LIST];
extern struct fame_list bg_fame_list[MAX_FAME_LIST];

int pc_readdb(void);
int do_init_pc(void);
void do_final_pc(void);

enum {ADDITEM_EXIST,ADDITEM_NEW,ADDITEM_OVERAMOUNT};

// timer for night.day
extern int day_timer_tid;
extern int night_timer_tid;
int map_day_timer(int tid, unsigned int tick, int id, intptr_t data); // by [yor]
int map_night_timer(int tid, unsigned int tick, int id, intptr_t data); // by [yor]

// PVP Mode [Zephyrus]
int pc_get_aura(struct map_session_data *sd);
void pc_pvpmode(struct map_session_data* sd);
void pc_pvpmodeoff(struct map_session_data* sd, short force, short flag);
void pc_calc_playtime(struct map_session_data* sd);
int pc_update_last_action(struct map_session_data *sd, int type);
void pc_pvpevent_addfame(struct map_session_data *sd, bool single);

// Rental System
void pc_inventory_rentals(struct map_session_data *sd);
int pc_inventory_rental_clear(struct map_session_data *sd);
void pc_inventory_rental_add(struct map_session_data *sd, int seconds);

// WoE Ranking Stats
void pc_record_damage(struct block_list *src, struct block_list *dst, int damage);
void pc_record_maxdamage(struct block_list *src, struct block_list *dst, int damage);
void pc_record_mobkills(struct map_session_data *sd, struct mob_data *md);
void pc_ranking_reset(int type, bool char_server);
void pc_item_remove4all(int nameid, bool char_server);

int pc_read_motd(void); // [Valaris]
int pc_disguise(struct map_session_data *sd, int class_);

#endif /* _PC_H_ */
