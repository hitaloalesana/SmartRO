// Copyright (c) Athena Dev Teams - Licensed under GNU GPL
// For more information, see LICENCE in the main folder

#ifndef _BATTLE_H_
#define _BATTLE_H_

// state of a single attack attempt; used in flee/def penalty calculations when mobbed
typedef enum damage_lv {
	ATK_NONE,    // not an attack
	ATK_LUCKY,   // attack was lucky-dodged
	ATK_FLEE,    // attack was dodged
	ATK_MISS,    // attack missed because of element/race modifier.
	ATK_BLOCK,   // attack was blocked by some skills.
	ATK_DEF      // attack connected
} damage_lv;

// ダメージ
struct Damage {
	int damage,damage2;
	int type,div_;
	int amotion,dmotion;
	int blewcount;
	int flag;
	enum damage_lv dmg_lv;	//ATK_LUCKY,ATK_FLEE,ATK_DEF
};

// 属性表（読み込みはpc.c、battle_attr_fixで使用）
extern int attr_fix_table[4][10][10];

struct map_session_data;
struct mob_data;
struct block_list;

// ダメージ計算

struct Damage battle_calc_attack(int attack_type,struct block_list *bl,struct block_list *target,int skill_num,int skill_lv,int count);

int battle_calc_return_damage(struct block_list *bl, int damage, int flag);

void battle_drain(struct map_session_data *sd, struct block_list *tbl, int rdamage, int ldamage, int race, int boss);

int battle_attr_ratio(int atk_elem,int def_type, int def_lv);
int battle_attr_fix(struct block_list *src, struct block_list *target, int damage,int atk_elem,int def_type, int def_lv);

// ダメージ最終計算
int battle_calc_damage(struct block_list *src,struct block_list *bl,struct Damage *d,int damage,int skill_num,int skill_lv);
int battle_calc_gvg_damage(struct block_list *src,struct block_list *bl,int damage,int div_,int skill_num,int skill_lv,int flag);
int battle_calc_bg_damage(struct block_list *src,struct block_list *bl,int damage,int div_,int skill_num,int skill_lv,int flag);
int battle_calc_pvpevent_damage(struct block_list *src, struct block_list *bl, int damage, int div_, int skill_num, int skill_lv, int flag);
int battle_calc_faction_damage(struct block_list *src, struct block_list *bl, int damage, int div_, int skill_num, int skill_lv, int flag);

enum {	// 最終計算のフラグ
	BF_WEAPON	= 0x0001,
	BF_MAGIC	= 0x0002,
	BF_MISC		= 0x0004,
	BF_SHORT	= 0x0010,
	BF_LONG		= 0x0040,
	BF_SKILL	= 0x0100,
	BF_NORMAL	= 0x0200,
	BF_WEAPONMASK=0x000f,
	BF_RANGEMASK= 0x00f0,
	BF_SKILLMASK= 0x0f00,
};

int battle_delay_damage (unsigned int tick, int amotion, struct block_list *src, struct block_list *target, int attack_type, int skill_id, int skill_lv, int damage, enum damage_lv dmg_lv, int ddelay, bool reflected);

// 通常攻撃処理まとめ
enum damage_lv battle_weapon_attack( struct block_list *bl,struct block_list *target,unsigned int tick,int flag);

// 各種パラメータを得る
struct block_list* battle_get_master(struct block_list *src);
struct block_list* battle_gettargeted(struct block_list *target);
struct block_list* battle_getenemy(struct block_list *target, int type, int range);
int battle_gettarget(struct block_list *bl);
int battle_getcurrentskill(struct block_list *bl);

enum e_battle_check_target
{//New definitions [Skotlex]
	BCT_ENEMY   = 0x020000,
	BCT_NOENEMY = 0x1d0000, //This should be (~BCT_ENEMY&BCT_ALL)
	BCT_PARTY	= 0x040000,
	BCT_NOPARTY = 0x1b0000, //This should be (~BCT_PARTY&BCT_ALL)
	BCT_GUILD	= 0x080000,
	BCT_NOGUILD = 0x170000, //This should be (~BCT_GUILD&BCT_ALL)
	BCT_ALL     = 0x1f0000,
	BCT_NOONE   = 0x000000,
	BCT_SELF    = 0x010000,
	BCT_NEUTRAL = 0x100000,
};

#define	is_boss(bl)	(status_get_mode(bl)&MD_BOSS)	// Can refine later [Aru]

int battle_check_undead(int race,int element);
int battle_check_target(struct block_list *src, struct block_list *target,int flag);
bool battle_check_range(struct block_list *src,struct block_list *bl,int range);

void battle_consume_ammo(struct map_session_data* sd, int skill, int lv);
// 設定

#define MIN_HAIR_STYLE battle_config.min_hair_style
#define MAX_HAIR_STYLE battle_config.max_hair_style
#define MIN_HAIR_COLOR battle_config.min_hair_color
#define MAX_HAIR_COLOR battle_config.max_hair_color
#define MIN_CLOTH_COLOR battle_config.min_cloth_color
#define MAX_CLOTH_COLOR battle_config.max_cloth_color

extern struct Battle_Config
{
	int warp_point_debug;
	int enable_critical;
	int mob_critical_rate;
	int critical_rate;
	int enable_baseatk;
	int enable_perfect_flee;
	int cast_rate, delay_rate;
	int delay_dependon_dex, delay_dependon_agi;
	int sdelay_attack_enable;
	int left_cardfix_to_right;
	int skill_add_range;
	int skill_out_range_consume;
	int skillrange_by_distance; //[Skotlex]
	int use_weapon_skill_range; //[Skotlex]
	int pc_damage_delay_rate;
	int defnotenemy;
	int vs_traps_bctall;	
	int traps_setting;
	int summon_flora; //[Skotlex]	
	int clear_unit_ondeath; //[Skotlex]
	int clear_unit_onwarp; //[Skotlex]
	int random_monster_checklv;
	int attr_recover;
	int item_auto_get;
	int flooritem_lifetime;
	int item_first_get_time;
	int item_second_get_time;
	int item_third_get_time;
	int mvp_item_first_get_time;
	int mvp_item_second_get_time;
	int mvp_item_third_get_time;
	int base_exp_rate,job_exp_rate;
	int base_exp_rate_boss,job_exp_rate_boss;
	int base_exp_rate_bonus,job_exp_rate_bonus;
	int drop_rate0item;
	int death_penalty_type;
	int death_penalty_base,death_penalty_job;
	int pvp_exp;  // [MouseJstr]
	int gtb_sc_immunity;
	int zeny_penalty;
	int restart_hp_rate;
	int restart_sp_rate;
	int mvp_exp_rate;
	int mvp_hp_rate;
	int monster_hp_rate;
	int monster_max_aspd;
	int view_range_rate;
	int chase_range_rate;
	int lowest_gm_level;
	int atc_gmonly;
	int atc_spawn_quantity_limit;
	int atc_slave_clone_limit;
	int partial_name_scan;
	int gm_allskill;
	int gm_allequip;
	int gm_skilluncond;
	int gm_join_chat;
	int gm_kick_chat;
	int skillfree;
	int skillup_limit;
	int wp_rate;
	int pp_rate;
	int monster_active_enable;
	int monster_damage_delay_rate;
	int monster_loot_type;
	int mob_skill_rate;	//[Skotlex]
	int mob_skill_delay;	//[Skotlex]
	int mob_count_rate;
	int no_spawn_on_player; //[Skotlex]
	int force_random_spawn; //[Skotlex]
	int mob_spawn_delay, plant_spawn_delay, boss_spawn_delay;	// [Skotlex]
	int slaves_inherit_mode;
	int slaves_inherit_speed;
	int summons_trigger_autospells;
	int pc_walk_delay_rate; //Adjusts can't walk delay after being hit for players. [Skotlex]
	int walk_delay_rate; //Adjusts can't walk delay after being hit. [Skotlex]
	int walk_delay_rate_boss; // Adjusts can't walk delay after bein hit por Boss monsters [Zephyrus]
	int multihit_delay;  //Adjusts can't walk delay per hit on multi-hitting skills. [Skotlex]
	int quest_skill_learn;
	int quest_skill_reset;
	int basic_skill_check;
	int guild_emperium_check;
	int guild_exp_limit;
	int guild_max_castles;
	int guild_skill_relog_delay;
	int emergency_call;
	int guild_aura;
	int guild_skills_separed_delay;
	int pc_invincible_time;

	int pet_catch_rate;
	int pet_rename;
	int pet_friendly_rate;
	int pet_hungry_delay_rate;
	int pet_hungry_friendly_decrease;
	int pet_status_support;
	int pet_attack_support;
	int pet_damage_support;
	int pet_support_min_friendly;	//[Skotlex]
	int pet_equip_min_friendly;
	int pet_support_rate;
	int pet_attack_exp_to_master;
	int pet_attack_exp_rate;
	int pet_lv_rate; //[Skotlex]
	int pet_max_stats; //[Skotlex]
	int pet_max_atk1; //[Skotlex]
	int pet_max_atk2; //[Skotlex]
	int pet_no_gvg; //Disables pets in gvg. [Skotlex]
	int pet_equip_required;

	int skill_min_damage;
	int finger_offensive_type;
	int heal_exp;
	int max_heal_lv;
	int max_heal; //Mitternacht
	int resurrection_exp;
	int shop_exp;
	int combo_delay_rate;
	int item_check;
	int item_use_interval;	//[Skotlex]
	int cashfood_use_interval;
	int wedding_modifydisplay;
	int wedding_ignorepalette;	//[Skotlex]
	int xmas_ignorepalette;	// [Valaris]
	int summer_ignorepalette; // [Zephyrus]
	int natural_healhp_interval;
	int natural_healsp_interval;
	int natural_heal_skill_interval;
	int natural_heal_weight_rate;
	int arrow_decrement;
	int max_aspd;
	int max_walk_speed;	//Maximum walking speed after buffs [Skotlex]
	int max_hp;
	int max_sp;
	int max_lv, aura_lv;
	int max_parameter, max_baby_parameter;
	int max_cart_weight;
	int skill_log;
	int battle_log;
	int save_log;
	int etc_log;
	int save_clothcolor;
	int undead_detect_type;
	int auto_counter_type;
	int min_hitrate;	//[Skotlex]
	int max_hitrate;	//[Skotlex]
	int agi_penalty_target;
	int agi_penalty_type;
	int agi_penalty_count;
	int agi_penalty_num;
	int vit_penalty_target;
	int vit_penalty_type;
	int vit_penalty_count;
	int vit_penalty_num;
	int weapon_defense_type;
	int magic_defense_type;
	int skill_reiteration;
	int skill_nofootset;
	int pc_cloak_check_type;
	int monster_cloak_check_type;
	int estimation_type;
	int gvg_short_damage_rate;
	int gvg_long_damage_rate;
	int gvg_weapon_damage_rate;
	int gvg_magic_damage_rate;
	int gvg_misc_damage_rate;
	int gvg_flee_penalty;
	int gvg_eliminate_time;
	int pk_short_damage_rate;
	int pk_long_damage_rate;
	int pk_weapon_damage_rate;
	int pk_magic_damage_rate;
	int pk_misc_damage_rate;
	int mob_changetarget_byskill;
	int attack_direction_change;
	int land_skill_limit;
	int party_skill_penalty;
	int monster_class_change_recover;
	int produce_item_name_input;
	int display_skill_fail;
	int chat_warpportal;
	int mob_warp;
	int dead_branch_active;
	int vending_max_value;
	int vending_over_max;
	int vending_tax;
	int show_steal_in_same_party;
	int party_share_type;
	int party_hp_mode;
	int party_show_share_picker;
	int show_picker_item_type;
	int attack_attr_none;
	int item_rate_mvp, item_rate_common, item_rate_common_boss, item_rate_card, item_rate_card_boss,
		item_rate_equip, item_rate_equip_boss, item_rate_heal, item_rate_heal_boss, item_rate_use,
		item_rate_use_boss, item_rate_treasure, item_rate_adddrop;

	int logarithmic_drops;
	int item_drop_common_min,item_drop_common_max;	// Added by TyrNemesis^
	int item_drop_card_min,item_drop_card_max;
	int item_drop_equip_min,item_drop_equip_max;
	int item_drop_mvp_min,item_drop_mvp_max;	// End Addition
	int item_drop_heal_min,item_drop_heal_max;	// Added by Valatris
	int item_drop_use_min,item_drop_use_max;	//End
	int item_drop_treasure_min,item_drop_treasure_max; //by [Skotlex]
	int item_drop_adddrop_min,item_drop_adddrop_max; //[Skotlex]

	int prevent_logout;	// Added by RoVeRT

	int alchemist_summon_reward;	// [Valaris]
	int drops_by_luk;
	int drops_by_luk2;
	int equip_natural_break_rate;	//Base Natural break rate for attacks.
	int equip_self_break_rate; //Natural & Penalty skills break rate
	int equip_skill_break_rate; //Offensive skills break rate
	int multi_level_up;
	int max_exp_gain_rate; //Max amount of exp bar % you can get in one go.
	int pk_mode;
	int pk_level_range;

	int manner_system; // end additions [Valaris]
	int show_mob_info; 

	int agi_penalty_count_lv;
	int vit_penalty_count_lv;

	int gx_allhit;
	int gx_disptype;
	int devotion_level_difference;
	int player_skill_partner_check;
	int hide_GM_session;
	int invite_request_check;
	int skill_removetrap_type;
	int disp_experience;
	int disp_zeny;
	int castle_defense_rate;
	int backstab_bow_penalty;
	int hp_rate;
	int sp_rate;
	int gm_cant_drop_min_lv;
	int gm_cant_drop_max_lv;
	int disp_hpmeter;
	int bone_drop;
	int buyer_name;
	int gm_cant_party_min_lv;
	int gm_can_party; // [SketchyPhoenix]
	int gm_channel_operator;
	int pc_validate_items;
	int pc_validate_stats;

// eAthena additions
	int night_at_start; // added by [Yor]
	int day_duration; // added by [Yor]
	int night_duration; // added by [Yor]
	int ban_hack_trade; // added by [Yor]
	int hack_info_GM_level; // added by [Yor]
	int any_warp_GM_min_level; // added by [Yor]
	int packet_ver_flag; // added by [Yor]
	
	int min_hair_style; // added by [MouseJstr]
	int max_hair_style; // added by [MouseJstr]
	int min_hair_color; // added by [MouseJstr]
	int max_hair_color; // added by [MouseJstr]
	int min_cloth_color; // added by [MouseJstr]
	int max_cloth_color; // added by [MouseJstr]
	int pet_hair_style; // added by [Skotlex]

	int castrate_dex_scale; // added by [MouseJstr]
	int area_size; // added by [MouseJstr]

	int max_def, over_def_bonus; //added by [Skotlex]
	
	int zeny_from_mobs; // [Valaris]
	int mobs_level_up; // [Valaris]
	int mobs_level_up_exp_rate; // [Valaris]
	int pk_min_level; // [celest]
	int skill_steal_max_tries; //max steal skill tries on a mob. if 0, then w/o limit [Lupus]
	int motd_type; // [celest]
	int finding_ore_rate; // orn
	int exp_calc_type;
	int exp_bonus_attacker;
	int exp_bonus_max_attacker;
	int min_skill_delay_limit;
	int default_walk_delay;
	int no_skill_delay;
	int attack_walk_delay;
	int require_glory_guild;
	int idle_no_share;
	int party_update_interval;
	int party_even_share_bonus;
	int delay_battle_damage;
	int hide_woe_damage;
	int display_version;
	int who_display_aid;

	int display_hallucination;	// [Skotlex]
	int use_statpoint_table;	// [Skotlex]

	int ignore_items_gender; //[Lupus]

	int copyskill_restrict; // [Aru]
	int berserk_cancels_buffs; // [Aru]
	int debuff_on_logout; // Removes a few "official" negative Scs on logout. [Skotlex]
	int mob_ai; //Configures various mob_ai settings to make them smarter or dumber(official). [Skotlex]
	int hom_setting; //Configures various homunc settings which make them behave unlike normal characters.. [Skotlex]
	int dynamic_mobs; // Dynamic Mobs [Wizputer] - battle_athena flag implemented by [random]
	int mob_remove_damaged; // Dynamic Mobs - Remove mobs even if damaged [Wizputer]
	int mob_remove_delay; // Dynamic Mobs - delay before removing mobs from a map [Skotlex]
	int mob_active_time; //Duration through which mobs execute their Hard AI after players leave their area of sight.
	int boss_active_time;

	int show_hp_sp_drain, show_hp_sp_gain;	//[Skotlex]

	int mob_npc_event_type; //Determines on who the npc_event is executed. [Skotlex]
	int mob_clear_delay; // [Valaris]

	int character_size; // if riders have size=2, and baby class riders size=1 [Lupus]
	int mob_max_skilllvl; // Max possible skill level [Lupus]
	int rare_drop_announce; // chance <= to show rare drops global announces

	int retaliate_to_master;	//Whether when a mob is attacked by another mob, it will retaliate versus the mob or the mob's master. [Skotlex]

	int title_lvl1; // Players titles [Lupus]
	int title_lvl2; // Players titles [Lupus]
	int title_lvl3; // Players titles [Lupus]
	int title_lvl4; // Players titles [Lupus]
	int title_lvl5; // Players titles [Lupus]
	int title_lvl6; // Players titles [Lupus]
	int title_lvl7; // Players titles [Lupus]
	int title_lvl8; // Players titles [Lupus]
	
	int duel_allow_pvp; // [LuzZza]
	int duel_allow_gvg; // [LuzZza]
	int duel_allow_teleport; // [LuzZza]
	int duel_autoleave_when_die; // [LuzZza]
	int duel_time_interval; // [LuzZza]
	int duel_only_on_same_map; // [Toms]
	
	int skip_teleport_lv1_menu; // possibility to disable (skip) Teleport Lv1 menu, that have only two lines `Random` and `Cancel` [LuzZza]

	int allow_skill_without_day; // [Komurka]
	int allow_es_magic_pc; // [Skotlex]
	int skill_wall_check; // [Skotlex]
	int cell_stack_limit; // [Skotlex]
	int skill_caster_check; // [Skotlex]
	int sc_castcancel; // [Skotlex]
	int pc_sc_def_rate; // [Skotlex]
	int mob_sc_def_rate;
	int pc_luk_sc_def;
	int mob_luk_sc_def;
	int pc_max_sc_def;
	int mob_max_sc_def;

	int sg_angel_skill_ratio;
	int sg_miracle_skill_ratio;
	int sg_miracle_skill_duration;
	int autospell_stacking; //Enables autospell cards to stack. [Skotlex]
	int override_mob_names; //Enables overriding spawn mob names with the mob_db names. [Skotlex]
	int min_chat_delay; //Minimum time between client messages. [Skotlex]
	int channel_min_chat_delay; //Minimum time between client channel messages.
	int friend_auto_add; //When accepting friends, both get friended. [Skotlex]
	int hvan_explosion_intimate;	// fix [albator]
	int hom_rename;
	int homunculus_show_growth ;	//[orn]
	int homunculus_friendly_rate;
	int quest_exp_rate;

	// [Terra Custom Settings]
	int autotrade_mapflag;
	int at_timeout;		int afk_timeout;
	int at_tax;
	int homunculus_autoloot;
	int idle_no_autoloot;
	int max_guild_alliance;
	int max_guild_opposition;
	int mob_graveyard;
	int pc_graveyard;

	int pvpmode_onlypc;
	int pvpmode_gvgreductions;
	int pvpmode_expbonus;
	int pvpmode_nowarp_cmd;
	int pvpmode_enable_delay;
	int pvpmode_disable_delay;

	int ksprotection;
	int auction_feeperhour;
	int auction_maximumprice;
	int gm_viewequip_min_lv;
	int homunculus_auto_vapor;	//Keep Homunculus from Vaporizing when master dies. [L0ne_W0lf]
	int display_status_timers;	//Show or hide skill buff/delay timers in recent clients [Sara]
	int skill_add_heal_rate;	//skills that bHealPower has effect on [Inkfish]
	int eq_single_target_reflectable;
	int invincible_nodamage;
	int mob_slave_keep_target;
	int autospell_check_range;	//Enable range check for autospell bonus. [L0ne_W0lf]
	int client_reshuffle_dice;  // Reshuffle /dice
	int client_sort_storage;
	int gm_check_minlevel;  // min GM level for /check
	int feature_buying_store;
	int feature_search_stores;
	int searchstore_querydelay;
	int searchstore_maxresults;
	int display_party_name;
	int cashshop_show_points;
	int mail_show_status;
	int client_limit_unit_lv;

	// [Flood Protection - Automute]
	int chat_allowed_per_interval;
	int chat_time_interval;
	int chat_flood_automute;

	// [Action Limits]
	int action_keyboard_limit;
	int action_mouse_limit;
	int action_dual_limit;

	// [BattleGround Settings]
	int bg_update_interval;
	int bg_short_damage_rate;
	int bg_long_damage_rate;
	int bg_weapon_damage_rate;
	int bg_magic_damage_rate;
	int bg_misc_damage_rate;
	int bg_flee_penalty;
	int bg_idle_announce;
	int bg_idle_autokick;
	int bg_reserved_char_id;
	int bg_items_on_pvp;
	int bg_reward_rates;
	int bg_ranking_bonus;

	int bg_ranked_mode;
	int bg_ranked_max_games;
	int bg_reportafk_leaderonly;
	int bg_queue2team_balanced;
	int bg_logincount_check;
	int bg_queue_onlytowns;

	int bg_eAmod_mode;

	// Faction System
	int faction_allow_party;
	int faction_allow_guild;
	int faction_allow_chat;
	int faction_allow_vending;
	int faction_allow_trade;

	int faction_short_damage_rate;
	int faction_long_damage_rate;
	int faction_weapon_damage_rate;
	int faction_magic_damage_rate;
	int faction_misc_damage_rate;

	// [Reserved Chars ID]
	int ancient_reserved_char_id;
	int costume_reserved_char_id;
	int woe_reserved_char_id;

	// [PVP Event Settings]
	int pvpevent_short_damage_rate;
	int pvpevent_long_damage_rate;
	int pvpevent_weapon_damage_rate;
	int pvpevent_magic_damage_rate;
	int pvpevent_misc_damage_rate;
	int pvpevent_flee_penalty;
	int pvpevent_cashperkill;
	// [Custom Settings Terra RO]
	int channel_system_enable;
	int channel_announces;
	int channel_announce_join;
	int skill_zeny2item;

	int myinfo_event_vote_points;
	int vending_cash_id;
	int vending_zeny_id;

	int premium_bonusexp;
	int premium_dropboost;
	int premium_discount;

	int guild_wars;
	int region_display; // Region Notifications
	int super_woe_enable;
	int at_changegm_cost;

	int mob_slave_adddrop;
	int reflect_damage_fix;
	int dancing_weaponchange_fix;
	int anti_mayapurple_hack;
	
		// Keitenai Delay
	int KEITENAI_DELAY_SYSTEM;
	int K_DELAY_SM_BASH;
	int K_DELAY_SM_MAGNUM;
	int K_DELAY_MG_NAPALMBEAT;
	int K_DELAY_MG_SOULSTRIKE;
	int K_DELAY_MG_COLDBOLT;
	int K_DELAY_MG_FROSTDIVER;
	int K_DELAY_MG_STONECURSE;
	int K_DELAY_MG_FIREBALL;
	int K_DELAY_MG_FIREWALL;
	int K_DELAY_MG_FIREBOLT;
	int K_DELAY_MG_LIGHTNINGBOLT;
	int K_DELAY_MG_THUNDERSTORM;
	int K_DELAY_AL_HEAL;
	int K_DELAY_AL_DECAGI;
	int K_DELAY_AL_CRUCIS;
	int K_DELAY_MC_MAMMONITE;
	int K_DELAY_AC_DOUBLE;
	int K_DELAY_AC_SHOWER;
	int K_DELAY_TF_POISON;
	int K_DELAY_KN_PIERCE;
	int K_DELAY_KN_BRANDISHSPEAR;
	int K_DELAY_KN_SPEARSTAB;
	int K_DELAY_KN_SPEARBOOMERANG;
	int K_DELAY_KN_BOWLINGBASH;
	int K_DELAY_PR_LEXDIVINA;
	int K_DELAY_PR_TURNUNDEAD;
	int K_DELAY_PR_LEXAETERNA;
	int K_DELAY_PR_MAGNUS;
	int K_DELAY_WZ_FIREPILLAR;
	int K_DELAY_WZ_SIGHTRASHER;
	int K_DELAY_WZ_FIREIVY;
	int K_DELAY_WZ_METEOR;
	int K_DELAY_WZ_JUPITEL;
	int K_DELAY_WZ_VERMILION;
	int K_DELAY_WZ_WATERBALL;
	int K_DELAY_WZ_ICEWALL;
	int K_DELAY_WZ_FROSTNOVA;
	int K_DELAY_WZ_STORMGUST;
	int K_DELAY_WZ_EARTHSPIKE;
	int K_DELAY_WZ_HEAVENDRIVE;
	int K_DELAY_WZ_QUAGMIRE;
	int K_DELAY_WZ_ESTIMATION;
	int K_DELAY_BS_HAMMERFALL;
	int K_DELAY_HT_BLITZBEAT;
	int K_DELAY_AS_SONICBLOW;
	int K_DELAY_AS_GRIMTOOTH;
	int K_DELAY_AC_CHARGEARROW;
	int K_DELAY_TF_BACKSLIDING;
	int K_DELAY_MC_CARTREVOLUTION;
	int K_DELAY_AL_HOLYLIGHT;
	int K_DELAY_RG_BACKSTAP;
	int K_DELAY_RG_RAID;
	int K_DELAY_RG_GRAFFITI;
	int K_DELAY_RG_FLAGGRAFFITI;
	int K_DELAY_RG_COMPULSION;
	int K_DELAY_RG_PLAGIARISM;
	int K_DELAY_AM_DEMONSTRATION;
	int K_DELAY_AM_ACIDTERROR;
	int K_DELAY_AM_POTIONPITCHER;
	int K_DELAY_AM_CANNIBALIZE;
	int K_DELAY_AM_SPHEREMINE;
	int K_DELAY_AM_FLAMECONTROL;
	int K_DELAY_AM_DRILLMASTER;
	int K_DELAY_CR_TRUST;
	int K_DELAY_CR_SHIELDBOOMERANG;
	int K_DELAY_CR_HOLYCROSS;
	int K_DELAY_CR_GRANDCROSS;
	int K_DELAY_MO_CALLSPIRITS;
	int K_DELAY_MO_ABSORBSPIRITS;
	int K_DELAY_MO_TRIPLEATTACK;
	int K_DELAY_MO_BODYRELOCATION;
	int K_DELAY_MO_INVESTIGATE;
	int K_DELAY_MO_FINGEROFFENSIVE;
	int K_DELAY_MO_EXPLOSIONSPIRITS;
	int K_DELAY_MO_EXTREMITYFIST;
	int K_DELAY_MO_CHAINCOMBO;
	int K_DELAY_MO_COMBOFINISH;
	int K_DELAY_SA_CASTCANCEL;
	int K_DELAY_SA_SPELLBREAKER;
	int K_DELAY_SA_DISPELL;
	int K_DELAY_SA_ABRACADABRA;
	int K_DELAY_SA_MONOCELL;
	int K_DELAY_SA_CLASSCHANGE;
	int K_DELAY_SA_SUMMONMONSTER;
	int K_DELAY_SA_REVERSEORCISH;
	int K_DELAY_SA_DEATH;
	int K_DELAY_SA_FORTUNE;
	int K_DELAY_SA_TAMINGMONSTER;
	int K_DELAY_SA_QUESTION;
	int K_DELAY_SA_GRAVITY;
	int K_DELAY_SA_LEVELUP;
	int K_DELAY_SA_INSTANTDEATH;
	int K_DELAY_SA_FULLRECOVERY;
	int K_DELAY_SA_COMA;
	int K_DELAY_BD_ADAPTATION;
	int K_DELAY_BD_ENCORE;
	int K_DELAY_BD_LULLABY;
	int K_DELAY_BD_RICHMANKIM;
	int K_DELAY_BA_MUSICALSTRIKE;
	int K_DELAY_BA_DISSONANCE;
	int K_DELAY_BA_FROSTJOKER;
	int K_DELAY_BA_WHISTLE;
	int K_DELAY_BA_ASSASSINCROSS;
	int K_DELAY_BA_POEMBRAGI;
	int K_DELAY_BA_APPLEIDUN;
	int K_DELAY_DC_THROWARROW;
	int K_DELAY_DC_UGLYDANCE;
	int K_DELAY_DC_SCREAM;
	int K_DELAY_DC_HUMMING;
	int K_DELAY_DC_DONTFORGETME;
	int K_DELAY_DC_FORTUNEKISS;
	int K_DELAY_DC_SERVICEFORYOU;
	int K_DELAY_LK_FURY;
	int K_DELAY_HW_MAGICCRASHER;
	int K_DELAY_PA_PRESSURE;
	int K_DELAY_CH_PALMSTRIKE;
	int K_DELAY_CH_TIGERFIST;
	int K_DELAY_CH_CHAINCRUSH;
	int K_DELAY_PF_SOULCHANGE;
	int K_DELAY_PF_SOULBURN;
	int K_DELAY_ASC_BREAKER;
	int K_DELAY_SN_FALCONASSAULT;
	int K_DELAY_SN_SHARPSHOOTING;
	int K_DELAY_CR_ALCHEMY;
	int K_DELAY_CR_SYNTHESISPOTION;
	int K_DELAY_CG_ARROWVULCAN;
	int K_DELAY_CG_MOONLIT;
	int K_DELAY_CG_MARIONETTE;
	int K_DELAY_LK_SPIRALPIERCE;
	int K_DELAY_LK_HEADCRUSH;
	int K_DELAY_LK_JOINTBEAT;
	int K_DELAY_HW_NAPALMVULCAN;
	int K_DELAY_CH_SOULCOLLECT;
	int K_DELAY_PF_MINDBREAKER;
	int K_DELAY_PF_SPIDERWEB;
	int K_DELAY_ASC_METEORASSAULT;
	int K_DELAY_TK_STORMKICK;
	int K_DELAY_TK_DOWNKICK;
	int K_DELAY_TK_TURNKICK;
	int K_DELAY_TK_JUMPKICK;
	int K_DELAY_TK_POWER;
	int K_DELAY_TK_HIGHJUMP;
	int K_DELAY_SL_KAIZEL;
	int K_DELAY_SL_KAAHI;
	int K_DELAY_SL_KAUPE;
	int K_DELAY_SL_KAITE;
	int K_DELAY_SL_KAINA;
	int K_DELAY_SL_STIN;
	int K_DELAY_SL_STUN;
	int K_DELAY_SL_SMA;
	int K_DELAY_SL_SWOO;
	int K_DELAY_SL_SKE;
	int K_DELAY_SL_SKA;
	int K_DELAY_ST_FULLSTRIP;
	int K_DELAY_CR_SLIMPITCHER;
	int K_DELAY_CR_FULLPROTECTION;
	int K_DELAY_PA_SHIELDCHAIN;
	int K_DELAY_HP_MANARECHARGE;
	int K_DELAY_PF_DOUBLECASTING;
	int K_DELAY_HW_GANBANTEIN;
	int K_DELAY_HW_GRAVITATION;
	int K_DELAY_WS_CARTTERMINATION;
	int K_DELAY_CG_HERMODE;
	int K_DELAY_CG_TAROTCARD;
	int K_DELAY_CR_ACIDDEMONSTRATION;
	int K_DELAY_SL_HIGH;
	int K_DELAY_HT_POWER;
	int K_DELAY_GS_TRIPLEACTION;
	int K_DELAY_GS_BULLSEYE;
	int K_DELAY_GS_MADNESSCANCEL;
	int K_DELAY_GS_INCREASING;
	int K_DELAY_GS_MAGICALBULLET;
	int K_DELAY_GS_CRACKER;
	int K_DELAY_GS_SINGLEACTION;
	int K_DELAY_GS_CHAINACTION;
	int K_DELAY_GS_TRACKING;
	int K_DELAY_GS_DISARM;
	int K_DELAY_GS_PIERCINGSHOT;
	int K_DELAY_GS_RAPIDSHOWER;
	int K_DELAY_GS_DESPERADO;
	int K_DELAY_GS_GATLINGFEVER;
	int K_DELAY_GS_DUST;
	int K_DELAY_GS_FULLBUSTER;
	int K_DELAY_GS_SPREADATTACK;
	int K_DELAY_GS_GROUNDDRIFT;
	int K_DELAY_NJ_TOBIDOUGU;
	int K_DELAY_NJ_SYURIKEN;
	int K_DELAY_NJ_KUNAI;
	int K_DELAY_NJ_HUUMA;
	int K_DELAY_NJ_ZENYNAGE;
	int K_DELAY_NJ_TATAMIGAESHI;
	int K_DELAY_NJ_KASUMIKIRI;
	int K_DELAY_NJ_SHADOWJUMP;
	int K_DELAY_NJ_KIRIKAGE;
	int K_DELAY_NJ_UTSUSEMI;
	int K_DELAY_NJ_BUNSINJYUTSU;
	int K_DELAY_NJ_NINPOU;
	int K_DELAY_NJ_KOUENKA;
	int K_DELAY_NJ_KAENSIN;
	int K_DELAY_NJ_BAKUENRYU;
	int K_DELAY_NJ_HYOUSENSOU;
	int K_DELAY_NJ_SUITON;
	int K_DELAY_NJ_HYOUSYOURAKU;
	int K_DELAY_NJ_HUUJIN;
	int K_DELAY_NJ_RAIGEKISAI;
	int K_DELAY_NJ_KAMAITACHI;
	int K_DELAY_NJ_NEN;
	int K_DELAY_NJ_ISSEN;
	int K_DELAY_KN_CHARGEATK;
	int K_DELAY_AS_VENOMKNIFE;
	int K_DELAY_RG_CLOSECONFINE;
	int K_DELAY_WZ_SIGHTBLASTER;
	int K_DELAY_HT_PHANTASMIC;
	int K_DELAY_BA_PANGVOICE;
	int K_DELAY_DC_WINKCHARM;
	int K_DELAY_PR_REDEMPTIO;
	int K_DELAY_MO_KITRANSLATION;
	int K_DELAY_MO_BALKYOUNG;
	int K_DELAY_RK_SONICWAVE;
	int K_DELAY_RK_DEATHBOUND;
	int K_DELAY_RK_HUNDREDSPEAR;
	int K_DELAY_RK_WINDCUTTER;
	int K_DELAY_RK_IGNITIONBREAK;
	int K_DELAY_RK_DRAGONBREATH;
	int K_DELAY_RK_CRUSHSTRIKE;
	int K_DELAY_RK_STORMBLAST;
	int K_DELAY_RK_PHANTOMTHRUST;
	int K_DELAY_GC_CROSSIMPACT;
	int K_DELAY_GC_WEAPONCRUSH;
	int K_DELAY_GC_ROLLINGCUTTER;
	int K_DELAY_GC_CROSSRIPPERSLASHER;
	int K_DELAY_AB_JUDEX;
	int K_DELAY_AB_ADORAMUS;
	int K_DELAY_AB_CHEAL;
	int K_DELAY_AB_EPICLESIS;
	int K_DELAY_AB_PRAEFATIO;
	int K_DELAY_AB_EUCHARISTICA;
	int K_DELAY_AB_RENOVATIO;
	int K_DELAY_AB_HIGHNESSHEAL;
	int K_DELAY_AB_CLEARANCE;
	int K_DELAY_AB_EXPIATIO;
	int K_DELAY_AB_DUPLELIGHT;
	int K_DELAY_AB_DUPLELIGHT_MELEE;
	int K_DELAY_AB_DUPLELIGHT_MAGIC;
	int K_DELAY_AB_SILENTIUM;
	int K_DELAY_WL_WHITEIMPRISON;
	int K_DELAY_WL_SOULEXPANSION;
	int K_DELAY_WL_FROSTMISTY;
	int K_DELAY_WL_JACKFROST;
	int K_DELAY_WL_MARSHOFABYSS;
	int K_DELAY_WL_RADIUS;
	int K_DELAY_WL_STASIS;
	int K_DELAY_WL_DRAINLIFE;
	int K_DELAY_WL_CRIMSONROCK;
	int K_DELAY_WL_HELLINFERNO;
	int K_DELAY_WL_COMET;
	int K_DELAY_WL_CHAINLIGHTNING;
	int K_DELAY_WL_CHAINLIGHTNING_ATK;
	int K_DELAY_WL_EARTHSTRAIN;
	int K_DELAY_WL_TETRAVORTEX;
	int K_DELAY_WL_TETRAVORTEX_FIRE;
	int K_DELAY_WL_TETRAVORTEX_WATER;
	int K_DELAY_WL_TETRAVORTEX_WIND;
	int K_DELAY_WL_TETRAVORTEX_GROUND;
	int K_DELAY_WL_RELEASE;
	int K_DELAY_WL_READING_SB;
	int K_DELAY_WL_FREEZE_SP;
	int K_DELAY_RA_ARROWSTORM;
	int K_DELAY_RA_AIMEDBOLT;
	int K_DELAY_RA_WUGSTRIKE;
	int K_DELAY_RA_WUGBITE;
	int K_DELAY_NC_BOOSTKNUCKLE;
	int K_DELAY_NC_PILEBUNKER;
	int K_DELAY_NC_VULCANARM;
	int K_DELAY_NC_FLAMELAUNCHER;
	int K_DELAY_NC_COLDSLOWER;
	int K_DELAY_NC_ARMSCANNON;
	int K_DELAY_NC_ACCELERATION;
	int K_DELAY_NC_F_SIDESLIDE;
	int K_DELAY_NC_B_SIDESLIDE;
	int K_DELAY_NC_MAINFRAME;
	int K_DELAY_NC_SHAPESHIFT;
	int K_DELAY_NC_INFRAREDSCAN;
	int K_DELAY_NC_ANALYZE;
	int K_DELAY_NC_MAGNETICFIELD;
	int K_DELAY_NC_NEUTRALBARRIER;
	int K_DELAY_NC_STEALTHFIELD;
	int K_DELAY_NC_AXEBOOMERANG;
	int K_DELAY_NC_POWERSWING;
	int K_DELAY_NC_AXETORNADO;
	int K_DELAY_NC_SILVERSNIPER;
	int K_DELAY_NC_MAGICDECOY;
	int K_DELAY_NC_DISJOINT;
	int K_DELAY_SC_FATALMENACE;
	int K_DELAY_SC_TRIANGLESHOT;
	int K_DELAY_SC_INVISIBILITY;
	int K_DELAY_SC_ENERVATION;
	int K_DELAY_SC_GROOMY;
	int K_DELAY_SC_IGNORANCE;
	int K_DELAY_SC_LAZINESS;
	int K_DELAY_SC_UNLUCKY;
	int K_DELAY_SC_WEAKNESS;
	int K_DELAY_SC_STRIPACCESSARY;
	int K_DELAY_SC_MANHOLE;
	int K_DELAY_SC_DIMENSIONDOOR;
	int K_DELAY_SC_CHAOSPANIC;
	int K_DELAY_SC_MAELSTROM;
	int K_DELAY_SC_BLOODYLUST;
	int K_DELAY_SC_FEINTBOMB;
	int K_DELAY_LG_CANNONSPEAR;
	int K_DELAY_LG_BANISHINGPOINT;
	int K_DELAY_LG_TRAMPLE;
	int K_DELAY_LG_PINPOINTATTACK;
	int K_DELAY_LG_RAGEBURST;
	int K_DELAY_LG_EXEEDBREAK;
	int K_DELAY_LG_OVERBRAND;
	int K_DELAY_LG_BANDING;
	int K_DELAY_LG_MOONSLASHER;
	int K_DELAY_LG_RAYOFGENESIS;
	int K_DELAY_LG_PIETY;
	int K_DELAY_LG_EARTHDRIVE;
	int K_DELAY_LG_HESPERUSLIT;
	int K_DELAY_SR_DRAGONCOMBO;
	int K_DELAY_SR_SKYNETBLOW;
	int K_DELAY_SR_EARTHSHAKER;
	int K_DELAY_SR_FALLENEMPIRE;
	int K_DELAY_SR_TIGERCANNON;
	int K_DELAY_SR_HELLGATE;
	int K_DELAY_SR_RAMPAGEBLASTER;
	int K_DELAY_SR_CRESCENTELBOW;
	int K_DELAY_SR_CURSEDCIRCLE;
	int K_DELAY_SR_LIGHTNINGWALK;
	int K_DELAY_SR_KNUCKLEARROW;
	int K_DELAY_SR_WINDMILL;
	int K_DELAY_SR_RAISINGDRAGON;
	int K_DELAY_SR_GENTLETOUCH;
	int K_DELAY_SR_ASSIMILATEPOWER;
	int K_DELAY_SR_POWERVELOCITY;
	int K_DELAY_SR_CRESBOW_AUTO;
	int K_DELAY_SR_GATEOFHELL;
	int K_DELAY_SR_GENTLE_QUIET;
	int K_DELAY_SR_GENTLE_CURE;
	int K_DELAY_SR_GENTLE_EGAIN;
	int K_DELAY_SR_GENTLE_CHANGE;
	int K_DELAY_SR_GENTLE_REVIT;
	int K_DELAY_WA_SWING_DANCE;
	int K_DELAY_WA_SYMPHONY;
	int K_DELAY_WA_MOONLIT;
	int K_DELAY_MI_RUSH_WINDMILL;
	int K_DELAY_MI_ECHOSONG;
	int K_DELAY_MI_HARMONIZE;
	int K_DELAY_WM_LESSON;
	int K_DELAY_WM_METALICSOUND;
	int K_DELAY_WM_REVERBERATION;
	int K_DELAY_WM_REVERB_MELEE;
	int K_DELAY_WM_REVERB_MAGIC;
	int K_DELAY_WM_DOMINION;
	int K_DELAY_WM_RAINSTORM;
	int K_DELAY_WM_RAINSTORM_;
	int K_DELAY_WM_POEM;
	int K_DELAY_WM_VOICEOFSIREN;
	int K_DELAY_WM_DEADHILLHERE;
	int K_DELAY_WM_LULLABY_DEEPSLEEP;
	int K_DELAY_WM_SIRCLEOFNATURE;
	int K_DELAY_WM_RANDOMIZESPELL;
	int K_DELAY_WM_GLOOMYDAY;
	int K_DELAY_WM_GREAT_ECHO;
	int K_DELAY_WM_SONG_OF_MANA;
	int K_DELAY_WM_DANCE_WITH_WUG;
	int K_DELAY_WM_DESTRUCTION;
	int K_DELAY_WM_SATNIGHT_FEVER;
	int K_DELAY_WM_LERADS_DEW;
	int K_DELAY_WM_MELODYOFSINK;
	int K_DELAY_WM_BEYOND_OF_WARCRY;
	int K_DELAY_WM_HUMMING_VOICE;
	int K_DELAY_SO_FIREWALK;
	int K_DELAY_SO_ELECTRICWALK;
	int K_DELAY_SO_SPELLFIST;
	int K_DELAY_SO_EARTHGRAVE;
	int K_DELAY_SO_DIAMONDDUST;
	int K_DELAY_SO_POISON_BUSTER;
	int K_DELAY_SO_PSYCHIC_WAVE;
	int K_DELAY_SO_CLOUD_KILL;
	int K_DELAY_SO_STRIKING;
	int K_DELAY_SO_WARMER;
	int K_DELAY_SO_VACUUM_EXTREME;
	int K_DELAY_SO_VARETYR_SPEAR;
	int K_DELAY_SO_ARRULLO;
	int K_DELAY_SO_EL_CONTROL;
	int K_DELAY_SO_EL_ACTION;
	int K_DELAY_SO_EL_ANALYSIS;
	int K_DELAY_SO_EL_SYMPATHY;
	int K_DELAY_SO_EL_CURE;
	int K_DELAY_GN_CART_TORNADO;
	int K_DELAY_GN_CARTCANNON;
	int K_DELAY_GN_THORNS_TRAP;
	int K_DELAY_GN_BLOOD_SUCKER;
	int K_DELAY_GN_SPORE_EXPLOSION;
	int K_DELAY_GN_WALLOFTHORN;
	int K_DELAY_GN_CRAZYWEED;
	int K_DELAY_GN_CRAZYWEED_;
	int K_DELAY_GN_DEMONIC_FIRE;
	int K_DELAY_GN_FIRE_EXPAN;
	int K_DELAY_GN_FIRE_EXPAN_SMOKE;
	int K_DELAY_GN_FIRE_EXPAN_TEAR;
	int K_DELAY_GN_FIRE_EXPAN_ACID;
	int K_DELAY_GN_HELLS_PLANT;
	int K_DELAY_GN_HELLS_PLANT_;
	int K_DELAY_GN_MANDRAGORA;
	int K_DELAY_GN_SLINGITEM;
	int K_DELAY_GN_CHANGEMATERIAL;
	int K_DELAY_GN_SLING_RANGEMELEE;
	int K_DELAY_AB_SECRAMENT;
	int K_DELAY_SR_HOWLINGOFLION;
	int K_DELAY_SR_RIDEINLIGHTNING;
	int K_DELAY_LG_OVERBRAND_BRANDISH;
	int K_DELAY_LG_OVERBRAND_PLUSATK;
	int K_DELAY_RL_GLITTERING_GREED;
	int K_DELAY_RL_RICHS_COIN;
	int K_DELAY_RL_MASS_SPIRAL;
	int K_DELAY_RL_BANISHING_BUSTER;
	int K_DELAY_RL_B_TRAP;
	int K_DELAY_RL_S_STORM;
	int K_DELAY_RL_E_CHAIN;
	int K_DELAY_RL_QD_SHOT;
	int K_DELAY_RL_C_MARKER;
	int K_DELAY_RL_FIREDANCE;
	int K_DELAY_RL_H_MINE;
	int K_DELAY_RL_P_ALTER;
	int K_DELAY_RL_FALLEN_ANGEL;
	int K_DELAY_RL_R_TRIP;
	int K_DELAY_RL_D_TAIL;
	int K_DELAY_RL_FIRE_RAIN;
	int K_DELAY_RL_HEAT_BARREL;
	int K_DELAY_RL_AM_BLAST;
	int K_DELAY_RL_SLUGSHOT;
	int K_DELAY_RL_HAMMER_OF_GOD;
	int K_DELAY_RL_R_TRIP_PLUSATK;
	int K_DELAY_RL_B_FLICKER_ATK;
	int K_DELAY_RL_GLITTERING_ATK;
	int K_DELAY_KO_YAMIKUMO;
	int K_DELAY_KO_JYUMONJIKIRI;
	int K_DELAY_KO_SETSUDAN;
	int K_DELAY_KO_BAKURETSU;
	int K_DELAY_KO_HAPPOKUNAI;
	int K_DELAY_KO_MUCHANAGE;
	int K_DELAY_KO_HUUMARANKA;
	int K_DELAY_KO_MAKIBISHI;
	int K_DELAY_KO_MEIKYOUSISUI;
	int K_DELAY_KO_ZANZOU;
	int K_DELAY_KO_KYOUGAKU;
	int K_DELAY_KO_JYUSATSU;
	int K_DELAY_KO_KAHU_ENTEN;
	int K_DELAY_KO_HYOUHU_HUBUKI;
	int K_DELAY_KO_KAZEHU_SEIRAN;
	int K_DELAY_KO_DOHU_KOUKAI;
	int K_DELAY_KO_KAIHOU;
	int K_DELAY_KO_ZENKAI;
	int K_DELAY_KO_GENWAKU;
	int K_DELAY_KO_IZAYOI;
	int K_DELAY_KG_KAGEHUMI;
	int K_DELAY_KG_KYOMU;
	int K_DELAY_KG_KAGEMUSYA;
	int K_DELAY_OB_ZANGETSU;
	int K_DELAY_OB_OBOROGENSOU;
	int K_DELAY_OB_OBOROGENSOU_;
	int K_DELAY_OB_AKAITSUKI;
	int K_DELAY_GC_DARKCROW;
	int K_DELAY_RA_UNLIMIT;
	int K_DELAY_GN_ILLUSIONDOPING;
	int K_DELAY_RK_DRAGONBREATH_W;
	int K_DELAY_RK_LUXANIMA;
	int K_DELAY_NC_MAGMA_ERUPTION;
	int K_DELAY_WM_FRIGG_SONG;
	int K_DELAY_SO_ELESHIELD;
	int K_DELAY_SR_FLASHCOMBO;
	int K_DELAY_SC_ESCAPE;
	int K_DELAY_AB_OFFERTORIUM;
	int K_DELAY_WL_TELEK_INTENSE;
	int K_DELAY_ALL_FULL_THROTTLE;
	int K_DELAY_SU_BITE;
	int K_DELAY_SU_SCRATCH;
	int K_DELAY_SU_STOOP;
	int K_DELAY_SU_LOPE;
	int K_DELAY_SU_SPRITEMABLE;
	int K_DELAY_SU_POWEROFLAND;
	int K_DELAY_SU_SV_STEMSPEAR;
	int K_DELAY_SU_CN_POWDERING;
	int K_DELAY_SU_CN_METEOR;
	int K_DELAY_SU_SV_ROOTTWIST;
	int K_DELAY_SU_SV_ROOTTWIST_;
	int K_DELAY_SU_POWEROFLIFE;
	int K_DELAY_SU_SCAROFTAROU;
	int K_DELAY_SU_PICKYPECK;
	int K_DELAY_SU_PICKYPECK_;
	int K_DELAY_SU_ARCLOUSEDASH;
	int K_DELAY_SU_CARROTBEAT;
	int K_DELAY_SU_POWEROFSEA;
	int K_DELAY_SU_TUNABELLY;
	int K_DELAY_SU_TUNAPARTY;
	int K_DELAY_SU_BUNCHOFSHRIMP;
	int K_DELAY_SU_FRESHSHRIMP;

	// WoE Keitenai skill delay
	int WOE_K_SHOW_DELAY;

	int WOE_K_DELAY_SM_BASH;
	int WOE_K_DELAY_SM_MAGNUM;
	int WOE_K_DELAY_MG_NAPALMBEAT;
	int WOE_K_DELAY_MG_SOULSTRIKE;
	int WOE_K_DELAY_MG_COLDBOLT;
	int WOE_K_DELAY_MG_FROSTDIVER;
	int WOE_K_DELAY_MG_STONECURSE;
	int WOE_K_DELAY_MG_FIREBALL;
	int WOE_K_DELAY_MG_FIREWALL;
	int WOE_K_DELAY_MG_FIREBOLT;
	int WOE_K_DELAY_MG_LIGHTBOLT;
	int WOE_K_DELAY_MG_THUNDERSTORM;
	int WOE_K_DELAY_AL_HEAL;
	int WOE_K_DELAY_AL_DECAGI;
	int WOE_K_DELAY_AL_CRUCIS;
	int WOE_K_DELAY_MC_MAMMONITE;
	int WOE_K_DELAY_AC_DOUBLE;
	int WOE_K_DELAY_AC_SHOWER;
	int WOE_K_DELAY_TF_POISON;
	int WOE_K_DELAY_KN_PIERCE;
	int WOE_K_DELAY_KN_BRANDISHSPEAR;
	int WOE_K_DELAY_KN_SPEARSTAB;
	int WOE_K_DELAY_KN_SPEARBOOMER;
	int WOE_K_DELAY_KN_BOWLINGBASH;
	int WOE_K_DELAY_PR_LEXDIVINA;
	int WOE_K_DELAY_PR_TURNUNDEAD;
	int WOE_K_DELAY_PR_LEXAETERNA;
	int WOE_K_DELAY_PR_MAGNUS;
	int WOE_K_DELAY_WZ_FIREPILLAR;
	int WOE_K_DELAY_WZ_SIGHTRASHER;
	int WOE_K_DELAY_WZ_FIREIVY;
	int WOE_K_DELAY_WZ_METEOR;
	int WOE_K_DELAY_WZ_JUPITEL;
	int WOE_K_DELAY_WZ_VERMILION;
	int WOE_K_DELAY_WZ_WATERBALL;
	int WOE_K_DELAY_WZ_ICEWALL;
	int WOE_K_DELAY_WZ_FROSTNOVA;
	int WOE_K_DELAY_WZ_STORMGUST;
	int WOE_K_DELAY_WZ_EARTHSPIKE;
	int WOE_K_DELAY_WZ_HEAVENDRIVE;
	int WOE_K_DELAY_WZ_QUAGMIRE;
	int WOE_K_DELAY_WZ_ESTIMATION;
	int WOE_K_DELAY_BS_HAMMERFALL;
	int WOE_K_DELAY_HT_BLITZBEAT;
	int WOE_K_DELAY_AS_SONICBLOW;
	int WOE_K_DELAY_AS_GRIMTOOTH;
	int WOE_K_DELAY_AC_CHARGEARROW;
	int WOE_K_DELAY_TF_BACKSLIDING;
	int WOE_K_DELAY_MC_CARTREVO;
	int WOE_K_DELAY_AL_HOLYLIGHT;
	int WOE_K_DELAY_RG_BACKSTAP;
	int WOE_K_DELAY_RG_RAID;
	int WOE_K_DELAY_RG_GRAFFITI;
	int WOE_K_DELAY_RG_FLAGGRAFFITI;
	int WOE_K_DELAY_RG_COMPULSION;
	int WOE_K_DELAY_RG_PLAGIARISM;
	int WOE_K_DELAY_AM_DEMONSTRATION;
	int WOE_K_DELAY_AM_ACIDTERROR;
	int WOE_K_DELAY_AM_POTIONPITCH;
	int WOE_K_DELAY_AM_CANNIBALIZE;
	int WOE_K_DELAY_AM_SPHEREMINE;
	int WOE_K_DELAY_AM_FLAMECONTROL;
	int WOE_K_DELAY_AM_DRILLMASTER;
	int WOE_K_DELAY_CR_TRUST;
	int WOE_K_DELAY_CR_SHLDBOOMRANG;
	int WOE_K_DELAY_CR_HOLYCROSS;
	int WOE_K_DELAY_CR_GRANDCROSS;
	int WOE_K_DELAY_MO_CALLSPIRITS;
	int WOE_K_DELAY_MO_ABSORBSPIRITS;
	int WOE_K_DELAY_MO_BODYRELOC;
	int WOE_K_DELAY_MO_INVESTIGATE;
	int WOE_K_DELAY_MO_FINGEROFFENSE;
	int WOE_K_DELAY_MO_EXPLOSIONSPIRIT;
	int WOE_K_DELAY_MO_EXTREMITYFIST;
	int WOE_K_DELAY_MO_CHAINCOMBO;
	int WOE_K_DELAY_MO_COMBOFINISH;
	int WOE_K_DELAY_SA_CASTCANCEL;
	int WOE_K_DELAY_SA_SPELLBREAKER;
	int WOE_K_DELAY_SA_DISPELL;
	int WOE_K_DELAY_SA_ABRACADABRA;
	int WOE_K_DELAY_SA_MONOCELL;
	int WOE_K_DELAY_SA_CLASSCHANGE;
	int WOE_K_DELAY_SA_SUMMONMONSTER;
	int WOE_K_DELAY_SA_REVERSEORCISH;
	int WOE_K_DELAY_SA_DEATH;
	int WOE_K_DELAY_SA_FORTUNE;
	int WOE_K_DELAY_SA_TAMINGMONSTER;
	int WOE_K_DELAY_SA_QUESTION;
	int WOE_K_DELAY_SA_GRAVITY;
	int WOE_K_DELAY_SA_LEVELUP;
	int WOE_K_DELAY_SA_INSTANTDEATH;
	int WOE_K_DELAY_SA_FULLRECOVERY;
	int WOE_K_DELAY_SA_COMA;
	int WOE_K_DELAY_BD_ADAPTATION;
	int WOE_K_DELAY_BD_ENCORE;
	int WOE_K_DELAY_BD_LULLABY;
	int WOE_K_DELAY_BD_RICHMANKIM;
	int WOE_K_DELAY_BA_MUSICALSTRIKE;
	int WOE_K_DELAY_BA_DISSONANCE;
	int WOE_K_DELAY_BA_FROSTJOKER;
	int WOE_K_DELAY_BA_WHISTLE;
	int WOE_K_DELAY_BA_ASSASSINCROSS;
	int WOE_K_DELAY_BA_POEMBRAGI;
	int WOE_K_DELAY_BA_APPLEIDUN;
	int WOE_K_DELAY_DC_THROWARROW;
	int WOE_K_DELAY_DC_UGLYDANCE;
	int WOE_K_DELAY_DC_SCREAM;
	int WOE_K_DELAY_DC_HUMMING;
	int WOE_K_DELAY_DC_DONTFORGETME;
	int WOE_K_DELAY_DC_FORTUNEKISS;
	int WOE_K_DELAY_DC_SERVICEFORYOU;
	int WOE_K_DELAY_LK_FURY;
	int WOE_K_DELAY_HW_MAGICCRASHER;
	int WOE_K_DELAY_PA_PRESSURE;
	int WOE_K_DELAY_CH_PALMSTRIKE;
	int WOE_K_DELAY_CH_TIGERFIST;
	int WOE_K_DELAY_CH_CHAINCRUSH;
	int WOE_K_DELAY_PF_SOULCHANGE;
	int WOE_K_DELAY_PF_SOULBURN;
	int WOE_K_DELAY_ASC_BREAKER;
	int WOE_K_DELAY_SN_FALCONASSAULT;
	int WOE_K_DELAY_SN_SHARPSHOOTING;
	int WOE_K_DELAY_CR_ALCHEMY;
	int WOE_K_DELAY_CR_SYNTHESIS;
	int WOE_K_DELAY_CG_ARROWVULCAN;
	int WOE_K_DELAY_CG_MOONLIT;
	int WOE_K_DELAY_CG_MARIONETTE;
	int WOE_K_DELAY_LK_SPIRALPIERCE;
	int WOE_K_DELAY_LK_HEADCRUSH;
	int WOE_K_DELAY_LK_JOINTBEAT;
	int WOE_K_DELAY_HW_NAPALMVULCAN;
	int WOE_K_DELAY_CH_SOULCOLLECT;
	int WOE_K_DELAY_PF_MINDBREAKER;
	int WOE_K_DELAY_PF_SPIDERWEB;
	int WOE_K_DELAY_ASC_METEORASSAULT;
	int WOE_K_DELAY_TK_STORMKICK;
	int WOE_K_DELAY_TK_DOWNKICK;
	int WOE_K_DELAY_TK_TURNKICK;
	int WOE_K_DELAY_TK_JUMPKICK;
	int WOE_K_DELAY_TK_POWER;
	int WOE_K_DELAY_TK_HIGHJUMP;
	int WOE_K_DELAY_SL_KAIZEL;
	int WOE_K_DELAY_SL_KAAHI;
	int WOE_K_DELAY_SL_KAUPE;
	int WOE_K_DELAY_SL_KAITE;
	int WOE_K_DELAY_SL_KAINA;
	int WOE_K_DELAY_SL_STIN;
	int WOE_K_DELAY_SL_STUN;
	int WOE_K_DELAY_SL_SMA;
	int WOE_K_DELAY_SL_SWOO;
	int WOE_K_DELAY_SL_SKE;
	int WOE_K_DELAY_SL_SKA;
	int WOE_K_DELAY_ST_FULLSTRIP;
	int WOE_K_DELAY_CR_SLIMPITCHER;
	int WOE_K_DELAY_CR_FULLPROTECTION;
	int WOE_K_DELAY_PA_SHIELDCHAIN;
	int WOE_K_DELAY_HP_MANARECHARGE;
	int WOE_K_DELAY_PF_DOUBLECASTING;
	int WOE_K_DELAY_HW_GANBANTEIN;
	int WOE_K_DELAY_HW_GRAVITATION;
	int WOE_K_DELAY_WS_CARTTERMINATION;
	int WOE_K_DELAY_CG_HERMODE;
	int WOE_K_DELAY_CG_TAROTCARD;
	int WOE_K_DELAY_CR_ACIDDEMO;
	int WOE_K_DELAY_SL_HIGH;
	int WOE_K_DELAY_HT_POWER;
	int WOE_K_DELAY_GS_TRIPLEACTION;
	int WOE_K_DELAY_GS_BULLSEYE;
	int WOE_K_DELAY_GS_MADNESSCANCEL;
	int WOE_K_DELAY_GS_INCREASING;
	int WOE_K_DELAY_GS_MAGICALBULLET;
	int WOE_K_DELAY_GS_CRACKER;
	int WOE_K_DELAY_GS_SINGLEACTION;
	int WOE_K_DELAY_GS_CHAINACTION;
	int WOE_K_DELAY_GS_TRACKING;
	int WOE_K_DELAY_GS_DISARM;
	int WOE_K_DELAY_GS_PIERCINGSHOT;
	int WOE_K_DELAY_GS_RAPIDSHOWER;
	int WOE_K_DELAY_GS_DESPERADO;
	int WOE_K_DELAY_GS_GATLINGFEVER;
	int WOE_K_DELAY_GS_DUST;
	int WOE_K_DELAY_GS_FULLBUSTER;
	int WOE_K_DELAY_GS_SPREADATTACK;
	int WOE_K_DELAY_GS_GROUNDDRIFT;
	int WOE_K_DELAY_NJ_TOBIDOUGU;
	int WOE_K_DELAY_NJ_SYURIKEN;
	int WOE_K_DELAY_NJ_KUNAI;
	int WOE_K_DELAY_NJ_HUUMA;
	int WOE_K_DELAY_NJ_ZENYNAGE;
	int WOE_K_DELAY_NJ_TATAMIGAESHI;
	int WOE_K_DELAY_NJ_KASUMIKIRI;
	int WOE_K_DELAY_NJ_SHADOWJUMP;
	int WOE_K_DELAY_NJ_KIRIKAGE;
	int WOE_K_DELAY_NJ_UTSUSEMI;
	int WOE_K_DELAY_NJ_BUNSINJYUTSU;
	int WOE_K_DELAY_NJ_NINPOU;
	int WOE_K_DELAY_NJ_KOUENKA;
	int WOE_K_DELAY_NJ_KAENSIN;
	int WOE_K_DELAY_NJ_BAKUENRYU;
	int WOE_K_DELAY_NJ_HYOUSENSOU;
	int WOE_K_DELAY_NJ_SUITON;
	int WOE_K_DELAY_NJ_HYOUSYOURAKU;
	int WOE_K_DELAY_NJ_HUUJIN;
	int WOE_K_DELAY_NJ_RAIGEKISAI;
	int WOE_K_DELAY_NJ_KAMAITACHI;
	int WOE_K_DELAY_NJ_NEN;
	int WOE_K_DELAY_NJ_ISSEN;
	int WOE_K_DELAY_KN_CHARGEATK;
	int WOE_K_DELAY_AS_VENOMKNIFE;
	int WOE_K_DELAY_RG_CLOSECONFINE;
	int WOE_K_DELAY_WZ_SIGHTBLASTER;
	int WOE_K_DELAY_HT_PHANTASMIC;
	int WOE_K_DELAY_BA_PANGVOICE;
	int WOE_K_DELAY_DC_WINKCHARM;
	int WOE_K_DELAY_PR_REDEMPTIO;
	int WOE_K_DELAY_MO_KITRANSLATION;
	int WOE_K_DELAY_MO_BALKYOUNG;
	int WOE_K_DELAY_RK_SONICWAVE;
	int WOE_K_DELAY_RK_DEATHBOUND;
	int WOE_K_DELAY_RK_HUNDREDSPEAR;
	int WOE_K_DELAY_RK_WINDCUTTER;
	int WOE_K_DELAY_RK_IGNITIONBREAK;
	int WOE_K_DELAY_RK_DRAGONBREATH;
	int WOE_K_DELAY_RK_CRUSHSTRIKE;
	int WOE_K_DELAY_RK_STORMBLAST;
	int WOE_K_DELAY_RK_PHANTOMTHRUST;
	int WOE_K_DELAY_GC_CROSSIMPACT;
	int WOE_K_DELAY_GC_WEAPONCRUSH;
	int WOE_K_DELAY_GC_ROLLINGCUTTER;
	int WOE_K_DELAY_GC_CROSSSLASHER;
	int WOE_K_DELAY_AB_JUDEX;
	int WOE_K_DELAY_AB_ADORAMUS;
	int WOE_K_DELAY_AB_CHEAL;
	int WOE_K_DELAY_AB_EPICLESIS;
	int WOE_K_DELAY_AB_PRAEFATIO;
	int WOE_K_DELAY_AB_EUCHARISTICA;
	int WOE_K_DELAY_AB_RENOVATIO;
	int WOE_K_DELAY_AB_HIGHNESSHEAL;
	int WOE_K_DELAY_AB_CLEARANCE;
	int WOE_K_DELAY_AB_EXPIATIO;
	int WOE_K_DELAY_AB_DUPLITE;
	int WOE_K_DELAY_AB_DUPLITE_MELEE;
	int WOE_K_DELAY_AB_DUPLITE_MAGIC;
	int WOE_K_DELAY_AB_SILENTIUM;
	int WOE_K_DELAY_WL_WHITEIMPRISON;
	int WOE_K_DELAY_WL_SOULEXPANSION;
	int WOE_K_DELAY_WL_FROSTMISTY;
	int WOE_K_DELAY_WL_JACKFROST;
	int WOE_K_DELAY_WL_MARSHOFABYSS;
	int WOE_K_DELAY_WL_RADIUS;
	int WOE_K_DELAY_WL_STASIS;
	int WOE_K_DELAY_WL_DRAINLIFE;
	int WOE_K_DELAY_WL_CRIMSONROCK;
	int WOE_K_DELAY_WL_HELLINFERNO;
	int WOE_K_DELAY_WL_COMET;
	int WOE_K_DELAY_WL_CHAIN;
	int WOE_K_DELAY_WL_CHAIN_;
	int WOE_K_DELAY_WL_EARTHSTRAIN;
	int WOE_K_DELAY_WL_TETRA;
	int WOE_K_DELAY_WL_TETRA_FIRE;
	int WOE_K_DELAY_WL_TETRA_WATER;
	int WOE_K_DELAY_WL_TETRA_WIND;
	int WOE_K_DELAY_WL_TETRA_GROUND;
	int WOE_K_DELAY_WL_RELEASE;
	int WOE_K_DELAY_WL_READING_SB;
	int WOE_K_DELAY_WL_FREEZE_SP;
	int WOE_K_DELAY_RA_ARROWSTORM;
	int WOE_K_DELAY_RA_AIMEDBOLT;
	int WOE_K_DELAY_RA_WUGSTRIKE;
	int WOE_K_DELAY_RA_WUGBITE;
	int WOE_K_DELAY_NC_BOOSTKNUCKLE;
	int WOE_K_DELAY_NC_PILEBUNKER;
	int WOE_K_DELAY_NC_VULCANARM;
	int WOE_K_DELAY_NC_FLAMELAUNCHER;
	int WOE_K_DELAY_NC_COLDSLOWER;
	int WOE_K_DELAY_NC_ARMSCANNON;
	int WOE_K_DELAY_NC_ACCELERATION;
	int WOE_K_DELAY_NC_F_SIDESLIDE;
	int WOE_K_DELAY_NC_B_SIDESLIDE;
	int WOE_K_DELAY_NC_MAINFRAME;
	int WOE_K_DELAY_NC_SHAPESHIFT;
	int WOE_K_DELAY_NC_INFRAREDSCAN;
	int WOE_K_DELAY_NC_ANALYZE;
	int WOE_K_DELAY_NC_MAGNETICFIELD;
	int WOE_K_DELAY_NC_NEUTRALBARRIER;
	int WOE_K_DELAY_NC_STEALTHFIELD;
	int WOE_K_DELAY_NC_AXEBOOMERANG;
	int WOE_K_DELAY_NC_POWERSWING;
	int WOE_K_DELAY_NC_AXETORNADO;
	int WOE_K_DELAY_NC_SILVERSNIPER;
	int WOE_K_DELAY_NC_MAGICDECOY;
	int WOE_K_DELAY_NC_DISJOINT;
	int WOE_K_DELAY_SC_FATALMENACE;
	int WOE_K_DELAY_SC_TRIANGLESHOT;
	int WOE_K_DELAY_SC_INVISIBILITY;
	int WOE_K_DELAY_SC_ENERVATION;
	int WOE_K_DELAY_SC_GROOMY;
	int WOE_K_DELAY_SC_IGNORANCE;
	int WOE_K_DELAY_SC_LAZINESS;
	int WOE_K_DELAY_SC_UNLUCKY;
	int WOE_K_DELAY_SC_WEAKNESS;
	int WOE_K_DELAY_SC_STRIPACCESSARY;
	int WOE_K_DELAY_SC_MANHOLE;
	int WOE_K_DELAY_SC_DIMENSIONDOOR;
	int WOE_K_DELAY_SC_CHAOSPANIC;
	int WOE_K_DELAY_SC_MAELSTROM;
	int WOE_K_DELAY_SC_BLOODYLUST;
	int WOE_K_DELAY_SC_FEINTBOMB;
	int WOE_K_DELAY_LG_CANNONSPEAR;
	int WOE_K_DELAY_LG_BANISHINGPOINT;
	int WOE_K_DELAY_LG_TRAMPLE;
	int WOE_K_DELAY_LG_PINPOINTATTACK;
	int WOE_K_DELAY_LG_RAGEBURST;
	int WOE_K_DELAY_LG_EXEEDBREAK;
	int WOE_K_DELAY_LG_OVERBRAND;
	int WOE_K_DELAY_LG_BANDING;
	int WOE_K_DELAY_LG_MOONSLASHER;
	int WOE_K_DELAY_LG_RAYOFGENESIS;
	int WOE_K_DELAY_LG_PIETY;
	int WOE_K_DELAY_LG_EARTHDRIVE;
	int WOE_K_DELAY_LG_HESPERUSLIT;
	int WOE_K_DELAY_SR_DRAGONCOMBO;
	int WOE_K_DELAY_SR_SKYNETBLOW;
	int WOE_K_DELAY_SR_EARTHSHAKER;
	int WOE_K_DELAY_SR_FALLENEMPIRE;
	int WOE_K_DELAY_SR_TIGERCANNON;
	int WOE_K_DELAY_SR_HELLGATE;
	int WOE_K_DELAY_SR_RAMPAGEBLASTER;
	int WOE_K_DELAY_SR_CRESCENTELBOW;
	int WOE_K_DELAY_SR_CURSEDCIRCLE;
	int WOE_K_DELAY_SR_LIGHTNINGWALK;
	int WOE_K_DELAY_SR_KNUCKLEARROW;
	int WOE_K_DELAY_SR_WINDMILL;
	int WOE_K_DELAY_SR_RAISINGDRAGON;
	int WOE_K_DELAY_SR_GENTLETOUCH;
	int WOE_K_DELAY_SR_ASSIMILATEPOWER;
	int WOE_K_DELAY_SR_POWERVELOCITY;
	int WOE_K_DELAY_SR_CRESBOW_AUTO;
	int WOE_K_DELAY_SR_GATEOFHELL;
	int WOE_K_DELAY_SR_GENTLE_QUIET;
	int WOE_K_DELAY_SR_GENTLE_CURE;
	int WOE_K_DELAY_SR_GENTLE_EGAIN;
	int WOE_K_DELAY_SR_GENTLE_CHANGE;
	int WOE_K_DELAY_SR_GENTLE_REVIT;
	int WOE_K_DELAY_WA_SWING_DANCE;
	int WOE_K_DELAY_WA_SYMPHONY;
	int WOE_K_DELAY_WA_MOONLIT;
	int WOE_K_DELAY_MI_WINDMILL;
	int WOE_K_DELAY_MI_ECHOSONG;
	int WOE_K_DELAY_MI_HARMONIZE;
	int WOE_K_DELAY_WM_LESSON;
	int WOE_K_DELAY_WM_METALICSOUND;
	int WOE_K_DELAY_WM_REVERB;
	int WOE_K_DELAY_WM_REVERB_MELEE;
	int WOE_K_DELAY_WM_REVERB_MAGIC;
	int WOE_K_DELAY_WM_DOMINION;
	int WOE_K_DELAY_WM_RAINSTORM;
	int WOE_K_DELAY_WM_RAINSTORM_;
	int WOE_K_DELAY_WM_POEM;
	int WOE_K_DELAY_WM_VOICE;
	int WOE_K_DELAY_WM_HILLHERE;
	int WOE_K_DELAY_WM_DEEPSLEEP;
	int WOE_K_DELAY_WM_SIRCLE;
	int WOE_K_DELAY_WM_RANDOMIZE;
	int WOE_K_DELAY_WM_GLOOMYDAY;
	int WOE_K_DELAY_WM_GREAT_ECHO;
	int WOE_K_DELAY_WM_SONG_OF_MANA;
	int WOE_K_DELAY_WM_DANCEWUG;
	int WOE_K_DELAY_WM_DESTRUCTION;
	int WOE_K_DELAY_WM_SATFEVER;
	int WOE_K_DELAY_WM_LERADS_DEW;
	int WOE_K_DELAY_WM_MELODYOFSINK;
	int WOE_K_DELAY_WM_WARCRY;
	int WOE_K_DELAY_WM_HUMMING;
	int WOE_K_DELAY_SO_FIREWALK;
	int WOE_K_DELAY_SO_ELECTRICWALK;
	int WOE_K_DELAY_SO_SPELLFIST;
	int WOE_K_DELAY_SO_EARTHGRAVE;
	int WOE_K_DELAY_SO_DIAMONDDUST;
	int WOE_K_DELAY_SO_POISON_BUSTER;
	int WOE_K_DELAY_SO_PSYCHIC_WAVE;
	int WOE_K_DELAY_SO_CLOUD_KILL;
	int WOE_K_DELAY_SO_STRIKING;
	int WOE_K_DELAY_SO_WARMER;
	int WOE_K_DELAY_SO_VACUUM_EXTREME;
	int WOE_K_DELAY_SO_VARETYR_SPEAR;
	int WOE_K_DELAY_SO_ARRULLO;
	int WOE_K_DELAY_SO_EL_CONTROL;
	int WOE_K_DELAY_SO_EL_ACTION;
	int WOE_K_DELAY_SO_EL_ANALYSIS;
	int WOE_K_DELAY_SO_EL_SYMPATHY;
	int WOE_K_DELAY_SO_EL_CURE;
	int WOE_K_DELAY_GN_CART_TORNADO;
	int WOE_K_DELAY_GN_CARTCANNON;
	int WOE_K_DELAY_GN_THORNS_TRAP;
	int WOE_K_DELAY_GN_BLOOD_SUCKER;
	int WOE_K_DELAY_GN_SPORE_EXPLO;
	int WOE_K_DELAY_GN_WALLOFTHORN;
	int WOE_K_DELAY_GN_CRAZYWEED;
	int WOE_K_DELAY_GN_CRAZYWEED_;
	int WOE_K_DELAY_GN_DEMONIC_FIRE;
	int WOE_K_DELAY_GN_FIRE_EXP;
	int WOE_K_DELAY_GN_FIRE_EXPSMOKE;
	int WOE_K_DELAY_GN_FIRE_EXPTEAR;
	int WOE_K_DELAY_GN_FIRE_EXPACID;
	int WOE_K_DELAY_GN_HELLSPLANT;
	int WOE_K_DELAY_GN_HELLSPLANT_;
	int WOE_K_DELAY_GN_MANDRAGORA;
	int WOE_K_DELAY_GN_SLINGITEM;
	int WOE_K_DELAY_GN_SLINGITEM_;
	int WOE_K_DELAY_GN_CHANGEMATERIAL;
	int WOE_K_DELAY_AB_SECRAMENT;
	int WOE_K_DELAY_SR_HOWLINGOFLION;
	int WOE_K_DELAY_SR_RIDEINLIGHTNING;
	int WOE_K_DELAY_LG_OVERBRANDISH;
	int WOE_K_DELAY_LG_OVERPLUSATK;
	int WOE_K_DELAY_RL_GLITTERING_GREED;
	int WOE_K_DELAY_RL_RICHS_COIN;
	int WOE_K_DELAY_RL_MASS_SPIRAL;
	int WOE_K_DELAY_RL_BANISHING_BUSTER;
	int WOE_K_DELAY_RL_B_TRAP;
	int WOE_K_DELAY_RL_S_STORM;
	int WOE_K_DELAY_RL_E_CHAIN;
	int WOE_K_DELAY_RL_QD_SHOT;
	int WOE_K_DELAY_RL_C_MARKER;
	int WOE_K_DELAY_RL_FIREDANCE;
	int WOE_K_DELAY_RL_H_MINE;
	int WOE_K_DELAY_RL_P_ALTER;
	int WOE_K_DELAY_RL_FALLEN_ANGEL;
	int WOE_K_DELAY_RL_R_TRIP;
	int WOE_K_DELAY_RL_D_TAIL;
	int WOE_K_DELAY_RL_FIRE_RAIN;
	int WOE_K_DELAY_RL_HEAT_BARREL;
	int WOE_K_DELAY_RL_AM_BLAST;
	int WOE_K_DELAY_RL_SLUGSHOT;
	int WOE_K_DELAY_RL_HAMMER_OF_GOD;
	int WOE_K_DELAY_RL_R_TRIP_PLUSATK;
	int WOE_K_DELAY_RL_B_FLICKER_ATK;
	int WOE_K_DELAY_RL_GLITTERING_ATK;
	int WOE_K_DELAY_KO_YAMIKUMO;
	int WOE_K_DELAY_KO_JYUMONJIKIRI;
	int WOE_K_DELAY_KO_SETSUDAN;
	int WOE_K_DELAY_KO_BAKURETSU;
	int WOE_K_DELAY_KO_HAPPOKUNAI;
	int WOE_K_DELAY_KO_MUCHANAGE;
	int WOE_K_DELAY_KO_HUUMARANKA;
	int WOE_K_DELAY_KO_MAKIBISHI;
	int WOE_K_DELAY_KO_MEIKYOUSISUI;
	int WOE_K_DELAY_KO_ZANZOU;
	int WOE_K_DELAY_KO_KYOUGAKU;
	int WOE_K_DELAY_KO_JYUSATSU;
	int WOE_K_DELAY_KO_KAHU_ENTEN;
	int WOE_K_DELAY_KO_HYOUHU_HUBUKI;
	int WOE_K_DELAY_KO_KAZEHU_SEIRAN;
	int WOE_K_DELAY_KO_DOHU_KOUKAI;
	int WOE_K_DELAY_KO_KAIHOU;
	int WOE_K_DELAY_KO_ZENKAI;
	int WOE_K_DELAY_KO_GENWAKU;
	int WOE_K_DELAY_KO_IZAYOI;
	int WOE_K_DELAY_KG_KAGEHUMI;
	int WOE_K_DELAY_KG_KYOMU;
	int WOE_K_DELAY_KG_KAGEMUSYA;
	int WOE_K_DELAY_OB_ZANGETSU;
	int WOE_K_DELAY_OB_OBOROGENSOU;
	int WOE_K_DELAY_OB_OBOROGENSOU_;
	int WOE_K_DELAY_OB_AKAITSUKI;
	int WOE_K_DELAY_GC_DARKCROW;
	int WOE_K_DELAY_RA_UNLIMIT;
	int WOE_K_DELAY_GN_ILLUSIONDOPING;
	int WOE_K_DELAY_RK_DRAGONBREATH_W;
	int WOE_K_DELAY_RK_LUXANIMA;
	int WOE_K_DELAY_NC_MAGMA_ERUPTION;
	int WOE_K_DELAY_WM_FRIGG_SONG;
	int WOE_K_DELAY_SO_ELESHIELD;
	int WOE_K_DELAY_SR_FLASHCOMBO;
	int WOE_K_DELAY_SC_ESCAPE;
	int WOE_K_DELAY_AB_OFFERTORIUM;
	int WOE_K_DELAY_WL_TELEK_INTENSE;
	int WOE_K_DELAY_ALL_FULL_THROTTLE;
	int WOE_K_DELAY_SU_BITE;
	int WOE_K_DELAY_SU_SCRATCH;
	int WOE_K_DELAY_SU_STOOP;
	int WOE_K_DELAY_SU_LOPE;
	int WOE_K_DELAY_SU_SPRITEMABLE;
	int WOE_K_DELAY_SU_POWEROFLAND;
	int WOE_K_DELAY_SU_SV_STEMSPEAR;
	int WOE_K_DELAY_SU_CN_POWDERING;
	int WOE_K_DELAY_SU_CN_METEOR;
	int WOE_K_DELAY_SU_SV_ROOTTWIST;
	int WOE_K_DELAY_SU_SV_ROOTTWIST_;
	int WOE_K_DELAY_SU_POWEROFLIFE;
	int WOE_K_DELAY_SU_SCAROFTAROU;
	int WOE_K_DELAY_SU_PICKYPECK;
	int WOE_K_DELAY_SU_PICKYPECK_;
	int WOE_K_DELAY_SU_ARCLOUSEDASH;
	int WOE_K_DELAY_SU_CARROTBEAT;
	int WOE_K_DELAY_SU_POWEROFSEA;
	int WOE_K_DELAY_SU_TUNABELLY;
	int WOE_K_DELAY_SU_TUNAPARTY;
	int WOE_K_DELAY_SU_BUNCHOFSHRIMP;
	int WOE_K_DELAY_SU_FRESHSHRIMP;

} battle_config;

void do_init_battle(void);
void do_final_battle(void);
extern int battle_config_read(const char *cfgName);
extern void battle_validate_conf(void);
extern void battle_set_defaults(void);
int battle_set_value(const char* w1, const char* w2);
int battle_get_value(const char* w1);

#endif /* _BATTLE_H_ */
