/// Copyright (c) Adelays - Licensed under GNU GPL
// adelays@ragnawork.com
// http://ragnawork.com


#ifndef _ADELAYS_H_
#define _ADELAYS_H_

#ifndef _WIN32
#define ADELAYS
#endif

#ifdef ADELAYS

struct Adelays_State {
	unsigned int adelays_showinfo : 1;
	unsigned int adelays_log : 1;
	int adelays_last_skill_id;
	int adelays_previous_skill_id;
	int adelays_hitlock;
	int64 adelays_hitlock_until;
	int64 adelays_skill_tick;
	int adelays_hackcount;
	int adelays_allowedReduction;
	int adelays_jauge;
	int adelays_infixed;
	int adelays_chain_count;
	int adelays_accumulated_delay;
	int adelays_missing_delay;
	int adelays_expected_hard_delay;
	int adelays_previous_client_delay;
	int adelays_last_client_delay;
	int adelays_last_server_delay;
	int adelays_last_amotion;
	int adelays_last_hard;
	int adelays_last_acceptable_delay;
	int adelays_last_skill_failed;
	int adelays_hard_ignore;
	char adelays_player_name[24];
	char adelays_last_result[10];
};

extern struct Adelays_Config {
	int adelays_tol;
	int adelays_hitlock_time_tolerance;
	int adelays_incr;
	int adelays_enable;
	int adelays_max_hitlock_count;
	int adelays_hitlock_incr;
	int adelays_reset_time;
	int adelays_reduce_hitlock_on_correct;
	int adelays_reduce_hitlock_on_hitlock;
	int adelays_reduce_hackcount_on_correct;
	int adelays_reduce_hackcount_on_hitlock;
	int adelays_fixeddelay_tol;
	int adelays_buffer_size;
	int adelays_max_infixed;
	int adelays_buffer_time;
	int adelays_scroll_fix;
	int adelays_jauge_kept_multiplicator;
	int adelays_fix_delay_multiplicator;
	int adelays_jauge_kept_multiplicator_on_new_skill_without_delay;
	int adelays_jauge_augmentation_multiplicator;
	int adelays_jauge_to_decrease_multiplicator;
	int adelays_max_hackcount_to_get_jauge;
	int adelays_acceptable_multiplicator_for_jauge;
	int adelays_delay_multiplicator_to_reduce_hackcount;
	int adelays_max_infixed_to_get_jauge;
	int adelays_jauge_added_after_cutanim;
	int adelays_allow_buffer_on_fixed;
	int adelays_max_aspd;
	int adelays_min_jauge_multiplicator;
	int adelays_allow_harddelays;
	int adelays_put_hard_limit_on_all_skills;
	int adelays_hard_chain_limit;
	int adelays_sit_delay_bodyreloc;

} adelays_config; 

int adelays_battle_config_read();
void adelays_set_defaults(void);
int adelays_init();



int adelays_read_db();
int	adelays_is_sit_allowed(struct Adelays_State *adstate);
int  adelays_recordSkillDelay (int skillid, int delayfix, int cooldown, struct Adelays_State *adstate, int64 tick, unsigned short amotion, short _class, unsigned char sex, unsigned int isRiding, int minskilldelay);
void adelays_set1HitLock(struct Adelays_State *adstate);
void adelays_setHitLock(struct Adelays_State *adstate);
void adelays_setHitLockUntil(struct Adelays_State *adstate, int64 tick);
void adelays_dataset(struct Adelays_State *adstate, char name[24]);
int adelays_canuseitem(int i, int64 cannact_tick);
float adelays_getVersion();
extern int64 adelays_gettick();
extern int adelays_skill_name2id(const char * skillName);
extern int adelays_skill_get_index(int id);
extern const char *  adelays_skill_get_name(int id);
int adelays_get_log_message(char message[256], struct Adelays_State *adstate);
int adelays_process_record(int64 tick, int64 * cannact_tick, struct Adelays_State *adstate, int minskilldelay);
int adelays_get_updates();
void adelays_skillFailed(struct Adelays_State * adstate);
int adelays_set_damages(struct Adelays_State *srcadstate, struct Adelays_State * destadstate, int skillId, int64 tick, int sdelay, int damage, int div);
int adelays_is_enabled();
int adelays_check_ip(const int fd);
int adelays_time_to_hard(struct Adelays_State *adstate, int skillId);
void adelays_calculateMissingDelay(int skillid, struct Adelays_State *adstate, int amotion, int minskilldelay);
int adelays_get_harddelay(int skillId, int amotion, int skillamount, int minskilldelay);
int adelays_calculateAcceptableDelay(int skillId, int amotion);
int adelays_getFixDelay(unsigned short amotion, int skillid, int minskilldelay);
int adelays_skillHasDoubles(int skillId);
int adelays_setJobDelay(int count, int option, int classId, int sex, int per);
int adelays_setItemDelays(int count, int nameid, int option);
int adelays_setskillDelays(char * skillName, int delayAt150, int delayAt190, int option, int allowhitlock);

int adelays_server_config_read();
int adelays_get_force_harddelay(int id);
int adelays_getHardDelayChainCount(int id);
int adelays_get_harddelay190(int id);
int adelays_get_harddelay150(int id);
int adelays_get_delay190(int id);
int adelays_get_option(int id);
int adelays_get_allowhitlock(int id);
int adelays_canHardDelayOnThatSkill(int id);
int adelays_setSkillHardDelays(char * skillName, int delayAt150, int delayAt190, int chainMax, int doubles);

#endif
#endif /* _ADELAYS_H_ */